The files listed here are used to create the bulk of automatically generated files with `new_file.py` script.

Most of them intend to fill the content inside the namespace block; they are copied as such with `[[CLASS_NAME]]`that will be replaced by the adequate name chosen by the end used when calling the script.