    //! \copydoc doxygen_hide_simple_varf
    class [[CLASS_NAME]] final
    // clang-format off
    : public ::MoReFEM::VariationalFormulation
    <
        [[CLASS_NAME]],
        EnumUnderlyingType(SolverIndex::solver),
        time_manager_type
    >
    // clang-format on
    {
    private:
        
        //! \copydoc doxygen_hide_alias_self
        // \TODO This might seem a bit dumb but is actually very convenient for template classes.
        using self = [[CLASS_NAME]];
        
        //! Alias to the parent class.
        // clang-format off
        using parent = ::MoReFEM::VariationalFormulation
        <
            self,
            EnumUnderlyingType(SolverIndex::solver),
            time_manager_type
        >;
        // clang-format on
        
        static_assert(std::is_convertible<self*, parent*>());
        
        //! Friendship to parent class, so this one can access private methods defined below through CRTP.
        friend parent;
        
    public:
        
        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;
        
    public:

        /// \name Special members.
        ///@{

        //! \copydoc doxygen_hide_varf_constructor
        explicit [[CLASS_NAME]](const GodOfDof& god_of_dof,
                                        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
                                        morefem_data_type& morefem_data);

        //! Destructor.
        ~[[CLASS_NAME]]() override;

        //! \copydoc doxygen_hide_copy_constructor
        [[CLASS_NAME]](const [[CLASS_NAME]]& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        [[CLASS_NAME]]([[CLASS_NAME]]&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        [[CLASS_NAME]]& operator=(const [[CLASS_NAME]]& rhs) = delete;
        
        //! \copydoc doxygen_hide_move_affectation
        [[CLASS_NAME]]& operator=([[CLASS_NAME]]&& rhs) = delete;

        ///@}
        
        
    private:

        /// \name CRTP-required methods.
        ///@{

        //! \copydoc doxygen_hide_varf_suppl_init
        void SupplInit(const morefem_data_type& morefem_data);

        /*!
         * \brief Allocate the global matrices and vectors.
         */
        void AllocateMatricesAndVectors();

        //! Define the pointer function required to test the convergence required by the non-linear problem.
        Wrappers::Petsc::Snes::SNESConvergenceTestFunction ImplementSnesConvergenceTestFunction() const;



        ///@}
        
    private:
        
        
        /// \name Global variational operators.
        ///@{
        
        // \TODO Define there as data attributes the global operators involved in the formulation.
        // They should be defined as std::unique_ptr; accessors that returns a reference to them should also be
        // foreseen.
    
        ///@}
        
    private:
        
        /// \name Global vectors and matrices specific to the problem.
        ///@{
        
        // \TODO Define here the GlobalMatrix and GlobalVector objects into which the global operators are 
        // assembled.
        
        ///@}
        
    private:
        
           

    };
