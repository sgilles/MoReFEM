# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
#######################################################################################################################
# *** MoReFEM copyright notice *** < #

import argparse
import os
import pathlib
import sys
import tempfile

from enum import Enum

# Clunky way to make substitute_env_variable available
utilities_abspath = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "Utilities")
sys.path.append(utilities_abspath)
from substitute_env_variable import SubstituteEnvVariable


morefem_copyright_notice = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "..", "Documentation", "CopyrightNotice.txt")    


class BlockDelimiters:
    """
    Handy class to define what constitute the beginning and end lines of blocks whose content
    should be automatically generated.

    All instances should be created within ExistingBlockDelimiters object - thus enabling 
    further reuse of the script for external models that do not share the project name.

    @param comment_signs '//' for C++, '#' for Python.
    @param project Name of the project.
    @param text Specific text defining the specific block.
    """
    def __init__(self, text, project = "MoReFEM", comment_signs = "//", is_external = False):
        self.start = f"{comment_signs} > *** {project} {text} *** {comment_signs}"
        self.end = f"{comment_signs} *** {project} {text} *** < {comment_signs}"
        self._is_external = is_external

    def __repr__(self):
        return f"{self.start}\n{self.end}\n"


class ExistingBlockDelimiters:
    """
    Class to store the existing block delimiters.
    """

    def __init__(self, project = "MoReFEM", is_external = False):
        """
        @param is_external If True, script is called for file(s) that are not directly included 
        in MoReFEM core library. In this case, the blocks regarding Doxygen groups are skipped.
        """        
        self.header_guards = BlockDelimiters("header guards", project = project)
        self.end_header_guards = BlockDelimiters("end header guards", project = project)        
        self.python_banner = BlockDelimiters("copyright notice", comment_signs = "#", project = project)

        if is_external:
            self.cpp_banner = BlockDelimiters("copyright notice", project = project)
            self.doxygen_end_of_group = ""
        else:
            self.cpp_banner = BlockDelimiters("Doxygen information and copyright notice", project = project)
            self.doxygen_end_of_group = BlockDelimiters("Doxygen end of group", project = project)


class BlockError(Exception):
    pass

class BlockNotFoundError(BlockError):

    def __init__(self, file, block_delimiter):
        super(BlockNotFoundError, self).__init__(f"Couldn't handle file {file}: no block starting with line"
                                f" '{block_delimiter.start}' was found.")
        self.file = file # Could be handy if user wants only file list and not the complete exception message
                
class BlockNotExitedError(BlockError):

    def __init__(self, file, block_delimiter):
        super(BlockNotExitedError, self).__init__(f"Couldn't handle file {file}: the starting line '{block_delimiter.start}' was found "
                                f"but not the closing line '{block_delimiter.end}'.")        
        self.file = file

class GenerateAutomatedBlocksContentInSourceFiles():
    """
    This class skims through all source and header files within a directory, and for each of them:

    - Checks that several expected blocks are indeed present (it really checks the beginning and the end lines) 
    that act as delimiters are found).
    - Generates the expected content inside said block. If this matches exactly what is already in the file, nothing
    is done. If not, the file is replaced by another with the correct content within the block.

    Several such blocks are present:

    - One at the top of the file which provides copyright notice and provides Doxygen group information (based on )
    the first level subdirectory into which the file is located - 'Core', 'Geometry', 'Model', etc...
    - One near the end to signal the Doxygen group must be closed.
    - If the file is a header, two blocks are also present to handle header guards.

    Init the class itself only initialize useful content to do the work; to really do it method 'CorrectAutomatedBlocksInAllFiles' or 'CorrectAutomatedBlocksInOneFile' must be called.

    """
    def __init__(self, directory, copyright_notice, project = "MoReFEM", is_external = False):
        """
        @param project Name of the project ('MoReFEM' for core library)
        @param[in] directory Directory that is walked through. This is intended to be 'Sources' or 'Scripts' directories at the root of the project for the core library.
        @param[in] copyright_notice Path to a file which specifies the text that should appear in the banner at the top of each source and header files. May be completed by information useful for Doxygen in C++ files.
        @param is_external If True, script is called for file(s) that are not directly included 
        in MoReFEM core library. In this case, the blocks regarding Doxygen groups are skipped.
        """
        self.existing_block_delimiters = ExistingBlockDelimiters(project, is_external)
        self._Nparts = -1

        self.directory = directory
        self.copyright_notice = copyright_notice
        self._is_external = is_external

        if not  os.path.exists(self._directory):
            raise Exception("[WARNING] Directory {0} not found.".format(self._directory))

    @property
    def directory(self):
        return self._directory

    @directory.setter
    def directory(self, value):
        self._directory = os.path.normpath(SubstituteEnvVariable(value))
        # Helper value to extract module names
        self._Nparts = len(pathlib.Path(self._directory).parts)

    @property
    def NpartsInDirectory(self):
        assert(self._Nparts != -1)
        return self._Nparts
    
    @property
    def copyright_notice(self):
        return self._copyright_notice

    @copyright_notice.setter
    def copyright_notice(self, path):

        if not os.path.exists(path):
            raise Exception(f"File {path} supposed to hold copyright notice was not found!")

        # Copyright notice is kept as a list of lines.
        with open(path) as file:
            self._copyright_notice = [line.rstrip() for line in file]

        self._python_copyright_notice = [f"# {item}".rstrip() for item in self._copyright_notice]
        self._cplusplus_copyright_notice = [f" * {item}".rstrip() for item in self._copyright_notice]
     
        
    def _ExtractModule(self, directory):
        """
        Extract from current path the module ('Utilities', 'Core', 'Geometry', etc...) concerned.
        """
        assert(not self._is_external)
        assert(directory.startswith(self.directory))

        return pathlib.Path(directory).parts[self.NpartsInDirectory]


    def CorrectAutomatedBlocksInAllFiles(self):
        """
        Method which does the bulk of the work for all files found in directory.
        """
        self._exception_list = []

        for root, dirs, files in os.walk(self._directory):
            
            if root == self.directory:
                continue

            no_header_guards = False
 
            # Very specific case: we don't want header guards for the header files whose role is to
            # include third part warnings with some compiler warnings deactivated.
            if root.endswith("IgnoreWarning"):
                no_header_guards = True

            if self._is_external:
                module_name = None
            else:
                module_name = self._ExtractModule(root)

            # Work on files that match the extension.
            for myfile in files:
                extension = os.path.splitext(myfile)[1]
                extension = extension[1:]

                filepath = os.path.join(root, myfile)

                if extension == "cpp":
                    self.__CorrectAutomatedBlocksInCppFile(filepath, module_name)
                elif extension in ("hpp", "hxx"):
                    self.__CorrectAutomatedBlocksInHeaderFile(filepath, module_name, no_header_guards)
                elif extension == "py":
                    self.__CorrectAutomatedBlocksInPythonFile(filepath) 
                   
        if self._exception_list:
            print("=== The following files could not be handled as problems were reported: \n" )
            print("\t - {}".format("\n\t - ".join(self._exception_list)))

    def CorrectAutomatedBlocksInOneFile(self, file_absolute_path):
        """
        Method which does the bulk of the work for one specific file.

        @param file_absolute_path Absolute path to the file for which content is generated (must be inside self._directory)

        WARNING: this was added after the other method that deal with all files,
        and code is not completely DRY. I deem it acceptable as it is just for an
        aside script that shouldn't evolve much, but if I'm wrong refactoring should
        be on the table.

        This method is useful on other scripts such as new_file.py
        """
        self._exception_list = []

        root, filename = os.path.split(file_absolute_path)

        no_header_guards = False

        # Very specific case: we don't want header guards for the header files whose role is to
        # include third part warnings with some compiler warnings deactivated.
        if root.endswith("IgnoreWarning"):
            no_header_guards = True

        if self._is_external:
            module_name = None
        else:
            module_name = self._ExtractModule(root)

        extension = os.path.splitext(filename)[1]
        extension = extension[1:]

        if extension == "cpp":
            self.__CorrectAutomatedBlocksInCppFile(file_absolute_path, module_name)
        elif extension in ("hpp", "hxx"):
            self.__CorrectAutomatedBlocksInHeaderFile(file_absolute_path, module_name, no_header_guards)
        elif extension == "py":
            self.__CorrectAutomatedBlocksInPythonFile(file_absolute_path) 
                   
        if self._exception_list:
            print("=== The following file could not be handled as problems were reported: \n" )
            print("\t - {}".format("\n\t - ".join(self._exception_list)))


    def __CorrectAutomatedBlocksInCppFile(self, filepath, module_name):
        """
        Apply changes in the case @filepath is a cpp file.
        """
        try:
            was_banner_modified = self._CheckAndFixBannerForCplusplusFile(filepath, module_name)
            was_doxygen_endgroup_modified = self._CheckAndFixDoxygenEndgroup(filepath, module_name)

            if was_banner_modified or was_doxygen_endgroup_modified:
                print(f"File {filepath} was modified.")
                # Note: I'm fully aware message will not be printed if banner was modified and then
                # an exception is thrown in one of the following. That is not an issue: the file will
                # appear in the list of exceptions later, that anyway call for a closer look that just
                # the files that were modified.
        except BlockError as e:
            self._exception_list.append(str(e))


    def __CorrectAutomatedBlocksInHeaderFile(self, filepath, module_name, no_header_guards = False):
        """
        Apply changes in the case @filepath is a hpp or hxx file.

        @param no_header_guards If True, don't look for a block for header guards. This is solely to deal
        with the special header files in Utilities/Warnings/Internal/IgnoreWarning.
        """
        assert(filepath.endswith("hpp") or filepath.endswith("hxx"))

        try:
            was_banner_modified = self._CheckAndFixBannerForCplusplusFile(filepath, module_name)
            was_doxygen_endgroup_modified = self._CheckAndFixDoxygenEndgroup(filepath, module_name)

            # Later case is to account for an ugly work around used in some tests.
            if no_header_guards or filepath.endswith("AnonymousNamespace.hpp"):
                were_header_guards_modified = False
            else:
                were_header_guards_modified = self._CheckAndFixHeaderGuards(filepath)

            if was_banner_modified or was_doxygen_endgroup_modified or were_header_guards_modified:
                print(f"File {filepath} was modified.")
                # Note: I'm fully aware message will not be printed if banner was modified and then
                # an exception is thrown in one of the following. That is not an issue: the file will
                # appear in the list of exceptions later, that anyway call for a closer look that just
                # the files that were modified.
        except BlockError as e:
            self._exception_list.append(str(e))
            #self._exception_list.append(e.file) ++ Use this line if you wish just names of the
            #file without exception message (for debug)



    def __CorrectAutomatedBlocksInPythonFile(self, filepath):
        """
        Apply changes in the case @filepath is a Python file.
        """
        assert(filepath.endswith("py"))

        try:
            was_banner_modified = self._CheckAndFixBannerForPythonFile(filepath)

            if was_banner_modified:
                print(f"File {filepath} was modified.")
                # Note: I'm fully aware message will not be printed if banner was modified and then
                # an exception is thrown in one of the following. That is not an issue: the file will
                # appear in the list of exceptions later, that anyway call for a closer look that just
                # the files that were modified.
        except BlockError as e:
            self._exception_list.append(str(e))


    def _CheckAndFixDoxygenEndgroup(self, myfile, module_name):
        """
        Check that there is a block present to properly close Doxygen group at the end of the file.

        If self._is_external is True, this method always return False.

        @return True if the block was modified. 
        """
        if self._is_external:
            assert(module_name is None)
            return False

        expected_content = [ r"///@}} // \addtogroup {}Group".format(module_name), ]

        return self.__CheckAndFixBlock(myfile, self.existing_block_delimiters.doxygen_end_of_group, expected_content)
    

    def _CheckAndFixHeaderGuards(self, myfile):
        """
        Check header guards blocks (there are two of thems: opening one and closing one at the end of the file)

        @return True if the block was modified. False is returned for source files.
        """
        assert(myfile.endswith("hpp") or myfile.endswith("hxx"))

        header_guard = self._GenerateHeaderGuardName(myfile)

        expected_content = [ 
            fr"#ifndef {header_guard}",
            fr"#define {header_guard}"
        ]

        if myfile.endswith("hxx"):
            hxx_path = self.__ExtractRelativePathFromSources(myfile)
            hpp_path = [item.replace(".hxx", ".hpp") for item in hxx_path]
            
            expected_content.extend([
                fr'// IWYU pragma: private, include "{"/".join(hpp_path)}"',
                ])

        return_value = self.__CheckAndFixBlock(myfile, self.existing_block_delimiters.header_guards, expected_content)

        expected_content = [ fr"#endif // {header_guard}", ]
        
        return_value = self.__CheckAndFixBlock(myfile, self.existing_block_delimiters.end_header_guards, expected_content) or return_value

        return return_value




    def _CheckAndFixBannerForCplusplusFile(self, myfile, module_name):
        """
        Check and fix the copyright notice block at the top of the file (there are no checks 
        the block is at the top of the file but it is what is done for all of them).

        @param module_name Name of the module concerned  ('Utilities', 'Core', 'Geometry', etc...).

        @return True if the block was modified.
        """
        expected_content = [
        "/*!"]

        expected_content.extend(self._cplusplus_copyright_notice)

        expected_content.extend([
        " *",
        r" * \file",
        " *"])

        if not self._is_external:
            assert(module_name is not None)
            expected_content.extend([r" * \ingroup {}Group".format(module_name),
                                     r" * \addtogroup {}Group".format(module_name),
                                     r" * @{"])
        
        expected_content.append(r" */")

        return self.__CheckAndFixBlock(myfile, self.existing_block_delimiters.cpp_banner, expected_content)
    

    def _CheckAndFixBannerForPythonFile(self, myfile):
        """
        Check and fix the copyright notice block at the top of the file (there are no checks 
        the block is at the top of the file but it is what is done for all of them).

        @return True if the block was modified.
        """
        hash_sign_line = "#######################################################################################################################"

        expected_content = [ hash_sign_line ]
        expected_content.extend(self._python_copyright_notice)
        expected_content.append(hash_sign_line)

        return self.__CheckAndFixBlock(myfile, self.existing_block_delimiters.python_banner, expected_content)


    def __CheckAndFixBlock(self, myfile, block_delimiter, expected_content):
        """Open the file and check the block that is delimited by 'block_delimiter' object  
        is up-to-date with expected content.

        @return True if the block was modified.
        """
        original_file = os.path.join(self._directory, myfile)   

        Status = Enum('Status', ['not_found', 'inside', 'exited'])

        status = Status.not_found
        read_content = []

        start_block = block_delimiter.start
        end_block = block_delimiter.end

        with open(original_file) as FILE_in:
            
            for line in FILE_in:
                stripped_line = line.rstrip("\r\n\t ")

                if stripped_line == start_block:
                    status = Status.inside
                    continue

                if status != Status.inside:
                    continue

                if stripped_line == end_block:
                    status = Status.exited
                    break

                read_content.append(stripped_line)

            if status == Status.inside:
                raise BlockNotExitedError(original_file, block_delimiter)
            
            if status == Status.not_found:
                raise BlockNotFoundError(original_file, block_delimiter)

        if read_content != expected_content:
       
            rewritten_file = tempfile.NamedTemporaryFile(mode='w+t', delete = False)

            with open(original_file) as FILE_in:

                for line in FILE_in:
                    stripped_line = line.strip("\r\n\t ")

                    if stripped_line == start_block:
                        status = Status.inside
                        rewritten_file.write(f"{start_block}\n")
                        rewritten_file.write("{}\n".format("\n".join(expected_content)))
                        rewritten_file.write(f"{end_block}\n")
                        continue

                    if status == Status.inside:
                        if stripped_line == end_block:
                            status = Status.exited
                        continue
                    
                    rewritten_file.write(f"{line}")
        
            os.remove(original_file)
            os.rename(rewritten_file.name, original_file)
            return True

        return False


    def _GenerateHeaderGuardName(self, filepath):
        """Generate a unique name for header guards from the relative location to the source of the project.

        For instance, PictStock -> PICT_STOCK

        @param[in] filepath Absolute path of the file for which a header guard is generated. It is expected
        this path is within self._directory.
        """        
        useful_part_of_path = self.__ExtractRelativePathFromSources(filepath)
        
        modified = [item.replace('.', '_DOT_').upper() for item in useful_part_of_path]

        return f"MOREFEM_{"_".join(modified)}_"


    def __ExtractRelativePathFromSources(self, filepath):
        """
        Extract the relative path of 'filepath' relatively to self.directory.
        
        @return The different parts of the remaining path as a tuple.
        """
        assert(filepath.startswith(self.directory))
        useful_part_in_name = pathlib.Path(filepath).parts[self.NpartsInDirectory:]
        return useful_part_in_name


if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(
                    prog="generate_automated_blocks_content_in_source_files",
                    description='Facility to fill automatically pre-identified blocks in all source and header files located within specified directory.')
    
    parser.add_argument('--project', help = "Name of the project. If none 'MoReFEM' is assumed.") 
    parser.add_argument('--copyright_notice', help = "Path to a file which specifies the text that should appear "
                        "in the banner at the top of each source and header files. If none specified, default "
                        "MoReFEM copyright notice is used.")
    parser.add_argument('--directory', help = "All source and header files inside this directory will be considered."
                        "If none specified, it is assumed the intent is to run it for 'Scripts' and 'Sources' "
                        "directory in MoReFEM core library. If one is specified, it is assumed script is run for "
                        "an external project.")

    args = parser.parse_args()

    project = args.project or "MoReFEM"
    copyright_notice = args.copyright_notice or morefem_copyright_notice
    
    if args.directory is None:
        directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "..", "Sources")
        object = GenerateAutomatedBlocksContentInSourceFiles(directory, copyright_notice = copyright_notice, project=project, is_external=False)
        object.CorrectAutomatedBlocksInAllFiles()

        directory = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "..", "Scripts")
        object = GenerateAutomatedBlocksContentInSourceFiles(directory, copyright_notice = copyright_notice, project=project, is_external=False)
        object.CorrectAutomatedBlocksInAllFiles()
    else:
        object = GenerateAutomatedBlocksContentInSourceFiles(args.directory, copyright_notice = copyright_notice, project=project, is_external=True)
        object.CorrectAutomatedBlocksInAllFiles()



