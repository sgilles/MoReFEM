# This is not a genuine file: I messed up with the summary lines so that the test is more meaningful (catching many 0 could hide issues).

==1371== Memcheck, a memory error detector
==1371== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==1371== Using Valgrind-3.16.0 and LibVEX; rerun with -h for copyright info
==1371== Command: Sources/MoReFEM4Heat -i /builds/MoReFEM/CoreLibrary/MoReFEM/Sources/ModelInstances/Heat/demo_input_heat.lua -e MOREFEM_RESULT_DIR=Results -e MOREFEM_PREPARTITIONED_DATA_DIR=PrepartitionedData --overwrite_directory
==1371== Parent PID: 10
==1371== 
==1371== 
==1371== HEAP SUMMARY:
==1371==     in use at exit: 437 bytes in 14 blocks
==1371==   total heap usage: 205,329 allocs, 205,315 frees, 65,723,859 bytes allocated
==1371== 
==1371== 8 bytes in 1 blocks are indirectly lost in loss record 3 of 14
==1371==    at 0x483980B: malloc (vg_replace_malloc.c:307)
==1371==    by 0x138E1CD6: ???
==1371==    by 0x138E23E0: ???
==1371==    by 0x138E495B: ???
==1371==    by 0x138E0712: ???
==1371==    by 0x138E0450: ???
==1371==    by 0x139322FD: ???
==1371==    by 0x139B4A1E: ???
==1371==    by 0x139B364C: ???
==1371==    by 0x139A86E1: ???
==1371==    by 0x13854D49: ???
==1371==    by 0x138544E7: ???
==1371== 
{
   <insert_a_suppression_name_here>
   Memcheck:Leak
   match-leak-kinds: indirect
   fun:malloc
   obj:*
   obj:*
   obj:*
   obj:*
   obj:*
   obj:*
   obj:*
   obj:*
   obj:*
   obj:*
   obj:*
}
==1371== LEAK SUMMARY:
==1371==    definitely lost: 578 bytes in 17 blocks
==1371==    indirectly lost: 8 bytes in 1 blocks
==1371==      possibly lost: 15 bytes in 3 blocks
==1371==    still reachable: 245 bytes in 24 blocks
==1371==         suppressed: 429 bytes in 13 blocks
==1371== 
==1371== For lists of detected and suppressed errors, rerun with: -s
==1371== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 13 from 13)
