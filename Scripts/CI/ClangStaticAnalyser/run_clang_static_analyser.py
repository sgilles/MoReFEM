# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
#######################################################################################################################
# *** MoReFEM copyright notice *** < #

import os
import sys
import shutil
import subprocess
import time


if __name__ == "__main__":
    
    morefem_source_dir = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..", "..", "..", "Sources")

    build_dir = os.path.join(morefem_source_dir, "..", "build_4_sonarqube")
        
    if os.path.exists(build_dir):
        shutil.rmtree(build_dir)
        
    os.mkdir(build_dir)
    os.chdir(build_dir)
    
    cmd = ( "python", \
            "../cmake/Scripts/configure_cmake.py", \
            "--cache_file=../cmake/PreCache/linux.cmake", \
            "--cmake_args=\"-G Ninja\"", \
            "--install_directory=/opt",
            "--no_run_command" )    
    
    start = time.time()
    
    subprocess.Popen(cmd, shell = False).communicate()
    
    step1 = time.time()
    print("Preparing CMakeLists.txt done - {} seconds elapsed".format(round(step1 - start, 1)))
    
    cmd = ( "scan-build",
            "-v",
            "--plist-html",
            "--intercept-first",
            "--analyze-headers",
            "-o",
            "analyzer_reports",
            "cmake",
            "-C",
            "PreCacheFile.cmake",
            "-G",
            "Ninja",
            "-DCMAKE_VERBOSE_MAKEFILE=ON",
            "-DCMAKE_EXPORT_COMPILE_COMMANDS=ON",
            "..")
            
    step2 = time.time()
    print("Running CMake done - {} seconds elapsed".format(round(step2 - start, 1)))    

    subprocess.Popen(cmd, shell = False).communicate()
            
    cmd = "scan-build -v --plist-html --intercept-first --analyze-headers -o analyzer_reports ninja 2>&1 | tee morefem-build.log"
    
    step3 = time.time()
    print("Running scan build - {} seconds elapsed".format(round(step3 - start, 1)))    
    
    subprocess.Popen(cmd, shell = True).communicate()

    end = time.time()
    print("Output written in analyzer_reports directory; analysis performed in {} seconds".format(round(end - start, 1)))