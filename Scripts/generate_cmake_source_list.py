# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
# 
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
#######################################################################################################################
# *** MoReFEM copyright notice *** < #

import os


def FilterCppFiles(file_list):
    """Keep in the \a file_list given in input only the files which extension is cpp, hpp or hxx.

    \\param[in] file_list Initial file list.

    \\return A pair with cpp file list in the first slot and other file list (hpp and hxx) in the second one.
    """
    ret = []

    public_extension = "cpp"
    private_extension_list = ("hpp", "hxx", "doxygen", "cmake", "txt")

    public_list = []
    private_list = []

    for myfile in file_list:
        extension = os.path.splitext(myfile)[1]

        if extension.startswith('.'):
            extension = extension[1:]

        if extension == public_extension:
            public_list.append(myfile)
        elif extension in private_extension_list:
            private_list.append(myfile)

    public_list.sort()
    private_list.sort()

    return (public_list, private_list)



def GenerateInModule(morefem_directory, subdirectory, module_alias, ignore_list = None):
    """
    \\param[in] morefem_directory Path to the MoReFEM directory.
    \\param[in] subdirectory Name of the subdirectory being investigated, within "Sources" directory. e.g 'Utilities', 'ThirdParty', etc...
    \\param[in] module_alias Alias to the library being built, e.g. 'MOREFEM_UTILITIES'. If BUILD_MOREFEM_UNIQUE_LIBRARY is set, this alias will actually point to the MoReFEM library.
    \\param[in] ignore_list If None, all subdirectories of _subdirectory_ are included. If a list is given, each subfolder named after an element of the list is ignored. For instance for 'ThirdParty' there is 'Tclap'in the list; Tclap is a header only library embedded in MoReFEM which source files aren't required in the build.
    """
    source_directory = directory = os.path.join(morefem_directory, "Sources")
    directory = os.path.join(source_directory, subdirectory)

    tab_prefix = "\t\t"

    for root, dirs, files in os.walk(directory):

        if ignore_list:
            dirs[:] = [d for d in dirs if d not in ignore_list]
            
        dirs.sort()
    
        (public_list, private_list) = FilterCppFiles(files)
        
#        reduced_source_root = os.path.relpath(root, source_directory)

        with open(os.path.join(root, "SourceList.cmake"), "w") as SourceList:

            SourceList.write("### ===================================================================================\n"
            "### This file is generated automatically by Scripts/generate_cmake_source_list.py.\n"
            "### Do not edit it manually! \n"
            "### Convention is that:\n"
            "###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.\n"
            "###.  - When it is generated automatically, it is named SourceList.cmake.\n"
            "### ===================================================================================\n\n\n")



            SourceList.write("target_sources({}{}{}\n".format("${", module_alias, "}"))

            if public_list:
                SourceList.write("\n\tPRIVATE\n")

                for myfile in public_list:
                    SourceList.write("{}{}/{}\n".format(tab_prefix, r"${CMAKE_CURRENT_LIST_DIR}", myfile))

            # PUBLIC would be better for IDE but I don't know how to make it work with FindMoReFEM functionality.
            SourceList.write("\n\tPRIVATE\n")

            for myfile in private_list:
                SourceList.write("{}{}/{}\n".format(tab_prefix, r"${CMAKE_CURRENT_LIST_DIR}", myfile))

            SourceList.write(")\n\n")

            dirs.sort()

            for directory in dirs:
                SourceList.write("include({}/{}/SourceList.cmake)\n".format(r"${CMAKE_CURRENT_LIST_DIR}", directory))


if __name__ == "__main__":
    morefem_path = os.path.join(os.path.dirname(os.path.realpath(__file__)), "..")
    
    GenerateInModule(morefem_path, "Utilities", "MOREFEM_UTILITIES")
    GenerateInModule(morefem_path, "ThirdParty", "MOREFEM_UTILITIES") # not a typo
    GenerateInModule(morefem_path, "Core", "MOREFEM_CORE")
    GenerateInModule(morefem_path, "Geometry", "MOREFEM_GEOMETRY")
    GenerateInModule(morefem_path, "FiniteElement", "MOREFEM_FELT")
    GenerateInModule(morefem_path, "Parameters", "MOREFEM_PARAM")
    GenerateInModule(morefem_path, "Operators", "MOREFEM_OP")
    GenerateInModule(morefem_path, "OperatorInstances", "MOREFEM_OP_INSTANCES")
    GenerateInModule(morefem_path, "ParameterInstances", "MOREFEM_PARAM_INSTANCES")
    GenerateInModule(morefem_path, "FormulationSolver", "MOREFEM_FORMULATION_SOLVER")
    GenerateInModule(morefem_path, "Model", "MOREFEM_MODEL")
