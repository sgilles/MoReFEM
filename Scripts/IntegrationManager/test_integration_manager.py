# > *** MoReFEM copyright notice *** #
#######################################################################################################################
# This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
# It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
#
# MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
# Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
#######################################################################################################################
# *** MoReFEM copyright notice *** < #

import os
from integration_manager import extract_issue_numbers

def test_extract_issue_numbers():

    assert(extract_issue_numbers("#1784 ") == [ 1784 ])

    assert(extract_issue_numbers("#1784 - #234 :") == [ 1784, 234 ])

    assert(extract_issue_numbers("#1784 `foo` - #234 ad") == [ 1784, 234 ])

    assert(extract_issue_numbers("qwerty") == None)

    assert(extract_issue_numbers("#1784:") == [ 1784 ])

    assert(extract_issue_numbers("#1784 #abcd #4a #a4 ") == [ 1784 ])