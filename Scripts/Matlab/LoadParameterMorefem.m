function parameter_out = LoadParameterMoReFEM(path, file_name, type_of_element)

if (strfind(file_name,'.dat'))
    parameter_file_name = [path file_name];
else
    parameter_file_name = [path file_name '.dat'];
end

parameter_file = fopen( parameter_file_name, 'rt' );

parameter_out = [];

while (~feof( parameter_file ))
    str = fgetl( parameter_file );
    str = strtrim(str);
    if (strfind(str,type_of_element))
        beginPos = strfind(str,';');
        parameter_out = [parameter_out ; str2num(str(beginPos(end)+1:end))];
    end
end

fclose( parameter_file );

end