function indices_out = CreateIndicesOutMoReFEMToMatlab(interfaces_indices, dof_infos_indices, dimension)

nDof = length(dof_infos_indices);
nVertex = length(interfaces_indices);

indices_out = zeros(nDof, 1);

for i=0:(nVertex-1)
    for j=0:(dimension-1)
        vertex_index = dof_infos_indices(i*dimension + j + 1);
        %vertex_index
        %interfaces_indices(vertex_index + 1)
        %interfaces_indices(vertex_index+ 1)*dimension + j
        indices_out(i*dimension + j + 1) = interfaces_indices(vertex_index+ 1)*dimension + j - dimension;
    end
end

end