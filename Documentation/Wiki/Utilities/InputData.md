[TOC]

# As a model user

## Very basic executable run

When you're using a model, let's say `Elasticity` model here for the sake of argument (which is one provided with the library), you will have to invoke it through a line that looks something like (in its most basic form; I also didn't bother with paths here):

````
MoReFEM4Elasticity -i my_input_data.lua
````

or 

````
mpirun -np 4 MoReFEM4Elasticity -i my_input_data.lua
````

if you're running in parallel mode (here on 4 ranks).

There are actually 2 very different behaviour depending whether `my_input_data.lua` exists or not:

- If it does exist, the file is read and its content is interpreted for the model. If it is ok (and if there are no issue with output directory - see [below](#OutputDirectory)) the model will run its course.
- If it does not, a default file is generated on the root processor and the program stops. The file must then be filled properly by the model developer (some fields get default values that are the most likely choices, but for others such as for instance path to a file it is a text telling what must be provided). Once it's done, it must be copied for all ranks for parallel mode (if several filesystems are used - if you're running in parallel on your computer no need to bother). And then you may try again previous step and this time the model should run properly (provided the file was correctly filled of course).

For all models embedded directly in the library, there are files prefixed by `demo` that are ready to use to check models. They are not meant to be modified - if you want to play with it the proper approach is to copy them and then work upon the copy. These `demo` files are used in the test suite and modifying them would break some tests.


<div id="ContentInputDataFile"></div>

## Content of the input data file

Let's take an example of a so-called `section` in the input data file:


````
-- Highest dimension geometric elements
-- mesh_index: { 1 }
Domain1 = {

	-- List of dimensions encompassed by the domain. Might be left empty if no restriction at all upon 
	-- dimensions. 
	-- Expected format: { VALUE1, VALUE2, ...}
	-- Constraint: value_in(v, {0, 1, 2, 3})
	dimension_list = { 2 },

	-- List of mesh labels encompassed by the domain. Might be left empty if no restriction at all upon mesh 
	-- labels. This parameter does not make sense if no mesh is defined for the domain. 
	-- Expected format: { VALUE1, VALUE2, ...}
	mesh_label_list = {  },

	-- List of geometric element types considered in the domain. Might be left empty if no restriction upon 
	-- these. No constraint is applied at  OptionFile level, as some geometric element types could be added 
	-- after generation of current input data file. Current list is below; if an incorrect value is put there it 
	-- will be detected a bit later when the domain object is built. 
	--  The known types when this file was generated are:  
	--  . Point1 
	--  . Segment2, Segment3 
	--  . Triangle3, Triangle6 
	--  . Quadrangle4, Quadrangle8, Quadrangle9 
	--  . Tetrahedron4, Tetrahedron10 
	--  . Hexahedron8, Hexahedron20, Hexahedron27. 
	-- Expected format: {"VALUE1", "VALUE2", ...}
	geometric_element_type_list = {  }

} -- Domain1
````

With data provided here, MoReFEM will be able to define a `Domain` object that will be used in the `Model`.

Some very basic syntax you will need to fill it:

- `--` marks a comment - all that is after this in a given line is ignored.
<div id="Section"></div>

- `Domain1` is a _section_  (it is even an _indexed_section_ but when just using a pre-existing model you don't need to bother about the distinction).
- `dimension_list`, `mesh_label_list` and `geometric_element_type_list` are _leaves_ - this means they are objects for which a specific value is expected. The comments are meant to guide you as much as possible:
  - _Expected format:_ tells you how the values should be provided. Under the hood the strings provided here are interpreted by Lua and then converted into a C++ type; for instance here `mesh_label_list` will in fact be interpreted as a `std::vector` of integers.
  - _Constraint_ when existing tells you a condition the provided value must fulfill; if the provided value doesn't respect it an exception will be thrown very early in the runtime of the model.
  - Please notice that a comma act as a separator inside a _section_ - you will get a runtime error if you forget to put it. No comma though between sections.



By design, MoReFEM is **very** uptight about the content of this input file: 

- The author of the model must have defined exactly which _leaves_ are expected, and there will be a check that all are accounted for, no less, no more.
- - `Domain1`, `dimension_list`, `mesh_label_list` and `geometric_element_type_list` act as unique identifiers (with a subtlety) and must be unique: you can't get them multiple times in the input file. The subtlely is that a section is very akin to a namespace - you could define `dimension_list`, `mesh_label_list` and `geometric_element_type_list` in a `Domain2` - if such a `Domain` has been foreseen by the author of the model of course.
- There are no leaves inside the file that are not accounted for - you can't add a leaf `Domain3.dimension_list` if the author of the model didn't foresee it.

The benefits of this approach is that (provided the author of the model did his job correctly of course):

- The input files are really tailored for their models - you shouldn't see a `temperature` unknown in the `Elasticity` model in which it is not used for instance.
- There are no hidden values - mass volumic of the solid is relevant for `Elasticity` model and is modifiable so an explicit leaf `Solid.VolumicMass` will be present.
- If a value shouldn't be modified it shouldn't be present in the Lua file at all[^ticket_1796]. 

[^ticket_1796]: This is actually a very recent change, performed in issue #1796 in 2023; prior to that there were more leaves in the file and some of them were not meant to be modified. It wasn't satisfactory of course hence the new paradigm.

<div id="OutputDirectory"></div>

## Output directory

The library is conservative about output directories - the goal is to provide a safety to avoid overwriting data through too quick a command.

So by default when a model runs and a pre-existing result directory is found, the library will prompt the end-user to ask what it should do _for each rank_:

- Either delete the existing directory and keep running[^no_result_data_meddling].
- Or stop here the program and let the user decide what he wants to do (change the `ResultDirectory` leaf for instance, or renaming existing directories).

[^no_result_data_meddling]: So that a given output directory can't mix data generated through different runs.

If you want to simply overwrite in any case, you may provide `--overwrite_directory` flag on command line.

## Environment variables

The _demo_ files provided with the models embedded in the library are using environment variables, which are very useful in the test suite for instance.

However, if some environment variables are used you may have to provide their values explicitly as command line arguments[^env_for_test]:

[^env_for_test]: This is actually already done automatically in the test suite when running `ctest` command.

````
MoReFEM4Elasticity -i ${MOREFEM_ROOT}/Sources/ModelInstances/Elasticity/demo_2d.lua -e MOREFEM_ROOT=*your path to your MoReFEM sources*  -e MOREFEM_RESULT_DIR=*your result directory* 
````

If you generated your Lua file yourself you shouldn't have to bother with this, but if you get an exception yelling about an environment variable needing to be specified you now know the syntax: `-e FOO=BAR` to give `BAR` value to `FOO` environment variable.

There is an environment variable that shouldn't be defined this way: `MOREFEM_START_TIME`. This one may be used in the definition of the `Result` directory to ensure its uniqueness (and avoid the possible issues mentioned [here](#OutputDirectory))


<div id="UpdateExistingLuaFile"></div>

## Update an existing Lua file

A model develop may define a specific executable to enable quick update of a Lua file (the procedure to create one for a model developer is detailed [later](#HowToCreateUpdateLuaFileExe)).

Command is something like (executable name is up to the model author; I show here the convention used for models embedded in the library):

````
MoReFEM4ElasticityUpdateLuaFile -i my_input_data.lua 
````

This will regenerate the Lua file with the comments up-to-date (if for instance in the library a comment has been refined it will put the new version).

It should be noted it really is beta executable at best - it is intended to update comments and is not meant to deal with major changes in the fields that should be covered in the Lua file.

In 2023 we added the possibility to provide a much less verbosy version with the flag `--skip-comments`

````
MoReFEM4ElasticityUpdateLuaFile -i my_input_data.lua  --skip-comments
````

In this case, most of the comments are removed; only those that are truly required by the end user are kept. For instance the `Domain1` section shown above becomes something like:

````
-- Highest dimension geometric elements
-- mesh_index: { 1 }
Domain1 = {

	dimension_list = { 2 },
	mesh_label_list = {  },
	geometric_element_type_list = {  }

} -- Domain1
````

The description of what the section entails (here `Highest dimension geometric elements`) is kept, along with the indication the domain covers `Mesh1`, but the detailed comments for each leaf are gone.



# As a model developer

<div id="InputDataAndModelSettings"></div>

## `InputData` and `ModelSettings`

### Principle

First of all, please keep in mind both objects introduced below are intended to store **read-only** values; if the values are to be modified in the run of your model you aren't considering the right kind of objects (or you may - only if the data here is the starting value to consider at initialization).

A very important modification introduced in [#1796](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1796) (in 2023) was to clearly separate:

- Fields that are deemed to be modifiable directly by the end user (e.g. path to the mesh file)
- Fields that are fixed once and for all by the model developer and shouldn't be modified by the end user (typically the _NumberingSubset_ for almost all the models).

Prior to [#1796](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1796) it was not really possible: when a [section](#Section) block was opened all its leaves were to be defined there.

Now the second type of input data may be handled by a new kind of object named `ModelSettings`, which is able to fill values in a compiled file outside of the reach of an end user. A same section may be defined partly in `InputData` and partly in `ModelSettings`; it is your job as the model developer to choose in which object each of the model input belongs to.

As it is a late addition to the library, `ModelSettings` borrows as much as possible the same API as `InputData` to avoid spending too much time refactoring the code (a benefit is also to facilitate the transition if a leaf is reconfigured from `ModelSettings` to `InputData` or from `InputData` to `ModelSettings`).




<div id="ConcreteExampleFEltSpaceSection"></div>

### Concrete example: a `FEltSpace` section

When you define in a model a finite element space, you need to provide:

- A unique identifier to distinguish it from possible other finite element spaces (and to make possible to call for it within the code). This will be explained in [section](#IndexedSection).
- A description of said finite element space.
- The `GodOfDof` in which the finite element space is defined.
- The `Unknown`s that are considered
- The `Domain` upon which the finite element space is defined.
- The shape functions to use for each of the unknowns.

Conversely, almost all of these data are tightly related to the model itself, and therefore up to the author of the model. The only one that is really configurable is the very last one.

Concretely:

- The unique identifier is used to tag the section directly (more on this [below](#IndexedSection))
- Almost all of the data should be provided to the dedicated `ModelSettings` for your `Model`
- The shape function fields should be provided to `InputData`.

The way to define both is to indicate the fields to use in the dedicated tuples (I invite you here to have a look at one of the `InputData.hpp` file of one of the example models in `Sources/ModelInstances` directory); it will be something like:

````
using model_settings_tuple = std::tuple
<
	MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(enum_class_id)>::IndexedSectionDescription,
    MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(enum_class_id)>::GodOfDofIndex,                           
    MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(enum_class_id)>::UnknownList,                             
    MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(enum_class_id)>::DomainIndex,                             
    MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(enum_class_id)>::NumberingSubsetList

	... (other sections and leaves in InputData)

>;
````

and

````
using input_data_tuple = std::tuple
<
	MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(enum_class_id)>::ShapeFunctionList

	... (other sections and leaves in ModelSettings)
>;
````

These tuples are provided more or less indirectly (see below) to the `Model`; by doing so we declare the leaves are expected and **must** be provided (through the Lua file for `InputData` and a compiled file for `ModelSettings`).

Few clarifications on the syntax above:

- The convention is to use `enum class` to tag all of the instances of a given object (here a finite element space). So `enum_class_id` is the value in the enum class that identifies finite element space at end (e.g. `volume` in `enum class FEltSpaceIndex { volume = 1 };` and `EnumUnderlyingType(enum_class_id)` just returns the underlying numeric value (1 here).
- `IndexedSectionDescription` is a special field for indexed sections (see [below](#IndexedSection)); its content will be a description of the object at hand.

As for most finite element spaces the separation of fields will probably be the same as the one above, some macros have been defined to lighten the declarations of the tuples. Instead of what is above you could also use:


````
using input_data_tuple = std::tuple
<
	MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE(enum_class_id)>,

	... (other sections and leaves in InputData)

>;
````

and

````
using model_settings_tuple = std::tuple
<
	MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_FELT_SPACE(enum_class_id)>,

	... (other sections and leaves in ModelSettings)
>;
````

<div id="IndexedSection"></div>

### Indexed sections

In [section about content of the `InputData` file](#ContentInputDataFile), the difference between sections and leaves was explained.

However there is for sections an additional subtlety concerning sections:

- Some sections are if present only present once. For instance sections about how to handle parallelism (`InputDataNS::Parallelism`) or where to write the outputs (`InputDataNS::Result`) are present at most once.
- Some sections may be present multiple times, each time to represent a different instance. For instance we may consider several different meshes in a same model; in this case there is in the input data file one section per mesh considered. 

The second type of section is dubbed `IndexedSection`, as concretely the differentiation occurs with an integer template parameter.

So you may define in a tuple a `Mesh<1>` and a `Mesh<15>` for instance, but not two meshes with exactly the same index.

Integer however are rather inconvenient for code expressivity: it means that at all time you should have handily the matching table to understand exactly which index refers to which object. This is circumvented in all existing models by the use of C++ `enum class`; the macros such as `MOST_USUAL_INPUT_DATA_FIELDS_FOR_FELT_SPACE` assume you are using such an `enum class`.

Concretely when you define an indexed section you just have to define a field named `IndexedSectionDescription` for the specific section, e.g. `MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(enum_class_id)>::IndexedSectionDescription`. The associated value is a string that describes for the end user what the section represents.

If you forget to do this step, the library won't identify the leaves related to the section and you will get a runtime error.

As shown above, this field is typically covered implicitly in the `MOST_USUAL_MODEL_SETTINGS...` macros.

### Setting up `InputData` and `ModelSettings` objects

It is the [`MoReFEMData` object](../Core/MoReFEMData.md) that is in charge of initializing properly and storing both `InputData` and `ModelSettings` used throughout a given model (or test); the exact types to use for each of them is given through template parameters.

So far, we have defined two tuples `input_data_tuple` and `model_settings_tuple` which respectively describe the sections and leaves for `InputData` and `ModelSettings`.

#### InputData

It is the easiest to feed to the `MoReFEMData` base template class: you just need to provide as first template argument 
for `MoReFEMData` the type:

````
using input_data_type = MoReFEM::InputData<input_data_tuple>;
````

#### ModelSettings

`ModelSettings` is slightly more complicated: it is not an alias you provide but an inherited class from `MoReFEM::ModelSettings`.

The reason for this departure is that you have to define yourself a virtual method `Init()` (see below).

````
struct MyModelSettings : public MoReFEM::ModelSettings<model_settings_tuple>
{
    void Init() override;
};
````

This `MyModelSettings` is to be given as second template argument to `MoReFEMData`. In practice in real code I do not 
use this `My` prefix and rely instead upon namespaces.

The definition of this `Init()` method is typically performed in a `ModelSettings.cpp` file; its point is to provide the values to use similarly to the role of the Lua file for `InputData`. As for `InputData`, MoReFEM is extremely touchy here and exactly all the leaves present in the `model_settings_tuple` tuple must be defined.


##### `Add` method

The syntax to provide a definition in the `Init()` method is:

````
Add<**tuple item to set**>(**value to set**);
````

The `value to set` type depends of the way the tuple item has been defined; it is provided to C++ syntax (and departs from `InputData` on this: `InputData` expects Lua syntax that is typically specified explicitly in the comment of the field considered).

For instance for `MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume)>::GodOfDofIndex` in the library, if you look at `GodOfDofIndex` declaration you see:

````
struct GodOfDofIndex : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<GodOfDofIndex, self, std::size_t>,
                               public ::MoReFEM::Internal::InputDataNS::FEltSpaceNS::GodOfDofIndex
{ };
````

The relevant information is the last template argument of `::MoReFEM::Advanced::InputDataNS::Crtp::Leaf`: the value to set expected here is a `std::size_t` (i.e. an unsigned integer).

##### Syntactic sugar for `IndexedSectionDescription`

The `Add` syntax above is all you need to define every members of `model_settings_tuple`.

However, syntactic sugar has been introduced to hide `IndexedSectionDescription` leaves, which are not immediately apparent if the  `MOST_USUAL_MODEL_SETTINGS...` macros are used.

So instead of using:

````
Add<MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume)>::IndexedSectionDescription>>(
                "Finite element space which covers the whole volume of the mesh.");
````

you may use:

````
SetDescription<MoReFEM::InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::volume)>(
                "Finite element space which covers the whole volume of the mesh.");
````

It is strictly syntactic sugar so you may not use it; however don't feel surprised if an exception tells you a `SetDescription` field is missing when you forgot to provide the value.

<div id="ExtractValues"></div>

### Extracting values from `InputData` or `ModelSettings`

As already stated, `ModelSettings` was written so that most of existing `InputData` API may be used directly.

The functions (or assimilate) to use to do so may be found in `#include "Utilities/InputData/Extract.hpp`; the most useful ones by far are the static methods defined in `InputDataNS::ExtractLeaf` struct.

For instance:

````
decltype(auto) prepartitioned_data_dir = InputDataNS::ExtractLeaf<InputDataNS::Parallelism::Directory>::Path(morefem_data.GetInputData());
````

or 

````
decltype(auto) prepartitioned_data_dir = InputDataNS::ExtractLeaf<InputDataNS::Parallelism::Directory>::Path(model_settings);
````

However you probably won't need it (unless you defined your own entries, if you're an [advanced model developer](CreatingNewEntries)): most of the sections are used internally by the library. 

You may have to use some higher level function which themselves are using them, such as `InitScalarParameterFromInputData` which will need data provided in `InputData`.


<div id="HowToCreateUpdateLuaFileExe"></div>

## Update an existing Lua file

In the [end user section](UpdateExistingLuaFile), we spoke of the beta-quality executable used to update an existing Lua file.

Setting it up for your own model is rather trivial; as usual I invite you to have a look at existing models supplied in `ModelInstances` directory but the `main` of such executable just calls a dedicated template function which argument is your model:

````
int main(int argc, char** argv)
{
    return ModelNS::MainUpdateLuaFile<**your model**>(argc, argv);
}
````


# As an advanced model developer

<div id="CreatingNewEntries"></div>

## Creating new input data or model settings entries

The leaves and sections shipped with the core library are in `Core/InputData/Instances`.

However, for your own model you may need to provide supplementary ones.

I won't write here a detailed guide how to do so - by looking to existing ones you should get it quickly - and will just underline few hints:

- First take as example an existing element which share the same nature (leaf, section or indexed section) as the one you want to implement. This will decide among other things from which CRTP parent class you inherit from.
- In sections (indexed or otherwise), the field `section_content_type` is very important: it describes all the leaves and sections that will be derived when you just write the name of the section in the tuple considered. You may not write all of the leaves inside, for instance if one of the leave is seldom used and you would rather not consider it except if model developer explicitly put it.
- Don't skip lines such as `static_assert(std::is_convertible<self*, parent*>());`. They are very precious to prevent typos, copy/paste errors, etc...
- Don't neglect the `Description()` content - it will generate explanations for the model user how to use your fields!
- Be wary of the enclosing sections fields: you will get nasty compilation errors of you refer to non existing types.


# As library developer

### `InputData` and tests

From what is [above](InputDataAndModelSettings), tests clearly should rely exclusively - save for few related to `InputData` implementation itself - upon `ModelSettings`. From now on, you should strive to write new tests with no input lua file, probably using the `MoReFEM::TestNS::FixtureNS::ModelNoInputData` fixture.

This was done for most recent tests such as those related to hyperelastic laws (TestCiarleyGeymonat for instance). However it would be cumbersome and time consuming to migrate all older tests to this new paradigm; that's why most of the tests feature an `InputData` even if the Lua file is not to be modified at all (or it would break the test).

### Extracting values from either `InputData` or `ModelSettings`

I presented [earlier](#ExtractValues) the functions to extract the values stored in the tuples pertaining to `InputData` or `ModelSettings`; but for a model writer standpoint you know whether the data you seek is positioned into one or the other.

That is not always the case when you're writing code in the library: you may have to deal with fields that the model developers may position in either one.

In this case, you should consider `InputDataNS::ExtractLeafFromInputDataOrModelSettings`, which:

- Check the leaf is defined only in one of them (compile-time error if that is not the case).
- Returns the associated value.