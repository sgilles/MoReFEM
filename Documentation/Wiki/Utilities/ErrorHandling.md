[TOC]

# As a model user

Runtime errors (for instance if a value you provided in the model Lua file) are expected to be handled through an exception: the program should stop exactly and tells you in a clear message what went wrong.

If that is not the case, please [fill an issue](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/new) or contact us if you do not have the rights to create the issue directly, with the exact steps that resulted in the unwanted program exit.

# As a model developer

First of all, you should have a look at [this notebook](https://gitlab.inria.fr/formations/cpp/gettingstartedwithmoderncpp/-/blob/master/5-UsefulConceptsAndSTL/1-ErrorHandling.ipynb) from a C++ training session some of us created; it will give you the hints of the different levels and methods of error handling.

Very basically in MoReFEM:

- Exceptions are the way to go to handle runtime errors that may happen when a user just use a model by setting up a Lua file and making it run. They are triggered both in debug and release mode, and the program should exit gracefully. It should be noted that PETSc libraries operate checks through error codes; all those error codes are wrapped in dedicated functions that use exceptions instead. You should not use PETSc API directly.
- `assert` are used extensively to identify logical errors; they should highlight errors made by the library developer or by you, the model developer, while using content provided in the library. They should not depend on the data provided in the input file. Typically, an assert may be used to check a pointer is not null, that a vector provided to a function gets the proper size, that an object has been correctly initialized before being used, and so forth... They should be activated only in debug mode - there are extensive and costly sanity checks that we want to perform only when writing a model, not in production mode.
- Whenever possible, there are also checks that occur at compile time - and even more so with novelties introduced in C++17 and C++20 such as `if constexpr`, `concept` and `require` clauses. For instance if you're trying to extract value from an input data that was not provided, the program won't compile at all.

## Assertions

Sometimes, an `assert` may be fairly limited:

- The only information provided is the line in which the assert is written; you might want more intel about the calling site.
- assert that occur in specific part of the code (related to PETSc architecture) will be handled poorly and generate lots of gibberish output (see next section).

In these cases, [#1867](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1867) introduced a tailored class named `Assertion`.

Basically:

- `Assertion` (or its child class) is expected to be thrown as an exception.
- As we do not want the usual `exception` mechanism to be activated - the assertion should result in the stopping of the program and therefore should not be caught anywhere outside of the `main` function -, it does not inherit from `std::exception`.
- However the class has been defined to be as close as possible for expected API for an exception, with a `what()` method (defined with a slightly different prototype).
- `Assertion` takes (as `MoReFEM::Exception` for that matter) a `std::source_location` aimed at identifying the calling site.
- `Assertion` is declared defined only in debug mode.

You may define syntactic sugar for it:

- A call is a bit of a mouthful

````c++
#ifndef NDEBUG
if (petsc_internal == MOREFEM_PETSC_NULL)
    throw MoReFEM::Wrappers::Petsc::AssertionNS::ShouldHaveBeenInitialized(source_location);
#endif // NDEBUG
````

(with `ShouldHaveBeenInitialized` a child class of `Assertion`, defined as such only in debug mode).


- A free function may be defined to reduce this as a one liner. In this case, define it both when NDEBUG is defined (in which case the function does absolutely nothing) or when it isn't (the bulk of the function is in this case the test above and the throwing of the assertion) - exactly as `assert` in the STL.

Doing so ease the call:

````c++
 InternalNotNull(PetscInternalT petsc_internal, source_location);
````

### Special case in the `VariationalFormulation` implementations

In two specific methods expected in a `VariationalFormulation`:

````c++
void ComputeTangent(const GlobalVector& evaluation_state, GlobalMatrix& tangent, GlobalMatrix& preconditioner);
void ComputeResidual(const GlobalVector& evaluation_state, GlobalVector& residual);
````
  
you should really use the `Assertion` instead of `assert` if you want the output not to be gibberish. The gory details for that are in [this section aimed at library developers](#SolveNonLinearConundrum).


# As a library developer

<div id="SolveNonLinearConundrum"></div>

## The `SolveNonLinear` conundrum

As mentioned in the previous section, `SNESSolve()` (the PETSc functionality used to solve a non linear system) gets a very special architecture which may derail entirely logging when something awry happens.

The principle of `SNESSolve` is to call the solver under the hood; developer is expected to provide pointers to the functions used to compute the function and the jacobian through the functions:

````c++
PetscErrorCode SNESSetFunction(SNES snes, Vec r, PetscErrorCode (*f)(SNES, Vec, Vec, void *), void *ctx);
PetscErrorCode SNESSetJacobian(SNES snes, Mat Amat, Mat Pmat, PetscErrorCode (*J)(SNES, Vec, Mat, Mat, void *), void *ctx)
````

where `ctx` is whatever the developer wants to provide as additional data (in MoReFEM the `VariationalFormulation` is provided here).

The gymnastics of it is hidden under the hood in MoReFEM; what is required of the author of a model is to provide in `VariationalFormulation` two methods which signatures are:

````c++
void ComputeTangent(const GlobalVector& evaluation_state, GlobalMatrix& tangent, GlobalMatrix& preconditioner);
void ComputeResidual(const GlobalVector& evaluation_state, GlobalVector& residual);
````

The issue is that assert or even exceptions are not well handled and will produce lots of gibberish on the output screen.

To avoid this, in methods defined in `MoReFEM::Internal::SolverNS::SnesInterface` there are try/catch blocks to ensure all the exceptions and MoReFEM assertions are properly caught and replaced by an error code.

I mentioned this as if at some point a new kind of exceptions is added (as `Assertion` were in [#1867](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues/1867)) the catch block[^catch_conundrum] needs to be updated to handle properly errors. `assert` should be avoided in those two functions for the same reason, as mentioned above; please use `Assertion` instead.

[^catch_conundrum]: This is a rare occurrence where I actually used a macro in compliance to DRY principle; look for `SnesMacro.hpp` file for the exact implementation of the `catch` blocks.