[TOC]

# Introduction

This file is a work in progress: there was many layers of documentation in MoReFEM, but we aim to provide a unique one which is clearly extensive. It will of course be done step by step; when new features such as restart or heavy refactoring of the handling of input data is done, a wiki note is written to start a virtuous circle. The purpose of current file is just to provide some agency to the various notes hence written, to avoid the layer effect we got with our previous attempts.

In each of this note, we address the different use cases of MoReFEM:

- Model user - you're just running a model someone else wrote and just interact with the input data file
- Model developer - you're writing your own MoReFEM model, but using only on the shelf tools (operators, finite elements, etc...)
- Advanced model developer - you're writing your own MoReFEM model, with some customized elements (for instance you need to write yourself your own variational operator). Materialized throught the `Advanced` namespace in the code.
- Library developer - you're working in the internals of the library. Materialized throught the `Internal` namespace in the code.


# Installation

- [How to install main compilers used with the libraries in your environment](InstallingCompilers.md)

# General presentation of the library

- [Overall structure of the code](CodeStructure.md)

# Utilities 

- [How to handle errors in MoReFEM?](Utilities/ErrorHandling.md)
- [How are handled input data that are specific to a given Model (or test)?](Utilities/InputData.md)

# Core

- [MoReFEMData facility](Core/MoReFEMData.md)


# Model

- [What is the restart mode and how is it working](Restart.md)



# Tooling

- [Continuous integration](ContinuousIntegration/README.md)






