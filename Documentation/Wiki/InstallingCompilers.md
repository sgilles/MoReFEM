[TOC]

# Installing compilers on macOS

## Warning

Apple made the dubious choice when they switched from GNU compilers to AppleClang ones to provide symbolic links that point toward the new compiler. So on a standard installation of macOS if you type in a terminal `gcc` or `g++` the odds are you're in fact using AppleClang... Typing `gcc -v` will tell you which version you are using!

For most users Apple Clang is fine; getting g++ or LLVM-clang++ is useful for the integration manager to be able to check locally that those compilers are properly supported (and even then CI provides most of those needs - it is only when something is awry than it is practical to be able to check locally).

## AppleClang

- Apple clang is shipped with XCode; just install XCode from the AppleStore and open it; additional components will be installed at this moment (including development tools such as git command line).

- gfortran is typically installed through [Homebrew](https://brew.sh/). At the time of this writing this means you need to type:

````
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
````

to install Homebrew and then:

````
brew install gfortran
````

to install gfortran.

For a long time gfortran was installed from [this website](hpc.sourceforge.net/) but in the recent years the archives there weren't without issues; using Homebrew is safer.

## gcc or clang

[Homebrew](https://brew.sh/) is your friend here if you want a version of gcc or LLVM-clang on your Mac:

````
brew install gcc  
````

You may have to change write permissions to install it properly (follow the Homebrew instructions in your Terminal to do so).

Please notice that `gcc` or `g++` will keep pointing to the default ones set up by Apple (see the warning above); you need either to update your path or to explicitly specify the compiler to use, e.g. `/usr/local/Cellar/gcc/11.3.0/bin/g++-11` for me at the time of this writing.


Same for clang:

````
brew install llvm
````

which installs a version to look at in `/usr/local/Cellar` (e.g. `/usr/local/Cellar/llvm/13.0.1_1/bin/clang++`).



# Installing GNU compiler on Ubuntu

## Compilers and their standard library

It is better to use along with a compiler the companion standard library, which is:

- `libstdc++` for `g++`
- `libc++` for `clang++`

On Ubuntu it is rather painful to install properly and use `libc++` (or at least it was last time I tried...), so the choice was made to only provide guidance for `g++` compiler (continuous integration for instance in Gitlab-CI does not cover the Ubuntu/clang configuration for that reason).

## gcc

Ubuntu distribution is rather conservative in its compiler version and your Ubuntu is probably shipped with a not very recent gcc.

To install a more recent one (gcc 11 at the time of this writing), type [^1]:

````
sudo apt-get install --no-install-recommends -y software-properties-common
sudo add-apt-repository ppa:ubuntu-toolchain-r/test && sudo apt-get update \
     && sudo apt-get install -y gcc-11 g++-11 gfortran-11     
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-11 100 --slave /usr/bin/g++ g++ /usr/bin/g++-11 --slave /usr/bin/gfortran gfortran /usr/bin/gfortran-11
````

and check with

````
which g++
````

the correct version is returned.

Running again

````
sudo update-alternatives --config gcc
````

allows you to switch back to another version of gcc if need be.

# Installing compilers on Fedora

Contrary to Ubuntu, Fedora is shipped with fairly recent versions of compilers; check it but on recent Fedora gcc is already a recent one.

## gcc 

So the install `gcc` (and `gfortran`)[^1]:

````
dnf --setopt=install_weak_deps=False --best install -y gcc gcc-c++ gcc-gfortran
````

## clang

For `clang` (and `gfortran`)[^1]:

````
dnf --setopt=install_weak_deps=False --best install -y clang libcxx libunwind libcxx-devel gcc-gfortran
````


[^1]: You may have a look at the [companion project](https://gitlab.inria.fr/MoReFEM/ThirdPartyCompilationFactory) Dockerfiles to get the hint of how the compilers were installed for the Docker images used in continuous integration (keep in mind though there are lot of explicit dependencies as a Docker image is bare-bone - if you're using a desktop Linux OS some of the stuff will already be installed).