%
%  untitled
%
%  Created by Sebastien Gilles on 2013-05-16.
%  Copyright (c) 2013 __MyCompanyName__. All rights reserved.
%
\documentclass[]{article}

% Use utf-8 encoding for foreign characters
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}

% Setup for fullpage use
\usepackage{fullpage}

% Uncomment some of the following if you use the features
%
% Running Headers and footers
%\usepackage{fancyhdr}

% Multipart figures
%\usepackage{subfigure}

% More symbols
\usepackage{amsmath}
\usepackage{amssymb}
%\usepackage{latexsym}
\usepackage{amsfonts}

% Surround parts of graphics with box
\usepackage{boxedminipage}



% Package for biblio
\usepackage[authoryear,round,comma]{natbib}



% ========================================================================
% Code
% ========================================================================
% Package for including code in the document
\usepackage{color}
\usepackage{../new_commands}



 \usepackage{listings}
  \usepackage{courier}
 \lstset{
		language = C++, 
		deletestring=[b]{'},
		morekeywords={macro,func,solve,problem,fespace,dx,dy,dz,int3d,int2d,'},
		morecomment=[l][\color{mygreen}]{//},
		morecomment=[l][\color{myblue}\bfseries]{//EOM},
        basicstyle=\footnotesize\ttfamily,
        %numbers=left,             
        numberstyle=\tiny,         
        %stepnumber=2,             
        numbersep=5pt,             
        tabsize=2,                 
        extendedchars=true,        
        breaklines=true,           
        keywordstyle=\color{myred}\bfseries,
   		%frame=b,         
        stringstyle=\color{myblue}\bfseries, 
        showspaces=false,          
        showtabs=false,            
        xleftmargin=17pt,          
        framexleftmargin=17pt,
        framexrightmargin=5pt,
        framexbottommargin=4pt,
        %backgroundcolor=\color{lightgray},
        showstringspaces=false,     
		frame=shadowbox,
		rulesepcolor=\color{lightgray}
 }
 %\lstloadlanguages{C++}


% If you want to generate a toc for each chapter (use with book)
\usepackage{minitoc}

% This is now the recommended way for checking for PDFLaTeX:
\usepackage{ifpdf}

%\newif\ifpdf
%\ifx\pdfoutput\undefined
%\pdffalse % we are not running PDFLaTeX
%\else
%\pdfoutput=1 % we are running PDFLaTeX
%\pdftrue
%\fi

\ifpdf
\usepackage[pdftex]{graphicx}
\else
\usepackage{graphicx}
\fi



\title{Current structure of FiniteElementSpace}
\author{Sébastien Gilles}

%\date{2013-05-23}

\begin{document}
	
\newcommand{\refchapter}[1]{chapter \ref{#1}}
\newcommand{\refsection}[1]{section \ref{#1}}
\newcommand{\refcode}[1]{code excerpt \ref{#1}}
\renewcommand{\lstlistingname}{Code excerpt}
\renewcommand{\lstlistlistingname}{Table of code excerpts}


\newcommand{\subsubsubsection}[1]
{
\bigskip
\textbf{#1}
\bigskip
}

\ifpdf
\DeclareGraphicsExtensions{.pdf, .jpg, .tif}
\else
\DeclareGraphicsExtensions{.eps, .jpg}
\fi

\maketitle


%\tableofcontents
%\newpage
%\lstlistoflistings
%\newpage
\section*{Introduction}

The goal here is to present briefly the current state of \lstinline{FiniteElementSpace}; I won't delve into details about nodes and dofs creation as they were addressed by a specific note few months ago and the current state is still very similar.
\medskip

It must be kept in mind that current finite element space work on fairly easy monolithic problems in which one finite element space and one mesh are involved; some generalization is probably required at some places.


\section{FiniteElementSpace}

A \lstinline{FiniteElementSpace} contains:

\begin{itemize}
    \item A list of unknowns and the associated shape function label. For instance (velocity, P2) and (pressure, P1) for Stokes problem.
    \item The lists of processor-wise and ghost nodes and dofs.
    \item The list of boundary conditions to apply.
    \item The list of finite elements involved shelved by \lstinline{FiniteElementType}.
\end{itemize}

Currently the whole mesh is used to create the list of finite elements, but there is no difficulty here to apply a \lstinline{Domain} in \lstinline{FiniteElementSpace} construction. 
\medskip

I see no strong reason so far from a computing point-of-view to restrict the \lstinline{FiniteElementSpace} to one \lstinline{GeometricMeshRegion}.
\medskip

There is exactly one \lstinline{FiniteElement} for each \lstinline{GeometricElement} present in the chosen domain (see \refsection{SectionElementaryData} for more about it).

\section{FiniteElement}

A \lstinline{FiniteElement} contains:

\begin{itemize}
    \item A pointer to the underlying \lstinline{GeometricElement}.
    \item The list of all its nodes.
    \item Local-2-global where global is in the program-wise numbering.
    \item Local-2-global where global is in the processor-wise numbering for the FiniteElement that require it. This choice is made during the construction; they may be required for instance if results from previous iteration are involved in the calculation.
\end{itemize}

A \lstinline{FiniteElement} object can't really do much by itself; it should be used along with its \lstinline{FiniteElementType}.

\section{FiniteElementType}\label{SectionFiniteElementType}

A \lstinline{FiniteElementType} contains:

\begin{itemize}
    \item A pointer to the underlying \lstinline{GeomRefElement}.
    \item The reference element for each of the unknowns of the \lstinline{FiniteElementSpace}.
    \item A reference to the list of unknowns, which is the same as the one in \lstinline{FiniteElementSpace} (more on that in \refsection{SectionElementaryData}).
    \item The quadrature rule in use.    
\end{itemize}


\section{LocalVariationalOperator/ElementaryData}\label{SectionElementaryData}

Each \lstinline{LocalVariationalOperator} gets in its constructor among other parameters the list of unknowns concerned; the elementary matrices and vectors required for the calculation are stored in a private class named \lstinline{ElementaryData}. This list of unknowns must be a subset of the ones defined in the \lstinline{FiniteElementSpace}.
\medskip

So for instance in the static Stokes problem:

\begin{itemize}
    \item Finite element space defines two unknowns with a shape function label for each of them: velocity/P2 and pressure/P1.
    \item Operator "psi\_div" acts upon both of them.
    \item Operator "grad\_grad" acts only upon velocity.    
\end{itemize}

In the instantiation:
\begin{itemize}
    \item The same \lstinline{FiniteElementSpace} is used for both.
    \item The same list of \lstinline{FiniteElementType} is used for both.
    \item \lstinline{ElementaryData} on the contrary differ: the elementary matrices stored for "psi\_div" are bigger than the ones defined for "grad\_grad".
    
\end{itemize}


\end{document}