include(${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake)

get_property(current_module_files TARGET "${MOREFEM_UTILITIES}" PROPERTY SOURCES)

list(FILTER current_module_files INCLUDE REGEX ^${CMAKE_CURRENT_SOURCE_DIR}/ThirdParty)

source_group(TREE "${CMAKE_CURRENT_SOURCE_DIR}" FILES ${current_module_files})