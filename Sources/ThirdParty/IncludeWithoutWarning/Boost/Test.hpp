// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_INCLUDEWITHOUTWARNING_BOOST_TEST_DOT_HPP_
#define MOREFEM_THIRDPARTY_INCLUDEWITHOUTWARNING_BOOST_TEST_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Warnings/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wsign-conversion")
PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
PRAGMA_DIAGNOSTIC(ignored "-Wparentheses")
PRAGMA_DIAGNOSTIC(ignored "-Wconversion")
PRAGMA_DIAGNOSTIC(ignored "-Wswitch-enum")

#include "Utilities/Warnings/Internal/IgnoreWarning/array-bounds.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/covered-switch-default.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/deprecated-builtins.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/deprecated-declarations.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/deprecated-dynamic-exception-spec.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/deprecated.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/extra-semi-stmt.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/float-equal.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/missing-prototypes.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/redundant-parens.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/reserved-id-macro.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/reserved-identifier.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/suggest-destructor-override.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/suggest-override.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/undef.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/used-but-marked-unused.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/weak-vtables.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/zero-as-null-pointer-constant.hpp"

#include "boost/test/unit_test.hpp"

PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_INCLUDEWITHOUTWARNING_BOOST_TEST_DOT_HPP_
// *** MoReFEM end header guards *** < //
