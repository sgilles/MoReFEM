// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_INCLUDEWITHOUTWARNING_PETSC_PETSCVERSION_DOT_HPP_
#define MOREFEM_THIRDPARTY_INCLUDEWITHOUTWARNING_PETSC_PETSCVERSION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Warnings/Pragma.hpp"

PRAGMA_DIAGNOSTIC(push)
PRAGMA_DIAGNOSTIC(ignored "-Wsign-conversion")
PRAGMA_DIAGNOSTIC(ignored "-Wcast-qual")
PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")

#include "Utilities/Warnings/Internal/IgnoreWarning/comma.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/float-equal.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/redundant-decls.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/reserved-id-macro.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/reserved-identifier.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/undef.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/zero-as-null-pointer-constant.hpp"

#include "petscmacros.h"
#include "petscversion.h"

PRAGMA_DIAGNOSTIC(pop)

/*!
 * \brief Alias in MoReFEM for the macro that represents `PETSC_NULL`
 *
 * PETSc changed its convention in v3.19 and for the time being I want to be able to cover both older and most
 * recent version seamlessly.
 * At some point it should be dropped and all instances of MOREFEM_PETSC_NULL in the code replaced by
 * `PETSC_NULLPTR`
 */
#if PETSC_VERSION_LT(3, 19, 0)
#define MOREFEM_PETSC_NULL PETSC_NULL
#else
#define MOREFEM_PETSC_NULL PETSC_NULLPTR
#endif

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_INCLUDEWITHOUTWARNING_PETSC_PETSCVERSION_DOT_HPP_
// *** MoReFEM end header guards *** < //
