// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_XTENSOR_FUNCTIONS_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_XTENSOR_FUNCTIONS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export
#include "Utilities/LinearAlgebra/LocalAlias.hpp"
#include "Utilities/Numeric/Numeric.hpp"


namespace MoReFEM::Wrappers::Xtensor
{


    /*!
     * \brief Computes the cross product of two 3d vectors.
     *
     * \internal <b><tt>[internal]</tt></b> The types of the vectors and matrix below are very constrained
     * on purpose; I need this calculation only for a very specific case (calculation of small matrices for
     * local finite elements).
     * \endinternal
     *
     * \tparam T1 Type of the content of the vector and matrix. Typically 'double'.
     * \tparam Allocator1 Allocator of the first vector.
     * \tparam Allocator2 Allocator of the second vector.
     * *
     * \todo #1491 Probably already existing in Xtensor-blas
     *
     * \param[in] X First vector. Must be 3d.
     * \param[in] Y Second vector. Must be 3d.
     * \param[out] out Resulting vector.
     */
    void CrossProduct(const LocalVector& X, const LocalVector& Y, LocalVector& out);


    /*!
     * \brief Check whether a matrix is filled with zero or not.
     *
     * \param[in] matrix Matrix under scrutiny.
     * \param[in] epsilon Epsilon used for the comparison. A default value is provided; but the parameter
     * is there if you want to play with it.
     *
     * \todo #1491 Could probably be done more efficiently (by going through the unidimensional container
     * directly)
     */
    bool IsZeroMatrix(const LocalMatrix& matrix, double epsilon = NumericNS::DefaultEpsilon<double>());


    /*!
     * \brief Check whether a vector is filled with zero or not.
     *
     * \param[in] vector Vector under scrutiny.
     * \param[in] epsilon Epsilon used for the comparison. A default value is provided; but the parameter
     * is there if you want to play with it.
     *
     * \todo #1491 Could probably be done more efficiently (by going through the unidimensional container
     * directly)
     */
    bool IsZeroVector(const LocalVector& vector, double epsilon = NumericNS::DefaultEpsilon<double>());


    /*!
     * \brief Check whether there is a nan or inf value in a \a LocalMatrix.
     *
     * \param[in] matrix Matrix being scrutinized.
     *
     * \return True if at least one value if NaN or inf.
     */
    bool AreNanOrInfValues(const LocalMatrix& matrix);


    /*!
     * \brief Check whether there is a nan or inf value in a \a LocalVector.
     *
     * \param[in] vector Vector being scrutinized.
     *
     * \return True if at least one value if NaN or inf.
     */
    bool AreNanOrInfValues(const LocalVector& vector);


    /*!
     * \brief Compute the inverse matrix of \a local_matrix (if existing; if not an exception is throw).
     *
     * Works only for dimensions 1 to 3.
     * \param[out] inverse Inverse of the matrix.
     * \param[in] local_matrix Matrix for which we want to compute the determinant.
     * \param[out] determinant Determinant of \a local_matrix.
     */
    void ComputeInverseSquareMatrix(const LocalMatrix& local_matrix, LocalMatrix& inverse, double& determinant);


} // namespace MoReFEM::Wrappers::Xtensor


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_XTENSOR_FUNCTIONS_DOT_HPP_
// *** MoReFEM end header guards *** < //
