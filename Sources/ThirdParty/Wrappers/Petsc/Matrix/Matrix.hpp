// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_MATRIX_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_MATRIX_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <memory>  // IWYU pragma: keep
#include <source_location>
#include <utility>
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"        // IWYU pragma: export
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMatPrivate.hpp" // IWYU pragma: export
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVec.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscViewer.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"                // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Advanced/Enum.hpp"    // IWYU pragma: export
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp" // IWYU pragma: export // IWYU pragma: keep
#include "ThirdParty/Wrappers/Petsc/Matrix/Advanced/AbstractMatrix.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/FreeFunctions.hpp" // IWYU pragma: export // IWYU pragma: keep


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }
namespace MoReFEM::Wrappers::Petsc { class MatrixPattern; }
namespace MoReFEM::Wrappers::Petsc { class Vector; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Wrappers::Petsc
{


    /*!
     * \brief Enum which enable to ignore zero entries in SetValues() calls.
     *
     * It is advanced use; default value of no is used.
     */
    enum class ignore_zero_entries { yes, no };


    /*!
     * \brief A wrapper class over Petsc Mat objects.
     *
     * Most of the Petsc functions used over Petsc matrices have been included as methods in this class, which
     * also acts as RAII over Petsc Mat object.
     *
     * \internal <b><tt>[internal]</tt></b> This class is way more trickier to implement that it might seems
     * because of Petsc's internal structure: Mat objects are in fact pointers over an internal Petsc type.
     * So copy and destruction operations must be made with great care! That's why several choices have been
     * made:
     *
     * - No implicit conversion to the internal Mat. It seems alluring at first sight to allow it but can
     * lead very fastly to runtime problems (covered by asserts in debug mode).
     * - No copy semantic for this class.
     * - The most usual way to proceed is to construct with the default constructor and then init it either'
     * by a 'Init***()' method or by using 'DuplicateLayout', 'Copy' or 'CompleteCopy'methods.
     * \endinternal
     *
     */
    class Matrix : public Advanced::Wrappers::Petsc::AbstractMatrix
    {
      public:
        //! Alias to self.
        using self = Matrix;

        //! Alias to parent.
        using parent = Advanced::Wrappers::Petsc::AbstractMatrix;

        static_assert(std::is_convertible<self*, parent*>());

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! \copydoc doxygen_hide_petsc_matrix_concept_keyword
        static inline constexpr bool ConceptIsPetscMatrix = true;

        //! Convenient alias.
        using call_petsc_destroy = Advanced::Wrappers::Petsc::call_petsc_destroy;


      public:
        /*!
         * \brief Constructor.
         *
         * \copydetails doxygen_hide_petsc_matrix_name_arg
         *
         * \copydetails doxygen_hide_print_linalg_arg
         *
         * We are here at a fairly low level - for most linear algebra it is advised to use \a GlobalMatrix hence the
         * default value that choose not to print destruction of the object. The flag in command line concerns the
         * matrices defined with \a GlobalMatrix only - if for some reason you need specifically a \a
         * Wrappers::Petsc::Matrix you have to give it explicitly to the constructor (the value handled in the \a
         * CommandLineFlags singleton may of course be used but it's not baked in as for \a GlobalMatrix (the reason is
         * the hierarchy of modules: the singleton is defined in Core and here we are at a lower level than that).
         */
        explicit Matrix(std::string&& name,
                        Advanced::Wrappers::Petsc::print_linalg_destruction do_print_linalg_destruction =
                            Advanced::Wrappers::Petsc::print_linalg_destruction::no);

        //! Destructor.
        virtual ~Matrix() override;

        //! \copydoc doxygen_hide_move_constructor
        Matrix(Matrix&& rhs);

        /*!
         * \copydoc doxygen_hide_copy_constructor
         *
         * Both layout and data are copied.
         *
         * \copydetails doxygen_hide_petsc_matrix_name_arg
         *
         * \attention Standard mandates that additional parameters for copy constructor get default value.
         *
         * \attention This copy constructor assumes the \a rhs matrix has been properly initialized (through a call to one of the \a InitXXX() method.
         * If not an `Advanced::Assertion` is raised (in debug mode) or PETSc underlying function will return an error.
         */
        Matrix(const Matrix& rhs, std::string&& name = "Default value");

        //! \copydoc doxygen_hide_copy_affectation
        Matrix& operator=(const Matrix& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Matrix& operator=(Matrix&& rhs) = delete;


        /*!
         * \brief A method which provides a very bare initialization: only the type of the matrix is set.
         *
         * Most of the time, you should use another method such as \a InitSequentialMatrix() or \a
         * InitParallelDenseMatrix(); however when you just want to load a matrix from a file you may not know
         * the informations related to sizes. In this case, this method will provide the minimum set up required
         * to properly init your matrix. Beware though: if you're reloading a matrix in parallel (typically with
         * \a petsc_matrix_type = MATMPIAIJ) you get no guarantee whatsoever of the repartition that will be
         * done among the ranks. To control this, you should use instead another of the \a InitXXX() methods for
         * which you are able to set the dimensions yourself (you may have a look at TestPetscMatrixIO where
         * both versions are checked).
         *
         * \copydetails doxygen_hide_mpi_param
         * \copydoc doxygen_hide_source_location
         * \param[in] petsc_matrix_type The PETSc identifier for the requested matrix (see
         * https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatType.html#MatType)
         */
        void InitMinimalCase(const Mpi& mpi,
                             MatType petsc_matrix_type,
                             const std::source_location location = std::source_location::current());


        /*!
         * \brief Create a sequential sparse matrix.
         *
         *
         * \param[in] Nrow Number of rows.
         * \param[in] Ncolumn Number of columns.
         * \param[in] matrix_pattern Pattern of the matrix (number of elements expected on each row_).
         * \copydetails doxygen_hide_mpi_param
         *
         * \copydoc doxygen_hide_source_location
         *
         * The matrix hence created will keep the non-zero structure: even if a value becomes 0 it will
         * go on being held as a non-zero emplacement.
         */
        void InitSequentialMatrix(std::size_t Nrow,
                                  std::size_t Ncolumn,
                                  const MatrixPattern& matrix_pattern,
                                  const Mpi& mpi,
                                  const std::source_location location = std::source_location::current());


        /*!
         * \brief Create a parallel sparse matrix.
         *
         * \copydoc doxygen_hide_parallel_matrix_args
         *
         * \param[in] matrix_pattern Pattern of the matrix (number of elements expected on each row_).
         * \copydetails doxygen_hide_mpi_param
         *
         * \copydoc doxygen_hide_source_location
         *
         */
        void InitParallelMatrix(std::size_t Nlocal_row,
                                std::size_t Nlocal_column,
                                std::size_t Nglobal_row,
                                std::size_t Nglobal_column,
                                const MatrixPattern& matrix_pattern,
                                const Mpi& mpi,
                                const std::source_location location = std::source_location::current());


        /*!
         * \brief Create a sequential dense matrix.
         *
         * \param[in] Nrow Number of rows.
         * \param[in] Ncolumn Number of columns.
         * \copydetails doxygen_hide_mpi_param
         * \copydoc doxygen_hide_source_location
         */
        void InitSequentialDenseMatrix(std::size_t Nrow,
                                       std::size_t Ncolumn,
                                       const Mpi& mpi,
                                       const std::source_location location = std::source_location::current());


        /*!
         * \brief Create a parallel dense matrix.
         *
         * \copydoc doxygen_hide_parallel_matrix_args
         * \copydetails doxygen_hide_mpi_param
         * \copydoc doxygen_hide_source_location
         *
         */
        void InitParallelDenseMatrix(std::size_t Nlocal_row,
                                     std::size_t Nlocal_column,
                                     std::size_t Nglobal_row,
                                     std::size_t Nglobal_column,
                                     const Mpi& mpi,
                                     const std::source_location location = std::source_location::current());


        /*!
         * \brief Duplicate the layout of the matrix.
         *
         * \param[in] original Original matrix, which layout is to be duplicated.
         * \param[in] option Fine tune the behaviour of the duplication. Three values are possible:
         * - MAT_DO_NOT_COPY_VALUES
         * - MAT_COPY_VALUES
         * - MAT_SHARE_NONZERO_PATTERN (share the nonzero patterns with the previous matrix but do not copy
         * them.)
         * \copydoc doxygen_hide_source_location
         *
         * To make the third option work, the \a original matrix must have been assembled (i.e. where are the
         * non-zero values must already be determined accurately, not just how many of them there are on
         * each line - which is the information we get when a matrix is created). So technically
         * it means you can't just duplicate the layout of a matrix you've just initialized but in which
         * you haven't put any value yet.
         *
         */
        void DuplicateLayout(const Matrix& original,
                             MatDuplicateOption option,
                             const std::source_location location = std::source_location::current());


        /*!
         * \brief A wrapper over MatCopy, which assumed target already gets the right layout.
         *
         * \param[in] source Original matrix which content is copied. It is assumed the object
         * that called the method already gets the same layout as \a source.
         * \param[in] structure SAME_NONZERO_PATTERN or DIFFERENT_NONZERO_PATTERN. SAME_NONZERO_PATTERN
         * should apply for almost all cases in MoReFEM; however if you're using raw Petsc library
         * you'll see there are more prerequisite to this choice (the pattern must be fully built).
         * Within the frame of this wrapper there shouldn't be any problem provided one of the InitXXX()
         * method has been called.
         * \copydoc doxygen_hide_source_location
         */
        void Copy(const Matrix& source,
                  const MatStructure& structure = SAME_NONZERO_PATTERN,
                  const std::source_location location = std::source_location::current());


        /*!
         * \brief A complete copy: layout is copied first and then the values.
         *
         * \param[in] source Original matrix which layout AND content is copied.
         * Beware: this method requires that \a source has already been deeply initialized (SetValues()
         * and Assembly() should already have been called). See DuplicateLayout() for a lenghtier explanation.
         * \copydoc doxygen_hide_source_location
         *
         */
        void CompleteCopy(const Matrix& source, const std::source_location location = std::source_location::current());


        /*!
         * \brief Petsc Assembling of the matrix.
         *
         * \copydoc doxygen_hide_source_location
         */
        void Assembly(const std::source_location location = std::source_location::current());


        /*!
         * \brief Indicates if a matrix has been assembled and is ready for use; for example, in matrix-vector
         * product.
         *
         * \note This is a direct wrapper upon Petsc MatAssembled() function.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return True if the matrix has been assembled.
         */
        bool IsAssembled(const std::source_location location = std::source_location::current()) const;


        /*!
         * \brief Add or modify values inside a Petsc matrix.
         *
         * \note Petsc documentation says you should use this over MatSetValue, but usage is actually not that
         * wide: you actually define here a block of values. So for instance you choose \a row_indexing = { 1, 2
         * } and \a col_indexing = { 3, 5 }, you modify the four values (1, 3), (1, 5), (2, 3), (2, 5)... (which
         * might fail if some of it are actually not non zero values! See however \a ignore_zero_entries
         * for a possible workaround here).
         * To my knowledge there are no way to modify just (1, 3) and (2, 5) in one call! I skimmed a bit
         * through petsc-user help list, and they seem to suggest MatSetValues should be called for one row at a
         * time.
         *
         * \warning Assembly() must be called afterwards!
         *
         * \param[in] row_indexing Program-wise index of the rows which values will be set.
         * \param[in] column_indexing Program-wise index of the columns which values will be set.
         * \param[in] values Values to put in the matrix. This array should be the same size as \a row_indexing
         * x \a column_indexing: it is a block of values that is actually introduced!
         * \param [in] insertOrAppend Petsc ADD_VALUES or INSERT_VALUES (see Petsc documentation).
         * \copydoc doxygen_hide_source_location
         *
         * \param[in] do_ignore_zero_entries If yes, you might specify a block which encompass locations that
         * are not within the pattern. For instance in our case above, you may specify \a row_indexing = { 1, 2
         * } and \a col_indexing = { 3, 5 } even if pattern says there are no value at (1, 5). In this case, the
         * associated value in \a values MUST be 0.; any other value will rightfully trigger a Petsc error.
         *
         */
        void SetValues(const std::vector<PetscInt>& row_indexing,
                       const std::vector<PetscInt>& column_indexing,
                       const PetscScalar* values,
                       InsertMode insertOrAppend,
                       const std::source_location location = std::source_location::current(),
                       ignore_zero_entries do_ignore_zero_entries = ignore_zero_entries::no);


        /*!
         * \brief Set the values of a row.
         *
         * \warning Pattern must have been set beforehand!
         * \warning Assembly() must be called afterwards!
         *
         * \param[in] row_index Program-wise index of the row which values will be set.
         * \param[in] values Non-zero values to report in the matrix.
         * \copydoc doxygen_hide_source_location
         */
        void SetValuesRow(PetscInt row_index,
                          const PetscScalar* values,
                          const std::source_location location = std::source_location::current());

        /*!
         * \brief Set a single entry into a Petsc matrix.
         *
         * \warning Assembly() must be called afterwards!
         *
         * As noted in Petsc manual pages, if several values the SetValues() method should be preferred;
         * however see \a SetValues() documentation that specify the cases in which it can be applied.
         *
         * \param[in] row_index Program-wise index of the row which value will be set.
         * \param[in] column_index Program-wise index of the column which value will be set.
         * \param[in] value Value to put in the matrix.
         * \param [in] insertOrAppend Petsc ADD_VALUES or INSERT_VALUES (see Petsc documentation).
         * \copydoc doxygen_hide_source_location
         */
        void SetValue(PetscInt row_index,
                      PetscInt column_index,
                      PetscScalar value,
                      InsertMode insertOrAppend,
                      const std::source_location location = std::source_location::current());


        /*!
         * \brief Wrapper over \a MatLoad in the case the viewer is a file.
         *
         * \param[in] mpi Mpi object which knows the rank of the processor, the total number of processors,
         * etc... \param[in] input_file File from which matrix is to be loaded ON THE ROOT PROCESSOR.
         * \copydoc doxygen_hide_source_location
         *
         * \attention Thie methods should be called after at least a minimal initialization of the matrix has been done (type
         * of PETSc matrix should be set, and under the hood \a MatCreate should have been invoked. Concretely
         * in MoReFEM wrapper, this means you should have called beforehand a InitXXX() method - \a
         * InitMinimalCase() for instance. Of course you may also have
         *
         *
         * \attention So far (June 2020), the underlying \a MatLoad function works with two formats:
         * - Binary
         * - HDF5
         *  (see https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatLoad.html)
         * HDF5 is not currently used within MoReFEM, but PETSc comes with strong guarantees upon the
         * portability of binary files (see
         * https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Sys/PetscBinaryRead.html)
         *
         */
        void LoadBinary(const Mpi& mpi,
                        const FilesystemNS::File& input_file,
                        const std::source_location location = std::source_location::current());


      private:
        /*!
         * \brief Helper to init a matrix.
         *
         * \copydoc doxygen_hide_parallel_matrix_args
         * \copydetails doxygen_hide_mpi_param
         * \copydoc doxygen_hide_source_location
         * \param[in] petsc_matrix_type The PETSc identifier for the requested matrix (see
         * https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Mat/MatType.html#MatType)
         */
        void InitMatrixHelper(const Mpi& mpi,
                              MatType petsc_matrix_type,
                              std::size_t Nlocal_row,
                              std::size_t Nlocal_column,
                              std::size_t Nglobal_row,
                              std::size_t Nglobal_column,
                              const std::source_location location = std::source_location::current());


      private:
        // ============================
        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        //! Friendship.
        // ============================


        friend void Swap(Matrix& A, Matrix& B);


        // ============================
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN
        // ============================
    };


    //! Swap two matrices.
    void Swap(Matrix&, Matrix&);


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_MATRIX_DOT_HPP_
// *** MoReFEM end header guards *** < //
