// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_SHELLMATRIX_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_SHELLMATRIX_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    /*!
     * \brief Wrappers over a Petsc shell matrix.
     *
     * A shell matrix is described in Petsc as a user-defined private data storage format.
     * It is used to define indirectly a matrix used in a linear system; please have a look at FSI model to
     * see how it may be used.
     *
     * \attention Context is not stored explicitly in current class as Petsc already stores it in its matrix;
     * the type is given to enable interpretation of the void* Petsc is actually storing.
     *
     * \attention `Shell` here is the word used in PETSc context (we wrap here over a matrix created with `MatCreateShell`);
     * it is not related to the shell finite elements that will be soon supported by the library.
     *
     * \attention This was introduced YEARS ago in a model that is no longer up-to-date, and it is not present in any test.
     * It will probably be needed at some point, so I'd rather keep it, but it is entirely possible it should be
     * triggered to be recognized as a `PetscMatrixOperationResult` rather than a `PetscMatrix` (see `Concept` classes
     * for matrices).
     */
    template<class ContextT>
    class ShellMatrix
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = ShellMatrix;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to context.
        using context_type = ContextT;

        //! \copydoc doxygen_hide_petsc_matrix_concept_keyword
        static inline constexpr bool ConceptIsPetscMatrix = true;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_parallel_matrix_args
         *
         * \copydetails doxygen_hide_mpi_param
         *
         * \param[in] mat_op MatOperation object which specifies which operation is to be redefined for the
         * shell matrix. So far only MATOP_MULT has been redefined this way in FSI model.
         * \param[in] context Pointer to data needed by the shell matrix routines.
         * \copydoc doxygen_hide_source_location
         */
        explicit ShellMatrix(const Wrappers::Mpi& mpi,
                             std::size_t Nlocal_row,
                             std::size_t Nlocal_column,
                             std::size_t Nglobal_row,
                             std::size_t Nglobal_column,
                             ContextT* context,
                             MatOperation mat_op,
                             const std::source_location location = std::source_location::current());

        //! Destructor.
        ~ShellMatrix();

        //! \copydoc doxygen_hide_copy_constructor
        ShellMatrix(const ShellMatrix& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ShellMatrix(ShellMatrix&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        ShellMatrix& operator=(const ShellMatrix& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ShellMatrix& operator=(ShellMatrix&& rhs) = delete;

        ///@}


        /*!
         * \brief Handle over the internal Mat object.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return Internal Mat object, which is indeed a pointer in Petsc.
         *
         * Ideally it shouldn't be used at all except in the implementation of the Petsc Wrapper: a wrapper
         * method should be implemented over the function that might need access to the \a Mat internal object.
         */
        Mat Internal(std::source_location location);

        /*!
         * \brief Handle over the internal Mat object - when you can guarantee the call is only to read the
         * value, not act upon it.
         *
         * \copydoc doxygen_hide_source_location
         *
         * \return Internal Mat object, which is indeed a pointer in Petsc.
         *
         * Ideally it shouldn't be used at all except in the implementation of the Petsc Wrapper: a wrapper
         * method should be implemented over the function that might need access to the \a Mat internal object.
         */
        Mat InternalForReadOnly(std::source_location location) const noexcept;

      private:
        /*!
         * \brief Thin wrapper over MatShellSetOperation.
         *
         * \param[in] mat_op MatOperation object which specifies which operation is to be redefined for the
         * shell matrix. So far only MATOP_MULT has been redefined this way in FSI model.
         * \copydoc doxygen_hide_source_location
         *
         * The implementation itself is expected to be given by ContextT::ShellMatrixOperation(). This one must
         * respect the prototype expected by Petsc (e.g. int (Mat, Vec, Vec) for MATOP_MULT) and return 0 if
         * all is fine.
         *
         */
        void SetOperation(MatOperation mat_op, const std::source_location location = std::source_location::current());


      private:
        //! Underlying Petsc matrix.
        Mat petsc_matrix_;
    };


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ThirdParty/Wrappers/Petsc/Matrix/ShellMatrix.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_SHELLMATRIX_DOT_HPP_
// *** MoReFEM end header guards *** < //
