// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_INTERNAL_NONZEROPATTERN_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_INTERNAL_NONZEROPATTERN_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cassert>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/NonZeroPattern.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc
{


    /*!
     * \brief Associate the Petsc value that match the value of MoReFEM enum class.
     *
     * \tparam NonZeroPatternT Value of the enum class in MoReFEM.
     * \return The corresponding MatStructure object from Petsc.
     */
    template<NonZeroPattern NonZeroPatternT>
    constexpr MatStructure NonZeroPatternPetsc();


} // namespace MoReFEM::Internal::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ThirdParty/Wrappers/Petsc/Matrix/Internal/NonZeroPattern.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_INTERNAL_NONZEROPATTERN_DOT_HPP_
// *** MoReFEM end header guards *** < //
