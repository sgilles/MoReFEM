// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixInfo.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hpp"


namespace MoReFEM::Wrappers::Petsc
{


#ifndef NDEBUG


    void
    MatrixInfo(const std::string& tag, const Matrix& matrix, const std::source_location location, std::ostream& stream)
    {
        MatInfo infos;

        int error_code = MatGetInfo(matrix.InternalForReadOnly(location), MAT_LOCAL, &infos);


        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatGetInfo", location);

        MatType type;
        error_code = MatGetType(matrix.InternalForReadOnly(location), &type);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "MatGetType", location);


        stream << "Information about matrix " << tag << ':' << std::endl;

        stream << "\t- Type = " << type << std::endl;
        stream << "\t- Block size = " << infos.block_size << std::endl;
        stream << "\t- Number of non zeros (allocated | used | unneeded) = " << infos.nz_allocated << " | "
               << infos.nz_used << " | " << infos.nz_unneeded << std::endl;
        stream << "\t- Memory allocated = " << infos.memory << std::endl;
        stream << "\t- Number of Assemblies called = " << infos.assemblies << std::endl;
        stream << "\t- Number of mallocs during MatSetValues() = " << infos.mallocs << std::endl;
        stream << "\t- Fill ratio for LU/ILU = " << infos.fill_ratio_given << " | " << infos.fill_ratio_needed
               << std::endl;
        stream << "\t- Number of mallocs during factorization = " << infos.factor_mallocs << std::endl;
    }


#endif // NDEBUG


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
