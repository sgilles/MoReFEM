// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_MATRIXPATTERN_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_MATRIXPATTERN_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    inline std::size_t MatrixPattern::Nrow() const noexcept
    {
        assert(!iCSR_.empty());
        return iCSR_.size() - 1u;
    }


    inline const std::vector<PetscInt>& MatrixPattern::GetICsr() const noexcept
    {
        assert(!iCSR_.empty());
        return iCSR_;
    }

    inline const std::vector<PetscInt>& MatrixPattern::GetJCsr() const noexcept
    {
        // I used to put a condition upon jCSR_'s emptiness here, but it could hurt degenerate cases in which
        // one of the processor got very few or even no dofs to handle.
        return jCSR_;
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_MATRIX_MATRIXPATTERN_DOT_HXX_
// *** MoReFEM end header guards *** < //
