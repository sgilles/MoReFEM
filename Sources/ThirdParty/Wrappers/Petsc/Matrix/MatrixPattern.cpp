// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iterator>
#include <sstream>
#include <string_view>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Utilities/Containers/Print.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    MatrixPattern::MatrixPattern(const std::vector<std::vector<PetscInt>>& non_zero_slots_per_local_row)
    {
        iCSR_.reserve(non_zero_slots_per_local_row.size() + 1);

        iCSR_.push_back(0);
        std::size_t iCSR_index = 0;

        for (const auto& row : non_zero_slots_per_local_row)
        {
            iCSR_index += row.size();
            iCSR_.push_back(static_cast<PetscInt>(iCSR_index));
            std::move(row.begin(), row.end(), std::back_inserter(jCSR_));
        }
    }


    MatrixPattern::MatrixPattern(std::vector<PetscInt>&& iCSR, std::vector<PetscInt>&& jCSR)
    : iCSR_(std::move(iCSR)), jCSR_(std::move(jCSR))
    { }


    bool operator==(const MatrixPattern& lhs, const MatrixPattern& rhs)
    {
        if (lhs.GetICsr() != rhs.GetICsr())
            return false;

        return lhs.GetJCsr() == rhs.GetJCsr();
    }


    std::ostream& operator<<(std::ostream& out, const MatrixPattern& rhs)
    {
        std::ostringstream internal_out; // internal step to guarantee no interleaving in parallel.
        internal_out.copyfmt(out);

        const auto nnz_per_row = ExtractNonZeroPositionsPerRow(rhs);

        auto row_index{ 0 };
        std::ostringstream oconv;

        for (const auto& row : nnz_per_row)
        {
            oconv.str("");
            oconv << "For row " << row_index++ << ": [";

            Utilities::PrintContainer<>::Do(row,
                                            internal_out,
                                            ::MoReFEM::PrintNS::Delimiter::separator(", "),
                                            ::MoReFEM::PrintNS::Delimiter::opener(oconv.str()));
        }

        out << internal_out.str();

        return out;
    }


    std::vector<std::vector<PetscInt>> ExtractNonZeroPositionsPerRow(const Wrappers::Petsc::MatrixPattern& pattern)
    {
        decltype(auto) iCSR = pattern.GetICsr();

        const auto Nrow = pattern.Nrow();
        assert(Nrow > 0);

        decltype(auto) jCSR = pattern.GetJCsr();

        std::vector<std::vector<PetscInt>> ret;

        auto cumulative_iCSR_value{ 0ul };
        assert(iCSR.size() == Nrow + 1);

        auto current_jcsr_index{ 0ul };

        for (auto row_index = 0ul; row_index < Nrow; ++row_index)
        {
            std::vector<PetscInt> row_content;

            auto current_icsr_value = static_cast<std::size_t>(iCSR[row_index + 1]);

            auto Nnnz_for_row = current_icsr_value - cumulative_iCSR_value;
            cumulative_iCSR_value = current_icsr_value;

            for (auto i = 0ul; i < Nnnz_for_row; ++i)
            {
                assert(current_jcsr_index < jCSR.size());
                row_content.push_back(jCSR[current_jcsr_index++]);
            }

            ret.emplace_back(row_content);
        }


        return ret;
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
