// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <iostream>
#include <string>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMatPrivate.hpp" // IWYU pragma: export
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/Advanced/AbstractMatrix.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"
#include "ThirdParty/Wrappers/Petsc/Viewer.hpp"


namespace MoReFEM::Advanced::Wrappers::Petsc
{


    void AbstractMatrix::Unused() const
    {
        assert(false
               && "This method exists solely to provide an out-of-line virtual method definition and is not intended "
                  "to be ever used!");
        exit(EXIT_FAILURE);
    }


    AbstractMatrix::AbstractMatrix(std::string&& name,
                                   Advanced::Wrappers::Petsc::print_linalg_destruction do_print_linalg_destruction)
    : petsc_matrix_{ MOREFEM_PETSC_NULL }, do_call_petsc_destroy_{ call_petsc_destroy::no }, name_{ name },
      do_print_linalg_destruction_{ do_print_linalg_destruction }
    { }


    AbstractMatrix::AbstractMatrix(const AbstractMatrix& rhs, std::string&& name)
    : petsc_matrix_{ MOREFEM_PETSC_NULL }, do_call_petsc_destroy_{ rhs.do_call_petsc_destroy_ }, name_{ name },
      do_print_linalg_destruction_{ rhs.do_print_linalg_destruction_ }
    { }


    AbstractMatrix::AbstractMatrix(AbstractMatrix&& rhs)
    : petsc_matrix_(rhs.petsc_matrix_), do_call_petsc_destroy_(rhs.do_call_petsc_destroy_), name_{ rhs.name_ },
      do_print_linalg_destruction_{ rhs.do_print_linalg_destruction_ }
    {
        rhs.petsc_matrix_ = MOREFEM_PETSC_NULL;
        rhs.do_call_petsc_destroy_ = call_petsc_destroy::no; // important line here...
        rhs.name_ = "";
        rhs.do_print_linalg_destruction_ = Advanced::Wrappers::Petsc::print_linalg_destruction::no;
    }


    AbstractMatrix::~AbstractMatrix()
    {
        switch (do_call_petsc_destroy_)
        {
        case call_petsc_destroy::no:
            break;
        case call_petsc_destroy::yes:
        {
            assert(petsc_matrix_ != MOREFEM_PETSC_NULL);

            if (do_print_linalg_destruction_ == Advanced::Wrappers::Petsc::print_linalg_destruction::yes)
            {
                try
                {
                    std::cout << "=== Destroy matrix '" << name_ << "' (address was " << petsc_matrix_ << ")."
                              << std::endl;
                }
                catch (...) // can't throw in destructor!
                {
                    assert(false && "Shouldn't happen...");
                }
            }

            [[maybe_unused]] int error_code = MatDestroy(&petsc_matrix_);
            assert(!error_code && "Error in Mat destruction."); // no exception in destructors!
        }
        }
    }


    void Swap(AbstractMatrix& A, AbstractMatrix& B)
    {
        using std::swap;
        swap(A.do_call_petsc_destroy_, B.do_call_petsc_destroy_);
        swap(A.petsc_matrix_, B.petsc_matrix_);
        swap(A.name_, B.name_);
        swap(A.do_print_linalg_destruction_, B.do_print_linalg_destruction_);
    }


    void AbstractMatrix::ZeroEntries(const std::source_location location)
    {
        int error_code = MatZeroEntries(Internal(location));
        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatZeroEntries", location);
    }


    void AbstractMatrix::ZeroRows(const std::vector<PetscInt>& row_indexes,
                                  PetscScalar diagonal_value,
                                  const std::source_location location)
    {
        int error_code = MatZeroRows(Internal(location),
                                     static_cast<PetscInt>(row_indexes.size()),
                                     row_indexes.data(),
                                     diagonal_value,
                                     MOREFEM_PETSC_NULL,
                                     MOREFEM_PETSC_NULL);

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatZeroRows", location);
    }


    void AbstractMatrix::ZeroRowsColumns(const std::vector<PetscInt>& row_indexes,
                                         PetscScalar diagonal_value,
                                         const std::source_location location)
    {
        int error_code = MatZeroRowsColumns(Internal(location),
                                            static_cast<PetscInt>(row_indexes.size()),
                                            row_indexes.data(),
                                            diagonal_value,
                                            MOREFEM_PETSC_NULL,
                                            MOREFEM_PETSC_NULL);

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatZeroRowsColumns", location);
    }


    PetscScalar
    AbstractMatrix::GetValue(PetscInt row_index, PetscInt column_index, const std::source_location location) const
    {
        PetscScalar ret;

        int error_code = MatGetValues(InternalForReadOnly(location), 1, &row_index, 1, &column_index, &ret);

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatGetValues", location);

        return ret;
    }


    void AbstractMatrix::Scale(PetscScalar a, const std::source_location location)
    {
        int error_code = MatScale(Internal(location), a);
        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatScale", location);
    }


    std::pair<PetscInt, PetscInt> AbstractMatrix::GetProcessorWiseSize(const std::source_location location) const
    {
        std::pair<PetscInt, PetscInt> ret;

        int error_code = MatGetLocalSize(InternalForReadOnly(location), &ret.first, &ret.second);
        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatGetLocalSize", location);

        return ret;
    }


    std::pair<PetscInt, PetscInt> AbstractMatrix::GetProgramWiseSize(const std::source_location location) const
    {
        std::pair<PetscInt, PetscInt> ret;

        int error_code = MatGetSize(InternalForReadOnly(location), &ret.first, &ret.second);
        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatGetSize", location);

        return ret;
    }


    void AbstractMatrix::View(const ::MoReFEM::Wrappers::Mpi& mpi, const std::source_location location) const
    {
        int error_code = MatView(InternalForReadOnly(location), PETSC_VIEWER_STDOUT_(mpi.GetCommunicator()));
        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatView", location);
    }


    void AbstractMatrix::View(const ::MoReFEM::Wrappers::Mpi& mpi,
                              const FilesystemNS::File& output_file,
                              const std::source_location location,
                              PetscViewerFormat format,
                              PetscFileMode file_mode) const
    {
        ::MoReFEM::Wrappers::Petsc::Viewer viewer(mpi, output_file, format, file_mode, location);

        int error_code = MatView(InternalForReadOnly(location), viewer.GetUnderlyingPetscObject());
        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatView", location);
    }


    void AbstractMatrix::ViewBinary(const ::MoReFEM::Wrappers::Mpi& mpi,
                                    const FilesystemNS::File& output_file,
                                    const std::source_location location) const
    {
        View(mpi, output_file, location, PETSC_VIEWER_BINARY_MATLAB, FILE_MODE_WRITE);
    }


    void AbstractMatrix::GetRow(PetscInt row_index,
                                std::vector<std::pair<PetscInt, PetscScalar>>& row_content,
                                const std::source_location location) const
    {
        std::vector<PetscInt> row_content_position_list;
        std::vector<PetscScalar> row_content_value_list;

        GetRow(row_index, row_content_position_list, row_content_value_list, location);

        const auto size = row_content_position_list.size();
        assert(size == row_content_value_list.size());

        row_content.reserve(size);

        for (auto i = 0ul; i < size; ++i)
            row_content.emplace_back(std::make_pair(row_content_position_list[i], row_content_value_list[i]));
    }


    void AbstractMatrix::GetRow(PetscInt row_index,
                                std::vector<PetscInt>& row_content_position_list,
                                std::vector<PetscScalar>& row_content_value_list,
                                const std::source_location location) const
    {
        assert(row_content_position_list.empty());
        assert(row_content_value_list.empty());

        PetscInt Nnon_zero_cols;
        const PetscInt* columns;
        const PetscScalar* values;

        // Petsc MatGetRow() crashed if row index is not in the interval given below, so I have added
        // this very crude test to return nothing in this case.
        if (row_index < InternalForReadOnly(location)->rmap->rstart
            || row_index > InternalForReadOnly(location)->rmap->rend)
            return;

        int error_code = MatGetRow(InternalForReadOnly(location), row_index, &Nnon_zero_cols, &columns, &values);

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatGetRow", location);

        // This condition is there because Petsc might not be very consistent and return for instance:
        // - Nnon_zero_cols = 0
        // - columns = 0x0
        // - values to an address in memory (0x0 would be much better...)
        if (Nnon_zero_cols > 0)
        {
            assert(values != nullptr);
            assert(values != nullptr);

            const auto size = static_cast<std::size_t>(Nnon_zero_cols);
            row_content_position_list.resize(size);
            row_content_value_list.resize(size);

            for (auto i = 0ul; i < size; ++i)
            {
                row_content_position_list[i] = columns[i];
                row_content_value_list[i] = values[i];
            }
        }

        error_code = MatRestoreRow(InternalForReadOnly(location), row_index, &Nnon_zero_cols, &columns, &values);

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatRestoreRow", location);
    }


    void AbstractMatrix::GetColumnVector(PetscInt column_index,
                                         ::MoReFEM::Wrappers::Petsc::Vector& column,
                                         const std::source_location location) const
    {
        int error_code = MatGetColumnVector(InternalForReadOnly(location), column.Internal(location), column_index);
        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatGetColumnVector", location);
    }


    double AbstractMatrix::Norm(NormType type, const std::source_location location) const
    {
        PetscReal norm;

        assert(type == NORM_1 || type == NORM_FROBENIUS || type == NORM_INFINITY);

        int error_code = MatNorm(InternalForReadOnly(location), type, &norm);
        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatNorm", location);

        return static_cast<double>(norm);
    }


    void AbstractMatrix::GetInfo(MatInfo* infos, const std::source_location location)
    {
        int error_code = MatGetInfo(Internal(location), MAT_LOCAL, infos);

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatGetInfo", location);
    }


    void AbstractMatrix::GetOption(MatOption op, PetscBool* flg, const std::source_location location)
    {
        int error_code = MatGetOption(Internal(location), op, flg);

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatGetOption", location);
    }


    const std::string& AbstractMatrix::GetName() const noexcept
    {
        return name_;
    }


    void AbstractMatrix::SetDoNotDestroyPetscMatrix()
    {
        do_call_petsc_destroy_ = call_petsc_destroy::no;
    }


    void AbstractMatrix::SetDoCallPetscDestroy(call_petsc_destroy value)
    {
        do_call_petsc_destroy_ = value;
    }


    void AbstractMatrix::SetOption(MatOption op, PetscBool flg, const std::source_location location)
    {
        int error_code = MatSetOption(Internal(location), op, flg);

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatSetOption", location);
    }


    void AbstractMatrix::ChangeInternal(const ::MoReFEM::Wrappers::Mpi& mpi, Mat new_petsc_matrix)
    {
        petsc_matrix_ = new_petsc_matrix;
        mpi.Barrier();
    }


    MatType AbstractMatrix::GetType(const std::source_location location) const
    {
        MatType ret;
        int error_code = MatGetType(InternalForReadOnly(location), &ret);

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "MatGetType", location);

        return ret;
    }


    void AbstractMatrix::SetFromPetscMat(Mat petsc_matrix, const std::source_location location)
    {
        auto& underlying_petsc_object = InternalNoCheck();
        ::MoReFEM::Wrappers::Petsc::AssertionNS::InternalNotNull(petsc_matrix, location);
        ::MoReFEM::Wrappers::Petsc::AssertionNS::InternalNull(underlying_petsc_object, location);

        assert(DoCallPetscDestroy() == call_petsc_destroy::no);
        underlying_petsc_object = petsc_matrix;
        SetDoNotDestroyPetscMatrix();
    }


} // namespace MoReFEM::Advanced::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
