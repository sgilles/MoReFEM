// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixOpResult.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    MatrixOpResult::MatrixOpResult(std::string&& name,
                                   Advanced::Wrappers::Petsc::print_linalg_destruction do_print_linalg_destruction)
    : parent(std::move(name), do_print_linalg_destruction)
    { }


    MatrixOpResult::~MatrixOpResult() = default;


    MatrixOpResult::MatrixOpResult(MatrixOpResult&& rhs) : parent(std::move(rhs))
    { }


    bool MatrixOpResult::IsAlreadyInitialized() const noexcept
    {
        return parent::InternalNoCheckForReadOnly() != MOREFEM_PETSC_NULL;
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
