// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <string>
// IWYU pragma: no_include <iosfwd>

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscViewer.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Viewer.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    Viewer::Viewer(const Mpi& mpi,
                   const FilesystemNS::File& file,
                   PetscViewerFormat format,
                   PetscFileMode file_mode,
                   const std::source_location location)
    : viewer_(nullptr)
    {
        switch (format)
        {
        case PETSC_VIEWER_DEFAULT:
        case PETSC_VIEWER_ASCII_MATLAB:
        case PETSC_VIEWER_ASCII_MATHEMATICA:
        case PETSC_VIEWER_ASCII_IMPL:
        case PETSC_VIEWER_ASCII_INFO:
        case PETSC_VIEWER_ASCII_INFO_DETAIL:
        case PETSC_VIEWER_ASCII_COMMON:
        case PETSC_VIEWER_ASCII_SYMMODU:
        case PETSC_VIEWER_ASCII_INDEX:
        case PETSC_VIEWER_ASCII_DENSE:
        case PETSC_VIEWER_ASCII_MATRIXMARKET:
#if PETSC_VERSION_LT(3, 14, 0)
        case PETSC_VIEWER_ASCII_VTK:
        case PETSC_VIEWER_ASCII_VTK_CELL:
        case PETSC_VIEWER_ASCII_VTK_COORDS:
#else
        case PETSC_VIEWER_ASCII_VTK_DEPRECATED:
        case PETSC_VIEWER_ASCII_VTK_CELL_DEPRECATED:
        case PETSC_VIEWER_ASCII_VTK_COORDS_DEPRECATED:
#endif
        case PETSC_VIEWER_ASCII_PCICE:
        case PETSC_VIEWER_ASCII_PYTHON:
        case PETSC_VIEWER_ASCII_FACTOR_INFO:
        case PETSC_VIEWER_ASCII_LATEX:
        case PETSC_VIEWER_ASCII_XML:
        case PETSC_VIEWER_ASCII_GLVIS:
        case PETSC_VIEWER_ASCII_CSV:
#if PETSC_VERSION_GE(3, 15, 0)
        case PETSC_VIEWER_ASCII_FLAMEGRAPH:
#endif
            AsciiCase(mpi, file, format, location);
            break;
        case PETSC_VIEWER_BINARY_MATLAB:
            BinaryCase(mpi, file, file_mode, location);
            break;
        case PETSC_VIEWER_HDF5_PETSC:
        case PETSC_VIEWER_HDF5_VIZ:
        case PETSC_VIEWER_HDF5_XDMF:
        case PETSC_VIEWER_HDF5_MAT:
        case PETSC_VIEWER_DRAW_BASIC:
        case PETSC_VIEWER_DRAW_LG:
        case PETSC_VIEWER_DRAW_LG_XRANGE:
        case PETSC_VIEWER_DRAW_CONTOUR:
        case PETSC_VIEWER_DRAW_PORTS:
        case PETSC_VIEWER_VTK_VTS:
        case PETSC_VIEWER_VTK_VTR:
        case PETSC_VIEWER_VTK_VTU:
        case PETSC_VIEWER_NATIVE:
        case PETSC_VIEWER_NOFORMAT:
        case PETSC_VIEWER_LOAD_BALANCE:
#if PETSC_VERSION_GE(3, 14, 0)
        case PETSC_VIEWER_FAILED:
#endif
#if PETSC_VERSION_GE(3, 17, 0)
        case PETSC_VIEWER_ALL:
#endif
            throw Exception("The required Petsc Viewer format is not supported yet; if you need it "
                            "please contact the maintainers of MoReFEM library.");
        }
    }


    void Viewer::AsciiCase(const Mpi& mpi,
                           const FilesystemNS::File& ascii_file,
                           PetscViewerFormat format,
                           const std::source_location location)
    {
        int error_code = PetscViewerCreate(mpi.GetCommunicator(), &viewer_);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "PetscViewerCreate", location);

        if (format == PETSC_VIEWER_ASCII_MATLAB
            && !Utilities::String::EndsWith(static_cast<std::string>(ascii_file), ".m"))
            throw ExceptionNS::WrongMatlabExtension(ascii_file, location);

        // Important: 'ascii_file' is actually read only on root processor; on the others the argument
        // is not used. However as there are mpi communication involved the function must be called
        // for all ranks.
        error_code =
            PetscViewerASCIIOpen(mpi.GetCommunicator(), static_cast<std::string>(ascii_file).c_str(), &viewer_);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "PetscViewerASCIIOpen", location);

        error_code = PetscViewerPushFormat(viewer_, format);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "PetscViewerPushFormat", location);
    }


    void Viewer::BinaryCase(const Mpi& mpi,
                            const FilesystemNS::File& binary_file,
                            PetscFileMode file_mode,
                            const std::source_location location)
    {
        int error_code = PetscViewerCreate(mpi.GetCommunicator(), &viewer_);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "PetscViewerCreate", location);

        // Important: 'binary_file' is actually read only on root processor; on the others the argument
        // is not used. However as there are mpi communication involved the function must be called
        // for all ranks.
        error_code = PetscViewerBinaryOpen(
            mpi.GetCommunicator(), static_cast<std::string>(binary_file).c_str(), file_mode, &viewer_);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "PetscViewerBinaryOpen", location);
    }


    Viewer::~Viewer()
    {
        assert(!(!viewer_));
        int error_code = PetscViewerDestroy(&viewer_);
        static_cast<void>(error_code); // to avoid warning in release compilation.
        assert(!error_code);
    }


    PetscViewer& Viewer::GetUnderlyingPetscObject()
    {
        assert(!(!viewer_));
        return viewer_;
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
