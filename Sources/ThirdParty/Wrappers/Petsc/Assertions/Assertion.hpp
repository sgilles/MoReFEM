// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_ASSERTIONS_ASSERTION_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_ASSERTIONS_ASSERTION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
#include <source_location>
// IWYU pragma: no_include <string>

#include "Utilities/Exceptions/Advanced/Assertion/Assertion.hpp" // IWYU pragma: export


namespace MoReFEM::Wrappers::Petsc::AssertionNS
{


#ifndef NDEBUG


    //! When an internal PETSc object should have not been initialized and was notetheless.
    struct ShouldNotHaveBeenInitialized : public ::MoReFEM::Advanced::Assertion
    {
        /*!
         * \brief Constructor with simple message
         *
         * \copydoc doxygen_hide_source_location
         */
        explicit ShouldNotHaveBeenInitialized(const std::source_location location = std::source_location::current());

        //! Destructor.
        virtual ~ShouldNotHaveBeenInitialized() override;
    };


    //! When an internal PETSc object should have been initialized but was not.
    struct ShouldHaveBeenInitialized : public ::MoReFEM::Advanced::Assertion
    {
        /*!
         * \brief Constructor with simple message
         *
         * \copydoc doxygen_hide_source_location
         */
        explicit ShouldHaveBeenInitialized(const std::source_location location = std::source_location::current());

        //! Destructor.
        virtual ~ShouldHaveBeenInitialized() override;
    };


#endif // NDEBUG


    /*!
     * \brief In debug mode, check \a petsc_internal is not MOREFEM_PETSC_NULL. In release mode, do nothing.
     *
     * \param[in] petsc_internal PETSc internal value to check (typically a \a Mat or \a Vec object).
     *
     * \copydoc doxygen_hide_source_location
     */
    template<class PetscInternalT>
    void InternalNotNull(PetscInternalT petsc_internal, const std::source_location location);


    /*!
     * \brief In debug mode, check \a petsc_internal is MOREFEM_PETSC_NULL. In release mode, do nothing.
     *
     * \param[in] petsc_internal PETSc internal value to check (typically a \a Mat or \a Vec object).
     *
     * \copydoc doxygen_hide_source_location
     */
    template<class PetscInternalT>
    void InternalNull(PetscInternalT petsc_internal, const std::source_location location);


} // namespace MoReFEM::Wrappers::Petsc::AssertionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ThirdParty/Wrappers/Petsc/Assertions/Assertion.hxx"

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_ASSERTIONS_ASSERTION_DOT_HPP_
// *** MoReFEM end header guards *** < //
