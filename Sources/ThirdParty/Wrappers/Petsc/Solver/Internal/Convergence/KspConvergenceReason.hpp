// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_CONVERGENCE_KSPCONVERGENCEREASON_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_CONVERGENCE_KSPCONVERGENCEREASON_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSnes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp" // IWYU pragma: keep


namespace MoReFEM::Internal::Wrappers::Petsc
{


    //! \copydoc doxygen_hide_namespace_cluttering
    using convergence_status = ::MoReFEM::Wrappers::Petsc::convergence_status;


    /*!
     * \brief The purpose of this family of class is to provide two information about Ksp: whether
     * it converged or not, and a small text that explain exactly the reason.
     *
     * This text might be very short: it is taken from Petsc documentation.
     *
     * Each specialization must provide the method:
     *
     * \code
     * static constexpr convergence_status GetConvergenceStatus() noexcept;
     * static const std::string& Explanation();
     * \endcode
     *
     * \attention After updating Petsc version, you might have to
     */
    template<KSPConvergedReason ReasonT>
    struct KspConvergenceReason;


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    template<>
    struct KspConvergenceReason<KSP_CONVERGED_RTOL_NORMAL>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct KspConvergenceReason<KSP_CONVERGED_ATOL_NORMAL>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct KspConvergenceReason<KSP_CONVERGED_RTOL>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct KspConvergenceReason<KSP_CONVERGED_ATOL>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct KspConvergenceReason<KSP_CONVERGED_ITS>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


#if PETSC_VERSION_LT(3, 19, 0)
    template<>
    struct KspConvergenceReason<KSP_CONVERGED_CG_NEG_CURVE>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct KspConvergenceReason<KSP_CONVERGED_CG_CONSTRAINED>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };
#endif


    template<>
    struct KspConvergenceReason<KSP_CONVERGED_STEP_LENGTH>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct KspConvergenceReason<KSP_CONVERGED_HAPPY_BREAKDOWN>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct KspConvergenceReason<KSP_DIVERGED_NULL>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct KspConvergenceReason<KSP_DIVERGED_ITS>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct KspConvergenceReason<KSP_DIVERGED_DTOL>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct KspConvergenceReason<KSP_DIVERGED_BREAKDOWN>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct KspConvergenceReason<KSP_DIVERGED_BREAKDOWN_BICG>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct KspConvergenceReason<KSP_DIVERGED_NONSYMMETRIC>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct KspConvergenceReason<KSP_DIVERGED_INDEFINITE_PC>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct KspConvergenceReason<KSP_DIVERGED_NANORINF>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    template<>
    struct KspConvergenceReason<KSP_DIVERGED_INDEFINITE_MAT>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


#if PETSC_VERSION_LT(3, 11, 0)
    template<>
    struct KspConvergenceReason<KSP_DIVERGED_PCSETUP_FAILED>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };
#endif


#if PETSC_VERSION_GE(3, 11, 0)
    template<>
    struct KspConvergenceReason<KSP_DIVERGED_PC_FAILED>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };
#endif // PETSC_VERSION_GE(3, 11, 0)


    template<>
    struct KspConvergenceReason<KSP_CONVERGED_ITERATING>
    {

        static constexpr convergence_status GetConvergenceStatus() noexcept;


        static const std::string& Explanation();
    };


    // ============================
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


} // namespace MoReFEM::Internal::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Convergence/KspConvergenceReason.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_CONVERGENCE_KSPCONVERGENCEREASON_DOT_HPP_
// *** MoReFEM end header guards *** < //
