// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef> // IWYU pragma: keep
#include <string>  // IWYU pragma: keep
#include <utility>

// IWYU pragma: no_include <iosfwd>
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscConf.hpp"   // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMacros.hpp" // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSnes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Factory.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Instantiations/Gmres.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp" // IWYU pragma: keep


namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations
{


    namespace // anonymous
    {

        auto Create(Internal::Wrappers::Petsc::SolverNS::Settings&& solver_settings)
        {
            return std::make_unique<Gmres>(std::move(solver_settings));
        }


        // Register the solver in the 'Internal::Wrappers::Petsc::SolverNS::Factory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        __attribute__((unused)) const bool registered = // NOLINT
            SolverNS::Factory::CreateOrGetInstance().Register<Gmres>(Create);

    } // namespace


    const std::string& Gmres::Name()
    {
        static const std::string ret("Gmres");
        return ret;
    }


    Gmres::Gmres(SolverNS::Settings&& solver_settings)
    : parent(solver_type::iterative, parallel_support::yes, std::move(solver_settings))
    { }


    void Gmres::SetSolveLinearOptions(Snes& snes, const std::source_location location)
    {
        static_cast<void>(snes);
        static_cast<void>(location);
        static_cast<void>(location);
    }


    void Gmres::SupplInitOptions(Snes& snes, const std::source_location location)
    {
        auto ksp = snes.GetKsp(location);

        auto error_code = KSPGMRESSetRestart(ksp, GetRestart());

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "KSPGMRESSetRestart", location);
    }


    void Gmres::SupplPrintSolverInfos(Snes& snes, const std::source_location location) const
    {
        static_cast<void>(snes);
        static_cast<void>(location);
        static_cast<void>(location);
    }


    const std::string& Gmres::GetPetscName() const noexcept
    {
        static const std::string ret(KSPGMRES);
        return ret;
    }


    PetscInt Gmres::GetRestart() const noexcept
    {
        return static_cast<PetscInt>(GetSettings().GetRestart().Get());
    }


} // namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
