// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string> // IWYU pragma: keep
#include <utility>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscConf.hpp"   // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMacros.hpp" // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscMat.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Enum.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Factory.hpp"
#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Instantiations/Umfpack.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations
{


    namespace // anonymous
    {


        std::string UmfpackName()
        {
            return "Umfpack";
        }


#if not defined(MOREFEM_WITH_UMFPACK)

        // Register the solver in the 'Internal::Wrappers::Petsc::SolverNS::Factory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        __attribute__((unused)) const bool registered = // NOLINT
            SolverNS::Factory::CreateOrGetInstance().RegisterUnavailableSolver(UmfpackName());

#else // if not defined (MOREFEM_WITH_UMFPACK)

#if not PetscDefined(HAVE_SUITESPARSE) // not a typo!
        static_assert(
            false,
            "If MOREFEM_WITH_UMFPACK is set to true, PETSc should have been installed with this solver configured!"
            " (typically with an option such as --download-suitesparse)");
#endif

        auto Create(Internal::Wrappers::Petsc::SolverNS::Settings&& solver_settings)
        {
            return std::make_unique<Umfpack>(std::move(solver_settings));
        }


        // Register the solver in the 'Internal::Wrappers::Petsc::SolverNS::Factory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        __attribute__((unused)) const bool registered = // NOLINT
            SolverNS::Factory::CreateOrGetInstance().Register<Umfpack>(Create);

#endif // if not defined (MOREFEM_WITH_UMFPACK)

    } // namespace


#ifdef MOREFEM_WITH_UMFPACK


    const std::string& Umfpack::Name()
    {
        static const std::string ret{ UmfpackName() };
        return ret;
    }


    Umfpack::Umfpack(SolverNS::Settings&& solver_settings)
    : parent(solver_type::direct, parallel_support::no, std::move(solver_settings))
    { }


    void Umfpack::SetSolveLinearOptions(Snes& snes, const std::source_location location)
    {
        static_cast<void>(snes);
        static_cast<void>(location);
        static_cast<void>(location);
    }


    void Umfpack::SupplInitOptions(Snes& snes, const std::source_location location)
    {
        static_cast<void>(snes);
        static_cast<void>(location);
        static_cast<void>(location);
    }


    void Umfpack::SupplPrintSolverInfos(Snes& snes, const std::source_location location) const
    {
        static_cast<void>(snes);
        static_cast<void>(location);
        static_cast<void>(location);
    }


    const std::string& Umfpack::GetPetscName() const noexcept
    {
        static const std::string ret(MATSOLVERUMFPACK);
        return ret;
    }

#endif // MOREFEM_WITH_UMFPACK


} // namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
