### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_UTILITIES}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Gmres.cpp
		${CMAKE_CURRENT_LIST_DIR}/Mumps.cpp
		${CMAKE_CURRENT_LIST_DIR}/Petsc.cpp
		${CMAKE_CURRENT_LIST_DIR}/SuperLU_dist.cpp
		${CMAKE_CURRENT_LIST_DIR}/Umfpack.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Gmres.hpp
		${CMAKE_CURRENT_LIST_DIR}/Instantiations.doxygen
		${CMAKE_CURRENT_LIST_DIR}/Mumps.hpp
		${CMAKE_CURRENT_LIST_DIR}/Petsc.hpp
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
		${CMAKE_CURRENT_LIST_DIR}/SuperLU_dist.hpp
		${CMAKE_CURRENT_LIST_DIR}/Umfpack.hpp
)

