// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_INSTANTIATIONS_SUPERLU_DIST_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_INSTANTIATIONS_SUPERLU_DIST_DOT_HPP_
// *** MoReFEM header guards *** < //

#ifdef MOREFEM_WITH_SUPERLU_DIST

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <source_location>
#include <type_traits> // IWYU pragma: keep

#include "ThirdParty/Wrappers/Petsc/Solver/Internal/Solver.hpp"


namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations
{


    /*!
     * \brief Wrappers over SuperLU_dist solver within Petsc.
     */
    class SuperLU_dist final : public Internal::Wrappers::Petsc::Solver
    {

      public:
        //! Alias to parent.
        using parent = Internal::Wrappers::Petsc::Solver;

        //! \copydoc doxygen_hide_alias_self
        using self = SuperLU_dist;

        static_assert(std::is_convertible<self*, parent*>());

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Class name
        static const std::string& Name();


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \copydoc doxygen_hide_solver_settings_param
        explicit SuperLU_dist(SolverNS::Settings&& solver_settings);

        //! Destructor.
        ~SuperLU_dist() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        SuperLU_dist(const SuperLU_dist& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        SuperLU_dist(SuperLU_dist&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        SuperLU_dist& operator=(const SuperLU_dist& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SuperLU_dist& operator=(SuperLU_dist&& rhs) = delete;

        ///@}

      private:
        /*!
         * \copydoc doxygen_hide_solver_set_solve_linear_option
         *
         */
        void SetSolveLinearOptions(Snes& snes,
                                   const std::source_location location = std::source_location::current()) override;

        /*!
         * \copydoc doxygen_hide_solver_suppl_init_option
         *
         * Currently nothing is done at this stage for SuperLU_dist solver.
         */
        void SupplInitOptions(Snes& snes,
                              const std::source_location location = std::source_location::current()) override;

        /*!
         * \copydoc doxygen_hide_solver_print_infos
         *
         */
        void
        SupplPrintSolverInfos(Snes& snes,
                              const std::source_location location = std::source_location::current()) const override;

        //! \copydoc doxygen_hide_petsc_solver_name
        const std::string& GetPetscName() const noexcept override;
    };


} // namespace MoReFEM::Internal::Wrappers::Petsc::Instantiations


#endif // MOREFEM_WITH_SUPERLU_DIST

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_INTERNAL_INSTANTIATIONS_SUPERLU_DIST_DOT_HPP_
// *** MoReFEM end header guards *** < //
