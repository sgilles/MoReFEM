// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_ENUM_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_ENUM_DOT_HPP_
// *** MoReFEM header guards *** < //


#include "Utilities/Type/StrongType/Skills/Addable.hpp"
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp"
#include "Utilities/Type/StrongType/Skills/Printable.hpp"
#include "Utilities/Type/StrongType/StrongType.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    //! Convenient enum class to tag the type of solver.
    enum class solver_type { direct, iterative };


    /*!
     * \brief Enum to say whether a Newton converged or not.
     *
     * 'pending' means the Newton is still in progress. I don't know how it may happen, as Petsc doc says
     *  SNESGetConvergedReason() can only be called once SNESSolve() is complete, but as it exists in Petsc
     * enum I retranscribe it here.
     * 'unspecified' is there for SNES_CONVERGED_ITS: this is obtained when the maximum number of iterations
     * is reached but no convergence test was performed. I put it if at some point we need to introduce
     * a wrapper over SNESConvergedSkip(); in current state it should never happen.
     */
    enum class convergence_status { yes, no, pending, unspecified };


    //! Strong type,
    // clang-format off
    using absolute_tolerance_type =
    StrongType
    <
        PetscReal,
        struct AbsoluteToleranceTag,
        StrongTypeNS::Printable,
        StrongTypeNS::DefaultConstructible
    >;
    // clang-format on


    //! Strong type,
    // clang-format off
    using relative_tolerance_type =
    StrongType
    <
        PetscReal,
        struct RelativeToleranceTag,
        StrongTypeNS::Printable,
        StrongTypeNS::DefaultConstructible
    >;
    // clang-format on


    //! Strong type,
    // clang-format off
    using set_restart_type =
    StrongType
    <
        PetscInt,
        struct SetRestartTag,
        StrongTypeNS::Printable,
        StrongTypeNS::DefaultConstructible
    >;
    // clang-format on


    //! Strong type,
    // clang-format off
    using max_iteration_type =
    StrongType
    <
        PetscInt,
        struct MaxIterationTag,
        StrongTypeNS::Printable,
        StrongTypeNS::DefaultConstructible
    >;
    // clang-format on


    //! Strong type,
    // clang-format off
    using preconditioner_type =
    StrongType
    <
        std::string,
        struct PreconditionerTag,
        StrongTypeNS::Printable,
        StrongTypeNS::DefaultConstructible
    >;
    // clang-format on


    //! Strong type,
    // clang-format off
    using step_size_tolerance_type =
    StrongType
    <
        PetscReal,
        struct StepSizeToleranceTag,
        StrongTypeNS::Printable,
        StrongTypeNS::DefaultConstructible
    >;
    // clang-format on


    //! Strong type,
    // clang-format off
    using solver_name_type =
    StrongType
    <
        std::string,
        struct SolverNameTag,
        StrongTypeNS::Printable,
        StrongTypeNS::DefaultConstructible
    >;
    // clang-format on


    //! Strong type,
    // clang-format off
    using preconditioner_name_type =
    StrongType
    <
        std::string,
        struct PreconditionerNameTag,
        StrongTypeNS::Printable,
        StrongTypeNS::DefaultConstructible
    >;
    // clang-format on


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_SOLVER_ENUM_DOT_HPP_
// *** MoReFEM end header guards *** < //
