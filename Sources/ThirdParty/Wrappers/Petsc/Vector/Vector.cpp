// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib> // IWYU pragma: keep
#include <iosfwd>
#include <sstream>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Utilities/Exceptions/Advanced/Assertion/Assertion.hpp"
#include "Utilities/Miscellaneous.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSfTypes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVec.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscViewer.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Print.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Internal/CheckUpdateGhostManager.hpp" // IWYU pragma: keep
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"                           // IWYU pragma: keep
#include "ThirdParty/Wrappers/Petsc/Viewer.hpp"                                  // IWYU pragma: associated


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Wrappers::Petsc
{


#ifdef MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE


    namespace // anonymous
    {

        // Extract ghost values only from vector.
        std::vector<double> ExtractGhostValues(const Vector& vector, const std::source_location location);


    } // namespace


#endif // MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE


    void Swap(Wrappers::Petsc::Vector& A, Wrappers::Petsc::Vector& B)
    {
        using std::swap;
        swap(A.do_call_petsc_destroy_, B.do_call_petsc_destroy_);
        swap(A.ghost_padding_, B.ghost_padding_);
        swap(A.petsc_vector_, B.petsc_vector_);
        swap(A.name_, B.name_);
        swap(A.do_print_linalg_destruction_, B.do_print_linalg_destruction_);
    }


    void Vector::DuplicateLayout(const Vector& original, const std::source_location location)
    {
        assert(petsc_vector_ == MOREFEM_PETSC_NULL && "Should not be initialized when this method is called!");

        int error_code = VecDuplicate(original.InternalForReadOnly(location), &petsc_vector_);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecDuplicate", location);

        do_call_petsc_destroy_ = original.do_call_petsc_destroy_;
    }


    PetscInt Vector::GetProcessorWiseSize(const std::source_location location) const
    {
        PetscInt ret;
        int error_code = VecGetLocalSize(InternalForReadOnly(location), &ret);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecGetLocalSize", location);

        return ret;
    }


    PetscInt Vector::GetProgramWiseSize(const std::source_location location) const
    {
        PetscInt ret;
        int error_code = VecGetSize(InternalForReadOnly(location), &ret);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecGetSize", location);

        return ret;
    }


    void Vector::ZeroEntries(const std::source_location location, update_ghost do_update_ghost)
    {
        int error_code = VecZeroEntries(Internal(location));
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecZeroEntries", location);

        UpdateGhosts(location, do_update_ghost);
    }


    void Vector::SetValues(const std::vector<PetscInt>& indexing,
                           const PetscScalar* values,
                           InsertMode insertOrAppend,
                           const std::source_location location)
    {
        int error_code = VecSetValues(
            Internal(location), static_cast<PetscInt>(indexing.size()), indexing.data(), values, insertOrAppend);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecSetValues", location);

        // Don't add UpdateGhosts here! (it would stuck parallel programs).
        // SetValues() must be  followed by Assembly() call which  takes care of  it.
    }


    void Vector::SetValue(PetscInt index,
                          PetscScalar value,
                          InsertMode insertOrAppend,
                          const std::source_location location,
                          update_ghost do_update_ghost)
    {
        int error_code = VecSetValue(Internal(location), index, value, insertOrAppend);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecSetValue", location);

        UpdateGhosts(location, do_update_ghost);
    }


    void Vector::SetUniformValue(PetscScalar value, const std::source_location location, update_ghost do_update_ghost)
    {
        int error_code = VecSet(Internal(location), value);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecSetValue", location);

        UpdateGhosts(location, do_update_ghost);
    }


    std::vector<PetscScalar> Vector::GetValues(const std::vector<PetscInt>& indexing,
                                               const std::source_location location) const
    {
        std::vector<PetscScalar> ret(indexing.size());

        int error_code = VecGetValues(
            InternalForReadOnly(location), static_cast<PetscInt>(indexing.size()), indexing.data(), ret.data());

        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecGetValues", location);

        return ret;
    }

    void Vector::GetValues(const std::vector<PetscInt>& indexing,
                           std::vector<PetscScalar>& values,
                           const std::source_location location) const
    {
#ifndef NDEBUG
        if (values.size() != indexing.size())
        {
            if (values.empty())
                throw MoReFEM::Advanced::Assertion(
                    "The output vector must be allocated when using this overload of `Vector::GetValues()`!", location);
            else
                throw MoReFEM::Advanced::Assertion(
                    "Mismatch between index and value vectors size in `Vector::GetValues()`!", location);
        }
#endif // NDEBUG

        int error_code = VecGetValues(
            InternalForReadOnly(location), static_cast<PetscInt>(indexing.size()), indexing.data(), values.data());

        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecGetValues", location);
    }

    PetscScalar Vector::GetValue(PetscInt index, const std::source_location location) const
    {
        PetscScalar ret;

        int error_code = VecGetValues(InternalForReadOnly(location), static_cast<PetscInt>(1), &index, &ret);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecGetValues", location);

        return ret;
    }


    void Vector::Assembly(const std::source_location location, update_ghost do_update_ghost)
    {
        int error_code = VecAssemblyBegin(Internal(location));
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecAssemblyBegin", location);


        error_code = VecAssemblyEnd(Internal(location));
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecAssemblyEnd", location);

        UpdateGhosts(location, do_update_ghost);
    }


    void Vector::Copy(const Vector& source, const std::source_location location, update_ghost do_update_ghost)
    {
        int error_code = VecCopy(source.InternalForReadOnly(location), Internal(location));
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecCopy", location);

        UpdateGhosts(location, do_update_ghost);
    }


    void Vector::CompleteCopy(const Vector& source, const std::source_location location, update_ghost do_update_ghost)
    {
        DuplicateLayout(source, location);
        Copy(source, location, do_update_ghost);
    }


    void Vector::Scale(PetscScalar a, const std::source_location location, update_ghost do_update_ghost)
    {
        int error_code = VecScale(Internal(location), a);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecScale", location);

        UpdateGhosts(location, do_update_ghost);
    }


    void Vector::Shift(PetscScalar a, const std::source_location location, update_ghost do_update_ghost)
    {
        int error_code = VecShift(Internal(location), a);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecShift", location);

        UpdateGhosts(location, do_update_ghost);
    }


    void Vector::UpdateGhosts(const std::source_location location)
    {
        if (!IsGhosted(location))
            return;

#ifdef MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE
        const auto initial_ghost_values = ExtractGhostValues(*this, location);
#endif // MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE

        int error_code = VecGhostUpdateBegin(Internal(location), INSERT_VALUES, SCATTER_FORWARD);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecGhostUpdateBegin", location);

        error_code = VecGhostUpdateEnd(Internal(location), INSERT_VALUES, SCATTER_FORWARD);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecGhostUpdateEnd", location);


#ifdef MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE
        const auto final_ghost_values = ExtractGhostValues(*this, location);

        // To ignore this in sequential runs!
        if (!final_ghost_values.empty())
        {
            decltype(auto) check_update_ghost_manager =
                Internal::Wrappers::Petsc::CheckUpdateGhostManager::GetInstance();

            if (initial_ghost_values != final_ghost_values)
                check_update_ghost_manager.NeededCall(location);
            else
                check_update_ghost_manager.UnneededCall(location);
        }
#endif // MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE
    }


    void DisplaySomeValues(std::ostream& stream,
                           const Vector& vector,
                           PetscInt first_index,
                           PetscInt last_index,
                           int rank,
                           const std::source_location location)
    {
        AccessVectorContent<Utilities::Access::read_only> local_array(vector, location);

#ifndef NDEBUG
        {
            const PetscInt size = vector.GetProcessorWiseSize(location);
            assert(local_array.GetSize() == static_cast<std::size_t>(size));

            assert(first_index < last_index);
            assert(last_index < size);
        }
#endif // NDEBUG
        stream << "On processor " << rank << " [";
        auto values = local_array.GetArray();

        for (PetscInt i = first_index; i <= last_index; ++i)
            stream << values[i] << ", ";

        stream << ']' << std::endl;
    }


    bool AreEqual(const Vector& vec1,
                  const Vector& vec2,
                  const double epsilon,
                  std::string& inequality_description,
                  const std::source_location location)
    {
        inequality_description.clear();

        PetscInt Nprocessor_wise = vec1.GetProcessorWiseSize(location);
        assert(Nprocessor_wise == vec2.GetProcessorWiseSize(location));

        AccessGhostContent with_ghost1(vec1, location);
        AccessGhostContent with_ghost2(vec2, location);

        AccessVectorContent<Utilities::Access::read_only> local_array_1(with_ghost1.GetVectorWithGhost(), location);
        AccessVectorContent<Utilities::Access::read_only> local_array_2(with_ghost2.GetVectorWithGhost(), location);

        const auto Nprocessor_wise_plus_ghost = static_cast<PetscInt>(local_array_1.GetSize());
        assert(Nprocessor_wise_plus_ghost == static_cast<PetscInt>(local_array_2.GetSize()));

        const PetscScalar* values1 = local_array_1.GetArray();
        const PetscScalar* values2 = local_array_2.GetArray();

        bool ret = true;

        for (PetscInt index = 0; index < Nprocessor_wise_plus_ghost && ret;)
        {
            {
                if (std::fabs(values1[index] - values2[index]) > epsilon)
                {
                    std::ostringstream oconv;
                    oconv << "Inequality found for index " << index << ": vector 1 displays " << values1[index]
                          << " while vector 2 displays " << values2[index] << '.';

                    if (index >= Nprocessor_wise)
                        oconv << " This index refers to a ghost value (Nprocessor_wise = " << Nprocessor_wise << ").";

                    oconv << std::endl;

                    inequality_description = oconv.str();
                    ret = false;
                } else
                    ++index;
            }
        }

        return ret;
    }


    void AXPY(PetscScalar alpha,
              const Vector& x,
              Vector& y,
              const std::source_location location,
              update_ghost do_update_ghost)
    {
        int error_code = VecAXPY(y.Internal(location), alpha, x.InternalForReadOnly(location));
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecAXPY", location);

        y.UpdateGhosts(location, do_update_ghost);
    }


    PetscScalar DotProduct(const Vector& x, const Vector& y, const std::source_location location)
    {
        PetscScalar ret;

        int error_code = VecDot(x.InternalForReadOnly(location), y.InternalForReadOnly(location), &ret);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecDot", location);

        return ret;
    }


    std::pair<PetscInt, PetscReal> Vector::Min(const std::source_location location) const
    {
        PetscInt position;
        PetscReal value;

        int error_code = VecMin(InternalForReadOnly(location), &position, &value);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecMin", location);

        return std::make_pair(position, value);
    }


    std::pair<PetscInt, PetscReal> Vector::Max(const std::source_location location) const
    {
        PetscInt position;
        PetscReal value;

        int error_code = VecMax(InternalForReadOnly(location), &position, &value);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecMax", location);

        return std::make_pair(position, value);
    }


    double Vector::Norm(NormType type, const std::source_location location) const
    {
        PetscReal norm;

        assert(type == NORM_1 || type == NORM_2 || type == NORM_INFINITY);

        int error_code = VecNorm(InternalForReadOnly(location), type, &norm);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecNorm", location);

        return static_cast<double>(norm);
    }


    void Vector::View(const Mpi& mpi, const std::source_location location) const
    {
        int error_code = VecView(InternalForReadOnly(location), PETSC_VIEWER_STDOUT_(mpi.GetCommunicator()));
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecView", location);
    }


    void Vector::View(const Mpi& mpi,
                      const FilesystemNS::File& output_file,
                      const std::source_location location,
                      PetscViewerFormat format) const
    {
        Viewer viewer(mpi, output_file, format, FILE_MODE_WRITE, location);

        int error_code = VecView(InternalForReadOnly(location), viewer.GetUnderlyingPetscObject());
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecView", location);
    }


    void
    Vector::ViewBinary(const Mpi& mpi, const FilesystemNS::File& output_file, const std::source_location location) const
    {
        Viewer viewer(mpi, output_file, PETSC_VIEWER_BINARY_MATLAB, FILE_MODE_WRITE, location);

        int error_code = VecView(InternalForReadOnly(location), viewer.GetUnderlyingPetscObject());
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecView", location);
    }


    void GatherVector(const Mpi& mpi,
                      const Wrappers::Petsc::Vector& local_parallel_vector,
                      Wrappers::Petsc::Vector& sequential_vector,
                      const std::source_location location)
    {
        if (mpi.Nprocessor<int>() == 1)
        {
            sequential_vector.Copy(local_parallel_vector);

            Wrappers::Petsc::PrintMessageOnFirstProcessor("[WARNING] Calling GatherVector in sequential is not "
                                                          "efficient as a Copy is done in the end.\n",
                                                          mpi);
        } else
        {

            VecScatter vecscat;

            Vec local_petsc_vector = local_parallel_vector.InternalForReadOnly(location);
            Vec sequential_petsc_vector;

            int error_code = VecScatterCreateToAll(local_petsc_vector, &vecscat, &sequential_petsc_vector);
            if (error_code)
                throw ExceptionNS::Exception(error_code, "VecScatterCreateToAll", location);

            error_code =
                VecScatterBegin(vecscat, local_petsc_vector, sequential_petsc_vector, INSERT_VALUES, SCATTER_FORWARD);
            if (error_code)
                throw ExceptionNS::Exception(error_code, "VecScatterBegin", location);

            error_code =
                VecScatterEnd(vecscat, local_petsc_vector, sequential_petsc_vector, INSERT_VALUES, SCATTER_FORWARD);
            if (error_code)
                throw ExceptionNS::Exception(error_code, "VecScatterEnd", location);

            error_code = VecScatterDestroy(&vecscat);
            if (error_code)
                throw ExceptionNS::Exception(error_code, "VecScatterDestroy", location);

            sequential_vector.SetFromPetscVec(sequential_petsc_vector);
        }
    }


    void Vector::SetDoNotDestroyPetscVector()
    {
        do_call_petsc_destroy_ = call_petsc_destroy::no;
    }


    void Vector::SetFromPetscVec(const Vec& petsc_vector, const std::source_location location)
    {
        // In this specific method alone I can't use syntax sugary provided by the class, as I manipulate
        // a raw Petsc Vec such as the one provided in arguments of SnesFunction.
        const PetscScalar* values;
        int error_code = VecGetArrayRead(petsc_vector, &values);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecGetArrayRead", location);

        const auto size = static_cast<std::size_t>(GetProcessorWiseSize(location));

#ifndef NDEBUG
        {
            PetscInt petsc_size;
            error_code = VecGetLocalSize(petsc_vector, &petsc_size);

            if (error_code)
                throw ExceptionNS::Exception(error_code, "VecGetLocalSize", location);

            assert(size == static_cast<std::size_t>(petsc_size));
        }
#endif // NDEBUG

        AccessVectorContent<Utilities::Access::read_and_write> content(*this, location);

        for (auto i = 0ul; i < size; ++i)
            content[i] = values[i];

        error_code = VecRestoreArrayRead(petsc_vector, &values);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecRestoreArrayRead", location);
    }


    void Vector::ChangeInternal(Vec new_petsc_vector)
    {
        petsc_vector_ = new_petsc_vector;
        UpdateGhosts();
    }


    VecType Vector::GetType(const std::source_location location) const
    {
        VecType ret;
        int error_code = VecGetType(InternalForReadOnly(location), &ret);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecGetType", location);

        return ret;
    }


    bool Vector::IsGhosted(const std::source_location location) const
    {
        // Handle separately sequential case: it is not directly addressed in PETSc documentation
        // (but VecSeq is mentioned in the list of "parallel" vectors...)
        // but the code that works in parallel would falsely tell the sequential vector
        // is ghosted.
        if (!IsParallel(location))
            return false;

        Vec possible_values;
        int error_code = VecGhostGetLocalForm(InternalForReadOnly(location), &possible_values);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecGhostGetLocalForm", location);

        bool ret = (possible_values != MOREFEM_PETSC_NULL);

        error_code = VecGhostRestoreLocalForm(InternalForReadOnly(location), &possible_values);

        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecGhostRestoreLocalForm", location);

        return ret;
    }


    bool Vector::IsParallel(const std::source_location location) const
    {
        const auto type = std::string(GetType(location));

        if (type == std::string(VECSEQ))
            return false;

        if (type == std::string(VECMPI))
            return true;

        assert(false
               && "If we're here you're using a type of PETSc vector not yet supported by the library "
                  "(or it is deemed supported but this method was not updated accordingly). In any case, please "
                  "open an issue to let the library maintainers know and fix it.");
        exit(EXIT_FAILURE);
    }


    const std::string& Vector::GetName() const noexcept
    {
        return name_;
    }


#ifdef MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE


    namespace // anonymous
    {


        decltype(auto) mark = Internal::Wrappers::Petsc::CheckUpdateGhostManager::CreateOrGetInstance();


        std::vector<double> ExtractGhostValues(const Vector& vector, const std::source_location location)
        {
#ifdef NDEBUG
            std::cout << "MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE should only be defined in debug mode!"
                      << std::endl;
            exit(EXIT_FAILURE);
#endif // NDEBUG

            Wrappers::Petsc::AccessGhostContent access_ghost_content(vector, location);

            const auto Nitem_on_proc = vector.GetProcessorWiseSize(location);

            const auto& vector_with_ghost = access_ghost_content.GetVectorWithGhost();

            Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> vector_with_ghost_content(
                vector_with_ghost);

            const auto size = static_cast<int>(vector_with_ghost_content.GetSize(location));
            assert(size >= Nitem_on_proc);
            std::vector<double> ghost_values;
            const auto Nghost = static_cast<std::size_t>(size - Nitem_on_proc);

            ghost_values.reserve(Nghost);

            for (auto i = Nitem_on_proc; i < size; ++i)
                ghost_values.push_back(vector_with_ghost_content.GetValue(static_cast<std::size_t>(i)));

            assert(ghost_values.size() == Nghost);

            return ghost_values;
        }


    } // namespace


#endif // MOREFEM_CHECK_UPDATE_GHOSTS_CALL_RELEVANCE


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
