// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/Wrappers/Petsc/Vector/Exceptions/Vector.hpp"


namespace // anonymous
{


    // Forward declarations here; definitions are at the end of the file
    std::string InvalidAsciiFileMsg(const MoReFEM::FilesystemNS::File& file);

} // namespace


namespace MoReFEM::Wrappers::Petsc::ExceptionNS
{


    InvalidAsciiFile::~InvalidAsciiFile() = default;


    InvalidAsciiFile::InvalidAsciiFile(const FilesystemNS::File& file, const std::source_location location)
    : MoReFEM::Exception(InvalidAsciiFileMsg(file), location)
    { }


} // namespace MoReFEM::Wrappers::Petsc::ExceptionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    std::string InvalidAsciiFileMsg(const MoReFEM::FilesystemNS::File& file)
    {
        std::ostringstream oconv;
        oconv << "File " << file
              << " couldn't be interpreted properly as an ascii file. Please check it is an ascii "
                 "file and not a binary file, and that it was:\n"
                 "\t- Either an ad hoc file generated internally which provides one value per row.\n"
                 "\t- Or a Matlab file generated directly by Petsc.\n";

        return oconv.str();
    }


} // namespace
