// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert> // IWYU pragma: keep
#include <memory>  // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    AccessGhostContent::AccessGhostContent(const Vector& vector, const std::source_location location)
    : vector_without_ghost_(vector)
    //            local_content_(nullptr)
    {
        int error_code =
            VecGhostGetLocalForm(vector_without_ghost_.InternalForReadOnly(location), &petsc_vector_with_ghost_);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecGhostGetLocalForm", location);
    }


    AccessGhostContent::~AccessGhostContent()
    {
        assert(petsc_vector_with_ghost_ != MOREFEM_PETSC_NULL);
        assert(vector_without_ghost_.InternalForReadOnly(std::source_location::current()) != MOREFEM_PETSC_NULL);

        int error_code = VecGhostRestoreLocalForm(
            vector_without_ghost_.InternalForReadOnly(std::source_location::current()), &petsc_vector_with_ghost_);
        static_cast<void>(error_code); // avoid warning in release mode
        assert(error_code == 0);       // error code should be 0; exception can't be thrown in a destructor!
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
