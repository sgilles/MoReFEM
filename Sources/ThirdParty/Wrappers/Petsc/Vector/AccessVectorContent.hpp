// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_ACCESSVECTORCONTENT_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_ACCESSVECTORCONTENT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <source_location>

#include "Utilities/Miscellaneous.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    /*!
     * \brief Class used to get a glimpse over the local content of a Vector, and possibly modify them.
     *
     * \tparam AccessT Whether there are read-only or read/write rights to the content of the vector.
     * The point of it is that Petsc itself doesn't enforce constness at all; so
     *
     * In Petsc you can reach what is in the local processor by:
     *  \code
     * Vec vector;
     * ...
     * PetscScalar* values;
     * VecGetArray(vector, &values);
     * ...
     * (you can modify the values there if you like)
     * ...
     * // Do not forget to call this line at the end!!!
     * VecRestoreArray(vector, &values);
     * \endcode
     *
     * Current class is a RAII around what is above.
     *
     * \attention make sure to create object of this class locally, typically in a block, to make sure
     * VecRestoreArray is called as soon as possible!
     *
     * For instance,
     * \code
     * {  // very important brace!!!
     *
     *    AccessVectorContent<Utilities::Access::read_only> local_array(vector);
     *    PetscScalar* values = local_array.values();
     *    ...
     * }
     * \endcode
     */
    template<Utilities::Access AccessT>
    class AccessVectorContent
    {
      public:
        //! Alias over const unique_ptr.
        using const_unique_ptr = std::unique_ptr<const AccessVectorContent>;

        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor
         *
         * \param[in] vector Vector for which local access is granted.
         * \copydoc doxygen_hide_source_location
         *
         * NOTE: Vector is accessed by value instead of reference because it is an alias over a pointer...
         */
        explicit AccessVectorContent(typename VectorForAccess<AccessT>::Type& vector,
                                     const std::source_location location = std::source_location::current());

        //! Destructor
        ~AccessVectorContent();

        //! \copydoc doxygen_hide_copy_constructor
        AccessVectorContent(const AccessVectorContent& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        AccessVectorContent(AccessVectorContent&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        AccessVectorContent& operator=(const AccessVectorContent& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        AccessVectorContent& operator=(AccessVectorContent&& rhs) = delete;

        ///@}


        //! Get a pointer to the underlying local array.
        typename VectorForAccess<AccessT>::scalar_array_type GetArray() const;

        //! Get a given value of the underlying local array.
        //! \param[in] i Index of the requested value.
        PetscScalar GetValue(std::size_t i) const;

        //! Get the number of elements in the array.
        //! \copydoc doxygen_hide_source_location
        std::size_t GetSize(const std::source_location location = std::source_location::current()) const;

        /*!
         * \copydoc doxygen_hide_non_const_subscript_operator
         *
         * \internal <b><tt>[internal]</tt></b> Compilation will fail if this method is called with
         * AccessT set as read_only.
         * \endinternal
         *
         * \note Index \a must be in [0, N[ where N is the (processor-wise) size of the underlying vector.
         *
         */
        PetscScalar& operator[](std::size_t i);


      private:
        /*!
         * \brief Vector from which the value will be read.
         *
         */
        typename VectorForAccess<AccessT>::Type& vector_;

        /*!
         * \brief Underlying local array. Do not free it: Petsc is in charge of doing so!
         *
         * Might be NULL is the vector used in constructor is empty.
         */
        typename VectorForAccess<AccessT>::scalar_array_type values_;
    };


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_ACCESSVECTORCONTENT_DOT_HPP_
// *** MoReFEM end header guards *** < //
