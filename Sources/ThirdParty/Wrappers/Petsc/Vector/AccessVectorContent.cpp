// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstdlib>
#include <iostream>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"


namespace MoReFEM::Wrappers::Petsc
{


    template<>
    AccessVectorContent<Utilities::Access::read_and_write>::AccessVectorContent(
        typename VectorForAccess<Utilities::Access::read_and_write>::Type& vector,
        const std::source_location location)
    : vector_(vector)
    {
        int error_code = VecGetArray(vector.Internal(location), &values_);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecGetArray", location);
    }


    template<>
    AccessVectorContent<Utilities::Access::read_only>::AccessVectorContent(
        typename VectorForAccess<Utilities::Access::read_only>::Type& vector,
        const std::source_location location)
    : vector_(vector)
    {
        int error_code = VecGetArrayRead(vector.InternalForReadOnly(location), &values_);
        if (error_code)
            throw ExceptionNS::Exception(error_code, "VecGetArrayRead", location);
    }


    template<>
    AccessVectorContent<Utilities::Access::read_and_write>::~AccessVectorContent()
    {
        assert(vector_.Internal<Vector::check_non_null_ptr::no>(std::source_location::current()) != MOREFEM_PETSC_NULL);

        int error_code = VecRestoreArray(vector_.Internal(std::source_location::current()), &values_);

        if (error_code)
        {
            std::cerr << "Error during call to Petsc function VecRestoreArray(). As a result, program will abort."
                      << std::endl;
            abort();
        }

        try
        {
            vector_.UpdateGhosts();
        }
        catch (...)
        {
            std::cerr << "Exception thrown when updating ghosts. As a result, program will abort." << std::endl;
            abort();
        }
    }


    template<>
    AccessVectorContent<Utilities::Access::read_only>::~AccessVectorContent()
    {
        assert(vector_.InternalForReadOnly<Vector::check_non_null_ptr::no>(std::source_location::current())
               != MOREFEM_PETSC_NULL);

        int error_code = VecRestoreArrayRead(vector_.InternalForReadOnly(std::source_location::current()), &values_);
        assert(error_code == 0);       // error code should be 0; exception can't be thrown in a destructor!
        static_cast<void>(error_code); // avoid warning in release mode
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
