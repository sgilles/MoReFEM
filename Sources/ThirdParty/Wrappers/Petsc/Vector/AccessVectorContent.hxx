// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_ACCESSVECTORCONTENT_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_ACCESSVECTORCONTENT_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"


#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Assertions/Assertion.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"

namespace MoReFEM::Wrappers::Petsc
{


    template<Utilities::Access AccessT>
    inline auto AccessVectorContent<AccessT>::GetArray() const -> typename VectorForAccess<AccessT>::scalar_array_type
    {
        return values_;
    }


    template<Utilities::Access AccessT>
    inline PetscScalar AccessVectorContent<AccessT>::GetValue(std::size_t i) const
    {
        assert(values_ != NULL);
        assert(i <= static_cast<std::size_t>(vector_.GetProcessorWiseSize()));
        return values_[i];
    }


    template<Utilities::Access AccessT>
    inline std::size_t AccessVectorContent<AccessT>::GetSize(const std::source_location location) const
    {
        AssertionNS::InternalNotNull(vector_.template InternalForReadOnly<Vector::check_non_null_ptr::no>(location),
                                     location);

        return static_cast<std::size_t>(vector_.GetProcessorWiseSize(location));
    }


    template<Utilities::Access AccessT>
    PetscScalar& AccessVectorContent<AccessT>::operator[](std::size_t i)
    {
        static_assert(AccessT != Utilities::Access::read_only,
                      "This method should only be called when read/write rights are given!");
        assert(values_ != NULL);
        assert(i <= static_cast<std::size_t>(vector_.GetProcessorWiseSize()));
        return values_[i];
    }


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VECTOR_ACCESSVECTORCONTENT_DOT_HXX_
// *** MoReFEM end header guards *** < //
