// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VIEWER_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VIEWER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <source_location>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscViewer.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Wrappers::Petsc
{


    /*!
     * \brief RAII over PetscViewer object.
     *
     * You shouldn't have to use directly such object; the idea is to instead functions such as \a
     * Matrix::LoadBinary() or \a Vector::View().
     */
    class Viewer
    {
      public:
        /// \name Special members.
        ///@{

        /*!
         * \class doxygen_hide_petsc_file_mode
         *
         * \param[in] file_mode Same modes as in
         * [Petsc
         * documentation](https://www.mcs.anl.gov/petsc/petsc-current/docs/manualpages/Sys/PetscFileMode.html)
         * , i.e.:
         * - FILE_MODE_WRITE - create new file for binary output.
         * - FILE_MODE_READ - open existing file for binary input.
         * - FILE_MODE_APPEND - open existing file for binary output.
         * Note: this field is actually not used if the format is an ascii one.
         */


        /*!
         * \class doxygen_hide_petsc_viewer_format
         *
         * \param[in] format Format in which the matrix is written. See Petsc manual pages to get all the
         * formats available; relevant ones so far are PETSC_VIEWER_DEFAULT and PETSC_VIEWER_ASCII_* ones and
         * PETSC_VIEWER_BINARY_MATLAB.
         */


        /*!
         * \brief Constructor for an ascii file.
         *
         * \copydetails doxygen_hide_mpi_param
         * \param[in] file Path to the file to which the Petsc object will be associated.
         * \copydoc doxygen_hide_source_location
         * \copydoc doxygen_hide_petsc_viewer_format
         * \copydoc doxygen_hide_petsc_file_mode
         *
         */
        Viewer(const Mpi& mpi,
               const FilesystemNS::File& file,
               PetscViewerFormat format,
               PetscFileMode file_mode,
               const std::source_location location = std::source_location::current());


        //! Destructor.
        ~Viewer();

        //! \copydoc doxygen_hide_copy_constructor
        Viewer(const Viewer& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Viewer(Viewer&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Viewer& operator=(const Viewer& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Viewer& operator=(Viewer&& rhs) = delete;


        ///@}


        //! Access to the underlying viewer.
        PetscViewer& GetUnderlyingPetscObject();

      private:
        /*!
         * \brief Method called by the constructor if the format is an ascii one.
         *
         * \param[in] file Path to the file to which the Petsc object will be associated.
         * \copydetails doxygen_hide_mpi_param
         * \copydoc doxygen_hide_petsc_viewer_format
         * \copydoc doxygen_hide_source_location
         */
        void AsciiCase(const Mpi& mpi,
                       const FilesystemNS::File& file,
                       PetscViewerFormat format,
                       const std::source_location location = std::source_location::current());


        /*!
         * \brief Method called by the constructor if the format is a binary one.
         *
         *  \param[in] file Path to the file to which the Petsc object will be associated.
         * \copydetails doxygen_hide_mpi_param
         * \copydoc doxygen_hide_petsc_file_mode
         * \copydoc doxygen_hide_source_location
         */
        void BinaryCase(const Mpi& mpi,
                        const FilesystemNS::File& file,
                        PetscFileMode file_mode,
                        const std::source_location location = std::source_location::current());

      private:
        //! Underlying PetscViewer object (it is truly a pointer over an alias);
        PetscViewer viewer_;
    };


} // namespace MoReFEM::Wrappers::Petsc


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_PETSC_VIEWER_DOT_HPP_
// *** MoReFEM end header guards *** < //
