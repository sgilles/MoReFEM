// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#ifdef MOREFEM_WITH_SLEPC

#include <cassert>
#include <memory> // IWYU pragma: keep
#include <string>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp" // IWYU pragma: keep
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/IncludeWithoutWarning/Slepc/SlepcSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Exceptions/Petsc.hpp"
#include "ThirdParty/Wrappers/Slepc/Internal/RAII.hpp"


namespace MoReFEM::Internal::SlepcNS
{


    RAII::RAII()
    {
        int error_code = SlepcInitialize(MOREFEM_PETSC_NULL, MOREFEM_PETSC_NULL, MOREFEM_PETSC_NULL, "");

        if (error_code)
            throw ::MoReFEM::Wrappers::Petsc::ExceptionNS::Exception(error_code, "SlepcInitialize");
    }


    RAII::~RAII()
    {
        int error_code = SlepcFinalize();
        assert(!error_code && "Error in SlepcFinalize call!"); // No exception in destructors...
        static_cast<void>(error_code);                         // avoid warning in release compilation.
    }


    const std::string& RAII::ClassName()
    {
        static const std::string ret("Internal::SlepcNS::RAII");
        return ret;
    }


} // namespace MoReFEM::Internal::SlepcNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
#endif // MOREFEM_WITH_SLEPC
