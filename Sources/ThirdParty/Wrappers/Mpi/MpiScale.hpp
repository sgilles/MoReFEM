// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_MPI_MPISCALE_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_MPI_MPISCALE_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM
{


    /*!
     * \brief Convenient alias to distinguish processor-wise and program-wise quantities.
     *
     * \note The usual words used in HPC are simply 'local' and 'global'. However, given that in a finite element
     * context those words also get a very different meaning, we chose to rename the ones related to parallelism.
     */
    enum class MpiScale { processor_wise = 0, program_wise };


    /*!
     * \brief An enum to distinguish between processor-wise and ghost on a given mpi rank.
     */
    enum class RoleOnProcessor { processor_wise = 0, ghost };


    /*!
     * \brief Same as \a RoleOnProcessor with the additional possibility to choose 'both'
     */
    enum class RoleOnProcessorPlusBoth { processor_wise = 0, ghost, both };


    /*!
     * \class doxygen_hide_tparam_role_on_processor
     *
     * \tparam RoleOnProcessorT Whether we consider processor-wise or ghost.
     */

    /*!
     * \class doxygen_hide_tparam_role_on_processor_plus_both
     *
     * \tparam RoleOnProcessorPlusBothT Whether we consider processor-wise or ghost. If both, encompass both cases.
     */

} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_MPI_MPISCALE_DOT_HPP_
// *** MoReFEM end header guards *** < //
