// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cassert>
#include <iostream>
#include <limits>
#include <optional>
#include <source_location>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Mpi/Exceptions/Mpi.hpp"
#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
#include "mpi.h"


namespace MoReFEM::Wrappers
{


    void Mpi::InitEnvironment(int argc, char** argv)
    {
        if (IsEnvironment())
            throw ExceptionNS::Mpi::Exception("Mpi::InitEnvironment() called when an MPI environment is still active!");

        assert(!Nalive() && "If not environment set Nalive() must be equal to 0!");

        IsEnvironment() = true;
        MPI_Init(&argc, &argv);
    }


    bool& Mpi::IsEnvironment()
    {
        static bool value = false;
        return value;
    }


    int& Mpi::Nalive()
    {
        static int counter = 0;
        return counter;
    }


    void Mpi::IncrementNalive()
    {
        ++Nalive();
    }


    void Mpi::DecrementNalive()
    {
        if (--Nalive() == 0)
        {
            IsEnvironment() = false;
            const int error_code = MPI_Finalize();
            AbortIfErrorCode(std::numeric_limits<int>::lowest(), error_code);
        }
    }


    Mpi::Mpi(int root_processor, MpiNS::Comm comm)
    : root_processor_(root_processor), Nprocessor_(NumericNS::UninitializedIndex<int>()),
      comm_(MpiNS::Communicator(comm)), rank_(NumericNS::UninitializedIndex<int>())
    {
        if (!IsEnvironment())
            throw ExceptionNS::Mpi::Exception("Mpi::InitEnvironment() must be called before a Mpi object is created! "
                                              "(such a call should typically occur in the beginning of your main).");

        const auto& communicator = GetCommunicator();

        int error_code = MPI_Comm_rank(communicator, &rank_);

        if (error_code != MPI_SUCCESS)
            throw ExceptionNS::Mpi::Exception("Didn't manage to get rank information through MPI_Comm_rank() call");

        error_code = MPI_Comm_size(communicator, &Nprocessor_);

        if (error_code != MPI_SUCCESS)
            throw ExceptionNS::Mpi::Exception(rank_, error_code);

        //            PRAGMA_DIAGNOSTIC(push)
        //            PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")
        error_code = MPI_Comm_set_errhandler(communicator, MPI_ERRORS_RETURN);
        //            PRAGMA_DIAGNOSTIC(pop)

        if (error_code != MPI_SUCCESS)
            throw ExceptionNS::Mpi::Exception(rank_, error_code);

        IncrementNalive();
    }


    Mpi::~Mpi()
    {
        DecrementNalive();
    }


    namespace // anonymous
    {


        std::vector<short int> BoolToShortInt(const std::vector<bool>& data)
        {
            std::vector<short int> ret(data.size());
            std::transform(data.cbegin(),
                           data.cend(),
                           ret.begin(),
                           [](bool value)
                           {
                               return value ? 1 : 0;
                           });
            return ret;
        }


        template<class T>
        void ShortIntToBool(T&& data, std::vector<bool>& bool_data)
        {
            static_assert(std::is_same<std::remove_reference_t<T>, std::vector<short int>>(),
                          "Forwarding reference trick; T is expected to be std::vector<short int> "
                          "with possibly const or r/l value reference.");

            bool_data.resize(data.size());
            std::transform(data.cbegin(),
                           data.cend(),
                           bool_data.begin(),
                           [](bool value)
                           {
                               return value == 1;
                           });
        }


    } // namespace


    void Mpi::Gather(const std::vector<bool>& sent_data, std::vector<bool>& gathered_data) const
    {
        assert(!sent_data.empty());

        // As there is no mpi type for bool, we'll make the mpi operation with short int and then convert
        // back to bool.
        auto&& converted_sent_data = BoolToShortInt(sent_data);
        std::vector<short int> converted_gathered_data;
        Gather(converted_sent_data, converted_gathered_data);
        if (IsRootProcessor())
            ShortIntToBool(converted_gathered_data, gathered_data);
    }


    void Mpi::Gatherv(const std::vector<bool>& sent_data, std::vector<bool>& gathered_data) const
    {
        assert(!sent_data.empty());

        // As there is no mpi type for bool, we'll make the mpi operation with short int and then convert
        // back to bool.
        auto&& converted_sent_data = BoolToShortInt(sent_data);
        std::vector<short int> converted_gathered_data;
        Gatherv(converted_sent_data, converted_gathered_data);
        if (IsRootProcessor())
            ShortIntToBool(converted_gathered_data, gathered_data);
    }


    void Mpi::AllGather(const std::vector<bool>& sent_data, std::vector<bool>& gathered_data) const
    {
        assert(!sent_data.empty());

        // As there is no mpi type for bool, we'll make the mpi operation with short int and then convert
        // back to bool.
        auto&& converted_sent_data = BoolToShortInt(sent_data);
        std::vector<short int> converted_gathered_data;
        AllGather(converted_sent_data, converted_gathered_data);
        if (IsRootProcessor())
            ShortIntToBool(converted_gathered_data, gathered_data);
    }


    void Mpi::AllGatherv(const std::vector<bool>& sent_data, std::vector<bool>& gathered_data) const
    {
        assert(!sent_data.empty());

        // As there is no mpi type for bool, we'll make the mpi operation with short int and then convert
        // back to bool.
        auto&& converted_sent_data = BoolToShortInt(sent_data);
        std::vector<short int> converted_gathered_data;
        AllGatherv(converted_sent_data, converted_gathered_data);
        if (IsRootProcessor())
            ShortIntToBool(converted_gathered_data, gathered_data);
    }


    void Mpi::Broadcast(std::vector<bool>& data, std::optional<int> send_processor) const
    {
        // assert(!data.empty());

        // As there is no mpi type for bool, we'll make the mpi operation with short int and then convert
        // back to bool.
        auto converted_data = BoolToShortInt(data);

        Broadcast(converted_data, send_processor);

        ShortIntToBool(converted_data, data);
    }


    std::vector<bool> Mpi::AllReduce(const std::vector<bool>& sent_data, MpiNS::Op mpi_operation) const
    {
        assert(!sent_data.empty());

        // As there is no mpi type for bool, we'll make the mpi operation with short int and then convert
        // back to bool.
        auto&& converted_sent_data = BoolToShortInt(sent_data);

        auto&& converted_gathered_data = AllReduce(converted_sent_data, mpi_operation);

        std::vector<bool> ret;
        ShortIntToBool(std::move(converted_gathered_data), ret);
        return ret;
    }


    std::vector<bool> Mpi::ReduceOnRootProcessor(const std::vector<bool>& sent_data, MpiNS::Op mpi_operation) const
    {
        assert(!sent_data.empty());

        // As there is no mpi type for bool, we'll make the mpi operation with short int and then convert
        // back to bool.
        auto&& converted_sent_data = BoolToShortInt(sent_data);

        auto&& converted_gathered_data = ReduceOnRootProcessor(converted_sent_data, mpi_operation);

        std::vector<bool> ret;
        ShortIntToBool(std::move(converted_gathered_data), ret);
        return ret;
    }


    namespace // anonymous
    {


        std::string ComputeRankPrefix(const Mpi* const mpi)
        {
            std::ostringstream oconv;
            assert(!(!mpi));
            oconv << '[' << mpi->GetRank<int>() << ']';
            return oconv.str();
        }


    } // namespace


    const std::string& Mpi::GetRankPrefix() const
    {
        static const std::string ret = ComputeRankPrefix(this);
        return ret;
    }


    void Mpi::AbortIfErrorCode(int rank, int error_code, const std::source_location location) const
    {
        if (error_code != MPI_SUCCESS)
        {
            // By implementing a failure on top of an exception, it's easy to change our mind if someday we rather
            // use exception instead of MPI abort. (However if you do so don't forget to handle the special case of
            // Mpi destructor: no exception should be called there).
            const ::MoReFEM::Wrappers::ExceptionNS::Mpi::Exception exception(rank, error_code, location);
            std::cerr << exception.what() << '\n';
            MPI_Abort(GetCommunicator(), error_code);
        }
    }


} // namespace MoReFEM::Wrappers


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
