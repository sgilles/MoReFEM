// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstdlib>

#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"
#include "ThirdParty/Wrappers/Mpi/MacroEncapsulation/Op.hpp"


namespace MoReFEM::Wrappers::MpiNS
{


    PRAGMA_DIAGNOSTIC(push)
    PRAGMA_DIAGNOSTIC(ignored "-Wold-style-cast")

    MPI_Op Operator(Op operation)
    {
        switch (operation)
        {
        case Op::Sum:
            return MPI_SUM;
        case Op::Max:
            return MPI_MAX;
        case Op::Min:
            return MPI_MIN;
        case Op::LogicalOr:
            return MPI_LOR;
        }

        assert(false && "Operation required wasn't correctly match with a native Mpi operation!");
        exit(EXIT_FAILURE);
    }


    PRAGMA_DIAGNOSTIC(pop)


} // namespace MoReFEM::Wrappers::MpiNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
