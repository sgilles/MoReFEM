// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_MPI_INTERNAL_DATATYPE_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_MPI_INTERNAL_DATATYPE_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cassert>
#include <vector>

#include "Utilities/Type/StrongType/StrongType.hpp" // IWYU pragma: export

#include "ThirdParty/IncludeWithoutWarning/Mpi/Mpi.hpp"


namespace MoReFEM::Internal::Wrappers::MpiNS
{


    /*!
     * \brief Associate the appropriate MPI datatype to a given C++ type.
     *
     * For instance:
     *
     * Datatype<unsigned long>::Type() should yield MPI_UNSIGNED_LONG.
     *
     * \tparam T C++ POD type considered.
     *
     * \internal <b><tt>[internal]</tt></b> There is no content on purpose: this class is intended to be
     * specialized for each C++ type handled by MPI.
     * \endinternal
     */
    template<typename T>
    struct Datatype;


} // namespace MoReFEM::Internal::Wrappers::MpiNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ThirdParty/Wrappers/Mpi/Internal/Datatype.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_MPI_INTERNAL_DATATYPE_DOT_HPP_
// *** MoReFEM end header guards *** < //
