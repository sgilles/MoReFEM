// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_LUA_FUNCTION_FUNCTION_DOT_HXX_
#define MOREFEM_THIRDPARTY_WRAPPERS_LUA_FUNCTION_FUNCTION_DOT_HXX_
// IWYU pragma: private, include "ThirdParty/Wrappers/Lua/Function/Function.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "ThirdParty/Wrappers/Lua/Function/Function.hpp"


namespace MoReFEM::Wrappers::Lua
{


    template<typename ReturnTypeT, typename... Args>
    Function<ReturnTypeT(Args...)>::Function()
    { }


    template<typename ReturnTypeT, typename... Args>
    Function<ReturnTypeT(Args...)>::Function(const std::string& content) : state_(Internal::LuaNS::LuaState(content))
    {
        decltype(auto) state = GetInternalState();

        std::ostringstream oconv;
        oconv << "f = " << content; // function is arbitrarily called f.

        if (luaL_dostring(state, oconv.str().c_str()))
            throw Exception("The string with the definition of the function couldn't be interpreted correctly; "
                            "it was: \n"
                                + content + "\n",
                            std::source_location::current());
    }


    template<typename ReturnTypeT, typename... Args>
    inline ReturnTypeT Function<ReturnTypeT(Args...)>::operator()(Args... args) const
    {
        decltype(auto) state = GetInternalState();

        if (state == nullptr)
        {
            assert(state_.GetString() == "");
            std::ostringstream oconv;
            oconv << "You are attempting to compute the value of a Lua function which was not properly "
                     "initialized.";
            throw Exception(oconv.str());
        }

        Internal::LuaNS::PutOnStack(state, "f");

        Internal::LuaNS::PushOnStack(state, std::forward_as_tuple(args...));

        //  int n = lua_gettop(state_);

        const auto size = static_cast<int>(sizeof...(args));

        if (lua_pcall(state, size, LUA_MULTRET, 0) != 0)
        {
            std::ostringstream oconv;
            oconv << "Failure while trying to compute the function with Lua. FYI the Lua stack was at the time "
                     "of the call: \n";
            Internal::LuaNS::LuaStackDump(state, oconv);

            throw Exception(oconv.str());
        }

        /* retrieve result */
        if (!lua_isnumber(state, -1))
            throw Exception("Function `f' must return a number!");

        ReturnTypeT ret = Internal::LuaNS::PullFromStack<ReturnTypeT>(state, -1);

        lua_pop(state, 1);

        return ret;
    }


    template<typename ReturnTypeT, typename... Args>
    inline const std::string& Function<ReturnTypeT(Args...)>::GetString() const noexcept
    {
        return state_.GetString();
    }


    template<typename ReturnTypeT, typename... Args>
    inline lua_State* Function<ReturnTypeT(Args...)>::GetInternalState() const noexcept
    {
        return state_.GetInternal();
    }


} // namespace MoReFEM::Wrappers::Lua


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_LUA_FUNCTION_FUNCTION_DOT_HXX_
// *** MoReFEM end header guards *** < //
