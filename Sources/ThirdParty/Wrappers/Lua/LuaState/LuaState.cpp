// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <sstream>

#include <cstddef> // IWYU pragma: keep
#include <new>
#include <string>

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/Warnings/Pragma.hpp"

#include "ThirdParty/Wrappers/Lua/LuaState/LuaState.hpp"


namespace MoReFEM::Internal::LuaNS
{


    namespace // anonymous
    {


        void OpenState(lua_State* state, const std::string& content);


    } // namespace


    LuaState::~LuaState()
    {
        if (state_ != nullptr) // the condition is not trivial: see the move assignment operator for instance to
                               // convince yourself...
            lua_close(state_);
    }


    LuaState::LuaState() : state_(luaL_newstate())
    {
        PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/zero-as-null-pointer-constant.hpp" // IWYU pragma: keep
        if (state_ == nullptr)
            throw std::bad_alloc();
        PRAGMA_DIAGNOSTIC(pop)

        // > *** MoReFEM Doxygen end of group *** //
        ///@} // \addtogroup ThirdPartyGroup
        // *** MoReFEM Doxygen end of group *** < //
    }


    LuaState::LuaState(const std::string& content) : LuaState()
    {
        content_ = content;
        OpenState(state_, content_);
    }


    LuaState::LuaState(const LuaState& rhs) : LuaState()
    {
        content_ = rhs.content_;
        OpenState(state_, content_);
    }


    LuaState::LuaState(LuaState&& rhs) : LuaState()
    {
        content_ = std::move(rhs.content_);

        OpenState(state_, content_);

        if (rhs.state_ != nullptr)
        {
            lua_close(rhs.state_);
            rhs.state_ = nullptr;
        }

        rhs.content_.clear();
    }


    LuaState& LuaState::operator=(const LuaState& rhs)
    {
        if (this != &rhs)
        {
            if (state_ != nullptr)
                lua_close(state_);

            state_ = luaL_newstate();

            content_ = rhs.content_;
            OpenState(state_, content_);
        }

        return *this;
    }


    LuaState& LuaState::operator=(LuaState&& rhs)
    {
        if (this != &rhs)
        {
            if (state_ != nullptr)
                lua_close(state_);

            state_ = luaL_newstate();

            OpenState(state_, rhs.content_);
            content_ = rhs.content_;

            if (rhs.state_ != nullptr)
            {
                lua_close(rhs.state_);
                rhs.state_ = nullptr;
            }

            rhs.content_ = "";
        }

        return *this;
    }


    namespace // anonymous
    {


        void OpenState(lua_State* state, const std::string& content)
        {
            luaL_openlibs(state);

            if (!content.empty())
            {
                std::ostringstream oconv;
                oconv << "f = " << content; // function is arbitrarily called f.

                if (luaL_dostring(state, oconv.str().c_str()))
                    throw Exception("The string with the definition of the function couldn't be interpreted correctly; "
                                    "it was: \n"
                                    + content + "\n");
            }
        }


    } // namespace


} // namespace MoReFEM::Internal::LuaNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //
