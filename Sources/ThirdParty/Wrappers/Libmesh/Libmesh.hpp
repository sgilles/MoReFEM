// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ThirdPartyGroup
 * \addtogroup ThirdPartyGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_THIRDPARTY_WRAPPERS_LIBMESH_LIBMESH_DOT_HPP_
#define MOREFEM_THIRDPARTY_WRAPPERS_LIBMESH_LIBMESH_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Libmeshb/Libmeshb.hpp"


namespace MoReFEM::Wrappers
{


    /*!
     * \brief Wrapper to call functions of libmesh 5 library.
     *
     * Libmesh5 uses an interface to read geometrical geometric elements:
     *   GmfGetLin(mesh_index, meditCode, &coor[0], &coor[1], ..., &coor[n-1], &label)
     *
     * where mesh_index is the returned code when the Medit file was opened, meditCode est the
     * enumeration value matching the kind of geometrical hape read (for instance Triangles)
     * and coordinates are the coords belonging to the current geometric element.
     *
     * Such a form is clumsy for generic use: we can't for instance store all coordinates in
     * a single vector easily.
     *
     * The current class provides a static template function to do such tasks; the menial
     * task of providing one by one each element of the vector is hence done once here in
     * this class and we shouldn't have to repeat it later.
     *
     * There is a Python script "generateFunctions.py" in current directory that generates
     * the specializations if someday a new geometric elementis introduced with a number of coords not yet handled.
     */

    class Libmesh final
    {
      public:
        /*!
         * \brief Wrapper around libmesh GmfSetLin.
         *
         * \param[in] mesh_index Index provided by libmesh when the output file was opened
         * \param[in] geometric_elt_code Code of the geometric element considered in Medit (for instance
         * 'GmfTriangles') \param[in] coords Index of coords belonging to the geometric element \param[in]
         * label_index Index of the labels to which the geometric elementbelongs to
         */
        template<std::size_t NumberOfCoordsT>
        static void MeditSetLin(libmeshb_int mesh_index,
                                GmfKwdCod geometric_elt_code,
                                const std::vector<int>& coords,
                                int label_index);


        /*!
         * \brief Wrapper around libmesh GmfGetLin.
         *
         * \param[in] mesh_index Index provided by libmesh when the input file was opened
         * \param[in] geometric_elt_code Code of the geometric elementconsidered in Medit (for instance
         * 'GmfTriangles') \param[out] coords Index of coords belonging to the geometric element \param[out]
         * label_index Index of the labels to which the geometric elementbelongs to
         */

        template<std::size_t NumberOfCoordsT>
        static void MeditGetLin(libmeshb_int mesh_index,
                                GmfKwdCod geometric_elt_code,
                                std::vector<std::size_t>& coords,
                                int& label_index);
    };


} // namespace MoReFEM::Wrappers


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ThirdPartyGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_THIRDPARTY_WRAPPERS_LIBMESH_LIBMESH_DOT_HPP_
// *** MoReFEM end header guards *** < //
