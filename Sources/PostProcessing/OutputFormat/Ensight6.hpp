// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_POSTPROCESSING_OUTPUTFORMAT_ENSIGHT6_DOT_HPP_
#define MOREFEM_POSTPROCESSING_OUTPUTFORMAT_ENSIGHT6_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <string>
#include <vector>

#include "Core/NumberingSubset/UniqueId.hpp"

#include "PostProcessing/OutputFormat/Enum.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM::FilesystemNS { class Directory; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::PostProcessingNS::OutputFormat
{


    /*!
     * \brief Generates the output in Ensight 6 format.
     *
     */
    class Ensight6 final
    {


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] data_directory The directory into which outputs of a model were written (this is
         * the field Result/output_directory in the Lua file).
         * \param[in] numbering_subset_id_list List of \a NumberingSubset unique identifiers to consider.
         * \param[in] mesh Mesh upon which the results are to be displayed. Most of the time
         * it is the mesh given in the Lua file; it might also be a refined mesh if you used
         * RefineMeshSpectral(). \param[in] is_mesh_refined \see RefinedMesh for more details. \param[in]
         * unknown_list Define here the list of unknowns for which Ensight output is sought. If empty, all the
         * unknowns found in the output file are taken; if not only the selected subset is taken. The name
         * specified here MUST match some of the unknowns in the unknown file. \param[in] ensight_directory
         * Directory into which the Ensight outputs will be written. If nullptr, a subdirectory named "Ensight6"
         * will be created in the Mesh directory (this should be the default behaviour but I need the other one
         * for some tests). Call site is responsible of the object destruction (should be stored in a smart
         * pointer in the first place).
         */
        explicit Ensight6(const FilesystemNS::Directory& data_directory,
                          const std::vector<std::string>& unknown_list,
                          const std::vector<NumberingSubsetNS::unique_id>& numbering_subset_id_list,
                          const Mesh& mesh,
                          RefinedMesh is_mesh_refined = RefinedMesh::no,
                          const FilesystemNS::Directory* ensight_directory = nullptr);

        //! Destructor.
        ~Ensight6() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Ensight6(const Ensight6& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Ensight6(Ensight6&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Ensight6& operator=(const Ensight6& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Ensight6& operator=(Ensight6&& rhs) = delete;

        ///@}
    };


} // namespace MoReFEM::PostProcessingNS::OutputFormat


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_POSTPROCESSING_OUTPUTFORMAT_ENSIGHT6_DOT_HPP_
// *** MoReFEM end header guards *** < //
