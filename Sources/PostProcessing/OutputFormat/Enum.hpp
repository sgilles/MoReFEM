// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup PostProcessingGroup
 * \addtogroup PostProcessingGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_POSTPROCESSING_OUTPUTFORMAT_ENUM_DOT_HPP_
#define MOREFEM_POSTPROCESSING_OUTPUTFORMAT_ENUM_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM::PostProcessingNS
{


    /*!
     * \brief Enum class that specifies the nature of the mesh using in post-processing.
     *
     * No if the mesh is the one that was present during the run of the model.
     * Yes if it is rather a finer mesh upon which the solution is displayed as if it was Q1.
     *
     * Currently 'no' assumes the model was P1 or Q1, and 'yes' that it was run in sequential.
     */
    enum class RefinedMesh { no, yes };


} // namespace MoReFEM::PostProcessingNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup PostProcessingGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_POSTPROCESSING_OUTPUTFORMAT_ENUM_DOT_HPP_
// *** MoReFEM end header guards *** < //
