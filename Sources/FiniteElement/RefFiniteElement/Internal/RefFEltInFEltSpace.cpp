// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"
#include "FiniteElement/RefFiniteElement/Internal/RefFEltInFEltSpace.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"


namespace MoReFEM::Internal::RefFEltNS
{


    RefFEltInFEltSpace::RefFEltInFEltSpace(const BasicRefFElt& basic_ref_felt,
                                           const ExtendedUnknown& extended_unknown,
                                           const std::size_t mesh_dimension,
                                           const std::size_t felt_space_dimension)
    : basic_ref_felt_(basic_ref_felt), extended_unknown_(extended_unknown), mesh_dimension_(mesh_dimension),
      felt_space_dimension_(felt_space_dimension)
    { }

    RefFEltInFEltSpace::~RefFEltInFEltSpace() = default;


    std::size_t RefFEltInFEltSpace::Ndof() const noexcept
    {
        const auto& unknown = GetExtendedUnknown().GetUnknown();
        const auto Nlocal_node = GetBasicRefFElt().NlocalNode();

        switch (unknown.GetNature())
        {
        case UnknownNS::Nature::scalar:
            return Nlocal_node;
        case UnknownNS::Nature::vectorial:
            return Nlocal_node * GetMeshDimension();
        }

        assert(false);
        return NumericNS::UninitializedIndex<std::size_t>();
    }


    Advanced::ComponentNS::index_type RefFEltInFEltSpace::Ncomponent() const
    {
        const auto unknown_nature = GetExtendedUnknown().GetUnknown().GetNature();

        switch (unknown_nature)
        {
        case UnknownNS::Nature::scalar:
            return Advanced::ComponentNS::index_type{ 1ul };
        case UnknownNS::Nature::vectorial:
            return Advanced::ComponentNS::index_type{ GetMeshDimension() };
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::Internal::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
