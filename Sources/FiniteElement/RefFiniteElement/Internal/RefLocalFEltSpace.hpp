// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_REFLOCALFELTSPACE_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_REFLOCALFELTSPACE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Core/Parameter/TypeEnum.hpp" // IWYU pragma: keep // IWYU doesn't manage gracefully this enum as forward declaration
#include "Core/TimeManager/Concept.hpp" // IWYU pragma: export

#include "Geometry/RefGeometricElt/RefGeomElt.hpp"

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"       // IWYU pragma: export
#include "FiniteElement/RefFiniteElement/Internal/RefFEltInFEltSpace.hpp" // IWYU pragma: export
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { enum class enable_non_linear_solver; }

namespace MoReFEM
{
    class FEltSpace;

    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, std::size_t Ndim>
    class ParamAtDof; // IWYU pragma: keep

    template<class DerivedT, std::size_t SolverIndexT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,  enable_non_linear_solver NonLinearSolverT>
    class VariationalFormulation; // IWYU pragma: keep
}

namespace MoReFEM::Advanced::ConformInterpolatorNS { class SourceOrTargetData; }
namespace MoReFEM::Internal::FEltSpaceNS { class MatchInterfaceNodeBearer; }
namespace MoReFEM::Internal::LocalVariationalOperatorNS { class ElementaryDataImpl; }

namespace MoReFEM::ParameterNS::Policy
{
    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, std::size_t Ndim>
    class AtDof; // IWYU pragma: keep
}

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::RefFEltNS
{


    /*!
     * \brief This class stores the list of all RefFElts that are related to a given RefGeomElt.
     *
     * Its major use is inside the FEltSpace: there will be one RefLocalFEltSpace per RefGeomElt. Within
     * current object, there is indeed a RefFEltInFEltSpace object for each Unknown considered in the FEltSpace.
     */
    class RefLocalFEltSpace final
    {

      public:
        //! Alias to shared pointer over a const object.
        using const_shared_ptr = std::shared_ptr<const RefLocalFEltSpace>;

        //! \cond IGNORE_BLOCK_IN_DOXYGEN


        //! Friendship to MatchInterfaceNodeBearer.
        friend FEltSpaceNS::MatchInterfaceNodeBearer;

        //! Friendship to ElementaryDataImpl.
        friend Internal::LocalVariationalOperatorNS::ElementaryDataImpl;

        //! Friendship.
        friend Advanced::ConformInterpolatorNS::SourceOrTargetData;

        //! Friendship to AtDof parameter policy.
        template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, std::size_t Ndim>
        friend class ::MoReFEM::ParameterNS::Policy::AtDof;

        //! Friendship to AtDof parameter policy.
        template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, std::size_t Ndim>
        friend class ::MoReFEM::ParamAtDof;


        //! Friendship.
        template<class DerivedT,
                 std::size_t SolverIndexT,
                 TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
                 enable_non_linear_solver NonLinearSolverT>
        friend class ::MoReFEM::VariationalFormulation;

        //! \endcond IGNORE_BLOCK_IN_DOXYGEN

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] ref_geom_elt Reference geometric element (e.g. 'Triangle3').
         * \param[in] extended_unknown_list List of all unknowns consider in the FEltSpace. One
         * \a RefFEltInFEltSpace object will be created per unknown.
         * \param[in] mesh_dimension Highest dimension of the mesh onto which the finite element space is built.
         * May be higher than \a felt_space_dimension.
         * \param[in] felt_space_dimension Dimension considered in the finite element space into which new
         * object is built.
         */
        explicit RefLocalFEltSpace(const RefGeomElt::shared_ptr& ref_geom_elt,
                                   const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                   std::size_t mesh_dimension,
                                   std::size_t felt_space_dimension);


        //! Destructor.
        ~RefLocalFEltSpace() = default;

        //! \copydoc doxygen_hide_copy_constructor
        RefLocalFEltSpace(const RefLocalFEltSpace& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        RefLocalFEltSpace(RefLocalFEltSpace&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        RefLocalFEltSpace& operator=(const RefLocalFEltSpace& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        RefLocalFEltSpace& operator=(RefLocalFEltSpace&& rhs) = delete;

        ///@}

        //! Access to the related RefGeomElt.
        const RefGeomElt& GetRefGeomElt() const noexcept;

        //! Returns the number of nodes.
        std::size_t Nnode() const noexcept;

        //! Returns the number of dofs.
        std::size_t Ndof() const noexcept;

        //! Return the RefFEltInFEltSpace list regardless of the unknown.
        const RefFEltInFEltSpace::vector_const_shared_ptr& GetRefFEltList() const noexcept;

        //! Return the RefFEltInFEltSpace related to \a extended_unknown.
        //! \param[in] extended_unknown \a ExtendedUnknown for which reference finite element is sought.
        const RefFEltInFEltSpace& GetRefFElt(const ExtendedUnknown& extended_unknown) const;

        /*!
         * \brief Tells whether one of the \a RefFElt contained here is related to \a numbering_subset
         *
         * \param[in] numbering_subset \a NumberingSubset used in the query.
         *
         * \return True if there is a \a RefFElt that tackles \a numbering_subset
         */
        bool IsInNumberingSubset(const NumberingSubset& numbering_subset) const;

      private:
        //! Return the RefFEltInFEltSpace related to \a unknown in \a felt_space.
        //! \param[in] felt_space \a FEltSpace in which the reference finite element is sought.
        //! \param[in] unknown \a Unknown for which reference finite element is sought.
        const RefFEltInFEltSpace& GetRefFElt(const FEltSpace& felt_space, const Unknown& unknown) const;


      private:
        //! Related geometric element type. It is truly the type of the geometric elements in the mesh.
        const RefGeomElt::shared_ptr ref_geom_elt_;

        /*!
         * \brief List of RefFEltInFEltSpace objects related to current ref_geom_elt_.
         */
        RefFEltInFEltSpace::vector_const_shared_ptr ref_felt_list_;


      private:
        /// \name Cached data.
        ///@{

        //! Number of nodes in the finite element.
        std::size_t Nnode_;

        //! Number of dofs in the finite element.
        std::size_t Ndof_;

        ///@}
    };


} // namespace MoReFEM::Internal::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INTERNAL_REFLOCALFELTSPACE_DOT_HPP_
// *** MoReFEM end header guards *** < //
