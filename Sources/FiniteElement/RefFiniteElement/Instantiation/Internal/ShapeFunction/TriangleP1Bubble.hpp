// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_SHAPEFUNCTION_TRIANGLEP1BUBBLE_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_SHAPEFUNCTION_TRIANGLEP1BUBBLE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>

#include "Geometry/RefGeometricElt/Internal/ShapeFunction/AccessShapeFunction.hpp" // IWYU pragma: export
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"


namespace MoReFEM::Internal::ShapeFunctionNS
{


    /*!
     * \brief Define shape functions of triangle with P1 bubble (additional dof in the interior).
     */
    struct TriangleP1Bubble : public RefGeomEltNS::ShapeFunctionNS::Crtp::AccessShapeFunction<TriangleP1Bubble>
    {


        //! Aliases to avoid magic numbers.
        enum { Nderivate_component_ = 2, Nphi_ = 4, Order = 1 };

        //! Alias to a function that takes a Coords and returns a double.
        using shape_function_type = MoReFEM::RefGeomEltNS::ShapeFunctionType;

        //! Shape functions.
        static const std::array<shape_function_type, Nphi_>& ShapeFunctionList();

        /*!
         * \brief First derivative of the shape functions.
         *
         * Ordering:
         *   \li d(phi[0], r), d(phi[0], s)
         *   \li d(phi[1], r), d(phi[1], s)
         *   etc...
         *
         * \return The derivatives as an array of functions (ordering defined just above)
         */
        static const std::array<shape_function_type, Nphi_ * Nderivate_component_>& FirstDerivateShapeFunctionList();
    };


} // namespace MoReFEM::Internal::ShapeFunctionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/TriangleP1Bubble.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_INTERNAL_SHAPEFUNCTION_TRIANGLEP1BUBBLE_DOT_HPP_
// *** MoReFEM end header guards *** < //
