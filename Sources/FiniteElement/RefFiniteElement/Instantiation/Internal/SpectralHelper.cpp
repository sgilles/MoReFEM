// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>

#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"

#include "FiniteElement/RefFiniteElement/Instantiation/Internal/SpectralHelper.hpp"


namespace MoReFEM::Internal::RefFEltNS
{


    double Interpolation(const std::vector<double>& pos, double p, LocalNodeNS::index_type phi)
    {
        const LocalNodeNS::index_type n{ static_cast<LocalNodeNS::index_type::underlying_type>(pos.size()) };
        double s = 1.;

        assert(phi.Get() < pos.size());
        const double pos_phi = pos[phi.Get()];

        for (LocalNodeNS::index_type r{ 0ul }; r < n; ++r)
        {
            if (r != phi)
            {
                const auto r_value = r.Get();
                s *= (p - pos[r_value]) / (pos_phi - pos[r_value]);
            }
        }

        return s;
    }


    double DerivativeInterpolation(const std::vector<double>& pos, double p, LocalNodeNS::index_type phi)
    {
        double c;
        double s = 0.;
        double d = 1.;
        const LocalNodeNS::index_type n{ static_cast<LocalNodeNS::index_type::underlying_type>(pos.size()) };

        for (LocalNodeNS::index_type r{ 0ul }; r < n; ++r)
        {
            if (r != phi)
                d *= (pos[phi.Get()] - pos[r.Get()]);
        }

        for (LocalNodeNS::index_type r1{ 0ul }; r1 < n; ++r1)
        {
            if (r1 != phi)
            {
                c = 1.;

                for (LocalNodeNS::index_type r2{ 0ul }; r2 < n; ++r2)
                {
                    if (r2 != r1 && r2 != phi)
                        c *= (p - pos[r2.Get()]);
                }

                s += c;
            }
        }
        return s / d;
    }


} // namespace MoReFEM::Internal::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
