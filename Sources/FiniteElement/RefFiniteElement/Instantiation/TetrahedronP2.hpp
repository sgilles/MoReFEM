// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_TETRAHEDRONP2_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_TETRAHEDRONP2_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Geometry/RefGeometricElt/Instances/Tetrahedron/ShapeFunction/Tetrahedron10.hpp"
#include "Geometry/RefGeometricElt/Instances/Tetrahedron/Topology/Tetrahedron.hpp"
#include "Geometry/RefGeometricElt/Instances/Triangle/Topology/Triangle.hpp" // IWYU pragma: keep (IWYU indecisive here)

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp" // IWYU pragma: export

namespace MoReFEM::RefFEltNS
{


    /*!
     * \brief Reference finite element for TetrahedronP2.
     *
     * Numbering convention: local nodes are grouped per nature: first all local nodes on vertices, then
     * the one on edges.
     * Local nodes on vertices are numbered exactly as they were on the RefGeomEltNS::TopologyNS::Tetrahedron class.
     * Local nodes on edges are numbered from Nvertex to Nedge - 1; edge 'i' in
     * RefGeomEltNS::TopologyNS::Tetrahedron traits class is now 'Nvertex + i'.
     *
     * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
     */
    class TetrahedronP2
    // clang-format off
        : public Internal::RefFEltNS::GeometryBasedBasicRefFElt
        <
            RefGeomEltNS::TopologyNS::Tetrahedron,
            RefGeomEltNS::ShapeFunctionNS::Tetrahedron10,
            InterfaceNS::Nature::edge,
            0ul
        >
    // clang-format on
    {

      public:
        //! Name of the shape function used.
        static const std::string& ShapeFunctionLabel();


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit TetrahedronP2() = default;

        //! Destructor.
        ~TetrahedronP2() override;

        //! \copydoc doxygen_hide_copy_constructor
        TetrahedronP2(const TetrahedronP2& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        TetrahedronP2(TetrahedronP2&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        TetrahedronP2& operator=(const TetrahedronP2& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        TetrahedronP2& operator=(TetrahedronP2&& rhs) = default;

        ///@}
    };


} // namespace MoReFEM::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_TETRAHEDRONP2_DOT_HPP_
// *** MoReFEM end header guards *** < //
