// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <string>

#include "FiniteElement/RefFiniteElement/Instantiation/QuadrangleQ1.hpp"


namespace MoReFEM::RefFEltNS
{


    namespace // anonymous
    {

        // #896 obsolete with the correction of the spectral elements.
        //__attribute__((unused)) const bool registered =  //NOLINT
        //    Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance(__FILE__,
        //    __LINE__).Register<QuadrangleQ1>();


    } // namespace


    const std::string& QuadrangleQ1::ShapeFunctionLabel()
    {
        static const std::string ret("Q1");
        return ret;
    }


    QuadrangleQ1::~QuadrangleQ1() = default;


} // namespace MoReFEM::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
