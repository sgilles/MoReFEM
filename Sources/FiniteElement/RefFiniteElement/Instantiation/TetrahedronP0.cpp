// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <string>

#include "FiniteElement/RefFiniteElement/Instantiation/TetrahedronP0.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"


namespace MoReFEM::RefFEltNS
{


    namespace // anonymous
    {


        __attribute__((unused)) const bool registered = // NOLINT
            Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance().Register<TetrahedronP0>();


    } // namespace


    const std::string& TetrahedronP0::ShapeFunctionLabel()
    {
        static const std::string ret("P0");
        return ret;
    }


    TetrahedronP0::~TetrahedronP0() = default;


} // namespace MoReFEM::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
