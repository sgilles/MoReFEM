// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_QUADRANGLEQ0_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_QUADRANGLEQ0_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Geometry/RefGeometricElt/Instances/Quadrangle/Topology/Quadrangle.hpp"

#include "FiniteElement/RefFiniteElement/Instantiation/FwdForCpp.hpp" // IWYU pragma: export
#include "FiniteElement/RefFiniteElement/Instantiation/Internal/GeometryBasedBasicRefFElt.hpp"
#include "FiniteElement/RefFiniteElement/Instantiation/Internal/ShapeFunction/Order0.hpp"


namespace MoReFEM::RefFEltNS
{


    /*!
     * \brief Reference finite element for QuadrangleQ0.
     *
     * Numbering convention: Local nodes (all on vertices) are numbered exactly as they were on the
     * RefGeomEltNS::TopologyNS::Quadrangle class.
     *
     * \extends Internal::RefFEltNS::GeometryBasedBasicRefFElt
     */
    class QuadrangleQ0 : public Internal::RefFEltNS::GeometryBasedBasicRefFElt<RefGeomEltNS::TopologyNS::Quadrangle,
                                                                               Internal::ShapeFunctionNS::Order0,
                                                                               InterfaceNS::Nature::none,
                                                                               1u>
    {

      public:
        //! Name of the shape function used.
        static const std::string& ShapeFunctionLabel();

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit QuadrangleQ0() = default;

        //! Destructor.
        ~QuadrangleQ0() override;

        //! \copydoc doxygen_hide_copy_constructor
        QuadrangleQ0(const QuadrangleQ0& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        QuadrangleQ0(QuadrangleQ0&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        QuadrangleQ0& operator=(const QuadrangleQ0& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        QuadrangleQ0& operator=(QuadrangleQ0&& rhs) = default;

        ///@}
    };


} // namespace MoReFEM::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_QUADRANGLEQ0_DOT_HPP_
// *** MoReFEM end header guards *** < //
