// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <string>

#include "FiniteElement/RefFiniteElement/Instantiation/Point.hpp"
#include "FiniteElement/RefFiniteElement/Internal/BasicRefFEltFactory.hpp"


namespace MoReFEM::RefFEltNS
{


    namespace // anonymous
    {


        __attribute__((unused)) const bool registered_P0 =
            Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance().Register<Point<PointShapeEnum::P0>>();

        __attribute__((unused)) const bool registered_P1 =
            Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance().Register<Point<PointShapeEnum::P1>>();

        __attribute__((unused)) const bool registered_P2 =
            Internal::RefFEltNS::BasicRefFEltFactory::CreateOrGetInstance().Register<Point<PointShapeEnum::P2>>();


    } // namespace


    template<>
    const std::string& Point<PointShapeEnum::P0>::ShapeFunctionLabel()
    {
        static const std::string ret("P0");
        return ret;
    }


    template<>
    const std::string& Point<PointShapeEnum::P1>::ShapeFunctionLabel()
    {
        static const std::string ret("P1");
        return ret;
    }


    template<>
    const std::string& Point<PointShapeEnum::P2>::ShapeFunctionLabel()
    {
        static const std::string ret("P2");
        return ret;
    }


    template<>
    Point<PointShapeEnum::P0>::~Point() = default;


    template<>
    Point<PointShapeEnum::P1>::~Point() = default;


    template<>
    Point<PointShapeEnum::P2>::~Point() = default;


} // namespace MoReFEM::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
