// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_SPECTRAL_DOT_HPP_
#define MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_SPECTRAL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <vector>

#include "Utilities/Miscellaneous.hpp" // IWYU pragma: export

#include "Geometry/RefGeometricElt/Advanced/ComponentIndex.hpp"
#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"
#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"

#include "FiniteElement/RefFiniteElement/Internal/BasicRefFElt.hpp"


namespace MoReFEM::RefFEltNS
{


    /*!
     * \brief Defines a Spectral finite element.
     *
     * \tparam TopologyT One of the following class defined in RefGeomEltNS::TopologyNS namespace:
     * Segment, Quadrangle, Hexahedron.
     * \tparam NI Finite element order in the first direction,
     * \tparam NJ Finite element order in the second direction. Might differ from \a NI if for instance
     * considering Q3-Q2 finite elements.
     * \tparam NK Same as NJ for third direction.
     *
     * Numbering convention: local nodes are numbered by increasing (x,y,z); hence local nodes of different natures
     * are completely intertwined. For instance for a Quadrangle Q3:
     *
     *   12 13 14 15
     *    8  9 10 11
     *    4  5  6  7
     *    0  1  2  3
     *
     * So here vertices are numbered 0, 3, 12, 15, edges 1, 2, 4, 8, 7, 11, 13, 14 and faces 5, 6, 9, 10.
     */
    template<::MoReFEM::Concept::TopologyTraitsClass TopologyT, std::size_t NI, std::size_t NJ, std::size_t NK>
    class Spectral : public Internal::RefFEltNS::BasicRefFElt
    {


      public:
        //! Name of the shape function used.
        static const std::string& ShapeFunctionLabel();


      public:
        //! Alias over Topology Traits class.
        using topology = TopologyT;


        /// \name Special members.
        ///@{

        /*!
         * \brief Private constructor.
         *
         * Privacy is required to ensure proper singleton behaviour.
         */
        explicit Spectral();

      public:
        //! Destructor.
        virtual ~Spectral() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        Spectral(const Spectral& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Spectral(Spectral&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Spectral& operator=(const Spectral& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Spectral& operator=(Spectral&& rhs) = delete;


        ///@}


        /*!
         *
         *  \copydoc doxygen_hide_shape_function
         */
        virtual double ShapeFunction(LocalNodeNS::index_type local_node_index,
                                     const LocalCoords& local_coords) const override final;


        /*!
         *
         *  \copydoc doxygen_hide_first_derivate_shape_function
         */
        virtual double FirstDerivateShapeFunction(LocalNodeNS::index_type local_node_index,
                                                  Advanced::ComponentNS::index_type component,
                                                  const LocalCoords& local_coords) const override final;

        //! \copydoc doxygen_hide_shape_function_order_method
        virtual std::size_t GetOrder() const noexcept override final;


      private:
        /*!
         * \brief Compute the list of all local nodes to feed to Init() method.
         *
         * \internal <b><tt>[internal]</tt></b> This method should not be used outside of constructor; it is not
         * const as it sets some work data attributes.
         * \endinternal
         *
         * \return List of local nodes.
         */
        virtual Advanced::LocalNode::vector_const_shared_ptr ComputeLocalNodeList() override final;


      private:
        /*!
         * \brief A work variable that stores all the first component values in a first container,
         * and so forth up to third component.
         *
         */
        std::array<std::vector<double>, 3> points_per_ijk_;
    };


} // namespace MoReFEM::RefFEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/RefFiniteElement/Instantiation/Spectral.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_REFFINITEELEMENT_INSTANTIATION_SPECTRAL_DOT_HPP_
// *** MoReFEM end header guards *** < //
