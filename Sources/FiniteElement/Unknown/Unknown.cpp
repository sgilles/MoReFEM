// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <string>

#include "FiniteElement/Unknown/Unknown.hpp"


namespace MoReFEM
{


    const std::string& Unknown::ClassName()
    {
        static const std::string ret("Unknown");
        return ret;
    }


    Unknown::Unknown(const std::string& name, const UnknownNS::unique_id unique_id, const UnknownNS::Nature nature)
    : unique_id_parent(unique_id), name_(name), nature_(nature)
    { }


    bool operator<(const Unknown& unknown1, const Unknown& unknown2)
    {
        return unknown1.GetUniqueId() < unknown2.GetUniqueId();
    }


    bool operator==(const Unknown& unknown1, const Unknown& unknown2)
    {
        return unknown1.GetUniqueId() == unknown2.GetUniqueId();
    }


    std::size_t Ncomponent(const Unknown& unknown, const std::size_t mesh_dimension)
    {

        switch (unknown.GetNature())
        {
        case UnknownNS::Nature::scalar:
            return 1u;
        case UnknownNS::Nature::vectorial:
            return mesh_dimension;
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
