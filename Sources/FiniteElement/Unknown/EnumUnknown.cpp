// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <ostream>

#include "FiniteElement/Unknown/EnumUnknown.hpp"


namespace MoReFEM::UnknownNS
{


    std::ostream& operator<<(std::ostream& stream, const Nature nature)
    {
        switch (nature)
        {
        case Nature::scalar:
            stream << "scalar";
            break;
        case Nature::vectorial:
            stream << "vectorial";
            break;
        }

        return stream;
    }


} // namespace MoReFEM::UnknownNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
