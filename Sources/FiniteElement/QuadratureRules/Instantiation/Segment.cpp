// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Warnings/Pragma.hpp"

#include "FiniteElement/QuadratureRules/Instantiation/FwdForCpp.hpp" // IWYU pragma: keep
#include "FiniteElement/QuadratureRules/Instantiation/Segment.hpp"


namespace MoReFEM::QuadratureNS
{


    namespace // anonymous
    {


        const std::array<QuadratureRule::const_shared_ptr, 3> CreateQuadratureRuleListPerDegreeOfExactness();


    } // namespace


    const std::array<QuadratureRule::const_shared_ptr, 3>& Segment::GetPerDegreeOfExactnessList()
    {
        static const std::array<QuadratureRule::const_shared_ptr, 3> ret =
            CreateQuadratureRuleListPerDegreeOfExactness();

        return ret;
    }


    PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/return-std-move-in-c++11.hpp" // IWYU pragma: keep
    QuadratureRule::const_shared_ptr Segment::GetShapeFunctionOrder(std::size_t order)
    {
        auto ret = std::make_shared<QuadratureRule>(std::string("segment_") + std::to_string(order) + "_points",
                                                    TopologyNS::Type::segment);

        {
            auto& n_points = *ret;

            std::vector<double> points;
            std::vector<double> weights;

            const std::size_t Ngauss_points = order + 1u;
            QuadratureNS::ComputeGaussFormulas<QuadratureNS::GaussFormula::Gauss>(Ngauss_points, points, weights);

            const auto Npoints = points.size();

            for (auto i = 0ul; i < Npoints; ++i)
                n_points.AddQuadraturePoint(LocalCoords({ points[i] }), weights[i]);
        }

        return ret;
    }
    PRAGMA_DIAGNOSTIC(pop)


    namespace // anonymous
    {


        const std::array<QuadratureRule::const_shared_ptr, 3> CreateQuadratureRuleListPerDegreeOfExactness()
        {

            auto one_point_ptr = std::make_shared<QuadratureRule>("segment_1_point", TopologyNS::Type::segment, 1);

            one_point_ptr->AddQuadraturePoint(LocalCoords({ 0. }), 2.);


            auto two_points_ptr = std::make_shared<QuadratureRule>("segment_2_points", TopologyNS::Type::segment, 3);

            {
                auto& two_points = *two_points_ptr;
                const double alpha = std::sqrt(1. / 3.);
                two_points.AddQuadraturePoint(LocalCoords({ -alpha }), 1.);
                two_points.AddQuadraturePoint(LocalCoords({ alpha }), 1.);
            }


            auto three_points_ptr = std::make_shared<QuadratureRule>("segment_3_points", TopologyNS::Type::segment, 4);


            {
                auto& three_points = *three_points_ptr;

                const double alpha = std::sqrt(3. / 5.);
                const double one_ninth = 1. / 9.;
                const double five_ninth = 5. * one_ninth;

                three_points.AddQuadraturePoint(LocalCoords({ 0. }), 8. * one_ninth);
                three_points.AddQuadraturePoint(LocalCoords({ -alpha }), five_ninth);
                three_points.AddQuadraturePoint(LocalCoords({ alpha }), five_ninth);
            }


            assert(one_point_ptr->NquadraturePoint() == 1u);
            assert(two_points_ptr->NquadraturePoint() == 2u);
            assert(three_points_ptr->NquadraturePoint() == 3u);

            return { { one_point_ptr, two_points_ptr, three_points_ptr } };
        }


    } // namespace


} // namespace MoReFEM::QuadratureNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
