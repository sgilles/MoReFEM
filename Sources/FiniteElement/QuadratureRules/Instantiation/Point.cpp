// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <initializer_list>

#include "FiniteElement/QuadratureRules/Instantiation/FwdForCpp.hpp" // IWYU pragma: keep
#include "FiniteElement/QuadratureRules/Instantiation/Point.hpp"


namespace MoReFEM::QuadratureNS
{


    namespace // anonymous
    {


        const std::array<QuadratureRule::const_shared_ptr, 1> CreateQuadratureRuleListPerDegreeOfExactness()
        {
            auto one_point_ptr = std::make_shared<QuadratureRule>("point", TopologyNS::Type::point, 1);
            one_point_ptr->AddQuadraturePoint(LocalCoords({}), 1.);


            return { { one_point_ptr } };
        }


    } // namespace


    const std::array<QuadratureRule::const_shared_ptr, 1>& Point::GetPerDegreeOfExactnessList()
    {
        static const std::array<QuadratureRule::const_shared_ptr, 1> ret =
            CreateQuadratureRuleListPerDegreeOfExactness();

        return ret;
    }


} // namespace MoReFEM::QuadratureNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
