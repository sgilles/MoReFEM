// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Warnings/Pragma.hpp"

#include "FiniteElement/QuadratureRules/Instantiation/FwdForCpp.hpp" // IWYU pragma: keep
#include "FiniteElement/QuadratureRules/Instantiation/Quadrangle.hpp"


namespace MoReFEM::QuadratureNS
{


    namespace // anonymous
    {


        const std::array<QuadratureRule::const_shared_ptr, 3> CreateQuadratureRuleListPerDegreeOfExactness();


    } // namespace


    const std::array<QuadratureRule::const_shared_ptr, 3>& Quadrangle::GetPerDegreeOfExactnessList()
    {
        static const std::array<QuadratureRule::const_shared_ptr, 3> ret =
            CreateQuadratureRuleListPerDegreeOfExactness();

        return ret;
    }


    PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/return-std-move-in-c++11.hpp" // IWYU pragma: keep
    QuadratureRule::const_shared_ptr Quadrangle::GetShapeFunctionOrder(std::size_t order)
    {
        auto ret = std::make_shared<QuadratureRule>(std::string("quadrangle_") + std::to_string(order) + "_points",
                                                    TopologyNS::Type::quadrangle);

        {
            auto& n_points = *ret;

            std::vector<double> points;
            std::vector<double> weights;

            const std::size_t Ngauss_points = order + 1u;
            QuadratureNS::ComputeGaussFormulas<QuadratureNS::GaussFormula::Gauss>(Ngauss_points, points, weights);

            const auto Npoints = points.size();

            for (auto i = 0ul; i < Npoints; ++i)
            {
                for (auto j = 0ul; j < Npoints; ++j)
                    n_points.AddQuadraturePoint(LocalCoords({ points[j], points[i] }), weights[i] * weights[j]);
            }
        }

        return ret;
    }
    PRAGMA_DIAGNOSTIC(pop)


    namespace // anonymous
    {


        const std::array<QuadratureRule::const_shared_ptr, 3> CreateQuadratureRuleListPerDegreeOfExactness()
        {

            auto one_point_ptr =
                std::make_shared<QuadratureRule>("quadrangle_1_point", TopologyNS::Type::quadrangle, 1);
            {
                one_point_ptr->AddQuadraturePoint(LocalCoords({ 0., 0. }), .5);
            }


            auto four_points_ptr =
                std::make_shared<QuadratureRule>("quadrangle_4_points", TopologyNS::Type::quadrangle, 3);

            {
                auto& four_points = *four_points_ptr;

                const double q2ptx1 = -std::sqrt(1. / 3.);
                const double q2ptx2 = -q2ptx1;

                four_points.AddQuadraturePoint(LocalCoords({ q2ptx1, q2ptx1 }), 1.);
                four_points.AddQuadraturePoint(LocalCoords({ q2ptx2, q2ptx1 }), 1.);
                four_points.AddQuadraturePoint(LocalCoords({ q2ptx2, q2ptx2 }), 1.);
                four_points.AddQuadraturePoint(LocalCoords({ q2ptx1, q2ptx2 }), 1.);
            }


            auto nine_points_ptr =
                std::make_shared<QuadratureRule>("quadrangle_9_points", TopologyNS::Type::quadrangle, 5);

            {
                auto& nine_points = *nine_points_ptr;

                const double q3ptx1 = 0., q3ptx2 = -std::sqrt(3. / 5.), q3ptx3 = -q3ptx2;
                const double one_ninth = 1. / 9.;

                const double q3ptw1 = 8 * one_ninth;
                const double q3ptw2 = 5 * one_ninth;
                const double q3ptw3 = 5 * one_ninth;

                nine_points.AddQuadraturePoint(LocalCoords({ q3ptx1, q3ptx1 }), q3ptw1 * q3ptw1);
                nine_points.AddQuadraturePoint(LocalCoords({ q3ptx2, q3ptx1 }), q3ptw2 * q3ptw1);
                nine_points.AddQuadraturePoint(LocalCoords({ q3ptx3, q3ptx1 }), q3ptw3 * q3ptw1);
                nine_points.AddQuadraturePoint(LocalCoords({ q3ptx1, q3ptx2 }), q3ptw1 * q3ptw2);
                nine_points.AddQuadraturePoint(LocalCoords({ q3ptx2, q3ptx2 }), q3ptw2 * q3ptw2);
                nine_points.AddQuadraturePoint(LocalCoords({ q3ptx3, q3ptx2 }), q3ptw3 * q3ptw2);
                nine_points.AddQuadraturePoint(LocalCoords({ q3ptx1, q3ptx3 }), q3ptw1 * q3ptw3);
                nine_points.AddQuadraturePoint(LocalCoords({ q3ptx2, q3ptx3 }), q3ptw2 * q3ptw3);
                nine_points.AddQuadraturePoint(LocalCoords({ q3ptx3, q3ptx3 }), q3ptw3 * q3ptw3);
            }

            assert(one_point_ptr->NquadraturePoint() == 1u);
            assert(four_points_ptr->NquadraturePoint() == 4u);
            assert(nine_points_ptr->NquadraturePoint() == 9u);

            return { { one_point_ptr, four_points_ptr, nine_points_ptr } };
        }


    } // namespace


} // namespace MoReFEM::QuadratureNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
