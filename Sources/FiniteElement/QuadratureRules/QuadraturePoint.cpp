// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Geometry/Coords/LocalCoords.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"


namespace MoReFEM
{


    QuadraturePoint::QuadraturePoint(LocalCoords&& local_coords,
                                     double weight,
                                     const std::string& rule_name,
                                     const std::size_t index)
    : LocalCoords(std::move(local_coords)), weight_(weight), rule_name_(rule_name), index_(index)
    { }


    QuadraturePoint::~QuadraturePoint() = default;


    void QuadraturePoint::Print(std::ostream& out) const
    {
        LocalCoords::Print(out);
        out << " (weight = " << GetWeight() << ')';
    }


    std::ostream& operator<<(std::ostream& stream, const QuadraturePoint& point)
    {
        point.Print(stream);
        return stream;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
