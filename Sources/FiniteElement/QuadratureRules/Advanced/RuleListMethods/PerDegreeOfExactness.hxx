// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_QUADRATURERULES_ADVANCED_RULELISTMETHODS_PERDEGREEOFEXACTNESS_DOT_HXX_
#define MOREFEM_FINITEELEMENT_QUADRATURERULES_ADVANCED_RULELISTMETHODS_PERDEGREEOFEXACTNESS_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/QuadratureRules/Advanced/RuleListMethods/PerDegreeOfExactness.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Advanced::QuadratureRuleNS
{


    template<class DerivedT>
    std::size_t PerDegreeOfExactness<DerivedT>::Nrule()
    {
        return DerivedT::GetPerDegreeOfExactnessList().size();
    }


    template<class DerivedT>
    std::size_t PerDegreeOfExactness<DerivedT>::MaximumDegreeOfExactness()
    {
        // As the rules are assumed to be sort in increasing order, this is the degree of the last element.
        const auto& list = DerivedT::GetPerDegreeOfExactnessList();
        assert(!list.empty());

        assert(std::is_sorted(list.cbegin(),
                              list.cend(),
                              [](const QuadratureRule& rule1, const QuadratureRule& rule2)
                              {
                                  return rule1.DegreeOfExactness() < rule2.DegreeOfExactness();
                              }));

        return list.back().DegreeOfExactness();
    }


    template<class DerivedT>
    const QuadratureRule::const_shared_ptr&
    PerDegreeOfExactness<DerivedT>::GetRuleFromDegreeOfExactness(std::size_t degree)
    {
        const auto& list = DerivedT::GetPerDegreeOfExactnessList();
        assert(!list.empty());

        auto sorting_rule = [](const QuadratureRule::const_shared_ptr& lhs, const QuadratureRule::const_shared_ptr& rhs)
        {
            assert(!(!lhs));
            assert(!(!rhs));
            return lhs->DegreeOfExactness() < rhs->DegreeOfExactness();
        };

        // There is a strong assumption in the class that the rules are sort in increasing order; this is
        // checked here.
        assert(std::is_sorted(list.cbegin(), list.cend(), sorting_rule));

        auto dummy = std::make_shared<const QuadratureRule>((*list.cbegin())->GetTopologyIdentifier(), degree);

        auto it = std::lower_bound(list.cbegin(), list.cend(), dummy, sorting_rule);

        // If the requested degree of exactness is too high, take the biggest available.
        if (it == list.cend())
            return *(list.crbegin());

        // throw ExceptionNS::QuadratureRuleListNS::InvalidDegree(degree, MaximumDegreeOfExactness(), __FILE__,
        // __LINE__);

        return *it;
    }


} // namespace MoReFEM::Advanced::QuadratureRuleNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_QUADRATURERULES_ADVANCED_RULELISTMETHODS_PERDEGREEOFEXACTNESS_DOT_HXX_
// *** MoReFEM end header guards *** < //
