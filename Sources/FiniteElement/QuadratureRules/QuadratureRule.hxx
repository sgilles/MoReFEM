// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_QUADRATURERULES_QUADRATURERULE_DOT_HXX_
#define MOREFEM_FINITEELEMENT_QUADRATURERULES_QUADRATURERULE_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/QuadratureRules/QuadratureRule.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "FiniteElement/QuadratureRules/QuadratureRule.hpp"


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>

#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"


namespace MoReFEM
{


    inline std::size_t QuadratureRule::NquadraturePoint() const noexcept
    {
        assert(!point_list_.empty());
        return point_list_.size();
    }


    inline const QuadraturePoint& QuadratureRule::Point(std::size_t index) const noexcept
    {
        assert(!point_list_.empty());
        assert(index < point_list_.size());
        assert(!(!point_list_[index]));
        return *(point_list_[index]);
    }


    inline std::size_t QuadratureRule::DegreeOfExactness() const noexcept
    {
        assert(degree_of_exactness_ != NumericNS::UninitializedIndex<std::size_t>()
               && "Should be called only if it is relevant for the law!");
        return degree_of_exactness_;
    }


    inline TopologyNS::Type QuadratureRule::GetTopologyIdentifier() const noexcept
    {
        return topology_id_;
    }


    inline const std::string& QuadratureRule::GetName() const noexcept
    {
        assert(!name_.empty());
        return name_;
    }


    inline const QuadraturePoint::vector_const_shared_ptr& QuadratureRule::GetQuadraturePointList() const noexcept
    {
        assert(!point_list_.empty());
        return point_list_;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_QUADRATURERULES_QUADRATURERULE_DOT_HXX_
// *** MoReFEM end header guards *** < //
