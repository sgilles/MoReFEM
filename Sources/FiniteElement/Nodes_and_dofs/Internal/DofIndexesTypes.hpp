// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_NODES_AND_DOFS_INTERNAL_DOFINDEXESTYPES_DOT_HPP_
#define MOREFEM_FINITEELEMENT_NODES_AND_DOFS_INTERNAL_DOFINDEXESTYPES_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Type/StrongType/Skills/Addable.hpp"              // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/AsMpiDatatype.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"           // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp" // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Hashable.hpp"             // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Printable.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/StrongType.hpp"                  // IWYU pragma: export


namespace MoReFEM::Internal::DofNS
{

    /*!
     * \brief Strong type to represent an internal processor-wise or ghost index for a \a Dof.
     */
    // clang-format off
    using internal_processor_wise_or_ghost_index =
    StrongType
    <
        std::size_t,
        struct InternalProcessorWiseOrGhostIndexTypeTag,
        ::MoReFEM::StrongTypeNS::Comparable,
        ::MoReFEM::StrongTypeNS::Hashable,
        ::MoReFEM::StrongTypeNS::DefaultConstructible,
        ::MoReFEM::StrongTypeNS::Printable,
        ::MoReFEM::StrongTypeNS::AsMpiDatatype,
        ::MoReFEM::StrongTypeNS::Addable
    >;
    // clang-format on


} // namespace MoReFEM::Internal::DofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_NODES_AND_DOFS_INTERNAL_DOFINDEXESTYPES_DOT_HPP_
// *** MoReFEM end header guards *** < //
