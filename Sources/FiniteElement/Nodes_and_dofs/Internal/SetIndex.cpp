// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include "FiniteElement/Nodes_and_dofs/Internal/SetIndex.hpp"
#include "FiniteElement/Nodes_and_dofs/Dof.hpp"
// IWYU pragma: no_include "SetIndex.hpp"

namespace MoReFEM::Internal::DofNS
{


    void SetIndex::ProgramWiseIndex(const NumberingSubset& numbering_subset,
                                    ::MoReFEM::DofNS::program_wise_index index,
                                    Dof& dof)
    {
        dof.SetProgramWiseIndex(numbering_subset, index);
    }


    void SetIndex::ProcessorWiseOrGhostIndex(const NumberingSubset& numbering_subset,
                                             ::MoReFEM::DofNS::processor_wise_or_ghost_index index,
                                             Dof& dof)
    {
        dof.SetProcessorWiseOrGhostIndex(numbering_subset, index);
    }


    void SetIndex::InternalProcessorWiseOrGhostIndex(
        ::MoReFEM::Internal::DofNS::internal_processor_wise_or_ghost_index index,
        Dof& dof)
    {
        dof.SetInternalProcessorWiseOrGhostIndex(index);
    }


} // namespace MoReFEM::Internal::DofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
