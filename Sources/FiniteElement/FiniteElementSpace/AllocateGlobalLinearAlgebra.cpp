// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/MatrixPattern.hpp"

#include "Core/LinearAlgebra/GlobalDiagonalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"


namespace MoReFEM
{


    void AllocateGlobalMatrix(const GodOfDof& god_of_dof, GlobalMatrix& matrix)
    {
        const auto& row_numbering_subset = matrix.GetRowNumberingSubset();
        const auto& col_numbering_subset = matrix.GetColNumberingSubset();

        const auto& matrix_pattern = god_of_dof.GetMatrixPattern(row_numbering_subset, col_numbering_subset);

        const auto& mpi = god_of_dof.GetMpi();

        const std::size_t Nrow_processor_wise_dof = god_of_dof.NprocessorWiseDof(row_numbering_subset);
        const std::size_t Ncolumn_processor_wise_dof = god_of_dof.NprocessorWiseDof(col_numbering_subset);

        const std::size_t Nproc = mpi.template Nprocessor<std::size_t>();

        if (Nproc == 1)
        {
            matrix.InitSequentialMatrix(Nrow_processor_wise_dof, Ncolumn_processor_wise_dof, matrix_pattern, mpi);
        } else
        {
            const std::size_t Nrow_program_wise_dof = god_of_dof.NprogramWiseDof(row_numbering_subset);
            const std::size_t Ncolumn_program_wise_dof = god_of_dof.NprogramWiseDof(col_numbering_subset);

            matrix.InitParallelMatrix(Nrow_processor_wise_dof,
                                      Ncolumn_processor_wise_dof,
                                      Nrow_program_wise_dof,
                                      Ncolumn_program_wise_dof,
                                      matrix_pattern,
                                      mpi);
        }

        matrix.ZeroEntries();
    }


    void AllocateGlobalMatrix(const GodOfDof& god_of_dof, GlobalDiagonalMatrix& matrix)
    {
        const auto& numbering_subset = matrix.GetRowNumberingSubset();
        assert(numbering_subset == matrix.GetColNumberingSubset() && "Square matrix expected!");

        // ===================================================
        // Determine the (simplistic) matrix pattern of the diagonal matrix.
        // ===================================================
        const auto Nrow_proc_wise = god_of_dof.NprocessorWiseDof(numbering_subset);
        std::vector<std::vector<PetscInt>> non_zero_slots_per_local_row(Nrow_proc_wise);

        const auto& dof_list = god_of_dof.GetProcessorWiseDofList();

        for (const auto& dof_ptr : dof_list)
        {
            assert(!(!dof_ptr));
            const auto& dof = *dof_ptr;

            if (!dof.IsInNumberingSubset(numbering_subset))
                continue;

            const auto proc_wise_index = dof.GetProcessorWiseOrGhostIndex(numbering_subset);
            assert(proc_wise_index < DofNS::processor_wise_or_ghost_index{ Nrow_proc_wise });
            assert(non_zero_slots_per_local_row[proc_wise_index.Get()].empty() && "Each should be filled once!");

            const auto program_wise_index = static_cast<PetscInt>(dof.GetProgramWiseIndex(numbering_subset).Get());
            non_zero_slots_per_local_row[proc_wise_index.Get()] = { program_wise_index };
        }

        Wrappers::Petsc::MatrixPattern diagonal_pattern(non_zero_slots_per_local_row);

        // ===================================================
        // Now fill the structure of the matrix as in AllocateGlobalMatrix().
        // ===================================================

        const auto& mpi = god_of_dof.GetMpi();

        const std::size_t Nproc = mpi.template Nprocessor<std::size_t>();

        if (Nproc == 1)
        {
            matrix.InitSequentialMatrix(Nrow_proc_wise, Nrow_proc_wise, diagonal_pattern, mpi);
        } else
        {
            const std::size_t Nrow_program_wise = god_of_dof.NprogramWiseDof(numbering_subset);

            matrix.InitParallelMatrix(
                Nrow_proc_wise, Nrow_proc_wise, Nrow_program_wise, Nrow_program_wise, diagonal_pattern, mpi);
        }

        matrix.ZeroEntries();
    }


    void AllocateGlobalVector(const GodOfDof& god_of_dof, GlobalVector& vector)
    {
        const auto& mpi = god_of_dof.GetMpi();

        const auto& numbering_subset = vector.GetNumberingSubset();

        const std::size_t Nprocessor_wise_dof = god_of_dof.NprocessorWiseDof(numbering_subset);

        const std::size_t Nproc = mpi.template Nprocessor<std::size_t>();

        if (Nproc == 1)
            vector.InitSequentialVector(mpi, Nprocessor_wise_dof);
        else
        {
            std::vector<PetscInt> ghost_dof_index_list;

            const std::size_t Nprogram_wise_dof = god_of_dof.NprogramWiseDof(numbering_subset);

            {
                const auto& ghost_dof_list = god_of_dof.GetGhostDofList();
                ghost_dof_index_list.reserve(ghost_dof_list.size());

                for (const auto& dof_ptr : ghost_dof_list)
                {
                    assert((!(!dof_ptr)));
                    const auto& dof = *dof_ptr;

                    if (dof.IsInNumberingSubset(numbering_subset))
                    {
                        ghost_dof_index_list.push_back(
                            static_cast<PetscInt>(dof.GetProgramWiseIndex(numbering_subset).Get()));
                    }
                }
            }

            assert(std::is_sorted(ghost_dof_index_list.cbegin(), ghost_dof_index_list.cend()));

            vector.InitMpiVectorWithGhost(mpi, Nprocessor_wise_dof, Nprogram_wise_dof, ghost_dof_index_list);
        }

        vector.ZeroEntries();
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
