// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_GODOFDOF_DOT_HXX_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_GODOFDOF_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <filesystem>
#include <map>
#include <memory> // IWYU pragma: keep
#include <string>

#include "Utilities/Containers/PointerComparison.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"
#include "ThirdParty/Wrappers/Lua/OptionFile/OptionFile.hpp"

#include "Core/InputData/Instances/Parallelism/Parallelism.hpp"
#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/MoReFEMData/Advanced/Concept.hpp"
#include "Core/MoReFEMData/Extract.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryCondition.hpp"
#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalVector; }
namespace MoReFEM { class Mesh; }
namespace MoReFEM::Wrappers::Petsc { class MatrixPattern; }
namespace MoReFEM { enum class DoConsiderProcessorWiseLocal2Global; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    void GodOfDof::InitFromPreprocessedData(const MoReFEMDataT& morefem_data)
    {
        assert("This method is checked at compile time even for MoReFEMDataT without Parallelism block, which is "
               "required below. Hence this runtime check here..."
               && MoReFEMDataT::HasParallelismField());

        if constexpr (MoReFEMDataT::HasParallelismField()) // see assert explanation above
        {
            decltype(auto) prepartitioned_data_dir_str =
                ::MoReFEM::InputDataNS::ExtractLeafAsPath<InputDataNS::Parallelism::Directory>(morefem_data);

            decltype(auto) mpi = GetMpi();

            FilesystemNS::Directory prepartitioned_data_dir(
                mpi, prepartitioned_data_dir_str, FilesystemNS::behaviour::read);

            FilesystemNS::Directory mesh_subdir(prepartitioned_data_dir, "Mesh_" + std::to_string(GetUniqueId().Get()));

            const auto god_of_dof_prepartitioned_data_file = mesh_subdir.AddFile("god_of_dof_data.lua");

            ::MoReFEM::Wrappers::Lua::OptionFile god_of_dof_prepartitioned_data(god_of_dof_prepartitioned_data_file);

            InitFromPreprocessedDataHelper(god_of_dof_prepartitioned_data);
        }
    }


    inline const Mesh& GodOfDof::GetMesh() const noexcept
    {
        return mesh_;
    }


    inline Mesh& GodOfDof::GetNonCstMesh() noexcept
    {
        return mesh_;
    }


    inline const FEltSpace::vector_unique_ptr& GodOfDof::GetFEltSpaceList() const noexcept
    {
        return felt_space_list_;
    }


    inline const NodeBearer::vector_shared_ptr& GodOfDof::GetProcessorWiseNodeBearerList() const noexcept
    {
        return processor_wise_node_bearer_list_;
    }


    inline NodeBearer::vector_shared_ptr& GodOfDof::GetNonCstProcessorWiseNodeBearerList() noexcept
    {
        return processor_wise_node_bearer_list_;
    }


    inline NodeBearer::vector_shared_ptr& GodOfDof::GetNonCstGhostNodeBearerList() noexcept
    {
        return ghost_node_bearer_list_;
    }


    inline const NodeBearer::vector_shared_ptr& GodOfDof::GetGhostNodeBearerList() const noexcept
    {
        return ghost_node_bearer_list_;
    }


    inline std::size_t GodOfDof::NprogramWiseDof() const noexcept
    {
        return GetNdofHolder().NprogramWiseDof();
    }


    inline const Dof::vector_shared_ptr& GodOfDof::GetProcessorWiseDofList() const noexcept
    {
        return processor_wise_dof_list_;
    }


    inline const Dof::vector_shared_ptr& GodOfDof::GetGhostDofList() const noexcept
    {
        return ghost_dof_list_;
    }


    inline std::size_t GodOfDof::NprocessorWiseDof() const noexcept
    {
        assert(GetNdofHolder().NprocessorWiseDof() == processor_wise_dof_list_.size());
        return GetNdofHolder().NprocessorWiseDof();
    }


    inline std::size_t GodOfDof::NprocessorWiseDof(const NumberingSubset& numbering_subset) const
    {
        return GetNdofHolder().NprocessorWiseDof(numbering_subset);
    }


    inline std::size_t GodOfDof::NprogramWiseDof(const NumberingSubset& numbering_subset) const
    {
        return GetNdofHolder().NprogramWiseDof(numbering_subset);
    }


    inline std::size_t GodOfDof::NprocessorWiseNodeBearer() const noexcept
    {
        return processor_wise_node_bearer_list_.size();
    }


    inline GodOfDof::shared_ptr GodOfDof::GetSharedPtr()
    {
        return shared_from_this();
    }


#ifndef NDEBUG
    inline bool GodOfDof::HasInitBeenCalled() const
    {
        return has_init_been_called_;
    }
#endif // NDEBUG


    inline Dof::vector_shared_ptr& GodOfDof::GetNonCstProcessorWiseDofList() noexcept
    {
        return processor_wise_dof_list_;
    }


    inline Dof::vector_shared_ptr& GodOfDof::GetNonCstGhostDofList() noexcept
    {
        return ghost_dof_list_;
    }


    inline const NumberingSubset::vector_const_shared_ptr& GodOfDof::GetNumberingSubsetList() const noexcept
    {
        assert(!numbering_subset_list_.empty());
        assert(std::is_sorted(numbering_subset_list_.cbegin(),
                              numbering_subset_list_.cend(),
                              Utilities::PointerComparison::Less<NumberingSubset::const_shared_ptr>()));

        return numbering_subset_list_;
    }


    template<Internal::GodOfDofNS::wildcard_for_rank is_wildcard>
    inline const FilesystemNS::Directory& GodOfDof::GetOutputDirectory() const noexcept
    {
        if constexpr (is_wildcard == Internal::GodOfDofNS::wildcard_for_rank::no)
        {
            assert(!(!output_directory_storage_));
            return output_directory_storage_->GetOutputDirectory();
        } else
        {
            assert(!(!output_directory_wildcard_storage_));
            return output_directory_wildcard_storage_->GetOutputDirectory();
        }
    }


    inline const Internal::FEltSpaceNS::NdofHolder& GodOfDof::GetNdofHolder() const noexcept
    {
        assert(!(!Ndof_holder_));
        return *Ndof_holder_;
    }


    inline const Wrappers::Petsc::MatrixPattern&
    GodOfDof ::GetMatrixPattern(const NumberingSubset& numbering_subset) const
    {
        return GetMatrixPattern(numbering_subset, numbering_subset);
    }


    inline const NumberingSubset& GodOfDof::GetNumberingSubset(NumberingSubsetNS::unique_id unique_id) const
    {
        return *(GetNumberingSubsetPtr(unique_id));
    }


#ifndef NDEBUG
    inline DoConsiderProcessorWiseLocal2Global GodOfDof::GetDoConsiderProcessorWiseLocal2Global() const
    {
        return do_consider_proc_wise_local_2_global_;
    }

#endif // NDEBUG


    template<BoundaryConditionMethod BoundaryConditionMethodT>
    void GodOfDof ::ApplyBoundaryCondition(const DirichletBoundaryCondition& boundary_condition,
                                           GlobalMatrix& matrix) const
    {
        // These lines are because currently the ghosts dofs are present in the dof list, but for some
        // operations upon the matrices it is not allowed with the default behaviour of MAT_NO_OFF_PROC_ZERO_ROWS.
        // See in the upcoming refactoring of boundary conditions (#1581) if we need to keep this or if we can do things
        // more cleverly.
        PetscBool no_ghost_rows_allowed;
        matrix.GetOption(MAT_NO_OFF_PROC_ZERO_ROWS, &no_ghost_rows_allowed);
        matrix.SetOption(MAT_NO_OFF_PROC_ZERO_ROWS, PETSC_FALSE);

        switch (BoundaryConditionMethodT)
        {
        case BoundaryConditionMethod::pseudo_elimination:
            ApplyPseudoElimination(boundary_condition, matrix);
            break;
        case BoundaryConditionMethod::penalization:
            ApplyPenalization(boundary_condition, matrix);
            break;
        }

        matrix.SetOption(MAT_NO_OFF_PROC_ZERO_ROWS, no_ghost_rows_allowed);
    }


    template<BoundaryConditionMethod BoundaryConditionMethodT>
    void GodOfDof ::ApplyBoundaryCondition(const DirichletBoundaryCondition& boundary_condition,
                                           GlobalVector& vector) const
    {
        switch (BoundaryConditionMethodT)
        {
        case BoundaryConditionMethod::pseudo_elimination:
            ApplyPseudoElimination(boundary_condition, vector);
            break;
        case BoundaryConditionMethod::penalization:
            ApplyPenalization(boundary_condition, vector);
            break;
        }
    }


    template<Internal::GodOfDofNS::wildcard_for_rank is_wildcard>
    inline const FilesystemNS::Directory&
    GodOfDof::GetOutputDirectoryForNumberingSubset(const NumberingSubset& numbering_subset) const
    {
        if constexpr (is_wildcard == Internal::GodOfDofNS::wildcard_for_rank::yes)
        {
            assert(!(!output_directory_wildcard_storage_));
            return output_directory_wildcard_storage_->GetOutputDirectoryForNumberingSubset(numbering_subset);
        } else
        {
            assert(!(!output_directory_storage_));
            return output_directory_storage_->GetOutputDirectoryForNumberingSubset(numbering_subset);
        }
    }


    inline const std::map<NumberingSubsetNS::unique_id, std::size_t>&
    GodOfDof::NprogramWiseDofPerNumberingSubset() const noexcept
    {
        return GetNdofHolder().NprogramWiseDofPerNumberingSubset();
    }


    inline bool operator<(const GodOfDof& lhs, const GodOfDof& rhs)
    {
        return lhs.GetUniqueId() < rhs.GetUniqueId();
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_GODOFDOF_DOT_HXX_
// *** MoReFEM end header guards *** < //
