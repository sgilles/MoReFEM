// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_FELTSPACE_DOT_HXX_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_FELTSPACE_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FEltSpace.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FEltSpace.hpp"


#include <cstddef> // IWYU pragma: keep

#include "Utilities/Type/StrongType/Internal/Convert.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    // clang-format off
    template
    <
        class FEltSectionT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    ExtendedUnknown::vector_const_shared_ptr ExtractExtendedUnknownList(const ModelSettingsT& model_settings,
                                                                        const InputDataT& input_data)
    {
        decltype(auto) unknown_name_list =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename FEltSectionT::UnknownList>(model_settings, input_data);

        decltype(auto) shape_function_list =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename FEltSectionT::ShapeFunctionList>(model_settings, input_data);

        decltype(auto) numbering_subset_list_as_int =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename FEltSectionT::NumberingSubsetList>(model_settings, input_data);

        auto numbering_subset_list =
            Internal::StrongTypeNS::Convert<::MoReFEM::NumberingSubsetNS::unique_id>(numbering_subset_list_as_int);

        constexpr auto unique_id = FEltSectionT::GetUniqueId();

        if (unknown_name_list.size() != numbering_subset_list.size())
            throw Exception("Error in input data file: the number of numbering subsets and the number "
                            "of unknowns in finite element space "
                            + std::to_string(unique_id)
                            + " should be "
                              "the same.");

        if (unknown_name_list.size() != shape_function_list.size())
            throw Exception("Error in input data file: the number of numbering subsets and the number "
                            "of shape functions in finite element space "
                            + std::to_string(unique_id)
                            + " should be "
                              "the same.");

        const auto& unknown_manager = UnknownManager::GetInstance();
        auto& numbering_subset_manager = Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance();

        const auto size = unknown_name_list.size();

        ExtendedUnknown::vector_const_shared_ptr ret;

        for (std::size_t i = 0; i < size; ++i)
        {
            auto&& ptr = std::make_shared<ExtendedUnknown>(
                unknown_manager.GetUnknownPtr(unknown_name_list[i]),
                numbering_subset_manager.GetNumberingSubsetPtr(numbering_subset_list[i]),
                shape_function_list[i]);

            ret.emplace_back(std::move(ptr));
        }

        return ret;
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_FELTSPACE_FELTSPACE_DOT_HXX_
// *** MoReFEM end header guards *** < //
