// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Mpi/Mpi.hpp"

#include "ThirdParty/Wrappers/Mpi/MpiScale.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/FEltSpace/FEltSpaceStorage.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Domain; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FEltSpaceNS
{


    Storage::Storage(const ::MoReFEM::Wrappers::Mpi& mpi,
                     LocalFEltSpacePerRefLocalFEltSpace&& felt_list_per_type,
                     LocalFEltSpacePerRefLocalFEltSpace&& ghost_felt_list_per_type)
    : ::MoReFEM::Crtp::CrtpMpi<Storage>(mpi),
      internal_storage_(Impl::InternalStorage(mpi, std::move(felt_list_per_type))),
      ghost_internal_storage_(Impl::InternalStorage(mpi, std::move(ghost_felt_list_per_type)))
    {
        constexpr auto factor = Utilities::DefaultMaxLoadFactor();
        GetNonCstFEltStoragePerDomain<RoleOnProcessor::processor_wise>().max_load_factor(factor);
        GetNonCstFEltStoragePerDomain<RoleOnProcessor::ghost>().max_load_factor(factor);
    }


    void Storage::SetReducedData(LocalFEltSpacePerRefLocalFEltSpace&& processor_wise_content,
                                 LocalFEltSpacePerRefLocalFEltSpace&& ghost_content)
    {
        internal_storage_.GetNonCstFEltListPerRefLocalFEltSpace() = std::move(processor_wise_content);
        ghost_internal_storage_.GetNonCstFEltListPerRefLocalFEltSpace() = std::move(ghost_content);

        processor_wise_felt_storage_per_domain_.clear();
        assert(ghost_felt_storage_per_domain_.empty());
    }


    LocalFEltSpacePerRefLocalFEltSpace
    Storage ::ComputeFEltListForDomain(const LocalFEltSpacePerRefLocalFEltSpace& full_content,
                                       const Domain& domain) const
    {
        LocalFEltSpacePerRefLocalFEltSpace ret;

        for (const auto& pair : full_content)
        {
            auto& ref_felt_space_ptr = pair.first;
            const auto& complete_local_felt_space_list = pair.second;

            LocalFEltSpace::per_geom_elt_index local_felt_space_list;
            local_felt_space_list.max_load_factor(Utilities::DefaultMaxLoadFactor());

            for (const auto& local_felt_space_pair : complete_local_felt_space_list)
            {
                const auto& local_felt_space_ptr = local_felt_space_pair.second;
                assert(!(!local_felt_space_ptr));

                const auto& local_felt_space = *local_felt_space_ptr;

                if (IsLocalFEltSpaceInDomain(local_felt_space, domain))
                    local_felt_space_list.insert(
                        { local_felt_space.GetGeometricElt().GetIndex(), local_felt_space_ptr });
            }

            if (!local_felt_space_list.empty())
            {
                auto&& copy_ref_felt_space_ptr =
                    std::make_unique<Internal::RefFEltNS::RefLocalFEltSpace>(*ref_felt_space_ptr);
                ret.push_back({ std::move(copy_ref_felt_space_ptr), local_felt_space_list });
            }
        }

        return ret;
    }


    bool Storage::IsEmpty() const noexcept
    {
        return GetStorage<RoleOnProcessor::processor_wise>().IsEmpty()
               && GetStorage<RoleOnProcessor::ghost>().IsEmpty();
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
