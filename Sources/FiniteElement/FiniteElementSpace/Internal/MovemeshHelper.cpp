// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cassert>
#include <cstddef>     // IWYU pragma: keep
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "ThirdParty/Wrappers/Petsc/Vector/AccessGhostContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/MovemeshHelper.hpp" // IWYU pragma: associated
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    void MovemeshHelper::ConstructHelper(const NodeBearer::vector_shared_ptr& node_bearer_list,
                                         const ExtendedUnknown& extended_unknown
#ifndef NDEBUG
                                         ,
                                         const Mesh& mesh,
                                         const Dof::vector_shared_ptr& felt_space_dof_list
#endif // NDEBUG
    )
    {
#ifndef NDEBUG
        const auto Ncoords = ::MoReFEM::CoordsNS::processor_wise_position(this->Ncoords());
        assert(Ncoords
               == ::MoReFEM::CoordsNS::processor_wise_position(mesh.NprocessorWiseCoord() + mesh.NghostCoord()));
#endif // NDEBUG

        const auto& unknown = extended_unknown.GetUnknown();
        const auto& numbering_subset = extended_unknown.GetNumberingSubset();
        decltype(auto) shape_function_label = extended_unknown.GetShapeFunctionLabel();

        for (const auto& node_bearer_ptr : node_bearer_list)
        {
            assert(!(!node_bearer_ptr));

            const auto& node_bearer = *node_bearer_ptr;

            const auto node_list = node_bearer.GetNodeList(unknown, shape_function_label);

            const auto& interface = node_bearer.GetInterface();

            const auto& coords_list = interface.GetCoordsList();
            //            assert(coords_list.size() == 1 && "Due to current limitation to P1 geometry..."); // #1679
            //            Transform into exception: we require in fact the concerned interface are represented by
            //            exactly one Coords

            if (coords_list.size() > 1)
                continue; // #1679 temporary trick; waiting for proper P2/Q2 geometry to see how to handle  this the
                          // best

            assert(!(!coords_list.back()));
            const auto& coords = *(coords_list.back());
            const auto coords_index = coords.GetProcessorWisePosition();
            assert(coords_index < Ncoords);

            for (const auto& node_ptr : node_list)
            {
                assert(!(!node_ptr));
                const auto& node = *node_ptr;

                if (!node.IsInNumberingSubset(numbering_subset))
                    continue;

                std::vector<::MoReFEM::DofNS::processor_wise_or_ghost_index> new_entry;

                const auto& dof_list = node.GetDofList();

                for (const auto& dof_ptr : dof_list)
                {
                    assert(!(!dof_ptr));
                    const auto& dof = *dof_ptr;

                    assert(std::find_if(felt_space_dof_list.cbegin(),
                                        felt_space_dof_list.cend(),
                                        [&dof](const auto& felt_space_dof_ptr)
                                        {
                                            return *felt_space_dof_ptr == dof;
                                        })
                           != felt_space_dof_list.cend());

                    new_entry.push_back(dof.GetProcessorWiseOrGhostIndex(numbering_subset));
                }

                if (new_entry.empty())
                    continue;

                assert(new_entry.size() == mesh.GetDimension());
                dof_index_list_per_coord_[coords_index.Get()] = std::move(new_entry);
                initial_position_per_coord_[coords_index.Get()] = coords.GetCoordinateList();
            }
        }
    }


    MovemeshHelper::MovemeshHelper(const GodOfDof& god_of_dof,
                                   const ExtendedUnknown& extended_unknown
#ifndef NDEBUG
                                   ,
                                   const Dof::vector_shared_ptr& felt_space_processor_wise_dof_list,
                                   const Dof::vector_shared_ptr& felt_space_ghost_dof_list
#endif // NDEBUG
                                   )
    : god_of_dof_(god_of_dof)
#ifndef NDEBUG
      ,
      numbering_subset_(extended_unknown.GetNumberingSubset())
#endif // NDEBUG
    {
        const auto& mesh = god_of_dof.GetMesh();
        const auto Ncoords = mesh.NprocessorWiseCoord() + mesh.NghostCoord();

        dof_index_list_per_coord_.resize(Ncoords);
        initial_position_per_coord_.resize(Ncoords);

        ConstructHelper(god_of_dof.GetProcessorWiseNodeBearerList(),
                        extended_unknown
#ifndef NDEBUG
                        ,
                        mesh,
                        felt_space_processor_wise_dof_list
#endif // NDEBUG
        );

        ConstructHelper(god_of_dof.GetGhostNodeBearerList(),
                        extended_unknown
#ifndef NDEBUG
                        ,
                        mesh,
                        felt_space_ghost_dof_list
#endif // NDEBUG
        );
    }


    void MovemeshHelper::MoveCoords(
        const Coords::vector_shared_ptr& coords_list,
        const ::MoReFEM::Wrappers::Petsc ::AccessVectorContent<Utilities::Access::read_only>& vector_content,
        From from) const
    {
        for (const auto& coords_ptr : coords_list)
        {
            assert(!(!coords_ptr));
            auto& coord = *coords_ptr;
            const auto processor_wise_position = coord.GetProcessorWisePosition();

            const auto& dof_index_list = GetDofIndexList(processor_wise_position);

            std::size_t component_index = 0ul;

            const auto& coord_position = GetInitialPosition(processor_wise_position);

            for (const auto& dof_index : dof_index_list)
            {
                switch (from)
                {
                case From::current_mesh:
                    coord.GetNonCstValue(component_index) += vector_content.GetValue(dof_index.Get());
                    break;
                case From::initial_mesh:
                    assert(component_index < coord_position.size());
                    coord.GetNonCstValue(component_index) =
                        coord_position[component_index] + vector_content.GetValue(dof_index.Get());
                    break;
                }

                ++component_index;
            }
        }
    }


    void MovemeshHelper::Movemesh(const GlobalVector& displacement, From from) const
    {
        assert(numbering_subset_ == displacement.GetNumberingSubset());

        const auto& god_of_dof = GetGodOfDof();
        const auto& mesh = god_of_dof.GetMesh();

        ::MoReFEM::Wrappers::Petsc ::AccessGhostContent helper(displacement);

        decltype(auto) vector_with_ghost = helper.GetVectorWithGhost();

        ::MoReFEM::Wrappers::Petsc ::AccessVectorContent<Utilities::Access::read_only> vector_content(
            vector_with_ghost);

        MoveCoords(mesh.GetProcessorWiseCoordsList(), vector_content, from);
        MoveCoords(mesh.GetGhostCoordsList(), vector_content, from);
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
