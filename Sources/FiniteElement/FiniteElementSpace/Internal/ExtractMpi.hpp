// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_EXTRACTMPI_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_EXTRACTMPI_DOT_HPP_
// *** MoReFEM header guards *** < //


#ifndef NDEBUG


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal
{


    /*!
     * \brief Extract the \a Mpi object from a given \a GeometricElt.
     *
     * \internal This is purely for debug purposes: contrary to other classes, a \a GeometricElt has little business storing this information... but it can
     * be retrieved and is useful for some internal sanity checks.
     *
     * \param[in] geom_elt \a GeometricElt which rank is sought.
     *
     * \return \a Mpi object.
     */
    const ::MoReFEM::Wrappers::Mpi& ExtractMpi(const GeometricElt& geom_elt);


} // namespace MoReFEM::Internal


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //

#endif // NDEBUG

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_EXTRACTMPI_DOT_HPP_
// *** MoReFEM end header guards *** < //
