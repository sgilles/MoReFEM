// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_COORDSMATCHING_IDENTIFYGHOSTNODEBEARERFROMCOORDSMATCHING_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_COORDSMATCHING_IDENTIFYGHOSTNODEBEARERFROMCOORDSMATCHING_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "FiniteElement/Nodes_and_dofs/Internal/GenerateNodeBearerPerCoordsList.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::MeshNS::InterpolationNS { class CoordsMatching; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::GodOfDofNS
{


    /*!
     * \brief Prepare a list of \a NodeBearer that will be needed to define the \a FromCoordsMatching operator.
     *
     * \attention The goal is to identify the ones that would be left aside otherwise. For this reason, I keep only those that aren't on current processor  -
     * the reduction has not yet occurred but I already have the information of which will be handled processor-wise.
     *
     * \internal In fact, *more* \a NodeBearer than the list below is actually required: the ones connected to then within a \a LocalFEltSpace
     * shall also be required so that the integrity of the data is preserved (without that rebuilding fromn
     * prepartitioned data would fail).
     *
     * \param[in] vertex_matching The geometric match between \a Coords of source and target meshes.
     *
     * \return List of *non* processor-wise \a NodeBearer that are a direct match for a \a FromCoordsMatching operator on the current rank.
     */
    Internal::NodeBearerNS::node_bearer_per_coords_index_list_type IdentifyGhostNodeBearerFromCoordsMatching(
        const ::MoReFEM::MeshNS::InterpolationNS::CoordsMatching& vertex_matching);


} // namespace MoReFEM::Internal::GodOfDofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_COORDSMATCHING_IDENTIFYGHOSTNODEBEARERFROMCOORDSMATCHING_DOT_HPP_
// *** MoReFEM end header guards *** < //
