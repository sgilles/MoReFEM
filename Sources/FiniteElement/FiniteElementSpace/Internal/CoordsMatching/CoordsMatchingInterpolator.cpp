// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <string>

#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/CoordsMatchingInterpolator.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    CoordsMatchingInterpolator ::CoordsMatchingInterpolator(std::size_t unique_id,
                                                            const FEltSpace& source_felt_space,
                                                            const NumberingSubset& source_numbering_subset,
                                                            const FEltSpace& target_felt_space,
                                                            const NumberingSubset& target_numbering_subset)
    : unique_id_parent(unique_id), source_felt_space_(source_felt_space),
      source_numbering_subset_(source_numbering_subset), target_felt_space_(target_felt_space),
      target_numbering_subset_(target_numbering_subset)
    {
        assert(source_felt_space.GetUniqueId() != target_felt_space.GetUniqueId());
        assert(source_numbering_subset_.GetUniqueId() != target_numbering_subset.GetUniqueId());
    }


    const std::string& CoordsMatchingInterpolator::ClassName()
    {
        static const std::string ret("CoordsMatchingInterpolator");
        return ret;
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
