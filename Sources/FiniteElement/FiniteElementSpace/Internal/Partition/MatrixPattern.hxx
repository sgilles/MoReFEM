// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_PARTITION_MATRIXPATTERN_DOT_HXX_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_PARTITION_MATRIXPATTERN_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/Partition/MatrixPattern.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "FiniteElement/FiniteElementSpace/Internal/Partition/MatrixPattern.hpp"


#include <cassert>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }
namespace MoReFEM::Wrappers::Petsc { class MatrixPattern; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FEltSpaceNS
{


    inline const NumberingSubset& MatrixPattern::GetRowNumberingSubset() const
    {
        assert(!(!row_numbering_subset_));
        return *row_numbering_subset_;
    }


    inline const NumberingSubset& MatrixPattern::GetColumnNumberingSubset() const
    {
        assert(!(!column_numbering_subset_));
        return *column_numbering_subset_;
    }


    inline const ::MoReFEM::Wrappers::Petsc::MatrixPattern& MatrixPattern::GetPattern() const
    {
        assert(!(!matrix_pattern_));
        return *matrix_pattern_;
    }


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_PARTITION_MATRIXPATTERN_DOT_HXX_
// *** MoReFEM end header guards *** < //
