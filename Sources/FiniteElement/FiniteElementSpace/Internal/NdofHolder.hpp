// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_NDOFHOLDER_DOT_HPP_
#define MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_NDOFHOLDER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <map>
#include <memory>
#include <optional>

#include "Core/NumberingSubset/NumberingSubset.hpp"
#include "FiniteElement/Nodes_and_dofs/NodeBearer.hpp"


namespace MoReFEM::Internal::FEltSpaceNS
{


    /*!
     * \brief Structure that holds the number of dofs for different configurations.
     *
     * This structure is an helper structure to be used by \a GodOfDof and \a FEltSpace.
     */
    class NdofHolder final
    {
      public:
        //! Alias for unique_ptr.
        using const_unique_ptr = std::unique_ptr<const NdofHolder>;

        /*!
         * \brief Constructor used in \a GodOfDof.
         *
         * Compute the number of dofs (both program- and processor-wise).
         *
         * \param[in] program_wise_node_bearer_list Node bearer list before processor-wise reduction.
         * \param[in] mpi_rank Rank of the current processor.
         * \param[in] numbering_subset_list List of \a NumberingSubset.
         * \param[in] Nprogram_wise_dof In the case of running with pre-partitioned data, we need to force feed
         * the number of program-wise values. Otherwise leave it empty and compute them from the data available
         * (this call occurs before partitioning so the whole information is still there). \param[in]
         * Nprogram_wise_dof_per_numbering_subset Same as \a Nprogram_wise_dof for each \a NumberingSubset.
         * Please notice that if one is optional the other must be as well.
         *
         */
        NdofHolder(const NodeBearer::vector_shared_ptr& program_wise_node_bearer_list,
                   const NumberingSubset::vector_const_shared_ptr& numbering_subset_list,
                   const std::size_t mpi_rank,
                   std::optional<std::size_t> Nprogram_wise_dof = std::nullopt,
                   std::optional<std::map<::MoReFEM::NumberingSubsetNS::unique_id, std::size_t>>
                       Nprogram_wise_dof_per_numbering_subset = std::nullopt);

        /*!
         * \brief Constructor used in \a FEltSpace.
         *
         * \copydoc doxygen_hide_mpi_param
         * \param[in] processor_wise_dof_list \a Dof list after processor-wise reduction (ghost excluded).

         * \param[in] numbering_subset_list List of \a NumberingSubset.
         *
         */
        NdofHolder(const ::MoReFEM::Wrappers::Mpi& mpi,
                   const Dof::vector_shared_ptr& processor_wise_dof_list,
                   const NumberingSubset::vector_const_shared_ptr& numbering_subset_list);

        //! Destructor.
        ~NdofHolder() = default;

        //! \copydoc doxygen_hide_copy_constructor
        NdofHolder(const NdofHolder& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        NdofHolder(NdofHolder&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        NdofHolder& operator=(const NdofHolder& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        NdofHolder& operator=(NdofHolder&& rhs) = delete;

        //! Get the number of program-wise dof.
        std::size_t NprogramWiseDof() const noexcept;

        //! Get the number of processor-wise dof (ghost excluded).
        std::size_t NprocessorWiseDof() const noexcept;

        //! Get the number of processor-wise dof (ghost excluded) within a \a numbering_subset.
        //! \param[in] numbering_subset \a NumberingSubset for which the tally is done.
        std::size_t NprocessorWiseDof(const NumberingSubset& numbering_subset) const;

        //! Get the number of program-wise dof within a \a numbering_subset.
        //! \param[in] numbering_subset \a NumberingSubset for which the tally is done.
        std::size_t NprogramWiseDof(const NumberingSubset& numbering_subset) const;

      public:
        //! Accessor to the number of program-wise dof per numbering subset.
        const std::map<::MoReFEM::NumberingSubsetNS::unique_id, std::size_t>&
        NprogramWiseDofPerNumberingSubset() const noexcept;

      private:
        //! Accessor to the number of processor-wise dof per numbering subset.
        const std::map<::MoReFEM::NumberingSubsetNS::unique_id, std::size_t>&
        NprocessorWiseDofPerNumberingSubset() const noexcept;

      private:
        //! Number of program-wise dof.
        std::size_t Nprogram_wise_dof_ = 0ul;

        //! Number of processor-wise dof (ghost excluded).
        std::size_t Nprocessor_wise_dof_ = 0ul;

        /*!
         * \brief Number of processor-wise dof for each numbering subset.
         *
         * . Key is numbering subset id.
         * . Value is the number of dofs.
         */
        std::map<::MoReFEM::NumberingSubsetNS::unique_id, std::size_t> Nprocessor_wise_dof_per_numbering_subset_;


        /*!
         * \brief Number of program-wise dof for each numbering subset.
         *
         * . Key is numbering subset id.
         * . Value is the number of dofs.
         */
        std::map<::MoReFEM::NumberingSubsetNS::unique_id, std::size_t> Nprogram_wise_dof_per_numbering_subset_;
    };


} // namespace MoReFEM::Internal::FEltSpaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/FiniteElementSpace/Internal/NdofHolder.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENTSPACE_INTERNAL_NDOFHOLDER_DOT_HPP_
// *** MoReFEM end header guards *** < //
