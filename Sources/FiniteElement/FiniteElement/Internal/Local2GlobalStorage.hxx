// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENT_INTERNAL_LOCAL2GLOBALSTORAGE_DOT_HXX_
#define MOREFEM_FINITEELEMENT_FINITEELEMENT_INTERNAL_LOCAL2GLOBALSTORAGE_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/FiniteElement/Internal/Local2GlobalStorage.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "FiniteElement/FiniteElement/Internal/Local2GlobalStorage.hpp"


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <utility>
#include <vector>

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

#include "FiniteElement/FiniteElement/Internal/FElt.hpp"
#include "FiniteElement/Unknown/UniqueId.hpp"

namespace MoReFEM::Internal::FEltNS
{


    inline std::vector<::MoReFEM::UnknownNS::unique_id>& Local2GlobalStorage::GetNonCstUnknownIdList() noexcept
    {
        return const_cast<std::vector<::MoReFEM::UnknownNS::unique_id>&>(GetUnknownIdList());
    }


    inline const FElt::vector_shared_ptr& Local2GlobalStorage::GetFEltList() const noexcept
    {
        assert(!felt_list_.empty());
        return felt_list_;
    }


    inline const std::vector<std::pair<std::vector<::MoReFEM::UnknownNS::unique_id>, std::vector<PetscInt>>>&
    Local2GlobalStorage::GetLocal2GlobalPerUnknownIdList() const noexcept
    {
        return local_2global_per_unknown_id_list_;
    }


    inline std::vector<std::pair<std::vector<::MoReFEM::UnknownNS::unique_id>, std::vector<PetscInt>>>&
    Local2GlobalStorage::GetNonCstLocal2GlobalPerUnknownIdList() const noexcept
    {
        return const_cast<std::vector<std::pair<std::vector<::MoReFEM::UnknownNS::unique_id>, std::vector<PetscInt>>>&>(
            GetLocal2GlobalPerUnknownIdList());
    }


} // namespace MoReFEM::Internal::FEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENT_INTERNAL_LOCAL2GLOBALSTORAGE_DOT_HXX_
// *** MoReFEM end header guards *** < //
