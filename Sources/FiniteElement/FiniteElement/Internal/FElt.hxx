// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_FINITEELEMENT_INTERNAL_FELT_DOT_HXX_
#define MOREFEM_FINITEELEMENT_FINITEELEMENT_INTERNAL_FELT_DOT_HXX_
// IWYU pragma: private, include "FiniteElement/FiniteElement/Internal/FElt.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "FiniteElement/FiniteElement/Internal/FElt.hpp"


#include "FiniteElement/Nodes_and_dofs/Node.hpp"
#include "FiniteElement/RefFiniteElement/Internal/RefFEltInFEltSpace.hpp"
#include <cassert>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class ExtendedUnknown; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal
{


    inline const ExtendedUnknown& FElt::GetExtendedUnknown() const noexcept
    {
        return GetRefFElt().GetExtendedUnknown();
    }


    inline const Node::vector_shared_ptr& FElt::GetNodeList() const noexcept
    {
        assert(!node_list_.empty()
               && "As this container is used only to compute the Local2Global, it is "
                  "erased once it's done.");
        return node_list_;
    }


    inline const Internal::RefFEltNS::RefFEltInFEltSpace& FElt::GetRefFElt() const noexcept
    {
        return ref_felt_;
    }


} // namespace MoReFEM::Internal


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_FINITEELEMENT_INTERNAL_FELT_DOT_HXX_
// *** MoReFEM end header guards *** < //
