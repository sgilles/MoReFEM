// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_COMPONENTFACTORY_DOT_HPP_
#define MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_COMPONENTFACTORY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <functional>
#include <map>
#include <string>

#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "FiniteElement/BoundaryConditions/Internal/Component/TComponentManager.hpp"


namespace MoReFEM::Internal::BoundaryConditionNS
{


    /*!
     * \brief The purpose of this class is to create on demand a pointer to a newly created object
     * which type depends on the name given in a specific format.
     *
     *
     *
     */
    class ComponentFactory final : public Utilities::Singleton<ComponentFactory>
    {
      public:
        //! Alias for a function which will create a 'Component'
        using FunctionPrototype = std::function<ComponentManager::const_shared_ptr()>;

        /*!
         * \brief Alias for call back.
         *
         * \internal <b><tt>[internal]</tt></b> As very few variables are expected, a std::map is used rather
         * than a std::unordered_map. This choice might be questioned: I read once that for more than 3 keys
         * the has map is alreadyy more efficient...
         * \endinternal
         */
        using CallBack = std::map<std::string, FunctionPrototype>;

        /*!
         * \brief Returns the name of the class (required for some Singleton-related errors).
         *
         * \return Name of the class.
         */
        static const std::string& ClassName();


      public:
        /*!
         * \brief Register a component.
         *
         * \tparam ComponentT Component to register; ComponentT::Name() is expected to return a name
         * which will be used as a key in the internal storage.
         * \param[in] function New function to register.
         */
        template<class ComponentT>
        bool Register(FunctionPrototype function);

        /*!
         * \brief Create an object according to its name.

         * \param[in] component_name Name of a component that should have been registered previously.
         */
        ComponentManager::const_shared_ptr CreateFromName(const std::string& component_name) const;

        //! Number of elements registered in the factory.
        inline CallBack::size_type Nvariable() const;


      private:
        //! \name Singleton requirements.
        ///@{

        //! Constructor.
        ComponentFactory();

        //! Destructor.
        virtual ~ComponentFactory() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<ComponentFactory>;
        ///@}


      private:
        /*!
         * \brief Associative container to choose the right function given its string identifier.
         *
         *
         */
        CallBack callbacks_;
    };


} // namespace MoReFEM::Internal::BoundaryConditionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //


#include "FiniteElement/BoundaryConditions/Internal/Component/ComponentFactory.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FINITEELEMENT_BOUNDARYCONDITIONS_INTERNAL_COMPONENT_COMPONENTFACTORY_DOT_HPP_
// *** MoReFEM end header guards *** < //
