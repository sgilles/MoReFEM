// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FiniteElementGroup
 * \addtogroup FiniteElementGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// IWYU pragma: no_include <iosfwd>
#include "FiniteElement/BoundaryConditions/Internal/Component/ComponentManager.hpp"
#include "FiniteElement/BoundaryConditions/Internal/Component/FwdForCpp.hpp"

#include "FiniteElement/BoundaryConditions/Internal/Component/Comp12.hpp"


namespace MoReFEM::Internal::BoundaryConditionNS
{


    namespace // anonymous
    {


        ComponentManager::const_shared_ptr Create()
        {
            return std::make_unique<Comp12>();
        }


        // Register the geometric element in the 'ComponentFactory' singleton.
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205.
        __attribute__((unused)) const bool registered = // NOLINT
            Internal::BoundaryConditionNS::ComponentFactory::CreateOrGetInstance().Register<Comp12>(Create);


    } // anonymous namespace


    Comp12::Comp12() = default;


    const std::string& Comp12::Name()
    {
        static const std::string ret("Comp12");
        return ret;
    }


    const std::bitset<3>& Comp12::IsActivated()
    {
        static const std::bitset<3> ret("011");
        return ret;
    }


} // namespace MoReFEM::Internal::BoundaryConditionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FiniteElementGroup
// *** MoReFEM Doxygen end of group *** < //
