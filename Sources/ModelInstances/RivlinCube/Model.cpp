// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <utility>

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"

#include "ModelInstances/RivlinCube/Model.hpp"


namespace MoReFEM::RivlinCubeNS
{


    Model::Model(morefem_data_type& morefem_data) : parent(morefem_data)
    { }


    void Model::SupplInitialize()
    {
        const auto& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));

        const auto& numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::monolithic));
        decltype(auto) morefem_data = parent::GetNonCstMoReFEMData();

        {
            const auto& bc_manager = DirichletBoundaryConditionManager::GetInstance();

            auto&& bc_list = {
                bc_manager.GetDirichletBoundaryConditionPtr(AsBoundaryConditionId(BoundaryConditionIndex::face1)),
                bc_manager.GetDirichletBoundaryConditionPtr(AsBoundaryConditionId(BoundaryConditionIndex::face2)),
                bc_manager.GetDirichletBoundaryConditionPtr(AsBoundaryConditionId(BoundaryConditionIndex::face3))
            };

            variational_formulation_ = std::make_unique<VariationalFormulation>(
                numbering_subset, god_of_dof, std::move(bc_list), morefem_data);
        }

        auto& formulation = GetNonCstVariationalFormulation();
        const auto& mpi = GetMpi();

        formulation.Init(morefem_data);

        Wrappers::Petsc::PrintMessageOnFirstProcessor("\n----------------------------------------------\n", mpi);

        Wrappers::Petsc::PrintMessageOnFirstProcessor("Static problem\n", mpi);

        Wrappers::Petsc::PrintMessageOnFirstProcessor("----------------------------------------------\n", mpi);


        // Solve the system.
        formulation.SolveNonLinear(numbering_subset, numbering_subset);
        formulation.WriteSolution(GetTimeManager(), numbering_subset);
    }


    void Model::Forward()
    { }


    void Model::SupplFinalizeStep()
    { }


    void Model::SupplFinalize()
    {
        auto& formulation = GetNonCstVariationalFormulation();
        formulation.WasTangentQuadratic();
    }


} // namespace MoReFEM::RivlinCubeNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
