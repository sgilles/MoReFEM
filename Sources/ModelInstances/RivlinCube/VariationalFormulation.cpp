// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// IWYU pragma: no_include "Utilities/InputData/Internal/AbstractClass/AbstractClass.hpp"
// IWYU pragma: no_include "Core/MoReFEMData/Internal/AbstractClass.hpp"

#include <cassert>
#include <cstddef>
#include <functional>
#include <iomanip>
#include <map>
#include <ostream>
#include <tuple>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/Wrappers/Petsc/Matrix/NonZeroPattern.hpp"

#include "Core/InputData/Instances/Parameter/Source/Pressure.hpp"
#include "Core/InputData/Instances/Solver/Petsc.hpp"
#include "Core/LinearAlgebra/Operations.hpp"
#include "Core/MoReFEMData/Enum.hpp"
#include "Core/MoReFEMData/Extract.hpp"
#include "Core/NumberingSubset/NumberingSubset.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "Operators/GlobalVariationalOperator/GlobalVariationalOperator.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "ModelInstances/RivlinCube/VariationalFormulation.hpp"


namespace MoReFEM::RivlinCubeNS
{


    VariationalFormulation::~VariationalFormulation() = default;


    const std::string& VariationalFormulation::ClassName()
    {
        static const std::string ret = "Rivlin cube variational formulation";
        return ret;
    }


    VariationalFormulation::VariationalFormulation(
        const NumberingSubset& numbering_subset,
        const GodOfDof& god_of_dof,
        DirichletBoundaryCondition::vector_shared_ptr&& boundary_condition_list,
        morefem_data_type& morefem_data)
    : parent(god_of_dof, std::move(boundary_condition_list), morefem_data), numbering_subset_(numbering_subset)
    { }


    void VariationalFormulation::SupplInit(const morefem_data_type& morefem_data)
    {
        const auto& god_of_dof = GetGodOfDof();
        decltype(auto) domain = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::full_mesh));

        const auto& felt_space_highest_dimension =
            god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));

        solid_ = std::make_unique<Solid<time_manager_type>>(
            morefem_data, domain, felt_space_highest_dimension.GetQuadratureRulePerTopology());

        static_pressure_ = InitScalarParameterFromInputData<InputDataNS::Source::StaticPressure, time_manager_type>(
            "StaticPressure", domain, morefem_data);

        {
            const auto& numbering_subset = GetNumberingSubset();

            const auto& displacement =
                UnknownManager::GetInstance().GetUnknown(AsUnknownId(UnknownIndex::displacement));

            constexpr auto index = EnumUnderlyingType(InitialConditionIndex::displacement_initial_condition);

            parent::SetInitialSystemSolution<index>(
                morefem_data, numbering_subset, displacement, felt_space_highest_dimension);
        }

        tangent_quadratic_verification_file_ =
            parent::GetResultDirectory().AddFile("tangent_quadratic_verification.dat");

        const auto& mpi = GetMpi();

        if (mpi.IsRootProcessor())
        {
            FilesystemNS::File file{ GetTangentQuadraticVerificationFile() };

            if (file.DoExist())
            {
                file.Remove();
            }

            if (!file.DoExist())
            {
                std::ofstream stream{ file.NewContent() };

                stream << "Time | Newton iteration | Residual norm at current iteration | Previous residual norm "
                          "square\n\n";
                stream << "===========================================\n";
                stream << "Static\n";
                stream << "===========================================\n";


                using ip_petsc = InputDataNS::Petsc<1>; // #892 hardcoded.
                decltype(auto) max_iteration =
                    ::MoReFEM::InputDataNS::ExtractLeaf<typename ip_petsc::MaxIteration>(morefem_data);

                std::stringstream temp;
                temp << max_iteration;
                stew_iteration_size_ = static_cast<int>(temp.str().size());
            }
        }

        mpi.Barrier();

        DefineOperators();
    }


    void VariationalFormulation::AllocateMatricesAndVectors()
    {
        const auto& numbering_subset = GetNumberingSubset();

        parent::AllocateSystemMatrix(numbering_subset, numbering_subset);
        parent::AllocateSystemVector(numbering_subset);

        const auto& system_matrix = GetSystemMatrix(numbering_subset, numbering_subset);
        const auto& system_rhs = GetSystemRhs(numbering_subset);

        vector_stiffness_residual_ = std::make_unique<GlobalVector>(system_rhs);
        vector_following_pressure_residual_ = std::make_unique<GlobalVector>(system_rhs);

        matrix_tangent_stiffness_ = std::make_unique<GlobalMatrix>(system_matrix);
        matrix_tangent_following_pressure_ = std::make_unique<GlobalMatrix>(system_matrix);
    }


    void VariationalFormulation::DefineOperators()
    {
        const auto& god_of_dof = GetGodOfDof();
        const auto& felt_space_highest_dimension =
            god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));
        const auto& felt_space_surface_pressure =
            god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::surface_pressure));

        const auto& displacement_ptr =
            UnknownManager::GetInstance().GetUnknownPtr(AsUnknownId(UnknownIndex::displacement));

        hyperelastic_law_parent::Create(god_of_dof.GetMesh().GetDimension(), GetSolid());

        stiffness_operator_ = std::make_unique<StiffnessOperatorType>(felt_space_highest_dimension,
                                                                      displacement_ptr,
                                                                      displacement_ptr,
                                                                      GetSolid(),
                                                                      GetTimeManager(),
                                                                      GetHyperelasticLawPtr());

        following_pressure_operator_ = std::make_unique<following_pressure_op_type>(
            felt_space_surface_pressure, displacement_ptr, displacement_ptr, GetStaticPressure());
    }


    void VariationalFormulation::AssembleStaticOperators(const GlobalVector& evaluation_state)
    {
        {
            auto& matrix_tangent_stiffness = GetNonCstMatrixTangentStiffness();
            auto& vector_stiffness_residual = GetNonCstVectorStiffnessResidual();

            matrix_tangent_stiffness.ZeroEntries();
            vector_stiffness_residual.ZeroEntries();

            GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
            GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);

            GetStiffnessOperator().Assemble(std::make_tuple(std::ref(mat), std::ref(vec)),
                                            DisplacementGlobalVector(evaluation_state));
        }

        {
            auto& matrix_tangent_following_pressure = GetNonCstMatrixTangentFollowingPressure();
            auto& vector_following_pressure_residual = GetNonCstVectorFollowingPressureResidual();

            matrix_tangent_following_pressure.ZeroEntries();
            vector_following_pressure_residual.ZeroEntries();

            GlobalMatrixWithCoefficient mat(matrix_tangent_following_pressure, 1.);
            GlobalVectorWithCoefficient vec(vector_following_pressure_residual, 1.);

            GetFollowingPressureOperator().Assemble(std::make_tuple(std::ref(mat), std::ref(vec)), evaluation_state);
        }
    }


    void VariationalFormulation::ComputeResidual(const GlobalVector& evaluation_state, GlobalVector& residual)
    {
        AssembleStaticOperators(evaluation_state);

        GlobalLinearAlgebraNS::AXPY(1., GetVectorStiffnessResidual(), residual);

        GlobalLinearAlgebraNS::AXPY(1., GetVectorFollowingPressureResidual(), residual);

        ApplyEssentialBoundaryCondition(residual);

        TangentCheck(residual);
    }


    void VariationalFormulation::ComputeTangent(const GlobalVector& evaluation_state,
                                                GlobalMatrix& tangent,
                                                GlobalMatrix& preconditioner)
    {
        static_cast<void>(evaluation_state);
        static_cast<void>(preconditioner);
        assert("For this model, we are in the most generic case in which tangent is used as preconditioner."
               && tangent.Internal(std::source_location::current())
                      == preconditioner.Internal(std::source_location::current()));

#ifndef NDEBUG
        AssertSameNumberingSubset(GetMatrixTangentStiffness(), tangent);
        AssertSameNumberingSubset(GetMatrixTangentFollowingPressure(), tangent);
#endif // NDEBUG

        GlobalLinearAlgebraNS::AXPY<NonZeroPattern::same>(1., GetMatrixTangentStiffness(), tangent);

        GlobalLinearAlgebraNS::AXPY<NonZeroPattern::same>(1., GetMatrixTangentFollowingPressure(), tangent);

        ApplyEssentialBoundaryCondition(tangent);
    }


    void VariationalFormulation::TangentCheck(GlobalVector& rhs)
    {
        // Verifying if the tangent is quadratic.
        const double rhs_norm_iter = rhs.Norm(NORM_2);

        std::size_t iteration = GetSnes().GetSnesIteration();

        if (iteration == 0)
        {
            rhs_norm_previous_iter_ = 0;
        }

        if (!(iteration == 0))
        {
            if (rhs_norm_iter > rhs_norm_previous_iter_ * rhs_norm_previous_iter_)
            {
                tangent_not_quadratic_in_static_ = true;

                if (iteration > 1)
                    tangent_not_quadratic_at_other_than_first_iteration_in_static_ = true;

                if (GetMpi().IsRootProcessor())
                {
                    FilesystemNS::File file{ GetTangentQuadraticVerificationFile() };

                    if (file.DoExist())
                    {
                        std::ofstream inout{ file.Append() };

                        inout << GetTimeManager().GetTime();
                        inout << " | ";
                        inout << std::setw(stew_iteration_size_) << iteration;
                        inout << std::scientific << std::setprecision(12) << " | " << rhs_norm_iter;
                        inout << " | " << rhs_norm_previous_iter_ * rhs_norm_previous_iter_;
                        inout << std::endl;
                    }
                }

                GetMpi().Barrier();

                if (print_tangent_warning_)
                {
                    Wrappers::Petsc::PrintMessageOnFirstProcessor("\n===========================================\n",
                                                                  GetMpi());
                    Wrappers::Petsc::PrintMessageOnFirstProcessor(
                        "[WARNING] Tangent is not quadratic at iteration %d.\n",
                        GetMpi(),
                        std::source_location::current(),
                        iteration);
                    Wrappers::Petsc::PrintMessageOnFirstProcessor("Residual norm at current iteration    %1.12e\n",
                                                                  GetMpi(),
                                                                  std::source_location::current(),
                                                                  rhs_norm_iter);
                    Wrappers::Petsc::PrintMessageOnFirstProcessor("Previous residual norm square         %1.12e\n",
                                                                  GetMpi(),
                                                                  std::source_location::current(),
                                                                  rhs_norm_previous_iter_ * rhs_norm_previous_iter_);
                    Wrappers::Petsc::PrintMessageOnFirstProcessor("===========================================\n\n",
                                                                  GetMpi());
                }
            }
        }

        rhs_norm_previous_iter_ = rhs_norm_iter;
    }

    void VariationalFormulation::WasTangentQuadratic()
    {
        if (tangent_not_quadratic_in_static_)
        {
            Wrappers::Petsc::PrintMessageOnFirstProcessor("\n===========================================\n", GetMpi());
            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "[WARNING] In the static resolution the tangent was not quadratic.\n", GetMpi());

            if (tangent_not_quadratic_at_other_than_first_iteration_in_static_)
            {
                Wrappers::Petsc::PrintMessageOnFirstProcessor(
                    "[WARNING] The tangent was not quadratic at another step than the first one.\n", GetMpi());
            }

            Wrappers::Petsc::PrintMessageOnFirstProcessor("===========================================\n", GetMpi());
        }

        if (tangent_not_quadratic_in_static_)
        {
            Wrappers::Petsc::PrintMessageOnFirstProcessor("\n===========================================\n", GetMpi());

            decltype(auto) filename = static_cast<std::string>(GetTangentQuadraticVerificationFile());

            Wrappers::Petsc::PrintMessageOnFirstProcessor(
                "[WARNING] Information about the tangent have been printed in %s\n",
                GetMpi(),
                std::source_location::current(),
                filename.c_str());
            Wrappers::Petsc::PrintMessageOnFirstProcessor("===========================================\n\n", GetMpi());
        }
    }


} // namespace MoReFEM::RivlinCubeNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
