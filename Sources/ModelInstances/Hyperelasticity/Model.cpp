// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <iostream>
#include <sstream>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "ModelInstances/Hyperelasticity/Model.hpp"


namespace MoReFEM::MidpointHyperelasticityNS
{


    Model::Model(morefem_data_type& morefem_data) : parent(morefem_data)
    { }


    void Model::SupplInitialize()
    {
        const auto& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));

        {
            decltype(auto) felt_space_list = god_of_dof.GetFEltSpaceList();

            std::ostringstream oconv;

            oconv << "SUMMARY FOR RANK " << GetMpi().GetRank<int>() << ": " << std::endl;

            for (const auto& felt_space_ptr : felt_space_list)
            {
                oconv << "\tFor FEltSpace " << felt_space_ptr->GetUniqueId()
                      << " Nproc = " << felt_space_ptr->GetProcessorWiseDofList().size()
                      << " and Nghost = " << felt_space_ptr->GetGhostDofList().size() << std::endl;
            }

            oconv << std::endl;

            std::cout << oconv.str();
        }

        decltype(auto) morefem_data = parent::GetNonCstMoReFEMData();

        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::volume));

        {
            const DirichletBoundaryConditionManager& bc_manager = DirichletBoundaryConditionManager::GetInstance();

            auto&& bc_list = { bc_manager.GetDirichletBoundaryConditionPtr(
                AsBoundaryConditionId(BoundaryConditionIndex::clamped)) };

            const auto& unknown_manager = UnknownManager::GetInstance();

            const auto& displacement = unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::displacement));

            variational_formulation_ = std::make_unique<VariationalFormulation>(
                felt_space_volume.GetNumberingSubset(displacement), god_of_dof, std::move(bc_list), morefem_data);
        }

        // Exit the program if the 'precompute' mode was chosen.
        PrecomputeExit(morefem_data);

        auto& variational_formulation = GetNonCstVariationalFormulation();

        variational_formulation.Init(morefem_data);

        const auto& displacement_numbering_subset = variational_formulation.GetDisplacementNumberingSubset();

        const auto& mpi = GetMpi();

        if (!parent::GetTimeManager().IsInRestartMode())
        {
            Wrappers::Petsc::PrintMessageOnFirstProcessor("\n----------------------------------------------\n", mpi);

            Wrappers::Petsc::PrintMessageOnFirstProcessor("Static problem\n", mpi);

            Wrappers::Petsc::PrintMessageOnFirstProcessor("----------------------------------------------\n", mpi);


            variational_formulation.SolveNonLinear(displacement_numbering_subset, displacement_numbering_subset);

            variational_formulation.WriteSolution(GetTimeManager(), displacement_numbering_subset);
        }

        variational_formulation.PrepareDynamicRuns();
    }


    void Model::SupplInitializeStep()
    {
        if (parent::DoWriteRestartData())
            GetVariationalFormulation().WriteRestartData();
    }


    void Model::Forward()
    {
        VariationalFormulation& variational_formulation = GetNonCstVariationalFormulation();
        const NumberingSubset& displacement_numbering_subset = variational_formulation.GetDisplacementNumberingSubset();

        variational_formulation.SolveNonLinear(displacement_numbering_subset, displacement_numbering_subset);
    }


    void Model::SupplFinalizeStep()
    {
        auto& variational_formulation = GetNonCstVariationalFormulation();
        const auto& displacement_numbering_subset = variational_formulation.GetDisplacementNumberingSubset();

        variational_formulation.WriteSolution(GetTimeManager(), displacement_numbering_subset);
        variational_formulation.UpdateForNextTimeStep();
    }


    void Model::SupplFinalize()
    { }


} // namespace MoReFEM::MidpointHyperelasticityNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
