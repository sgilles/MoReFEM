// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <utility>

#include "Utilities/Filesystem/Directory.hpp"

#include "FiniteElement/BoundaryConditions/DirichletBoundaryConditionManager.hpp"
#include "FiniteElement/Unknown/UnknownManager.hpp"

#include "ModelInstances/NonLinearShell/Model.hpp"


namespace MoReFEM::MidpointNonLinearShellNS
{


    Model::Model::Model(morefem_data_type& morefem_data) : parent(morefem_data)
    { }


    void Model::SupplInitialize()
    {
        const auto& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) morefem_data = parent::GetNonCstMoReFEMData();

        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::volume));

        {
            const DirichletBoundaryConditionManager& bc_manager = DirichletBoundaryConditionManager::GetInstance();

            auto&& bc_list = { bc_manager.GetDirichletBoundaryConditionPtr(
                AsBoundaryConditionId(BoundaryConditionIndex::clamped)) };

            const auto& unknown_manager = UnknownManager::GetInstance();

            const auto& displacement = unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::displacement));

            variational_formulation_ = std::make_unique<VariationalFormulation>(
                felt_space_volume.GetNumberingSubset(displacement), god_of_dof, std::move(bc_list), morefem_data);
        }

        // Exit the program if the 'precompute' mode was chosen.
        PrecomputeExit(morefem_data);

        auto& variational_formulation = GetNonCstVariationalFormulation();

        variational_formulation.Init(morefem_data);

        const auto& displacement_numbering_subset = variational_formulation.GetDisplacementNumberingSubset();

        const auto& mpi = GetMpi();

        Wrappers::Petsc::PrintMessageOnFirstProcessor("\n----------------------------------------------\n", mpi);

        Wrappers::Petsc::PrintMessageOnFirstProcessor("Static problem\n", mpi);

        Wrappers::Petsc::PrintMessageOnFirstProcessor("----------------------------------------------\n", mpi);

        variational_formulation.SolveStaticContinuation();

        variational_formulation.WriteSolution(GetTimeManager(), displacement_numbering_subset);

        const auto output_path = god_of_dof.GetOutputDirectoryForNumberingSubset(displacement_numbering_subset);

        const auto medit_sol_output_file_x = output_path.AddFile("shell_x.sol");
        variational_formulation.WriteMeditSolution(medit_sol_output_file_x, 0);
        const auto medit_sol_output_file_y = output_path.AddFile("shell_y.sol");
        variational_formulation.WriteMeditSolution(medit_sol_output_file_y, 1);
        const auto medit_sol_output_file_z = output_path.AddFile("shell_z.sol");
        variational_formulation.WriteMeditSolution(medit_sol_output_file_z, 2);

        variational_formulation.PrepareDynamicRuns();
    }


    void Model::SupplInitializeStep()
    { }


    void Model::Forward()
    {
        VariationalFormulation& variational_formulation = GetNonCstVariationalFormulation();
        const NumberingSubset& displacement_numbering_subset = variational_formulation.GetDisplacementNumberingSubset();

        variational_formulation.SolveNonLinear(displacement_numbering_subset, displacement_numbering_subset);
    }


    void Model::SupplFinalizeStep()
    {
        auto& variational_formulation = GetNonCstVariationalFormulation();
        const auto& displacement_numbering_subset = variational_formulation.GetDisplacementNumberingSubset();

        variational_formulation.WriteSolution(GetTimeManager(), displacement_numbering_subset);
        variational_formulation.UpdateForNextTimeStep();
    }


    void Model::SupplFinalize()
    { }


} // namespace MoReFEM::MidpointNonLinearShellNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
