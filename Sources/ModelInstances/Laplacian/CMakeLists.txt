##########################################
# Library to compile once files that will be used in several executables.
# There are also non source files (README, lua files, etc...) so that
# they appear correctly when CMake generates project for an IDE.
##########################################

add_library(MoReFEM4Laplacian_lib ${LIBRARY_TYPE} )

target_sources(MoReFEM4Laplacian_lib
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/README.md
        ${CMAKE_CURRENT_LIST_DIR}/demo.lua        
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/Model.cpp 
        ${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.cpp
		${CMAKE_CURRENT_LIST_DIR}/Model.hpp
		${CMAKE_CURRENT_LIST_DIR}/Model.hxx
 		${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hpp
		${CMAKE_CURRENT_LIST_DIR}/VariationalFormulation.hxx
)

target_link_libraries(MoReFEM4Laplacian_lib
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>)

morefem_organize_IDE(MoReFEM4Laplacian_lib ModelInstances/Laplacian ModelInstances/Laplacian)                         

##########################################
# Executable to run the model
##########################################

add_executable(MoReFEM4Laplacian ${CMAKE_CURRENT_LIST_DIR}/main.cpp)
target_link_libraries(MoReFEM4Laplacian
                      MoReFEM4Laplacian_lib)
apply_lto_if_supported(MoReFEM4Laplacian)

morefem_organize_IDE(MoReFEM4Laplacian ModelInstances/Laplacian ModelInstances/Laplacian)                         

morefem_install(MoReFEM4Laplacian MoReFEM4Laplacian_lib)

##########################################
# Executable to produce outputs in Ensight format
##########################################

add_executable(MoReFEM4LaplacianEnsightOutput
               ${CMAKE_CURRENT_LIST_DIR}/main_ensight_output.cpp)

target_link_libraries(MoReFEM4LaplacianEnsightOutput
                      ${MOREFEM_POST_PROCESSING}
                      MoReFEM4Laplacian_lib)

morefem_organize_IDE(MoReFEM4LaplacianEnsightOutput ModelInstances/Laplacian ModelInstances/Laplacian)

morefem_install(MoReFEM4LaplacianEnsightOutput)

##########################################
# Executable to update a input Lua file
##########################################

add_executable(MoReFEM4LaplacianUpdateLuaFile
                ${CMAKE_CURRENT_LIST_DIR}/main_update_lua_file.cpp)
               
target_link_libraries(MoReFEM4LaplacianUpdateLuaFile
                      ${MOREFEM_POST_PROCESSING}
                      MoReFEM4Laplacian_lib)

morefem_organize_IDE(MoReFEM4LaplacianUpdateLuaFile ModelInstances/Laplacian ModelInstances/Laplacian)                         

morefem_install(MoReFEM4LaplacianUpdateLuaFile)


##########################################
# Tests
##########################################

morefem_test_run_model_in_both_modes(NAME Laplacian
                                     LUA ${MOREFEM_ROOT}/Sources/ModelInstances/Laplacian/demo.lua
                                     EXE MoReFEM4Laplacian
                                     TIMEOUT 20)
                                     
##########################################
# Executable and test which check reference results are properly found.
##########################################

add_executable(MoReFEM4LaplacianCheckResults ${CMAKE_CURRENT_LIST_DIR}/test_results.cpp)
target_link_libraries(MoReFEM4LaplacianCheckResults
                      MoReFEM4Laplacian_lib
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})

morefem_organize_IDE(MoReFEM4LaplacianCheckResults ModelInstances/Laplacian ModelInstances/Laplacian)                         
                      
morefem_boost_test_check_results(NAME Laplacian
                                   EXE MoReFEM4LaplacianCheckResults
                                   TIMEOUT 20)


##########################################
## Populate XCode schemes with valid arguments
##########################################

set_property (TARGET MoReFEM4Laplacian PROPERTY XCODE_SCHEME_ENVIRONMENT "MOREFEM_RESULT_DIR=/tmp/MoReFEM_in_XCode/Results" "MOREFEM_PREPARTITIONED_DATA_DIR=/tmp/MoReFEM_in_XCode/PrepartitionedData")

set_property (TARGET MoReFEM4Laplacian PROPERTY XCODE_SCHEME_ARGUMENTS "-i ${CMAKE_CURRENT_LIST_DIR}/demo.lua" "--overwrite_directory")
