// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODELINSTANCES_HEAT_VARIATIONALFORMULATION_DOT_HXX_
#define MOREFEM_MODELINSTANCES_HEAT_VARIATIONALFORMULATION_DOT_HXX_
// IWYU pragma: private, include "ModelInstances/Heat/VariationalFormulation.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>

#include "ThirdParty/Wrappers/Petsc/Solver/Snes.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalMatrix; }  // lines 28-28
namespace MoReFEM { class GlobalVector; }  // lines 29-29
namespace MoReFEM { class NumberingSubset; }
namespace MoReFEM::GlobalVariationalOperatorNS { class GradPhiGradPhi; }
namespace MoReFEM::GlobalVariationalOperatorNS { class Mass; }  // lines 30-30

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::HeatNS
{


    inline Wrappers::Petsc::Snes::SNESConvergenceTestFunction
    VariationalFormulation::ImplementSnesConvergenceTestFunction() const
    {
        return nullptr;
    }


    inline const VariationalFormulation::source_operator_type&
    VariationalFormulation::GetVolumicSourceOperator() const noexcept
    {
        assert(!(!volumic_source_operator_));
        return *volumic_source_operator_;
    }


    inline const GlobalVariationalOperatorNS::GradPhiGradPhi&
    VariationalFormulation::GetConductivityOperator() const noexcept
    {
        assert(!(!conductivity_operator_));
        return *conductivity_operator_;
    }


    inline const GlobalVariationalOperatorNS::Mass& VariationalFormulation ::GetCapacityOperator() const noexcept
    {
        assert(!(!capacity_operator_) && "Only exists in dynamic");
        return *capacity_operator_;
    }


    inline const VariationalFormulation::source_operator_type&
    VariationalFormulation::GetNeumannOperator() const noexcept
    {
        assert(!(!neumann_operator_));
        return *neumann_operator_;
    }


    inline const GlobalVariationalOperatorNS::Mass&
    VariationalFormulation ::GetRobinBilinearPartOperator() const noexcept
    {
        assert(!(!robin_bilinear_part_operator_));
        return *robin_bilinear_part_operator_;
    }


    inline const VariationalFormulation::source_operator_type&
    VariationalFormulation ::GetRobinLinearPartOperator() const noexcept
    {
        assert(!(!robin_linear_part_operator_));
        return *robin_linear_part_operator_;
    }


    inline const GlobalVector& VariationalFormulation::GetVectorCurrentVolumicSource() const
    {
        assert(!(!vector_current_volumic_source_));
        return *vector_current_volumic_source_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorCurrentVolumicSource()
    {
        return const_cast<GlobalVector&>(GetVectorCurrentVolumicSource());
    }

    inline const GlobalVector& VariationalFormulation::GetVectorCurrentNeumann() const
    {
        assert(!(!vector_current_neumann_));
        return *vector_current_neumann_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorCurrentNeumann()
    {
        return const_cast<GlobalVector&>(GetVectorCurrentNeumann());
    }


    inline const GlobalVector& VariationalFormulation::GetVectorCurrentRobin() const
    {
        assert(!(!vector_current_robin_));
        return *vector_current_robin_;
    }


    inline GlobalVector& VariationalFormulation::GetNonCstVectorCurrentRobin()
    {
        return const_cast<GlobalVector&>(GetVectorCurrentRobin());
    }


    inline const GlobalMatrix& VariationalFormulation::GetMatrixConductivity() const
    {
        assert(!(!matrix_conductivity_));
        return *matrix_conductivity_;
    }


    inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixConductivity()
    {
        return const_cast<GlobalMatrix&>(GetMatrixConductivity());
    }


    inline const GlobalMatrix& VariationalFormulation::GetMatrixCapacityPerTimeStep() const
    {
        assert(!(!matrix_capacity_));
        return *matrix_capacity_;
    }


    inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixCapacityPerTimeStep()
    {
        return const_cast<GlobalMatrix&>(GetMatrixCapacityPerTimeStep());
    }


    inline const GlobalMatrix& VariationalFormulation::GetMatrixRobin() const
    {
        assert(!(!matrix_robin_));
        return *matrix_robin_;
    }


    inline GlobalMatrix& VariationalFormulation::GetNonCstMatrixRobin()
    {
        return const_cast<GlobalMatrix&>(GetMatrixRobin());
    }


    inline VariationalFormulation::StaticOrDynamic VariationalFormulation::GetStaticOrDynamic() const
    {
        return run_case_;
    }


    inline const NumberingSubset& VariationalFormulation::GetNumberingSubset() const
    {
        return numbering_subset_;
    }


    inline auto VariationalFormulation::GetDiffusionDensity() const -> const scalar_parameter_type&
    {
        assert(!(!density_));
        return *density_;
    }


    inline auto VariationalFormulation::GetTransfertCoefficient() const -> const scalar_parameter_type&
    {
        assert(!(!transfert_coefficient_));
        return *transfert_coefficient_;
    }


    inline auto VariationalFormulation::GetDiffusionTensor() const -> const scalar_parameter_type&
    {
        assert(!(!diffusion_tensor_));
        return *diffusion_tensor_;
    }


} // namespace MoReFEM::HeatNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODELINSTANCES_HEAT_VARIATIONALFORMULATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
