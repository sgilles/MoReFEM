A static Stokes model.

This model has been let in the main library as it displays nicely features:

- This is the only model left in the main project that deals with a 2 unknowns operator. That's not a big deal
(NumberingSubset were devised to make that straightforward) but it's nice to have at least an illustration.

- It is the sole model ever written in MoReFEM in which you may actually play with numbering subset in the input data
file. If you set the same for both, you will use monolithic resolution, if not, a Uzawa resolution. For this specific
model monolithic is clearly the most efficient way to go (Uzawa most noticeably requires to fine tune a model parameter
which is rather tricky and is much slower) but it is a proof of concept of the possibility to write a flexible model.

- It was written firstly with separate operators, and finally a dedicated Stokes operator was also written. A macro
enables to switch from one to the other (there's a scheme for each case in XCode). This highlights the possibility
for an advanced developer to implement his own operators, which is more efficient (assembling loop done only once and
no need to perform linear algebra computations).


