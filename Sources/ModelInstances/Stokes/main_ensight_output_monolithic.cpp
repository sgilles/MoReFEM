// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ModelInstancesGroup
 * \addtogroup ModelInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef> // IWYU pragma: keep

#include "Model/Main/MainEnsightOutput.hpp"

#include "ModelInstances/Stokes/Environment.hpp"
#include "ModelInstances/Stokes/Model.hpp"


using namespace MoReFEM;
using namespace MoReFEM::StokesNS;


int main(int argc, char** argv)
{
    StokesNS::SetStokesConfiguration();

    std::vector<NumberingSubsetNS::unique_id> numbering_subset_id_list{
        AsNumberingSubsetId(NumberingSubsetIndex::velocity_or_monolithic),
        AsNumberingSubsetId(NumberingSubsetIndex::velocity_or_monolithic)
    };

    std::vector<std::string> unknown_list{ "velocity", "pressure" };

    return ModelNS::MainEnsightOutput<StokesNS::Model>(
        argc, argv, AsMeshId(MeshIndex::mesh), numbering_subset_id_list, unknown_list);
}


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
