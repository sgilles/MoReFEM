add_executable(TestBCInvalidComp)

target_sources(TestBCInvalidComp
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/demo_too_high_dimension.lua
        ${CMAKE_CURRENT_LIST_DIR}/demo_scalar.lua
)
          
target_link_libraries(TestBCInvalidComp
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_FELT}>
                      ${MOREFEM_BASIC_TEST_TOOLS})

morefem_organize_IDE(TestBCInvalidComp Test/FiniteElement/BoundaryCondition Test/FiniteElement/BoundaryCondition/InvalidComp)


morefem_boost_test_sequential_mode(NAME BCInvalidComp-TooHighDimension
                                   EXE TestBCInvalidComp
                                   TIMEOUT 10
                                   LUA ${MOREFEM_ROOT}/Sources/Test/FiniteElement/BoundaryCondition/InvalidComp/demo_too_high_dimension.lua)

morefem_boost_test_sequential_mode(NAME BCInvalidComp-Scalar
                                   EXE TestBCInvalidComp
                                   TIMEOUT 10
                                   LUA ${MOREFEM_ROOT}/Sources/Test/FiniteElement/BoundaryCondition/InvalidComp/demo_scalar.lua)
