// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cstdlib>

#define BOOST_TEST_MODULE p2_mesh
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/String/String.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // #__clang__


BOOST_FIXTURE_TEST_CASE(p2_mesh, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::CreateOrGetInstance();

    constexpr auto medit_unique_id = MeshNS::unique_id{ 1ul };
    constexpr auto dimension = 3u;
    constexpr auto space_unit = 1.;

    std::filesystem::path path{ "${MOREFEM_ROOT}/Data/Mesh/cubeQ2.mesh" };
    FilesystemNS::File mesh_file{ std::move(path) };

    mesh_manager.Create(medit_unique_id, mesh_file, dimension, MeshNS::Format::Medit, space_unit);

    decltype(auto) medit_mesh = mesh_manager.GetMesh(medit_unique_id);

    decltype(auto) medit_mesh_label_list = medit_mesh.GetLabelList();

    std::vector<std::size_t> medit_mesh_label_index_list(medit_mesh_label_list.size());

    std::transform(medit_mesh_label_list.cbegin(),
                   medit_mesh_label_list.cend(),
                   medit_mesh_label_index_list.begin(),
                   [](const auto& ptr)
                   {
                       assert(!(!ptr));
                       return ptr->GetIndex();
                   });

    for (auto& val : medit_mesh_label_index_list)
        std::cout << val << std::endl;

    const auto& geo_list = medit_mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();

    for (const auto& geo_elt : geo_list)
    {
        // const auto& vertex_list = geo_elt->GetVertexList();
        const auto& coords_list = geo_elt->GetCoordsList();
        std::cout << "Ncoords: " << geo_elt->Ncoords() << std::endl;
        for (const auto& coord_ptr : coords_list)
        {
            const auto& coord = *coord_ptr;
            coord.Print(std::cout);
        }
    }

    auto medit_bag = medit_mesh.BagOfEltType<RoleOnProcessorPlusBoth::processor_wise>();

    std::sort(medit_bag.begin(),
              medit_bag.end(),
              [](const auto& lhs, const auto& rhs)
              {
                  assert(!(!lhs));
                  assert(!(!rhs));
                  return lhs->GetIdentifier() < rhs->GetIdentifier();
              });


    // decltype(auto) medit_geom_elt_list = medit_mesh.GetGeometricEltList<RoleOnProcessor::processor_wise>();
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
