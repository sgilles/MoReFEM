add_executable(TestColoring)

target_sources(TestColoring
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt        
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/demo.lua        
)
          
target_link_libraries(TestColoring
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>
                      ${MOREFEM_BASIC_TEST_TOOLS})

morefem_organize_IDE(TestColoring Test/Geometry Test/Geometry/Coloring)

morefem_boost_test_both_modes(NAME Coloring
                              EXE TestColoring
                              LUA ${MOREFEM_ROOT}/Sources/Test/Geometry/Coloring/demo.lua
                              TIMEOUT 5)
