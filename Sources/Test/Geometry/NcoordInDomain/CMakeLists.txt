add_executable(TestNcoordInDomain)

target_sources(TestNcoordInDomain
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/README.md
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
        ${CMAKE_CURRENT_LIST_DIR}/demo.lua
)
          
target_link_libraries(TestNcoordInDomain
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_FELT}>
                      ${MOREFEM_BASIC_TEST_TOOLS})

morefem_organize_IDE(TestNcoordInDomain Test/Geometry Test/Geometry/NcoordInDomain)

morefem_boost_test_both_modes(NAME NcoordInDomain
                              EXE TestNcoordInDomain
                              TIMEOUT 5
                              LUA ${MOREFEM_ROOT}/Sources/Test/Geometry/NcoordInDomain/demo.lua)
