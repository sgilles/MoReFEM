// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_GEOMETRY_LOADPREPARTITIONEDMESH_MODEL_DOT_HPP_
#define MOREFEM_TEST_GEOMETRY_LOADPREPARTITIONEDMESH_MODEL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Core/InputData/Instances/Result.hpp"

#include "Model/Model.hpp"

#include "Test/Geometry/LoadPrepartitionedMesh/InputData.hpp"


namespace MoReFEM::TestNS::LoadPrepartitionedMeshNS
{


    /*!
     * \brief Toy model used to perform tests about reconstruction of a \a Mesh from pre-partitioned data.
     *
     * This test must be run in parallel; its principle is to:
     * - Initialize the model normally (including separating the mesh among all mpi processors and reducing it).
     * - Write the data related to the partition of said mesh.
     * - Load from the data written a new mesh.
     * - Check both mesh are indistinguishable.
     */
    //! \copydoc doxygen_hide_model_4_test
    // clang-format off
    class Model
    : public ::MoReFEM::Model
             <
                Model,
                morefem_data_type,
                DoConsiderProcessorWiseLocal2Global::no
             >
    // clang-format on
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = Model;

        //! Convenient alias.
        // clang-format off
        using parent = ::MoReFEM::Model
        <
            Model,
            morefem_data_type,
            DoConsiderProcessorWiseLocal2Global::no
        >;
        // clang-format on

        static_assert(std::is_convertible<self*, parent*>());


      public:
        //! Return the name of the model.
        static const std::string& ClassName();

        //! Friendship granted to the base class so this one can manipulates private methods.
        friend parent;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         */
        Model(morefem_data_type& morefem_data);

        //! Destructor.
        ~Model() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Model(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Model(Model&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Model& operator=(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Model& operator=(Model&& rhs) = delete;

        ///@}


        //! Check the list of \a GeometricElt is properly reconstructed from pre-partitioned data.
        void CheckGeometricEltList() const;

        //! Check the list of \a MeshLabel is properly reconstructed from pre-partitioned data.
        void CheckLabelList() const;

        //! Check the list of \a Coords is properly reconstructed from pre-partitioned data.
        void CheckCoordsList() const;

        /*!
         * \brief Check the list of \a interfaces is properly reconstructed from pre-partitioned data.
         *
         * This one is slightly trickier as a list at mesh level is not kept within a \a Mesh object; however
         * it may be computed on the fly if need be (it's done in this test...)
         */
        void CheckInterfaceList() const;

        //! Accessor to the original mesh (see class explanation).
        const Mesh& GetOriginalMesh() const;

        //! Accessor to the reconstructed mesh (see class explanation).
        const Mesh& GetMeshFromPrepartitionedData() const;


        /// \name Crtp-required methods.
        ///@{


        /*!
         * \brief Initialise the problem.
         *
         * This initialisation includes the resolution of the static problem.
         */
        void SupplInitialize();


        /*!
         * \brief Initialise a dynamic step.
         *
         */
        void SupplFinalize();

        ///@}
    };


} // namespace MoReFEM::TestNS::LoadPrepartitionedMeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/Geometry/LoadPrepartitionedMesh/Model.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_GEOMETRY_LOADPREPARTITIONEDMESH_MODEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
