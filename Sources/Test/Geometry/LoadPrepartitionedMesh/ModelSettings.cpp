// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/Geometry/LoadPrepartitionedMesh/InputData.hpp"


namespace MoReFEM::TestNS::LoadPrepartitionedMeshNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<sole>>({ "Sole finite element space" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::mesh)>>({ " mesh)" });
        SetDescription<InputDataNS::Unknown<sole>>({ " sole" });
        SetDescription<InputDataNS::Domain<sole>>({ " sole" });
        SetDescription<InputDataNS::NumberingSubset<sole>>({ " sole" });
    }


} // namespace MoReFEM::TestNS::LoadPrepartitionedMeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
