// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <memory>
#include <sstream>

#define BOOST_TEST_MODULE coords_matching_reverse
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Containers/Vector.hpp"
#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/String/String.hpp"

#include "Core/InputData/Advanced/SetFromInputData.hpp"
#include "Utilities/InputData/InputData.hpp"

#include "Geometry/Interpolator/CoordsMatching.hpp"
#include "Geometry/Interpolator/Internal/CoordsMatchingManager.hpp"

#include "Test/Geometry/CoordsMatching/InputData.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/unused-function.hpp"
#include "Utilities/Warnings/Internal/IgnoreWarning/unused-member-function.hpp"

#include "Test/Geometry/CoordsMatching/AnonymousTestData.hpp"
#include "Test/Geometry/CoordsMatching/InputData.hpp"
#include "Test/Tools/EmptyModelSettings.hpp"


namespace MoReFEM::TestNS::CoordsMatchingNS
{

    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::CoordsMatchingFile<1>>({ "Coords matching" });
    }


} // namespace MoReFEM::TestNS::CoordsMatchingNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

using namespace MoReFEM;


namespace
{

    auto empty_model_settings = TestNS::EmptyModelSettings();

}


BOOST_FIXTURE_TEST_SUITE(coords_matching, Fixture)


// This test is run independently from others as it uses up the manager which is a singleton - so using it would
// break reentry in others tests.
BOOST_AUTO_TEST_CASE(reverse)
{
    const auto mesh_ids = std::make_pair(MeshNS::unique_id{ 1ul }, MeshNS::unique_id{ 42ul });

    const auto& source_mesh_id = mesh_ids.first;
    const auto& target_mesh_id = mesh_ids.second;

    TestData generated_data("reverse", mesh_ids, { 1, 2, 3 }, { 11, 12, 13 }, compute_reverse::yes);

    TestNS::CoordsMatchingNS::input_data_type input_data(empty_model_settings, generated_data.GetLuaFile());

    TestNS::CoordsMatchingNS::ModelSettings model_settings;
    model_settings.Init();

    auto& manager = Internal::MeshNS::CoordsMatchingManager::CreateOrGetInstance();
    Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);

    decltype(auto) coords_matching = manager.GetCoordsMatching(source_mesh_id, target_mesh_id);
    decltype(auto) reverse_coords_matching = manager.GetCoordsMatching(target_mesh_id, source_mesh_id);

    BOOST_CHECK_EQUAL(coords_matching.FindSourceIndex(CoordsNS::index_from_mesh_file{ 11 }),
                      CoordsNS::index_from_mesh_file{ 1 });
    BOOST_CHECK_EQUAL(coords_matching.FindSourceIndex(CoordsNS::index_from_mesh_file{ 12 }),
                      CoordsNS::index_from_mesh_file{ 2 });
    BOOST_CHECK_EQUAL(coords_matching.FindSourceIndex(CoordsNS::index_from_mesh_file{ 13 }),
                      CoordsNS::index_from_mesh_file{ 3 });

    BOOST_CHECK_EQUAL(coords_matching.FindTargetIndex(CoordsNS::index_from_mesh_file{ 1 }),
                      CoordsNS::index_from_mesh_file{ 11 });
    BOOST_CHECK_EQUAL(coords_matching.FindTargetIndex(CoordsNS::index_from_mesh_file{ 2 }),
                      CoordsNS::index_from_mesh_file{ 12 });
    BOOST_CHECK_EQUAL(coords_matching.FindTargetIndex(CoordsNS::index_from_mesh_file{ 3 }),
                      CoordsNS::index_from_mesh_file{ 13 });

    BOOST_CHECK_EQUAL(reverse_coords_matching.FindSourceIndex(CoordsNS::index_from_mesh_file{ 1 }),
                      CoordsNS::index_from_mesh_file{ 11 });
    BOOST_CHECK_EQUAL(reverse_coords_matching.FindSourceIndex(CoordsNS::index_from_mesh_file{ 2 }),
                      CoordsNS::index_from_mesh_file{ 12 });
    BOOST_CHECK_EQUAL(reverse_coords_matching.FindSourceIndex(CoordsNS::index_from_mesh_file{ 3 }),
                      CoordsNS::index_from_mesh_file{ 13 });

    BOOST_CHECK_EQUAL(reverse_coords_matching.FindTargetIndex(CoordsNS::index_from_mesh_file{ 11 }),
                      CoordsNS::index_from_mesh_file{ 1 });
    BOOST_CHECK_EQUAL(reverse_coords_matching.FindTargetIndex(CoordsNS::index_from_mesh_file{ 12 }),
                      CoordsNS::index_from_mesh_file{ 2 });
    BOOST_CHECK_EQUAL(reverse_coords_matching.FindTargetIndex(CoordsNS::index_from_mesh_file{ 13 }),
                      CoordsNS::index_from_mesh_file{ 3 });
}


BOOST_AUTO_TEST_CASE(reverse_without_flag)
{
    const auto mesh_ids =
        std::make_pair(MeshNS::unique_id{ 2ul }, MeshNS::unique_id{ 40ul }); // must differ from the one in first test

    const auto& source_mesh_id = mesh_ids.first;
    const auto& target_mesh_id = mesh_ids.second;

    TestData generated_data("reverse_without_flag", mesh_ids, { 1, 2, 3 }, { 11, 12, 13 }, compute_reverse::no);

    TestNS::CoordsMatchingNS::input_data_type input_data(empty_model_settings, generated_data.GetLuaFile());

    TestNS::CoordsMatchingNS::ModelSettings model_settings;
    model_settings.Init();

    auto& manager = Internal::MeshNS::CoordsMatchingManager::CreateOrGetInstance();
    Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);

    manager.GetCoordsMatching(source_mesh_id, target_mesh_id);

    BOOST_CHECK_THROW(manager.GetCoordsMatching(target_mesh_id, source_mesh_id), Exception);
}


BOOST_AUTO_TEST_SUITE_END()


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
