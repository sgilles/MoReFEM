// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_GEOMETRY_COORDSMATCHING_ANONYMOUSTESTDATA_DOT_HPP_
#define MOREFEM_TEST_GEOMETRY_COORDSMATCHING_ANONYMOUSTESTDATA_DOT_HPP_
// *** MoReFEM header guards *** < //

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{

    enum class print_meshes_line { no, yes, botched };

    enum class compute_reverse { no, yes };


    /*!
     * \brief Generate on the fly in the test directory a Lua file and a simple interpolation file from given vectors.
     */
    class TestData
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] moniker A moniker for the test which will be used in creating a dedicated subdirectory (so that a test doesn't overwrite the directory
         * of the previous one).
         */
        TestData(std::string_view moniker,
                 std::pair<MoReFEM::MeshNS::unique_id, MoReFEM::MeshNS::unique_id> mesh_ids,
                 std::vector<std::size_t>&& source_values,
                 std::vector<std::size_t>&& target_values,
                 compute_reverse do_compute_reverse = compute_reverse::no,
                 print_meshes_line do_print_meshes_line = print_meshes_line::yes);

        const MoReFEM::FilesystemNS::File& GetLuaFile() const noexcept;

        MoReFEM::MeshNS::InterpolationNS::CoordsMatching::const_unique_ptr GenerateCoordsMatching();

      private:
        MoReFEM::FilesystemNS::File lua_file_;

        std::vector<MoReFEM::CoordsNS::index_from_mesh_file> source_values_;

        std::vector<MoReFEM::CoordsNS::index_from_mesh_file> target_values_;

        MoReFEM::FilesystemNS::File path_;
    };


    struct Fixture : public MoReFEM::TestNS::FixtureNS::TestEnvironment
    { };


    using IntToStrongType =
        MoReFEM::Utilities::StaticCast<std::vector<std::size_t>, std::vector<MoReFEM::CoordsNS::index_from_mesh_file>>;

} // namespace


namespace // anonymous
{


    TestData::TestData(std::string_view moniker,
                       std::pair<MoReFEM::MeshNS::unique_id, MoReFEM::MeshNS::unique_id> mesh_ids,
                       std::vector<std::size_t>&& source_values,
                       std::vector<std::size_t>&& target_values,
                       compute_reverse do_compute_reverse,
                       print_meshes_line do_print_meshes_line)
    : source_values_(IntToStrongType::Perform(source_values)), target_values_(IntToStrongType::Perform(target_values))
    {
        decltype(auto) environment = MoReFEM::Utilities::Environment::GetInstance();

        // Circumvent the fact Directory can't create non mpi directories - with good reasons!
        std::filesystem::path directory_path{ environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR") };

        directory_path /= "Geometry/CoordsMatching";

        if (do_compute_reverse == compute_reverse::yes)
            directory_path /= "Reverse";
        else
            directory_path /= "Straight";

        directory_path /= std::filesystem::path(moniker);

        std::filesystem::create_directories(directory_path);

        MoReFEM::FilesystemNS::Directory test_directory(directory_path, MoReFEM::FilesystemNS::behaviour::ignore);

        BOOST_CHECK(test_directory.DoExist());

        std::ostringstream oconv;
        lua_file_ = test_directory.AddFile("demo.lua");

        {
            MoReFEM::FilesystemNS::File file(lua_file_);
            std::ofstream out{ file.NewContent() };

            out << "CoordsMatchingFile1 = {" << std::endl;

            path_ = test_directory.AddFile("test.hhdata");

            out << "path = \"" << path_ << "\"," << std::endl;
            out << "do_compute_reverse = " << (do_compute_reverse == compute_reverse::yes ? "true" : "false")
                << std::endl;
            out << "}" << std::endl;

            out.close();
        }

        {
            MoReFEM::FilesystemNS::File file{ test_directory.AddFile("test.hhdata") };
            std::ofstream out{ file.NewContent() };

            switch (do_print_meshes_line)
            {
            case print_meshes_line::yes:
                out << "Meshes: " << mesh_ids.first << ' ' << mesh_ids.second << std::endl;
                break;
            case print_meshes_line::no:
                break;
            case print_meshes_line::botched:
                out << "Meshes: " << mesh_ids.first << ' ' << mesh_ids.second << " 5" << std::endl;
                break;
            }

            const auto source_size = source_values.size();
            const auto target_size = target_values.size();
            const auto min_size = std::min(source_size, target_size);

            for (auto i = 0ul; i < min_size; ++i)
            {
                out << source_values[i] << " " << target_values[i] << std::endl;
            }

            if (source_size > min_size)
            {
                for (auto i = min_size; i < source_size; ++i)
                    out << source_values[i] << "  " << std::endl;
            } else if (target_size > min_size)
            {
                for (auto i = min_size; i < target_size; ++i)
                    out << "  " << target_values[i] << std::endl;
            }

            out.close();
        }
    }


    const MoReFEM::FilesystemNS::File& TestData::GetLuaFile() const noexcept
    {
        return lua_file_;
    }


    MoReFEM::MeshNS::InterpolationNS::CoordsMatching::const_unique_ptr TestData::GenerateCoordsMatching()
    {
        return std::make_unique<const MoReFEM::MeshNS::InterpolationNS::CoordsMatching>(path_);
    }


} // namespace

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_GEOMETRY_COORDSMATCHING_ANONYMOUSTESTDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
