LightweightDomainList is a shorthand to define more quickly several domains that are defined only by a mesh and a mesh
label; this test simply checks the domains hence defined are correctly formed.
