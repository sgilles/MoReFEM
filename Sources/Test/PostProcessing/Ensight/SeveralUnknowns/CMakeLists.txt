add_executable(TestPostProcessingEnsightSeveralUnknowns)

target_sources(TestPostProcessingEnsightSeveralUnknowns
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp     
        ${CMAKE_CURRENT_LIST_DIR}/README.md
)
          
target_link_libraries(TestPostProcessingEnsightSeveralUnknowns
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>
                      ${MOREFEM_POST_PROCESSING}
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})
    
morefem_organize_IDE(TestPostProcessingEnsightSeveralUnknowns Test/PostProcessing/Ensight Test/PostProcessing/Ensight/SeveralUnknowns)
    
morefem_boost_test_sequential_mode(NAME PostProcessingEnsightSeveralUnknowns
                                   EXE TestPostProcessingEnsightSeveralUnknowns
                                   TIMEOUT 20)
                      
