// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#define BOOST_TEST_MODULE symmetrize_local_matrix
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Operators/LocalVariationalOperator/Advanced/SymmetrizeMatrix.hpp"

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

using namespace MoReFEM;

BOOST_AUTO_TEST_CASE(symmetrize_matrix)
{
    constexpr auto size = 5ul;

    LocalMatrix matrix({ size, size });
    matrix.fill(0.);

    for (auto row = 0ul; row < size; ++row)
    {
        for (auto col = row; col < size; ++col)
            matrix(row, col) = static_cast<double>(1 + row + 3 * col);
    }

    Advanced::LocalVariationalOperatorNS::SymmetrizeMatrix(matrix);

    for (auto row = 0ul; row < size; ++row)
        for (auto col = 0ul; col < size; ++col)
            BOOST_CHECK_EQUAL(matrix(row, col), matrix(col, row));
}

PRAGMA_DIAGNOSTIC(pop)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
