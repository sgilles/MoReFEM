// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include "Test/Operators/VariationalInstances/Microsphere/Model.hpp"


namespace MoReFEM::TestNS::Microsphere
{

    Model::Model(morefem_data_type& morefem_data) : parent(morefem_data, create_domain_list_for_coords::yes)
    { }


    void Model::SupplInitialize()
    {
        const auto& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));
        decltype(auto) morefem_data = parent::GetNonCstMoReFEMData();

        const auto& felt_space_volume = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::highest_dimension));

        {
            const DirichletBoundaryConditionManager& bc_manager = DirichletBoundaryConditionManager::GetInstance();

            auto&& bc_list = {
                bc_manager.GetDirichletBoundaryConditionPtr(AsBoundaryConditionId(BoundaryConditionIndex::edge1)),
                bc_manager.GetDirichletBoundaryConditionPtr(AsBoundaryConditionId(BoundaryConditionIndex::edge2)),
                bc_manager.GetDirichletBoundaryConditionPtr(AsBoundaryConditionId(BoundaryConditionIndex::edge3))
            };

            const auto& unknown_manager = UnknownManager::GetInstance();

            const auto& displacement = unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::displacement));

            variational_formulation_ = std::make_unique<VariationalFormulation>(
                felt_space_volume.GetNumberingSubset(displacement), god_of_dof, std::move(bc_list), morefem_data);
        }

        auto& variational_formulation = GetNonCstVariationalFormulation();

        variational_formulation.Init(morefem_data);

        const auto& displacement_numbering_subset = variational_formulation.GetDisplacementNumberingSubset();

        const auto& mpi = GetMpi();

        Wrappers::Petsc::PrintMessageOnFirstProcessor("\n----------------------------------------------\n", mpi);

        Wrappers::Petsc::PrintMessageOnFirstProcessor("Static problem\n", mpi);

        Wrappers::Petsc::PrintMessageOnFirstProcessor("----------------------------------------------\n", mpi);


        variational_formulation.SolveNonLinear(displacement_numbering_subset, displacement_numbering_subset);

        variational_formulation.WriteSolution(GetTimeManager(), displacement_numbering_subset);

        variational_formulation.PrepareDynamicRuns();
    }


    void Model::SupplInitializeStep()
    { }


    void Model::Forward()
    {
        VariationalFormulation& variational_formulation = GetNonCstVariationalFormulation();
        const NumberingSubset& displacement_numbering_subset = variational_formulation.GetDisplacementNumberingSubset();

        variational_formulation.SolveNonLinear(displacement_numbering_subset, displacement_numbering_subset);
    }


    void Model::SupplFinalizeStep()
    {
        auto& variational_formulation = GetNonCstVariationalFormulation();
        const auto& displacement_numbering_subset = variational_formulation.GetDisplacementNumberingSubset();

        variational_formulation.WriteSolution(GetTimeManager(), displacement_numbering_subset);
        variational_formulation.UpdateForNextTimeStep();
    }


    void Model::SupplFinalize()
    { }


} // namespace MoReFEM::TestNS::Microsphere


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
