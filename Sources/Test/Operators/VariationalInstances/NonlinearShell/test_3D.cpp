// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#define BOOST_TEST_MODULE nonlinear_shell_operator
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/Model.hpp"

#include "Test/Operators/VariationalInstances/NonlinearShell/InputData.hpp"
#include "Test/Operators/VariationalInstances/NonlinearShell/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    // clang-format off
      using fixture_type = TestNS::FixtureNS::Model
      <
         TestNS::NonlinearShell::Model
      >;
    // clang-format on

    using ::MoReFEM::Internal::assemble_into_matrix;

    using ::MoReFEM::Internal::assemble_into_vector;

} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__


BOOST_FIXTURE_TEST_SUITE(same_unknown_for_test, fixture_type)

BOOST_AUTO_TEST_CASE(matrix_only)
{
    GetModel().SameUnknown(assemble_into_matrix::yes, assemble_into_vector::no);
}


BOOST_AUTO_TEST_CASE(vector_only)
{
    GetModel().SameUnknown(assemble_into_matrix::no, assemble_into_vector::yes);
}


BOOST_AUTO_TEST_CASE(matrix_and_vector)
{
    GetModel().SameUnknown(assemble_into_matrix::yes, assemble_into_vector::yes);
}

BOOST_AUTO_TEST_SUITE_END()


BOOST_FIXTURE_TEST_SUITE(different_unknown, fixture_type)

BOOST_AUTO_TEST_CASE(unknown_p1_test_p1)
{
    GetModel().UnknownP1TestP1();
}

BOOST_AUTO_TEST_CASE(unknown_p2_test_p1)
{
    GetModel().UnknownP2TestP1();
}


BOOST_AUTO_TEST_SUITE_END()

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
