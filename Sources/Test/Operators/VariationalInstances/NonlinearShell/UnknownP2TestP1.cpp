// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/VariationalInstances/NonlinearShell/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/NonlinearShell/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM::TestNS::NonlinearShell
{


    void Model::UnknownP2TestP1() const
    {
        const auto& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        const auto& displacement_p1_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::displacement_p1));
        const auto& displacement_p2_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::displacement_p2));

        NonlinearShellType shell_operator(felt_space,
                                          displacement_p2_ptr,
                                          displacement_p1_ptr,
                                          hyperelastic_law_parent::GetHyperelasticLawPtr(),
                                          nullptr);


        decltype(auto) numbering_subset_p1 =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement_p1));
        decltype(auto) numbering_subset_p2 =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacement_p2));

        GlobalMatrix matrix_p1_p2(numbering_subset_p1, numbering_subset_p2);
        AllocateGlobalMatrix(god_of_dof, matrix_p1_p2);

        GlobalVector vector_p1(numbering_subset_p1);
        AllocateGlobalVector(god_of_dof, vector_p1);

        GlobalMatrix matrix_p2_p2(numbering_subset_p2, numbering_subset_p2);
        AllocateGlobalMatrix(god_of_dof, matrix_p2_p2);

        GlobalVector vector_p2(numbering_subset_p2);
        AllocateGlobalVector(god_of_dof, vector_p2);

        GlobalVector previous_iteration_p2(numbering_subset_p2);
        AllocateGlobalVector(god_of_dof, previous_iteration_p2);

        const auto& mesh = god_of_dof.GetMesh();
        const auto dimension = mesh.GetDimension();

        {
            matrix_p1_p2.ZeroEntries();
            vector_p1.ZeroEntries();
            previous_iteration_p2.ZeroEntries();

            previous_iteration_p2.SetValue(3, 0.1, INSERT_VALUES);
            previous_iteration_p2.SetValue(6, 0.1, INSERT_VALUES);
            previous_iteration_p2.SetValue(7, 0.1, INSERT_VALUES);
            previous_iteration_p2.SetValue(10, 0.1, INSERT_VALUES);
        }

        previous_iteration_p2.Assembly();

        GlobalMatrixWithCoefficient matrix(matrix_p1_p2, 1.);
        GlobalVectorWithCoefficient vec(vector_p1, 1.);

        shell_operator.Assemble(std::make_tuple(std::ref(matrix), std::ref(vec)), previous_iteration_p2);

        /* BOOST_CHECK_NO_THROW */ (CheckVector(god_of_dof, vector_p1, GetExpectedVectorP2P1(dimension), 1.e-5));

        /* BOOST_CHECK_NO_THROW */ (CheckMatrix(god_of_dof, matrix_p1_p2, GetExpectedMatrixP2P1(dimension), 1.e-5));
    }


} // namespace MoReFEM::TestNS::NonlinearShell


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
