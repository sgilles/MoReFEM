// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_NONLINEARSHELL_MODEL_DOT_HXX_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_NONLINEARSHELL_MODEL_DOT_HXX_
// IWYU pragma: private, include "Test/Operators/VariationalInstances/NonlinearShell/Model.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Test/Operators/VariationalInstances/NonlinearShell/Model.hpp"


namespace MoReFEM::TestNS::NonlinearShell
{


    inline const std::string& Model::ClassName()
    {
        static std::string name("Test NonlinearShell operator");
        return name;
    }


} // namespace MoReFEM::TestNS::NonlinearShell


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_NONLINEARSHELL_MODEL_DOT_HXX_
// *** MoReFEM end header guards *** < //
