// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/NonlinearMembrane.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"


#include "Test/Operators/VariationalInstances/NonlinearMembrane/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/NonlinearMembrane/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM::TestNS::NonLinearMembraneOperatorNS
{


    namespace // anonymous
    {


        using ::MoReFEM::Internal::assemble_into_matrix;

        using ::MoReFEM::Internal::assemble_into_vector;

        const double thickness_value = 4.2375;

        const double pretension_value = 17.984152;


    } // namespace


    void Model::TestP1P1(pretension is_pretension,
                         assemble_into_matrix do_assemble_into_matrix,
                         assemble_into_vector do_assemble_into_vector) const
    {
        const auto& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) morefem_data = parent::GetMoReFEMData();

        const auto& displacement_numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::displacementP1));

        const auto& felt_space_surface = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::surface));

        decltype(auto) domain_full_mesh = DomainManager::GetInstance().GetDomain(AsDomainId(DomainIndex::full_mesh));

        Solid<time_manager_type> solid(
            morefem_data, domain_full_mesh, felt_space_surface.GetQuadratureRulePerTopology());

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        const auto& displacement_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::displacementP1));

        namespace GVO = GlobalVariationalOperatorNS;

        using scalar_parameter_type = Internal::ParameterNS::ParameterInstance<ParameterNS::Type::scalar,
                                                                               ::MoReFEM::ParameterNS::Policy::Constant,
                                                                               time_manager_type,
                                                                               ParameterNS::TimeDependencyNS::None>;

        decltype(auto) domain_manager = DomainManager::GetInstance();

        decltype(auto) domain_surface = domain_manager.GetDomain(AsDomainId(DomainIndex::surface));

        scalar_parameter_type thickness("Thickness", domain_surface, thickness_value);

        const double pretension_to_use = is_pretension == pretension::yes ? pretension_value : 0.;

        scalar_parameter_type pretension_param("Pretension", domain_surface, pretension_to_use);

        GVO::NonlinearMembrane<time_manager_type> stiffness_operator(felt_space_surface,
                                                                     displacement_ptr,
                                                                     displacement_ptr,
                                                                     solid.GetYoungModulus(),
                                                                     solid.GetPoissonRatio(),
                                                                     thickness,
                                                                     pretension_param);

        GlobalMatrix matrix_tangent_stiffness(displacement_numbering_subset, displacement_numbering_subset);
        AllocateGlobalMatrix(god_of_dof, matrix_tangent_stiffness);

        GlobalVector vector_stiffness_residual(displacement_numbering_subset);
        AllocateGlobalVector(god_of_dof, vector_stiffness_residual);

        GlobalVector previous_iteration(vector_stiffness_residual);

        {
            Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(previous_iteration);

            content[0] = 30.;
            content[1] = -42.;
            content[2] = -17.;
            content[3] = 10.;
            content[4] = 97.;
            content[5] = 41.;
            content[6] = 5.;
            content[7] = -84.;
            content[8] = -20.5;
        }

        GlobalMatrixWithCoefficient mat(matrix_tangent_stiffness, 1.);
        GlobalVectorWithCoefficient vec(vector_stiffness_residual, 1.);

        if (do_assemble_into_matrix == assemble_into_matrix::yes
            && do_assemble_into_vector == assemble_into_vector::yes)
        {
            stiffness_operator.Assemble(std::make_tuple(std::ref(mat), std::ref(vec)), previous_iteration);

            CheckMatrix(god_of_dof, matrix_tangent_stiffness, GetExpectedMatrixP1P1(is_pretension), 1.);

            CheckVector(god_of_dof, vector_stiffness_residual, GetExpectedVectorP1P1(is_pretension), 1.);
        } else if (do_assemble_into_matrix == assemble_into_matrix::yes
                   && do_assemble_into_vector == assemble_into_vector::no)
        {
            stiffness_operator.Assemble(std::make_tuple(std::ref(mat)), previous_iteration);

            CheckMatrix(god_of_dof, matrix_tangent_stiffness, GetExpectedMatrixP1P1(is_pretension), 1.);
        } else if (do_assemble_into_matrix == assemble_into_matrix::no
                   && do_assemble_into_vector == assemble_into_vector::yes)
        {
            stiffness_operator.Assemble(std::make_tuple(std::ref(vec)), previous_iteration);

            CheckVector(god_of_dof, vector_stiffness_residual, GetExpectedVectorP1P1(is_pretension), 1.);
        }
    }


} // namespace MoReFEM::TestNS::NonLinearMembraneOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
