// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

/*!
// \file
//
//
 on the Thu, 5 Mar 2020 14:30:17 +0100
// Copyright (c) Inria. All rights reserved.
//
*/


#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/VariationalInstances/QuasiIncompressiblePk2/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/QuasiIncompressiblePk2/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM::TestNS::QuasiIncompressiblePk2
{


    namespace // anonymous
    {


        using ::MoReFEM::Internal::assemble_into_matrix;

        using ::MoReFEM::Internal::assemble_into_vector;


    } // namespace


    void Model::DifferentCauchyGreen(assemble_into_matrix do_assemble_into_matrix,
                                     assemble_into_vector do_assemble_into_vector) const
    {
        const auto& god_of_dof = GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::monolithic));

        const auto& displacement_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::displacement));
        const auto& pressure_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::pressure));

        const std::array<Unknown::const_shared_ptr, 2> displacement_pressure{ { displacement_ptr, pressure_ptr } };

        DifferentCauchyGreenIncompressibleStiffnessOperatorType incompressible_stiffness_operator(
            felt_space,
            displacement_pressure,
            displacement_pressure,
            *solid_,
            GetTimeManager(),
            hyperelastic_deviatoric_law_parent::GetHyperelasticLawPtr(),
            penalization_volumetric_.get(),
            penalization_deviatoric_.get(),
            nullptr,
            nullptr);


        decltype(auto) monolithic_numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::monolithic));

        GlobalMatrix matrix_p1b_p1(monolithic_numbering_subset, monolithic_numbering_subset);
        AllocateGlobalMatrix(god_of_dof, matrix_p1b_p1);

        GlobalVector vector_p1b_p1(monolithic_numbering_subset);
        AllocateGlobalVector(god_of_dof, vector_p1b_p1);

        GlobalVector previous_iteration_p1b_p1(vector_p1b_p1);

        const auto& mesh = god_of_dof.GetMesh();
        const auto dimension = mesh.GetDimension();

        {
            matrix_p1b_p1.ZeroEntries();
            vector_p1b_p1.ZeroEntries();
            previous_iteration_p1b_p1.ZeroEntries();

            switch (dimension)
            {
            case 3:
                previous_iteration_p1b_p1.SetValue(3, 2., INSERT_VALUES);
                [[fallthrough]];
            case 2:
                previous_iteration_p1b_p1.SetValue(2, 2., INSERT_VALUES);
                [[fallthrough]];
            case 1:
                previous_iteration_p1b_p1.SetValue(0, -1., INSERT_VALUES);
                previous_iteration_p1b_p1.SetValue(1, -2., INSERT_VALUES);
                break;
            }

            previous_iteration_p1b_p1.Assembly();

            GlobalMatrixWithCoefficient matrix(matrix_p1b_p1, 1.);
            GlobalVectorWithCoefficient vec(vector_p1b_p1, 1.);

            if (do_assemble_into_matrix == assemble_into_matrix::yes
                && do_assemble_into_vector == assemble_into_vector::yes)
            {
                incompressible_stiffness_operator.Assemble(std::make_tuple(std::ref(matrix), std::ref(vec)),
                                                           MonolithicDeviatoricGlobalVector(previous_iteration_p1b_p1));


                /* BOOST_CHECK_NO_THROW */ (
                    CheckMatrix(god_of_dof, matrix_p1b_p1, GetExpectedMatrixP1bP1(dimension), 1.e2));

                /* BOOST_CHECK_NO_THROW */ (
                    CheckVector(god_of_dof, vector_p1b_p1, GetExpectedVectorP1bP1(dimension), 1.e2));

                matrix_p1b_p1.ZeroEntries();
                vector_p1b_p1.ZeroEntries();

                incompressible_stiffness_operator.Assemble(std::make_tuple(std::ref(matrix), std::ref(vec)),
                                                           MonolithicDeviatoricGlobalVector(previous_iteration_p1b_p1),
                                                           MonolithicVolumetricGlobalVector(previous_iteration_p1b_p1));

                /* BOOST_CHECK_NO_THROW */ (
                    CheckMatrix(god_of_dof, matrix_p1b_p1, GetExpectedMatrixP1bP1(dimension), 1.e2));

                /* BOOST_CHECK_NO_THROW */ (
                    CheckVector(god_of_dof, vector_p1b_p1, GetExpectedVectorP1bP1(dimension), 1.e2));
            } else if (do_assemble_into_matrix == assemble_into_matrix::yes
                       && do_assemble_into_vector == assemble_into_vector::no)
            {
                incompressible_stiffness_operator.Assemble(std::make_tuple(std::ref(matrix)),
                                                           MonolithicDeviatoricGlobalVector(previous_iteration_p1b_p1));

                /* BOOST_CHECK_NO_THROW */ (
                    CheckMatrix(god_of_dof, matrix_p1b_p1, GetExpectedMatrixP1bP1(dimension), 1.e2));

                matrix_p1b_p1.ZeroEntries();

                incompressible_stiffness_operator.Assemble(std::make_tuple(std::ref(matrix)),
                                                           MonolithicDeviatoricGlobalVector(previous_iteration_p1b_p1),
                                                           MonolithicVolumetricGlobalVector(previous_iteration_p1b_p1));

                /* BOOST_CHECK_NO_THROW */ (
                    CheckMatrix(god_of_dof, matrix_p1b_p1, GetExpectedMatrixP1bP1(dimension), 1.e2));
            } else if (do_assemble_into_matrix == assemble_into_matrix::no
                       && do_assemble_into_vector == assemble_into_vector::yes)
            {
                incompressible_stiffness_operator.Assemble(std::make_tuple(std::ref(vec)),
                                                           MonolithicDeviatoricGlobalVector(previous_iteration_p1b_p1));

                /* BOOST_CHECK_NO_THROW */ (
                    CheckVector(god_of_dof, vector_p1b_p1, GetExpectedVectorP1bP1(dimension), 1.e2));

                vector_p1b_p1.ZeroEntries();

                incompressible_stiffness_operator.Assemble(std::make_tuple(std::ref(vec)),
                                                           MonolithicDeviatoricGlobalVector(previous_iteration_p1b_p1),
                                                           MonolithicVolumetricGlobalVector(previous_iteration_p1b_p1));

                /* BOOST_CHECK_NO_THROW */ (
                    CheckVector(god_of_dof, vector_p1b_p1, GetExpectedVectorP1bP1(dimension), 1.e2));
            }
        }
    }


} // namespace MoReFEM::TestNS::QuasiIncompressiblePk2


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
