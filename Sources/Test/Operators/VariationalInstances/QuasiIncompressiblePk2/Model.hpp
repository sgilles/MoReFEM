// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_QUASIINCOMPRESSIBLEPK2_MODEL_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_QUASIINCOMPRESSIBLEPK2_MODEL_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Utilities/TimeKeep/TimeKeep.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/None.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/None.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/Viscoelasticity.hpp"

#include "OperatorInstances/HyperelasticLaws/CiarletGeymonatDeviatoric.hpp"
#include "OperatorInstances/HyperelasticLaws/LogI3Penalization.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/QuasiIncompressibleCauchyGreenPolicy/DifferentCauchyGreenMixedSolidIncompressibility.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/QuasiIncompressibleCauchyGreenPolicy/SameCauchyGreenMixedSolidIncompressibility.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/QuasiIncompressibleSecondPiolaKirchhoffStressTensor.hpp"

#include "FormulationSolver/Crtp/HyperelasticLaw.hpp"
#include "FormulationSolver/Crtp/Penalization.hpp"

#include "Model/Model.hpp"

#include "Test/Operators/VariationalInstances/QuasiIncompressiblePk2/InputData.hpp"


namespace MoReFEM::TestNS::QuasiIncompressiblePk2
{


    //! \copydoc doxygen_hide_model_4_test
    class Model
    : public ::MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>,
      public FormulationSolverNS::HyperelasticLaw<Model,
                                                  HyperelasticLawNS::CiarletGeymonatDeviatoric<time_manager_type>>
    {

      private:
        //! \copydoc doxygen_hide_alias_self
        using self = Model;

        //! Convenient alias.
        using parent = MoReFEM::Model<Model, morefem_data_type, DoConsiderProcessorWiseLocal2Global::yes>;

        static_assert(std::is_convertible<self*, parent*>());

      public:
        //! Return the name of the model.
        static const std::string& ClassName();

        //! Friendship granted to the base class so this one can manipulates private methods.
        friend parent;

        //! Alias to hyperlastic law parent,
        using hyperelastic_deviatoric_law_parent =
            FormulationSolverNS::HyperelasticLaw<self, HyperelasticLawNS::CiarletGeymonatDeviatoric<time_manager_type>>;

        //! Alias to the viscoelasticity policy used.
        using ViscoelasticityPolicy =
            GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None<
                time_manager_type>;

        //! Alias to the active stress policy used.
        using InternalVariablePolicy =
            GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::None<
                time_manager_type>;

        //! Alias to the hyperelasticity policy used for the deviatoric part.
        using hyperelasticity_deviatoric_policy =
            GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS ::Hyperelasticity<
                typename hyperelastic_deviatoric_law_parent::hyperelastic_law_type>;

        //! Policy used to have a different cauchy green on the volumetric and deviatoric parts.
        template<class U>
        using DifferentCauchyGreenMixedSolidIncompressibilityPolicy =
            GlobalVariationalOperatorNS::DifferentCauchyGreenMixedSolidIncompressibility<U, time_manager_type>;

        //! Policy used to have the same cauchy green on the volumetric and deviatoric parts.
        template<class U>
        using SameCauchyGreenMixedSolidIncompressibilityPolicy =
            GlobalVariationalOperatorNS::SameCauchyGreenMixedSolidIncompressibility<U, time_manager_type>;

        //! Alias on a pair of Unknown.
        using UnknownPair = std::pair<const Unknown&, const Unknown&>;

        //! Strong type for displacement global vectors.
        using DisplacementGlobalVector =
            StrongType<const GlobalVector&, struct MoReFEM::GlobalVariationalOperatorNS::DisplacementTag>;

        //! Strong type for deviatoric part of monolithic global vectors.
        using MonolithicDeviatoricGlobalVector =
            StrongType<const GlobalVector&, struct MoReFEM::GlobalVariationalOperatorNS::MonolithicDeviatoricTag>;

        //! Strong type for volumetric part of monolithic global vectors.
        using MonolithicVolumetricGlobalVector =
            StrongType<const GlobalVector&, struct MoReFEM::GlobalVariationalOperatorNS::MonolithicVolumetricTag>;

        //! Alias for penalization law.
        using penalization_law_type = HyperelasticLawNS::LogI3Penalization<time_manager_type>;

      public:
        // clang-format off
        //! Alias to the QuasiIncompressibleSecondPiolaKirchhoff type.
        using DifferentCauchyGreenIncompressibleStiffnessOperatorType =
            GlobalVariationalOperatorNS::QuasiIncompressibleSecondPiolaKirchhoffStressTensor
            <
                time_manager_type,
                hyperelasticity_deviatoric_policy,
                ViscoelasticityPolicy,
                InternalVariablePolicy,
                penalization_law_type,
                DifferentCauchyGreenMixedSolidIncompressibilityPolicy
            >;

        //! Alias to the QuasiIncompressibleSecondPiolaKirchhoff type.
        using SameCauchyGreenIncompressibleStiffnessOperatorType =
            GlobalVariationalOperatorNS::QuasiIncompressibleSecondPiolaKirchhoffStressTensor
            <
                time_manager_type,
                hyperelasticity_deviatoric_policy,
                ViscoelasticityPolicy,
                InternalVariablePolicy,
                penalization_law_type,
                SameCauchyGreenMixedSolidIncompressibilityPolicy
            >;
        // clang-format on

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_morefem_data_arg_inout
         */
        Model(morefem_data_type& morefem_data);

        //! Destructor.
        ~Model() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Model(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Model(Model&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Model& operator=(const Model& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Model& operator=(Model&& rhs) = delete;

        ///@}


        /// \name Crtp-required methods.
        ///@{

        //! Manage the change of time iteration.
        void Forward();

      private:
        /*!
         * \brief Initialise the problem.
         *
         * This initialisation includes the resolution of the static problem.
         */
        void SupplInitialize();

        /*!
         * \brief Additional operations to finalize a dynamic step.
         *
         * Base class already update the time for next time iterations.
         */
        void SupplFinalizeStep();


        /*!
         * \brief Initialise a dynamic step.
         *
         */
        void SupplFinalize();


      public:
        /*!
         * \brief Case when the test function uses up the same \a Unknown.
         *
         * \param[in] do_assemble_into_matrix Whether we're assembling into a matrix or not.
         * \param[in] do_assemble_into_vector Whether we're assembling into a vector or not.
         *
         * The three cases yes/yes, yes/no and no/yes are considered in the unit tests.
         */
        void DifferentCauchyGreen(::MoReFEM::Internal::assemble_into_matrix do_assemble_into_matrix,
                                  ::MoReFEM::Internal::assemble_into_vector do_assemble_into_vector) const;

        /*!
         * \brief Case when the test function uses up the same \a Unknown.
         *
         * \param[in] do_assemble_into_matrix Whether we're assembling into a matrix or not.
         * \param[in] do_assemble_into_vector Whether we're assembling into a vector or not.
         *
         * The three cases yes/yes, yes/no and no/yes are considered in the unit tests.
         */
        void SameCauchyGreen(::MoReFEM::Internal::assemble_into_matrix do_assemble_into_matrix,
                             ::MoReFEM::Internal::assemble_into_vector do_assemble_into_vector) const;

      private:
        //! \copydoc doxygen_hide_model_SupplHasFinishedConditions_always_true
        bool SupplHasFinishedConditions() const;


        /*!
         * \brief Part of InitializedStep() specific to Elastic model.
         *
         * As there are none, the body of this method is empty.
         */
        void SupplInitializeStep();

        ///@}

      private:
        //! Penalization - volumetric part
        const penalization_law_type& GetPenalizationVolumetricPart() const noexcept;

        //! Penalization - deviatoric part
        const penalization_law_type& GetPenalizationVDeviatoricPart() const noexcept;

        //! Penalization - volumetric part
        penalization_law_type& GetNonCstPenalizationVolumetricPart() noexcept;

        //! Penalization - deviatoric part
        penalization_law_type& GetNonCstPenalizationVDeviatoricPart() noexcept;

      private:
        //! Young modulus.
        ScalarParameter<time_manager_type>::unique_ptr young_modulus_ = nullptr;

        //! Poisson ratio.
        ScalarParameter<time_manager_type>::unique_ptr poisson_ratio_ = nullptr;

        //! Material parameters of the solid.
        typename Solid<time_manager_type>::const_unique_ptr solid_ = nullptr;

      private:
        //! Quadrature rule topology used for the operators.
        QuadratureRulePerTopology::const_unique_ptr quadrature_rule_per_topology_for_operators_ = nullptr;

        //! Penalization - volumetric part
        typename penalization_law_type::const_unique_ptr penalization_volumetric_{ nullptr };

        //! Penalization - deviatoric part
        typename penalization_law_type::const_unique_ptr penalization_deviatoric_{ nullptr };
    };


} // namespace MoReFEM::TestNS::QuasiIncompressiblePk2


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/Operators/VariationalInstances/QuasiIncompressiblePk2/Model.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_VARIATIONALINSTANCES_QUASIINCOMPRESSIBLEPK2_MODEL_DOT_HPP_
// *** MoReFEM end header guards *** < //
