// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstddef> // IWYU pragma: keep

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Operators/VariationalInstances/Mass/ExpectedResults.hpp"
#include "Test/Operators/VariationalInstances/Mass/Model.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM::TestNS::Mass
{


    void Model::SameUnknownP1(UnknownNS::Nature scalar_or_vectorial) const
    {
        const auto& god_of_dof = parent::GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        const auto unknown_id =
            AsUnknownId(scalar_or_vectorial == UnknownNS::Nature::scalar ? UnknownIndex::potential_P1
                                                                         : UnknownIndex::displacement_P1);

        const auto numbering_subset_id = AsNumberingSubsetId(scalar_or_vectorial == UnknownNS::Nature::scalar
                                                                 ? NumberingSubsetIndex::potential_P1
                                                                 : NumberingSubsetIndex::displacement_P1);


        const auto& unknown_ptr = unknown_manager.GetUnknownPtr(unknown_id);

        GlobalVariationalOperatorNS::Mass mass_op(felt_space, unknown_ptr, unknown_ptr);

        decltype(auto) numbering_subset = god_of_dof.GetNumberingSubset(numbering_subset_id);

        GlobalMatrix matrix(numbering_subset, numbering_subset);
        AllocateGlobalMatrix(god_of_dof, matrix);

        const auto& mesh = god_of_dof.GetMesh();
        const auto dimension = mesh.GetDimension();

        matrix.ZeroEntries();

        GlobalMatrixWithCoefficient matrix_with_coeff(matrix, 1.);

        mass_op.Assemble(std::make_tuple(std::ref(matrix_with_coeff)));

        /* BOOST_CHECK_NO_THROW */ (
            CheckMatrix(god_of_dof, matrix, GetExpectedMatrixP1P1(dimension, scalar_or_vectorial), 1.e-5));
    }


    void Model::SameUnknownP2() const
    {
        const auto& god_of_dof = parent::GetGodOfDof(AsMeshId(MeshIndex::mesh));

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        const auto& felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        const auto& unknown_ptr = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::potential_P2));

        GlobalVariationalOperatorNS::Mass mass_op(felt_space, unknown_ptr, unknown_ptr);

        decltype(auto) numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::potential_P2));

        GlobalMatrix matrix(numbering_subset, numbering_subset);
        AllocateGlobalMatrix(god_of_dof, matrix);

        const auto& mesh = god_of_dof.GetMesh();
        const auto dimension = mesh.GetDimension();

        matrix.ZeroEntries();

        GlobalMatrixWithCoefficient matrix_with_coeff(matrix, 1.);

        mass_op.Assemble(std::make_tuple(std::ref(matrix_with_coeff)));

        /* BOOST_CHECK_NO_THROW */ (CheckMatrix(god_of_dof, matrix, GetExpectedMatrixP2P2(dimension), 1.e-5));
    }


} // namespace MoReFEM::TestNS::Mass


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
