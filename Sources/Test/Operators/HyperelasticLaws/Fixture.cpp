// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include "Core/InputData/Advanced/SetFromInputData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Test/Tools/MoReFEMDataForTest.hpp"

#include "Geometry/Domain/DomainManager.hpp"

#include "OperatorInstances/ParameterOperator/UpdateCauchyGreenTensor.hpp"

#include "Test/Operators/HyperelasticLaws/Fixture.hpp"
#include "Test/Operators/HyperelasticLaws/ModelSettings.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/Fixture/ModelNoInputData.hpp"


namespace MoReFEM::TestNS::HyperelasticLawNS
{


    Fixture::Fixture()
    {
        decltype(auto) model = GetModel();


        // Set Mesh, and extract three geometric elements which will be used for tests.
        {
            decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance();
            decltype(auto) mesh = mesh_manager.GetMesh(AsMeshId(HyperelasticLawNS::MeshIndex::sole));

            decltype(auto) geom_elt_factory = Advanced::GeometricEltFactory::GetInstance();

            {
                decltype(auto) ref_tetra4 = geom_elt_factory.GetRefGeomElt(Advanced::GeometricEltEnum::Tetrahedron4);
                auto [begin, end] = mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_tetra4);
                assert(end - begin == 1);
                tetra_ = *begin;
            }
        }

        // Set Domain
        decltype(auto) domain_manager = DomainManager::GetInstance();
        decltype(auto) domain = domain_manager.GetDomain(AsDomainId(HyperelasticLawNS::DomainIndex::volume));

        // Set Solid
        quadrature_rule_per_topology_ = std::make_unique<QuadratureRulePerTopology>(3, 2);

        solid_ =
            std::make_unique<Solid<time_manager_type>>(model.GetMoReFEMData(), domain, *quadrature_rule_per_topology_);

        decltype(auto) god_of_dof = model.GetGodOfDof(AsMeshId(MeshIndex::sole));

        decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        decltype(auto) unknown = unknown_manager.GetUnknown(AsUnknownId(UnknownIndex::displacement));

        time_manager_ = std::make_unique<TimeManager<::MoReFEM::TimeManagerNS::Policy::Static>>();

        LocalVector initial_value{ 0., 0., 0., 0., 0., 0. }; // 6 value as mesh is dimension 3

        cauchy_green_tensor_ = std::make_unique<cauchy_green_tensor_type>(
            "Cauchy-Green tensor", domain, *quadrature_rule_per_topology_, initial_value, *time_manager_);

        cauchy_green_tensor_operator_ = std::make_unique<update_cauchy_green_op_type>(
            felt_space, unknown, *cauchy_green_tensor_, quadrature_rule_per_topology_.get());
    }


    const Solid<time_manager_type>& Fixture::GetSolid() const noexcept
    {
        assert(!(!solid_));
        return *solid_;
    }


    const GeometricElt& Fixture::GetTetrahedron() const noexcept
    {
        assert(!(!tetra_));
        return *tetra_;
    }


    const QuadratureRulePerTopology& Fixture::GetQuadratureRulePerTopology() const noexcept
    {
        assert(!(!quadrature_rule_per_topology_));
        return *quadrature_rule_per_topology_;
    }


    const QuadraturePoint& Fixture::GetFirstQuadraturePoint(const GeometricElt& geom_elt) const noexcept
    {
        decltype(auto) quadrature_rule_per_topology = GetQuadratureRulePerTopology();
        decltype(auto) quadrature_rule =
            quadrature_rule_per_topology.GetRule(geom_elt.GetRefGeomElt().GetTopologyIdentifier());
        decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();
        assert(!quad_pt_list.empty());
        assert(!(!quad_pt_list[0]));
        return *quad_pt_list[0];
    }


    constexpr std::string_view OutputDirWrapper::Path()
    {
        return "Operators/HyperelasticLaws/CiarletGeymonat";
    }


} // namespace MoReFEM::TestNS::HyperelasticLawNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
