// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#define BOOST_TEST_MODULE invariant_holder

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/TimeManager/Policy/Evolution/None.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Advanced/InvariantHolder.hpp"
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Cartesian/Invariant1.hpp"
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Cartesian/Invariant2.hpp"
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Cartesian/Invariant3.hpp"
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Cartesian/Invariant4.hpp"
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Cartesian/Invariant6.hpp"

using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_AUTO_TEST_CASE(tuple_iteration)
{
    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = TimeManager<TimeManagerNS::Policy::None>;

    using I1 = InvariantNS::CartesianNS::Invariant1;
    using I2 = InvariantNS::CartesianNS::Invariant2;
    using I3 = InvariantNS::CartesianNS::Invariant3;
    using I4 = InvariantNS::CartesianNS::Invariant4<FiberNS::AtNodeOrAtQuadPt::at_node, time_manager_type>;
    using I6 = InvariantNS::CartesianNS::Invariant6<FiberNS::AtNodeOrAtQuadPt::at_node, time_manager_type>;

    using desorganized_tuple = std::tuple<I6, I3, I2, I4, I1>;

    using tuple_iteration = Internal::InvariantNS::TupleIteration<desorganized_tuple, 0ul>;

    static_assert(tuple_iteration::FindIndex<InvariantNS::index::I1>() == 4ul);
    static_assert(tuple_iteration::FindIndex<InvariantNS::index::I2>() == 2ul);
    static_assert(tuple_iteration::FindIndex<InvariantNS::index::I3>() == 1ul);
    static_assert(tuple_iteration::FindIndex<InvariantNS::index::I4>() == 3ul);
    static_assert(tuple_iteration::FindIndex<InvariantNS::index::I6>() == 0ul);
}


PRAGMA_DIAGNOSTIC(pop)
// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
