// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#define BOOST_TEST_MODULE log_I3_penalization

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "OperatorInstances/HyperelasticLaws/LogI3Penalization.hpp"

#include "Test/Operators/HyperelasticLaws/AnonymousNamespace.hpp"
#include "Test/Operators/HyperelasticLaws/Fixture.hpp"
#include "Test/Operators/HyperelasticLaws/ModelSettings.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS::HyperelasticLawNS;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(tetrahedron_at_node_cartesian, Fixture)
{
    using law_type = HyperelasticLawNS::LogI3Penalization<time_manager_type>;

    law_type law(3ul, GetSolid());


    const auto cauchy_green_tensor = SampleCauchyGreenTensor();

    decltype(auto) geom_elt = GetTetrahedron();
    decltype(auto) quad_pt = GetFirstQuadraturePoint(geom_elt);

    law.UpdateInvariants(cauchy_green_tensor, quad_pt, geom_elt);

    {
        const auto computed = law.W(quad_pt, geom_elt);
        constexpr auto expected_value = 0.396042;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }


    {
        const auto computed = law.FirstDerivativeWThirdInvariant(quad_pt, geom_elt);
        constexpr auto expected_value = 588.014341;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWThirdInvariant(quad_pt, geom_elt);
        constexpr auto expected_value = 436030.754589;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }
}


PRAGMA_DIAGNOSTIC(pop)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
