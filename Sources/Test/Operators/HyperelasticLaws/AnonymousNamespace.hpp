// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include "Utilities/LinearAlgebra/LocalAlias.hpp"

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{


    //! An arbitrary Cauchy-Green tensor that appeared in the hyperelastic model. Might not be realistic - that's not
    //! the point of current test anyway.
    MoReFEM::LocalVector SampleCauchyGreenTensor();

    constexpr auto epsilon = 1.e-6;


} // namespace


namespace // anonymous
{


    MoReFEM::LocalVector SampleCauchyGreenTensor()
    {
        return MoReFEM::LocalVector{ 1.001654, 0.999784, 0.999909, -0.000358, 0.000206, -0.000215 };
    }


} // namespace
