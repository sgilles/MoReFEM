// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#define BOOST_TEST_MODULE exponential_J1J4_deviatoric

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "OperatorInstances/HyperelasticLaws/ExponentialJ1J4Deviatoric.hpp"

#include "Test/Operators/HyperelasticLaws/AnonymousNamespace.hpp"
#include "Test/Operators/HyperelasticLaws/Fixture.hpp"
#include "Test/Operators/HyperelasticLaws/ModelSettings.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS::HyperelasticLawNS;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


namespace // anonymous
{

    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = TimeManager<TimeManagerNS::Policy::None>;


} // namespace


BOOST_FIXTURE_TEST_CASE(tetrahedron_at_node_cartesian, Fixture)
{
    using law_type = HyperelasticLawNS::ExponentialJ1J4Deviatoric<time_manager_type>;

    decltype(auto) fiber_list_manager = FiberNS::FiberListManager<FiberNS::AtNodeOrAtQuadPt::at_node,
                                                                  ParameterNS::Type::vector,
                                                                  time_manager_type>::GetInstance();

    decltype(auto) fiberI4 = fiber_list_manager.GetNonCstFiberList(AsFiberListId(FiberIndex::fiberI4));
    fiberI4.Initialize(&GetQuadratureRulePerTopology());

    law_type law(3ul, GetSolid(), fiberI4);


    const auto cauchy_green_tensor = SampleCauchyGreenTensor();

    decltype(auto) geom_elt = GetTetrahedron();
    decltype(auto) quad_pt = GetFirstQuadraturePoint(geom_elt);

    law.UpdateInvariants(cauchy_green_tensor, quad_pt, geom_elt);

    {
        const auto computed = law.W(quad_pt, geom_elt);
        constexpr auto expected_value = 14200.013948;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.FirstDerivativeWFirstInvariant(quad_pt, geom_elt);
        constexpr auto expected_value = 6996.863906;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.FirstDerivativeWSecondInvariant(quad_pt, geom_elt);
        constexpr auto expected_value = 3496.862164;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.FirstDerivativeWThirdInvariant(quad_pt, geom_elt);
        constexpr auto expected_value = -13980.919824;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWFirstInvariant(quad_pt, geom_elt);
        constexpr auto expected_value = 1868.323499;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWSecondInvariant(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWThirdInvariant(quad_pt, geom_elt);
        constexpr auto expected_value = 22972.251625;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWFirstAndSecondInvariant(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWFirstAndThirdInvariant(quad_pt, geom_elt);
        constexpr auto expected_value = -4195.801558;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWSecondAndThirdInvariant(quad_pt, geom_elt);
        constexpr auto expected_value = -2328.107124;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.FirstDerivativeWFourthInvariant(quad_pt, geom_elt);
        constexpr auto expected_value = -0.798916;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWFirstAndFourthInvariant(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWSecondAndFourthInvariant(quad_pt, geom_elt);
        constexpr auto expected_value = 0.;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }

    {
        const auto computed = law.SecondDerivativeWThirdAndFourthInvariant(quad_pt, geom_elt);
        constexpr auto expected_value = -492.582791;
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (computed)(expected_value)(epsilon));
    }
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
