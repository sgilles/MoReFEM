add_executable(TestOperatorP1_to_P1b)

target_sources(TestOperatorP1_to_P1b
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test_P1_to_P1b.cpp
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/AnonymousNamespace.hpp
        ${CMAKE_CURRENT_LIST_DIR}/demo_P1_to_P1b.lua
)
          
target_link_libraries(TestOperatorP1_to_P1b
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})

morefem_organize_IDE(TestOperatorP1_to_P1b Test/Operators/P1_to_HigherOrder Test/Operators/P1_to_HigherOrder)                  


morefem_boost_test_both_modes(NAME OperatorP1_to_P1b
                              EXE TestOperatorP1_to_P1b
                              TIMEOUT 10
                              LUA ${MOREFEM_ROOT}/Sources/Test/Operators/P1_to_HigherOrder/demo_P1_to_P1b.lua)


add_executable(TestOperatorP1_to_P2)

target_sources(TestOperatorP1_to_P2
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test_P1_to_P2.cpp
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
        ${CMAKE_CURRENT_LIST_DIR}/AnonymousNamespace.hpp
        ${CMAKE_CURRENT_LIST_DIR}/demo_P1_to_P2.lua
)

target_link_libraries(TestOperatorP1_to_P2
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_MODEL}>
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})

morefem_organize_IDE(TestOperatorP1_to_P2 Test/Operators/P1_to_HigherOrder Test/Operators/P1_to_HigherOrder)                  

morefem_boost_test_both_modes(NAME OperatorP1_to_P2
                              EXE TestOperatorP1_to_P2
                              TIMEOUT 10
                              LUA ${MOREFEM_ROOT}/Sources/Test/Operators/P1_to_HigherOrder/demo_P1_to_P2.lua)
