// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

/*!
 * \file
 *
 */


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_REACTIONLAW_FIXTURE_DOT_HXX_
#define MOREFEM_TEST_OPERATORS_REACTIONLAW_FIXTURE_DOT_HXX_
// IWYU pragma: private, include "Test/Operators/ReactionLaw/Fixture.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Test/Operators/ReactionLaw/Fixture.hpp"


namespace MoReFEM::TestNS::ReactionLawNS
{


    template<class ReactionLawSpecificDataT>
    Fixture<ReactionLawSpecificDataT>::Fixture()
    {
        decltype(auto) model = parent::GetModel();
        static_cast<void>(model); // we really need the call above, contrary to what the compiler may infer!

        // Set Mesh, and extract three geometric elements which will be used for tests.
        {
            decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance();
            decltype(auto) mesh = mesh_manager.GetMesh(AsMeshId(ReactionLawNS::MeshIndex::sole));

            decltype(auto) geom_elt_factory = Advanced::GeometricEltFactory::GetInstance();

            {
                decltype(auto) ref_tetra4 = geom_elt_factory.GetRefGeomElt(Advanced::GeometricEltEnum::Tetrahedron4);
                auto [begin, end] = mesh.GetSubsetGeometricEltList<RoleOnProcessor::processor_wise>(ref_tetra4);
                assert(end - begin == 1);
                tetra_ = *begin;
            }
        }

        quadrature_rule_per_topology_ = std::make_unique<QuadratureRulePerTopology>(3, 2);
    }


    template<class ReactionLawSpecificDataT>
    const GeometricElt& Fixture<ReactionLawSpecificDataT>::GetTetrahedron() const noexcept
    {
        assert(!(!tetra_));
        return *tetra_;
    }


    template<class ReactionLawSpecificDataT>
    const QuadratureRulePerTopology& Fixture<ReactionLawSpecificDataT>::GetQuadratureRulePerTopology() const noexcept
    {
        assert(!(!quadrature_rule_per_topology_));
        return *quadrature_rule_per_topology_;
    }


    template<class ReactionLawSpecificDataT>
    const QuadraturePoint&
    Fixture<ReactionLawSpecificDataT>::GetFirstQuadraturePoint(const GeometricElt& geom_elt) const noexcept
    {
        decltype(auto) quadrature_rule_per_topology = GetQuadratureRulePerTopology();
        decltype(auto) quadrature_rule =
            quadrature_rule_per_topology.GetRule(geom_elt.GetRefGeomElt().GetTopologyIdentifier());
        decltype(auto) quad_pt_list = quadrature_rule.GetQuadraturePointList();
        assert(!quad_pt_list.empty());
        assert(!(!quad_pt_list[0]));
        return *quad_pt_list[0];
    }


} // namespace MoReFEM::TestNS::ReactionLawNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_REACTIONLAW_FIXTURE_DOT_HXX_
// *** MoReFEM end header guards *** < //
