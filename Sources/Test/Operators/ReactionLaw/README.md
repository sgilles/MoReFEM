These tests have been added as the reaction laws were not tested at all; when v24.03 was published all the 
tests were working but I discovered while updating ReactionDiffusion external model that the code in template
classes was actually not up-to-date with the latest API changes.

The tests are mostly to check everything compiles; the numerical values have been lifted from the external model
with some tweaks to stress more (for instance initial gate set to 0 isn't a terribly usefuln value for a test).

As we are essenitally testing something called at local operator level, there are no parallel tests here.
