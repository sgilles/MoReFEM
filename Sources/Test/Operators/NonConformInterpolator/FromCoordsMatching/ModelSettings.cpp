// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/InputData.hpp"


namespace MoReFEM::TestNS::FromCoordsMatchingNS
{


    void ModelSettings::Init()
    {
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::fluid)>>(
            { "Finite element space for fluid mesh" });
        SetDescription<InputDataNS::FEltSpace<EnumUnderlyingType(FEltSpaceIndex::solid)>>(
            { "Finite element space for solid mesh" });

        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_fluid)>>(
            { " unknown_on_fluid" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::unknown_on_solid)>>(
            { " unknown_on_solid" });
        SetDescription<InputDataNS::NumberingSubset<EnumUnderlyingType(NumberingSubsetIndex::chaos)>>({ " chaos" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::unknown)>>({ " unknown" });
        SetDescription<InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::chaos)>>({ " chaos" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::fluid)>>({ " fluid" });
        SetDescription<InputDataNS::Mesh<EnumUnderlyingType(MeshIndex::solid)>>({ " solid" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::fluid)>>({ " fluid" });
        SetDescription<InputDataNS::Domain<EnumUnderlyingType(DomainIndex::solid)>>({ " solid" });
        SetDescription<InputDataNS::CoordsMatchingFile<1>>({ "sole" });

        SetDescription<
            InputDataNS::CoordsMatchingInterpolator<EnumUnderlyingType(CoordsMatchingInterpolator::unknown)>>(
            { "sole" });
    }


} // namespace MoReFEM::TestNS::FromCoordsMatchingNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
