// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/Model.hpp"


namespace MoReFEM::TestNS::FromCoordsMatchingNS
{


    Model::Model(morefem_data_type& morefem_data) : parent(morefem_data)
    { }


    void Model::SupplInitialize()
    {
        using type = NonConformInterpolatorNS::FromCoordsMatching;
        decltype(auto) morefem_data = parent::GetMoReFEMData();

        unknown_solid_2_fluid_ = std::make_unique<type>(morefem_data,
                                                        EnumUnderlyingType(CoordsMatchingInterpolator::unknown),
                                                        NonConformInterpolatorNS::store_matrix_pattern::yes);
    }


    void Model::SupplFinalize()
    { }


} // namespace MoReFEM::TestNS::FromCoordsMatchingNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
