// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>

#define BOOST_TEST_MODULE from_coords_matching_invalid_data
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/CoordsMatching/Exceptions/Exception.hpp"

#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/InputData.hpp"
#include "Test/Operators/NonConformInterpolator/FromCoordsMatching/Model.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


namespace // anonymous
{

    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
        TestNS::FromCoordsMatchingNS::Model
    >;
    // clang-format on


} // namespace


BOOST_FIXTURE_TEST_CASE(check_interpolation_matrix, fixture_type)
{
    BOOST_CHECK_THROW(GetModel(), MoReFEM::Internal::FromCoordsMatchingNS::Exception);
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
