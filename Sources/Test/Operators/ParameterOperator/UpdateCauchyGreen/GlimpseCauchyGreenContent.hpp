// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_OPERATORS_PARAMETEROPERATOR_UPDATECAUCHYGREEN_GLIMPSECAUCHYGREENCONTENT_DOT_HPP_
#define MOREFEM_TEST_OPERATORS_PARAMETEROPERATOR_UPDATECAUCHYGREEN_GLIMPSECAUCHYGREENCONTENT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Utilities/TimeKeep/TimeKeep.hpp"

#include "OperatorInstances/ParameterOperator/UpdateCauchyGreenTensor.hpp"

#include "Model/Model.hpp"

#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/InputData.hpp"
#include "Test/Operators/ParameterOperator/UpdateCauchyGreen/Model.hpp"


namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS
{


    /*!
     * \brief Return the value of the 3rd (arbitrarily) element of the only \a GeometricElt of the highest dimension.
     *
     * \param[in] model The \a Model used for the test.
     *
     * \return The local value of the \a Parameter at the specific (\a GeometricElt, \a QuadraturePoint).
     */
    auto GlimpseCauchyGreenContent(const Model& model) -> const Model::glimpse_cauchy_green_type&;


} // namespace MoReFEM::TestNS::UpdateCauchyGreenTensorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_OPERATORS_PARAMETEROPERATOR_UPDATECAUCHYGREEN_GLIMPSECAUCHYGREENCONTENT_DOT_HPP_
// *** MoReFEM end header guards *** < //
