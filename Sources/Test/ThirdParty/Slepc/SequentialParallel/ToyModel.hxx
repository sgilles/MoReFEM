// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_THIRDPARTY_SLEPC_SEQUENTIALPARALLEL_TOYMODEL_DOT_HXX_
#define MOREFEM_TEST_THIRDPARTY_SLEPC_SEQUENTIALPARALLEL_TOYMODEL_DOT_HXX_
// IWYU pragma: private, include "Test/ThirdParty/Slepc/SequentialParallel/ToyModel.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Test/ThirdParty/Slepc/SequentialParallel/ToyModel.hpp"


namespace MoReFEM::TestNS::SlepcNS
{


    inline const GlobalMatrix& ToyModel::GetMatrix() const noexcept
    {
        assert(!(!matrix_));
        return *matrix_;
    }


    inline const FilesystemNS::Directory& ToyModel::GetOutputDirectory() const noexcept
    {
        return output_directory_;
    }


    inline auto ToyModel::GetNonCstModelSettings() noexcept -> ModelSettings&
    {
        return model_settings_;
    }

} // namespace MoReFEM::TestNS::SlepcNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_THIRDPARTY_SLEPC_SEQUENTIALPARALLEL_TOYMODEL_DOT_HXX_
// *** MoReFEM end header guards *** < //
