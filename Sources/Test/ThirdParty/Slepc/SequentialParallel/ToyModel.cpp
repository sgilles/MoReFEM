// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <fstream>
#include <sstream>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "FiniteElement/FiniteElementSpace/Internal/GodOfDof/InitAllGodOfDof.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Mass.hpp"

#include "Geometry/Mesh/Internal/CreateMeshDataDirectory.hpp"
#include "Model/Internal/InitializeHelper.hpp"

#include "Test/ThirdParty/Slepc/SequentialParallel/ToyModel.hpp"
#include "Test/Tools/EmptyModelSettings.hpp"


namespace MoReFEM::TestNS::SlepcNS
{


    namespace // anonymous
    {


        FilesystemNS::Directory ExtractResultDirectory(const morefem_data_type& morefem_data)
        {
            const std::string path =
                ::MoReFEM::InputDataNS::ExtractLeafAsPath<InputDataNS::Result::OutputDirectory>(morefem_data);

            FilesystemNS::Directory ret(morefem_data.GetMpi(), path, FilesystemNS::behaviour::overwrite);

            ret.ActOnFilesystem();

            return ret;
        }


    } // namespace


    ToyModel::ToyModel(morefem_data_type& morefem_data)
    : Crtp::CrtpMpi<ToyModel>(morefem_data.GetMpi()), output_directory_(ExtractResultDirectory(morefem_data))
    {
        decltype(auto) model_settings = GetNonCstModelSettings();

        model_settings.Init();

        Internal::ModelNS::InitMostSingletonManager(morefem_data);

        Internal::GodOfDofNS::InitAllGodOfDof(morefem_data,
                                              model_settings,
                                              DoConsiderProcessorWiseLocal2Global::no,
                                              Internal::MeshNS::CreateMeshDataDirectory(GetOutputDirectory()));

        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();
        decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::sole));

        decltype(auto) numbering_subset =
            god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::sole));

        matrix_ = std::make_unique<GlobalMatrix>(numbering_subset, numbering_subset);
        auto& matrix = *matrix_;

        AllocateGlobalMatrix(god_of_dof, matrix);

        decltype(auto) unknown_manager = UnknownManager::GetInstance();

        decltype(auto) unknown = unknown_manager.GetUnknownPtr(AsUnknownId(UnknownIndex::sole));

        decltype(auto) felt_space = god_of_dof.GetFEltSpace(AsFEltSpaceId(FEltSpaceIndex::sole));

        GlobalVariationalOperatorNS::Mass mass_op(felt_space, unknown, unknown);

        GlobalMatrixWithCoefficient matrix_with_coeff(matrix, 1.);

        mass_op.Assemble(std::make_tuple(std::ref(matrix_with_coeff)));
    }


} // namespace MoReFEM::TestNS::SlepcNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
