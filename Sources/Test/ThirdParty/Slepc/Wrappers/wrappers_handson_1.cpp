// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>

#define BOOST_TEST_MODULE slepc_wrappers_handson_1

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSysTypes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Slepc/SlepcEps.hpp"

#include "ThirdParty/Wrappers/Petsc/Internal/RAII.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hpp"
#include "ThirdParty/Wrappers/Petsc/Print.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"
#include "ThirdParty/Wrappers/Slepc/Internal/RAII.hpp"
#include "ThirdParty/Wrappers/Slepc/Solver/Eps.hpp"

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    constexpr auto epsilon = 1.e-12;

    Wrappers::Petsc::Matrix PrepareMatrix(const Wrappers::Mpi& mpi, std::size_t size);


} // namespace


BOOST_AUTO_TEST_CASE(Standard_Symmetric_Eigenvalue_Problem)
{
    decltype(auto) master_test_suite = boost::unit_test::framework::master_test_suite();
    auto argc = master_test_suite.argc;
    auto argv = master_test_suite.argv;

    std::size_t size{ 30ul };

    decltype(auto) petsc = Internal::PetscNS::RAII::CreateOrGetInstance(std::source_location::current(), argc, argv);
    Internal::SlepcNS::RAII::CreateOrGetInstance();

    decltype(auto) mpi = petsc.GetMpi();
    BOOST_REQUIRE_EQUAL(mpi.Nprocessor<int>(), 1);

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Compute the operator matrix that defines the eigensystem, Ax=kx
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    Wrappers::Petsc::Matrix problem_matrix = PrepareMatrix(mpi, size);

    Wrappers::Slepc::Eps eps(mpi, problem_matrix, Wrappers::Slepc::problem_type::non_hermitian);

    // By default in MoReFEM smallest magnitude is chosen, but here we match an existing Slepc example
    // which uses up as default largest magnitude.
    eps.SetEigenSpectrum(Wrappers::Slepc::which_type::largest_magnitude);

    eps.Solve();

    // Values obtained when the test was first set up.
    BOOST_CHECK_EQUAL(eps.GetIterationNumber(), 4ul);
    BOOST_CHECK_EQUAL(eps.GetType(), "krylovschur");
    BOOST_CHECK_EQUAL(eps.NeigenValues(), 1ul);
    BOOST_CHECK_EQUAL(eps.GetConvergenceTolerance(), 1e-8);
    BOOST_CHECK_EQUAL(eps.NmaxIterations(), 100ul);

    BOOST_REQUIRE_EQUAL(eps.NconvergedEigenPairs(), 1ul);

    auto [real, imaginary] = eps.GetEigenPair(0ul);
    const auto error = eps.ComputeRelativeError(0ul);

    BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (real.GetEigenValue())(3.989739)(1.e-6));
    BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (error)(4.72989e-09)(epsilon));

    BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (imaginary.GetEigenValue())(0.)(epsilon));
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


namespace
{


    Wrappers::Petsc::Matrix PrepareMatrix(const Wrappers::Mpi& mpi, std::size_t size)
    {
        Wrappers::Petsc::Matrix problem_matrix("problem matrix");

        problem_matrix.InitSequentialDenseMatrix(size, size, mpi);

        const auto size_as_petsc_int = static_cast<PetscInt>(size);

        for (PetscInt index = 0; index < size_as_petsc_int; ++index)
        {
            if (index > 0)
                problem_matrix.SetValue(index, index - 1, -1., INSERT_VALUES);

            if (index < size_as_petsc_int - 1)
                problem_matrix.SetValue(index, index + 1, -1.0, INSERT_VALUES);

            problem_matrix.SetValue(index, index, 2.0, INSERT_VALUES);
        }

        problem_matrix.Assembly();

        return problem_matrix;
    }

} // namespace
