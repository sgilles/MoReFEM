// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>

#define BOOST_TEST_MODULE slepc_wrappers_handson_3

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSysTypes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Slepc/SlepcEps.hpp"

#include "ThirdParty/Wrappers/Petsc/Internal/RAII.hpp"
#include "ThirdParty/Wrappers/Petsc/Matrix/Matrix.hpp"
#include "ThirdParty/Wrappers/Petsc/Print.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"
#include "ThirdParty/Wrappers/Slepc/Internal/RAII.hpp"
#include "ThirdParty/Wrappers/Slepc/Solver/Eps.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


using namespace MoReFEM;

namespace // anonymous
{


    constexpr auto epsilon = 1.e-9;

    // Load the matrix from binary \a matrix_file.
    Wrappers::Petsc::Matrix Load(const Wrappers::Mpi& mpi, std::string_view matrix_file);


} // namespace


BOOST_FIXTURE_TEST_CASE(Generalized_Eigenvalue_Problem_Stored_in_a_File, MoReFEM::TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) master_test_suite = boost::unit_test::framework::master_test_suite();
    auto argc = master_test_suite.argc;
    auto argv = master_test_suite.argv;

    decltype(auto) petsc = Internal::PetscNS::RAII::CreateOrGetInstance(std::source_location::current(), argc, argv);
    Internal::SlepcNS::RAII::CreateOrGetInstance();

    decltype(auto) mpi = petsc.GetMpi();

    static char help[] = "Solves a generalized eigensystem Ax=kBx with matrices loaded from a file.\n";
    static_cast<void>(help);

    Wrappers::Petsc::PrintMessageOnFirstProcessor("\nGeneralized eigenproblem stored in file.\n\n", mpi);

    auto A = Load(mpi, "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Slepc/Data/bfw62a.petsc");
    auto B = Load(mpi, "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Slepc/Data/bfw62b.petsc");

    Wrappers::Slepc::Eps eps(mpi, A, B, Wrappers::Slepc::problem_type::generalized_non_hermitian);

    // By default in MoReFEM smallest magnitude is chosen, but here we match an existing Slepc example
    // which uses up as default largest magnitude.
    eps.SetEigenSpectrum(Wrappers::Slepc::which_type::largest_magnitude);

    eps.Solve();

    // Following figures were obtained when the test was set up
    BOOST_CHECK_EQUAL(eps.GetIterationNumber(), 3ul);
    BOOST_CHECK_EQUAL(eps.GetLinearIterationNumber(), 32ul);

    BOOST_CHECK_EQUAL(eps.NconvergedEigenPairs(), 2ul);

    BOOST_CHECK_EQUAL(eps.GetConvergenceTolerance(), 1e-8);
    BOOST_CHECK_EQUAL(eps.NmaxIterations(), 100ul);

    // Additions for MoReFEM test
    // IMPORTANT: the epsilon is much softer than the one used in the hands on test, as the obtained value obtained
    // when using a MATMPIAIJ matrix is not exactly the same, even with only one processor. Of course it remains very
    // close - I have lowered the constraints upon epsilon but the reference values here are exactly the same as
    // in the hands on test.
    {
        auto [real, imaginary] = eps.GetEigenPair(0ul);
        const auto error = eps.ComputeRelativeError(0ul);

        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (real.GetEigenValue())(-243874.97870450976)(1.e-6));
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>,
                              (imaginary.GetEigenValue())(6999.6692722132493)(1.e-6));
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (error)(8.73583e-16)(epsilon));
    }

    {
        auto [real, imaginary] = eps.GetEigenPair(1ul);
        const auto error = eps.ComputeRelativeError(1ul);

        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (real.GetEigenValue())(-243874.97870450976)(1.e-6));
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>,
                              (imaginary.GetEigenValue())(-6999.6692722132493)(1.e-6));
        BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (error)(8.73583e-16)(epsilon));
    }
    // End of additions for MoReFEM test
}

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{

    Wrappers::Petsc::Matrix Load(const Wrappers::Mpi& mpi, std::string_view str_matrix_file)
    {
        MoReFEM::FilesystemNS::File matrix_file{ std::filesystem::path(str_matrix_file) };

        Wrappers::Petsc::Matrix ret("problem_matrix");
        ret.InitMinimalCase(mpi, MATSEQAIJ);
        ret.LoadBinary(mpi, matrix_file);

        return ret;
    }


} // namespace


PRAGMA_DIAGNOSTIC(pop)
