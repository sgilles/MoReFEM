# test1.cpp

`test1.cpp` is just the same as handson1.cpp from `HandsOn` directory save that is uses up the Slepc wrappers defined in MoReFEM library.

The code has been simplified and is intended to run only in sequential (however not much would prevent it from working in parallel, save the way the matrix is defined).