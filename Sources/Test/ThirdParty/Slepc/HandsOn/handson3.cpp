// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cstdlib>

#define BOOST_TEST_MODULE slepc_handson_3

#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSysTypes.hpp"
#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscVersion.hpp"
#include "ThirdParty/IncludeWithoutWarning/Slepc/SlepcEps.hpp"
#include "ThirdParty/Wrappers/Petsc/Viewer.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


namespace // anonymous
{


    constexpr auto epsilon = 1.e-9;


} // namespace


BOOST_FIXTURE_TEST_CASE(Generalized_Eigenvalue_Problem_Stored_in_a_File, MoReFEM::TestNS::FixtureNS::TestEnvironment)
{
    Mat A, B; /* matrices */
    EPS eps;  /* eigenproblem solver context */
    ST st;
    KSP ksp;
    EPSType type;
    PetscReal tol;
    Vec xr, xi;
    PetscInt nev, maxit, its, lits;
    PetscViewer viewer;

    int ierr;

    static char help[] = "Solves a generalized eigensystem Ax=kBx with matrices loaded from a file.\n";
    static_cast<void>(help);
    PetscFunctionBeginUser;
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Load the matrices that define the eigensystem, Ax=kBx
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    ierr = PetscPrintf(PETSC_COMM_WORLD, "\nGeneralized eigenproblem stored in file.\n\n");
    BOOST_REQUIRE_EQUAL(ierr, 0);
    MoReFEM::FilesystemNS::File matrix_file_1{ std::filesystem::path(
        "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Slepc/Data/bfw62a.petsc") };

#if defined(PETSC_USE_COMPLEX)
    ierr = PetscPrintf(PETSC_COMM_WORLD, " Reading COMPLEX matrices from binary files...\n");
    BOOST_REQUIRE_EQUAL(ierr, 0);
#else
    ierr = PetscPrintf(PETSC_COMM_WORLD, " Reading REAL matrices from binary files...\n");
    BOOST_REQUIRE_EQUAL(ierr, 0);
#endif
    ierr = PetscViewerBinaryOpen(
        PETSC_COMM_WORLD, static_cast<std::string>(matrix_file_1).c_str(), FILE_MODE_READ, &viewer);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatCreate(PETSC_COMM_WORLD, &A);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatSetFromOptions(A);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatLoad(A, viewer);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = PetscViewerDestroy(&viewer);
    BOOST_CHECK_EQUAL(ierr, 0);

    MoReFEM::FilesystemNS::File matrix_file_2{ std::filesystem::path(
        "${MOREFEM_ROOT}/Sources/Test/ThirdParty/Slepc/Data/bfw62b.petsc") };

    ierr = PetscViewerBinaryOpen(
        PETSC_COMM_WORLD, static_cast<std::string>(matrix_file_2).c_str(), FILE_MODE_READ, &viewer);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatCreate(PETSC_COMM_WORLD, &B);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatSetFromOptions(B);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatLoad(B, viewer);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = PetscViewerDestroy(&viewer);
    BOOST_REQUIRE_EQUAL(ierr, 0);

    ierr = MatCreateVecs(A, MOREFEM_PETSC_NULL, &xr);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatCreateVecs(A, MOREFEM_PETSC_NULL, &xi);
    BOOST_CHECK_EQUAL(ierr, 0);


    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Create the eigensolver and set various options
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    /*
     Create eigensolver context
     */
    ierr = EPSCreate(PETSC_COMM_WORLD, &eps);
    BOOST_REQUIRE_EQUAL(ierr, 0);

    /*
     Set operators. In this case, it is a generalized eigenvalue problem
     */
    ierr = EPSSetOperators(eps, A, B);
    BOOST_CHECK_EQUAL(ierr, 0);

    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Solve the eigensystem
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    ierr = EPSSolve(eps);
    BOOST_CHECK_EQUAL(ierr, 0);
    /*
     Optional: Get some information from the solver and display it
     */
    ierr = EPSGetIterationNumber(eps, &its);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = PetscPrintf(PETSC_COMM_WORLD, " Number of iterations of the method: %" PetscInt_FMT "\n", its);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = EPSGetST(eps, &st);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = STGetKSP(st, &ksp);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = KSPGetTotalIterations(ksp, &lits);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    BOOST_CHECK_EQUAL(its, 3);   // Figures obtained when the test was set up
    BOOST_CHECK_EQUAL(lits, 32); // Figures obtained when the test was set up
    ierr = PetscPrintf(PETSC_COMM_WORLD, " Number of linear iterations of the method: %" PetscInt_FMT "\n", lits);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = EPSGetType(eps, &type);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = PetscPrintf(PETSC_COMM_WORLD, " Solution method: %s\n\n", type);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    BOOST_CHECK_EQUAL(type, "krylovschur");
    ierr = EPSGetDimensions(eps, &nev, MOREFEM_PETSC_NULL, MOREFEM_PETSC_NULL);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = PetscPrintf(PETSC_COMM_WORLD, " Number of requested eigenvalues: %" PetscInt_FMT "\n", nev);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    BOOST_CHECK_EQUAL(nev, 1);
    ierr = EPSGetTolerances(eps, &tol, &maxit);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    BOOST_CHECK_EQUAL(tol, 1e-08);
    BOOST_CHECK_EQUAL(maxit, 100);

    ierr = PetscPrintf(
        PETSC_COMM_WORLD, " Stopping condition: tol=%.4g, maxit=%" PetscInt_FMT "\n", static_cast<double>(tol), maxit);

    BOOST_CHECK_EQUAL(ierr, 0);
    /* - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
     Display solution and clean up
     - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - */

    /*
     Show detailed info
     */
    ierr = PetscViewerPushFormat(PETSC_VIEWER_STDOUT_WORLD, PETSC_VIEWER_ASCII_INFO_DETAIL);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = EPSConvergedReasonView(eps, PETSC_VIEWER_STDOUT_WORLD);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = EPSErrorView(eps, EPS_ERROR_RELATIVE, PETSC_VIEWER_STDOUT_WORLD);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = PetscViewerPopFormat(PETSC_VIEWER_STDOUT_WORLD);

    BOOST_CHECK_EQUAL(ierr, 0);

    // Additions for MoReFEM test
    PetscScalar re, im, error;
    ierr = EPSGetEigenvalue(eps, 0, &re, &im);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (re)(-243874.97870450976)(epsilon));
    BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (im)(6999.6692722132493)(epsilon));

    ierr = EPSComputeError(eps, 0, EPS_ERROR_RELATIVE, &error);
    BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (error)(8.73583e-16)(epsilon));

    ierr = EPSGetEigenvalue(eps, 1, &re, &im);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (re)(-243874.97870450976)(epsilon));
    BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (im)(-6999.6692722132493)(epsilon));

    ierr = EPSComputeError(eps, 1, EPS_ERROR_RELATIVE, &error);
    BOOST_CHECK_PREDICATE(MoReFEM::NumericNS::AreEqual<double>, (error)(8.73583e-16)(epsilon));
    // End of additions for MoReFEM test

    /*
     Free work space
     */
    ierr = EPSDestroy(&eps);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatDestroy(&A);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = MatDestroy(&B);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = VecDestroy(&xr);
    BOOST_REQUIRE_EQUAL(ierr, 0);
    ierr = VecDestroy(&xi);
    BOOST_CHECK_EQUAL(ierr, 0);
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
