The point of these tests is to check Slepc is properly linked against MoReFEM.

The codes used here are [lifted from Slepc website](https://slepc.upv.es/handson)

It is on purpose the code has been modified as little as possible from the original one found on Slepc site: we want to make sure the library works correctly in the first place.

It wasn't the case in the first try; we had to modify how Petsc and Slepc were installed to make it work (see #1751 for more about it).