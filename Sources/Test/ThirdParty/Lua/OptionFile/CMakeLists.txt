add_executable(TestOptionFile)

target_sources(TestOptionFile
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp     
        ${CMAKE_CURRENT_LIST_DIR}/demo_lua_option_file.lua
        ${CMAKE_CURRENT_LIST_DIR}/advanced_map.lua
        ${CMAKE_CURRENT_LIST_DIR}/entry_with_underscore.lua
        ${CMAKE_CURRENT_LIST_DIR}/map_in_vector.lua
        ${CMAKE_CURRENT_LIST_DIR}/no_section.lua
        ${CMAKE_CURRENT_LIST_DIR}/redundancy.lua
        ${CMAKE_CURRENT_LIST_DIR}/redundancy_in_section.lua
        ${CMAKE_CURRENT_LIST_DIR}/keys_with_numbers_inside.lua
)
                
target_link_libraries(TestOptionFile
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>)

morefem_boost_test_sequential_mode(NAME OptionFile
                                   EXE TestOptionFile
                                   TIMEOUT 5)

morefem_organize_IDE(TestOptionFile Test/ThirdParty/Lua Test/ThirdParty/Lua/OptionFile)
