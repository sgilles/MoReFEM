// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>

#define BOOST_TEST_MODULE petsc_vector_is_ghosted

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(sequential_vector, MoReFEM::TestNS::FixtureNS::TestEnvironment)
{
    if (GetMpi().Nprocessor<int>() == 1)
    {
        Wrappers::Petsc::Vector vector("Sequential vector");
        const auto local_size = 2ul;
        vector.InitSequentialVector(GetMpi(), local_size);

        BOOST_CHECK_EQUAL(vector.IsParallel(std::source_location::current()), false);

        BOOST_CHECK(vector.IsGhosted(std::source_location::current()) == false);

        vector.UpdateGhosts(std::source_location::current()); // check no exception are thrown
    }
}


BOOST_FIXTURE_TEST_CASE(parallel_without_ghost, MoReFEM::TestNS::FixtureNS::TestEnvironment)
{
    if (GetMpi().Nprocessor<int>() > 1)
    {
        Wrappers::Petsc::Vector vector("Parallel vector without ghost");
        const auto local_size = 2ul;
        const auto global_size = local_size * GetMpi().Nprocessor<std::size_t>(); // we take simple case here!

        vector.InitMpiVector(GetMpi(), local_size, global_size);
        BOOST_CHECK_EQUAL(vector.IsParallel(std::source_location::current()), true);

        BOOST_CHECK(vector.IsGhosted(std::source_location::current()) == false);
        vector.UpdateGhosts(std::source_location::current()); // check no exception are thrown
    }
}


BOOST_FIXTURE_TEST_CASE(parallel_with_ghost, MoReFEM::TestNS::FixtureNS::TestEnvironment)
{
    if (GetMpi().Nprocessor<int>() > 1)
    {
        Wrappers::Petsc::Vector vector("Parallel vector with ghost");
        const auto local_size = 2ul;
        const auto global_size = local_size * GetMpi().Nprocessor<std::size_t>(); // we take simple case here!

        vector.InitMpiVectorWithGhost(GetMpi(), local_size, global_size, { 0, 1, 1, 0 });
        BOOST_CHECK_EQUAL(vector.IsParallel(std::source_location::current()), true);

        BOOST_CHECK(vector.IsGhosted(std::source_location::current()) == true);
        vector.UpdateGhosts(std::source_location::current()); // check no exception are thrown
    }
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
