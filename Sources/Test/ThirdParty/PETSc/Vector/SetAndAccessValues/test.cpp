// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>

#define BOOST_TEST_MODULE petsc_vector_set_and_access_values

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    constexpr auto epsilon = NumericNS::DefaultEpsilon<PetscScalar>();


    struct Fixture : public MoReFEM::TestNS::FixtureNS::TestEnvironment
    {


        Fixture();

        Wrappers::Petsc::Vector vector;

        const std::size_t local_size = 2ul;
        const std::size_t global_size = local_size * GetMpi().Nprocessor<std::size_t>();

        static PetscScalar ComputeValueForIndex(PetscInt index);
    };


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(size_accessors, Fixture)
{
    BOOST_CHECK_EQUAL(static_cast<std::size_t>(vector.GetProcessorWiseSize()), local_size);
    BOOST_CHECK_EQUAL(static_cast<std::size_t>(vector.GetProgramWiseSize()), global_size);
}


BOOST_FIXTURE_TEST_CASE(get_value, Fixture)
{
    // Check only the processor-wise values for each rank - or PETSc will yell!
    PetscInt begin_index = GetMpi().GetRank<PetscInt>() * static_cast<PetscInt>(local_size);
    PetscInt end_index = begin_index + static_cast<PetscInt>(local_size);

    for (auto index = begin_index; index < end_index; ++index)
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>,
                              (vector.GetValue(index))(ComputeValueForIndex(index))(epsilon));
}


BOOST_FIXTURE_TEST_CASE(get_value_outside_rank, Fixture)
{
    // This test should only fail in parallel; no need to check all combinations.
    if (GetMpi().GetRank<int>() > 0)
    {
        BOOST_CHECK_THROW(vector.GetValue(0), Wrappers::Petsc::ExceptionNS::Exception);
    }
}


BOOST_FIXTURE_TEST_CASE(get_values, Fixture)
{
    PetscInt begin_index = GetMpi().GetRank<PetscInt>() * static_cast<PetscInt>(local_size);

    std::vector<PetscInt> index_list(local_size);

    std::iota(index_list.begin(), index_list.end(), begin_index);

    const auto local_values = vector.GetValues(index_list);

    BOOST_CHECK_EQUAL(local_values.size(), local_size);

    for (PetscInt index = 0; index < static_cast<PetscInt>(local_size); ++index)
        BOOST_CHECK_PREDICATE(
            NumericNS::AreEqual<double>,
            (local_values[static_cast<std::size_t>(index)])(ComputeValueForIndex(begin_index + index))(epsilon));
}


BOOST_FIXTURE_TEST_CASE(get_values_output_argument, Fixture)
{
    PetscInt begin_index = GetMpi().GetRank<PetscInt>() * static_cast<PetscInt>(local_size);

    std::vector<PetscInt> index_list(local_size);

    std::iota(index_list.begin(), index_list.end(), begin_index);

    std::vector<PetscScalar> value_list(local_size);

    vector.GetValues(index_list, value_list);

    BOOST_CHECK_EQUAL(value_list.size(), local_size);

    for (PetscInt index = 0; index < static_cast<PetscInt>(local_size); ++index)
        BOOST_CHECK_PREDICATE(
            NumericNS::AreEqual<double>,
            (value_list[static_cast<std::size_t>(index)])(ComputeValueForIndex(begin_index + index))(epsilon));
}

#ifndef NDEBUG
BOOST_FIXTURE_TEST_CASE(get_values_output_argument_not_properly_allocated, Fixture)
{
    PetscInt begin_index = GetMpi().GetRank<PetscInt>() * static_cast<PetscInt>(local_size);

    std::vector<PetscInt> index_list(local_size);

    std::iota(index_list.begin(), index_list.end(), begin_index);

    std::vector<PetscScalar> value_list; // no allocation here.

    // Don't be astonished: PETSC ERROR will nonetheless appear on screen...
    BOOST_CHECK_THROW(vector.GetValues(index_list, value_list), MoReFEM::Advanced::Assertion);
}
#endif // NDEBUG


BOOST_FIXTURE_TEST_CASE(access_vector_content, Fixture)
{
    PetscInt begin_index = GetMpi().GetRank<PetscInt>() * static_cast<PetscInt>(local_size);

    Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> content(vector);

    BOOST_CHECK_EQUAL(content.GetSize(), local_size);

    for (auto index = 0ul; index < local_size; ++index)
        BOOST_CHECK_PREDICATE(
            NumericNS::AreEqual<double>,
            (content.GetValue(index))(ComputeValueForIndex(begin_index + static_cast<PetscInt>(index)))(epsilon));
}


BOOST_FIXTURE_TEST_CASE(access_vector_content_and_modify, Fixture)
{
    PetscInt begin_index = GetMpi().GetRank<PetscInt>() * static_cast<PetscInt>(local_size);

    Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_and_write> content(vector);
    {
        BOOST_CHECK_EQUAL(content.GetSize(), local_size);

        for (auto i = 0ul; i < local_size; ++i)
            content[i] *= 2.;
    }

    for (auto index = 0ul; index < local_size; ++index)
        BOOST_CHECK_PREDICATE(
            NumericNS::AreEqual<double>,
            (content.GetValue(index))(2. * ComputeValueForIndex(begin_index + static_cast<PetscInt>(index)))(epsilon));
}

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //


namespace // anonymous
{

    Fixture::Fixture() : vector("Vector in fixture")
    {
        decltype(auto) mpi = GetMpi();

        if (mpi.Nprocessor<int>() == 1)
            vector.InitSequentialVector(mpi, local_size);
        else
            vector.InitMpiVector(mpi, local_size, global_size);

        if (mpi.IsRootProcessor())
        {
            std::vector<PetscInt> index_list(global_size);
            std::vector<PetscScalar> value_list(global_size);

            std::iota(index_list.begin(), index_list.end(), 0);

            std::transform(index_list.cbegin(), index_list.cend(), value_list.begin(), ComputeValueForIndex);

            Utilities::PrintContainer<>::Do(value_list);

            vector.SetValues(index_list, value_list.data(), ADD_VALUES);
        }

        vector.Assembly(std::source_location::current(), update_ghost::no);
    }


    PetscScalar Fixture::ComputeValueForIndex(PetscInt index)
    {
        return 2. * static_cast<PetscScalar>(index) - 3.;
    };


} // namespace


PRAGMA_DIAGNOSTIC(pop)
