// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <vector>

#define BOOST_TEST_MODULE petsc_vector_operations

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Exceptions/PrintAndAbort.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"
#include "ThirdParty/Wrappers/Petsc/Vector/Vector.hpp"

#include "Core/LinearAlgebra/Operations.hpp"

#include "Test/Tools/ClearSingletons.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"
#include "Test/Tools/NumberingSubsetCreator.hpp"

using namespace MoReFEM;


namespace // anonymous
{


    struct Fixture : public MoReFEM::TestNS::FixtureNS::TestEnvironment
    {


        Fixture();

        GlobalVector::unique_ptr v1_ns1;
        GlobalVector::unique_ptr v2_ns1;
        GlobalVector::unique_ptr v1_ns2;
    };

    constexpr double epsilon = 1.e-8;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(axpy, Fixture)
{
    GlobalVector v2_initial{ *v2_ns1 };

    GlobalLinearAlgebraNS::AXPY(2., *v1_ns1, *v2_ns1, std::source_location::current());

    {
        Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> v2_initial_content(v2_initial);
        Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> v1_content(*v1_ns1);
        Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> v2_content(*v2_ns1);

        const auto local_size = v2_content.GetSize();

        BOOST_REQUIRE_EQUAL(local_size, v1_content.GetSize());
        BOOST_REQUIRE_EQUAL(local_size, v2_initial_content.GetSize());

        for (std::size_t index = 0ul; index < local_size; ++index)
        {
            BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>,
                                  (v2_content.GetValue(index))(2. * v1_content.GetValue(index)
                                                               + v2_initial_content.GetValue(index))(epsilon));
        }
    }

#ifndef NDEBUG

    BOOST_CHECK_THROW(GlobalLinearAlgebraNS::AXPY(1., *v1_ns1, *v1_ns2, std::source_location::current()),
                      GlobalLinearAlgebraNS::AssertionNS::MismatchNumberingSubset);

#endif // NDEBUG
}


BOOST_FIXTURE_TEST_CASE(dot_product, Fixture)
{
    BOOST_CHECK_PREDICATE(
        NumericNS::AreEqual<double>,
        (GlobalLinearAlgebraNS::DotProduct(*v1_ns1, *v2_ns1, std::source_location::current()))(18.)(epsilon));

#ifndef NDEBUG
    BOOST_CHECK_THROW(GlobalLinearAlgebraNS::DotProduct(*v1_ns1, *v1_ns2, std::source_location::current()),
                      GlobalLinearAlgebraNS::AssertionNS::MismatchNumberingSubset);
#endif // NDEBUG
}


namespace // anonymous
{

    Fixture::Fixture()
    {
        decltype(auto) mpi = GetMpi();

        static bool first_call = true;

        if (first_call)
        {
            TestNS::NumberingSubsetCreator creator;
            creator.Create(::MoReFEM::NumberingSubsetNS::unique_id{ 1ul });
            creator.Create(::MoReFEM::NumberingSubsetNS::unique_id{ 2ul });
            first_call = false;
        }

        decltype(auto) ns_manager = Internal::NumberingSubsetNS::NumberingSubsetManager::GetInstance();

        decltype(auto) ns1 = ns_manager.GetNumberingSubset(::MoReFEM::NumberingSubsetNS::unique_id{ 1ul });
        decltype(auto) ns2 = ns_manager.GetNumberingSubset(::MoReFEM::NumberingSubsetNS::unique_id{ 2ul });


        v1_ns1 = std::make_unique<GlobalVector>(ns1, "v1_ns1");
        v2_ns1 = std::make_unique<GlobalVector>(ns1, "v2_ns1");
        v1_ns2 = std::make_unique<GlobalVector>(ns2, "v1_ns2");


        if (mpi.Nprocessor<int>() == 1)
        {
            const auto ns1_local_size = 10ul;
            const auto ns2_local_size = 6ul;

            v1_ns1->InitSequentialVector(mpi, ns1_local_size);
            v2_ns1->InitSequentialVector(mpi, ns1_local_size);
            v1_ns2->InitSequentialVector(mpi, ns2_local_size);
        } else
        {
            // Arbitrary repartition between the four ranks (but same vector as in sequential case!)
            const auto ns1_local_size = 1ul + mpi.GetRank<std::size_t>();
            const auto ns1_global_size = 10ul; // 4 ranks and local size above

            const auto ns2_local_size = mpi.GetRank<std::size_t>();
            const auto ns2_global_size = 6ul; // 4 ranks and local size above

            v1_ns1->InitMpiVector(mpi, ns1_local_size, ns1_global_size);
            v2_ns1->InitMpiVector(mpi, ns1_local_size, ns1_global_size);
            v1_ns2->InitMpiVector(mpi, ns2_local_size, ns2_global_size);
        }


        std::vector<PetscInt> index_list_v1{ 0, 2, 4, 6 };
        std::vector<PetscScalar> values_v1{ 2., 3., 5., 7. };

        std::vector<PetscInt> index_list_v2{ 0, 1, 2, 8, 9 };
        std::vector<PetscScalar> values_v2{ 3., 1., 4., 5. };

        v1_ns1->SetValues(index_list_v1, values_v1.data(), INSERT_VALUES);
        v2_ns1->SetValues(index_list_v2, values_v2.data(), INSERT_VALUES);

        v1_ns1->Assembly();
        v2_ns1->Assembly();
    }


} // namespace


PRAGMA_DIAGNOSTIC(pop)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
