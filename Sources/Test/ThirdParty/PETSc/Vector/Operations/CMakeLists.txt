add_executable(TestPetscVectorOperations)


target_sources(TestPetscVectorOperations
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
)
          
target_link_libraries(TestPetscVectorOperations
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>
                      ${MOREFEM_EXTENSIVE_TEST_TOOLS})

morefem_organize_IDE(TestPetscVectorOperations Test/ThirdParty/PETSc/Vector Test/ThirdParty/PETSc/Vector/Operations)

morefem_boost_test_both_modes(NAME PetscVectorOperations
                              EXE TestPetscVectorOperations
                              TIMEOUT 10)
