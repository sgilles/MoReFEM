// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_THIRDPARTY_PETSC_MATRIXOPERATIONS_MODEL_DOT_HXX_
#define MOREFEM_TEST_THIRDPARTY_PETSC_MATRIXOPERATIONS_MODEL_DOT_HXX_
// IWYU pragma: private, include "Test/ThirdParty/PETSc/MatrixOperations/Model.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Test/ThirdParty/PETSc/MatrixOperations/Model.hpp"


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::TestNS::PetscNS::MatrixOperationsNS
{


    inline const std::string& Model::ClassName()
    {
        static const std::string ret("Test Petsc Matrix Operations");
        return ret;
    }


    template<std::size_t I>
    inline const GlobalMatrix& Model::GetInitializedMatrix() const noexcept
    {
        static_assert(I < Utilities::ArraySize<decltype(initialized_matrix_list_)>::GetValue());
        assert(!(!initialized_matrix_list_[I]));
        return *initialized_matrix_list_[I];
    }


    template<std::size_t I>
    inline GlobalMatrix& Model::GetNonCstInitializedMatrix() noexcept
    {
        return const_cast<GlobalMatrix&>(GetInitializedMatrix<I>());
    }


    inline const NumberingSubset& Model::GetScalarNumberingSubset() const noexcept
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();
        decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::sole));
        return god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::scalar));
    }


    inline const NumberingSubset& Model::GetVectorialNumberingSubset() const noexcept
    {
        decltype(auto) god_of_dof_manager = GodOfDofManager::GetInstance();
        decltype(auto) god_of_dof = god_of_dof_manager.GetGodOfDof(AsMeshId(MeshIndex::sole));
        return god_of_dof.GetNumberingSubset(AsNumberingSubsetId(NumberingSubsetIndex::vectorial));
    }


    template<std::size_t I>
    inline const GlobalVector& Model::GetInitializedVector() const noexcept
    {
        static_assert(I < Utilities::ArraySize<decltype(initialized_vector_list_)>::GetValue());
        assert(!(!initialized_vector_list_[I]));
        return *initialized_vector_list_[I];
    }


    template<std::size_t I>
    inline GlobalVector& Model::GetNonCstInitializedVector() noexcept
    {
        return const_cast<GlobalVector&>(GetInitializedVector<I>());
    }


} // namespace MoReFEM::TestNS::PetscNS::MatrixOperationsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_THIRDPARTY_PETSC_MATRIXOPERATIONS_MODEL_DOT_HXX_
// *** MoReFEM end header guards *** < //
