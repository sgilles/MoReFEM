// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>

#define BOOST_TEST_MODULE petsc_matrix_io
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Tclap/StringPair.hpp"

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Filesystem/File.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Test/ThirdParty/PETSc/Matrix/ToyModel.hpp"
#include "Test/Tools/Fixture/Model.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    // clang-format off
    using fixture_type = TestNS::FixtureNS::Model
    <
        TestNS::PetscNS::MatrixNS::ToyModel,
        TestNS::FixtureNS::call_run_method_at_first_call::no
    >;
    // clang-format on


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(creation, fixture_type)
{
    decltype(auto) model = GetModel();
    static_cast<void>(model);
}


BOOST_FIXTURE_TEST_CASE(equal_to_self, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) matrix = model.GetMatrix();

    BOOST_CHECK(Wrappers::Petsc::AreStrictlyEqual(matrix, matrix));
}


BOOST_FIXTURE_TEST_CASE(copy_is_equal, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) matrix = model.GetMatrix();

    GlobalMatrix copy(matrix);

    BOOST_CHECK(Wrappers::Petsc::AreStrictlyEqual(matrix, copy));
}


BOOST_FIXTURE_TEST_CASE(not_equal, fixture_type)
{
    decltype(auto) model = GetModel();
    decltype(auto) matrix = model.GetMatrix();

    GlobalMatrix zero(matrix);
    zero.ZeroEntries();

    BOOST_CHECK(!Wrappers::Petsc::AreStrictlyEqual(matrix, zero));
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
