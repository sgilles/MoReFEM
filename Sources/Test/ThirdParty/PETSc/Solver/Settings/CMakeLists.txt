add_executable(TestSolverSettings 
               ${CMAKE_CURRENT_LIST_DIR}/test.cpp
               ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
               ${CMAKE_CURRENT_LIST_DIR}/demo.lua)

target_link_libraries(TestSolverSettings
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>
                      ${MOREFEM_BASIC_TEST_TOOLS})

morefem_organize_IDE(TestSolverSettings Test/ThirdParty/PETSc/Solver Test/ThirdParty/PETSc/Solver/Settings)

morefem_boost_test_both_modes(NAME SolverSettings
                              EXE TestSolverSettings
                              TIMEOUT 5
                              LUA ${MOREFEM_ROOT}/Sources/Test/ThirdParty/PETSc/Solver/Settings/demo.lua)
