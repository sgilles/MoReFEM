// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#define BOOST_TEST_MODULE xtensor_det

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp"

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

// Check Xtensor-blas installation is ok and yield the proper result
BOOST_AUTO_TEST_CASE(xtensor_det)
{
    xt::xarray<double> a = { { -2, 2, -3 }, { -1, 1, 3 }, { 2, 0, -1 } };

    const auto d = xt::linalg::det(a);
    constexpr auto expected_value = 18.;

    BOOST_CHECK_EQUAL(d, expected_value);
}


BOOST_AUTO_TEST_CASE(xtensor_slogdet)
{
    xt::xarray<double> a = { { -2, 2, -3 }, { -1, 1, 3 }, { 2, 0, -1 } };
    const auto d = xt::linalg::slogdet(a);

    BOOST_CHECK_EQUAL(std::get<0>(d), 1);
    BOOST_CHECK_EQUAL(std::get<1>(d), 2.8903717578961645);
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
