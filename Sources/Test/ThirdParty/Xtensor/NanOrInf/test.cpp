// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#define BOOST_TEST_MODULE nan_or_inf

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"
#include "ThirdParty/Wrappers/Xtensor/Functions.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    double local_inf = 1. / 0.;

    double local_nan = local_inf * 0.;

} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_AUTO_TEST_CASE(xtensor_vector)
{
    LocalVector vector;
    vector.resize({ 3 });
    vector(0) = 0.;
    vector(1) = -10.;
    vector(2) = 10.;

    BOOST_CHECK(Wrappers::Xtensor::AreNanOrInfValues(vector) == false);

    vector(2) = local_nan;
    BOOST_CHECK(Wrappers::Xtensor::AreNanOrInfValues(vector) == true);

    vector(2) = 0.;
    vector(0) = local_inf;
    BOOST_CHECK(Wrappers::Xtensor::AreNanOrInfValues(vector) == true);
}


BOOST_AUTO_TEST_CASE(xtensor_matrix)
{
    LocalMatrix matrix({ 3, 3 });
    matrix.fill(0.);

    BOOST_CHECK(Wrappers::Xtensor::AreNanOrInfValues(matrix) == false);

    matrix(2, 0) = local_nan;
    BOOST_CHECK(Wrappers::Xtensor::AreNanOrInfValues(matrix) == true);

    matrix(2, 0) = 0.;
    matrix(1, 1) = local_inf;
    BOOST_CHECK(Wrappers::Xtensor::AreNanOrInfValues(matrix) == true);
}

PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
