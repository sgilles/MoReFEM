// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

/*!
//
// \file
//
//

// Copyright (c) Inria. All rights reserved.
*/

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_MOREFEMDATAFORTEST_DOT_HXX_
#define MOREFEM_TEST_TOOLS_MOREFEMDATAFORTEST_DOT_HXX_
// IWYU pragma: private, include "Test/Tools/MoReFEMDataForTest.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Test/Tools/MoReFEMDataForTest.hpp"


#include "Utilities/Environment/Environment.hpp"


namespace MoReFEM
{


    template<class ModelSettingsT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    MoReFEMDataForTest<ModelSettingsT, TimeManagerT>::MoReFEMDataForTest(
        const std::filesystem::path& result_directory_subpath)
    : parent()
    {
        decltype(auto) environment = Utilities::Environment::GetInstance();
        auto test_dir = std::filesystem::path{ environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR") };

        test_dir /= result_directory_subpath;

        parent::SetResultDirectory(test_dir, Advanced::CommandLineFlagsNS::overwrite_directory::yes);

        parent::SetInputData(std::make_unique<input_data_type>());

        parent::InitCheckInvertedElementsSingleton();
        parent::SetTimeManager();
    }


    template<class ModelSettingsT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    MoReFEMDataForTest<ModelSettingsT, TimeManagerT>::~MoReFEMDataForTest() = default;


    template<class ModelSettingsT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const Internal::Parallelism* MoReFEMDataForTest<ModelSettingsT, TimeManagerT>::GetParallelismPtr() const noexcept
    {
        return nullptr; // seems allright for the time being; maybe we'll have to flesh it out later.
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_MOREFEMDATAFORTEST_DOT_HXX_
// *** MoReFEM end header guards *** < //
