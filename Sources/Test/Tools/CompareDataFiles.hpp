// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_COMPAREDATAFILES_DOT_HPP_
#define MOREFEM_TEST_TOOLS_COMPAREDATAFILES_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <vector>

#include "Utilities/Numeric/Numeric.hpp"

#include "Geometry/Mesh/Format.hpp" // IWYU pragma: export

#include "Test/Tools/Internal/CompareDataFilesImpl.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class Directory; }
namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TestNS
{


    /*!
     * \brief Check a file obtained in a test about a model is mostly the same as a reference one, at a given expected
     * numerical precision.
     *
     *
     * \tparam FormatT File of the format considered; a specialization of \a TestNS::ReadFile() must exist for this
     * format (at  the time of this writing it is foreseen for \a Ensight and \a Vizir formats).
     */
    template<MeshNS::Format FormatT>
    class CompareDataFiles : public Internal::CompareDataFilesImpl
    {
      public:
        //! Alias to parent class.
        using parent = Internal::CompareDataFilesImpl;

        /*!
         * \copydoc doxygen_hide_compare_data_files_constructor
         */
        CompareDataFiles(const FilesystemNS::Directory& ref_dir,
                         const FilesystemNS::Directory& obtained_dir,
                         std::string&& filename,
                         double epsilon = NumericNS::DefaultEpsilon<double>(),
                         const std::source_location location = std::source_location::current());

        //! Destructor.
        ~CompareDataFiles() = default;

        //! \copydoc doxygen_hide_copy_constructor
        CompareDataFiles(const CompareDataFiles& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        CompareDataFiles(CompareDataFiles&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        CompareDataFiles& operator=(const CompareDataFiles& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        CompareDataFiles& operator=(CompareDataFiles&& rhs) = delete;

      public:
        /*!
         * \brief Method which reads a result file and interpret it as a list of doubles.
         *
         * This method is the only part that is dependent on the \a FormatT considered.
         *
         * \param[in] file File which content is translated into a list of doubles. It is expected to
         * be in \a FormatT format.
         *
         * \return List of all values.
         */
        std::vector<double> ExtractDoubleValues(const FilesystemNS::File& file);
    };

    /*!
     * \class doxygen_hide_compare_data_files_constructor
     *
     * \brief Constructor.
     *
     * \param[in] ref_dir The reference directory. It is a subdirectory of ModelInstances/ *YourModel*
     * /ExpectedResults, e.g. ${MOREFEM_ROOT}/Sources/ModelInstances/Heat/ExpectedResults/1D/Mesh_1/Ensight6.
     * \param[in] obtained_dir The pendant of \a ref_dir for the outputs given by the model that is expected to have
     * run juste before the call to the result check. It is expected to be a subdirectory somwehere in
     * ${MOREFEM_TEST_OUTPUT_DIR}.
     * \param[in] filename Name of the file being compared. It is expected to be the same in \a ref_dir and in
     * \a obtained_dir and to be located directly there (not in a subdirectory).
     * \copydoc doxygen_hide_source_location
     * \param[in] epsilon Epsilon used for the comparison. A default value is provided; but the parameter
     * is there if you want to play with it.
     */

} // namespace MoReFEM::TestNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/Tools/CompareDataFiles.hxx"

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_COMPAREDATAFILES_DOT_HPP_
// *** MoReFEM end header guards *** < //
