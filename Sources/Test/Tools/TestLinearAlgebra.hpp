// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_TESTLINEARALGEBRA_DOT_HPP_
#define MOREFEM_TEST_TOOLS_TESTLINEARALGEBRA_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <source_location>
#include <vector>

#include "Utilities/LinearAlgebra/LocalAlias.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalMatrix; }
namespace MoReFEM { class GlobalVector; }
namespace MoReFEM { class GodOfDof; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TestNS
{


    /*!
     * \class doxygen_hide_test_global_linear_alg_common_arg
     *
     * The function is written to work both in sequential and in parallel; in parallel ghost values aren't checked.
     *
     * \copydoc doxygen_hide_test_linear_alg_common_arg
     * \param[in] god_of_dof \a GodOfDof onto which the dofs present in the matrix or vector checked is defined.
     */


    /*!
     * \brief Check whether a Petsc matrix includes the expected content.
     *
     * \copydoc doxygen_hide_test_global_linear_alg_common_arg
     *
     * \param[in] obtained The matrix obtained by the computation. As it is for a test, it is expected to be a
     * fairly small matrix.
     * \param[in] expected The expected (program-wise) matrix, given in dense format. Interior vector features all
     * the values on a given row.
     */
    void CheckMatrix(const GodOfDof& god_of_dof,
                     const GlobalMatrix& obtained,
                     const std::vector<std::vector<PetscScalar>>& expected,
                     double epsilon = 1.e-12,
                     const std::source_location location = std::source_location::current());


    /*!
     * \brief Check whether a Petsc vector includes the expected content.
     *
     * \copydoc doxygen_hide_test_global_linear_alg_common_arg
     *
     * \param[in] obtained The vector obtained by the computation.
     * \param[in] expected The expected (program-wise) values to be found in the vector.
     */
    void CheckVector(const GodOfDof& god_of_dof,
                     const GlobalVector& obtained,
                     const std::vector<PetscScalar>& expected,
                     double epsilon = 1.e-12,
                     const std::source_location location = std::source_location::current());


    /*!
     * \brief Check whether a local matrix includes the expected content.
     *
     * \copydoc doxygen_hide_test_linear_alg_common_arg
     *
     * \param[in] obtained The matrix obtained by the computation.
     * \param[in] expected The expected matrix.
     */
    void CheckLocalMatrix(const LocalMatrix& obtained,
                          const LocalMatrix& expected,
                          double epsilon = 1.e-12,
                          const std::source_location location = std::source_location::current());

    /*!
     * \brief Check whether a local vector includes the expected content.
     *
     * \copydoc doxygen_hide_test_linear_alg_common_arg
     *
     * \param[in] obtained The vector obtained by the computation.
     * \param[in] expected The expected vector
     */
    void CheckLocalVector(const LocalVector& obtained,
                          const LocalVector& expected,
                          double epsilon = 1.e-12,
                          const std::source_location location = std::source_location::current());


} // namespace MoReFEM::TestNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Test/Tools/TestLinearAlgebra.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_TESTLINEARALGEBRA_DOT_HPP_
// *** MoReFEM end header guards *** < //
