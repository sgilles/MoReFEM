// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_TEST_TOOLS_COMPAREDATAFILES_DOT_HXX_
#define MOREFEM_TEST_TOOLS_COMPAREDATAFILES_DOT_HXX_
// IWYU pragma: private, include "Test/Tools/CompareDataFiles.hpp"
// *** MoReFEM header guards *** < //


#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Utilities/Filesystem/Directory.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::MeshNS { enum class Format; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::TestNS
{


    template<MeshNS::Format FormatT>
    CompareDataFiles<FormatT>::CompareDataFiles(const FilesystemNS::Directory& a_ref_dir,
                                                const FilesystemNS::Directory& a_obtained_dir,
                                                std::string&& filename,
                                                double epsilon,
                                                const std::source_location location)
    : parent(a_ref_dir, a_obtained_dir, std::move(filename), epsilon, location)
    {
        decltype(auto) reference_file = parent::GetReferenceFile();
        decltype(auto) obtained_file = parent::GetObtainedFile();

        const auto ref_values = ExtractDoubleValues(reference_file);
        const auto obtained_values = ExtractDoubleValues(obtained_file);

        if (ref_values.size() != obtained_values.size())
        {
            std::ostringstream oconv;
            oconv << "Number of values read in reference (" << ref_values.size() << " for file " << reference_file
                  << ") and output (" << obtained_values.size() << " for file " << obtained_file
                  << ") is not the same!";
            throw Exception(oconv.str(), location);
        }

        // An exception is thrown in this function if they aren't very similar.
        CheckAreEquals(ref_values, obtained_values, location);
    }

} // namespace MoReFEM::TestNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
// > *** MoReFEM end header guards *** //
#endif // MOREFEM_TEST_TOOLS_COMPAREDATAFILES_DOT_HXX_
// *** MoReFEM end header guards *** < //
