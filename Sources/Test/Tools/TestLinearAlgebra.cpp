// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <sstream>
#include <string>
#include <tuple>
#include <utility>

#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"
#include "ThirdParty/Wrappers/Xtensor/Functions.hpp"

#include "Core/LinearAlgebra/GlobalMatrix.hpp"
#include "Core/LinearAlgebra/GlobalVector.hpp"

#include "FiniteElement/FiniteElementSpace/GodOfDof.hpp"

#include "Test/Tools/TestLinearAlgebra.hpp"


namespace MoReFEM::TestNS
{


    namespace // anonymous
    {


        /*!
         * \brief Analyze string rendering of lhs and rhs until they differ.
         *
         * This function is far from being efficient and `std::format` would enable to write it more cleanly,
         * but as the function is used when an exception is thrown it's not really an issue.
         *
         * \attention This is expected to be called when lhs and rhs are not deemed equald by `AreEqual` function.
         * There is no assert to check this as it is an anonymous function, but don't pass it with strictly equals lhs
         * and rhs.
         *
         * \param[in] precision_limit If both strings are still equal when this precision is reached, stop the loop
         * and return false.
         *
         * \return The string rendering for lhs and rhs for the precision that showed a difference. If none found because
         * precision_limit is reached; it is expected to return two identifcal values; it is not the job of current
         * function to check it.
         */
        std::pair<std::string, std::string>
        FindDivergentExpression(double lhs, double rhs, std::streamsize precision_limit = 16)
        {
            std::ostringstream lhs_conv;
            std::ostringstream rhs_conv;

            lhs_conv << lhs;
            rhs_conv << rhs;

            if (lhs_conv.str() != rhs_conv.str())
                return { lhs_conv.str(), rhs_conv.str() };

            auto precision = lhs_conv.precision();

            while ((lhs_conv.str() == rhs_conv.str()) && (precision < precision_limit))
            {
                ++precision;
                lhs_conv.precision(precision);
                rhs_conv.precision(precision);

                lhs_conv.str("");
                rhs_conv.str("");

                lhs_conv << lhs;
                rhs_conv << rhs;
            }

            return { lhs_conv.str(), rhs_conv.str() };
        }


    } // namespace


    void CheckMatrix(const GodOfDof& god_of_dof,
                     const GlobalMatrix& obtained,
                     const std::vector<std::vector<PetscScalar>>& expected,
                     const double epsilon,
                     const std::source_location location)
    {
        PetscInt Nrow, Ncol;
        std::tie(Nrow, Ncol) = obtained.GetProgramWiseSize(location);

        const auto Nexpected_program_wise_row = expected.size();

        assert(Nexpected_program_wise_row > 0);

        const auto Ncol_first_row = expected.front().size();

#ifndef NDEBUG
        {
            for (auto i = 1ul; i < Nexpected_program_wise_row; ++i)
            {
                assert("The expected matrix is assumed to be dense in this function."
                       && expected[i].size() == Ncol_first_row);
            }
        }
#endif // NDEBUG

        if (Ncol_first_row != static_cast<std::size_t>(Ncol))
        {
            std::ostringstream oconv;
            oconv << "Mismatch between expected and obtained matrix format: a matrix with " << Ncol_first_row
                  << " columns was expected but the obtained matrix got " << Ncol << '.';
            throw Exception(oconv.str(), location);
        }


        if (Nexpected_program_wise_row != static_cast<std::size_t>(Nrow))
        {
            std::ostringstream oconv;
            oconv << "Mismatch between expected and obtained matrix format: a matrix with "
                  << Nexpected_program_wise_row << " program-wise rows was expected but the obtained matrix got "
                  << Nrow << '.';
            throw Exception(oconv.str(), location);
        }

        decltype(auto) row_numbering_subset = obtained.GetRowNumberingSubset();

        decltype(auto) processor_wise_dof_list = god_of_dof.GetProcessorWiseDofList();

        for (const auto& dof_ptr : processor_wise_dof_list)
        {
            assert(!(!dof_ptr));
            const auto& dof = *dof_ptr;

            if (!dof.IsInNumberingSubset(row_numbering_subset))
                continue;

            const auto program_wise_index = static_cast<PetscInt>(dof.GetProgramWiseIndex(row_numbering_subset).Get());

            assert(program_wise_index < static_cast<PetscInt>(expected.size())
                   && "Should be guaranteed by previous asserts!");
            decltype(auto) expected_row = expected[static_cast<std::size_t>(program_wise_index)];

            for (auto col = 0; col < Ncol; ++col)
            {
                // Remember: Petsc convention is that 0 is returned if location is sparse.
                const auto obtained_value = obtained.GetValue(program_wise_index, col, location);

                const auto expected_value = expected_row[static_cast<std::size_t>(col)];

                if (!NumericNS::AreEqual(obtained_value, expected_value, epsilon))
                {
                    auto [obtained_str, expected_str] = FindDivergentExpression(obtained_value, expected_value);

                    std::ostringstream oconv;

                    if (obtained_str != expected_str)
                        oconv << "Mismatch between expected and obtained matrix: value obtained for program-wise ("
                              << program_wise_index << ", " << col << ") was " << obtained_str << " whereas "
                              << expected_str << " was expected.";
                    else
                        oconv << "Mismatch between expected and obtained matrix for the value obtained "
                                 "for program-wise ("
                              << program_wise_index << ", " << col
                              << "); however the difference is so small it "
                                 "doesn't appear in string rendering. Probably loosen up the epsilon chosen in the "
                                 "comparison.";

                    throw Exception(oconv.str(), location);
                }
            }
        }
    }


    void CheckVector(const GodOfDof& god_of_dof,
                     const GlobalVector& obtained,
                     const std::vector<PetscScalar>& expected,
                     const double epsilon,
                     const std::source_location location)
    {
        Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> content(obtained, location);

        const auto size = obtained.GetProgramWiseSize(location);

        if (size != static_cast<PetscInt>(expected.size()))
        {
            std::ostringstream oconv;
            oconv << "Mismatch between expected and obtained vector format: a vector with " << expected.size()
                  << " rows was expected but the obtained vector got " << size << '.';
            throw Exception(oconv.str(), location);
        }

        decltype(auto) numbering_subset = obtained.GetNumberingSubset();
        decltype(auto) processor_wise_dof_list = god_of_dof.GetProcessorWiseDofList();

        for (const auto& dof_ptr : processor_wise_dof_list)
        {
            assert(!(!dof_ptr));
            const auto& dof = *dof_ptr;

            if (!dof.IsInNumberingSubset(numbering_subset))
                continue;

            const auto processor_wise_index = dof.GetProcessorWiseOrGhostIndex(numbering_subset);

            const auto program_wise_index = dof.GetProgramWiseIndex(numbering_subset);

            const auto obtained_value = content.GetValue(processor_wise_index.Get());

            assert(static_cast<std::size_t>(processor_wise_index) < expected.size());
            const auto expected_value = expected[static_cast<std::size_t>(program_wise_index)];

            if (!NumericNS::AreEqual(obtained_value, expected_value, epsilon))
            {
                auto [obtained_str, expected_str] = FindDivergentExpression(obtained_value, expected_value);

                std::ostringstream oconv;

                if (obtained_str != expected_str)
                    oconv << "Mismatch between expected and obtained vector: value obtained for program-wise ("
                          << dof.GetProgramWiseIndex(numbering_subset) << ") was " << obtained_str << " whereas "
                          << expected_str << " was expected.";
                else
                    oconv << "Mismatch between expected and obtained vector for the value obtained for program-wise ("
                          << dof.GetProgramWiseIndex(numbering_subset)
                          << "); however the difference is so small it "
                             "doesn't appear in string rendering. Probably loosen up the epsilon chosen in the "
                             "comparison.";

                throw Exception(oconv.str(), location);
            }
        }
    }


    void CheckLocalMatrix(const LocalMatrix& obtained,
                          const LocalMatrix& expected,
                          double epsilon,
                          const std::source_location location)
    {
        if (obtained.shape() != expected.shape())
        {
            std::ostringstream oconv;
            oconv << "Mismatch between expected and obtained local matrix: shape is not the same.";
            throw Exception(oconv.str(), location);
        }

        const auto diff = obtained - expected;

        if (!Wrappers::Xtensor::IsZeroMatrix(diff, epsilon))
        {
            std::ostringstream oconv;
            oconv << "Mismatch between expected and obtained local matrix: values don't match." << std::endl;
            oconv << "Expected = " << expected << std::endl;
            oconv << "Obtained = " << obtained << std::endl;
            throw Exception(oconv.str(), location);
        }
    }


    void CheckLocalVector(const LocalVector& obtained,
                          const LocalVector& expected,
                          double epsilon,
                          const std::source_location location)
    {
        if (obtained.shape() != expected.shape())
        {
            std::ostringstream oconv;
            oconv << "Mismatch between expected and obtained local vector: shape is not the same.";
            throw Exception(oconv.str(), location);
        }

        const auto diff = obtained - expected;

        if (!Wrappers::Xtensor::IsZeroVector(diff, epsilon))
        {
            std::ostringstream oconv;
            oconv << "Mismatch between expected and obtained local vector: values don't match.";
            oconv << "Expected = " << expected << std::endl;
            oconv << "Obtained = " << obtained << std::endl;
            throw Exception(oconv.str(), location);
        }
    }


} // namespace MoReFEM::TestNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
