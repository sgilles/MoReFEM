// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include <cstdlib>

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#define BOOST_TEST_MODULE vectorial_parameter_from_input_data_invalid_dimension
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"

#include "Test/Tools/Fixture/Model.hpp"

#include "Test/Parameter/FromInputData/VectorialInputData.hpp"
#include "Test/Tools/BareModel.hpp"
#include "Test/Tools/ClearSingletons.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"
#include "Test/Tools/TestLinearAlgebra.hpp"


using namespace MoReFEM;
using namespace MoReFEM::TestNS::FromInputDataNS;


namespace // anonymous
{


    //! \copydoc doxygen_hide_time_manager_type_alias
    using time_manager_type = TimeManager<::MoReFEM::TimeManagerNS::Policy::None>;

    // clang-format off
    using model_type =
    MoReFEM::TestNS::BareModel
    <
        MoReFEM::TestNS::FromInputDataNS::morefem_data_type,
        MoReFEM::DoConsiderProcessorWiseLocal2Global::yes
    >;
    // clang-format on


    // Workaround as Boost macros choke on invocation with two template parameters...
    template<class SpatialBehaviourT, class... Args>
    auto InitVectorialParameterFromInputDataFirstTemplateFixed(Args&&... args)
    {
        return InitVectorialParameterFromInputData<SpatialBehaviourT, time_manager_type>(args...);
    }


} // namespace


PRAGMA_DIAGNOSTIC(push)
#ifdef __clang__
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"
#endif // __clang__


BOOST_FIXTURE_TEST_CASE(too_big_vector, TestNS::FixtureNS::TestEnvironment)
{
    FilesystemNS::File lua_file{ std::filesystem::path(
        "${MOREFEM_ROOT}/Sources/Test/Parameter/FromInputData/too_big_vector.lua") };

    auto morefem_data = TestNS::FromInputDataNS::morefem_data_type{ std::move(lua_file) };


    model_type model(morefem_data);
    model.Initialize();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain));

    BOOST_CHECK_THROW(
        InitVectorialParameterFromInputDataFirstTemplateFixed<ConstantParameter>("constant", full_domain, morefem_data),
        ExceptionNS::ParameterNS::InconsistentVectorDimension);

    BOOST_CHECK_THROW(InitVectorialParameterFromInputDataFirstTemplateFixed<PiecewiseConstantByDomainParameter>(
                          "piecewise", full_domain, morefem_data),
                      ExceptionNS::ParameterNS::InconsistentVectorDimensionForDomain);
}


BOOST_FIXTURE_TEST_CASE(too_small_vector, TestNS::FixtureNS::TestEnvironment)
{
    TestNS::ClearSingletons<time_manager_type>::Do();

    FilesystemNS::File lua_file{ std::filesystem::path(
        "${MOREFEM_ROOT}/Sources/Test/Parameter/FromInputData/too_small_vector.lua") };

    auto morefem_data = TestNS::FromInputDataNS::morefem_data_type{ std::move(lua_file) };


    model_type model(morefem_data);
    model.Initialize();

    decltype(auto) domain_manager = DomainManager::GetInstance();
    decltype(auto) full_domain = domain_manager.GetDomain(AsDomainId(DomainIndex::full_domain));

    BOOST_CHECK_THROW(
        InitVectorialParameterFromInputDataFirstTemplateFixed<ConstantParameter>("constant", full_domain, morefem_data),
        ExceptionNS::ParameterNS::InconsistentVectorDimension);

    BOOST_CHECK_THROW(InitVectorialParameterFromInputDataFirstTemplateFixed<PiecewiseConstantByDomainParameter>(
                          "piecewise", full_domain, morefem_data),
                      ExceptionNS::ParameterNS::InconsistentVectorDimensionForDomain);
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
