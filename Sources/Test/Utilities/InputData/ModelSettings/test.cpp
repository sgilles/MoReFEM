// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#define BOOST_TEST_MODULE model_settings

#include <filesystem>

#include "Utilities/InputData/Extract.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Core/InputData/Instances/Core/NumberingSubset.hpp"
#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"
#include "Test/Utilities/InputData/ModelSettings/ModelSettings.hpp"


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

using namespace MoReFEM;
using namespace MoReFEM::TestNS::ReadInputDataNS;


namespace MoReFEM::TestNS::ReadInputDataNS
{


    void ModelSettings::Init()
    {
        Add<Section1::LeafInSection1>(std::vector<double>{ 2.1, -3.2, 5. });
        Add<Section1::SubsectionInSection1::LeafInSubSection1>(std::string{ "${MOREFEM_ROOT}/Sources/Utilities" });
        Add<LeafInNoEnclosingSection>(std::string{ "Hello world!" });
        Add<Section2::Leaf1InSection2>(std::map<int, double>{ { 3, 7.3 }, { 2, -12 } });
    }


} // namespace MoReFEM::TestNS::ReadInputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

BOOST_FIXTURE_TEST_CASE(add_value_to_leaf_not_in_tuple, TestNS::FixtureNS::TestEnvironment)
{
    TestNS::ReadInputDataNS::ModelSettings model_settings;
    model_settings.Init();
    model_settings.CheckTupleCompletelyFilled();

    BOOST_CHECK_THROW(model_settings.Add<Section2::Leaf2InSection2>(std::string{ "Foo" }),
                      MoReFEM::InputDataNS::ExceptionNS::NoEntryInModelSettings);
}


BOOST_FIXTURE_TEST_CASE(find_leaf, TestNS::FixtureNS::TestEnvironment)
{
    TestNS::ReadInputDataNS::ModelSettings model_settings;
    model_settings.Init();
    model_settings.CheckTupleCompletelyFilled();

    static_assert(model_settings.template Find<LeafInNoEnclosingSection>());

    static_assert(model_settings.template Find<Section2::Leaf1InSection2>());
    static_assert(!model_settings.template Find<Section2::Leaf2InSection2>());

    static_assert(model_settings.template Find<Section1::SubsectionInSection1>());
}


BOOST_FIXTURE_TEST_CASE(find_section, TestNS::FixtureNS::TestEnvironment)
{
    TestNS::ReadInputDataNS::ModelSettings model_settings;
    model_settings.Init();
    model_settings.CheckTupleCompletelyFilled();

    static_assert(model_settings.template Find<Section1>());

    static_assert(model_settings.template Find<Section1::SubsectionInSection1>());

    static_assert(!model_settings.template Find<Section2::SubsectionInSection2>());
}


BOOST_FIXTURE_TEST_CASE(leaf, TestNS::FixtureNS::TestEnvironment)
{
    TestNS::ReadInputDataNS::ModelSettings model_settings;
    model_settings.Init();
    model_settings.CheckTupleCompletelyFilled();

    decltype(auto) value = Internal::InputDataNS::ExtractLeaf<LeafInNoEnclosingSection>::Value(model_settings);
    BOOST_CHECK_EQUAL(value, "Hello world!");
}


BOOST_FIXTURE_TEST_CASE(leaf_in_section, TestNS::FixtureNS::TestEnvironment)
{
    TestNS::ReadInputDataNS::ModelSettings model_settings;
    model_settings.Init();
    model_settings.CheckTupleCompletelyFilled();

    {
        decltype(auto) value = Internal::InputDataNS::ExtractLeaf<Section1::LeafInSection1>::Value(model_settings);

        BOOST_REQUIRE_EQUAL(value.size(), 3ul);
        BOOST_CHECK_EQUAL(value[0], 2.1);
        BOOST_CHECK_EQUAL(value[1], -3.2);
        BOOST_CHECK_EQUAL(value[2], 5.);
    }

    {
        decltype(auto) value = Internal::InputDataNS::ExtractLeaf<Section2::Leaf1InSection2>::Value(model_settings);

        BOOST_REQUIRE_EQUAL(value.size(), 2ul);

        {
            auto it = value.find(3);
            BOOST_REQUIRE(it != value.cend());
            BOOST_CHECK_EQUAL(it->second, 7.3);
        }

        {
            auto it = value.find(2);
            BOOST_REQUIRE(it != value.cend());
            BOOST_CHECK_EQUAL(it->second, -12.);
        }
    }
}


BOOST_FIXTURE_TEST_CASE(extract_section, TestNS::FixtureNS::TestEnvironment)
{
    TestNS::ReadInputDataNS::ModelSettings model_settings;
    model_settings.Init();
    model_settings.CheckTupleCompletelyFilled();

    {
        decltype(auto) section = Internal::InputDataNS::ExtractSection<Section1>::Value(model_settings);
        BOOST_CHECK_EQUAL(section.GetFullName(), "Section1");
    }

    {
        decltype(auto) section =
            Internal::InputDataNS::ExtractSection<Section1::SubsectionInSection1>::Value(model_settings);
        BOOST_CHECK_EQUAL(section.GetFullName(), "Section1.SubsectionInSection1");
    }
}


BOOST_FIXTURE_TEST_CASE(print_keys, TestNS::FixtureNS::TestEnvironment)
{
    if (GetMpi().IsRootProcessor())
    {
        TestNS::ReadInputDataNS::ModelSettings model_settings;
        model_settings.Init();
        model_settings.CheckTupleCompletelyFilled();

        auto output_dir =
            FilesystemNS::Directory(GetMpi(),
                                    std::filesystem::path{ "${MOREFEM_TEST_OUTPUT_DIR}/Utilities/ModelSettings" },
                                    FilesystemNS::behaviour::overwrite);

        output_dir.ActOnFilesystem();

        // Write into a file all the keys found
        {
            FilesystemNS::File out = output_dir.AddFile("test_model_settings.txt");
            auto stream = out.NewContent();
            model_settings.PrintKeys(stream);
        }

        // Read this file line by line to fill a std::vector with all keys.
        std::vector<std::string> content;

        {
            FilesystemNS::File in = output_dir.AddFile("test_model_settings.txt");

            BOOST_REQUIRE(in.DoExist());

            auto stream = in.Read();
            std::string buf;
            while (stream >> buf)
                content.emplace_back(buf);
        }
        std::sort(content.begin(), content.end());
        BOOST_REQUIRE_EQUAL(content.size(), 4ul);

        BOOST_CHECK_EQUAL(content[0], "LeafInNoEnclosingSection");
        BOOST_CHECK_EQUAL(content[1], "Section1.LeafInSection1");
        BOOST_CHECK_EQUAL(content[2], "Section1.SubsectionInSection1.LeafInSubSection1");
        BOOST_CHECK_EQUAL(content[3], "Section2.FirstLeafInSection2");
    }
}


BOOST_FIXTURE_TEST_CASE(extract_keys, TestNS::FixtureNS::TestEnvironment)
{
    if (GetMpi().IsRootProcessor())
    {
        TestNS::ReadInputDataNS::ModelSettings model_settings;
        model_settings.Init();
        model_settings.CheckTupleCompletelyFilled();

        auto keys = model_settings.ExtractKeys();

        BOOST_REQUIRE_EQUAL(keys.size(), 4ul);

        BOOST_CHECK_EQUAL(keys[0], "LeafInNoEnclosingSection");
        BOOST_CHECK_EQUAL(keys[1], "Section1.LeafInSection1");
        BOOST_CHECK_EQUAL(keys[2], "Section1.SubsectionInSection1.LeafInSubSection1");
        BOOST_CHECK_EQUAL(keys[3], "Section2.FirstLeafInSection2");
    }
}


BOOST_FIXTURE_TEST_CASE(unused, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) mpi = GetMpi();
    TestNS::ReadInputDataNS::ModelSettings model_settings;
    model_settings.Init();
    model_settings.CheckTupleCompletelyFilled();

    {
        // At this point, all elements are left unused.
        auto unused_leaf_list = model_settings.ComputeUnusedLeafList(mpi);

        if (mpi.IsRootProcessor())
        {
            BOOST_REQUIRE_EQUAL(unused_leaf_list.size(), 4ul);

            BOOST_CHECK_EQUAL(unused_leaf_list[0ul], "LeafInNoEnclosingSection");
            BOOST_CHECK_EQUAL(unused_leaf_list[1ul], "Section1.LeafInSection1");
            BOOST_CHECK_EQUAL(unused_leaf_list[2ul], "Section1.SubsectionInSection1.LeafInSubSection1");
            BOOST_CHECK_EQUAL(unused_leaf_list[3ul], "Section2.FirstLeafInSection2");
        } else
            BOOST_CHECK(unused_leaf_list.empty()); // filled only on root processor!
    }

    {
        auto leaf_count_as_used =
            Internal::InputDataNS::ExtractLeaf<Section1::SubsectionInSection1::LeafInSubSection1>::Value(
                model_settings);
        static_cast<void>(leaf_count_as_used);

        // Now one is counted as used
        auto unused_leaf_list = model_settings.ComputeUnusedLeafList(mpi);

        if (mpi.IsRootProcessor())
        {
            BOOST_REQUIRE_EQUAL(unused_leaf_list.size(), 3ul);

            BOOST_CHECK_EQUAL(unused_leaf_list[0ul], "LeafInNoEnclosingSection");
            BOOST_CHECK_EQUAL(unused_leaf_list[1ul], "Section1.LeafInSection1");
            BOOST_CHECK_EQUAL(unused_leaf_list[2ul], "Section2.FirstLeafInSection2");
        } else
            BOOST_CHECK(unused_leaf_list.empty()); // filled only on root processor!
    }

    if (mpi.Nprocessor<int>() == 4)
    {
        if (mpi.GetRank<int>() == 0)
        {
            auto leaf_count_as_used =
                Internal::InputDataNS::ExtractLeaf<Section1::LeafInSection1>::Value(model_settings);
            static_cast<void>(leaf_count_as_used);
        }

        if (mpi.GetRank<int>() == 2)
        {
            auto leaf_count_as_used =
                Internal::InputDataNS::ExtractLeaf<Section2::Leaf1InSection2>::Value(model_settings);
            static_cast<void>(leaf_count_as_used);
        }

        auto unused_leaf_list = model_settings.ComputeUnusedLeafList(mpi);

        if (mpi.IsRootProcessor())
        {
            BOOST_REQUIRE_EQUAL(mpi.GetRank<int>(), 0);
            BOOST_REQUIRE_EQUAL(unused_leaf_list.size(), 1ul);
            BOOST_CHECK_EQUAL(unused_leaf_list[0ul], "LeafInNoEnclosingSection");
        } else
            BOOST_CHECK(unused_leaf_list.empty()); // filled only on root processor!
    }
}

BOOST_FIXTURE_TEST_CASE(duplicated_value, TestNS::FixtureNS::TestEnvironment)
{
    TestNS::ReadInputDataNS::ModelSettings model_settings;

    model_settings.Add<Section1::LeafInSection1>(std::vector<double>{ 2.1, -3.2, 5. });

    BOOST_CHECK_THROW(model_settings.Add<Section1::LeafInSection1>(std::vector<double>{ 0., 0. }),
                      MoReFEM::InputDataNS::ExceptionNS::ValueCantBeSetTwice);
}


BOOST_FIXTURE_TEST_CASE(incompletely_filled, TestNS::FixtureNS::TestEnvironment)
{
    TestNS::ReadInputDataNS::ModelSettings model_settings;

    model_settings.Add<Section1::LeafInSection1>(std::vector<double>{ 2.1, -3.2, 5. });

    BOOST_CHECK_THROW(model_settings.CheckTupleCompletelyFilled(),
                      MoReFEM::InputDataNS::ExceptionNS::ModelSettingsNotCompletelyFilled);
}


PRAGMA_DIAGNOSTIC(pop)
