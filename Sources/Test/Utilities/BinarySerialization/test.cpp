// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <array>
#include <vector>

#define BOOST_TEST_MODULE binary_serialization

#include "Utilities/AsciiOrBinary/BinarySerialization.hpp"
#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Exceptions/GracefulExit.hpp"
#include "Utilities/Filesystem/Directory.hpp"

#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


namespace // anonymous
{

    struct fixture : public TestNS::FixtureNS::TestEnvironment
    {

        explicit fixture();

        const FilesystemNS::Directory& GetPlaygroundDirectory() const noexcept;

      private:
        std::unique_ptr<FilesystemNS::Directory> playground_{ nullptr };
    };


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(not_existing, fixture)
{
    decltype(auto) output_dir = GetPlaygroundDirectory();
    const auto unexisting_file = output_dir.AddFile("not_existing_file.really_not");

    BOOST_CHECK(unexisting_file.DoExist() == false);

    BOOST_CHECK_THROW(Advanced::UnserializeVectorFromBinaryFile<int>(unexisting_file),
                      ExceptionNS::BinaryNS::NotExistingFile);
}


BOOST_FIXTURE_TEST_CASE(ascii_file, fixture)
{
    decltype(auto) output_dir = GetPlaygroundDirectory();
    const auto ascii_file = output_dir.AddFile("ascii.dat");

    std::ofstream out{ ascii_file.NewContent() };

    std::vector<int> vec{ 3, -4, 5, 21, 0 };

    for (auto& item : vec)
        out << item;

    out.close();

    BOOST_CHECK_THROW(Advanced::UnserializeVectorFromBinaryFile<int>(ascii_file),
                      ExceptionNS::BinaryNS::LoadBinaryFileException);
}


BOOST_FIXTURE_TEST_CASE(vector_int, fixture)
{
    std::vector<int> vec{ 3, -4, 5, 21, 0 };

    const auto vector_size = vec.size();


    decltype(auto) output_dir = GetPlaygroundDirectory();

    const auto file = output_dir.AddFile("vector_int.bin");
    Advanced::SerializeVectorIntoBinaryFile(file, vec);

    {
        const auto unserialized = Advanced::UnserializeVectorFromBinaryFile<int>(file);
        BOOST_CHECK(vec == unserialized);
    }

    {
        const auto unserialized = Advanced::UnserializeVectorFromBinaryFile<int>(file, vector_size);
        BOOST_CHECK(vec == unserialized);
    }

    {
        BOOST_CHECK_THROW(Advanced::UnserializeVectorFromBinaryFile<int>(file, 2ul),
                          ExceptionNS::BinaryNS::NotMatchingSize);
    }

    {
        if constexpr (sizeof(double) != sizeof(int))
            BOOST_CHECK_THROW(Advanced::UnserializeVectorFromBinaryFile<double>(file),
                              ExceptionNS::BinaryNS::LoadBinaryFileException);
    }

    {
        if constexpr (sizeof(long) != sizeof(int))
            BOOST_CHECK_THROW(Advanced::UnserializeVectorFromBinaryFile<long>(file),
                              ExceptionNS::BinaryNS::LoadBinaryFileException);
    }
}


BOOST_FIXTURE_TEST_CASE(vector_double, fixture)
{
    std::vector<double> vec{ 3., -4.2, 5.3, 2.121, 1.e-7 };
    constexpr auto epsilon = 1.e-9;
    const auto vector_size = vec.size();

    decltype(auto) output_dir = GetPlaygroundDirectory();

    const auto file = output_dir.AddFile("vector_double.bin");
    Advanced::SerializeVectorIntoBinaryFile(file, vec);

    {
        const auto unserialized = Advanced::UnserializeVectorFromBinaryFile<double>(file);

        BOOST_CHECK_EQUAL(unserialized.size(), vector_size);
        BOOST_CHECK_EQUAL(vector_size, 5ul);


        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[0])(unserialized[0])(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[1])(unserialized[1])(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[2])(unserialized[2])(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[3])(unserialized[3])(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[4])(unserialized[4])(epsilon));
    }

    {
        const auto unserialized = Advanced::UnserializeVectorFromBinaryFile<double>(file, vector_size);
        BOOST_CHECK_EQUAL(unserialized.size(), vector_size);
        BOOST_CHECK_EQUAL(vector_size, 5ul);


        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[0])(unserialized[0])(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[1])(unserialized[1])(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[2])(unserialized[2])(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[3])(unserialized[3])(epsilon));
        BOOST_CHECK_PREDICATE(NumericNS::AreEqual<double>, (vec[4])(unserialized[4])(epsilon));
    }

    {
        BOOST_CHECK_THROW(Advanced::UnserializeVectorFromBinaryFile<double>(file, 2ul),
                          ExceptionNS::BinaryNS::NotMatchingSize);
    }
}

PRAGMA_DIAGNOSTIC(pop)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    fixture::fixture()
    {
        decltype(auto) environment = Utilities::Environment::CreateOrGetInstance();
        decltype(auto) test_dir = environment.GetEnvironmentVariable("MOREFEM_TEST_OUTPUT_DIR");

        decltype(auto) mpi = GetMpi();

        std::filesystem::path path(test_dir);
        path /= "Utilities";
        path /= "BinarySerialization";

        playground_ = std::make_unique<FilesystemNS::Directory>(mpi, path, FilesystemNS::behaviour::overwrite);

        static bool is_first_call{ true };

        if (is_first_call)
        {
            playground_->ActOnFilesystem();
            is_first_call = false;
        }
    }


    const FilesystemNS::Directory& fixture::GetPlaygroundDirectory() const noexcept
    {
        assert(!(!playground_));
        return *playground_;
    }


} // namespace
