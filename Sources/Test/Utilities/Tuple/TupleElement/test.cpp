// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#define BOOST_TEST_MODULE tuple_has_type
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include <type_traits> // IWYU pragma: keep

#include "Utilities/Containers/Tuple/Tuple.hpp"


using namespace MoReFEM;


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_AUTO_TEST_CASE(tuple_element_with_fallback_non_empty_tuple)
{
    using tuple = std::tuple<int, float, double*, float, int&>;

    // BOOST_CHECK macro fails to compile if std::is_same is directly fed to it...
    {
        constexpr auto found_correct_element =
            std::is_same<Utilities::Tuple::tuple_element_with_fallback<0ul, tuple>::type, int>::value;
        BOOST_CHECK(found_correct_element);
    }

    {
        constexpr auto found_correct_element =
            std::is_same<Utilities::Tuple::tuple_element_with_fallback<1ul, tuple>::type, float>::value;
        BOOST_CHECK(found_correct_element);
    }

    {
        constexpr auto found_correct_element =
            std::is_same<Utilities::Tuple::tuple_element_with_fallback<2ul, tuple>::type, double*>::value;
        BOOST_CHECK(found_correct_element);
    }

    {
        constexpr auto found_correct_element =
            std::is_same<Utilities::Tuple::tuple_element_with_fallback<3ul, tuple>::type, float>::value;
        BOOST_CHECK(found_correct_element);
    }

    {
        constexpr auto found_correct_element =
            std::is_same<Utilities::Tuple::tuple_element_with_fallback<4ul, tuple>::type, int&>::value;
        BOOST_CHECK(found_correct_element);
    }

    {
        constexpr auto found_correct_element =
            std::is_same<Utilities::Tuple::tuple_element_with_fallback<5ul, tuple>::type, std::false_type>::value;
        BOOST_CHECK(found_correct_element);
    }
}


BOOST_AUTO_TEST_CASE(tuple_element_with_fallback_empty_tuple)
{
    constexpr auto callback =
        std::is_same<Utilities::Tuple::tuple_element_with_fallback<0ul, std::tuple<>>::type, std::false_type>::value;
    BOOST_CHECK(callback);
}


PRAGMA_DIAGNOSTIC(pop)


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
