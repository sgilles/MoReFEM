add_executable(TestTupleElement)

target_sources(TestTupleElement
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
)

target_link_libraries(TestTupleElement
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>)

morefem_boost_test_sequential_mode(NAME TupleElement
                                   EXE TestTupleElement
                                   TIMEOUT 5)

morefem_organize_IDE(TestTupleElement Test/Utilities/Tuple Test/Utilities/Tuple/TupleElement)
