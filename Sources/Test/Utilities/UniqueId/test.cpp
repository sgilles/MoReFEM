// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <algorithm>
#include <memory>

#define BOOST_TEST_MODULE unique_id
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/UniqueId/Exceptions/UniqueId.hpp"
#include "Utilities/UniqueId/UniqueId.hpp"

using namespace MoReFEM;

PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "Test/Tools/ClearSingletons.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"


namespace // anonymous
{


    struct ClassNoIdAllowed : public MoReFEM::Crtp::UniqueId<ClassNoIdAllowed,
                                                             std::size_t,
                                                             MoReFEM::UniqueIdNS::AssignationMode::manual,
                                                             MoReFEM::UniqueIdNS::DoAllowNoId::yes>
    {
        using parent = MoReFEM::Crtp::UniqueId<ClassNoIdAllowed,
                                               std::size_t,
                                               MoReFEM::UniqueIdNS::AssignationMode::manual,
                                               MoReFEM::UniqueIdNS::DoAllowNoId::yes>;

        ClassNoIdAllowed() : parent{ nullptr }
        { }


        ClassNoIdAllowed(std::size_t id) : parent(id)
        { }

        static const std::string& ClassName()
        {
            static const std::string ret = "ClassNoIdAllowed";
            return ret;
        }
    };


    struct ClassManualId1 : public MoReFEM::Crtp::UniqueId<ClassManualId1,
                                                           std::size_t,
                                                           MoReFEM::UniqueIdNS::AssignationMode::manual,
                                                           MoReFEM::UniqueIdNS::DoAllowNoId::no>
    {
        using parent = MoReFEM::Crtp::UniqueId<ClassManualId1,
                                               std::size_t,
                                               MoReFEM::UniqueIdNS::AssignationMode::manual,
                                               MoReFEM::UniqueIdNS::DoAllowNoId::no>;

        ClassManualId1(std::size_t id) : parent(id)
        { }

        static const std::string& ClassName()
        {
            static const std::string ret = "ClassManualId1";
            return ret;
        }
    };


    template<class T>
    struct ClassManualId2 : public MoReFEM::Crtp::UniqueId<ClassManualId2<T>,
                                                           std::size_t,
                                                           MoReFEM::UniqueIdNS::AssignationMode::manual,
                                                           MoReFEM::UniqueIdNS::DoAllowNoId::no>
    {
        using parent = MoReFEM::Crtp::UniqueId<ClassManualId2<T>,
                                               std::size_t,
                                               MoReFEM::UniqueIdNS::AssignationMode::manual,
                                               MoReFEM::UniqueIdNS::DoAllowNoId::no>;

        ClassManualId2(std::size_t id) : parent(id)
        { }

        static const std::string& ClassName()
        {
            static const std::string ret = "ClassManualId2";
            return ret;
        }
    };


    struct ClassAutomaticId : public MoReFEM::Crtp::UniqueId<ClassAutomaticId,
                                                             std::size_t,
                                                             MoReFEM::UniqueIdNS::AssignationMode::automatic,
                                                             MoReFEM::UniqueIdNS::DoAllowNoId::no>
    {

        using parent = MoReFEM::Crtp::UniqueId<ClassAutomaticId,
                                               std::size_t,
                                               MoReFEM::UniqueIdNS::AssignationMode::automatic,
                                               MoReFEM::UniqueIdNS::DoAllowNoId::no>;

        ClassAutomaticId() : parent()
        { }
    };


} // namespace


namespace Foo
{

    namespace Bar
    {


        struct ClassWithinNamespaces : public MoReFEM::Crtp::UniqueId<ClassWithinNamespaces,
                                                                      std::size_t,
                                                                      MoReFEM::UniqueIdNS::AssignationMode::manual,
                                                                      MoReFEM::UniqueIdNS::DoAllowNoId::no>
        {
            using parent = MoReFEM::Crtp::UniqueId<ClassWithinNamespaces,
                                                   std::size_t,
                                                   MoReFEM::UniqueIdNS::AssignationMode::manual,
                                                   MoReFEM::UniqueIdNS::DoAllowNoId::no>;

            ClassWithinNamespaces(std::size_t id) : parent(id)
            { }

            static const std::string& ClassName()
            {
                static const std::string ret = "ClassWithinNamespaces";
                return ret;
            }
        };

    } // namespace Bar

} // namespace Foo


BOOST_AUTO_TEST_CASE(manual_id)
{
    ClassManualId1 first_object(4ul);
    BOOST_CHECK_EQUAL(first_object.GetUniqueId(), 4ul);

    ClassManualId1 second_object(10ul);
    BOOST_CHECK_EQUAL(second_object.GetUniqueId(), 10ul);

    PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/unused-variable.hpp"

    BOOST_CHECK_THROW(auto redundant_object = ClassManualId1(4ul), Internal::ExceptionsNS::UniqueId::AlreadyExistingId);

    BOOST_CHECK_THROW(auto reserved_value_object = ClassManualId1(NumericNS::UninitializedIndex<std::size_t>()),
                      Internal::ExceptionsNS::UniqueId::ReservedValue);
    PRAGMA_DIAGNOSTIC(pop)

    // > *** MoReFEM Doxygen end of group *** //
    ///@} // \addtogroup TestGroup
    // *** MoReFEM Doxygen end of group *** < //

    // Should not throw: this is not the same class.
    ClassManualId2<int> int_template(10ul);
    BOOST_CHECK_EQUAL(int_template.GetUniqueId(), 10ul);

    ClassManualId2<int> second_int_template(4ul);
    BOOST_CHECK_EQUAL(second_int_template.GetUniqueId(), 4ul);

    ClassManualId2<double> double_template(10ul);
    BOOST_CHECK_EQUAL(int_template.GetUniqueId(), 10ul);

    ClassManualId2<double> second_double_template(4ul);
    BOOST_CHECK_EQUAL(second_double_template.GetUniqueId(), 4ul);
}

BOOST_AUTO_TEST_CASE(automatic_id)
{
    ClassAutomaticId first_object;
    BOOST_CHECK_EQUAL(first_object.GetUniqueId(), 0ul);

    ClassAutomaticId second_object;
    BOOST_CHECK_EQUAL(second_object.GetUniqueId(), 1ul);
}


BOOST_AUTO_TEST_CASE(no_id)
{
    ClassNoIdAllowed first_object;
    BOOST_CHECK_EQUAL(first_object.GetUniqueId(), NumericNS::UninitializedIndex<std::size_t>());

    ClassNoIdAllowed second_object;
    BOOST_CHECK_EQUAL(second_object.GetUniqueId(), NumericNS::UninitializedIndex<std::size_t>());

    ClassNoIdAllowed third_object(4ul);
    BOOST_CHECK_EQUAL(third_object.GetUniqueId(), 4ul);
}


BOOST_AUTO_TEST_CASE(clear)
{
    ClassManualId1 first_object(100ul);
    BOOST_CHECK_EQUAL(first_object.GetUniqueId(), 100ul);

    PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/unused-variable.hpp"

    BOOST_CHECK_THROW(auto redundant_object = ClassManualId1(100ul),
                      Internal::ExceptionsNS::UniqueId::AlreadyExistingId);

    PRAGMA_DIAGNOSTIC(pop)

    // > *** MoReFEM Doxygen end of group *** //
    ///@} // \addtogroup TestGroup
    // *** MoReFEM Doxygen end of group *** < //

    ClassManualId1::ClearUniqueIdList();

    ClassManualId1 second_object(100ul); // no throw as content was cleared.
    BOOST_CHECK_EQUAL(second_object.GetUniqueId(), 100ul);

    BOOST_CHECK_EQUAL(first_object.GetUniqueId(), second_object.GetUniqueId());
    // < To show TestNS::ClearSingletons<time_manager_type>::Do() should not be used
    // < lightly: if ill-used our unique ids become not so unique...
}


BOOST_FIXTURE_TEST_CASE(generate_unique_id, TestNS::FixtureNS::TestEnvironment)
{
    decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::CreateOrGetInstance();

    ::MoReFEM::FilesystemNS::File mesh_file{ "${MOREFEM_ROOT}/Data/Mesh/elasticity_Nx1_Ny1_force_label.mesh" };

    mesh_manager.Create(MeshNS::unique_id{ 1ul }, mesh_file, 2ul, ::MoReFEM::MeshNS::Format::Medit, 1.);

    const auto generated_id = mesh_manager.GenerateUniqueId();

    BOOST_CHECK_EQUAL(generated_id, MeshNS::unique_id{ 2ul }); // +1 after last one found in the list

    BOOST_CHECK_EQUAL(mesh_manager.GenerateUniqueId(),
                      MeshNS::unique_id{ 2ul }); // A call to GenerateUniqueId() don't add the result in the
    // list; this is done only if a new mesh is created using this result.

    mesh_manager.Create(generated_id, mesh_file, 2ul, ::MoReFEM::MeshNS::Format::Medit, 1.);

    BOOST_CHECK_EQUAL(mesh_manager.GenerateUniqueId(), MeshNS::unique_id{ 3ul });
}


BOOST_AUTO_TEST_CASE(equivalent_namespace_path)
{
    Foo::Bar::ClassWithinNamespaces first_object(3ul);

    BOOST_CHECK_EQUAL(first_object.GetUniqueId(), 3ul);

    PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/unused-variable.hpp"

    BOOST_CHECK_THROW(auto redundant_object = ::Foo::Bar::ClassWithinNamespaces(3ul),
                      Internal::ExceptionsNS::UniqueId::AlreadyExistingId);

    using namespace Foo;

    BOOST_CHECK_THROW(auto redundant_object = Bar::ClassWithinNamespaces(3ul),
                      Internal::ExceptionsNS::UniqueId::AlreadyExistingId);

    PRAGMA_DIAGNOSTIC(pop)

    // > *** MoReFEM Doxygen end of group *** //
    ///@} // \addtogroup TestGroup
    // *** MoReFEM Doxygen end of group *** < //
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
