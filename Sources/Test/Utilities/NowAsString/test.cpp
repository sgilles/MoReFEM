// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <chrono>
#include <iostream>
#include <string>
#include <thread>

#define BOOST_TEST_MODULE now_as_string
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Datetime/Now.hpp"
#include "Utilities/Environment/Environment.hpp"
#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "Test/Tools/Fixture/TestEnvironment.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    using fixture = TestNS::FixtureNS::TestEnvironment;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(same_on_all_processors, fixture)
{
    decltype(auto) mpi = GetMpi();

    for (auto i = 0ul; i < 10; ++i) // to ensure several values are tested (one per second).
    {
        auto now_as_string = Utilities::Now(mpi);

        auto hash_value =
            std::hash<std::string>{}(now_as_string); // easier to compare with mpi than the string directly...

        auto vector_result = mpi.CollectFromEachProcessor(hash_value);

        BOOST_CHECK(std::all_of(vector_result.cbegin(),
                                vector_result.cend(),
                                [hash_value](const auto value)
                                {
                                    return value == hash_value;
                                }));
        std::this_thread::sleep_for(std::chrono::milliseconds(300));
    }
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
