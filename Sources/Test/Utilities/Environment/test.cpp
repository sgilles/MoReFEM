// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#define BOOST_TEST_MODULE environment
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"

#include "Utilities/Environment/Environment.hpp"


using namespace MoReFEM;


namespace // anonymous
{


    struct Fixture
    {
        Fixture();

        Utilities::Environment& environment;

        static std::string unix_user;
    };


    Fixture::Fixture() : environment(Utilities::Environment::CreateOrGetInstance())
    { }


    std::string Fixture::unix_user = std::getenv("USER");
    // < I assume here USER is rather universal and should be defined in
    // all Unix environments.


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_SUITE(get_environment_variable, Fixture)

BOOST_AUTO_TEST_CASE(unix_user_as_string)
{
    BOOST_CHECK(environment.DoExist(std::string("USER")));
    BOOST_CHECK(environment.GetEnvironmentVariable(std::string("USER")) == unix_user);
}

BOOST_AUTO_TEST_CASE(unix_user_as_char_array)
{
    BOOST_CHECK(environment.DoExist("USER"));
    BOOST_CHECK(environment.GetEnvironmentVariable("USER") == unix_user);
}

BOOST_AUTO_TEST_CASE(unexisting_variable)
{
    BOOST_CHECK(environment.DoExist("FHLKJHFFLKJ") == false);
    BOOST_CHECK_THROW(environment.GetEnvironmentVariable("FHLKJHFFLKJ"), std::exception);
}


BOOST_AUTO_TEST_SUITE_END()


BOOST_FIXTURE_TEST_CASE(substitute_values, Fixture)
{
    std::string line = "/Volumes/Blah/${USER}";
    BOOST_CHECK(environment.SubstituteValues(line) == "/Volumes/Blah/" + unix_user);

    std::filesystem::path path{ "/Volumes/Blah/${USER}" };
    BOOST_CHECK(environment.SubstituteValuesInPath(path) == std::filesystem::path("/Volumes/Blah/" + unix_user));
}


BOOST_FIXTURE_TEST_CASE(internal_storage, Fixture)
{
    BOOST_CHECK(environment.DoExist("ASDFGHJK") == false);

    /* BOOST_CHECK_NO_THROW */ (environment.SetEnvironmentVariable(std::make_pair("ASDFGHJK", "42 ")));

    BOOST_CHECK(environment.DoExist("ASDFGHJK") == true);

    BOOST_CHECK(environment.GetEnvironmentVariable("ASDFGHJK") == "42 ");

    BOOST_CHECK_THROW(environment.SetEnvironmentVariable(std::make_pair("ASDFGHJK", "47 ")), std::exception);

    BOOST_CHECK_THROW(environment.SetEnvironmentVariable(std::make_pair("USER", "42")), std::exception);
}


PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //
