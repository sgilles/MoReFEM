// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#define BOOST_TEST_MODULE solid_no_kappa_mu_c
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"


#include <cstdlib>

#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "Test/Tools/EmptyModelSettings.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;


namespace MoReFEM
{


    struct TestHelper
    {
        TestHelper()
        {
            static bool first = true;

            if (first)
            {
                first = false;
                DomainManager::CreateOrGetInstance().Create(
                    DomainNS::unique_id{ 1 }, { MeshNS::unique_id{ 1ul } }, { 2 }, {}, {});
            }
        }
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    constexpr double volumic_mass = 10.4;

    constexpr double hyperelastic_bulk = 1750000.;

    constexpr double poisson_ratio = .3;

    constexpr double young_modulus = 21.e5;

    constexpr double lame_lambda = 1211538.4615384615;

    constexpr double lame_mu = 807692.3076923076;

    constexpr double viscosity = 8.;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"


BOOST_FIXTURE_TEST_CASE(no_kappa_mu_C, TestNS::FixtureNS::TestEnvironment)
{
    // clang-format off
    using input_data_tuple =
        std::tuple
        <
            InputDataNS::Solid::VolumicMass,
            InputDataNS::Solid::PlaneStressStrain,
            InputDataNS::Solid::HyperelasticBulk,
            InputDataNS::Solid::PoissonRatio,
            InputDataNS::Solid::YoungModulus,
            InputDataNS::Solid::LameLambda,
            InputDataNS::Solid::LameMu,
            InputDataNS::Solid::Viscosity,
            InputDataNS::Result
        >;
    // clang-format on

    using input_data_type = InputData<input_data_tuple>;

    FilesystemNS::File lua_file{ std::filesystem::path{ "${MOREFEM_ROOT}/Sources/Test/ParameterInstances/Solid/"
                                                        "demo_no_kappa_mu_C.lua" } };

    using time_manager_type = ::MoReFEM::TimeManagerNS::Instance::None;

    MoReFEMData<MoReFEM::TestNS::EmptyModelSettings, input_data_type, time_manager_type, program_type::test>
        morefem_data{ std::move(lua_file) };

    std::filesystem::path path{ "${MOREFEM_ROOT}/Data/Mesh/one_triangle.mesh" };
    FilesystemNS::File mesh_file{ std::move(path) };

    Internal::MeshNS::MeshManager::CreateOrGetInstance().Create(
        MeshNS::unique_id{ 1ul }, mesh_file, 2, MeshNS::Format::Medit, 1.);

    TestHelper test;

    QuadratureRulePerTopology quad_rule_per_topology(3, 3);

    Solid<TimeManager<::MoReFEM::TimeManagerNS::Policy::Static>> solid(
        morefem_data, DomainManager::GetInstance().GetDomain(DomainNS::unique_id{ 1 }), quad_rule_per_topology);

    BOOST_CHECK(NumericNS::AreEqual(solid.GetVolumicMass().GetConstantValue(), volumic_mass));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetHyperelasticBulk().GetConstantValue(), hyperelastic_bulk));
    BOOST_CHECK_THROW(solid.GetKappa1(), std::exception);

    BOOST_CHECK_THROW(solid.GetKappa2(), std::exception);
    BOOST_CHECK(NumericNS::AreEqual(solid.GetLameLambda().GetConstantValue(), lame_lambda));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetLameMu().GetConstantValue(), lame_mu));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetViscosity().GetConstantValue(), viscosity));
    BOOST_CHECK_THROW(solid.GetMu1(), std::exception);
    BOOST_CHECK_THROW(solid.GetMu2(), std::exception);
    BOOST_CHECK_THROW(solid.GetC0(), std::exception);
    BOOST_CHECK_THROW(solid.GetC1(), std::exception);
    BOOST_CHECK_THROW(solid.GetC2(), std::exception);
    BOOST_CHECK_THROW(solid.GetC3(), std::exception);
    BOOST_CHECK(NumericNS::AreEqual(solid.GetPoissonRatio().GetConstantValue(), poisson_ratio));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetYoungModulus().GetConstantValue(), young_modulus));
}

PRAGMA_DIAGNOSTIC(pop)

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
