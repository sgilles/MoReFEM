// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup TestGroup
 * \addtogroup TestGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstdlib>

#define BOOST_TEST_MODULE solid_lame_ignored
#include "ThirdParty/IncludeWithoutWarning/Boost/Test.hpp"


#include "Utilities/Exceptions/PrintAndAbort.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Core/MoReFEMData/MoReFEMData.hpp"
#include "Core/TimeManager/Policy/Evolution/Static.hpp"
#include "Core/TimeManager/TimeManager.hpp"

#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Mesh/Internal/MeshManager.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"

#include "Test/Tools/EmptyModelSettings.hpp"
#include "Test/Tools/Fixture/TestEnvironment.hpp"

using namespace MoReFEM;


namespace MoReFEM
{


    struct TestHelper
    {
        TestHelper()
        {
            static bool first = true;

            if (first)
            {
                first = false;
                DomainManager::CreateOrGetInstance().Create(
                    DomainNS::unique_id{ 1 }, { MeshNS::unique_id{ 1 } }, { 2 }, {}, {});
            }
        }
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup TestGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    constexpr double volumic_mass = 10.4;

    constexpr double hyperelastic_bulk = 1750000.;

    constexpr double kappa1 = 500.;

    constexpr double kappa2 = 403346.1538461538;

    constexpr double poisson_ratio = .3;

    constexpr double young_modulus = 21.e5;

    constexpr double viscosity = 8.;

    constexpr double mu1 = 9.;

    constexpr double mu2 = 10.;

    constexpr double c0 = 11.;

    constexpr double c1 = 12.;

    constexpr double c2 = 13.;

    constexpr double c3 = 14.;

    constexpr double c4 = 15.;

    constexpr double c5 = 16.;


} // namespace


PRAGMA_DIAGNOSTIC(push)
#include "Utilities/Warnings/Internal/IgnoreWarning/disabled-macro-expansion.hpp"

BOOST_FIXTURE_TEST_CASE(lame_ignored, TestNS::FixtureNS::TestEnvironment)
{
    // clang-format off
    using input_data_tuple =
        std::tuple
        <
            InputDataNS::Solid,
            InputDataNS::Result
        >;
    // clang-format on


    using input_data_type = InputData<input_data_tuple>;

    FilesystemNS::File lua_file{ std::filesystem::path{ "${MOREFEM_ROOT}/Sources/Test/ParameterInstances/Solid/"
                                                        "demo_lame_ignored.lua" } };

    using time_manager_type = ::MoReFEM::TimeManagerNS::Instance::None;

    MoReFEMData<MoReFEM::TestNS::EmptyModelSettings, input_data_type, time_manager_type, program_type::test>
        morefem_data{ std::move(lua_file) };

    std::filesystem::path path{ "${MOREFEM_ROOT}/Data/Mesh/one_triangle.mesh" };
    FilesystemNS::File mesh_file{ std::move(path) };

    Internal::MeshNS::MeshManager::CreateOrGetInstance().Create(
        MeshNS::unique_id{ 1ul }, mesh_file, 2, MeshNS::Format::Medit, 1.);

    TestHelper test;

    QuadratureRulePerTopology quad_rule_per_topology(3, 3);

    Solid<TimeManager<::MoReFEM::TimeManagerNS::Policy::Static>> solid(
        morefem_data, DomainManager::GetInstance().GetDomain(DomainNS::unique_id{ 1 }), quad_rule_per_topology);

    BOOST_CHECK_THROW(solid.GetLameLambda(), std::exception);
    BOOST_CHECK_THROW(solid.GetLameMu(), std::exception);
    BOOST_CHECK(NumericNS::AreEqual(solid.GetVolumicMass().GetConstantValue(), volumic_mass));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetHyperelasticBulk().GetConstantValue(), hyperelastic_bulk));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetKappa1().GetConstantValue(), kappa1));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetKappa2().GetConstantValue(), kappa2));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetViscosity().GetConstantValue(), viscosity));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetMu1().GetConstantValue(), mu1));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetMu2().GetConstantValue(), mu2));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetC0().GetConstantValue(), c0));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetC1().GetConstantValue(), c1));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetC2().GetConstantValue(), c2));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetC3().GetConstantValue(), c3));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetC4().GetConstantValue(), c4));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetC5().GetConstantValue(), c5));

    BOOST_CHECK(NumericNS::AreEqual(solid.GetPoissonRatio().GetConstantValue(), poisson_ratio));
    BOOST_CHECK(NumericNS::AreEqual(solid.GetYoungModulus().GetConstantValue(), young_modulus));
}


PRAGMA_DIAGNOSTIC(pop)
