# Command line options

Checks the arguments of the command line options are properly interpreted, and that additional arguments may be added.


# InputData

Generic checks about the content of the input file.


# InputParameter

Similar to InputData, but more focused on the Parameter description in the input file. This test was introduced while developing #1302.


# InitialCondition

Very similar interface than InputParameter for initial conditions.