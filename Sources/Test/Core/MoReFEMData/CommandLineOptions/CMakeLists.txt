add_executable(TestMoReFEMDataCommandLineOptions)

target_sources(TestMoReFEMDataCommandLineOptions
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/demo_morefem_data.lua
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test_command_line_options.cpp        
)

target_link_libraries(TestMoReFEMDataCommandLineOptions
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>
                      ${MOREFEM_BASIC_TEST_TOOLS})
                      
add_test(MoReFEMDataCommandLineOptions
         TestMoReFEMDataCommandLineOptions
         --
         ${MOREFEM_ROOT}
         ${MOREFEM_TEST_OUTPUT_DIR})

set_tests_properties(MoReFEMDataCommandLineOptions PROPERTIES TIMEOUT 20)

morefem_organize_IDE(TestMoReFEMDataCommandLineOptions Test/Core/MoReFEMData Test/Core/MoReFEMData/CommandLineOptions)
