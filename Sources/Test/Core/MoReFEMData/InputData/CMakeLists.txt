add_executable(TestMoReFEMDataInputData)

target_sources(TestMoReFEMDataInputData
    PUBLIC
        ${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
        ${CMAKE_CURRENT_LIST_DIR}/demo.lua
    PRIVATE
        ${CMAKE_CURRENT_LIST_DIR}/test.cpp
        ${CMAKE_CURRENT_LIST_DIR}/InputData.hpp
        ${CMAKE_CURRENT_LIST_DIR}/ModelSettings.cpp
)
                
target_link_libraries(TestMoReFEMDataInputData
                      ${MOREFEM_BASIC_TEST_TOOLS}
                      $<LINK_LIBRARY:WHOLE_ARCHIVE,${MOREFEM_CORE}>)

morefem_organize_IDE(TestMoReFEMDataInputData Test/Core/MoReFEMData Test/Core/MoReFEMData/InputData)

morefem_boost_test_sequential_mode(NAME MoReFEMDataInputData
                                   EXE TestMoReFEMDataInputData
                                   TIMEOUT 20
                                   LUA ${MOREFEM_ROOT}/Sources/Test/Core/MoReFEMData/InputData/demo.lua)
