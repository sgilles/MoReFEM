// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <chrono>
#include <cstddef> // IWYU pragma: keep
#include <fstream>
#include <ostream>
#include <sstream>
#include <string>      // IWYU pragma: keep
#include <type_traits> // IWYU pragma: keep
#include <utility>
// IWYU pragma: no_include <ratio>

#include "Utilities/TimeKeep/TimeKeep.hpp"


namespace MoReFEM
{


    TimeKeep::~TimeKeep() = default;


    const std::string& TimeKeep::ClassName()
    {
        static const std::string ret("TimeKeep");
        return ret;
    }


    TimeKeep::TimeKeep(std::ofstream&& stream)
    : stream_(std::move(stream)), init_time_(std::chrono::steady_clock::now()), previous_call_time_(init_time_)
    { }


#ifdef MOREFEM_EXTENDED_TIME_KEEP
    void TimeKeep::PrintTimeElapsed(const std::string& label)
    {
        auto now = std::chrono::steady_clock::now();
        auto time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(now - init_time_);
        auto time_elapsed_since_previous_call =
            std::chrono::duration_cast<std::chrono::milliseconds>(now - previous_call_time_);

        stream_ << label << "\n\t - Time elapsed = " << std::setw(8) << time_elapsed.count()
                << " milliseconds since beginning and " << std::setw(8) << time_elapsed_since_previous_call.count()
                << " milliseconds since previous call." << std::endl;

        previous_call_time_ = now;
    }
#endif // MOREFEM_EXTENDED_TIME_KEEP


    std::string TimeKeep::TimeElapsedSinceBeginning() const
    {
        auto now = std::chrono::steady_clock::now();
        auto time_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(now - init_time_);

        auto const time = static_cast<std::size_t>(time_elapsed.count());

        std::size_t seconds = 0;
        std::size_t minutes = 0;
        std::size_t hours = 0;

        seconds = (time / 1000) % 60;
        minutes = (time / 60000) % 60;
        hours = (time / 3600000) % 24;

        std::ostringstream out;
        out << hours << ":" << minutes << ":" << seconds;

        return out.str();
    }


    void TimeKeep::PrintEndProgram()
    {
        stream_ << "End of program after " << TimeElapsedSinceBeginning() << '\n';
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
