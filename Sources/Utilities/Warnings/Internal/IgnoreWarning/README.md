The files in this directory are to be used to ignore a warning temporarily in a push/pop block. 

This is to be used only for ThirdParty/IncludeWithoutWarning.

It is absolutely normal these header files don't have include guards: it would defeat their purpose...
