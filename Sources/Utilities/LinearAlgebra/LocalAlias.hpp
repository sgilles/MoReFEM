// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_LINEARALGEBRA_LOCALALIAS_DOT_HPP_
#define MOREFEM_UTILITIES_LINEARALGEBRA_LOCALALIAS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "ThirdParty/IncludeWithoutWarning/Xtensor/Xtensor.hpp" // IWYU pragma: export


namespace MoReFEM
{


    //! Convenient alias for the type used in MoReFEM for local vectors.
    using LocalVector = xt::xtensor<double, 1, xt::layout_type::row_major>;

    //! Convenient alias for the type used most of the time in MoReFEM for local matrices.
    using LocalMatrix = xt::xtensor<double, 2, xt::layout_type::row_major>;


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_LINEARALGEBRA_LOCALALIAS_DOT_HPP_
// *** MoReFEM end header guards *** < //
