// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_LINEARALGEBRA_SPARSEMATRIX_CSRPATTERN_DOT_HXX_
#define MOREFEM_UTILITIES_LINEARALGEBRA_SPARSEMATRIX_CSRPATTERN_DOT_HXX_
// IWYU pragma: private, include "Utilities/LinearAlgebra/SparseMatrix/CSRPattern.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Utilities/LinearAlgebra/SparseMatrix/CSRPattern.hpp"


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Utilities
{


    template<typename T>
    CSRPattern<T>::CSRPattern(std::vector<T>&& i_CSR, std::vector<T>&& j_CSR) : iCSR_(i_CSR), jCSR_(j_CSR)
    {
        static_assert(std::is_integral<T>::value == true, "Pattern is expected to handle integral values!");
        CheckConsistency();
    }

    template<typename T>
    const std::vector<T>& CSRPattern<T>::iCSR() const
    {
        return iCSR_;
    }


    template<typename T>
    const std::vector<T>& CSRPattern<T>::jCSR() const
    {
        return jCSR_;
    }


    template<typename T>
    std::vector<T>& CSRPattern<T>::NonCstiCSR()
    {
        return iCSR_;
    }


    template<typename T>
    std::vector<T>& CSRPattern<T>::NonCstjCSR()
    {
        return jCSR_;
    }


    template<typename T>
    std::size_t CSRPattern<T>::NiCSR() const
    {
        return iCSR_.size();
    }


    template<typename T>
    std::size_t CSRPattern<T>::Nrow() const
    {
        assert(!iCSR_.empty());
        return iCSR_.size() - 1;
    }


    template<typename T>
    std::size_t CSRPattern<T>::NjCSR() const
    {
        return jCSR_.size();
    }


    template<typename T>
    T CSRPattern<T>::iCSR(std::size_t i) const
    {
        assert(i < iCSR_.size());
        return iCSR_[i];
    }


    template<typename T>
    T CSRPattern<T>::jCSR(std::size_t j) const
    {
        assert(j < jCSR_.size());
        return jCSR_[j];
    }


    template<typename T>
    void CSRPattern<T>::CheckConsistency() const
    {
        assert(!iCSR_.empty());
        assert(iCSR_.front() == 0);

        if (static_cast<std::size_t>(iCSR_.back()) != jCSR_.size())
            throw MoReFEM::Exception("Invalid CSR format: last iCSR element read is not the size of jCSR.");
    }


    template<typename T>
    template<typename IntT>
    std::vector<IntT> CSRPattern<T>::NnonZeroTermsPerRow() const
    {
        const std::size_t csr_size = iCSR_.size();
        const std::size_t Nrow = csr_size - 1;
        std::vector<IntT> ret(Nrow);

        for (std::size_t i = 0; i < Nrow; ++i)
            ret[i] = iCSR_[i + 1] - iCSR_[i];

        return ret;
    }


    template<typename T>
    template<typename IntT>
    void CSRPattern<T>::NnonZeroTermsPerRow(std::function<bool(std::size_t)> is_on_local_proc,
                                            std::vector<IntT>& Ndiagonal_non_zero,
                                            std::vector<IntT>& Noff_diagonal_non_zero) const
    {
        const std::size_t Nrow = this->Nrow();
        Ndiagonal_non_zero.resize(Nrow);
        Noff_diagonal_non_zero.resize(Nrow);

        for (std::size_t i = 0; i < Nrow; ++i)
        {
            const std::size_t Nnon_zero_on_row = iCSR_[i + 1] - iCSR_[i];

            for (std::size_t j = 0; j < Nnon_zero_on_row; ++j)
            {
                const std::size_t jCSR_index = iCSR_[i] + j;

                if (is_on_local_proc(jCSR_[jCSR_index]))
                    ++Ndiagonal_non_zero[i];
                else
                    ++Noff_diagonal_non_zero[i];
            }
        }
    }


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_LINEARALGEBRA_SPARSEMATRIX_CSRPATTERN_DOT_HXX_
// *** MoReFEM end header guards *** < //
