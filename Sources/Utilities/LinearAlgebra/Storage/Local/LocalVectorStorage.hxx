// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_LOCAL_LOCALVECTORSTORAGE_DOT_HXX_
#define MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_LOCAL_LOCALVECTORSTORAGE_DOT_HXX_
// IWYU pragma: private, include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Utilities/LinearAlgebra/Storage/Local/LocalVectorStorage.hpp"


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Crtp
{


    template<class DerivedT, std::size_t NlocalVectorT, class LocalVectorT>
    constexpr std::size_t LocalVectorStorage<DerivedT, NlocalVectorT, LocalVectorT>::Size()
    {
        return NlocalVectorT;
    }


    template<class DerivedT, std::size_t NlocalVectorT, class LocalVectorT>
    void LocalVectorStorage<DerivedT, NlocalVectorT, LocalVectorT>::InitLocalVectorStorage(
        const std::array<std::size_t, NlocalVectorT>& vectors_dimension)
    {
        for (std::size_t i = 0ul; i < NlocalVectorT; ++i)
        {
            auto& vector = vector_list_[i];
            vector.resize({ static_cast<std::size_t>(vectors_dimension[i]) });
            vector.fill(0.);
        }
    }


    template<class DerivedT, std::size_t NlocalVectorT, class LocalVectorT>
    template<std::size_t IndexT>
    inline LocalVectorT& LocalVectorStorage<DerivedT, NlocalVectorT, LocalVectorT>::GetLocalVector() const
    {
        static_assert(IndexT < NlocalVectorT, "Check index is within bounds!");
        return vector_list_[IndexT];
    }


} // namespace MoReFEM::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_LOCAL_LOCALVECTORSTORAGE_DOT_HXX_
// *** MoReFEM end header guards *** < //
