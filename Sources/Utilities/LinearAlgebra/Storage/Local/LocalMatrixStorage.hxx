// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_LOCAL_LOCALMATRIXSTORAGE_DOT_HXX_
#define MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_LOCAL_LOCALMATRIXSTORAGE_DOT_HXX_
// IWYU pragma: private, include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Crtp
{


    template<class DerivedT, std::size_t NlocalMatricesT, class LocalMatrixT>
    constexpr std::size_t LocalMatrixStorage<DerivedT, NlocalMatricesT, LocalMatrixT>::Size()
    {
        return NlocalMatricesT;
    }


    template<class DerivedT, std::size_t NlocalMatricesT, class LocalMatrixT>
    void LocalMatrixStorage<DerivedT, NlocalMatricesT, LocalMatrixT>::InitLocalMatrixStorage(
        const std::array<std::pair<std::size_t, std::size_t>, NlocalMatricesT>& matrices_dimension)
    {
        for (std::size_t i = 0ul; i < NlocalMatricesT; ++i)
        {
            auto& matrix = matrix_list_[i];

            if constexpr (std::is_same<LocalMatrixT, LocalMatrix>())
            {
                matrix.resize({ static_cast<std::size_t>(matrices_dimension[i].first),
                                static_cast<std::size_t>(matrices_dimension[i].second) });
                matrix.fill(0.);
            }
        }
    }


    template<class DerivedT, std::size_t NlocalMatricesT, class LocalMatrixT>
    template<std::size_t IndexT>
    inline LocalMatrixT& LocalMatrixStorage<DerivedT, NlocalMatricesT, LocalMatrixT>::GetLocalMatrix() const
    {
        static_assert(IndexT < NlocalMatricesT, "Check index is within bounds!");
        return matrix_list_[IndexT];
    }


} // namespace MoReFEM::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_LOCAL_LOCALMATRIXSTORAGE_DOT_HXX_
// *** MoReFEM end header guards *** < //
