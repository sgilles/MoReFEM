// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_LOCAL_LOCALMATRIXSTORAGE_DOT_HPP_
#define MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_LOCAL_LOCALMATRIXSTORAGE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>
#include <cstddef> // IWYU pragma: keep

#include "Utilities/LinearAlgebra/LocalAlias.hpp"


namespace MoReFEM::Crtp
{


    /*!
     * \brief CRTP to give access to \a NlocalMatricesT local matrices.
     *
     * \tparam DerivedT Name of the base class for which the CRTP is deployed.
     * \tparam NlocalMatricesT Number of local matrices to add.
     *
     * It is advised to declare in \a DerivedT an enum class to tag the local matrices, e.g.:
     * \code
     * enum class LocalMatrixIndex : std::size_t
     * {
     *      tangent_matrix = 0,
     *      linear_part,
     *      dPhi_mult_gradient_based_block,
     *      ...
     * };
     * \endcode
     */
    template<class DerivedT, std::size_t NlocalMatricesT, class LocalMatrixT = LocalMatrix>
    class LocalMatrixStorage
    {
      public:
        //! Return the number of local matrices.
        //! \return Number of local matrices.
        static constexpr std::size_t Size();

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * Does nothing: the computation of the dimension in the constructor of DerivedT proved to be unreadable.
         * So matrix_parent::InitLocalMatrixStorage() must absolutely be called to set appropriately the dimensions.
         */
        explicit LocalMatrixStorage() = default;


      protected:
        //! Destructor.
        ~LocalMatrixStorage() = default;

        //! \copydoc doxygen_hide_copy_constructor
        LocalMatrixStorage(const LocalMatrixStorage& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        LocalMatrixStorage(LocalMatrixStorage&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        LocalMatrixStorage& operator=(const LocalMatrixStorage& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        LocalMatrixStorage& operator=(LocalMatrixStorage& rhs) = default;


        ///@}


      public:
        /*!
         * \brief This method must be called in the constructor of DerivedT, once the dimensions have been
         * computed.
         *
         * For safety reasons, matrix is filled with 0 to avoid undefined behaviour if no value is actually given.
         *
         * \param[in] matrices_dimension For each \a LocalMatrix stored this way, the pair is respectively the
         * number of rows and columns to consider.
         */
        void InitLocalMatrixStorage(
            const std::array<std::pair<std::size_t, std::size_t>, NlocalMatricesT>& matrices_dimension);

        /*!
         * \brief Access to the \a IndexT -th local matrix.
         *
         * \return Access to the \a IndexT -th local matrix.
         *
         * The idea is to use it as such:
         * \code
         * auto& local_matrix = matrix_parent::template GetLocalMatrix<0>();
         * ... (use local_matrix variable in the following)
         * \endcode
         *
         * \internal <b><tt>[internal]</tt></b> This method is const because we might want to use local matrices in
         * const methods of DerivedT; as local matrices are bound to be used within a single method they are
         * declared as mutable. \endinternal
         */
        template<std::size_t IndexT>
        LocalMatrixT& GetLocalMatrix() const;


      private:
        //! Local matrices stored.
        mutable std::array<LocalMatrixT, NlocalMatricesT> matrix_list_;
    };


} // namespace MoReFEM::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_LINEARALGEBRA_STORAGE_LOCAL_LOCALMATRIXSTORAGE_DOT_HPP_
// *** MoReFEM end header guards *** < //
