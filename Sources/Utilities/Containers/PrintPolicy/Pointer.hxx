// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_PRINTPOLICY_POINTER_DOT_HXX_
#define MOREFEM_UTILITIES_CONTAINERS_PRINTPOLICY_POINTER_DOT_HXX_
// IWYU pragma: private, include "Utilities/Containers/PrintPolicy/Pointer.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Utilities/Containers/PrintPolicy/Pointer.hpp"


namespace MoReFEM::Utilities::PrintPolicyNS
{


    template<class ElementTypeT>
    void Pointer::Do(std::ostream& stream, ElementTypeT&& ptr)
    {
        using type = std::remove_cv_t<std::remove_reference_t<ElementTypeT>>;

        static_assert(IsSharedPtr<type>() || std::is_pointer<type>() || IsUniquePtr<type>(),
                      "ptr must behaves like a pointer!");
        stream << *ptr;
    }


} // namespace MoReFEM::Utilities::PrintPolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_PRINTPOLICY_POINTER_DOT_HXX_
// *** MoReFEM end header guards *** < //
