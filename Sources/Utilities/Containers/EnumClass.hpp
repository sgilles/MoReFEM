// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_CONTAINERS_ENUMCLASS_DOT_HPP_
#define MOREFEM_UTILITIES_CONTAINERS_ENUMCLASS_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <type_traits> // IWYU pragma: keep


namespace MoReFEM
{


    /*!
     * \class doxygen_hide_enum_class_id_for_input_data_macro
     *
     * \param[in] enum_class_id Value within the enum class to designate the specific indexed section considered.
     * For instance typically when writing a model we define something like:
     * \code
     enum class UnknownIndex : std::size_t { displacement = 1ul, velocity = 2ul };
     \endcode
     * to describe all the objects (here unknowns) considered. The leaf in the input data and model settings to call the
     related
     * section is something like:
     \code
     InputDataNS::Unknown<EnumUnderlyingType(UnknownIndex::displacement)>
     \endcode
     * \a enum_class_id to be used in the macro would be here `UnknownIndex::displacement`.
     */


    /*!
     * \brief Extract the integer value behind an enum class.
     *
     * \tparam EnumT Type of the enum considered.
     *
     * \param[in] enumerator Element of the enum for which we want the underlying integer value.
     *
     * \return The underlying integer value behind an enum class member. The exact type of the integer is derived
     * with `std::underlying_type` traits.
     */
    template<typename EnumT>
    constexpr auto EnumUnderlyingType(EnumT enumerator) noexcept
    {
        return static_cast<std::underlying_type_t<EnumT>>(enumerator);
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_CONTAINERS_ENUMCLASS_DOT_HPP_
// *** MoReFEM end header guards *** < //
