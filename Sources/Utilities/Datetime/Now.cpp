// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <array>
#include <ctime>
// IWYU pragma: no_include <iosfwd>

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"
#include "Utilities/Datetime/Now.hpp"


namespace MoReFEM::Utilities
{


    std::string Now(const Wrappers::Mpi& mpi)
    {
        struct tm* timeinfo{ nullptr };

        constexpr auto buffer_size = 20UL;

        std::array<char, buffer_size> buffer{};

        time_t rawtime{};

        if (mpi.IsRootProcessor())
            time(&rawtime);

        mpi.Broadcast(rawtime);

        timeinfo = localtime(&rawtime);

        strftime(buffer.data(), buffer_size, "%Y-%m-%d_%H:%M:%S", timeinfo);

        return { buffer.data() };
    }


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
