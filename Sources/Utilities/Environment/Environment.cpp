// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstdlib>
#include <filesystem>
#include <source_location>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>

#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Environment/Environment.hpp"
#include "Utilities/String/String.hpp"


namespace MoReFEM::Utilities
{


    Environment::~Environment() = default;


    Environment::Environment()
    {
        values_.max_load_factor(Utilities::DefaultMaxLoadFactor());
    }


    const std::string& Environment::ClassName()
    {
        static const std::string ret("Environment");
        return ret;
    }


    std::string Environment::SubstituteValues(std::string string) const
    {
        auto begin_env_pos = string.find("${");
        auto end_env_pos = string.find('}');

        while (end_env_pos != std::string::npos && begin_env_pos < end_env_pos)
        {
            std::string const environment_variable_name_with_braces =
                string.substr(begin_env_pos, end_env_pos - begin_env_pos + 1UL);

            std::string const environment_variable_name =
                environment_variable_name_with_braces.substr(2, environment_variable_name_with_braces.size() - 3UL);

            auto environment_variable_value = GetEnvironmentVariable(environment_variable_name);

            Utilities::String::Replace(environment_variable_name_with_braces, environment_variable_value, string);

            // Look out for other environment variables to substitute.
            begin_env_pos = string.find("${");
            end_env_pos = string.find('}');
        }

        return string;
    }


    std::filesystem::path Environment::SubstituteValuesInPath(const std::filesystem::path& path) const
    {
        const auto& string = path.native();
        return std::filesystem::path{ SubstituteValues(string) };
    }


    void Environment::SetEnvironmentVariable(std::pair<std::string, std::string> variable,
                                             const std::source_location location)
    {
        const auto& key = variable.first;

        if (DoExist(key))
        {
            std::ostringstream oconv;
            oconv << "The environment variable " << key << " is already defined ";

            if (std::getenv(key.c_str()) != nullptr)
                oconv << " in your shell.";
            else
                oconv << " in MoReFEM internals. Make sure to set it only once";

            throw Exception(oconv.str(), location);
        }

        const auto check = values_.insert(std::move(variable));
        assert(check.second);
        static_cast<void>(check);
    }


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
