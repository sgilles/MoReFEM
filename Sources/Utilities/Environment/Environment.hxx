// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_ENVIRONMENT_ENVIRONMENT_DOT_HXX_
#define MOREFEM_UTILITIES_ENVIRONMENT_ENVIRONMENT_DOT_HXX_
// IWYU pragma: private, include "Utilities/Environment/Environment.hpp"
// *** MoReFEM header guards *** < //


#include <cstdlib>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/String/Traits.hpp"


namespace MoReFEM::Utilities
{


    template<class T>
    std::string Environment::GetEnvironmentVariable(T&& variable, const std::source_location location) const
    {
        static_assert(Utilities::String::IsString<T, Utilities::String::CharAllowed::no>::value);

        // First check internally.
        const auto it = values_.find(variable);

        if (it != values_.cend())
            return it->second;

        // If not found, see if shell defines it.
        char* value = nullptr;

        if constexpr (std::is_same<std::decay_t<T>, std::string>())
            value = std::getenv(variable.c_str());
        else
            value = std::getenv(variable);

        if (value == nullptr)
        {
            std::ostringstream oconv;
            oconv << "Environment variable " << variable << " is not defined!";
            throw Exception(oconv.str(), location);
        }

        return std::string(value);
    }


    template<class T>
    inline bool Environment::DoExist(T&& variable) const
    {
        static_assert(Utilities::String::IsString<T, Utilities::String::CharAllowed::no>::value);

        const auto it = values_.find(variable);

        if (it != values_.cend())
            return true;

        if constexpr (std::is_same<std::decay_t<T>, std::string>())
            return std::getenv(variable.c_str()) != nullptr;
        else
            return std::getenv(variable) != nullptr;
    }


} // namespace MoReFEM::Utilities


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_ENVIRONMENT_ENVIRONMENT_DOT_HXX_
// *** MoReFEM end header guards *** < //
