// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <source_location>
#include <sstream>
#include <string> // IWYU pragma: keep

#include "Utilities/Exceptions/Factory.hpp"


namespace // anonymous
{


    // Forward declarations here; definitions are at the end of the file
    std::string UnableToRegisterMsg(const std::string& object_name, const std::string& factory_content);


    std::string UnregisteredNameMsg(const std::string& object_name, const std::string& factory_content);


} // namespace


namespace MoReFEM::ExceptionNS::Factory
{


    Exception::Exception(const std::string& msg, const std::source_location location)
    : MoReFEM::Exception(msg, location)
    { }


    Exception::~Exception() = default;


    UnableToRegister::UnableToRegister(const std::string& object_name,
                                       const std::string& factory_content,
                                       const std::source_location location)
    : Exception(UnableToRegisterMsg(object_name, factory_content), location)
    { }


    UnableToRegister::~UnableToRegister() = default;


    UnregisteredName::~UnregisteredName() = default;


    UnregisteredName::UnregisteredName(const std::string& object_name,
                                       const std::string& factory_content,
                                       const std::source_location location)
    : Exception(UnregisteredNameMsg(object_name, factory_content), location)
    { }


} // namespace MoReFEM::ExceptionNS::Factory


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    // Definitions of functions defined at the beginning of the file
    std::string UnableToRegisterMsg(const std::string& object_name, const std::string& factory_content)
    {
        std::ostringstream oconv;
        oconv << "Unable to register " << factory_content << " named " << object_name
              << "; in all likelihood either another " << factory_content
              << " was already registered with that name or the pointer function passed to create the "
              << factory_content << " was invalid.";
        return oconv.str();
    }


    std::string UnregisteredNameMsg(const std::string& object_name, const std::string& factory_content)
    {
        std::ostringstream oconv;
        oconv << "Trying to create " << factory_content << " which identifier is '" << object_name
              << "'; no such "
                 "identifier has been registered in the "
              << factory_content << " factory.";

        return oconv.str();
    }


} // namespace
