// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <sstream>

#include "Utilities/Exceptions/GracefulExit.hpp"


namespace MoReFEM::ExceptionNS
{


    GracefulExit::GracefulExit(const std::source_location location)
    {
        std::ostringstream oconv;
        oconv << "A graceful exit of the program was required at file " << location.file_name() << " and line "
              << location.line() << ". The program will therefore end and return EXIT_SUCCESS.";

        what_message_ = oconv.str();
    }


    GracefulExit::~GracefulExit() = default;


    const char* GracefulExit::what() const noexcept
    {
        return what_message_.c_str();
    }


} // namespace MoReFEM::ExceptionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
