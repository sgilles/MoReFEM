// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_EXCEPTIONS_PRINTANDABORT_DOT_HPP_
#define MOREFEM_UTILITIES_EXCEPTIONS_PRINTANDABORT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers { class Mpi; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ExceptionNS
{


    /*!
     * \brief Print the exception in the main and abort the mpi processes.
     *
     * \copydetails doxygen_hide_mpi_param Used here to give away the rank for which the
     * exception was thrown.
     * \param[in] exception_message Explanation message encapsulated within the exception. Usually the output
     * of \a what() method for exceptions that derive from std::exception, but some exceptions from third-party
     * libraries do not and define their own method for that (currently no such library is in use within MoReFEM).
     */
    void PrintAndAbort(const Wrappers::Mpi& mpi, const std::string& exception_message);


} // namespace MoReFEM::ExceptionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_EXCEPTIONS_PRINTANDABORT_DOT_HPP_
// *** MoReFEM end header guards *** < //
