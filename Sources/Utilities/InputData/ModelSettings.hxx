// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_MODELSETTINGS_DOT_HXX_
#define MOREFEM_UTILITIES_INPUTDATA_MODELSETTINGS_DOT_HXX_
// IWYU pragma: private, include "Utilities/InputData/ModelSettings.hpp"
// *** MoReFEM header guards *** < //


#include <algorithm>
#include <cstddef> // IWYU pragma: keep

#include "Utilities/InputData/Exceptions/InputData.hpp"


namespace MoReFEM
{


    template<Concept::Tuple TupleT>
    ModelSettings<TupleT>::~ModelSettings() = default;


    template<Concept::Tuple TupleT>
    constexpr std::size_t ModelSettings<TupleT>::Size()
    {
        return std::tuple_size<TupleT>::value;
    }


    template<Concept::Tuple TupleT>
    ModelSettings<TupleT>::ModelSettings()
    { }


    template<Concept::Tuple TupleT>
    template<Advanced::Concept::InputDataNS::LeafType LeafT>
    void ModelSettings<TupleT>::Add(typename LeafT::return_type&& value)
    {
        {
            decltype(auto) identifier = LeafT::GetIdentifier();
            decltype(auto) list_identifiers_properly_set = GetNonCstListIdentifiersProperlySet();
            const auto end = list_identifiers_properly_set.cend();

            if (std::find(list_identifiers_properly_set.cbegin(), end, identifier) != end)
                throw InputDataNS::ExceptionNS::ValueCantBeSetTwice(identifier);

            list_identifiers_properly_set.push_back(identifier);
        }

        decltype(auto) leaf_optional =
            tuple_iteration::template ExtractModifyableLeafHandler<LeafT>(parent::GetNonCstTuple());

        if (!leaf_optional.has_value())
            throw InputDataNS::ExceptionNS::NoEntryInModelSettings(LeafT::GetIdentifier());

        leaf_optional.value().get().SetValue(std::move(value));
    }


    template<Concept::Tuple TupleT>
    inline const std::vector<std::string>& ModelSettings<TupleT>::GetListIdentifiersProperlySet() const
    {
        return list_identifiers_properly_set_;
    }


    template<Concept::Tuple TupleT>
    inline std::vector<std::string>& ModelSettings<TupleT>::GetNonCstListIdentifiersProperlySet()
    {
        return const_cast<std::vector<std::string>&>(GetListIdentifiersProperlySet());
    }


    template<Concept::Tuple TupleT>
    void ModelSettings<TupleT>::CheckTupleCompletelyFilled() const
    {
        decltype(auto) list_identifiers_properly_set = GetListIdentifiersProperlySet();

        if (list_identifiers_properly_set.size() != parent::Nleaves())
        {
            // We may refine a bit here to obtain the list of tmissing entries, not only their size!
            assert(parent::Nleaves() > list_identifiers_properly_set.size()
                   && "Should never happen! Please submit "
                      "a bug issue or contact one of the library developer.");

            auto copy_list_identifiers_properly_set = list_identifiers_properly_set;
            std::sort(copy_list_identifiers_properly_set.begin(), copy_list_identifiers_properly_set.end());

            decltype(auto) tuple_keys = parent::ExtractKeys();

            std::vector<std::string> missing_entries;

            auto end_tuple_keys = tuple_keys.cend();
            auto end_copy_list_identifiers_properly_set = copy_list_identifiers_properly_set.cend();

            std::set_difference(tuple_keys.cbegin(),
                                end_tuple_keys,
                                copy_list_identifiers_properly_set.cbegin(),
                                end_copy_list_identifiers_properly_set,
                                std::back_inserter(missing_entries));

            throw InputDataNS::ExceptionNS::ModelSettingsNotCompletelyFilled(missing_entries);
        }
    }


    template<::MoReFEM::Concept::Tuple TupleT>
    std::unordered_set<std::string> ModelSettings<TupleT>::ExtractIndexedSectionNames() const
    {
        std::vector<std::string> indexed_section_list;
        tuple_iteration::ExtractIndexedSectionDescriptionKeys(indexed_section_list);

        Utilities::EliminateDuplicate(indexed_section_list);

        // The convention of a token is that its key will be SECTION_NAME.MONIKER
        // where MONIKER is something as 'felt_space_token'.
        // We therefore want here to cut all that is beyond the dot and keep only SECTION_NAME
        std::unordered_set<std::string> ret;
        ret.max_load_factor(Utilities::DefaultMaxLoadFactor());

        for (const auto& description : indexed_section_list)
        {
            auto last_dot_position = description.rfind(".");
            assert(last_dot_position != std::string::npos
                   && "As explained in the comment, tokens are expected to be inside at least "
                      "one section.");

            auto section_name = description.substr(0ul, last_dot_position);
            ret.insert(section_name);
        }

        return ret;
    }


    template<::MoReFEM::Concept::Tuple TupleT>
    template<Advanced::Concept::InputDataNS::IndexedSectionType SectionT>
    void ModelSettings<TupleT>::SetDescription(std::string&& description)
    {
        Add<typename SectionT::IndexedSectionDescription>(std::move(description));
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_MODELSETTINGS_DOT_HXX_
// *** MoReFEM end header guards *** < //
