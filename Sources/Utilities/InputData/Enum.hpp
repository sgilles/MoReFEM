// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_ENUM_DOT_HPP_
#define MOREFEM_UTILITIES_INPUTDATA_ENUM_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd>


namespace MoReFEM::InputDataNS
{


    /*!
     * \brief An enum class that will basically act as a boolean.
     *
     * InputData::Base class takes into account which input data are actually used; the
     * way to do so is to set a bit in a bitset when some methods are called. However, sometimes these
     * methods might be called to some purposes that do not truly mean the input data is used.
     *
     * For instance, if we check at the beginning of the program that several input vectors are the same
     * length it doesn't mean all of them are actually used, hence the following enum to be able to
     * specify the method not to set the bit for this specific use.
     *
     */
    enum class CountAsUsed { no, yes };


    /*!
     * \brief Enum class to indicate whether the Lua file may not match the expected content.
     *
     * When ' yes'  is chosen (only when ProgramType of your MoReFEMData is  \a program_type::update_lua_file):
     * - A field found in the file might not match an element of the input data tuple.
     * - A tuple might not be present properly in the Lua file.
     *
     * 'no' is rather obviously the go-to choice for most cases.
     *
     */
    enum class do_update_lua_file { no, yes };


    /*!
     * \class doxygen_hide_tparam_do_update_lua_file
     *
     * \tparam DoUpdateLuaFileT In the case we want to update a Lua file, we need some wiggle room (for instance we may need to load an invalid Lua file
     * if a field no longer exists, which is forbidden in a normal run). Do not choose 'yes' except if ProgramType of
     * your MoReFEMData class is \a program_type::update_lua_file.
     */


    /*!
     * \brief Whether a field found in the file but not referenced in the tuple yields an exception or not.
     *
     * Should be 'yes' most of the time; I have introduced the 'no' option for cases in which we need only
     * a handful of parameters shared by all models for post-processing purposes (namely mesh-related ones).
     */
    enum class DoTrackUnusedFields { yes, no };


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_ENUM_DOT_HPP_
// *** MoReFEM end header guards *** < //
