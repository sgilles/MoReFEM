// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_INTERNAL_TUPLEITERATION_TRAITS_TRAITS_DOT_HPP_
#define MOREFEM_UTILITIES_INPUTDATA_INTERNAL_TUPLEITERATION_TRAITS_TRAITS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <filesystem>
#include <map>
#include <string>
#include <variant>
#include <vector>

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Wrappers::Lua { template <class T> struct Function; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::InputDataNS::Traits
{


    /*!
     * \brief Class specializations used in CreateDefaultInputFile() to write the expected
     * format for a given input data.
     */
    template<class return_type>
    struct Format
    {
        //! Print the expected format associated to \a return_type.
        //! \param[in] indent_comment Unused for most specializations, but required to make the ones
        //! that span several lines good-looking.
        static std::string Print(const std::string& indent_comment);
    };


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // Specializations.
    // ============================


    template<>
    struct Format<std::string>
    {
        static std::string Print(const std::string& indent_comment);
    };


    template<>
    struct Format<std::filesystem::path>
    {
        static std::string Print(const std::string& indent_comment);
    };


    template<class T>
    struct Format<std::vector<T>>
    {
        static std::string Print(const std::string& indent_comment);
    };


    template<>
    struct Format<std::vector<std::string>>
    {
        static std::string Print(const std::string& indent_comment);
    };


    template<class T>
    struct Format<::MoReFEM::Wrappers::Lua::Function<T>>
    {
        static std::string Print(const std::string& indent_comment, bool none_desc = true);
    };


    template<>
    struct Format<bool>
    {
        static std::string Print(const std::string& indent_comment);
    };


    template<>
    struct Format<std::vector<bool>>
    {
        static std::string Print(const std::string& indent_comment);
    };


    template<class T, class U>
    struct Format<std::map<T, U>>
    {
        static std::string Print(const std::string& indent_comment);
    };


    template<class... Args>
    struct Format<std::variant<Args...>>
    {
        static std::string Print(const std::string& indent_comment);
    };


    // ============================
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


} // namespace MoReFEM::Internal::InputDataNS::Traits


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/InputData/Internal/TupleIteration/Traits/Traits.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_INTERNAL_TUPLEITERATION_TRAITS_TRAITS_DOT_HPP_
// *** MoReFEM end header guards *** < //
