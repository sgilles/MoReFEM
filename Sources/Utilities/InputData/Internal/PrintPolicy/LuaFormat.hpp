// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_INTERNAL_PRINTPOLICY_LUAFORMAT_DOT_HPP_
#define MOREFEM_UTILITIES_INPUTDATA_INTERNAL_PRINTPOLICY_LUAFORMAT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <filesystem>
#include <iomanip>
#include <sstream>
#include <type_traits> // IWYU pragma: keep
#include <variant>
#include <vector>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/PrintPolicy/Associative.hpp"
#include "Utilities/Miscellaneous.hpp" // IWYU pragma: export
#include "Utilities/Type/PrintTypeName.hpp"

#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"


namespace MoReFEM::Internal::PrintPolicyNS
{


    /*!
     * \brief Policy to handle the printing of input data onto a stream.
     *
     * This is implemented very similarly as the policies of PrintContainer... because I am using this very facility
     * to print content of a vector!
     *
     * This function is implemented with recursivity: it is for instance possible to print a vector of heterogeneous
     * values (in a std::variant in this case).
     *
     * The point of this policy is to allow to print in a Lua file all the different types accepted in the Lua
     * interface. The policy has been classified as 'Internal' as it is very specific and is unlikely to be of use
     * directly to a end-user, contrary to the other policies provided in Utilities/Containers/PrintPolicy directory.
     */
    struct LuaFormat
    {

      public:
        //! \copydoc doxygen_hide_print_policy_do
        template<class ElementTypeT>
        static void Do(std::ostream& stream, ElementTypeT&& element);
    };


} // namespace MoReFEM::Internal::PrintPolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

#include "Utilities/InputData/Internal/PrintPolicy/LuaFormat.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_INTERNAL_PRINTPOLICY_LUAFORMAT_DOT_HPP_
// *** MoReFEM end header guards *** < //
