// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_EXCEPTIONS_INPUTDATA_DOT_HXX_
#define MOREFEM_UTILITIES_INPUTDATA_EXCEPTIONS_INPUTDATA_DOT_HXX_
// IWYU pragma: private, include "Utilities/InputData/Exceptions/InputData.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Utilities/InputData/Exceptions/InputData.hpp"


#include <source_location>
#include <sstream>
#include <string>


namespace MoReFEM::InputDataNS::ExceptionNS
{

    /*!
     * \brief Prepare the error message for MismatchedVector.
     *
     * \tparam SubTupleT The list of input data objects which vector should have had the same size.
     */
    template<class SubTupleT>
    std::string MismatchedVectorSizeMsg();


    template<class SubTupleT>
    MismatchedVectorSize<SubTupleT>::MismatchedVectorSize(const std::source_location location)
    : Exception(MismatchedVectorSizeMsg<SubTupleT>(), location)
    { }


    template<class SubTupleT>
    MismatchedVectorSize<SubTupleT>::~MismatchedVectorSize() = default;


    template<class SubTupleT>
    std::string MismatchedVectorSizeMsg()
    {
        std::ostringstream oconv;
        oconv << "Some input data were expected to be vectors of the same size, "
                 "but that is not the case:\n";

        return oconv.str();
    }


} // namespace MoReFEM::InputDataNS::ExceptionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_EXCEPTIONS_INPUTDATA_DOT_HXX_
// *** MoReFEM end header guards *** < //
