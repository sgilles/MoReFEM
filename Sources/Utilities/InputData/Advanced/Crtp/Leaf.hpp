// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_ADVANCED_CRTP_LEAF_DOT_HPP_
#define MOREFEM_UTILITIES_INPUTDATA_ADVANCED_CRTP_LEAF_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <optional>
#include <tuple>
#include <unordered_map>

#include "Utilities/Containers/Vector.hpp"
#include "Utilities/InputData/Advanced/NoEnclosingSection.hpp" // IWYU pragma: export
#include "Utilities/InputData/Enum.hpp"                        // IWYU pragma: export
#include "Utilities/InputData/Internal/ModelSettings/IndexedSectionDescription.hpp"
#include "Utilities/InputData/Internal/TupleIteration/Traits/Traits.hpp"
#include "Utilities/Miscellaneous.hpp" // IWYU pragma: export
#include "Utilities/String/String.hpp"

#include "ThirdParty/Wrappers/Lua/Function/Function.hpp"


namespace MoReFEM::Advanced::InputDataNS::Crtp
{

    /*!
     * \brief Generic class from which each Leaf should derive.
     *
     * \tparam DerivedT To enact CRTP behaviour.
     * \copydoc doxygen_hide_tparam_enclosing_section_type
     * \tparam ReturnTypeT Type that will be used to store the value read in the input
     * data file.
     */
    template<class DerivedT, class EnclosingSectionT, class ReturnTypeT>
    class Leaf
    {
      public:
        //! Type of the data read in the leaf.
        using return_type = ReturnTypeT;

        //! Type of the enclosing section.
        using enclosing_section_type = EnclosingSectionT;

        //! Specifies the nature is 'parameter' and not 'section'.
        static constexpr Nature GetNature() noexcept;

        //! Helper variable to define the \a MoReFEM::Advanced::InputDataNS::Leaf concept.
        static inline constexpr bool ConceptIsLeaf = true;


        /*!
         * \return Data read in the leaf.
         *
         * Shouldn't be called anywhere but within \a Leaf class: this class gets safeguards to ensure the
         * variable was properly initialized.
         *
         * \internal <b><tt>[internal]</tt></b> There is a reason why this method is NOT called Value(): as
         * explained in the class description, the purpose of this class is to spawn a new class for each
         * input datum to consider. A user might see fit to name such an input datum class Value,
         * which would trigger compilation error as the compiler would think the constructor is called
         * instead of the present method. On the contrary, I would raise a brow if a user would want to name
         * one of its input datum GetTheValue...
         * \endinternal
         */
        typename Utilities::ConstRefOrValue<return_type>::type GetTheValue() const;

        //! Set the value.
        //! \param[in] value Value to assign.
        void SetValue(return_type value);

        //! Tells the value has been called and has therefore probably be used.
        void SetAsUsed() const noexcept;

        //! Tells whether the value has been used at least once.
        bool IsUsed() const noexcept;

        //! Induce the identifier from section and name.
        static const std::string& GetIdentifier();

        /*!
         * \brief Get the enclosing name (i.e. the sections in which the current input data is nested).
         *
         * \return The enclosing sections with the final dot, e.g. 'solid.bulk.'. If none, empty string
         * is returned.
         */
        static const std::string& GetEnclosingSection();

        /*!
         * \brief Whether a value was assigned or not
         *
         * In most case it should always return true, but sometimes when updating a Lua file we need more
         * wiggle room.
         *
         * \return True if a value is properly stored.
         */
        bool HasValue() const noexcept;

      private:
        //! Value of the data stored.
        std::optional<return_type> value_{ std::nullopt };

        //! Whether the value has been called at least once.
        mutable bool is_used_ = false;
    };


    /*!
     * \brief A very special \a Leaf which is used to mark there is an indexed section that must be considered.
     *
     * The role of this specific leaf is explained more in depth
     * [here](../../../../../Documentation/Wiki/Utilities/InputData.md).
     *
     * \internal It is named \a IndexedSectionDescription as it is the associated value of the leaf and it is more
     * straightforward this way for model developers, but it really plays a very specific role in initialisation to make
     * the derived class known properly (its working name during development was \a IndexedSectionDescription for this
     * reason but it was a bit puzzling for the standpoint of a model developer.
     *
     * \internal I probably wouldn't implement it this way if I was designing from scratch the way to implement the
     * handling of input data, but the most cost-effective to separate the data  that are modifiable by the end-user
     * from those that are not (ticket #1796) was to introduce a very specific leaf named `IndexedSectionDescription`
     * for each indexed section. Current class introduce most of the boilerplate related to this class.
     *
     * \tparam DerivedT Class upon which CRTP is applied.
     * \tparam EnclosingSectionT (Traits) class which represents the expected enclosing section for which the
     * \a IndexedSectionDescription stands. In this specific case can't obviously be `NoEnclosingSection`.
     * \tparam TagT
     *
     */
    template<class DerivedT, class EnclosingSectionT, class TagT>
    struct IndexedSectionDescription : public Leaf<DerivedT, EnclosingSectionT, std::string>, public TagT
    {
        static_assert(!std::is_same<EnclosingSectionT, NoEnclosingSection>());

        //! Marker that current `Leaf` class is actually meant to be a `IndexedSectionDescription`.
        static inline constexpr bool indexed_section_description_class = true;

        /*!
         * \brief Internal name used for the `IndexedSectionDescription` class.
         *
         * Usually a namesake method tells the name that would appear in the Lua file, but by construct
         * `IndexedSectionDescription` classes are deemed to be defined in the `ModelSettings`. It uses this naming as
         * `ModelSettings` was introduced much later and uses up as much as possible existing functionalities.
         *
         * \return A string that is intended to be unique. A very specific suffix is added to the name of the \a EnclosingSectionT
         * to mark it is a \a IndexedSectionDescription (suffix is sufficiently weird to limit the risk a developer
         * would define another leaf with the exact same name.
         */
        static std::string NameInFile();
    };


} // namespace MoReFEM::Advanced::InputDataNS::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/InputData/Advanced/Crtp/Leaf.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_ADVANCED_CRTP_LEAF_DOT_HPP_
// *** MoReFEM end header guards *** < //
