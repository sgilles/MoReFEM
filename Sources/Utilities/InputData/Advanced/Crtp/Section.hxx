// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_ADVANCED_CRTP_SECTION_DOT_HXX_
#define MOREFEM_UTILITIES_INPUTDATA_ADVANCED_CRTP_SECTION_DOT_HXX_
// IWYU pragma: private, include "Utilities/InputData/Advanced/Crtp/Section.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Utilities/InputData/Advanced/Crtp/Section.hpp"


namespace MoReFEM::Advanced::InputDataNS::Crtp
{


    template<class DerivedT, class EnclosingSectionT>
    constexpr Nature Section<DerivedT, EnclosingSectionT>::GetNature() noexcept
    {
        return Nature::section;
    }


    template<class DerivedT, class EnclosingSectionT>
    std::string Section<DerivedT, EnclosingSectionT>::GetIdentifier() noexcept
    {
        return DerivedT::GetName();
    }


    template<class DerivedT, class EnclosingSectionT>
    const std::string& Section<DerivedT, EnclosingSectionT>::GetFullName()
    {
        if constexpr (!std::is_same<EnclosingSectionT, NoEnclosingSection>())
        {
            static const std::string ret = EnclosingSectionT::GetFullName() + "." + DerivedT::GetName();
            return ret;
        } else
        {
            static const std::string ret = DerivedT::GetName();
            return ret;
        }
    }


    template<class DerivedT, class EnclosingSectionT>
    const auto& Section<DerivedT, EnclosingSectionT>::GetSectionContent() const noexcept
    {
        return static_cast<const DerivedT&>(*this).section_content_;
    }


    template<class DerivedT, class EnclosingSectionT>
    auto& Section<DerivedT, EnclosingSectionT>::GetNonCstSectionContent() noexcept
    {
        return static_cast<DerivedT&>(*this).section_content_;
    }


    template<class DerivedT, std::size_t IndexT, class TagT, class EnclosingSectionT>
    constexpr std::size_t IndexedSection<DerivedT, IndexT, TagT, EnclosingSectionT>::GetUniqueId() noexcept
    {
        return IndexT;
    }


    template<class DerivedT, std::size_t IndexT, class TagT, class EnclosingSectionT>
    std::string IndexedSection<DerivedT, IndexT, TagT, EnclosingSectionT>::GetName()
    {
        return DerivedT::BaseName() + std::to_string(IndexT);
    }


} // namespace MoReFEM::Advanced::InputDataNS::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_ADVANCED_CRTP_SECTION_DOT_HXX_
// *** MoReFEM end header guards *** < //
