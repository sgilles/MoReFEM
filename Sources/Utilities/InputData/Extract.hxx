// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_INPUTDATA_EXTRACT_DOT_HXX_
#define MOREFEM_UTILITIES_INPUTDATA_EXTRACT_DOT_HXX_
// IWYU pragma: private, include "Utilities/InputData/Extract.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "Utilities/InputData/Internal/TupleIteration/TupleIteration.hpp"


namespace MoReFEM::InputDataNS
{

    // clang-format off
    template
    <
        ::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafPathT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    decltype(auto) ExtractLeaf(const ModelSettingsT& model_settings, const InputDataT& input_data)
    {
        using model_settings_tuple_iteration =
            Internal::InputDataNS::TupleIteration<typename ModelSettingsT::underlying_tuple_type, 0ul>;
        using input_data_tuple_iteration =
            Internal::InputDataNS::TupleIteration<typename InputDataT::underlying_tuple_type, 0ul>;

        constexpr auto is_in_model_settings = model_settings_tuple_iteration::template Find<LeafPathT>();
        constexpr auto is_in_input_data = input_data_tuple_iteration::template Find<LeafPathT>();

        static_assert((is_in_model_settings && !is_in_input_data) || (!is_in_model_settings && is_in_input_data),
                      "Field must be (exclusively) either in model settings or in input data.");

        if constexpr (is_in_model_settings)
            return ::MoReFEM::Internal::InputDataNS::ExtractLeaf<LeafPathT>::Value(model_settings);
        else
            return ::MoReFEM::Internal::InputDataNS::ExtractLeaf<LeafPathT>::Value(input_data);
    }


    template<::MoReFEM::Advanced::Concept::InputDataNS::LeafType LeafNameT,
             ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
             ::MoReFEM::Concept::InputDataType InputDataT,
             SubstituteEnvironmentVariables DoSubstituteEnvironmentVariablesT>
    // clang-format on
    decltype(auto) ExtractLeafAsPath(const ModelSettingsT& model_settings, const InputDataT& input_data)
    {

        auto as_string = ExtractLeaf<LeafNameT>(model_settings, input_data);

        if constexpr (DoSubstituteEnvironmentVariablesT == SubstituteEnvironmentVariables::yes)
        {
            decltype(auto) environment = Utilities::Environment::CreateOrGetInstance();
            as_string = environment.SubstituteValues(as_string);
        }

        return std::filesystem::path{ as_string };
    }


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_INPUTDATA_EXTRACT_DOT_HXX_
// *** MoReFEM end header guards *** < //
