// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef>

#include "Utilities/Numeric/Numeric.hpp"

#include "ThirdParty/Wrappers/Xtensor/Functions.hpp"
#include "Utilities/LinearAlgebra/LocalAlias.hpp"


namespace MoReFEM::NumericNS
{


    namespace // anonymous
    {


        /*!
         * \brief Iterate through both underlying 1D arrays of Xtensor and evaluate whether both are equal or not.
         *
         *  \param[in] size Size of the underlying array. It is assumed the validity for both linear algebra was checked
         * prior to the function call. \tparam DataT The type of the underlying data as returned by\a data() method
         * (e.g. LocalMatrix::data()).
         */
        template<class DataT>
        bool AreUnderlyingXtensorDataEqual(std::size_t size, const DataT& lhs, const DataT& rhs, double epsilon);


    } // namespace


    bool AreEqual(const LocalVector& lhs, const LocalVector& rhs, double epsilon)
    {
        if (lhs.shape() != rhs.shape())
            throw ExceptionNS::NumericNS::InconsistentVectors(lhs, rhs);

        const auto size = lhs.shape(0);

        return AreUnderlyingXtensorDataEqual(size, lhs.data(), rhs.data(), epsilon);
    }


    bool IsZero(const LocalVector& vector, double epsilon) noexcept
    {
        return Wrappers::Xtensor::IsZeroVector(vector, epsilon);
    }


    bool AreEqual(const LocalMatrix& lhs, const LocalMatrix& rhs, double epsilon)
    {
        if (lhs.shape() != rhs.shape())
            throw ExceptionNS::NumericNS::InconsistentMatrices(lhs, rhs);

        assert(lhs.size() == rhs.size());

        return AreUnderlyingXtensorDataEqual(lhs.size(), lhs.data(), rhs.data(), epsilon);
    }


    bool IsZero(const LocalMatrix& matrix, double epsilon) noexcept
    {
        return Wrappers::Xtensor::IsZeroMatrix(matrix, epsilon);
    }


    namespace // anonymous
    {


        template<class DataT>
        bool AreUnderlyingXtensorDataEqual(std::size_t size, const DataT& lhs, const DataT& rhs, double epsilon)
        {
            for (auto i = 0UL; i < size; ++i)
            {
                if (!AreEqual(lhs[i], rhs[i], epsilon))
                    return false;
            }

            return true;
        }

    } // namespace

} // namespace MoReFEM::NumericNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //
