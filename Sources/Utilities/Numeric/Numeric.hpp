// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_NUMERIC_NUMERIC_DOT_HPP_
#define MOREFEM_UTILITIES_NUMERIC_NUMERIC_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <type_traits> // IWYU pragma: keep

#include "Utilities/LinearAlgebra/LocalAlias.hpp"
#include "Utilities/Numeric/Exceptions/Numeric.hpp" // IWYU pragma: export


namespace MoReFEM::NumericNS
{


    /*!
     * \brief When an  index is not known at object constructor, better assign a controlled dumb value
     * than not knowing what the compiler decided.
     *
     * In most cases (typically for instance the initialization of a class attribute) it is a good idea to
     * use it along with decltype:
     *
     * \code
     * MyClass::MyClass()
     * : index_(MoReFEM::NumericNS::UninitializedIndex<decltype(index_)>())
     * { }
     *
     * \endcode
     *
     * Doing so makes the code robust to a change of type: if some day index_ becomes a std::size_t rather than
     * an int the default value will keep being the greatest value it can reach.
     *
     * \return Known value when an index is not properly initialized (currently it is the highest possible value for
     * \a T).
     */
    template<class T>
    constexpr T UninitializedIndex() noexcept;


    /*!
     * \class doxygen_hide_pow_like_operator
     *
     * \note std::pow() is more versatile but far less efficient than a simple call to operator*. Defining
     * a function is not just laziness: if value is for instance computed on the spot it is done only once;
     * consider for instance Operator(f(5)): f(5) is computed only once!
     *
     * \tparam T Type of the value; it is assumed operator* is defined for this type.
     *
     * \param[in] value Value upon which the operation is performed.
     *
     */

    /*!
     * \brief Computes the square of a value.
     *
     * \copydoc doxygen_hide_pow_like_operator
     *
     * \return value * value.
     */
    template<class T>
    constexpr T Square(T value) noexcept;


    /*!
     * \brief Computes the cube of a value.
     *
     * \copydoc doxygen_hide_pow_like_operator
     *
     * \return value * value * value.
     */
    template<class T>
    constexpr T Cube(T value) noexcept;


    /*!
     * \brief Computes the 4th power of a value.
     *
     * \copydoc doxygen_hide_pow_like_operator
     *
     * \return value * value * value * value.
     */
    template<class T>
    constexpr T PowerFour(T value) noexcept;


    /*!
     * \brief Returns value if it is positive or 0 otherwise.
     *
     * \tparam T Type of the value considered; it must both be initialized with 0 and comes with an operator<.
     *
     * \param[in] value Value upon which the operation is performed.
     *
     * \return value if value > 0, 0 otherwise.
     */
    template<class T>
    constexpr T AbsPlus(T value) noexcept;


    /*!
     * \brief Returns -1, 0 or 1 depending on the sign of tge value.
     *
     * \param[in] value Value which sign is evaluated.
     *
     * \tparam T Type of the value; 0, 1 and -1 must be convertible to this type and operator< must be defined for
     * it.
     *
     * \return -1 if negative, 1 if positive, 0 if null (as determined by the \a IsZero() function).
     */
    template<class T>
    constexpr T Sign(T value) noexcept;


    /*!
     * \brief A special version of \a Sign which considers 0 as positive.
     *
     * \param[in] value Value which sign is evaluated.
     *
     * \tparam T Type of the value; 0, 1 and -1 must be convertible to this type and operator< must be defined for
     * it.
     *
     * This is directly lifted from HeartLab code; TrueSign is like the sign except zero is counted as positive.
     *
     * \return -1 if negative, 1 if positive or zero (as determined by the \a IsZero function).
     */
    template<class T>
    constexpr T TrueSign(T value) noexcept;

    /*!
     * \brief Heaviside function.
     *
     * \param[in] value Value which sign is evaluated.
     *
     * \tparam T A floating-point type.
     *
     * \return 0 if negative, 1 if positive, 0.5 if null (as determined by the \a IsZero function).
     */
    template<class T>
    constexpr T Heaviside(T value) noexcept;


    /*!
     * \brief Returns a floating-point value that is small enough for most purposes.
     *
     * Currently set to 1.e-15 is used here.
     * \note We use a function for this so that there are no magic number in the code and the same value
     * is used everywhere.
     *
     * \tparam T Type for which the value is required; a double must be convertible into this type.
     *
     * \return 1.e-15.
     */
    template<class T>
    constexpr std::enable_if_t<std::is_floating_point<T>::value, T> DefaultEpsilon() noexcept;


    /*!
     * \brief Check whether a value is close enough to zero to be able to be considered as 0.
     *
     * \tparam T Floating point type considered (float, double or long double).
     *
     * \param[in] value Value that is tested.
     * \param[in] epsilon Epsilon used for the comparison. A default value is provided; but the parameter
     * is there if you want to play with it.
     *
     * \return True if \a value might be considered as null or not.
     */
    template<class T>
    std::enable_if_t<std::is_floating_point<T>::value, bool> IsZero(T value, T epsilon = DefaultEpsilon<T>()) noexcept;

    /*!
     * \brief Check whether a value is 0.
     *
     * \tparam T Integral type considered
     *
     * \param[in] value Value that is tested.
     *
     * \internal For an integral type such a function is trivial but it might be useful from a metaprogramming standpoint.
     *
     * \return True if \a value might be considered as null or not.
     */
    template<class T>
    std::enable_if_t<std::is_integral<T>::value, bool> IsZero(T value) noexcept;

    /*!
     * \brief Check whether a \a LocalVector or a \a LocalMatrix is filled only by values close enough to zero.
     *
     * There are in fact dedicated functions to handle the separate cases of \a Localvector and \a LocalMatrix, but
     we wanted to
     * enable the users to make a call such as:
     * \code
     NumericNS::IsZero<LocalVector>(vector);
     \endcode
     *  that might be useful in metaprogramming context. Of course current function just calls directly under the
     hood the other one.
     *
     * \param[in] value Vector or matrix that is tested.
     * \param[in] epsilon Epsilon used for the comparison. A default value is provided; but the parameter
     * is there if you want to play with it.
     */
    // clang-format off
        template<class T>
        std::enable_if_t
        <
            std::is_same<T, LocalMatrix>() || std::is_same<T, LocalVector>(),
            bool
        >
    // clang-format on
    IsZero(T value, double epsilon = DefaultEpsilon<double>()) noexcept;


    /*!
     * \brief Check whether a \a LocalVector is filled with all its values close enough to zero to be able to be considered as 0.
     *
     * \param[in] vector Vector that is tested.
     * \param[in] epsilon Epsilon used for the comparison. A default value is provided; but the parameter
     * is there if you want to play with it.
     *
     * \return True if \a vector is filled only with values close enough to zero.
     */
    bool IsZero(const LocalVector& vector, double epsilon = DefaultEpsilon<double>()) noexcept;

    /*!
     * \brief Check whether a \a LocalMatrix is filled with all its values close enough to zero to be able to be considered as 0.
     *
     * \param[in] matrix Matrix that is tested.
     * \param[in] epsilon Epsilon used for the comparison. A default value is provided; but the parameter
     * is there if you want to play with it.
     *
     * \return True if \a matrix is filled only with values close enough to zero.
     */
    bool IsZero(const LocalMatrix& matrix, double epsilon = DefaultEpsilon<double>()) noexcept;


    /*!
     * \brief Check whether a value is close enough to another.
     *
     * \tparam T Floating point type considered (float, double or long double).
     *
     * \param[in] lhs Lhs value.
     * \param[in] rhs Rhs value.
     * \param[in] epsilon Epsilon used for the comparison. A default value is provided; but the parameter
     * is there if you want to play with it.
     *
     * \return True if \a lhs and \a rhs are deemed to be close enough to be considered equal.
     */
    template<class T>
    std::enable_if_t<std::is_floating_point<T>::value, bool>
    AreEqual(T lhs, T rhs, T epsilon = DefaultEpsilon<T>()) noexcept;


    /*!
     * \brief Check whether two \a LocalVector are filled with all their values close enough to one another.
     *
     * If the vectors aren't the same size, an exception is thrown.
     *
     * \param[in] lhs First vector to be tested.
     * \param[in] rhs Second vector to be tested.
     * \param[in] epsilon Epsilon used for the comparison. A default value is provided; but the parameter
     * is there if you want to play with it.
     *
     * \return True if both \a lhs and \a rhs vectors are the same size and filled only with values close enough to one another.
     */
    bool AreEqual(const LocalVector& lhs, const LocalVector& rhs, double epsilon = DefaultEpsilon<double>());


    /*!
     * \brief Check whether two \a LocalMatrix are filled with all their values close enough to one another.
     *
     * If the matrices aren't the same dimension, an exception is thrown.
     *
     * \param[in] lhs First matrix to be tested.
     * \param[in] rhs Second matrix to be tested.
     * \param[in] epsilon Epsilon used for the comparison. A default value is provided; but the parameter
     * is there if you want to play with it.
     *
     * \return True if both \a lhs and \a rhs matrices are the same size and filled only with values close enough to one another.
     */
    bool AreEqual(const LocalMatrix& lhs, const LocalMatrix& rhs, double epsilon = DefaultEpsilon<double>());

    /*!
     * \brief Check whether two \a LocalVector or \a LocalMatrix are close enough.
     *
     * \param[in] lhs First linear algebra to be tested.
     * \param[in] rhs Second linear algebra  to be tested. Must be the same shape as the first one (or an exception
     is thrown).
     * \param[in] epsilon Epsilon used for the comparison (absolute tolerance). A default value is provided; but the
     parameter
     * is there if you want to play with it.
     *
     * There are in fact dedicated functions to handle the separate cases of \a LocalVector and \a LocalMatrix, but
     we wanted to
     * enable the users to make a call such as:
     * \code
     NumericNS::AreEqual<LocalVector>(vector1, vector2);
     \endcode
     *  that might be useful in metaprogramming context. Of course current function just calls directly under the
     hood the other one.
     *
     */
    // clang-format off
        template<class T>
        std::enable_if_t
        <
            std::is_same<T, LocalMatrix>() || std::is_same<T, LocalVector>(),
            bool
        >
    // clang-format on
    AreEqual(const T& lhs, const T& rhs, double epsilon = DefaultEpsilon<double>());


    /*!
     * \brief A wrapper over `std::pow` which throws whenever an invalid value is computed.
     *
     * `std::pow` handles its errors by:
     * - setting the return value as `nan`
     * - setting a global variable with the information about the state
     *
     * \tparam T Any floating-point type (less powerful than `std::pow` on this regard but at the moment no need to consider other types).
     *
     * \param[in] base Base value
     * \param[in] exponent Exponent
     * \copydoc doxygen_hide_source_location
     *
     * \return The computed value (that shouldn't be `nan`: if the computation is invalid an exception should be thrown.
     */
    template<class T>
    T Pow(T base, T exponent, const std::source_location location = std::source_location::current());


} // namespace MoReFEM::NumericNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/Numeric/Numeric.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_NUMERIC_NUMERIC_DOT_HPP_
// *** MoReFEM end header guards *** < //
