// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_NUMERIC_EXCEPTIONS_NUMERIC_DOT_HPP_
#define MOREFEM_UTILITIES_NUMERIC_EXCEPTIONS_NUMERIC_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export
#include "Utilities/LinearAlgebra/LocalAlias.hpp"


namespace MoReFEM::ExceptionNS::NumericNS
{


    /*!
     * \brief Thrown when \a NumericNS::AreEqual is used on two vectors that don't share the same shape.
     *
     */
    class InconsistentVectors final : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] lhs First vector considered.
         * \param[in] rhs Second vector considered.
         * \copydoc doxygen_hide_source_location

         */
        explicit InconsistentVectors(const LocalVector& lhs,
                                     const LocalVector& rhs,
                                     const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InconsistentVectors() override;

        //! \copydoc doxygen_hide_copy_constructor
        InconsistentVectors(const InconsistentVectors& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InconsistentVectors(InconsistentVectors&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InconsistentVectors& operator=(const InconsistentVectors& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InconsistentVectors& operator=(InconsistentVectors&& rhs) = default;
    };


    /*!
     * \brief Thrown when \a NumericNS::AreEqual is used on two matrices that don't share the same shape.
     *
     */
    class InconsistentMatrices final : public ::MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor.
         *
         * \param[in] lhs First matrix considered.
         * \param[in] rhs Second matrix considered.
         * \copydoc doxygen_hide_source_location
         */
        explicit InconsistentMatrices(const LocalMatrix& lhs,
                                      const LocalMatrix& rhs,
                                      const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~InconsistentMatrices() override;

        //! \copydoc doxygen_hide_copy_constructor
        InconsistentMatrices(const InconsistentMatrices& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        InconsistentMatrices(InconsistentMatrices&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        InconsistentMatrices& operator=(const InconsistentMatrices& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        InconsistentMatrices& operator=(InconsistentMatrices&& rhs) = default;
    };


} // namespace MoReFEM::ExceptionNS::NumericNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_NUMERIC_EXCEPTIONS_NUMERIC_DOT_HPP_
// *** MoReFEM end header guards *** < //
