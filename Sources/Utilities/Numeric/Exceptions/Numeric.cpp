// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <source_location>
#include <sstream>
#include <string>
#include <string_view>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/Containers/Print.hpp"
#include "Utilities/LinearAlgebra/LocalAlias.hpp"
#include "Utilities/Numeric/Exceptions/Numeric.hpp"


namespace // anonymous
{


    // \tparam LocalLinearAlgebraT Either \a LocalVector or \a LocalMatrix.
    template<class LocalLinearAlgebraT>
    std::string InconsistentLinearAlgebraMsg(const LocalLinearAlgebraT& lhs, const LocalLinearAlgebraT& rhs);


} // namespace


namespace MoReFEM::ExceptionNS::NumericNS
{


    InconsistentVectors::~InconsistentVectors() = default;


    InconsistentVectors::InconsistentVectors(const LocalVector& lhs,
                                             const LocalVector& rhs,
                                             const std::source_location location)
    : MoReFEM::Exception(InconsistentLinearAlgebraMsg(lhs, rhs), location)
    { }


    InconsistentMatrices::~InconsistentMatrices() = default;


    InconsistentMatrices::InconsistentMatrices(const LocalMatrix& lhs,
                                               const LocalMatrix& rhs,
                                               const std::source_location location)
    : MoReFEM::Exception(InconsistentLinearAlgebraMsg(lhs, rhs), location)
    { }


} // namespace MoReFEM::ExceptionNS::NumericNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    template<class LocalLinearAlgebraT>
    std::string InconsistentLinearAlgebraMsg(const LocalLinearAlgebraT& lhs, const LocalLinearAlgebraT& rhs)
    {
        using namespace MoReFEM;
        std::ostringstream oconv;

        static_assert(std::is_same<LocalLinearAlgebraT, LocalVector>()
                      || std::is_same<LocalLinearAlgebraT, LocalMatrix>());

        oconv << "NumericNS::AreEqual() facility is attempted upon two ";
        oconv << (std::is_same<LocalVector, LocalLinearAlgebraT>() ? "vectors" : "matrices");
        oconv << " that don't share the same "
                 "shape: first one is ";
        Utilities::PrintContainer<>::Do(lhs.shape(),
                                        oconv,
                                        PrintNS::Delimiter::separator(", "),
                                        PrintNS::Delimiter::opener("["),
                                        PrintNS::Delimiter::closer("]"));
        oconv << " whereas the second is ";
        Utilities::PrintContainer<>::Do(rhs.shape(),
                                        oconv,
                                        PrintNS::Delimiter::separator(", "),
                                        PrintNS::Delimiter::opener("["),
                                        PrintNS::Delimiter::closer("]"));

        return oconv.str();
    }


} // namespace
