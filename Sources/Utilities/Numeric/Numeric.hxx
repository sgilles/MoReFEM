// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_NUMERIC_NUMERIC_DOT_HXX_
#define MOREFEM_UTILITIES_NUMERIC_NUMERIC_DOT_HXX_
// IWYU pragma: private, include "Utilities/Numeric/Numeric.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Utilities/Numeric/Numeric.hpp"


#include <cmath>
#include <limits>
#include <memory>
#include <sstream>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/Exceptions/Exception.hpp"
#include "Utilities/LinearAlgebra/LocalAlias.hpp"


namespace MoReFEM::NumericNS
{


    template<class T>
    constexpr T UninitializedIndex() noexcept
    {
        if constexpr (std::is_integral<T>())
            return std::numeric_limits<T>::max();
        else // specialization of StrongType; needs to be more explicit...
            return T(std::numeric_limits<typename T::underlying_type>::max());
    }


    template<class T>
    constexpr T Square(T value) noexcept
    {
        static_assert(std::is_arithmetic<T>());
        return value * value;
    }


    template<class T>
    constexpr T Cube(T value) noexcept
    {
        return value * value * value;
    }


    template<class T>
    constexpr T PowerFour(T value) noexcept
    {
        return value * value * value * value;
    }


    template<class T>
    constexpr T AbsPlus(T value) noexcept
    {
        constexpr T zero = static_cast<T>(0);

        if (value < zero)
            return zero;
        else
            return value;
    }


    template<class T>
    constexpr T Sign(T value) noexcept
    {
        constexpr T zero = static_cast<T>(0);
        constexpr T minus_one = static_cast<T>(-1);
        constexpr T one = static_cast<T>(1);

        if (IsZero(value))
            return zero;
        else if (value < zero)
            return minus_one;
        else
            return one;
    }


    template<class T>
    constexpr T TrueSign(T value) noexcept
    {
        constexpr T zero = static_cast<T>(0);
        constexpr T minus_one = static_cast<T>(-1);
        constexpr T one = static_cast<T>(1);

        if (value < zero)
            return minus_one;
        else
            return one;
    }


    template<class T>
    constexpr T Heaviside(T value) noexcept
    {
        static_assert(std::is_floating_point<T>(), "Heaviside doesn't make any sense for integers.");

        constexpr T zero = static_cast<T>(0);
        constexpr T one_half = static_cast<T>(1. / 2.);
        constexpr T one = static_cast<T>(1.);

        if (value < zero)
            return zero;
        else if (IsZero(value))
            return one_half;
        else
            return one;
    }


    template<class T>
    constexpr std::enable_if_t<std::is_floating_point<T>::value, T> DefaultEpsilon() noexcept
    {
        return static_cast<T>(1.e-15);
    }


    template<class T>
    std::enable_if_t<std::is_floating_point<T>::value, bool> IsZero(T value, T epsilon) noexcept
    {
        return std::fabs(value) < epsilon;
    }


    template<class T>
    std::enable_if_t<std::is_integral<T>::value, bool> IsZero(T value) noexcept
    {
        return value == static_cast<T>(0);
    }


    // clang-format off
        template<class T>
        inline std::enable_if_t
        <
            std::is_same<T, LocalMatrix>() || std::is_same<T, LocalVector>(),
            bool
        >
    // clang-format on
    IsZero(const T& object, double epsilon)
    {
        // Calls the overload which is not templated.
        return IsZero(object, epsilon);
    }


    template<class T>
    std::enable_if_t<std::is_floating_point<T>::value, bool> AreEqual(T lhs, T rhs, T epsilon) noexcept
    {
        return IsZero(lhs - rhs, epsilon);
    }


    // clang-format off
        template<class T>
        inline std::enable_if_t
        <
            std::is_same<T, LocalMatrix>() || std::is_same<T, LocalVector>(),
            bool
        >
    // clang-format on
    AreEqual(const T& lhs, const T& rhs, double epsilon)
    {
        // Calls the overload which is not templated.
        return AreEqual(lhs, rhs, epsilon);
    }


    template<class T>
    T Pow(T base, T exponent, const std::source_location location)
    {
        static_assert(std::is_floating_point<T>());

        const T ret = std::pow(base, exponent);

        if (!std::isfinite(ret))
        {
            std::ostringstream oconv;
            oconv << "Invalid std::pow operation (base = " << base << ", exponent = " << exponent << ").";
            throw MoReFEM::Exception(oconv.str(), location);
        }

        return ret;
    }


} // namespace MoReFEM::NumericNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_NUMERIC_NUMERIC_DOT_HXX_
// *** MoReFEM end header guards *** < //
