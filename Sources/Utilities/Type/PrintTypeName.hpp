// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_TYPE_PRINTTYPENAME_DOT_HPP_
#define MOREFEM_UTILITIES_TYPE_PRINTTYPENAME_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <string_view>


namespace MoReFEM
{


    /*!
     * \brief Fetch the name of the type given as template parameter.
     *
     * Adapted from https://stackoverflow.com/questions/81870/is-it-possible-to-print-a-variables-type-in-standard-c
     *
     * \tparam T Type which information is requested.
     *
     * \return Name of the type, e.g. 'double' or 'const std::string&'.
     */
    template<class T>
    constexpr std::string_view GetTypeName();


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/Type/PrintTypeName.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_TYPE_PRINTTYPENAME_DOT_HPP_
// *** MoReFEM end header guards *** < //
