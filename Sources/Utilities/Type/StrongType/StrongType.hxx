// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_TYPE_STRONGTYPE_STRONGTYPE_DOT_HXX_
#define MOREFEM_UTILITIES_TYPE_STRONGTYPE_STRONGTYPE_DOT_HXX_
// IWYU pragma: private, include "Utilities/Type/StrongType/StrongType.hpp"
// *** MoReFEM header guards *** < //


#include <vector>


namespace MoReFEM
{


    template<class T, class Parameter, template<typename> class... Skills>
    constexpr StrongType<T, Parameter, Skills...>::StrongType(T const& value) : value_(value)
    { }


    template<class T, class Parameter, template<typename> class... Skills>
    template<class T_>
    constexpr StrongType<T, Parameter, Skills...>::StrongType(T&& value,
                                                              // clang-format off
                                                    std::enable_if_t
                                                    <
                                                        !std::is_reference<T_>{},
                                                        std::nullptr_t
                                                    >)
    // clang-format on
    : value_(std::move(value))
    { }


    template<class T, class Parameter, template<typename> class... Skills>
    template<class T_>
    constexpr StrongType<T, Parameter, Skills...>::StrongType(
        std::enable_if_t<typename almost_self<T_>::enable_default_constructor{}, std::nullptr_t>)
    : value_{}
    { }


    template<class T, class Parameter, template<typename> class... Skills>
    inline T const& StrongType<T, Parameter, Skills...>::Get() const noexcept
    {
        return value_;
    }


    template<class T, class Parameter, template<typename> class... Skills>
    inline T& StrongType<T, Parameter, Skills...>::Get() noexcept
    {
        return value_;
    }


    template<class T, class Parameter, template<typename> class... Skills>
    StrongType<T, Parameter, Skills...>::operator T() const noexcept
    {
        return value_;
    }


    template<class T, class ParameterT, template<typename> class... Skills>
    std::ostream& operator<<(std::ostream& stream, const StrongType<T, ParameterT, Skills...>& object)
    {
        object.Print(stream);
        return stream;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_TYPE_STRONGTYPE_STRONGTYPE_DOT_HXX_
// *** MoReFEM end header guards *** < //
