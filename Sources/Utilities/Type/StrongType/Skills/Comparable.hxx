// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_COMPARABLE_DOT_HXX_
#define MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_COMPARABLE_DOT_HXX_
// IWYU pragma: private, include "Utilities/Type/StrongType/Skills/Comparable.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Utilities/Type/StrongType/Skills/Comparable.hpp"


namespace MoReFEM::StrongTypeNS
{


    template<class StrongTypeT>
    bool Comparable<StrongTypeT>::operator<(Comparable<StrongTypeT> const& other) const noexcept
    {
        return static_cast<StrongTypeT const&>(*this).Get() < static_cast<StrongTypeT const&>(other).Get();
    }


    template<class StrongTypeT>
    bool Comparable<StrongTypeT>::operator==(Comparable<StrongTypeT> const& other) const noexcept
    {
        return static_cast<StrongTypeT const&>(*this).Get() == static_cast<StrongTypeT const&>(other).Get();
    }


    template<class StrongTypeT>
    bool Comparable<StrongTypeT>::operator>(Comparable<StrongTypeT> const& other) const noexcept
    {
        return !operator<(other) && !operator==(other);
    }


    template<class StrongTypeT>
    bool Comparable<StrongTypeT>::operator<=(Comparable<StrongTypeT> const& other) const noexcept
    {
        return !operator>(other);
    }


    template<class StrongTypeT>
    bool Comparable<StrongTypeT>::operator>=(Comparable<StrongTypeT> const& other) const noexcept
    {
        return !operator<(other);
    }


    template<class StrongTypeT>
    bool Comparable<StrongTypeT>::operator!=(Comparable<StrongTypeT> const& other) const noexcept
    {
        return !operator==(other);
    }


} // namespace MoReFEM::StrongTypeNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_TYPE_STRONGTYPE_SKILLS_COMPARABLE_DOT_HXX_
// *** MoReFEM end header guards *** < //
