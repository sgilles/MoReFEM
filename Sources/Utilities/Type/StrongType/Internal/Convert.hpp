// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_TYPE_STRONGTYPE_INTERNAL_CONVERT_DOT_HPP_
#define MOREFEM_UTILITIES_TYPE_STRONGTYPE_INTERNAL_CONVERT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iostream>
#include <type_traits> // IWYU pragma: keep
#include <utility>
#include <vector>


namespace MoReFEM::Internal::StrongTypeNS
{


    /*!
     * \brief Facility to convert of vector with POD (plain old data) into a \a StrongType which underlying type is matching.
     *
     * Typically it is used for data from the input file: \a OptionFile uses POD types, but in the rest of the
     * library we rather use strong types. This function may then be used when interpreting raw data from the input
     * file and convert them into usable types.
     *
     * \param[in] pod_vector Vector with the POD type.
     *
     * \return Vector of \a StrongTypeT with the same content as \a pod_vector plus the strong typing.
     */
    template<class StrongTypeT>
    std::vector<StrongTypeT> Convert(const std::vector<typename StrongTypeT::underlying_type>& pod_vector);


} // namespace MoReFEM::Internal::StrongTypeNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/Type/StrongType/Internal/Convert.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_TYPE_STRONGTYPE_INTERNAL_CONVERT_DOT_HPP_
// *** MoReFEM end header guards *** < //
