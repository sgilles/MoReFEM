// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup UtilitiesGroup
 * \addtogroup UtilitiesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_UTILITIES_MPI_MPI_DOT_HPP_
#define MOREFEM_UTILITIES_MPI_MPI_DOT_HPP_
// *** MoReFEM header guards *** < //


#include "ThirdParty/Wrappers/Mpi/Mpi.hpp" // IWYU pragma: export


namespace MoReFEM::Crtp
{


    /*!
     * \brief This CRTP class provides to its derived class a Mpi object and an accessor to it.
     */
    template<class DerivedT>
    class CrtpMpi
    {
      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydetails doxygen_hide_mpi_param
         */
        explicit CrtpMpi(const Wrappers::Mpi& mpi);

        //! Destructor.
        ~CrtpMpi() = default;

        //! \copydoc doxygen_hide_copy_constructor
        CrtpMpi(const CrtpMpi& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        CrtpMpi(CrtpMpi&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        CrtpMpi& operator=(const CrtpMpi& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        CrtpMpi& operator=(CrtpMpi&& rhs) = delete;

        ///@}

        //! Read-only access to underlying \a Mpi object.
        //! \return Constant accessor to underlying Mpi object.
        const Wrappers::Mpi& GetMpi() const noexcept;

      private:
        //! Mpi object.
        const Wrappers::Mpi& mpi_;
    };


} // namespace MoReFEM::Crtp


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup UtilitiesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Utilities/Mpi/Mpi.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_UTILITIES_MPI_MPI_DOT_HPP_
// *** MoReFEM end header guards *** < //
