/*!
 * \defgroup ModelGroup Model
 * 
 * \brief Module that encompass the model, which drives a MoReFEM program.
 *
 * \attention Currently model is a header-only library.
 *
 */


/// \addtogroup ModelGroup
///@{


/// \namespace MoReFEM::Internal::ModelNS
/// \brief Namespace that enclose helper classes of Model.


///@}

/*!
 * \class doxygen_hide_model_SupplHasFinishedConditions_common
 *
 * \brief Whether the model wants to add additional cases in which the Model stops (besides the reach of
 * maximum time).
 *
 */

/*!
* \class doxygen_hide_model_SupplHasFinishedConditions
*
* \copydoc doxygen_hide_model_SupplHasFinishedConditions_common
*
* \return True if one of the additional finish condition defined specifically in current \a Model is reached.
*/

/*!
 * \class doxygen_hide_model_SupplHasFinishedConditions_always_true
 *
 * \copydoc doxygen_hide_model_SupplHasFinishedConditions_common
 *
 * \return Always true (no such additional condition in this Model).
 */


/*!
 * \class doxygen_hide_do_write_restart_data
 *
  * \brief Whether data for a possible ulterior restart mode are written on disk or not.
  *
  * Please notice this field does little more than store what was indicated in the input data file in the
  * \a InputDataNS::Restart::DoWriteRestartData field; it is in each model that support restarts that you
  * may provide the logic to write or not the data, e.g. with something like (in which \a MyModel derives
  * from current class):
  *
  * \code
  void MyModel::WriteRestartData() const
  {
      decltype(auto) time_manager = parent::GetTimeManager();
      decltype(auto) god_of_dof = parent::GetGodOfDof();

      if (DoWriteRestartData())
      {
         Advanced::RestartNS::WriteDataForRestart(time_manager, god_of_dof, "velocity", GetVectorCurrentVelocity());
         Advanced::RestartNS::WriteDataForRestart(time_manager, god_of_dof, "displacement", GetVectorCurrentDisplacement());
      }
  }
  \endcode
  */
