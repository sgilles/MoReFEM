// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ModelGroup
 * \addtogroup ModelGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <memory>

#include "Utilities/InputData/Internal/Write/Enum.hpp"

#include "Model/Main/Internal/CommandLineArgumentsForUpdateLuaFile.hpp"


namespace MoReFEM::Internal::ModelNS
{


    void UpdateLuaFileCLI::Add(TCLAP::CmdLine& command)
    {
        skip_comment_arg_ =
            std::make_unique<TCLAP::SwitchArg>("",
                                               "skip-comments",
                                               "If this flag is set, skip all the comments and just let "
                                               "sections and leafs content.",
                                               command,
                                               false);
    };


    Internal::InputDataNS::verbosity UpdateLuaFileCLI::DoSkipComments() const
    {
        assert(!(!skip_comment_arg_));
        return skip_comment_arg_->getValue() ? InputDataNS::verbosity::compact : InputDataNS::verbosity::verbose;
    }


} // namespace MoReFEM::Internal::ModelNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelGroup
// *** MoReFEM Doxygen end of group *** < //
