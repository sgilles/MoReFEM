// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ModelGroup
 * \addtogroup ModelGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_MODEL_MAIN_MAINENSIGHTOUTPUT_DOT_HXX_
#define MOREFEM_MODEL_MAIN_MAINENSIGHTOUTPUT_DOT_HXX_
// IWYU pragma: private, include "Model/Main/MainEnsightOutput.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef>   // IWYU pragma: keep // IWYU pragma: export
#include <exception> // IWYU pragma: export

#include "Utilities/Exceptions/Advanced/Assertion/Assertion.hpp" // IWYU pragma: export


namespace MoReFEM::ModelNS
{


    template<class ModelT>
    int MainEnsightOutput(int argc,
                          char** argv,
                          MeshNS::unique_id mesh_index,
                          const std::vector<NumberingSubsetNS::unique_id>& numbering_subset_id_list,
                          const std::vector<std::string>& unknown_list)

    {
        assert(numbering_subset_id_list.size() == unknown_list.size());

        try
        {
            // We're not using the model input_data_type directly as we want to tolerate missing fields in the Lua file
            // (e.g. if a new one was introduced in the tuple after the first version of the Lua file was generated).
            using input_data_type = typename ModelT::morefem_data_type::input_data_type;

            // clang-format off
            using morefem_data_type =
                MoReFEMData
                <
                    typename  ModelT::morefem_data_type::model_settings_type,
                    input_data_type,
                    typename  ModelT::morefem_data_type::time_manager_type,
                    program_type::post_processing,
                    InputDataNS::DoTrackUnusedFields::no
                >;
            // clang-format on

            morefem_data_type morefem_data(argc, argv);

            decltype(auto) input_data = morefem_data.GetInputData();
            decltype(auto) mpi = morefem_data.GetMpi();
            decltype(auto) model_settings = morefem_data.GetModelSettings();

            try
            {


                using Result = InputDataNS::Result;
                decltype(auto) result_directory_path =
                    ::MoReFEM::InputDataNS::ExtractLeafAsPath<Result::OutputDirectory>(morefem_data);

                FilesystemNS::Directory result_directory(mpi, result_directory_path, FilesystemNS::behaviour::read);
                {
                    auto& manager = Internal::MeshNS::MeshManager::CreateOrGetInstance();
                    Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
                }

                decltype(auto) mesh_manager = Internal::MeshNS::MeshManager::GetInstance();

                const Mesh& mesh = mesh_manager.GetMesh(mesh_index);

                {
                    auto& manager = Internal::NumberingSubsetNS::NumberingSubsetManager::CreateOrGetInstance();
                    Advanced::SetFromInputDataAndModelSettings<>(model_settings, input_data, manager);
                }

                {
                    PostProcessingNS::OutputFormat::Ensight6 ensight_output(
                        result_directory, unknown_list, numbering_subset_id_list, mesh);
                }

                std::cout << "End of Post-Processing." << std::endl;
                std::cout << TimeKeep::GetInstance().TimeElapsedSinceBeginning() << std::endl;
            }
            catch (const ExceptionNS::GracefulExit&)
            {
                return EXIT_SUCCESS;
            }
            catch (const std::exception& e)
            {
                ExceptionNS::PrintAndAbort(mpi, e.what());
            }
        }
        catch (const ExceptionNS::GracefulExit&)
        {
            return EXIT_SUCCESS;
        }
        catch (const std::exception& e)
        {
            std::ostringstream oconv;
            oconv << "Exception caught from MoReFEMData: " << e.what() << std::endl;
            std::cout << oconv.str();
            return EXIT_FAILURE;
        }
        catch (const TCLAP::ExitException& e)
        {
            std::ostringstream oconv;
            oconv << "TCLAP Exception caught from MoReFEMData - status " << e.getExitStatus() << std::endl;

            std::cout << oconv.str();
            return EXIT_FAILURE;
        }
#ifndef NDEBUG
        catch (const Advanced::Assertion& e)
        {
            std::ostringstream oconv;
            oconv << "MoReFEM Assertion caught: " << e.what() << std::endl;

            std::cout << oconv.str();
            return EXIT_FAILURE;
        }
#endif // NDEBUG

        return EXIT_SUCCESS;
    }


} // namespace MoReFEM::ModelNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ModelGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_MODEL_MAIN_MAINENSIGHTOUTPUT_DOT_HXX_
// *** MoReFEM end header guards *** < //
