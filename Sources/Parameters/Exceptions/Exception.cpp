// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef>
#include <sstream>
#include <string_view>

#include "Utilities/Containers/Print.hpp"

#include "Geometry/Domain/UniqueId.hpp"

#include "Parameters/Exceptions/Exception.hpp"


namespace // anonymous
{


    std::string InconsistentVectorDimensionMsg(std::string_view parameter_name,
                                               std::size_t expected_dimension,
                                               std::size_t Nelts_read);

    std::string InconsistentVectorDimensionForDomainMsg(std::string_view parameter_name,
                                                        ::MoReFEM::DomainNS::unique_id domain_id,
                                                        std::size_t expected_dimension,
                                                        std::size_t Nelts_read);

    std::string NoLuaFunctionForVectorialOrMatricialMsg(std::string_view parameter_name);

    std::string InvalidMatrixDimensionInOptionFileMsg(std::string_view parameter_name,
                                                      const std::vector<std::size_t>& matrix_dimension);

    std::string InconsistentMatrixDimensionMsg(std::string_view parameter_name,
                                               const std::vector<std::size_t>& expected_dimension,
                                               std::size_t Nelts_read);

    std::string InconsistentMatrixDimensionForDomainMsg(std::string_view parameter_name,
                                                        ::MoReFEM::DomainNS::unique_id domain_id,
                                                        const std::vector<std::size_t>& expected_dimension,
                                                        std::size_t Nelts_read);

    std::string PartialIgnoredCompoundMsg(std::string_view parameter_name, const std::vector<std::string>& nature_list);

} // namespace


namespace MoReFEM::ExceptionNS::ParameterNS
{


    InconsistentVectorDimension::~InconsistentVectorDimension() = default;


    InconsistentVectorDimension::InconsistentVectorDimension(std::string_view parameter_name,
                                                             std::size_t expected_dimension,
                                                             std::size_t Nelts_read,
                                                             const std::source_location location)
    : MoReFEM::Exception(InconsistentVectorDimensionMsg(parameter_name, expected_dimension, Nelts_read), location)
    { }


    InconsistentVectorDimensionForDomain::~InconsistentVectorDimensionForDomain() = default;


    InconsistentVectorDimensionForDomain::InconsistentVectorDimensionForDomain(std::string_view parameter_name,
                                                                               ::MoReFEM::DomainNS::unique_id domain_id,
                                                                               std::size_t expected_dimension,
                                                                               std::size_t Nelts_read,
                                                                               const std::source_location location)
    : MoReFEM::Exception(
          InconsistentVectorDimensionForDomainMsg(parameter_name, domain_id, expected_dimension, Nelts_read),
          location)
    { }


    NoLuaFunctionForVectorialOrMatricial::~NoLuaFunctionForVectorialOrMatricial() = default;

    NoLuaFunctionForVectorialOrMatricial::NoLuaFunctionForVectorialOrMatricial(std::string_view parameter_name,
                                                                               const std::source_location location)
    : MoReFEM::Exception(NoLuaFunctionForVectorialOrMatricialMsg(parameter_name), location)
    { }


    InvalidMatrixDimensionInOptionFile::~InvalidMatrixDimensionInOptionFile() = default;

    InvalidMatrixDimensionInOptionFile::InvalidMatrixDimensionInOptionFile(
        std::string_view parameter_name,
        const std::vector<std::size_t>& matrix_dimension,
        const std::source_location location)
    : MoReFEM::Exception(InvalidMatrixDimensionInOptionFileMsg(parameter_name, matrix_dimension), location)
    { }


    InconsistentMatrixDimension::~InconsistentMatrixDimension() = default;


    InconsistentMatrixDimension::InconsistentMatrixDimension(std::string_view parameter_name,
                                                             const std::vector<std::size_t>& expected_dimension,
                                                             std::size_t Nelts_read,
                                                             const std::source_location location)
    : MoReFEM::Exception(InconsistentMatrixDimensionMsg(parameter_name, expected_dimension, Nelts_read), location)
    { }


    InconsistentMatrixDimensionForDomain::~InconsistentMatrixDimensionForDomain() = default;


    InconsistentMatrixDimensionForDomain::InconsistentMatrixDimensionForDomain(
        std::string_view parameter_name,
        ::MoReFEM::DomainNS::unique_id domain_id,
        const std::vector<std::size_t>& expected_dimension,
        std::size_t Nelts_read,
        const std::source_location location)
    : MoReFEM::Exception(
          InconsistentMatrixDimensionForDomainMsg(parameter_name, domain_id, expected_dimension, Nelts_read),
          location)
    { }


    PartialIgnoredCompound::~PartialIgnoredCompound() = default;


    PartialIgnoredCompound::PartialIgnoredCompound(std::string_view parameter_name,
                                                   const std::vector<std::string>& nature_list,
                                                   const std::source_location location)
    : MoReFEM::Exception(PartialIgnoredCompoundMsg(parameter_name, nature_list), location)
    { }


} // namespace MoReFEM::ExceptionNS::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    std::string InconsistentVectorDimensionMsg(std::string_view parameter_name,
                                               std::size_t expected_dimension,
                                               std::size_t Nelts_read)
    {
        std::ostringstream oconv;

        oconv << "In the Lua input data file, the dimensions given for Parameter " << parameter_name
              << " doesn't match its content: given dimension is " << expected_dimension << " whereas " << Nelts_read
              << " were actually read.";

        return oconv.str();
    }


    std::string InconsistentVectorDimensionForDomainMsg(std::string_view parameter_name,
                                                        ::MoReFEM::DomainNS::unique_id domain_id,
                                                        std::size_t expected_dimension,
                                                        std::size_t Nelts_read)
    {
        std::ostringstream oconv;

        oconv << "In the Lua input data file, the dimensions given for Parameter " << parameter_name << " in  domain "
              << domain_id << " doesn't match its content: given dimension is " << expected_dimension << " whereas "
              << Nelts_read << " were actually read.";

        return oconv.str();
    }


    std::string NoLuaFunctionForVectorialOrMatricialMsg(std::string_view parameter_name)
    {
        std::ostringstream oconv;

        oconv << "Parameter " << parameter_name
              << " is not scalar and can't be initialized with a Lua "
                 "function. The reason is that vectorial and matricial Parameters are built with Xtensor arrays "
                 "as internal storage, and each component is therefore a floating point, not a function that may "
                 "be evaluated on the fly. You may try to use a ThreeDimensionalCoumpoundParameter for "
                 "each of its component if you really need the functionality.";

        return oconv.str();
    }


    std::string InvalidMatrixDimensionInOptionFileMsg(std::string_view parameter_name,
                                                      const std::vector<std::size_t>& matrix_dimension)
    {
        std::ostringstream oconv;

        oconv << "Invalid dimension given in the option file for parameter " << parameter_name
              << ": the field 'matrix_dimension' was expected to give away exactly two values (for "
                 "respectively the number of rows and the number of columns) and the given value was ";
        MoReFEM::Utilities::PrintContainer<>::Do(matrix_dimension, oconv);
        oconv << ".";

        return oconv.str();
    }


    std::string InconsistentMatrixDimensionMsg(std::string_view parameter_name,
                                               const std::vector<std::size_t>& expected_dimension,
                                               std::size_t Nelts_read)
    {
        std::ostringstream oconv;
        using namespace MoReFEM;

        assert(expected_dimension.size() == 2ul && "Should have been checked beforehand");

        oconv << "In the Lua input data file, the dimensions given for Parameter " << parameter_name
              << " doesn't match its content: given dimension is ";

        Utilities::PrintContainer<>::Do(expected_dimension,
                                        oconv,
                                        PrintNS::Delimiter::separator(", "),
                                        PrintNS::Delimiter::opener("("),
                                        PrintNS::Delimiter::closer(")"));

        oconv << " (meaning " << expected_dimension[0] * expected_dimension[1]
              << " values were expected) "
                 "whereas "
              << Nelts_read << " were actually read.";

        return oconv.str();
    }

    std::string InconsistentMatrixDimensionForDomainMsg(std::string_view parameter_name,
                                                        ::MoReFEM::DomainNS::unique_id domain_id,
                                                        const std::vector<std::size_t>& expected_dimension,
                                                        std::size_t Nelts_read)
    {
        std::ostringstream oconv;
        using namespace MoReFEM;

        assert(expected_dimension.size() == 2ul && "Should have been checked beforehand");

        oconv << "In the Lua input data file, the dimensions given for Parameter " << parameter_name << " for Domain "
              << domain_id << " doesn't match its content: given dimension is ";

        Utilities::PrintContainer<>::Do(expected_dimension,
                                        oconv,
                                        PrintNS::Delimiter::separator(", "),
                                        PrintNS::Delimiter::opener("("),
                                        PrintNS::Delimiter::closer(")"));

        oconv << " (meaning " << expected_dimension[0] * expected_dimension[1]
              << " values were expected) "
                 "whereas "
              << Nelts_read << " were actually read.";

        return oconv.str();
    }


    std::string PartialIgnoredCompoundMsg(std::string_view parameter_name, const std::vector<std::string>& nature_list)
    {
        std::ostringstream oconv;
        using namespace MoReFEM;

        oconv << "In the Lua input data file, Parameter " << parameter_name << " got for its nature ";

        Utilities::PrintContainer<>::Do(nature_list,
                                        oconv,
                                        PrintNS::Delimiter::separator(", "),
                                        PrintNS::Delimiter::opener("("),
                                        PrintNS::Delimiter::closer(")"));

        oconv << " which is invalid: either none or all the components must be 'ignore'.";

        return oconv.str();
    }


} // namespace
