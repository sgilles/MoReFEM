### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_PARAM}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Alias.hpp
		${CMAKE_CURRENT_LIST_DIR}/None.hpp
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
		${CMAKE_CURRENT_LIST_DIR}/TimeDependency.hpp
		${CMAKE_CURRENT_LIST_DIR}/TimeDependency.hxx
)

include(${CMAKE_CURRENT_LIST_DIR}/Internal/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Policy/SourceList.cmake)
