// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_TIMEDEPENDENCY_POLICY_FUNCTOR_DOT_HPP_
#define MOREFEM_PARAMETERS_TIMEDEPENDENCY_POLICY_FUNCTOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>


namespace MoReFEM::ParameterNS::TimeDependencyNS::PolicyNS
{


    /*!
     * \brief Policy of the time dependency based on a functor.
     *
     * \copydoc doxygen_hide_param_time_dependancy
     *
     * \tparam FunctorT A function with takes as argument a double (the time in seconds) and returns
     * the time-related factor (g(t) in the explanation above).
     *
     */
    template<class FunctorT>
    class Functor
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Functor<FunctorT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] functor Functor which will be stored inside the \a Parameter.
        explicit Functor(FunctorT&& functor);

        //! Destructor.
        ~Functor() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Functor(const Functor& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Functor(Functor&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Functor& operator=(const Functor& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Functor& operator=(Functor&& rhs) = delete;

        ///@}

      public:
        //! Compute the adequate time factor for \a time.
        //! \param[in] time Time for which the factor is sought.
        double GetCurrentTimeFactor(double time) const;

      private:
        /*!
         * \brief Functor which returns the time dependent factor.
         *
         * It takes a double (the time step) and returns the value of the time-dependent factor; a possible
         * prototype is:
         * \code
         * [](double time) -> double
         * \endcode
         *
         */
        const FunctorT functor_;
    };


} // namespace MoReFEM::ParameterNS::TimeDependencyNS::PolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Parameters/TimeDependency/Policy/Functor.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_TIMEDEPENDENCY_POLICY_FUNCTOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
