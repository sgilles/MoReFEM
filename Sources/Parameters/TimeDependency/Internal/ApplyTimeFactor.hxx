// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_TIMEDEPENDENCY_INTERNAL_APPLYTIMEFACTOR_DOT_HXX_
#define MOREFEM_PARAMETERS_TIMEDEPENDENCY_INTERNAL_APPLYTIMEFACTOR_DOT_HXX_
// IWYU pragma: private, include "Parameters/TimeDependency/Internal/ApplyTimeFactor.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Parameters/TimeDependency/Internal/ApplyTimeFactor.hpp"


namespace MoReFEM::Internal::ParameterNS::TimeDependencyNS
{


    inline void
    ApplyTimeFactor(double value_without_time_factor, double time_factor, double& value_with_time_factor) noexcept
    {
        value_with_time_factor = value_without_time_factor * time_factor;
    }


    inline void ApplyTimeFactor(const LocalMatrix& value_without_time_factor,
                                double time_factor,
                                LocalMatrix& value_with_time_factor)
    {
        xt::noalias(value_with_time_factor) = time_factor * value_without_time_factor;
    }


    inline void ApplyTimeFactor(const LocalVector& value_without_time_factor,
                                double time_factor,
                                LocalVector& value_with_time_factor)
    {
        xt::noalias(value_with_time_factor) = time_factor * value_without_time_factor;
    }


} // namespace MoReFEM::Internal::ParameterNS::TimeDependencyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_TIMEDEPENDENCY_INTERNAL_APPLYTIMEFACTOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
