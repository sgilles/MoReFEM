// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include "Core/Parameter/TypeEnum.hpp"

#include "Parameters/TimeDependency/Internal/InitStoredQuantity.hpp"


namespace MoReFEM::Internal::ParameterNS::TimeDependencyNS
{


    void InitStoredQuantity<Type::scalar>::Perform(double any_value, double& stored_value)
    {
        static_cast<void>(any_value);
        static_cast<void>(stored_value);
    }


    void InitStoredQuantity<Type::vector>::Perform(const LocalVector& any_value, LocalVector& stored_value)
    {
        stored_value.resize({ any_value.shape(0) });
    }


    void InitStoredQuantity<Type::matrix>::Perform(const LocalMatrix& any_value, LocalMatrix& stored_value)
    {
        stored_value.resize({ any_value.shape(0), any_value.shape(1) });
    }


} // namespace MoReFEM::Internal::ParameterNS::TimeDependencyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //
