// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_INTERNAL_PARAMETERATDOF_DOT_HPP_
#define MOREFEM_PARAMETERS_INTERNAL_PARAMETERATDOF_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Parameters/Internal/Alias.hpp"
#include "Parameters/Internal/ParameterInstance.hpp" // IWYU pragma: export
#include "Parameters/Policy/AtDof/AtDof.hpp"
#include "Parameters/TimeDependency/None.hpp"


namespace MoReFEM::Internal::ParameterNS
{


    /*!
     * \brief Intermediate traits class that provide the type for a parameter defined at dof.
     *
     * \copydoc doxygen_hide_parameter_at_dof_template_args
     *
     * You shouldn't have to deal with this class directly; it's just a bridge to deal with the fact
     * ParameterNS::Policy::ParameterAtDof gets an additional template parameter (namely the number of
     * \a FEltSpace).
     */
    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT = ::MoReFEM::ParameterNS::TimeDependencyNS::None,
        std::size_t NfeltSpaceT = 1ul
    >
    // clang-format on
    struct ParameterAtDofImpl
    {
      private:
        static_assert(TypeT != ParameterNS::Type::matrix, "Doesn't make sense for a matrix parameter.");

        /*!
         * \brief Alias which the proper prototype for \a ParameterInstance.
         *
         * \a ParameterNS::Policy::AtDof does not work directly, as there are 2 template arguments whereas
         * template template argument of \a ParameterInstance expects only one.
         */
        template<ParameterNS::Type TypeTT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerTT>
        using intermediate_policy_type = ::MoReFEM::ParameterNS::Policy::AtDof<TypeTT, TimeManagerTT, NfeltSpaceT>;

      public:
        //! Alias which provides the parameter type.
        using type =
            Internal::ParameterNS::ParameterInstance<TypeT, intermediate_policy_type, TimeManagerT, TimeDependencyT>;
    };


} // namespace MoReFEM::Internal::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_INTERNAL_PARAMETERATDOF_DOT_HPP_
// *** MoReFEM end header guards *** < //
