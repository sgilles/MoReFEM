// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_INTERNAL_PARAMETERINSTANCE_DOT_HXX_
#define MOREFEM_PARAMETERS_INTERNAL_PARAMETERINSTANCE_DOT_HXX_
// IWYU pragma: private, include "Parameters/Internal/ParameterInstance.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Parameters/Internal/ParameterInstance.hpp"


namespace MoReFEM::Internal::ParameterNS
{


    // clang-format off
    template
    <
        Type TypeT,
        template<Type, typename... Args> class NaturePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        typename... Args
    >
    // clang-format on
    template<class T, typename... ConstructorArgs>
    ParameterInstance<TypeT, NaturePolicyT, TimeManagerT, TimeDependencyT, Args...>::ParameterInstance(
        T&& name,
        const Domain& domain,
        ConstructorArgs&&... arguments)
    : parent(std::forward<T>(name), domain),
      nature_policy(parent::GetName(), domain, std::forward<ConstructorArgs>(arguments)...)
    {
        static_assert(std::is_convertible<self*, parent*>());
    }


    // clang-format off
    template
    <
        Type TypeT,
        template<Type, typename... Args> class NaturePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        typename... Args
    >
    // clang-format on
    inline typename ParameterInstance<TypeT, NaturePolicyT, TimeManagerT, TimeDependencyT, Args...>::return_type
    ParameterInstance<TypeT, NaturePolicyT, TimeManagerT, TimeDependencyT, Args...>::SupplGetValue(
        const local_coords_type& local_coords,
        const GeometricElt& geom_elt) const
    {
        return nature_policy::GetValueFromPolicy(local_coords, geom_elt);
    }


    // clang-format off
    template
    <
        Type TypeT,
        template<Type, typename... Args> class NaturePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        typename... Args
    >
    // clang-format on
    inline typename ParameterInstance<TypeT, NaturePolicyT, TimeManagerT, TimeDependencyT, Args...>::return_type
    ParameterInstance<TypeT, NaturePolicyT, TimeManagerT, TimeDependencyT, Args...>::SupplGetConstantValue() const
    {
        assert(nature_policy::IsConstant());
        return nature_policy::GetConstantValueFromPolicy();
    }


    // clang-format off
    template
    <
        Type TypeT,
        template<Type, typename... Args> class NaturePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        typename... Args
    >
    // clang-format on
    inline typename ParameterInstance<TypeT, NaturePolicyT, TimeManagerT, TimeDependencyT, Args...>::return_type
    ParameterInstance<TypeT, NaturePolicyT, TimeManagerT, TimeDependencyT, Args...>::SupplGetAnyValue() const
    {
        return nature_policy::GetAnyValueFromPolicy();
    }


    // clang-format off
    template
    <
        Type TypeT,
        template<Type, typename... Args> class NaturePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        typename... Args
    >
    // clang-format on
    inline bool ParameterInstance<TypeT, NaturePolicyT, TimeManagerT, TimeDependencyT, Args...>::IsConstant() const
    {
        return nature_policy::IsConstant();
    }


    // clang-format off
    template
    <
        Type TypeT,
        template<Type, typename... Args> class NaturePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        typename... Args
    >
    // clang-format on
    inline void
    ParameterInstance<TypeT, NaturePolicyT, TimeManagerT, TimeDependencyT, Args...>::SupplWrite(std::ostream& out) const
    {
        return nature_policy::WriteFromPolicy(out);
    }


    // clang-format off
    template
    <
        Type TypeT,
        template<Type, typename... Args> class NaturePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        typename... Args
    >
    // clang-format on
    inline void ParameterInstance<TypeT, NaturePolicyT, TimeManagerT, TimeDependencyT, Args...>::SupplTimeUpdate()
    {
        // Do nothing in general case.
    }


    // clang-format off
    template
    <
        Type TypeT,
        template<Type, typename... Args> class NaturePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        typename... Args
    >
    // clang-format on
    inline void
    ParameterInstance<TypeT, NaturePolicyT, TimeManagerT, TimeDependencyT, Args...>::SupplTimeUpdate(double time)
    {
        // Do nothing in general case.
        static_cast<void>(time);
    }


    // clang-format off
    template
    <
        Type TypeT,
        template<Type, typename... Args> class NaturePolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        typename... Args
    >
    // clang-format on
    inline void
    ParameterInstance<TypeT, NaturePolicyT, TimeManagerT, TimeDependencyT, Args...>::SetConstantValue(value_type value)
    {
        nature_policy::SetConstantValue(value);
    }


} // namespace MoReFEM::Internal::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_INTERNAL_PARAMETERINSTANCE_DOT_HXX_
// *** MoReFEM end header guards *** < //
