// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_CONSTANT_CONSTANT_DOT_HXX_
#define MOREFEM_PARAMETERS_POLICY_CONSTANT_CONSTANT_DOT_HXX_
// IWYU pragma: private, include "Parameters/Policy/Constant/Constant.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Parameters/Policy/Constant/Constant.hpp"


namespace MoReFEM::ParameterNS::Policy
{


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    Constant<TypeT, TimeManagerT>::Constant(const std::string&,
                                            const Domain&,
                                            typename Constant<TypeT, TimeManagerT>::parameter_storage_type value)
    : value_(value)
    { }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    [[noreturn]] typename Constant<TypeT, TimeManagerT>::return_type
    Constant<TypeT, TimeManagerT>::GetValueFromPolicy(const local_coords_type& local_coords,
                                                      const GeometricElt& geom_elt) const
    {
        static_cast<void>(local_coords);
        static_cast<void>(geom_elt);

        assert(false && "Parameter class should have guided toward GetConstantValue()!");
        exit(-1);
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline typename Constant<TypeT, TimeManagerT>::return_type
    Constant<TypeT, TimeManagerT>::GetConstantValueFromPolicy() const
    {
        return value_;
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline typename Constant<TypeT, TimeManagerT>::return_type
    Constant<TypeT, TimeManagerT>::GetAnyValueFromPolicy() const
    {
        return GetConstantValueFromPolicy();
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline constexpr bool Constant<TypeT, TimeManagerT>::IsConstant() const noexcept
    {
        return true;
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void Constant<TypeT, TimeManagerT>::WriteFromPolicy(std::ostream& out) const
    {
        out << "# Homogeneous value:" << std::endl;
        out << GetConstantValueFromPolicy() << std::endl;
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline void Constant<TypeT, TimeManagerT>::SetConstantValue(parameter_storage_type value) noexcept
    {
        value_ = value;
    }


} // namespace MoReFEM::ParameterNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_CONSTANT_CONSTANT_DOT_HXX_
// *** MoReFEM end header guards *** < //
