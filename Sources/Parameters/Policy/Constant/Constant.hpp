// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_CONSTANT_CONSTANT_DOT_HPP_
#define MOREFEM_PARAMETERS_POLICY_CONSTANT_CONSTANT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Mesh/Mesh.hpp"

#include "Core/Parameter/TypeEnum.hpp"


namespace MoReFEM::ParameterNS::Policy
{


    /*!
     * \brief Parameter policy when the parameter gets the same value everywhere.
     *
     * \copydoc doxygen_hide_parameter_type_tparam
     */
    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class Constant
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Constant<TypeT, TimeManagerT>;

        //! \copydoc doxygen_hide_parameter_local_coords_type
        using local_coords_type = LocalCoords;

      private:
        //! Alias to traits class related to TypeT.
        using traits = Internal::ParameterNS::Traits<TypeT>;

      public:
        //! Alias to the return type.
        using return_type = typename traits::return_type;

        //! Alias to the type of the value actually stored.
        using parameter_storage_type = typename traits::value_type;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \copydoc doxygen_hide_parameter_name_and_domain_arg
         * \param[in] value Numerical value of the constant Parameter.
         */
        explicit Constant(const std::string& name, const Domain& domain, parameter_storage_type value);

        //! Destructor.
        ~Constant() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Constant(const Constant& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Constant(Constant&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Constant& operator=(const Constant& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Constant& operator=(Constant&& rhs) = delete;

        ///@}

      public:
        /*!
         * \brief Enables to modify the constant value of a parameter.
         *
         * \param[in] value Value for the \a Parameter.
         */
        void SetConstantValue(parameter_storage_type value) noexcept;

      protected:
        //! Get the value.
        return_type GetConstantValueFromPolicy() const;

        //! Provided here to make the code compile, but should never be called.
        [[noreturn]] return_type GetValueFromPolicy(const LocalCoords&, const GeometricElt&) const;

        //! \copydoc doxygen_hide_parameter_suppl_get_any_value
        return_type GetAnyValueFromPolicy() const;

      protected:
        //! Whether the parameter varies spatially or not.
        constexpr bool IsConstant() const noexcept;


        //! Write the content of the parameter for which policy is used in a stream.
        //! \copydoc doxygen_hide_stream_inout
        void WriteFromPolicy(std::ostream& stream) const;

      private:
        //! Get the value of the parameter.
        parameter_storage_type value_;
    };


} // namespace MoReFEM::ParameterNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Parameters/Policy/Constant/Constant.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_CONSTANT_CONSTANT_DOT_HPP_
// *** MoReFEM end header guards *** < //
