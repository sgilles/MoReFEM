// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <sstream>
#include <string>
#include <string_view>

#include "Utilities/Containers/Print.hpp"

#include "Parameters/Policy/PiecewiseConstantByDomain/Exceptions/PiecewiseConstantByDomain.hpp"


namespace // anonymous
{


    std::string
    InconsistentDomainsMsg(::MoReFEM::GeomEltNS::index_type geom_elt_index,
                           std::pair<::MoReFEM::DomainNS::unique_id, ::MoReFEM::DomainNS::unique_id> domain_ids,
                           std::string_view parameter_name);


    template<class LinearAlgebraT>
    std::string
    InconsistentLocalLinearAlgebraShapeMsg(const std::map<::MoReFEM::DomainNS::unique_id, LinearAlgebraT>& storage,
                                           std::string_view parameter_name);


} // namespace


namespace MoReFEM::ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS
{


    InconsistentDomains::~InconsistentDomains() = default;


    InconsistentDomains::InconsistentDomains(
        ::MoReFEM::GeomEltNS::index_type geom_elt_index,
        std::pair<::MoReFEM::DomainNS::unique_id, ::MoReFEM::DomainNS::unique_id> domain_ids,
        std::string_view parameter_name,
        const std::source_location location)
    : MoReFEM::Exception(InconsistentDomainsMsg(geom_elt_index, domain_ids, parameter_name), location)
    { }


    InconsistentLocalVectorShape::~InconsistentLocalVectorShape() = default;


    InconsistentLocalVectorShape::InconsistentLocalVectorShape(
        const std::map<DomainNS::unique_id, LocalVector>& storage,
        std::string_view parameter_name,
        const std::source_location location)
    : MoReFEM::Exception(InconsistentLocalLinearAlgebraShapeMsg(storage, parameter_name), location)
    { }


    InconsistentLocalMatrixShape::~InconsistentLocalMatrixShape() = default;


    InconsistentLocalMatrixShape::InconsistentLocalMatrixShape(
        const std::map<DomainNS::unique_id, LocalMatrix>& storage,
        std::string_view parameter_name,
        const std::source_location location)
    : MoReFEM::Exception(InconsistentLocalLinearAlgebraShapeMsg(storage, parameter_name), location)
    { }


} // namespace MoReFEM::ExceptionNS::ParameterNS::PiecewiseConstantByDomainNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //

namespace // anonymous
{


    std::string
    InconsistentDomainsMsg(::MoReFEM::GeomEltNS::index_type geom_elt_index,
                           std::pair<::MoReFEM::DomainNS::unique_id, ::MoReFEM::DomainNS::unique_id> domain_ids,
                           std::string_view parameter_name)
    {
        std::ostringstream oconv;
        oconv << "A geometric element (which index is " << geom_elt_index << ") is in at least two domains ("
              << domain_ids.first << " and " << domain_ids.second << ") with two different values for the '"
              << parameter_name << "' parameter.";

        return oconv.str();
    }


    template<class LinearAlgebraT>
    std::string
    InconsistentLocalLinearAlgebraShapeMsg(const std::map<::MoReFEM::DomainNS::unique_id, LinearAlgebraT>& storage,
                                           std::string_view parameter_name)
    {
        using namespace MoReFEM;

        std::ostringstream oconv;
        oconv << "Parameter '" << parameter_name
              << "' was defined as being piecewise constant by domain but the "
                 "shape of the provided linear algebra provided don't match: "
              << std::endl;

        for (const auto& [domain_id, linear_algebra] : storage)
        {
            Utilities::PrintContainer<>::Do(linear_algebra.shape(),
                                            oconv,
                                            PrintNS::Delimiter::separator(", "),
                                            PrintNS::Delimiter::opener("\t- ["),
                                            PrintNS::Delimiter::closer("]\n"));
        }

        return oconv.str();
    }


} // namespace
