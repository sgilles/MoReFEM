// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_ATQUADRATUREPOINT_INTERNAL_ATQUADRATUREPOINT_DOT_HPP_
#define MOREFEM_PARAMETERS_POLICY_ATQUADRATUREPOINT_INTERNAL_ATQUADRATUREPOINT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>


namespace MoReFEM::Internal::ParameterNS::AtQuadraturePointNS
{


    /*!
     * \brief Structure that holds both a value at a quadrature point and the last time it was modified.
     *
     * \tparam StorageValueTypeT Type of the value stored.
     */
    template<class StorageValueTypeT>
    struct ValueHolder
    {

        //! Alias to type stored.
        using type = StorageValueTypeT;

        /*!
         * \brief Constructor.
         *
         * \param[in] value to store.
         * \param[in] time_manager_Ntime_modified Index that specify the last time the value was modified.
         * (was obtained through TimeManager::NtimeModified() method).
         */
        ValueHolder(StorageValueTypeT value, std::size_t time_manager_Ntime_modified);

        //! Value at a given quadrature point.
        StorageValueTypeT value;

        //! Index that specifies when it was last updated (it is produced by TimeManager::NtimeModified()).
        std::size_t last_update_index;
    };


} // namespace MoReFEM::Internal::ParameterNS::AtQuadraturePointNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Parameters/Policy/AtQuadraturePoint/Internal/AtQuadraturePoint.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_ATQUADRATUREPOINT_INTERNAL_ATQUADRATUREPOINT_DOT_HPP_
// *** MoReFEM end header guards *** < //
