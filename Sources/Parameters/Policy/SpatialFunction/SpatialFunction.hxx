// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_SPATIALFUNCTION_SPATIALFUNCTION_DOT_HXX_
#define MOREFEM_PARAMETERS_POLICY_SPATIALFUNCTION_SPATIALFUNCTION_DOT_HXX_
// IWYU pragma: private, include "Parameters/Policy/SpatialFunction/SpatialFunction.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Parameters/Policy/SpatialFunction/SpatialFunction.hpp"


namespace MoReFEM::ParameterNS::Policy
{


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, CoordsType CoordsTypeT>
    SpatialFunction<TypeT, TimeManagerT, CoordsTypeT>::SpatialFunction(const std::string&,
                                                                       const Domain&,
                                                                       parameter_storage_type lua_function)
    : lua_function_(lua_function)
    { }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, CoordsType CoordsTypeT>
    auto SpatialFunction<TypeT, TimeManagerT, CoordsTypeT>::GetValueFromPolicy(const local_coords_type& local_coords,
                                                                               const GeometricElt& geom_elt) const
        -> return_type
    {
        if constexpr (CoordsTypeT == CoordsType::local)
            return lua_function_(
                local_coords.GetValueOrZero(0), local_coords.GetValueOrZero(1), local_coords.GetValueOrZero(2));

        else if constexpr (CoordsTypeT == CoordsType::global)
        {
            auto& coords = GetNonCstWorkCoords();
            Advanced::GeomEltNS::Local2Global(geom_elt, local_coords, coords);
            return lua_function_(coords.x(), coords.y(), coords.z());
        }

        assert(false
               && "Should never happen as all possible values of the enum class are addressed in the "
                  "cases above.");
        exit(EXIT_FAILURE);
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, CoordsType CoordsTypeT>
    auto SpatialFunction<TypeT, TimeManagerT, CoordsTypeT>::GetAnyValueFromPolicy() const -> return_type
    {
        return lua_function_(0., 0., 0.);
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, CoordsType CoordsTypeT>
    [[noreturn]] auto
    SpatialFunction<TypeT, TimeManagerT, CoordsTypeT>::GetConstantValueFromPolicy() const -> return_type
    {
        assert(false && "A Lua function should yield IsConstant() = false!");
        exit(-1);
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, CoordsType CoordsTypeT>
    bool SpatialFunction<TypeT, TimeManagerT, CoordsTypeT>::IsConstant() const
    {
        return false;
    }

    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, CoordsType CoordsTypeT>
    void SpatialFunction<TypeT, TimeManagerT, CoordsTypeT>::WriteFromPolicy(std::ostream& out) const
    {
        out << "# Given by the Lua function given in the input data file." << std::endl;
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, CoordsType CoordsTypeT>
    inline SpatialPoint& SpatialFunction<TypeT, TimeManagerT, CoordsTypeT>::GetNonCstWorkCoords() const noexcept
    {
        return work_coords_;
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, CoordsType CoordsTypeT>
    inline void SpatialFunction<TypeT, TimeManagerT, CoordsTypeT>::SetConstantValue(value_type value)
    {
        static_cast<void>(value);
        assert(false);
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::ParameterNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_SPATIALFUNCTION_SPATIALFUNCTION_DOT_HXX_
// *** MoReFEM end header guards *** < //
