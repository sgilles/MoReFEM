// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_ATDOF_ATDOF_DOT_HXX_
#define MOREFEM_PARAMETERS_POLICY_ATDOF_ATDOF_DOT_HXX_
// IWYU pragma: private, include "Parameters/Policy/AtDof/AtDof.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Parameters/Policy/AtDof/AtDof.hpp"


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::ParameterNS::Policy
{


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, std::size_t Ndim>
    AtDof<TypeT, TimeManagerT, Ndim>::AtDof(const std::string& name,
                                            const Domain& domain,
                                            const FEltSpace& felt_space,
                                            const Unknown& unknown,
                                            const GlobalVector& global_vector)
    : felt_space_storage_(felt_space), unknown_(unknown), global_vector_(global_vector)
    {
        static_assert(Ndim == 1, "This specific constructor should be called only in this case!");

        Construct(name, domain, felt_space, unknown, global_vector);
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, std::size_t Ndim>
    AtDof<TypeT, TimeManagerT, Ndim>::AtDof(const std::string& name,
                                            const Domain& domain,
                                            const FEltSpace& felt_space_dim,
                                            const FEltSpace& felt_space_dim_minus_1,
                                            const Unknown& unknown,
                                            const GlobalVector& global_vector)
    : felt_space_storage_(felt_space_dim, felt_space_dim_minus_1), unknown_(unknown), global_vector_(global_vector)
    {
        static_assert(Ndim == 2, "This specific constructor should be called only in this case!");

        Construct(name, domain, felt_space_dim, unknown, global_vector);

        assert(felt_space_dim_minus_1.GetGodOfDofFromWeakPtr()->GetUniqueId() == domain.GetMeshIdentifier());
        assert(felt_space_dim_minus_1.DoCoverNumberingSubset(global_vector.GetNumberingSubset()));
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, std::size_t Ndim>
    AtDof<TypeT, TimeManagerT, Ndim>::AtDof(const std::string& name,
                                            const Domain& domain,
                                            const FEltSpace& felt_space_dim,
                                            const FEltSpace& felt_space_dim_minus_1,
                                            const FEltSpace& felt_space_dim_minus_2,
                                            const Unknown& unknown,
                                            const GlobalVector& global_vector)
    : felt_space_storage_(felt_space_dim, felt_space_dim_minus_1, felt_space_dim_minus_2), unknown_(unknown),
      global_vector_(global_vector)
    {
        static_assert(Ndim == 3, "This specific constructor should be called only in this case!");

        Construct(name, domain, felt_space_dim, unknown, global_vector);

        assert(felt_space_dim_minus_1.GetGodOfDofFromWeakPtr()->GetUniqueId() == domain.GetMeshIdentifier());
        assert(felt_space_dim_minus_1.DoCoverNumberingSubset(global_vector.GetNumberingSubset()));
        assert(felt_space_dim_minus_2.GetGodOfDofFromWeakPtr()->GetUniqueId() == domain.GetMeshIdentifier());
        assert(felt_space_dim_minus_2.DoCoverNumberingSubset(global_vector.GetNumberingSubset()));
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, std::size_t Ndim>
    typename AtDof<TypeT, TimeManagerT, Ndim>::return_type
    AtDof<TypeT, TimeManagerT, Ndim>::GetValueFromPolicy(const local_coords_type& local_coords,
                                                         const GeometricElt& geom_elt) const
    {
        const auto& felt_space = GetFEltSpace(geom_elt);

        const auto& ref_geom_elt = geom_elt.GetRefGeomElt();
        const auto& ref_felt_space = felt_space.GetRefLocalFEltSpace(ref_geom_elt);

        const auto& local_felt_space = felt_space.GetLocalFEltSpace(geom_elt);

        const auto& unknown = GetUnknown();

        const auto& felt = local_felt_space.GetFElt(unknown);

        const auto& node_list = felt.GetNodeList();

        const auto& global_vector = GetGlobalVector();
        const auto& numbering_subset = global_vector.GetNumberingSubset();

        Wrappers::Petsc::AccessGhostContent access_ghost_content(global_vector);

        const auto& vector_with_ghost = access_ghost_content.GetVectorWithGhost();

        Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only> vector_with_ghost_content(vector_with_ghost);

        const auto& basic_ref_elt = ref_felt_space.GetRefFElt(felt_space, unknown).GetBasicRefFElt();

        Internal::ParameterNS::Policy::AtDofNS::ComputeLocalValue(
            basic_ref_elt, local_coords, node_list, numbering_subset, vector_with_ghost_content, local_value_);

        return local_value_;
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, std::size_t Ndim>
    typename AtDof<TypeT, TimeManagerT, Ndim>::return_type
    AtDof<TypeT, TimeManagerT, Ndim>::GetAnyValueFromPolicy() const
    {
        return local_value_;
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, std::size_t Ndim>
    void AtDof<TypeT, TimeManagerT, Ndim>::WriteFromPolicy(std::ostream& out) const
    {
        out << "AtDof<TypeT, TimeManagerT, Ndim>::WriteFromPolicy() is not activated at the moment; you can however "
               "ask "
               "instead "
            << "for GetGlobalVector() and then use one of its method to see its content." << std::endl;
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, std::size_t Ndim>
    [[noreturn]] typename AtDof<TypeT, TimeManagerT, Ndim>::return_type
    AtDof<TypeT, TimeManagerT, Ndim>::GetConstantValueFromPolicy() const
    {
        assert(false && "Should yield IsConstant() = false!");
        exit(-1);
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, std::size_t Ndim>
    inline bool AtDof<TypeT, TimeManagerT, Ndim>::IsConstant() const noexcept
    {
        return false;
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, std::size_t Ndim>
    inline const GlobalVector& AtDof<TypeT, TimeManagerT, Ndim>::GetGlobalVector() const noexcept
    {
        return global_vector_;
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, std::size_t Ndim>
    inline const Unknown& AtDof<TypeT, TimeManagerT, Ndim>::GetUnknown() const noexcept
    {
        return unknown_;
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, std::size_t Ndim>
    inline const FEltSpace& AtDof<TypeT, TimeManagerT, Ndim>::GetFEltSpace(const GeometricElt& geom_elt) const noexcept
    {
        return felt_space_storage_.GetFEltSpace(geom_elt);
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, std::size_t Ndim>
    void AtDof<TypeT, TimeManagerT, Ndim>::Construct(const std::string& name,
                                                     const Domain& domain,
                                                     const FEltSpace& first_felt_space,
                                                     const Unknown& unknown,
                                                     const GlobalVector& global_vector)
    {
        static_cast<void>(name);
        static_cast<void>(first_felt_space);
        static_cast<void>(unknown);
        static_cast<void>(global_vector);

        assert(first_felt_space.DoCoverNumberingSubset(global_vector.GetNumberingSubset()));
        assert(first_felt_space.GetGodOfDofFromWeakPtr()->GetUniqueId() == domain.GetMeshIdentifier());

#ifndef NDEBUG
        {
            if (TypeT == ParameterNS::Type::scalar && unknown.GetNature() == UnknownNS::Nature::vectorial)
                assert(false && "A scalar parameter given at dof can only be matched with a scalar Unknown.");
        }
#endif // NDEBUG

        decltype(auto) mesh = domain.GetMesh();

        assert(first_felt_space.GetMeshDimension() == mesh.GetDimension());
        Internal::ParameterNS::Policy::AtDofNS::InitLocalValue(mesh.GetDimension(), local_value_);
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, std::size_t Ndim>
    const Internal::ParameterNS::Policy::AtDofNS::FEltSpaceStorage<Ndim>&
    AtDof<TypeT, TimeManagerT, Ndim>::GetFEltSpaceStorage() const noexcept
    {
        return felt_space_storage_;
    }


    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, std::size_t Ndim>
    constexpr std::size_t AtDof<TypeT, TimeManagerT, Ndim>::NfeltSpace() noexcept
    {
        return Ndim;
    }

    template<ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, std::size_t Ndim>
    inline void AtDof<TypeT, TimeManagerT, Ndim>::SetConstantValue(value_type value)
    {
        static_cast<void>(value);
        assert(false && "SetConstantValue() meaningless for current Parameter.");
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::ParameterNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_ATDOF_ATDOF_DOT_HXX_
// *** MoReFEM end header guards *** < //
