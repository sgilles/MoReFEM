// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_POLICY_ATDOF_INTERNAL_ATDOF_DOT_HXX_
#define MOREFEM_PARAMETERS_POLICY_ATDOF_INTERNAL_ATDOF_DOT_HXX_
// IWYU pragma: private, include "Parameters/Policy/AtDof/Internal/AtDof.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Parameters/Policy/AtDof/Internal/AtDof.hpp"


#include <cstddef> // IWYU pragma: keep

#include "Geometry/GeometricElt/GeometricElt.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class FEltSpace; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::ParameterNS::Policy::AtDofNS
{


    inline const FEltSpace& FEltSpaceStorage<1>::GetFEltSpace(std::size_t) const
    {
        return felt_space_;
    }


    inline const FEltSpace& FEltSpaceStorage<1>::GetFEltSpace(const GeometricElt&) const
    {
        return felt_space_;
    }


    inline const FEltSpace& FEltSpaceStorage<2>::GetFEltSpace(const GeometricElt& geom_elt) const
    {
        return GetFEltSpace(geom_elt.GetDimension());
    }


    inline const FEltSpace& FEltSpaceStorage<3>::GetFEltSpace(const GeometricElt& geom_elt) const
    {
        return GetFEltSpace(geom_elt.GetDimension());
    }


} // namespace MoReFEM::Internal::ParameterNS::Policy::AtDofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_POLICY_ATDOF_INTERNAL_ATDOF_DOT_HXX_
// *** MoReFEM end header guards *** < //
