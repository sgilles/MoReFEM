// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <cstdlib>
#include <memory>
#include <type_traits> // IWYU pragma: keep


#include "ThirdParty/Wrappers/Petsc/Vector/AccessVectorContent.hpp"

#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#include "Parameters/Policy/AtDof/Internal/AtDof.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::ParameterNS::Policy::AtDofNS
{


    void InitLocalValue(std::size_t dimension, double& value)
    {
        static_cast<void>(dimension);
        value = 0.;
    }


    void InitLocalValue(std::size_t dimension, LocalVector& value)
    {
        value.resize({ dimension });
        value.fill(0.);
    }


    void ComputeLocalValue(
        const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
        const LocalCoords& local_coords,
        const Node::vector_shared_ptr& node_list,
        const NumberingSubset& numbering_subset,
        const ::MoReFEM::Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only>& vector_with_ghost_content,
        double& value)
    {
        const LocalNodeNS::index_type Nnode{ static_cast<LocalNodeNS::index_type::underlying_type>(node_list.size()) };

        value = 0.;

        for (LocalNodeNS::index_type local_node_index{ 0ul }; local_node_index < Nnode; ++local_node_index)
        {
            assert(local_node_index.Get() < node_list.size());
            const auto& node_ptr = node_list[local_node_index.Get()];
            assert(!(!node_ptr));
            const auto& node = *node_ptr;
            assert(node.IsInNumberingSubset(numbering_subset));

            const auto& dof_list = node.GetDofList();

            assert(dof_list.size() == 1ul && "Scalar case!");

            const auto& dof_ptr = dof_list.back();
            assert(!(!dof_ptr));
            const auto processor_wise_index = dof_ptr->GetProcessorWiseOrGhostIndex(numbering_subset);

            value += basic_ref_felt.ShapeFunction(local_node_index, local_coords)
                     * vector_with_ghost_content.GetValue(processor_wise_index.Get());
        }
    }


    void ComputeLocalValue(
        const Internal::RefFEltNS::BasicRefFElt& basic_ref_felt,
        const LocalCoords& local_coords,
        const Node::vector_shared_ptr& node_list,
        const NumberingSubset& numbering_subset,
        const ::MoReFEM::Wrappers::Petsc::AccessVectorContent<Utilities::Access::read_only>& vector_with_ghost_content,
        LocalVector& value)
    {
        value.fill(0.);

        const LocalNodeNS::index_type Nnode{ static_cast<LocalNodeNS::index_type::underlying_type>(node_list.size()) };

        for (LocalNodeNS::index_type local_node_index{ 0ul }; local_node_index < Nnode; ++local_node_index)
        {
            assert(local_node_index.Get() < node_list.size());
            const auto& node_ptr = node_list[local_node_index.Get()];
            assert(!(!node_ptr));
            const auto& node = *node_ptr;
            assert(node.IsInNumberingSubset(numbering_subset));

            const auto& dof_list = node.GetDofList();

            const auto Ndof = dof_list.size();

            for (auto dof_index = 0ul; dof_index < Ndof; ++dof_index)
            {
                const auto& dof_ptr = dof_list[dof_index];
                assert(!(!dof_ptr));
                const auto processor_wise_index = dof_ptr->GetProcessorWiseOrGhostIndex(numbering_subset);
                assert(dof_index < value.size());

                value(dof_index) += basic_ref_felt.ShapeFunction(local_node_index, local_coords)
                                    * vector_with_ghost_content.GetValue(processor_wise_index.Get());
            }
        }
    }


    FEltSpaceStorage<1>::FEltSpaceStorage(const FEltSpace& felt_space)
    : felt_space_(felt_space), mesh_dimension_(felt_space.GetMeshDimension())
    { }


    FEltSpaceStorage<2>::FEltSpaceStorage(const FEltSpace& felt_space_dim, const FEltSpace& felt_space_dim_minus_1)
    : felt_space_dim_(felt_space_dim), felt_space_dim_minus_1_(felt_space_dim_minus_1),
      mesh_dimension_(felt_space_dim.GetMeshDimension())
    {
        assert(mesh_dimension_ == felt_space_dim_minus_1.GetMeshDimension());
    }


    FEltSpaceStorage<3>::FEltSpaceStorage(const FEltSpace& felt_space_dim,
                                          const FEltSpace& felt_space_dim_minus_1,
                                          const FEltSpace& felt_space_dim_minus_2)
    : felt_space_dim_(felt_space_dim), felt_space_dim_minus_1_(felt_space_dim_minus_1),
      felt_space_dim_minus_2_(felt_space_dim_minus_2), mesh_dimension_(felt_space_dim.GetMeshDimension())
    {
        assert(mesh_dimension_ == felt_space_dim_minus_1.GetMeshDimension());
        assert(mesh_dimension_ == felt_space_dim_minus_2.GetMeshDimension());
    }


    const FEltSpace& FEltSpaceStorage<2>::GetFEltSpace(std::size_t geom_elt_dimension) const
    {
        assert(geom_elt_dimension <= mesh_dimension_);
        const auto diff = mesh_dimension_ - geom_elt_dimension;
        assert(diff <= 2); // 2 as the template parameter!

        switch (diff)
        {
        case 0:
            return felt_space_dim_;
        case 1:
            return felt_space_dim_minus_1_;
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


    const FEltSpace& FEltSpaceStorage<3>::GetFEltSpace(std::size_t geom_elt_dimension) const
    {
        assert(geom_elt_dimension <= mesh_dimension_);
        const auto diff = mesh_dimension_ - geom_elt_dimension;
        assert(diff <= 3); // 3 as the template parameter!

        switch (diff)
        {
        case 0:
            return felt_space_dim_;
        case 1:
            return felt_space_dim_minus_1_;
        case 2:
            return felt_space_dim_minus_2_;
        }

        assert(false);
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::Internal::ParameterNS::Policy::AtDofNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //
