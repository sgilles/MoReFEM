// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_PARAMETER_DOT_HXX_
#define MOREFEM_PARAMETERS_PARAMETER_DOT_HXX_
// IWYU pragma: private, include "Parameters/Parameter.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Parameters/Parameter.hpp"


namespace MoReFEM
{


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        class LocalCoordsT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    template<class T>
    Parameter<TypeT, LocalCoordsT, TimeManagerT, TimeDependencyT>::Parameter(T&& name, const Domain& domain)
    : name_(name), domain_(domain)
    {
        if (!domain.template IsConstraintOn<Advanced::DomainNS::Criterion::mesh>())
            throw Exception(std::string("Parameter ") + name
                            + " is inconsistently defined: domain should be "
                              "circumscribed to a mesh.");
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        class LocalCoordsT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    void Parameter<TypeT, LocalCoordsT, TimeManagerT, TimeDependencyT>::SetTimeDependency(
        typename time_dependency_type::unique_ptr&& time_dependency)
    {
        static_assert(
            !std::is_same<time_dependency_type, ParameterNS::TimeDependencyNS::None<TypeT, TimeManagerT>>::value,
            "Should not be called for Parameters without time dependency!");

        assert(time_dependency_ == nullptr && "Should be assigned only once!");

        assert(!(!time_dependency));
        time_dependency_ = std::move(time_dependency);

        time_dependency_->Init(SupplGetAnyValue());
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        class LocalCoordsT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline typename Parameter<TypeT, LocalCoordsT, TimeManagerT, TimeDependencyT>::return_type
    Parameter<TypeT, LocalCoordsT, TimeManagerT, TimeDependencyT>::GetConstantValue() const
    {
        assert(IsConstant() && "This method is relevant only for spatially constant parameters.");

        if constexpr (TimeDependencyT<TypeT, TimeManagerT>::no_time_dependency)
            return SupplGetConstantValue();
        else
            return GetTimeDependency().ApplyTimeFactor(SupplGetConstantValue());
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        class LocalCoordsT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline typename Parameter<TypeT, LocalCoordsT, TimeManagerT, TimeDependencyT>::return_type
    Parameter<TypeT, LocalCoordsT, TimeManagerT, TimeDependencyT>::GetValue(const LocalCoordsT& local_coords,
                                                                            const GeometricElt& geom_elt) const
    {
#ifndef NDEBUG
        {
            decltype(auto) domain = GetDomain();
            assert(domain.IsGeometricEltInside(geom_elt)
                   && "Attempt to use a Parameter outside of its domain "
                      "definition.");
        }
#endif // NDEBUG

        if (IsConstant())
            return GetConstantValue(); // Potential time dependency already applied within GetConstantValue().

        if constexpr (TimeDependencyT<TypeT, TimeManagerT>::no_time_dependency)
            return SupplGetValue(local_coords, geom_elt);
        else
            return GetTimeDependency().ApplyTimeFactor(SupplGetValue(local_coords, geom_elt));
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        class LocalCoordsT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline void
    Parameter<TypeT, LocalCoordsT, TimeManagerT, TimeDependencyT>::Write(const FilesystemNS::File& file) const
    {
        std::ofstream out{ file.NewContent() };

        this->Write(out);
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        class LocalCoordsT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline void Parameter<TypeT, LocalCoordsT, TimeManagerT, TimeDependencyT>::Write(std::ostream& out) const
    {
        out << "# Name = " << GetName() << std::endl;
        this->SupplWrite(out);
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        class LocalCoordsT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline const std::string& Parameter<TypeT, LocalCoordsT, TimeManagerT, TimeDependencyT>::GetName() const
    {
        return name_;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        class LocalCoordsT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline auto Parameter<TypeT, LocalCoordsT, TimeManagerT, TimeDependencyT>::GetTimeDependency() const noexcept
        -> const time_dependency_type&
    {
        assert(!(!time_dependency_)
               && "Make sure Parameter::SetTimeDependency() has been correctly called after "
                  "construction.");
        return *time_dependency_;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        class LocalCoordsT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline auto Parameter<TypeT, LocalCoordsT, TimeManagerT, TimeDependencyT>::GetNonCstTimeDependency() noexcept
        -> time_dependency_type&
    {
        return const_cast<time_dependency_type&>(GetTimeDependency());
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        class LocalCoordsT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline void Parameter<TypeT, LocalCoordsT, TimeManagerT, TimeDependencyT>::TimeUpdate()
    {
        if constexpr (IsTimeDependent())
            GetNonCstTimeDependency().Update();
        SupplTimeUpdate();
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        class LocalCoordsT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline void Parameter<TypeT, LocalCoordsT, TimeManagerT, TimeDependencyT>::TimeUpdate(double time)
    {
        if constexpr (IsTimeDependent())
            GetNonCstTimeDependency().Update(time);

        SupplTimeUpdate(time);
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        class LocalCoordsT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    constexpr bool Parameter<TypeT, LocalCoordsT, TimeManagerT, TimeDependencyT>::IsTimeDependent()
    {
        return (TimeDependencyT<TypeT, TimeManagerT>::no_time_dependency == false);
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        class LocalCoordsT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline const Domain& Parameter<TypeT, LocalCoordsT, TimeManagerT, TimeDependencyT>::GetDomain() const noexcept
    {
        return domain_;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_PARAMETER_DOT_HXX_
// *** MoReFEM end header guards *** < //
