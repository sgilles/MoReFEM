// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParametersGroup
 * \addtogroup ParametersGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERS_INITPARAMETERFROMINPUTDATA_INITPARAMETERFROMINPUTDATA_DOT_HXX_
#define MOREFEM_PARAMETERS_INITPARAMETERFROMINPUTDATA_INITPARAMETERFROMINPUTDATA_DOT_HXX_
// IWYU pragma: private, include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Parameters/InitParameterFromInputData/InitParameterFromInputData.hpp"


#include "Core/MoReFEMData/Extract.hpp"

#include "Parameters/Exceptions/Exception.hpp"


namespace MoReFEM
{


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        class StringT
    >
    // clang-format on
    typename ScalarParameter<TimeManagerT, TimeDependencyT>::unique_ptr InitScalarParameterFromInputData(
        StringT&& name,
        const Domain& domain,
        const std::string& nature,
        const typename Internal::ParameterNS::Traits<ParameterNS::Type::scalar>::variant_type& value)
    {
        if (nature == "ignore")
            return nullptr;

        using traits = Internal::ParameterNS::Traits<ParameterNS::Type::scalar>;

        if (nature == "constant")
        {
            using parameter_type = Internal::ParameterNS::ParameterInstance<ParameterNS::Type::scalar,
                                                                            ::MoReFEM::ParameterNS::Policy::Constant,
                                                                            TimeManagerT,
                                                                            TimeDependencyT>;

            using storage_type = traits::value_type;
            static_assert(std::is_same<storage_type, double>());

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, std::get<storage_type>(value));
        } else if (nature == "lua_function")
        {
            // clang-format off
             using parameter_type =
                 Internal::ParameterNS::ParameterInstance
                 <
                     ParameterNS::Type::scalar,
                     ::MoReFEM::ParameterNS::Policy::SpatialFunctionGlobalCoords,
                    TimeManagerT,
                     TimeDependencyT
                 >;
            // clang-format on

            using storage_type = ::MoReFEM::Wrappers::Lua::spatial_function;

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, std::get<storage_type>(value));
        } else if (nature == "piecewise_constant_by_domain")
        {
            // clang-format off
             using parameter_type =
                 Internal::ParameterNS::ParameterInstance
                 <
                     ParameterNS::Type::scalar,
                     ::MoReFEM::ParameterNS::Policy::PiecewiseConstantByDomain,
                     TimeManagerT,
                     TimeDependencyT
                 >;
            // clang-format on

            using storage_type = traits::piecewise_constant_by_domain_type;

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, std::get<storage_type>(value));
        } else
        {
            assert(false
                   && "Should not happen: all the possible choices are assumed to be checked by "
                      "OptionFile Constraints.");
            exit(EXIT_FAILURE);
        }
    }


    // clang-format off
    template
    <
        Advanced::Concept::InputDataNS::SectionType SectionT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        class StringT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    typename ScalarParameter<TimeManagerT, TimeDependencyT>::unique_ptr
    InitScalarParameterFromInputData(StringT&& name, const Domain& domain, const MoReFEMDataT& morefem_data)
    {
        decltype(auto) nature = ::MoReFEM::InputDataNS::ExtractLeaf<typename SectionT::Nature>(morefem_data);
        decltype(auto) value = ::MoReFEM::InputDataNS::ExtractLeaf<typename SectionT::Value>(morefem_data);

        return InitScalarParameterFromInputData<TimeManagerT, TimeDependencyT>(name, domain, nature, value);
    }


    // clang-format off
    template
    <
        Advanced::Concept::InputDataNS::SectionType SectionT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        class StringT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    typename VectorialParameter<TimeManagerT, TimeDependencyT>::unique_ptr
    InitVectorialParameterFromInputData(StringT&& name, const Domain& domain, const MoReFEMDataT& morefem_data)
    {
        decltype(auto) nature = ::MoReFEM::InputDataNS::ExtractLeaf<typename SectionT::Nature>(morefem_data);

        if (nature == "ignore")
            return nullptr;

        decltype(auto) value = ::MoReFEM::InputDataNS::ExtractLeaf<typename SectionT::Value>(morefem_data);
        decltype(auto) dimension =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename SectionT::VectorDimension>(morefem_data);

        using traits = Internal::ParameterNS::Traits<ParameterNS::Type::vector>;

        if (nature == "constant")
        {
            assert(std::holds_alternative<std::vector<double>>(value));

            decltype(auto) interpreted_value = std::get<std::vector<double>>(value);

            using parameter_type = Internal::ParameterNS::ParameterInstance<ParameterNS::Type::vector,
                                                                            ::MoReFEM::ParameterNS::Policy::Constant,
                                                                            TimeManagerT,
                                                                            TimeDependencyT>;

            using storage_type = traits::value_type;
            static_assert(std::is_same<storage_type, LocalVector>());

            if (interpreted_value.size() != dimension)
                throw ExceptionNS::ParameterNS::InconsistentVectorDimension(name, dimension, interpreted_value.size());

            LocalVector as_local_vector;
            as_local_vector.resize({ dimension });

            std::copy(interpreted_value.cbegin(), interpreted_value.cend(), as_local_vector.begin());

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, as_local_vector);
        } else if (nature == "piecewise_constant_by_domain")
        {
            using variant_alternative_type = traits::piecewise_constant_in_lua_file_type;
            assert(std::holds_alternative<variant_alternative_type>(value));

            decltype(auto) interpreted_value = std::get<variant_alternative_type>(value);

            using parameter_type =
                Internal::ParameterNS::ParameterInstance<ParameterNS::Type::vector,
                                                         ::MoReFEM::ParameterNS::Policy::PiecewiseConstantByDomain,
                                                         TimeManagerT,
                                                         TimeDependencyT>;

            using storage_type = traits::piecewise_constant_by_domain_type;

            storage_type content;

            for (const auto& [domain_id, as_vector] : interpreted_value)
            {
                if (as_vector.size() != dimension)
                    throw ExceptionNS::ParameterNS::InconsistentVectorDimensionForDomain(
                        name, domain_id, dimension, as_vector.size());
                LocalVector as_local_vector;
                as_local_vector.resize({ dimension });

                std::copy(as_vector.cbegin(), as_vector.cend(), as_local_vector.begin());

                content.insert({ domain_id, as_local_vector });
            }

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, content);
        } else if (nature == "lua_function")
            throw ExceptionNS::ParameterNS::NoLuaFunctionForVectorialOrMatricial(name);
        else
        {
            assert(false
                   && "Should not happen: all the possible choices are assumed to be checked by "
                      "OptionFile Constraints.");
            exit(EXIT_FAILURE);
        }
    }


    // clang-format off
    template
    <
        Advanced::Concept::InputDataNS::SectionType SectionT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        class StringT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    typename MatricialParameter<TimeManagerT, TimeDependencyT>::unique_ptr
    InitMatricialParameterFromInputData(StringT&& name, const Domain& domain, const MoReFEMDataT& morefem_data)
    {
        decltype(auto) nature = ::MoReFEM::InputDataNS::ExtractLeaf<typename SectionT::Nature>(morefem_data);

        if (nature == "ignore")
            return nullptr;

        decltype(auto) value = ::MoReFEM::InputDataNS::ExtractLeaf<typename SectionT::Value>(morefem_data);
        decltype(auto) dimension =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename SectionT::MatrixDimension>(morefem_data);

        if (dimension.size() != 2ul)
            throw ExceptionNS::ParameterNS::InvalidMatrixDimensionInOptionFile(name, dimension);

        using traits = Internal::ParameterNS::Traits<ParameterNS::Type::matrix>;

        if (nature == "constant")
        {
            assert(std::holds_alternative<traits::constant_in_lua_file_type>(value));

            decltype(auto) interpreted_value = std::get<traits::constant_in_lua_file_type>(value);

            using parameter_type = Internal::ParameterNS::ParameterInstance<ParameterNS::Type::matrix,
                                                                            ::MoReFEM::ParameterNS::Policy::Constant,
                                                                            TimeManagerT,
                                                                            TimeDependencyT>;

            using storage_type = traits::value_type;
            static_assert(std::is_same<storage_type, LocalMatrix>());

            if (interpreted_value.size() != dimension[0] * dimension[1])
                throw ExceptionNS::ParameterNS::InconsistentMatrixDimension(name, dimension, interpreted_value.size());

            LocalMatrix as_local_matrix;
            as_local_matrix.resize({ dimension[0], dimension[1] });

            std::copy(interpreted_value.cbegin(), interpreted_value.cend(), as_local_matrix.begin());

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, as_local_matrix);
        } else if (nature == "piecewise_constant_by_domain")
        {
            using variant_alternative_type = traits::piecewise_constant_in_lua_file_type;
            assert(std::holds_alternative<variant_alternative_type>(value));

            decltype(auto) interpreted_value = std::get<variant_alternative_type>(value);

            using parameter_type =
                Internal::ParameterNS::ParameterInstance<ParameterNS::Type::matrix,
                                                         ::MoReFEM::ParameterNS::Policy::PiecewiseConstantByDomain,
                                                         TimeManagerT,
                                                         TimeDependencyT>;

            using storage_type = traits::piecewise_constant_by_domain_type;

            storage_type content;

            for (const auto& [domain_id, as_std_vector] : interpreted_value)
            {
                if (as_std_vector.size() != dimension[0] * dimension[1])
                    throw ExceptionNS::ParameterNS::InconsistentMatrixDimensionForDomain(
                        name, domain_id, dimension, as_std_vector.size());

                LocalMatrix as_local_matrix;
                as_local_matrix.resize({ dimension[0], dimension[1] });

                std::copy(as_std_vector.cbegin(), as_std_vector.cend(), as_local_matrix.begin());

                content.insert({ domain_id, as_local_matrix });
            }

            return std::make_unique<parameter_type>(std::forward<StringT>(name), domain, content);
        } else if (nature == "lua_function")
            throw ExceptionNS::ParameterNS::NoLuaFunctionForVectorialOrMatricial(name);
        else
        {
            assert(false
                   && "Should not happen: all the possible choices are assumed to be checked by "
                      "OptionFile Constraints.");
            exit(EXIT_FAILURE);
        }
    }

} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParametersGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERS_INITPARAMETERFROMINPUTDATA_INITPARAMETERFROMINPUTDATA_DOT_HXX_
// *** MoReFEM end header guards *** < //
