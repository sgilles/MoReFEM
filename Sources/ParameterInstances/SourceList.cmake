### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_PARAM_INSTANCES}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/CMakeLists.txt
		${CMAKE_CURRENT_LIST_DIR}/ParameterInstances.doxygen
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
)

include(${CMAKE_CURRENT_LIST_DIR}/Compound/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/Fiber/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/FromParameterAtDof/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/GradientBasedElasticityTensor/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/InputInternalVariable/SourceList.cmake)
include(${CMAKE_CURRENT_LIST_DIR}/LameCoefficientsFromYoungAndPoisson/SourceList.cmake)
