// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_GRADIENTBASEDELASTICITYTENSOR_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_GRADIENTBASEDELASTICITYTENSOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "ParameterInstances/GradientBasedElasticityTensor/Internal/ComputeGradientBasedElasticityTensor.hpp"
#include "ParameterInstances/GradientBasedElasticityTensor/Internal/Gradient2Strain.hpp"
#include "Parameters/Parameter.hpp"


namespace MoReFEM::ParameterNS
{


    /*!
     * \brief Gradient based elasticity tensor.
     *
     * \copydoc doxygen_hide_parameter_without_time_dependency
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, GradientBasedElasticityTensorConfiguration ConfigurationT>
    class GradientBasedElasticityTensor final
    : public Parameter<ParameterNS::Type::matrix, LocalCoords, TimeManagerT, TimeDependencyNS::None>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = GradientBasedElasticityTensor<TimeManagerT, ConfigurationT>;

        //! \copydoc doxygen_hide_parameter_local_coords_type
        using local_coords_type = LocalCoords;

        //! Alias to base class.
        using parent = Parameter<ParameterNS::Type::matrix, LocalCoords, TimeManagerT, TimeDependencyNS::None>;

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = ScalarParameter<TimeManagerT, TimeDependencyNS::None>;

        //! Alias to return type.
        using return_type = typename parent::return_type;

        //! Alias to traits of parent class.
        using traits = typename parent::traits;

        //! Alias to the type of a value, 'inherited' from parent class.
        using value_type = typename parent::value_type;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] young_modulus Young's modulus.
         * \param[in] poisson_ratio Poisson ratio.
         */
        explicit GradientBasedElasticityTensor(const scalar_parameter_type& young_modulus,
                                               const scalar_parameter_type& poisson_ratio);

        //! Destructor.
        ~GradientBasedElasticityTensor() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        GradientBasedElasticityTensor(const GradientBasedElasticityTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        GradientBasedElasticityTensor(GradientBasedElasticityTensor&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        GradientBasedElasticityTensor& operator=(const GradientBasedElasticityTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GradientBasedElasticityTensor& operator=(GradientBasedElasticityTensor&& rhs) = delete;

        ///@}

        /*!
         * \brief Write the content of the Parameter in a stream.

         * \copydoc doxygen_hide_stream_inout
         */
        void SupplWrite(std::ostream& stream) const override;

        /*!
         * \brief Enables to modify the constant value of a parameter. Disabled for this Parameter.
         */
        void SetConstantValue(value_type) override;

      private:
        //! \copydoc doxygen_hide_parameter_suppl_get_value_local_coords
        return_type SupplGetValue(const local_coords_type& local_coords, const GeometricElt& geom_elt) const override;


        /*!
         * \brief Whether the parameter varies spatially or not.
         */
        bool IsConstant() const override;

        //! Young modulus.
        const scalar_parameter_type& GetYoungModulus() const;

        //! Poisson coefficient.
        const scalar_parameter_type& GetPoissonRatio() const;

        //! Compute the current value.
        //! \param[in] young_modulus Young's modulus at a given spatial position.
        //! \param[in] poisson_ratio Poisson ratio at a given spatial position.
        return_type ComputeValue(double young_modulus, double poisson_ratio) const;

        //! Returns the constant value (and compute it if first call).
        return_type SupplGetConstantValue() const override;

        //! \copydoc doxygen_hide_parameter_suppl_time_update
        void SupplTimeUpdate() override;

        //! \copydoc doxygen_hide_parameter_suppl_time_update_with_time
        void SupplTimeUpdate(double time) override;

        /*!
         *
         * \copydoc doxygen_hide_parameter_suppl_get_any_value
         */
        return_type SupplGetAnyValue() const override;

      private:
        //! Young modulus.
        const scalar_parameter_type& young_modulus_;

        //! Poisson coefficient.
        const scalar_parameter_type& poisson_ratio_;

        /*!
         * \brief Storage of the matrix.
         *
         * The matrix is allocated once in the initialization phase of the code; its content might change
         * during a call to GetValue() if either Young modulus or Poisson coefficient is not constant.
         */
        mutable Internal::ParameterNS::ComputeGradientBasedElasticityTensor<ConfigurationT> helper_;
    };


} // namespace MoReFEM::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ParameterInstances/GradientBasedElasticityTensor/GradientBasedElasticityTensor.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_GRADIENTBASEDELASTICITYTENSOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
