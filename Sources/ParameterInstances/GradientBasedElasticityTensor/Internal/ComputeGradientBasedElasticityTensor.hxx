// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_COMPUTEGRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_COMPUTEGRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/GradientBasedElasticityTensor/Internal/ComputeGradientBasedElasticityTensor.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "ParameterInstances/GradientBasedElasticityTensor/Internal/ComputeGradientBasedElasticityTensor.hpp"


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "Utilities/LinearAlgebra/LocalAlias.hpp"


#include "ParameterInstances/GradientBasedElasticityTensor/Internal/Gradient2Strain.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::ParameterNS { enum class GradientBasedElasticityTensorConfiguration; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::ParameterNS
{


    template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
    ComputeGradientBasedElasticityTensor<ConfigurationT>::ComputeGradientBasedElasticityTensor()
    {
        result_.resize(std::vector<std::size_t>{ traits::result_size, traits::result_size });
        engineering_elasticity_tensor_.resize(
            std::vector<std::size_t>{ traits::engineering_size, traits::engineering_size });
        engineering_elasticity_tensor_.fill(0.);
        intermediate_product_.resize(std::vector<std::size_t>{ traits::engineering_size, traits::result_size });
    }


    template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
    inline LocalMatrix& ComputeGradientBasedElasticityTensor<ConfigurationT>::GetNonCstResult()
    {
        return const_cast<LocalMatrix&>(GetResult());
    }


    template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
    inline const LocalMatrix& ComputeGradientBasedElasticityTensor<ConfigurationT>::GetResult() const noexcept
    {
        assert(result_.shape(0) == traits::result_size);
        assert(result_.shape(1) == traits::result_size);

        return result_;
    }


    template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
    inline LocalMatrix& ComputeGradientBasedElasticityTensor<ConfigurationT>::GetNonCstEngineeringElasticityTensor()
    {
        assert(engineering_elasticity_tensor_.shape(0) == traits::engineering_size);
        assert(engineering_elasticity_tensor_.shape(1) == traits::engineering_size);

        return engineering_elasticity_tensor_;
    }


    template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
    inline LocalMatrix& ComputeGradientBasedElasticityTensor<ConfigurationT>::GetNonCstIntermediateProduct()
    {
        assert(intermediate_product_.shape(0) == traits::engineering_size);
        assert(intermediate_product_.shape(1) == traits::result_size);

        return intermediate_product_;
    }


    template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
    const LocalMatrix& ComputeGradientBasedElasticityTensor<ConfigurationT>::Compute(const double young_modulus,
                                                                                     const double poisson_ratio)
    {
        const auto& engineering_elasticity_tensor = ComputeEngineeringElasticityTensor(young_modulus, poisson_ratio);

        const auto& gradient_2_strain = Gradient2Strain<traits::dimension>();
        auto& intermediate_product = GetNonCstIntermediateProduct();

        xt::noalias(intermediate_product) = xt::linalg::dot(engineering_elasticity_tensor, gradient_2_strain);
        auto& result = GetNonCstResult();
        xt::noalias(result) =
            xt::linalg::dot(xt::transpose(Gradient2Strain<traits::dimension>()), intermediate_product);

        return result_;
    }


} // namespace MoReFEM::Internal::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_COMPUTEGRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
