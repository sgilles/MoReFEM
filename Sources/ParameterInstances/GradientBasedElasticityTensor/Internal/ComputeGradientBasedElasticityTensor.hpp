// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_COMPUTEGRADIENTBASEDELASTICITYTENSOR_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_COMPUTEGRADIENTBASEDELASTICITYTENSOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/LinearAlgebra/LocalAlias.hpp"

#include "ParameterInstances/GradientBasedElasticityTensor/Configuration.hpp"
#include "ParameterInstances/GradientBasedElasticityTensor/Internal/Configuration.hpp"


namespace MoReFEM::Internal::ParameterNS
{


    //! Helper class that actually computes the tensor given Young modulus and Poisson ratio.
    template<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration ConfigurationT>
    class ComputeGradientBasedElasticityTensor
    {
      private:
        //! Alias to traits class that enrich ConfigurationT.
        using traits = TraitsNS::Configuration<ConfigurationT>;

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit ComputeGradientBasedElasticityTensor();

        //! Destructor.
        ~ComputeGradientBasedElasticityTensor() = default;

        //! \copydoc doxygen_hide_copy_constructor
        ComputeGradientBasedElasticityTensor(const ComputeGradientBasedElasticityTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ComputeGradientBasedElasticityTensor(ComputeGradientBasedElasticityTensor&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        ComputeGradientBasedElasticityTensor& operator=(const ComputeGradientBasedElasticityTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ComputeGradientBasedElasticityTensor& operator=(ComputeGradientBasedElasticityTensor&& rhs) = delete;

        ///@}

        //! Compute the gradient based elasticity tensor and returns it.
        //! \param[in] young_modulus Young's modulus at a given spatial position.
        //! \param[in] poisson_ratio Poisson ratio at a given spatial position.
        const LocalMatrix& Compute(double young_modulus, double poisson_ratio);

        /*!
         * \brief Returns the value of the gradient based elasticity tensor.
         *
         * Should not be called in non constant case (this is up to GradientBasedElasticityTensor class
         * to ensure that (except for GetAnyValue()); only this class is expected to manipulate present one).
         *
         * \return Value of the gradient based elasticity tensor.
         */
        const LocalMatrix& GetResult() const noexcept;

        //! Whether the tensor has already been computed. Meaningful only for spatially constant parameters.
        bool IsAlreadyComputed() const;


      private:
        //! Compute the Engineering elasticity tensor and returns it.
        //! \param[in] young_modulus Young's modulus at a given spatial position.
        //! \param[in] poisson_ratio Poisson ratio at a given spatial position.
        const LocalMatrix& ComputeEngineeringElasticityTensor(double young_modulus, double poisson_ratio);

        //! Non constant access to engineering_elasticity_tensor_.
        LocalMatrix& GetNonCstEngineeringElasticityTensor();

        //! Non constant access to result_.
        LocalMatrix& GetNonCstResult();

        //! Non constant access to intermediate_product_.
        LocalMatrix& GetNonCstIntermediateProduct();

      private:
        //! Storage of the matrix of interest.
        LocalMatrix result_;

        //! Engineering elasticity tensor.
        LocalMatrix engineering_elasticity_tensor_;

        //! Matrix used to store intermediate product in computation of result_.
        LocalMatrix intermediate_product_;
    };


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    template<>
    const LocalMatrix&
    ComputeGradientBasedElasticityTensor<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim1>::
        ComputeEngineeringElasticityTensor(const double young_modulus, const double poisson_ratio);

    template<>
    const LocalMatrix& ComputeGradientBasedElasticityTensor<
        ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_strain>::
        ComputeEngineeringElasticityTensor(const double young_modulus, const double poisson_ratio);

    template<>
    const LocalMatrix& ComputeGradientBasedElasticityTensor<
        ::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim2_plane_stress>::
        ComputeEngineeringElasticityTensor(const double young_modulus, const double poisson_ratio);


    template<>
    const LocalMatrix&
    ComputeGradientBasedElasticityTensor<::MoReFEM::ParameterNS::GradientBasedElasticityTensorConfiguration::dim3>::
        ComputeEngineeringElasticityTensor(const double young_modulus, const double poisson_ratio);


    // ============================
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


} // namespace MoReFEM::Internal::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ParameterInstances/GradientBasedElasticityTensor/Internal/ComputeGradientBasedElasticityTensor.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_COMPUTEGRADIENTBASEDELASTICITYTENSOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
