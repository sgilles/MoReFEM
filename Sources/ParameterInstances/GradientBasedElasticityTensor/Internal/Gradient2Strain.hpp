// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_GRADIENT2STRAIN_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_GRADIENT2STRAIN_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/LinearAlgebra/LocalAlias.hpp"


namespace MoReFEM::Internal::ParameterNS
{


    /*!
     * \brief Matrix defined so that strain = Gradient2Strain x gradient(displacement)
     *
     * No definition provided on purpose: this function is intended to be specialized.
     *
     * \tparam DimensionT Dimension of the mesh in which Parameter is defined.
     *
     * \return Matrix defined so that strain = Gradient2Strain x gradient(displacement)
     */
    template<int DimensionT>
    const LocalMatrix& Gradient2Strain();


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // ============================

    template<>
    const LocalMatrix& Gradient2Strain<1>();

    template<>
    const LocalMatrix& Gradient2Strain<2>();

    template<>
    const LocalMatrix& Gradient2Strain<3>();

    // ============================
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


} // namespace MoReFEM::Internal::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_GRADIENTBASEDELASTICITYTENSOR_INTERNAL_GRADIENT2STRAIN_DOT_HPP_
// *** MoReFEM end header guards *** < //
