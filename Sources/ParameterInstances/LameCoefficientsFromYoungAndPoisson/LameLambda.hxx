// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_LAMECOEFFICIENTSFROMYOUNGANDPOISSON_LAMELAMBDA_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_LAMECOEFFICIENTSFROMYOUNGANDPOISSON_LAMELAMBDA_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/LameCoefficientsFromYoungAndPoisson/LameLambda.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "ParameterInstances/LameCoefficientsFromYoungAndPoisson/LameLambda.hpp"


#include <cassert>
#include <cstdlib>
#include <limits>
// IWYU pragma: no_include <type_traits>

#include "Utilities/Numeric/Numeric.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ParameterNS
{

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    LameLambda<TimeManagerT>::LameLambda(const scalar_parameter_type& young_modulus,
                                         const scalar_parameter_type& poisson_ratio)
    : scalar_parameter_type("Lame coefficient 'lambda'", young_modulus.GetDomain()), young_modulus_(young_modulus),
      poisson_ratio_(poisson_ratio)
    {
        assert(parent::GetDomain() == poisson_ratio.GetDomain());

        if (IsConstant())
            constant_value_ = ComputeValue(young_modulus.GetConstantValue(), poisson_ratio.GetConstantValue());
    }

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void LameLambda<TimeManagerT>::SupplWrite(std::ostream& out) const
    {
        out << "# Lame lambda is defined from Young modulus and Poisson ratio, which values are defined the "
               "following way:"
            << std::endl;
        GetYoungModulus().Write(out);
        GetPoissonRatio().Write(out);
    }

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void LameLambda<TimeManagerT>::SupplTimeUpdate()
    {
        assert(!GetYoungModulus().IsTimeDependent());
        assert(!GetPoissonRatio().IsTimeDependent());
    }

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void LameLambda<TimeManagerT>::SupplTimeUpdate(double time)
    {
        assert(!GetYoungModulus().IsTimeDependent());
        assert(!GetPoissonRatio().IsTimeDependent());
        static_cast<void>(time);
    }

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    auto LameLambda<TimeManagerT>::SupplGetAnyValue() const -> return_type
    {
        return 0.;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto LameLambda<TimeManagerT>::SupplGetValue(const local_coords_type& local_coords,
                                                        const GeometricElt& geom_elt) const -> return_type
    {
        const double young_modulus = GetYoungModulus().GetValue(local_coords, geom_elt);
        const double poisson_ratio = GetPoissonRatio().GetValue(local_coords, geom_elt);

        return ComputeValue(young_modulus, poisson_ratio);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto LameLambda<TimeManagerT>::GetYoungModulus() const -> const scalar_parameter_type&
    {
        return young_modulus_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto LameLambda<TimeManagerT>::GetPoissonRatio() const -> const scalar_parameter_type&
    {
        return poisson_ratio_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline bool LameLambda<TimeManagerT>::IsConstant() const
    {
        return GetYoungModulus().IsConstant() && GetPoissonRatio().IsConstant();
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline typename LameLambda<TimeManagerT>::return_type
    LameLambda<TimeManagerT>::ComputeValue(const double young_modulus, const double poisson_ratio) const
    {
        assert(!NumericNS::AreEqual(poisson_ratio, -1.));
        assert(!NumericNS::AreEqual(poisson_ratio, .5));

        return poisson_ratio * young_modulus / ((1. + poisson_ratio) * (1. - 2. * poisson_ratio));
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline typename LameLambda<TimeManagerT>::return_type LameLambda<TimeManagerT>::SupplGetConstantValue() const
    {
        assert(IsConstant());
        assert(!NumericNS::AreEqual(constant_value_, std::numeric_limits<double>::lowest()));
        return constant_value_;
    }

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline void LameLambda<TimeManagerT>::SetConstantValue(value_type value)
    {
        static_cast<void>(value);
        assert(false && "SetConstantValue() meaningless for current Parameter.");
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_LAMECOEFFICIENTSFROMYOUNGANDPOISSON_LAMELAMBDA_DOT_HXX_
// *** MoReFEM end header guards *** < //
