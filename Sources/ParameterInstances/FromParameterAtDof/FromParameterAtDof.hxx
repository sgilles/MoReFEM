// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_FROMPARAMETERATDOF_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_FROMPARAMETERATDOF_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/FromParameterAtDof/FromParameterAtDof.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "ParameterInstances/FromParameterAtDof/FromParameterAtDof.hpp"


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::ParameterNS
{


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        std::size_t NfeltSpaceT
    >
    // clang-format on
    template<class StringT>
    typename ParameterAtQuadraturePoint<TypeT, TimeManagerT, TimeDependencyT>::unique_ptr
    FromParameterAtDof<TypeT, TimeManagerT, TimeDependencyT, NfeltSpaceT>::Perform(
        StringT&& name,
        const Domain& domain,
        const QuadratureRulePerTopology* const quadrature_rule_per_topology,
        const TimeManagerT& time_manager,
        const ParameterAtDof<TypeT, TimeManagerT, TimeDependencyT, NfeltSpaceT>& param_at_dof)
    {
        decltype(auto) felt_space_storage =
            param_at_dof.GetFEltSpaceStorage(); // current class is a friend of \a AtDof policy.

        decltype(auto) mesh = domain.GetMesh();
        const auto mesh_dimension = mesh.GetDimension();

        auto ret = std::make_unique<ParameterAtQuadraturePoint<TypeT, TimeManagerT, TimeDependencyT>>(
            name,
            domain,
            *quadrature_rule_per_topology,
            ::MoReFEM::Internal::ParameterNS::Traits<TypeT>::AllocateDefaultValue(mesh_dimension, mesh_dimension),
            time_manager);

        // For all \a FEltSpaces considered in the \a ParameterAtDof, report the values in the
        // \a ParameterAtQuadraturePoint. It is not straightforward to read as contrary to my habit, there
        // is an underlying knownledge of the internals of AtDof policy here (because exposing it would take more
        // time and would likely not be used elsewhere).
        for (auto i = 0ul; i < NfeltSpaceT; ++i)
        {
            decltype(auto) felt_space = felt_space_storage.GetFEltSpace(mesh_dimension - i);

            Internal::GlobalParameterOperatorNS::FromAtDofToAtQuadPt<TypeT, TimeManagerT, TimeDependencyT, NfeltSpaceT>
                conversion_operator(felt_space, param_at_dof, quadrature_rule_per_topology, *ret);

            conversion_operator.Update();
        }


        // Also transmit the time dependency.
        // In C++ 17 if constexpr would help... but not yet there: we have to resort to cumbersome template
        // specialization.

        Internal::ParameterNS::CopyTimeDependency<TypeT, TimeManagerT, TimeDependencyT, NfeltSpaceT>::Perform(
            param_at_dof, *ret);

        return ret;
    }


} // namespace MoReFEM::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_FROMPARAMETERATDOF_DOT_HXX_
// *** MoReFEM end header guards *** < //
