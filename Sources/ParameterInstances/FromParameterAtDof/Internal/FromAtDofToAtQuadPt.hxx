// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_INTERNAL_FROMATDOFTOATQUADPT_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_INTERNAL_FROMATDOFTOATQUADPT_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/FromParameterAtDof/Internal/FromAtDofToAtQuadPt.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::GlobalParameterOperatorNS
{


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        std::size_t NfeltSpaceT
    >
    // clang-format on
    const std::string& FromAtDofToAtQuadPt<TypeT, TimeManagerT, TimeDependencyT, NfeltSpaceT>::ClassName()
    {
        static const std::string ret = std::string("GlobalParameterOperatorNS::FromAtDofToAtQuadPt<")
                                       + ::MoReFEM::ParameterNS::Name<TypeT>() + ">";
        return ret;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        std::size_t NfeltSpaceT
    >
    // clang-format on
    FromAtDofToAtQuadPt<TypeT, TimeManagerT, TimeDependencyT, NfeltSpaceT>::FromAtDofToAtQuadPt(
        const FEltSpace& felt_space,
        const param_at_dof_type& param_at_dof,
        const QuadratureRulePerTopology* const quadrature_rule_per_topology,
        param_at_quad_pt_type& parameter_to_set)
    : parent(felt_space,
             param_at_dof.GetUnknown(),
             quadrature_rule_per_topology,
             AllocateGradientFEltPhi::no,
             parameter_to_set,
             param_at_dof)
    { }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
        std::size_t NfeltSpaceT
    >
    // clang-format on
    inline void FromAtDofToAtQuadPt<TypeT, TimeManagerT, TimeDependencyT, NfeltSpaceT>::Update() const
    {
        return parent::UpdateImpl();
    }


} // namespace MoReFEM::Internal::GlobalParameterOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_INTERNAL_FROMATDOFTOATQUADPT_DOT_HXX_
// *** MoReFEM end header guards *** < //
