// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_INTERNAL_COPYTIMEDEPENDENCY_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_INTERNAL_COPYTIMEDEPENDENCY_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/FromParameterAtDof/Internal/CopyTimeDependency.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "ParameterInstances/FromParameterAtDof/Internal/CopyTimeDependency.hpp"


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::ParameterNS
{


    // clang-format off
    template
    <
       ParameterNS::Type TypeT,
       TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
       template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
       std::size_t NfeltSpaceT
    >
    // clang-format on
    void CopyTimeDependency<TypeT, TimeManagerT, TimeDependencyT, NfeltSpaceT>::Perform(const at_dof_type& param_at_dof,
                                                                                        at_quad_pt_type& new_param)
    {
        auto &&time_dependency = std::make_unique < TimeDependencyT < TypeT,
             TimeManagerT >>> (param_at_dof.GetTimeDependency());
        new_param.SetTimeDependency(std::move(time_dependency));
    }


    // clang-format off
template
<
   ParameterNS::Type TypeT,
   TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
   std::size_t NfeltSpaceT
>
    // clang-format on
    void CopyTimeDependency<TypeT, TimeManagerT, ::MoReFEM::ParameterNS::TimeDependencyNS::None, NfeltSpaceT>::Perform(
        const at_dof_type&,
        at_quad_pt_type&)
    {
        // Do nothing.
    }


} // namespace MoReFEM::Internal::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_INTERNAL_COPYTIMEDEPENDENCY_DOT_HXX_
// *** MoReFEM end header guards *** < //
