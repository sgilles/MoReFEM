// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_INTERNAL_COPYTIMEDEPENDENCY_DOT_HPP_
#define MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_INTERNAL_COPYTIMEDEPENDENCY_DOT_HPP_
// *** MoReFEM header guards *** < //

// No include here: this include should be used at only one place in which all includes are already in place.

#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::ParameterNS
{


    /*!
     * \brief Static dispatcher for time dependency.
     *
     * \copydetails doxygen_hide_at_dof_policy_tparam
     *
     * \tparam TimeDependencyT Policy in charge of time dependency.
     *
     * TimeDependency is copied except when the time dependency is TimeDependencyNS::None.
     *
     * \internal To be refactored or removed (see #1854...)
     */
    // clang-format off
    template
    <
       ParameterNS::Type TypeT,
       TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
       template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT,
       std::size_t NfeltSpaceT = 1ul
    >
    // clang-format on
    struct CopyTimeDependency
    {

        //! Alias for the \a ParameterAtDof, which acts as the source one.
        using at_dof_type = ParameterAtDof<TypeT, TimeManagerT, TimeDependencyT, NfeltSpaceT>;

        //! Alias for the \a ParameterAtQuadraturePoint, which acts as the target one.
        using at_quad_pt_type = ParameterAtQuadraturePoint<TypeT, TimeManagerT, TimeDependencyT>;


        /*!
         * \brief Static method that performs the actual work.
         *
         * \param[in] param_at_dof Source parameter, from which the time dependency is taken.
         * \param[in,out] new_param Target parameter, which will get a copy of the time dependency after the
         * functions does its bidding.
         */
        static void Perform(const at_dof_type& param_at_dof, at_quad_pt_type& new_param);
    };


    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // clang-format off
    template
    <
       ParameterNS::Type TypeT,
       TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
       std::size_t NfeltSpaceT
    >
    // clang-format on
    struct CopyTimeDependency<TypeT, TimeManagerT, ::MoReFEM::ParameterNS::TimeDependencyNS::None, NfeltSpaceT>
    {


        using at_dof_type =
            ParameterAtDof<TypeT, TimeManagerT, ::MoReFEM::ParameterNS::TimeDependencyNS::None, NfeltSpaceT>;

        using at_quad_pt_type =
            ParameterAtQuadraturePoint<TypeT, TimeManagerT, ::MoReFEM::ParameterNS::TimeDependencyNS::None>;


        static void Perform(const at_dof_type& param_at_dof, at_quad_pt_type& new_param);
    };
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN


} // namespace MoReFEM::Internal::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "ParameterInstances/FromParameterAtDof/Internal/CopyTimeDependency.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FROMPARAMETERATDOF_INTERNAL_COPYTIMEDEPENDENCY_DOT_HPP_
// *** MoReFEM end header guards *** < //
