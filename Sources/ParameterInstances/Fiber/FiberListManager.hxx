// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_FIBER_FIBERLISTMANAGER_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_FIBER_FIBERLISTMANAGER_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/Fiber/FiberListManager.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "ParameterInstances/Fiber/FiberListManager.hpp"


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::FiberNS
{


    // clang-format off
    template
    <
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    const std::string& FiberListManager<FiberPolicyT, TypeT, TimeManagerT>::ClassName()
    {
        static const std::string ret(std::string("FiberListManager_") + ::MoReFEM::ParameterNS::Name<TypeT>());
        return ret;
    }


    // clang-format off
    template
    <
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    FiberListManager<FiberPolicyT, TypeT, TimeManagerT>::FiberListManager(const TimeManagerT& time_manager)
    : time_manager_(time_manager)
    {
        storage_.max_load_factor(Utilities::DefaultMaxLoadFactor());
    }


    // clang-format off
    template
    <
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    template
    <
        class IndexedSectionDescriptionT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void FiberListManager<FiberPolicyT, TypeT, TimeManagerT>::Create(const IndexedSectionDescriptionT&,
                                                                     const ModelSettingsT& model_settings,
                                                                     const InputDataT& input_data)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        decltype(auto) ensight_file = FilesystemNS::File{ std::filesystem::path{
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::EnsightFile>(model_settings, input_data) } };

        decltype(auto) domain_index =
            DomainNS::unique_id{ ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::DomainIndex>(model_settings,
                                                                                                         input_data) };
        decltype(auto) felt_space_unique_id = FEltSpaceNS::unique_id{
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::FEltSpaceIndex>(model_settings, input_data)
        };
        decltype(auto) unknown_name =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::UnknownName>(model_settings, input_data);

        const auto& domain = DomainManager::GetInstance().GetDomain(domain_index);

        decltype(auto) mesh = domain.GetMesh();

        const auto& god_of_dof = GodOfDofManager::GetInstance().GetGodOfDof(mesh.GetUniqueId());

        const auto& felt_space = god_of_dof.GetFEltSpace(felt_space_unique_id);
        const auto& unknown = UnknownManager::GetInstance().GetUnknown(unknown_name);

        Create(FiberListNS::unique_id{ section_type::GetUniqueId() }, ensight_file, domain, felt_space, unknown);
    }


    // clang-format off
    template
    <
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    void FiberListManager<FiberPolicyT, TypeT, TimeManagerT>::Create(const FiberListNS::unique_id unique_id,
                                                                     const FilesystemNS::File& fiber_file,
                                                                     const Domain& domain,
                                                                     const FEltSpace& felt_space,
                                                                     const Unknown& unknown)
    {
        // make_unique is not accepted here: it makes the code yell about private status of the constructor
        // with both clang and gcc.
        fiber_list_type* buf =
            new fiber_list_type(unique_id, fiber_file, domain, felt_space, GetTimeManager(), unknown);

        auto&& ptr = typename fiber_list_type::unique_ptr(buf);

        assert(ptr->GetUniqueId() == unique_id);

        auto&& pair = std::make_pair(unique_id, std::move(ptr));

        auto& storage = GetNonCstStorage();

        auto insert_return_value = storage.insert(std::move(pair));

        if (!insert_return_value.second)
            throw Exception("Two fiber lists objects can't share the same unique identifier! (namely "
                            + std::to_string(unique_id.Get()) + ").");
    }


    // clang-format off
    template
    <
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    const FiberList<FiberPolicyT, TypeT, TimeManagerT>&
    FiberListManager<FiberPolicyT, TypeT, TimeManagerT>::GetFiberList(FiberListNS::unique_id unique_id) const
    {
        decltype(auto) ptr = GetFiberListPtr(unique_id);
        return *ptr;
    }


    // clang-format off
    template
    <
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    auto FiberListManager<FiberPolicyT, TypeT, TimeManagerT>::GetFiberListPtr(FiberListNS::unique_id unique_id) const
        -> const fiber_list_type*
    {
        decltype(auto) storage = GetStorage();
        auto it = storage.find(unique_id);

        assert(it != storage.cend());
        assert(!(!(it->second)));

        return it->second.get();
    }


    // clang-format off
    template
    <
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    FiberList<FiberPolicyT, TypeT, TimeManagerT>&
    FiberListManager<FiberPolicyT, TypeT, TimeManagerT>::GetNonCstFiberList(FiberListNS::unique_id unique_id)
    {
        return const_cast<FiberList<FiberPolicyT, TypeT, TimeManagerT>&>(GetFiberList(unique_id));
    }


    // clang-format off
    template
    <
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    const TimeManagerT& FiberListManager<FiberPolicyT, TypeT, TimeManagerT>::GetTimeManager() const noexcept
    {
        return time_manager_;
    }


    // clang-format off
    template
    <
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    auto FiberListManager<FiberPolicyT, TypeT, TimeManagerT>::GetStorage() const noexcept -> const storage_type&
    {
        return storage_;
    }


    // clang-format off
    template
    <
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    auto FiberListManager<FiberPolicyT, TypeT, TimeManagerT>::GetNonCstStorage() noexcept -> storage_type&
    {
        return const_cast<storage_type&>(GetStorage());
    }


    // clang-format off
    template
    <
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    void FiberListManager<FiberPolicyT, TypeT, TimeManagerT>::Clear()
    {
        GetNonCstStorage().clear();
        fiber_list_type::ClearUniqueIdList();
    }


} // namespace MoReFEM::FiberNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_FIBER_FIBERLISTMANAGER_DOT_HXX_
// *** MoReFEM end header guards *** < //
