// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_COMPOUND_THREEDIMENSIONALPARAMETER_THREEDIMENSIONALCOMPOUNDPARAMETER_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_COMPOUND_THREEDIMENSIONALPARAMETER_THREEDIMENSIONALCOMPOUNDPARAMETER_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/Compound/ThreeDimensionalParameter/ThreeDimensionalCompoundParameter.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::ParameterNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<Type, TIME_MANAGER_TEMPLATE_KEYWORD>
             class TimeDependencyT>
    void ThreeDimensionalCoumpoundParameter<TimeManagerT, TimeDependencyT>::SupplWrite(std::ostream& out) const
    {
        out << "# This parameter is defined from three scalar parameters:" << std::endl;
        out << std::endl;
        GetScalarParameterX().Write(out);
        out << std::endl;
        GetScalarParameterY().Write(out);
        out << std::endl;
        GetScalarParameterZ().Write(out);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<Type, TIME_MANAGER_TEMPLATE_KEYWORD>
             class TimeDependencyT>
    template<class T>
    ThreeDimensionalCoumpoundParameter<TimeManagerT, TimeDependencyT>::ThreeDimensionalCoumpoundParameter(
        T&& name,
        scalar_parameter_no_time_dep_ptr&& x_component,
        scalar_parameter_no_time_dep_ptr&& y_component,
        scalar_parameter_no_time_dep_ptr&& z_component)
    : parent(std::forward<T>(name), x_component->GetDomain()), scalar_parameter_x_(std::move(x_component)),
      scalar_parameter_y_(std::move(y_component)), scalar_parameter_z_(std::move(z_component))
    {
        static_assert(std::is_convertible<self*, parent*>());

        assert(parent::GetDomain() == GetScalarParameterY().GetDomain());
        assert(parent::GetDomain() == GetScalarParameterZ().GetDomain());

        content_.resize({ 3 });

        if (IsConstant())
        {
            content_(0) = GetScalarParameterX().GetConstantValue();
            content_(1) = GetScalarParameterY().GetConstantValue();
            content_(2) = GetScalarParameterZ().GetConstantValue();
        }


        // clang-format off
        // This block is just a static sanity check.
        {
            using scalar_param_type = decltype(GetScalarParameterX());

            static_assert(std::is_same
                          <
                              scalar_param_type,
                              decltype(GetScalarParameterY())
                          >());

            static_assert(std::is_same
                          <
                              scalar_param_type,
                              decltype(GetScalarParameterZ())
                          >());

            using attribute_time_dep = typename std::remove_reference_t<scalar_param_type>::time_dependency_type;

            using expected_time_dep =
                TimeDependencyNS::None
                <
                    Type::scalar,
                    TimeManagerT
                >;

            static_assert(std::is_same
                          <
                              expected_time_dep,
                              attribute_time_dep
                          >(),
                          "This class has been designed so that time dependency is handled "
                          "at a global level, not for each of its scalar parameters. If you "
                          "get this message some refactoring concerning this class was "
                          "performed; please check with library developer - either the "
                          "refactoring entails this static check is no longer warranted, "
                          "or the refactoring didn't fully grasp how the class works in "
                          "the first place.");
        }
        // clang-format on
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<Type, TIME_MANAGER_TEMPLATE_KEYWORD>
             class TimeDependencyT>
    inline auto ThreeDimensionalCoumpoundParameter<TimeManagerT, TimeDependencyT>::GetScalarParameterX() const
        -> scalar_parameter_no_time_dep&
    {
        assert(!(!scalar_parameter_x_));
        return *scalar_parameter_x_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<Type, TIME_MANAGER_TEMPLATE_KEYWORD>
             class TimeDependencyT>
    inline auto ThreeDimensionalCoumpoundParameter<TimeManagerT, TimeDependencyT>::GetScalarParameterY() const
        -> scalar_parameter_no_time_dep&
    {
        assert(!(!scalar_parameter_y_));
        return *scalar_parameter_y_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<Type, TIME_MANAGER_TEMPLATE_KEYWORD>
             class TimeDependencyT>
    inline auto ThreeDimensionalCoumpoundParameter<TimeManagerT, TimeDependencyT>::GetScalarParameterZ() const
        -> scalar_parameter_no_time_dep&
    {
        assert(!(!scalar_parameter_z_));
        return *scalar_parameter_z_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<Type, TIME_MANAGER_TEMPLATE_KEYWORD>
             class TimeDependencyT>
    inline bool ThreeDimensionalCoumpoundParameter<TimeManagerT, TimeDependencyT>::IsConstant() const
    {
        return GetScalarParameterX().IsConstant() && GetScalarParameterY().IsConstant()
               && GetScalarParameterZ().IsConstant();
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<Type, TIME_MANAGER_TEMPLATE_KEYWORD>
             class TimeDependencyT>
    inline typename ThreeDimensionalCoumpoundParameter<TimeManagerT, TimeDependencyT>::return_type
    ThreeDimensionalCoumpoundParameter<TimeManagerT, TimeDependencyT>::SupplGetConstantValue() const
    {
        return content_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<Type, TIME_MANAGER_TEMPLATE_KEYWORD>
             class TimeDependencyT>
    inline typename ThreeDimensionalCoumpoundParameter<TimeManagerT, TimeDependencyT>::return_type
    ThreeDimensionalCoumpoundParameter<TimeManagerT, TimeDependencyT>::SupplGetValue(
        const local_coords_type& local_coords,
        const GeometricElt& geom_elt) const
    {
        content_(0) = GetScalarParameterX().GetValue(local_coords, geom_elt);
        content_(1) = GetScalarParameterY().GetValue(local_coords, geom_elt);
        content_(2) = GetScalarParameterZ().GetValue(local_coords, geom_elt);

        return content_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<Type, TIME_MANAGER_TEMPLATE_KEYWORD>
             class TimeDependencyT>
    void ThreeDimensionalCoumpoundParameter<TimeManagerT, TimeDependencyT>::SupplTimeUpdate()
    {
        GetScalarParameterX().TimeUpdate();
        GetScalarParameterY().TimeUpdate();
        GetScalarParameterZ().TimeUpdate();
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<Type, TIME_MANAGER_TEMPLATE_KEYWORD>
             class TimeDependencyT>
    void ThreeDimensionalCoumpoundParameter<TimeManagerT, TimeDependencyT>::SupplTimeUpdate(double time)
    {
        GetScalarParameterX().TimeUpdate(time);
        GetScalarParameterY().TimeUpdate(time);
        GetScalarParameterZ().TimeUpdate(time);
    }

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<Type, TIME_MANAGER_TEMPLATE_KEYWORD>
             class TimeDependencyT>
    auto ThreeDimensionalCoumpoundParameter<TimeManagerT, TimeDependencyT>::SupplGetAnyValue() const -> return_type
    {
        return content_;
    }

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
             template<Type, TIME_MANAGER_TEMPLATE_KEYWORD>
             class TimeDependencyT>
    inline void ThreeDimensionalCoumpoundParameter<TimeManagerT, TimeDependencyT>::SetConstantValue(value_type value)
    {
        static_cast<void>(value);
        assert(false && "SetConstantValue() meaningless for current Parameter.");
        exit(EXIT_FAILURE);
    }


} // namespace MoReFEM::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_COMPOUND_THREEDIMENSIONALPARAMETER_THREEDIMENSIONALCOMPOUNDPARAMETER_DOT_HXX_
// *** MoReFEM end header guards *** < //
