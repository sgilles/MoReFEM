// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup ParameterInstancesGroup
 * \addtogroup ParameterInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_PARAMETERINSTANCES_COMPOUND_INTERNALVARIABLE_ANALYTICALPRESTRESS_INPUTANALYTICALPRESTRESS_DOT_HXX_
#define MOREFEM_PARAMETERINSTANCES_COMPOUND_INTERNALVARIABLE_ANALYTICALPRESTRESS_INPUTANALYTICALPRESTRESS_DOT_HXX_
// IWYU pragma: private, include "ParameterInstances/Compound/InternalVariable/AnalyticalPrestress/InputAnalyticalPrestress.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "ParameterInstances/Compound/InternalVariable/AnalyticalPrestress/InputAnalyticalPrestress.hpp"


namespace MoReFEM::Advanced::ParameterInstancesNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS
{

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT>
    InputAnalyticalPrestress<TimeManagerT>::InputAnalyticalPrestress(const MoReFEMDataT& morefem_data,
                                                                     const Domain& domain)
    : initial_value_internal_variable_(
          ::MoReFEM::InputDataNS::ExtractLeaf<
              ::MoReFEM::InputDataNS::AnalyticalPrestress::InitialCondition::ActiveStress>(morefem_data))
    {
        using type = ::MoReFEM::InputDataNS::AnalyticalPrestress::Contractility;

        contractility_ = InitScalarParameterFromInputData<type, TimeManagerT>("Contractility", domain, morefem_data);
    };


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto
    InputAnalyticalPrestress<TimeManagerT>::GetContractility() const noexcept -> const scalar_parameter_type&
    {
        assert(!(!contractility_));
        return *contractility_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline double InputAnalyticalPrestress<TimeManagerT>::GetInitialValueInternalVariable() const noexcept
    {
        return initial_value_internal_variable_;
    }


} // namespace MoReFEM::Advanced::ParameterInstancesNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup ParameterInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_PARAMETERINSTANCES_COMPOUND_INTERNALVARIABLE_ANALYTICALPRESTRESS_INPUTANALYTICALPRESTRESS_DOT_HXX_
// *** MoReFEM end header guards *** < //
