// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_EXCEPTIONS_RESTART_DOT_HPP_
#define MOREFEM_FORMULATIONSOLVER_EXCEPTIONS_RESTART_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::ExceptionNS::FormulationSolverNS::RestartNS
{


    //! When the loading of data fails.
    struct InexistantFile : public MoReFEM::Exception
    {
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] file File that was not found
         * \copydoc doxygen_hide_source_location

         */
        explicit InexistantFile(const FilesystemNS::File& file,
                                const std::source_location location = std::source_location::current());


        //! Destructor.
        virtual ~InexistantFile() override;

        //! \copydoc doxygen_hide_copy_constructor
        InexistantFile(const InexistantFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        InexistantFile(InexistantFile&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        InexistantFile& operator=(const InexistantFile& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        InexistantFile& operator=(InexistantFile&& rhs) = delete;
    };


} // namespace MoReFEM::Advanced::ExceptionNS::FormulationSolverNS::RestartNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_EXCEPTIONS_RESTART_DOT_HPP_
// *** MoReFEM end header guards *** < //
