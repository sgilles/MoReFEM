// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_SNES_SOLVER_DOT_HXX_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_SNES_SOLVER_DOT_HXX_
// IWYU pragma: private, include "FormulationSolver/Internal/Snes/Solver.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "FormulationSolver/Internal/Snes/Solver.hpp"


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::SolverNS
{


    // clang-format off
    template
    <
        std::size_t SolverIndexT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    ::MoReFEM::Wrappers::Petsc::Snes::unique_ptr
    InitSolver(const MoReFEMDataT& morefem_data,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESFunction snes_function,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESJacobian snes_jacobian,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESViewer snes_viewer,
               ::MoReFEM::Wrappers::Petsc::Snes::SNESConvergenceTestFunction snes_convergence_test_function)
    {
        namespace input = ::MoReFEM::InputDataNS;

        const auto absolute_tolerance =
            input::ExtractLeaf<typename input::Petsc<SolverIndexT>::AbsoluteTolerance>(morefem_data);
        const auto relative_tolerance =
            input::ExtractLeaf<typename input::Petsc<SolverIndexT>::RelativeTolerance>(morefem_data);
        const auto gmres_restart = input::ExtractLeaf<typename input::Petsc<SolverIndexT>::GmresRestart>(morefem_data);
        const auto max_iteration = input::ExtractLeaf<typename input::Petsc<SolverIndexT>::MaxIteration>(morefem_data);
        const auto preconditioner_name =
            input::ExtractLeaf<typename input::Petsc<SolverIndexT>::Preconditioner>(morefem_data);
        const auto solver_name = input::ExtractLeaf<typename input::Petsc<SolverIndexT>::Solver>(morefem_data);
        const auto step_size_tolerance =
            input::ExtractLeaf<typename input::Petsc<SolverIndexT>::StepSizeTolerance>(morefem_data);

        Internal::Wrappers::Petsc::SolverNS::Settings solver_settings(absolute_tolerance,
                                                                      relative_tolerance,
                                                                      gmres_restart,
                                                                      max_iteration,
                                                                      preconditioner_name,
                                                                      solver_name,
                                                                      step_size_tolerance);

        return std::make_unique<::MoReFEM::Wrappers::Petsc::Snes>(morefem_data.GetMpi(),
                                                                  std::move(solver_settings),
                                                                  snes_function,
                                                                  snes_jacobian,
                                                                  snes_viewer,
                                                                  snes_convergence_test_function);
    }


} // namespace MoReFEM::Internal::SolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_SNES_SOLVER_DOT_HXX_
// *** MoReFEM end header guards *** < //
