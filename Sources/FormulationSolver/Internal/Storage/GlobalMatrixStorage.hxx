// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_STORAGE_GLOBALMATRIXSTORAGE_DOT_HXX_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_STORAGE_GLOBALMATRIXSTORAGE_DOT_HXX_
// IWYU pragma: private, include "FormulationSolver/Internal/Storage/GlobalMatrixStorage.hpp"
// *** MoReFEM header guards *** < //


#include "Core/LinearAlgebra/GlobalMatrix.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class NumberingSubset; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::VarfNS
{


    inline GlobalMatrix& GlobalMatrixStorage::GetNonCstMatrix(const NumberingSubset& row_numbering_subset,
                                                              const NumberingSubset& col_numbering_subset,
                                                              const std::source_location location)
    {
        return const_cast<GlobalMatrix&>(GetMatrix(row_numbering_subset, col_numbering_subset, location));
    }


    inline const GlobalMatrix::vector_unique_ptr& GlobalMatrixStorage ::GetStorage() const
    {
        return storage_;
    }


    inline GlobalMatrix::vector_unique_ptr& GlobalMatrixStorage ::GetNonCstStorage()
    {
        return storage_;
    }


} // namespace MoReFEM::Internal::VarfNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_STORAGE_GLOBALMATRIXSTORAGE_DOT_HXX_
// *** MoReFEM end header guards *** < //
