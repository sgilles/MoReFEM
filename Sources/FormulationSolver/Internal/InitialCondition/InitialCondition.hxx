// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INITIALCONDITION_DOT_HXX_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INITIALCONDITION_DOT_HXX_
// IWYU pragma: private, include "FormulationSolver/Internal/InitialCondition/InitialCondition.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Internal::FormulationSolverNS
{


    template<ParameterNS::Type TypeT>
    InitialCondition<TypeT>::InitialCondition(const Mesh& mesh) : mesh_(mesh)
    { }


    template<ParameterNS::Type TypeT>
    inline typename InitialCondition<TypeT>::return_type InitialCondition<TypeT>::GetConstantValue() const
    {
        assert(IsConstant() && "This method is relevant only for spatially constant parameters.");

        return SupplGetConstantValue();
    }


    template<ParameterNS::Type TypeT>
    inline typename InitialCondition<TypeT>::return_type
    InitialCondition<TypeT>::GetValue(const SpatialPoint& coords) const
    {
        if (IsConstant())
            return GetConstantValue();

        return SupplGetValue(coords);
    }


    template<ParameterNS::Type TypeT>
    inline const Mesh& InitialCondition<TypeT>::GetMesh() const noexcept
    {
        return mesh_;
    }


} // namespace MoReFEM::Internal::FormulationSolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INITIALCONDITION_DOT_HXX_
// *** MoReFEM end header guards *** < //
