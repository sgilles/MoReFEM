// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INSTANCES_THREEDIMENSIONALINITIALCONDITION_DOT_HXX_
#define MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INSTANCES_THREEDIMENSIONALINITIALCONDITION_DOT_HXX_
// IWYU pragma: private, include "FormulationSolver/Internal/InitialCondition/Instances/ThreeDimensionalInitialCondition.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "FormulationSolver/Internal/InitialCondition/Instances/ThreeDimensionalInitialCondition.hpp"


#include <cassert>

#include "Utilities/LinearAlgebra/LocalAlias.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class SpatialPoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::FormulationSolverNS
{


    inline ThreeDimensionalInitialCondition::scalar_initial_condition&
    ThreeDimensionalInitialCondition ::GetScalarInitialConditionX() const noexcept
    {
        assert(!(!scalar_initial_condition_x_));
        return *scalar_initial_condition_x_;
    }


    inline ThreeDimensionalInitialCondition::scalar_initial_condition&
    ThreeDimensionalInitialCondition ::GetScalarInitialConditionY() const noexcept
    {
        assert(!(!scalar_initial_condition_y_));
        return *scalar_initial_condition_y_;
    }


    inline ThreeDimensionalInitialCondition::scalar_initial_condition&
    ThreeDimensionalInitialCondition ::GetScalarInitialConditionZ() const noexcept
    {
        assert(!(!scalar_initial_condition_z_));
        return *scalar_initial_condition_z_;
    }


    inline bool ThreeDimensionalInitialCondition::IsConstant() const
    {
        return GetScalarInitialConditionX().IsConstant() && GetScalarInitialConditionY().IsConstant()
               && GetScalarInitialConditionZ().IsConstant();
    }


    inline ThreeDimensionalInitialCondition::return_type
    ThreeDimensionalInitialCondition ::SupplGetConstantValue() const
    {
        return content_;
    }


    inline const LocalVector& ThreeDimensionalInitialCondition::SupplGetValue(const SpatialPoint& coords) const
    {
        content_(0) = GetScalarInitialConditionX().GetValue(coords);
        content_(1) = GetScalarInitialConditionY().GetValue(coords);
        content_(2) = GetScalarInitialConditionZ().GetValue(coords);

        return content_;
    }


} // namespace MoReFEM::Internal::FormulationSolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_INTERNAL_INITIALCONDITION_INSTANCES_THREEDIMENSIONALINITIALCONDITION_DOT_HXX_
// *** MoReFEM end header guards *** < //
