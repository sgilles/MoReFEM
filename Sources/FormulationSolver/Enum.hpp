// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_ENUM_DOT_HPP_
#define MOREFEM_FORMULATIONSOLVER_ENUM_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM
{


    /*!
     * \brief Whether the matrix has already been factorized or not.
     *
     * If yes, there is a huge speed-up in the solver, but you must be sure the matrix is still the same.
     * So in the first call, the value must be 'no'.
     */
    enum class IsFactorized { yes, no };

    //! Whether volumic or surfacic source is considered.
    enum class SourceType { volumic, surfacic };


} // namespace MoReFEM


namespace MoReFEM::VariationalFormulationNS
{


    /*!
     * \brief Specify whether boundary conditions are applied on system rhs, system matrix or both.
     */
    enum class On { system_matrix, system_rhs, system_matrix_and_rhs };


} // namespace MoReFEM::VariationalFormulationNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_ENUM_DOT_HPP_
// *** MoReFEM end header guards *** < //
