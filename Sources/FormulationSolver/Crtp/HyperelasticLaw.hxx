// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup FormulationSolverGroup
 * \addtogroup FormulationSolverGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_FORMULATIONSOLVER_CRTP_HYPERELASTICLAW_DOT_HXX_
#define MOREFEM_FORMULATIONSOLVER_CRTP_HYPERELASTICLAW_DOT_HXX_
// IWYU pragma: private, include "FormulationSolver/Crtp/HyperelasticLaw.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "FormulationSolver/Crtp/HyperelasticLaw.hpp"


namespace MoReFEM::FormulationSolverNS
{


    template<class DerivedT, class HyperelasticLawT>
    template<typename... Args>
    void HyperelasticLaw<DerivedT, HyperelasticLawT>::Create(Args&&... args)
    {
        assert(hyperelastic_law_ == nullptr && "Should be called only once!");

        hyperelastic_law_ = std::make_unique<HyperelasticLawT>(std::forward<decltype(args)>(args)...);
    }


    template<class DerivedT, class HyperelasticLawT>
    const HyperelasticLawT& HyperelasticLaw<DerivedT, HyperelasticLawT>::GetHyperelasticLaw() const noexcept
    {
        assert(!(!hyperelastic_law_));
        return *hyperelastic_law_;
    }


    template<class DerivedT, class HyperelasticLawT>
    const HyperelasticLawT* HyperelasticLaw<DerivedT, HyperelasticLawT>::GetHyperelasticLawPtr() const noexcept
    {
        assert(!(!hyperelastic_law_));
        return hyperelastic_law_.get();
    }


} // namespace MoReFEM::FormulationSolverNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup FormulationSolverGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_FORMULATIONSOLVER_CRTP_HYPERELASTICLAW_DOT_HXX_
// *** MoReFEM end header guards *** < //
