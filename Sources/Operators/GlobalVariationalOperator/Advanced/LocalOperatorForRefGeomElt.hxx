// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_ADVANCED_LOCALOPERATORFORREFGEOMELT_DOT_HXX_
#define MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_ADVANCED_LOCALOPERATORFORREFGEOMELT_DOT_HXX_
// IWYU pragma: private, include "Operators/GlobalVariationalOperator/Advanced/LocalOperatorForRefGeomElt.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Operators/GlobalVariationalOperator/Advanced/LocalOperatorForRefGeomElt.hpp"


namespace MoReFEM::Advanced::GlobalVariationalOperatorNS
{


    template<Advanced::GeometricEltEnum RefGeomEltTypeT, class LocalVariationalOperatorPtrT>
    constexpr Advanced::GeometricEltEnum
    LocalOperatorForRefGeomElt<RefGeomEltTypeT, LocalVariationalOperatorPtrT>::RefGeomEltType()
    {
        return RefGeomEltTypeT;
    }


    template<Advanced::GeometricEltEnum RefGeomEltTypeT, class LocalVariationalOperatorPtrT>
    void LocalOperatorForRefGeomElt<RefGeomEltTypeT, LocalVariationalOperatorPtrT>::SetLocalOperator(
        LocalVariationalOperatorPtrT&& ptr) noexcept
    {
        assert(local_operator_ == nullptr && "Should be initialized only once!");

        local_operator_ = std::move(ptr);
    }


    template<Advanced::GeometricEltEnum RefGeomEltTypeT, class LocalVariationalOperatorPtrT>
    bool LocalOperatorForRefGeomElt<RefGeomEltTypeT, LocalVariationalOperatorPtrT>::IsRelevant() const noexcept
    {
        return (local_operator_ != nullptr);
    }


    template<Advanced::GeometricEltEnum RefGeomEltTypeT, class LocalVariationalOperatorPtrT>
    typename LocalOperatorForRefGeomElt<RefGeomEltTypeT, LocalVariationalOperatorPtrT>::local_operator_type&
    LocalOperatorForRefGeomElt<RefGeomEltTypeT, LocalVariationalOperatorPtrT>::GetNonCstLocalOperator() const noexcept
    {
        assert(!(!local_operator_));
        return *local_operator_;
    }


    template<Advanced::GeometricEltEnum RefGeomEltTypeT, class LocalVariationalOperatorPtrT>
    constexpr Advanced::OperatorNS::Nature
    LocalOperatorForRefGeomElt<RefGeomEltTypeT, LocalVariationalOperatorPtrT>::OperatorNature()
    {
        return local_operator_type::OperatorNature();
    }


    template<Advanced::GeometricEltEnum RefGeomEltTypeT>
    constexpr Advanced::GeometricEltEnum LocalOperatorForRefGeomElt<RefGeomEltTypeT, std::nullptr_t>::RefGeomEltType()
    {
        return RefGeomEltTypeT;
    }


} // namespace MoReFEM::Advanced::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_ADVANCED_LOCALOPERATORFORREFGEOMELT_DOT_HXX_
// *** MoReFEM end header guards *** < //
