// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_ADVANCED_DETERMINEEXTENDEDUNKNOWNLIST_DOT_HXX_
#define MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_ADVANCED_DETERMINEEXTENDEDUNKNOWNLIST_DOT_HXX_
// IWYU pragma: private, include "Operators/GlobalVariationalOperator/Advanced/DetermineExtendedUnknownList.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Operators/GlobalVariationalOperator/Advanced/DetermineExtendedUnknownList.hpp"


#include <array>
#include <cstddef> // IWYU pragma: keep

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"
#include "FiniteElement/Unknown/ExtendedUnknown.hpp"


namespace MoReFEM::Advanced::GlobalVariationalOperatorNS
{


    template<std::size_t NunknownsT>
    ExtendedUnknown::vector_const_shared_ptr
    DetermineExtendedUnknownList(const FEltSpace& felt_space,
                                 const std::array<Unknown::const_shared_ptr, NunknownsT>& unknown_list)
    {
        ExtendedUnknown::vector_const_shared_ptr ret;

        for (std::size_t i = 0; i < NunknownsT; ++i)
        {
            const auto& unknown_ptr = unknown_list[i];
            ret.push_back(felt_space.GetExtendedUnknownPtr(*unknown_ptr));
        }

        return ret;
    }


} // namespace MoReFEM::Advanced::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_GLOBALVARIATIONALOPERATOR_ADVANCED_DETERMINEEXTENDEDUNKNOWNLIST_DOT_HXX_
// *** MoReFEM end header guards *** < //
