// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_PARAMETEROPERATOR_LOCALPARAMETEROPERATOR_LOCALPARAMETEROPERATOR_DOT_HXX_
#define MOREFEM_OPERATORS_PARAMETEROPERATOR_LOCALPARAMETEROPERATOR_LOCALPARAMETEROPERATOR_DOT_HXX_
// IWYU pragma: private, include "Operators/ParameterOperator/LocalParameterOperator/LocalParameterOperator.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Operators/ParameterOperator/LocalParameterOperator/LocalParameterOperator.hpp"


namespace MoReFEM::Advanced
{


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    LocalParameterOperator<TypeT, TimeManagerT, TimeDependencyT>::LocalParameterOperator(
        const ExtendedUnknown::const_shared_ptr& unknown,
        elementary_data_type&& elementary_data,
        ParameterAtQuadraturePoint<TypeT, TimeManagerT, TimeDependencyT>& parameter)
    : elementary_data_(std::move(elementary_data)), parameter_(parameter), extended_unknown_(unknown)
    { }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline const typename LocalParameterOperator<TypeT, TimeManagerT, TimeDependencyT>::elementary_data_type&
    LocalParameterOperator<TypeT, TimeManagerT, TimeDependencyT>::GetElementaryData() const
    {
        return elementary_data_;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline typename LocalParameterOperator<TypeT, TimeManagerT, TimeDependencyT>::elementary_data_type&
    LocalParameterOperator<TypeT, TimeManagerT, TimeDependencyT>::GetNonCstElementaryData()
    {
        return elementary_data_;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline auto LocalParameterOperator<TypeT, TimeManagerT, TimeDependencyT>::GetParameter() const noexcept
        -> const param_at_quad_pt_type&
    {
        return parameter_;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline auto
    LocalParameterOperator<TypeT, TimeManagerT, TimeDependencyT>::GetNonCstParameter() -> param_at_quad_pt_type&
    {
        return parameter_;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline void LocalParameterOperator<TypeT, TimeManagerT, TimeDependencyT>::SetLocalFEltSpace(
        const LocalFEltSpace& local_felt_space)
    {
        auto& elementary_data = GetNonCstElementaryData();
        elementary_data.ComputeLocalFEltSpaceData(local_felt_space);
        elementary_data.Zero();
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline const ExtendedUnknown::const_shared_ptr&
    LocalParameterOperator<TypeT, TimeManagerT, TimeDependencyT>::GetExtendedUnknownPtr() const
    {
        assert(!(!extended_unknown_));
        return extended_unknown_;
    }


    // clang-format off
    template
    <
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline const ExtendedUnknown&
    LocalParameterOperator<TypeT, TimeManagerT, TimeDependencyT>::GetExtendedUnknown() const
    {
        assert(!(!extended_unknown_));
        return *extended_unknown_;
    }


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_PARAMETEROPERATOR_LOCALPARAMETEROPERATOR_LOCALPARAMETEROPERATOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
