// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_PARAMETEROPERATOR_GLOBALPARAMETEROPERATOR_GLOBALPARAMETEROPERATOR_DOT_HXX_
#define MOREFEM_OPERATORS_PARAMETEROPERATOR_GLOBALPARAMETEROPERATOR_GLOBALPARAMETEROPERATOR_DOT_HXX_
// IWYU pragma: private, include "Operators/ParameterOperator/GlobalParameterOperator/GlobalParameterOperator.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM
{


    // clang-format off
    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    template<typename... Args>
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeManagerT, TimeDependencyT>::
        GlobalParameterOperator(const FEltSpace& felt_space,
                                const Unknown& unknown,
                                const QuadratureRulePerTopology* const quadrature_rule_per_topology,
                                AllocateGradientFEltPhi do_allocate_gradient_felt_phi,
                                ParameterAtQuadraturePoint<TypeT, TimeManagerT, TimeDependencyT>& parameter,
                                Args&&... args)
    : felt_space_(felt_space), quadrature_rule_per_topology_(quadrature_rule_per_topology), parameter_(parameter),
      do_allocate_gradient_felt_phi_(do_allocate_gradient_felt_phi),
      extended_unknown_(felt_space.GetExtendedUnknownPtr(unknown))
    {
        local_operator_per_ref_geom_elt_.max_load_factor(Utilities::DefaultMaxLoadFactor());

        CreateLocalOperatorList(felt_space.GetMeshDimension(), parameter, std::forward<decltype(args)>(args)...);
    }


    // clang-format off
    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline const FEltSpace&
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeManagerT, TimeDependencyT>::GetFEltSpace()
        const noexcept
    {
        return felt_space_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    template<typename... Args>
    void GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeManagerT, TimeDependencyT>::UpdateImpl(
        Args&&... args) const
    {
        const auto& felt_storage =
            this->GetFEltSpace().template GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>();

        // \todo #1586: just processor-wise is to check... Anyway, if ghost it would be best to exchange information
        // through mpi rather than recomputing (two ranks might not compute exactly the same result...) It is highly
        // possible that Parameter just needs processor-wise - but in this case it should be at the very least
        // explicitly documented!

        for (const auto& [ref_felt_space_ptr, local_felt_space_list] : felt_storage)
        {
            assert(!(!ref_felt_space_ptr));
            const auto& ref_felt_space = *ref_felt_space_ptr;

            const auto& ref_geom_elt = ref_felt_space.GetRefGeomElt();
            const auto ref_geom_elt_id = ref_geom_elt.GetIdentifier();

            if (!DoConsider(ref_geom_elt_id))
                continue;

            auto& local_operator = GetNonCstLocalOperator(ref_geom_elt_id);

            for (const auto& local_felt_space_pair : local_felt_space_list)
            {
                const auto& local_felt_space_ptr = local_felt_space_pair.second;
                assert(!(!local_felt_space_ptr));

                auto& local_felt_space = *local_felt_space_ptr;
                local_operator.SetLocalFEltSpace(local_felt_space);
                PerformElementaryCalculation(local_felt_space, local_operator, std::forward_as_tuple(args...));
            }
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    LocalParameterOperatorT&
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeManagerT, TimeDependencyT>::
        GetNonCstLocalOperator(Advanced::GeometricEltEnum ref_geom_elt_id) const
    {
        auto it = local_operator_per_ref_geom_elt_.find(ref_geom_elt_id);
        assert(it != local_operator_per_ref_geom_elt_.cend());
        return *(it->second);
    }


    // clang-format off
    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    bool GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeManagerT, TimeDependencyT>::DoConsider(
        Advanced::GeometricEltEnum ref_geom_elt_id) const
    {
        auto it = local_operator_per_ref_geom_elt_.find(ref_geom_elt_id);
        return it != local_operator_per_ref_geom_elt_.cend();
    }


    // clang-format off
    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    template<typename... Args>
    void GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeManagerT, TimeDependencyT>::
        PerformElementaryCalculation(const LocalFEltSpace& local_felt_space,
                                     LocalParameterOperatorT& local_operator,
                                     std::tuple<Args...>&& additional_arguments) const
    {
        static_assert(std::tuple_size<std::tuple<Args...>>::value > 0,
                      "If no supplementary arguments, the overload of current method should have been called. It is "
                      "likely it was not because last template parameter in LocalParameterOperatorT "
                      "was not set to std::false_type.");

        static_cast<const DerivedT&>(*this).SetComputeEltArrayArguments(
            local_felt_space, local_operator, std::move(additional_arguments));

        local_operator.ComputeEltArray();
    }


    // clang-format off
    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    void GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeManagerT, TimeDependencyT>::
        PerformElementaryCalculation(const LocalFEltSpace& local_felt_space,
                                     LocalParameterOperatorT& local_operator,
                                     std::tuple<>&&) const
    {
        static_cast<void>(local_felt_space);

        local_operator.ComputeEltArray();
    }


    // clang-format off
    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    template<typename... Args>
    void GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeManagerT, TimeDependencyT>::
        CreateLocalOperatorList(
            const std::size_t mesh_dimension,
            ParameterAtQuadraturePoint<TypeT, TimeManagerT, ParameterNS::TimeDependencyNS::None>& parameter,
            Args&&... args)
    {
        const auto& felt_space = GetFEltSpace();
        const auto& felt_storage =
            felt_space.template GetLocalFEltSpacePerRefLocalFEltSpace<RoleOnProcessor::processor_wise>();

        if (felt_storage.empty())
            std::cout
                << "[WARNING] Finite element space related to operator " << DerivedT::ClassName()
                << " is empty! "
                   "It might be due for instance to a non existing label for the dimension considered here (namely "
                << GetFEltSpace().GetDimension() << ")." << std::endl;

        using pair_type = typename decltype(local_operator_per_ref_geom_elt_)::value_type;

        const auto& unknown_storage = this->GetExtendedUnknownPtr();

        for (const auto& pair : felt_storage)
        {
            const auto& ref_felt_space_ptr = pair.first;
            assert(!(!ref_felt_space_ptr));
            const auto& ref_felt_space = *ref_felt_space_ptr;

            decltype(auto) ref_geom_elt = ref_felt_space.GetRefGeomElt();
            const auto ref_geom_elt_id = ref_geom_elt.GetIdentifier();

            assert(local_operator_per_ref_geom_elt_.find(ref_geom_elt_id) == local_operator_per_ref_geom_elt_.cend());

            const auto& quadrature_rule = GetQuadratureRule(ref_geom_elt);

            typename LocalParameterOperatorT::elementary_data_type elementary_data(ref_felt_space,
                                                                                   quadrature_rule,
                                                                                   { unknown_storage },
                                                                                   { unknown_storage },
                                                                                   felt_space.GetDimension(),
                                                                                   mesh_dimension,
                                                                                   DoAllocateGradientFEltPhi());

            auto&& local_operator_ptr = std::make_unique<LocalParameterOperatorT>(
                unknown_storage, std::move(elementary_data), parameter, std::forward<decltype(args)>(args)...);

            pair_type&& ret_pair{ ref_geom_elt_id, std::move(local_operator_ptr) };

            local_operator_per_ref_geom_elt_.insert(std::move(ret_pair));
        }
    }


    // clang-format off
    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline const QuadratureRule&
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeManagerT, TimeDependencyT>::GetQuadratureRule(
        const RefGeomElt& ref_geom_elt) const
    {
        return GetQuadratureRulePerTopology().GetRule(ref_geom_elt.GetTopologyIdentifier());
    }


    // clang-format off
    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    const QuadratureRulePerTopology&
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeManagerT, TimeDependencyT>::
        GetQuadratureRulePerTopology() const noexcept
    {
        return (quadrature_rule_per_topology_ == nullptr ? GetFEltSpace().GetQuadratureRulePerTopology()
                                                         : *quadrature_rule_per_topology_);
    }


    // clang-format off
    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    const Utilities::PointerComparison::Map<RefGeomElt::shared_ptr, typename LocalParameterOperatorT::unique_ptr>&
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeManagerT, TimeDependencyT>::
        GetLocalOperatorPerRefGeomElt() const noexcept
    {
        return local_operator_per_ref_geom_elt_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline const ParameterAtQuadraturePoint<TypeT, TimeManagerT, TimeDependencyT>&
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeManagerT, TimeDependencyT>::GetParameter()
        const noexcept
    {
        return parameter_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline AllocateGradientFEltPhi
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeManagerT, TimeDependencyT>::
        DoAllocateGradientFEltPhi() const noexcept
    {
        return do_allocate_gradient_felt_phi_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline const ExtendedUnknown::const_shared_ptr&
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeManagerT, TimeDependencyT>::
        GetExtendedUnknownPtr() const
    {
        assert(!(!extended_unknown_));
        return extended_unknown_;
    }


    // clang-format off
    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    // clang-format on
    inline const ExtendedUnknown&
    GlobalParameterOperator<DerivedT, LocalParameterOperatorT, TypeT, TimeManagerT, TimeDependencyT>::
        GetExtendedUnknown() const
    {
        assert(!(!extended_unknown_));
        return *extended_unknown_;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_PARAMETEROPERATOR_GLOBALPARAMETEROPERATOR_GLOBALPARAMETEROPERATOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
