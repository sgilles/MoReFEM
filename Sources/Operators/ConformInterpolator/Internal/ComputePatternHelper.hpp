// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_CONFORMINTERPOLATOR_INTERNAL_COMPUTEPATTERNHELPER_DOT_HPP_
#define MOREFEM_OPERATORS_CONFORMINTERPOLATOR_INTERNAL_COMPUTEPATTERNHELPER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <map>
#include <unordered_map>
#include <vector>

#include "Utilities/LinearAlgebra/LocalAlias.hpp"

#include "ThirdParty/IncludeWithoutWarning/Petsc/PetscSys.hpp"

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
#include "FiniteElement/Nodes_and_dofs/DofIndexesTypes.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Advanced::ConformInterpolatorNS { class InterpolationData; }
namespace MoReFEM::Internal::RefFEltNS { class RefLocalFEltSpace; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::ConformInterpolatorNS
{


    /*!
     * \brief Free function called by LagrangianInterpolator::ComputePattern().
     *
     * \param[in] ref_felt_space \a RefLocalFEltSpace for which pattern is computed.
     * \param[in] source_local_felt_space_list Key is the index of the geometric element, value the actual
     * pointer to the LocalFEltSpace.
     * \param[in] local_projection_matrix Local projection matrix.
     * \param[in] interpolation_data Interpolation data.
     * \param[in] map_pattern Map pattern.
     * \param[in] map_values Map values.
     */
    void ComputePatternFromRefGeomElt(
        const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
        const LocalFEltSpace::per_geom_elt_index& source_local_felt_space_list,
        const LocalMatrix& local_projection_matrix,
        const Advanced::ConformInterpolatorNS::InterpolationData& interpolation_data,
        std::map<::MoReFEM::DofNS::processor_wise_or_ghost_index, std::vector<PetscInt>>& map_pattern,
        std::unordered_map<::MoReFEM::DofNS::processor_wise_or_ghost_index, std::vector<PetscScalar>>& map_values);


} // namespace MoReFEM::Internal::ConformInterpolatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_CONFORMINTERPOLATOR_INTERNAL_COMPUTEPATTERNHELPER_DOT_HPP_
// *** MoReFEM end header guards *** < //
