// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Operators/ConformInterpolator/Advanced/InterpolationData.hpp"


namespace MoReFEM::Advanced::ConformInterpolatorNS
{


    InterpolationData::InterpolationData(SourceOrTargetData::unique_ptr&& source_data,
                                         SourceOrTargetData::unique_ptr&& target_data)
    : source_data_(std::move(source_data)), target_data_(std::move(target_data))
    { }


} // namespace MoReFEM::Advanced::ConformInterpolatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //
