// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_SYMMETRIZEMATRIX_DOT_HPP_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_SYMMETRIZEMATRIX_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/LinearAlgebra/LocalAlias.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief Symmetrize a \a LocalMatrix
     *
     * When Seldon was used to handle local linear algebra, it provided an handy matrix class for symmetric matrices:
     * only the upper half of the matrix was in fact stored in memory, and operations were overloaded knowing the
     * represented matrix was symmetric.
     *
     * To my knowledge, there is no such thing in Xtensor. However, some \a LocalOperator were written with the idea of
     * filling only the upper half.
     *
     * Current function is then to be called at the end of such symmetric operators: it will copy the values in the
     * lower half of the matrix (in debug mode, there is a check the lower half is properly populated with zeroes).
     *
     * \param[in,out] matrix The local matrix to symmetrize. In input, it is expected only the upper half of this matrix is filled with non zero values.
     */
    void SymmetrizeMatrix(LocalMatrix& matrix);


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_SYMMETRIZEMATRIX_DOT_HPP_
// *** MoReFEM end header guards *** < //
