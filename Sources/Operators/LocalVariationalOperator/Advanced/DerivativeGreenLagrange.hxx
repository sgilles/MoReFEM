// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_DERIVATIVEGREENLAGRANGE_DOT_HXX_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_DERIVATIVEGREENLAGRANGE_DOT_HXX_
// IWYU pragma: private, include "Operators/LocalVariationalOperator/Advanced/DerivativeGreenLagrange.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Operators/LocalVariationalOperator/Advanced/DerivativeGreenLagrange.hpp"


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    template<GreenLagrangeOrEta GreenLagrangeOrEtaT>
    inline const LocalMatrix& DerivativeGreenLagrange<GreenLagrangeOrEtaT>::GetMatrix() const noexcept
    {
        return matrix_;
    }


    template<GreenLagrangeOrEta GreenLagrangeOrEtaT>
    inline LocalMatrix& DerivativeGreenLagrange<GreenLagrangeOrEtaT>::GetNonCstMatrix() noexcept
    {
        return const_cast<LocalMatrix&>(GetMatrix());
    }


    template<GreenLagrangeOrEta GreenLagrangeOrEtaT>
    DerivativeGreenLagrange<GreenLagrangeOrEtaT>::DerivativeGreenLagrange(std::size_t mesh_dimension)
    : mesh_dimension_(mesh_dimension)
    {
        auto& matrix = GetNonCstMatrix();

        switch (mesh_dimension)
        {
        case 1u:
            matrix.resize({ 1, 1 });
            transposed_matrix_.resize({ 1, 1 });
            break;
        case 2u:
            matrix.resize({ 3, 4 });
            transposed_matrix_.resize({ 4, 3 });
            break;
        case 3u:
            matrix.resize({ 6, 9 });
            transposed_matrix_.resize({ 9, 6 });
            break;
        default:
            assert(false && "Only implemented for dimensions 2 and 3.");
            exit(EXIT_FAILURE);
        }

        matrix.fill(0.);
    }


    namespace // anonymous
    {

        template<GreenLagrangeOrEta GreenLagrangeOrEtaT>
        [[maybe_unused]] void Update1D(const LocalMatrix& gradient_displacement_matrix, LocalMatrix& result);

        template<GreenLagrangeOrEta GreenLagrangeOrEtaT>
        [[maybe_unused]] void Update2D(const LocalMatrix& gradient_displacement_matrix, LocalMatrix& result);

        template<GreenLagrangeOrEta GreenLagrangeOrEtaT>
        [[maybe_unused]] void Update3D(const LocalMatrix& gradient_displacement_matrix, LocalMatrix& result);


    } // namespace


    template<GreenLagrangeOrEta GreenLagrangeOrEtaT>
    const LocalMatrix&
    DerivativeGreenLagrange<GreenLagrangeOrEtaT>::Update(const LocalMatrix& gradient_displacement_matrix)
    {
        auto& ret = GetNonCstMatrix();

        switch (mesh_dimension_)
        {
        case 1u:
            Update1D<GreenLagrangeOrEtaT>(gradient_displacement_matrix, ret);
            return ret;
        case 2u:
            Update2D<GreenLagrangeOrEtaT>(gradient_displacement_matrix, ret);
            return ret;
        case 3u:
            Update3D<GreenLagrangeOrEtaT>(gradient_displacement_matrix, ret);
            return ret;
        default:
            assert(false && "Only implemented for dimensions 2 and 3.");
            exit(EXIT_FAILURE);
        }
    }


    template<GreenLagrangeOrEta GreenLagrangeOrEtaT>
    const LocalMatrix& DerivativeGreenLagrange<GreenLagrangeOrEtaT>::GetTransposed()
    {
        xt::noalias(transposed_matrix_) = xt::transpose(GetMatrix());
        return transposed_matrix_;
    }


    namespace // anonymous
    {


        template<GreenLagrangeOrEta GreenLagrangeOrEtaT>
        void Update1D(const LocalMatrix& gradient_displacement_matrix, LocalMatrix& result)
        {
            assert(gradient_displacement_matrix.shape(0) == 1);
            assert(gradient_displacement_matrix.shape(1) == 1);

            result(0, 0) = gradient_displacement_matrix(0, 0);

            if (GreenLagrangeOrEtaT == GreenLagrangeOrEta::green_lagrange)
            {
                result(0, 0) += 1.;
            }
        }


        template<GreenLagrangeOrEta GreenLagrangeOrEtaT>
        void Update2D(const LocalMatrix& gradient_displacement_matrix, LocalMatrix& result)
        {
            assert(gradient_displacement_matrix.shape(0) == 2);
            assert(gradient_displacement_matrix.shape(1) == 2);

            result(0, 0) = gradient_displacement_matrix(0, 0);
            result(0, 2) = gradient_displacement_matrix(1, 0);

            result(1, 1) = gradient_displacement_matrix(0, 1);
            result(1, 3) = gradient_displacement_matrix(1, 1);

            result(2, 0) = gradient_displacement_matrix(0, 1);
            result(2, 1) = gradient_displacement_matrix(0, 0);
            result(2, 2) = gradient_displacement_matrix(1, 1);
            result(2, 3) = gradient_displacement_matrix(1, 0);

            if (GreenLagrangeOrEtaT == GreenLagrangeOrEta::green_lagrange)
            {
                result(0, 0) += 1.;
                result(1, 3) += 1.;
                result(2, 1) += 1.;
                result(2, 2) += 1.;
            }
        }


        template<GreenLagrangeOrEta GreenLagrangeOrEtaT>
        void Update3D(const LocalMatrix& gradient_displacement_matrix, LocalMatrix& result)
        {
            assert(gradient_displacement_matrix.shape(0) == 3);
            assert(gradient_displacement_matrix.shape(1) == 3);

            result(0, 0) = gradient_displacement_matrix(0, 0);
            result(0, 3) = gradient_displacement_matrix(1, 0);
            result(0, 6) = gradient_displacement_matrix(2, 0);

            result(1, 1) = gradient_displacement_matrix(0, 1);
            result(1, 4) = gradient_displacement_matrix(1, 1);
            result(1, 7) = gradient_displacement_matrix(2, 1);

            result(2, 2) = gradient_displacement_matrix(0, 2);
            result(2, 5) = gradient_displacement_matrix(1, 2);
            result(2, 8) = gradient_displacement_matrix(2, 2);

            result(3, 0) = gradient_displacement_matrix(0, 1);
            result(3, 1) = gradient_displacement_matrix(0, 0);
            result(3, 3) = gradient_displacement_matrix(1, 1);
            result(3, 4) = gradient_displacement_matrix(1, 0);
            result(3, 6) = gradient_displacement_matrix(2, 1);
            result(3, 7) = gradient_displacement_matrix(2, 0);

            result(4, 1) = gradient_displacement_matrix(0, 2);
            result(4, 2) = gradient_displacement_matrix(0, 1);
            result(4, 4) = gradient_displacement_matrix(1, 2);
            result(4, 5) = gradient_displacement_matrix(1, 1);
            result(4, 7) = gradient_displacement_matrix(2, 2);
            result(4, 8) = gradient_displacement_matrix(2, 1);

            result(5, 0) = gradient_displacement_matrix(0, 2);
            result(5, 2) = gradient_displacement_matrix(0, 0);
            result(5, 3) = gradient_displacement_matrix(1, 2);
            result(5, 5) = gradient_displacement_matrix(1, 0);
            result(5, 6) = gradient_displacement_matrix(2, 2);
            result(5, 8) = gradient_displacement_matrix(2, 0);

            if (GreenLagrangeOrEtaT == GreenLagrangeOrEta::green_lagrange)
            {
                result(0, 0) += 1.;
                result(1, 4) += 1.;
                result(2, 8) += 1.;
                result(3, 1) += 1.;
                result(3, 3) += 1.;
                result(4, 5) += 1.;
                result(4, 7) += 1.;
                result(5, 2) += 1.;
                result(5, 6) += 1.;
            }
        }


    } // namespace


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_DERIVATIVEGREENLAGRANGE_DOT_HXX_
// *** MoReFEM end header guards *** < //
