// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_GREENLAGRANGETENSOR_DOT_HPP_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_GREENLAGRANGETENSOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>

#include "Utilities/LinearAlgebra/LocalAlias.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{

    /*!
     * \brief Class in charge of computing Green-Lagrange tensor.
     */
    class GreenLagrangeTensor final
    {
      public:
        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<GreenLagrangeTensor>;

      public:
        //! Constructor.
        //! \param[in] mesh_dimension Dimension of the mesh.
        explicit GreenLagrangeTensor(std::size_t mesh_dimension);

        //! Destructor.
        ~GreenLagrangeTensor() = default;

        //! \copydoc doxygen_hide_copy_constructor
        GreenLagrangeTensor(const GreenLagrangeTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        GreenLagrangeTensor(GreenLagrangeTensor&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        GreenLagrangeTensor& operator=(const GreenLagrangeTensor& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GreenLagrangeTensor& operator=(GreenLagrangeTensor&& rhs) = delete;

        //! Update the values.
        //! \param[in] cauchy_green_tensor_value Value of the Cauchy-Green tensor.
        //! \return The updated GreenLagrangeTensor vector.
        const LocalVector& Update(const LocalVector& cauchy_green_tensor_value);

      private:
        //! Dimension of the mesh considered.
        std::size_t GetMeshDimension() const noexcept;

        /*!
         * \brief Access to the vector.
         *
         * \internal This one is at the moment private because all uses are currently covered by Update().
         * \endinternal
         *
         * \return Vector.
         */
        const LocalVector& GetVector() const noexcept;

        //! Non constant access to the vector.
        LocalVector& GetNonCstVector() noexcept;

      private:
        //! Mesh dimension.
        const std::size_t mesh_dimension_;

        //! The matrix.
        LocalVector vector_;
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Operators/LocalVariationalOperator/Advanced/GreenLagrangeTensor.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_GREENLAGRANGETENSOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
