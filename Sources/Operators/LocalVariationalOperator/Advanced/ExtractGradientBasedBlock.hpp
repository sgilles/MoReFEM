// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_EXTRACTGRADIENTBASEDBLOCK_DOT_HPP_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_EXTRACTGRADIENTBASEDBLOCK_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/LinearAlgebra/LocalAlias.hpp"

#include "Geometry/RefGeometricElt/Advanced/ComponentIndex.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief Extract from \a full matrix a block delimited by \a row_component for the rows and \a
     * col_component for the columns.
     *
     * This is used in elastic and hyperelastic operator.
     *
     * \param[in] full_matrix Matrix from which the block is extracted.
     * \param[in] row_component Index of the component we want to extract from rows.
     * \param[in] col_component Index of the component we want to extract from columns.
     * \param[out] matrix The extracted matrix. It is expected to be already properly allocated; only the
     * content is set by current function.
     *
     */
    void ExtractGradientBasedBlock(const LocalMatrix& full_matrix,
                                   ComponentNS::index_type row_component,
                                   ComponentNS::index_type col_component,
                                   LocalMatrix& matrix);


    /*!
     * \brief Extract from \a full column matrix a block delimited by \a row_component for the rows.
     *
     * This is used in mixed solid incompressibility operator.
     *
     * \param[in] full_column_matrix Column matrix from which the block is extracted.
     * \param[in] row_component Index of the component we want to extract from rows.
     * \param[out] column_matrix The extracted column matrix. It is expected to be already properly allocated;
     * only the content is set by current function.
     *
     */
    void ExtractGradientBasedBlockColumnMatrix(const LocalVector& full_column_matrix,
                                               ComponentNS::index_type row_component,
                                               LocalMatrix& column_matrix);

    /*!
     * \brief Extract from \a full row matrix a block delimited by \a col_component for the rows.
     *
     * This is used in mixed solid incompressibility operator.
     *
     * \param[in] full_row_matrix Row matrix from which the block is extracted.
     * \param[in] col_component Index of the component we want to extract from cols.
     * \param[out] row_matrix The extracted row matrix. It is expected to be already properly allocated; only
     * the content is set by current function.
     *
     */
    void ExtractGradientBasedBlockRowMatrix(const LocalVector& full_row_matrix,
                                            ComponentNS::index_type col_component,
                                            LocalMatrix& row_matrix);


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Operators/LocalVariationalOperator/Advanced/ExtractGradientBasedBlock.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_EXTRACTGRADIENTBASEDBLOCK_DOT_HPP_
// *** MoReFEM end header guards *** < //
