// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_INFORMATIONATQUADRATUREPOINT_FORUNKNOWNLIST_DOT_HXX_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_INFORMATIONATQUADRATUREPOINT_FORUNKNOWNLIST_DOT_HXX_
// IWYU pragma: private, include "Operators/LocalVariationalOperator/Advanced/InformationAtQuadraturePoint/ForUnknownList.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "Utilities/Containers/EnumClass.hpp"
#include "Utilities/LinearAlgebra/LocalAlias.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM::Advanced::GeomEltNS { class ComputeJacobian; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS
{


    inline const QuadraturePoint& ForUnknownList::GetQuadraturePoint() const noexcept
    {
        return quadrature_point_;
    }


    inline const LocalVector& ForUnknownList::GetRefGeometricPhi() const noexcept
    {
        return GetLocalVector<EnumUnderlyingType(LocalVectorIndex::phi_ref_geo)>();
    }


    inline LocalVector& ForUnknownList::GetNonCstRefGeometricPhi() noexcept
    {
        return const_cast<LocalVector&>(GetRefGeometricPhi());
    }


    inline double ForUnknownList::GetRefGeometricPhi(std::size_t local_node_index) const
    {
        assert(local_node_index < GetRefGeometricPhi().size());
        return GetRefGeometricPhi()(local_node_index);
    }


    inline const LocalMatrix& ForUnknownList::GetGradientRefGeometricPhi() const noexcept
    {
        return GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::deriv_phi_ref_geo)>();
    }


    inline LocalMatrix& ForUnknownList::GetNonCstGradientRefGeometricPhi() noexcept
    {
        return const_cast<LocalMatrix&>(GetGradientRefGeometricPhi());
    }


    inline const LocalVector& ForUnknownList::GetRefFEltPhi() const noexcept
    {
        return GetLocalVector<EnumUnderlyingType(LocalVectorIndex::phi_ref_felt)>();
    }


    inline const LocalVector& ForUnknownList::GetFEltPhi() const noexcept
    {
        return GetRefFEltPhi();
    }


    inline LocalVector& ForUnknownList::GetNonCstRefFEltPhi() noexcept
    {
        return const_cast<LocalVector&>(GetRefFEltPhi());
    }


    inline double ForUnknownList::GetRefFEltPhi(std::size_t local_node_index) const
    {
        assert(local_node_index < GetRefFEltPhi().size());
        return GetRefFEltPhi()(local_node_index);
    }


    inline const LocalMatrix& ForUnknownList::GetGradientRefFEltPhi() const noexcept
    {
        return GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::deriv_phi_ref_felt)>();
    }


    inline LocalMatrix& ForUnknownList::GetNonCstGradientRefFEltPhi() noexcept
    {
        return const_cast<LocalMatrix&>(GetGradientRefFEltPhi());
    }


    inline double& ForUnknownList::GetNonCstDeterminant() noexcept
    {
        return determinant_;
    }


    inline LocalMatrix& ForUnknownList::GetNonCstGradientFEltPhi() noexcept
    {
        return const_cast<LocalMatrix&>(GetGradientFEltPhi());
    }


    inline const LocalMatrix& ForUnknownList ::GetGradientFEltPhi() const noexcept
    {
        decltype(auto) ret =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::deriv_phi_felt)>();
        assert(ret.shape(0) != 0
               && "If not fulfilled, it's likely the operator what calls current object set "
                  "AllocateGradientFEltPhi::no in its constructor; try replace it by 'yes'.");
        return ret;
    }


    inline const std::vector<int>& ForUnknownList::GetComponentSequence() const noexcept
    {
        return Ncomponent_sequence_;
    }


    inline double ForUnknownList::GetJacobianDeterminant() const noexcept
    {
        return determinant_;
    }


    inline double ForUnknownList::GetAbsoluteValueJacobianDeterminant() const
    {
        return std::fabs(GetJacobianDeterminant());
    }


    inline std::size_t ForUnknownList::GetMeshDimension() const noexcept
    {
        return mesh_dimension_;
    }


    inline bool ForUnknownList::DoAllocateGradientFEltPhi() const
    {
        // Do not use the accessor GetGradientFEltPhi() here, as it asserts number of rows is not null...
        return GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::deriv_phi_felt)>().shape(0) != 0;
    }


    inline LocalMatrix& ForUnknownList::GetNonCstInverseJacobian() noexcept
    {
        return GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::inverse_jacobian)>();
    }


    inline std::size_t ForUnknownList::Nnode() const noexcept
    {
        return GetFEltPhi().size();
    }


    inline const Advanced::GeomEltNS::ComputeJacobian& ForUnknownList ::GetComputeJacobianHelper() const noexcept
    {
        assert(!(!compute_jacobian_helper_));
        return *compute_jacobian_helper_;
    }


    inline Advanced::GeomEltNS::ComputeJacobian& ForUnknownList ::GetNonCstComputeJacobianHelper() noexcept
    {
        return const_cast<Advanced::GeomEltNS::ComputeJacobian&>(GetComputeJacobianHelper());
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_INFORMATIONATQUADRATUREPOINT_FORUNKNOWNLIST_DOT_HXX_
// *** MoReFEM end header guards *** < //
