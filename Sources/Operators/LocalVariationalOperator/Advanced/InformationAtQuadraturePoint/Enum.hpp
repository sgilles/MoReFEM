// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_INFORMATIONATQUADRATUREPOINT_ENUM_DOT_HPP_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_INFORMATIONATQUADRATUREPOINT_ENUM_DOT_HPP_
// *** MoReFEM header guards *** < //


namespace MoReFEM
{


    /*!
     * \brief Whether a gradient matrix should be allocated for \a InformationAtQuadraturePoint.
     *
     * This is required for \a LocalVariationalOperator that uses up gradient-based elements. If you're not sure,
     * try 'no' and run in debug mode: an assert will tell you if 'yes' was required.
     */
    enum class AllocateGradientFEltPhi { no, yes };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_INFORMATIONATQUADRATUREPOINT_ENUM_DOT_HPP_
// *** MoReFEM end header guards *** < //
