// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_GRADIENTDISPLACEMENTMATRIX_DOT_HPP_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_GRADIENTDISPLACEMENTMATRIX_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <vector>

#include "Utilities/LinearAlgebra/LocalAlias.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS { class ForUnknownList; }
namespace MoReFEM::Advanced { class RefFEltInLocalOperator; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::OperatorNS
{

    /*!
     * \brief Compute the gradient matrix related to a given local displacement.
     *
     * \copydoc doxygen_hide_quad_pt_unknown_data_arg
     *
     * \attention This method clearly expects to work within an operator which acts only upon a solid
     * displacement unknown. If your operator actually consider others unknown, you should rather create another
     * operator (typically a \a GlobalParameterOperator) which acts only on solid displacement and then call the
     * resulting value in your own operator (see \a UpdateCauchyGreenTensor use for an illustration).
     *
     * \param[in] ref_felt Reference finite element.
     * \param[in] local_displacement Displacement at the dofs of the finite
     * element under consideration.
     * \param[out] gradient_matrix A square matrix which dimension is dimension of
     * the mesh.
     */
    void ComputeGradientDisplacementMatrix(
        const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data,
        const Advanced::RefFEltInLocalOperator& ref_felt,
        const std::vector<double>& local_displacement,
        LocalMatrix& gradient_matrix);


} // namespace MoReFEM::Advanced::OperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_GRADIENTDISPLACEMENTMATRIX_DOT_HPP_
// *** MoReFEM end header guards *** < //
