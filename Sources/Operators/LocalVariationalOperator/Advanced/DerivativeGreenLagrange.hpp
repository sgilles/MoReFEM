// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_DERIVATIVEGREENLAGRANGE_DOT_HPP_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_DERIVATIVEGREENLAGRANGE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Utilities/LinearAlgebra/LocalAlias.hpp"

#include "ThirdParty/Wrappers/Xtensor/Functions.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    //! Switch between the derivatives of eta or green lagrange.
    enum class GreenLagrangeOrEta { green_lagrange, eta };


    /*!
     * \brief Helper class for Green-Lagrange or a Eta local matrix and its transposed.
     *
     * The key of this class is its Update() method: when it is called the relevant matrix is computed
     * given the gradient displacement matrix.
     *
     * \internal <b><tt>[internal]</tt></b> Same template class is used for both Green-Lagrange and eta as
     * these matrices are extremely similar: Green-Lagrange is just Eta matrix with some terms incremented by 1.
     * \endinternal
     *
     */
    template<GreenLagrangeOrEta GreenLagrangeOrEtaT>
    class DerivativeGreenLagrange
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = DerivativeGreenLagrange;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] mesh_dimension Dimension of the mesh.
        explicit DerivativeGreenLagrange(std::size_t mesh_dimension);

        //! Destructor.
        ~DerivativeGreenLagrange() = default;

        //! \copydoc doxygen_hide_copy_constructor
        DerivativeGreenLagrange(const DerivativeGreenLagrange& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        DerivativeGreenLagrange(DerivativeGreenLagrange&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        DerivativeGreenLagrange& operator=(const DerivativeGreenLagrange& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        DerivativeGreenLagrange& operator=(DerivativeGreenLagrange&& rhs) = delete;

        ///@}

        //! Compute the matrix from a given gradient displacement matrix.
        //! \param[in] gradient_displacement_matrix Matri used to compute the new derivative GreenLagrange
        //! matrix.
        const LocalMatrix& Update(const LocalMatrix& gradient_displacement_matrix);

        /*!
         * \brief Get transposed matrix.
         *
         * \attention This method uses up the current value of the derivative Green-Lagrange; make sure
         * the latter is up-to-date!
         *
         * \return Transposed matrix.
         */
        const LocalMatrix& GetTransposed();

      private:
        /*!
         * \brief Access to the matrix.
         *
         * \internal <b><tt>[internal]</tt></b> This one is at the moment private because all uses are currently
         * covered by Update().
         * \endinternal
         *
         * \return Underlying matrix.
         */
        const LocalMatrix& GetMatrix() const noexcept;

        //! Non constant access to the matrix.
        LocalMatrix& GetNonCstMatrix() noexcept;

      private:
        //! Mesh dimension.
        const std::size_t mesh_dimension_;

        //! The matrix.
        LocalMatrix matrix_;

        //! Transpose of the matrix.
        LocalMatrix transposed_matrix_;
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Operators/LocalVariationalOperator/Advanced/DerivativeGreenLagrange.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_DERIVATIVEGREENLAGRANGE_DOT_HPP_
// *** MoReFEM end header guards *** < //
