// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_GREENLAGRANGETENSOR_DOT_HXX_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_GREENLAGRANGETENSOR_DOT_HXX_
// IWYU pragma: private, include "Operators/LocalVariationalOperator/Advanced/GreenLagrangeTensor.hpp"
// *** MoReFEM header guards *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "Utilities/LinearAlgebra/LocalAlias.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    inline std::size_t GreenLagrangeTensor::GetMeshDimension() const noexcept
    {
        assert(mesh_dimension_ == 2u || mesh_dimension_ == 3u);
        return mesh_dimension_;
    }


    inline const LocalVector& GreenLagrangeTensor::GetVector() const noexcept
    {
        return vector_;
    }


    inline LocalVector& GreenLagrangeTensor::GetNonCstVector() noexcept
    {
        return const_cast<LocalVector&>(GetVector());
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ADVANCED_GREENLAGRANGETENSOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
