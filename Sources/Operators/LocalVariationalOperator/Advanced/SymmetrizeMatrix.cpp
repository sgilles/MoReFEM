// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
#include <cassert>
#include <iostream>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/Numeric/Numeric.hpp"


#include "Operators/LocalVariationalOperator/Advanced/SymmetrizeMatrix.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    void SymmetrizeMatrix(LocalMatrix& matrix)
    {
        assert(matrix.shape().size() == 2ul);
        const auto size = matrix.shape(0);
        assert("Function must only be used on symmetrix matrices!" && size == matrix.shape(1));

#ifndef NDEBUG
        for (auto row = 0ul; row < size; ++row)
            for (auto col = row + 1; col < size; ++col)
            {
                if (!NumericNS::IsZero(matrix(col, row)))
                {
                    std::cerr << "(" << col << ", " << row << ") should be a null value but isn't... ("
                              << matrix(col, row) << ')' << std::endl;
                    assert("Lower half of the matrix should be filled with zeros!" && false);
                }
            }
#endif // NDEBUG

        for (auto row = 0ul; row < size; ++row)
            for (auto col = row; col < size; ++col)
                matrix(col, row) = matrix(row, col);
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //
