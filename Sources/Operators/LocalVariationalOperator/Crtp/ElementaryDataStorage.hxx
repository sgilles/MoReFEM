// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_ELEMENTARYDATASTORAGE_DOT_HXX_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_ELEMENTARYDATASTORAGE_DOT_HXX_
// IWYU pragma: private, include "Operators/LocalVariationalOperator/Crtp/ElementaryDataStorage.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Operators/LocalVariationalOperator/Crtp/ElementaryDataStorage.hpp"


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::LocalVariationalOperatorNS
{


    template<class DerivedT, class VectorTypeT>
    const VectorTypeT&
    ElementaryDataStorage<DerivedT, LinearAlgebraNS::type::vector, VectorTypeT>::GetVectorResult() const
    {
        return vector_;
    }


    template<class DerivedT, class VectorTypeT>
    VectorTypeT& ElementaryDataStorage<DerivedT, LinearAlgebraNS::type::vector, VectorTypeT>::GetNonCstVectorResult()
    {
        return vector_;
    }


    template<class DerivedT, class MatrixTypeT>
    const MatrixTypeT&
    ElementaryDataStorage<DerivedT, LinearAlgebraNS::type::matrix, MatrixTypeT>::GetMatrixResult() const
    {
        return matrix_;
    }


    template<class DerivedT, class MatrixTypeT>
    MatrixTypeT& ElementaryDataStorage<DerivedT, LinearAlgebraNS::type::matrix, MatrixTypeT>::GetNonCstMatrixResult()
    {
        return matrix_;
    }


    template<class DerivedT, class VectorTypeT>
    void ElementaryDataStorage<DerivedT, LinearAlgebraNS::type::vector, VectorTypeT>::AllocateVector(std::size_t Ndof)
    {
        assert(Ndof > 0);

        if constexpr (std::is_same<VectorTypeT, LocalVector>())
        {
            std::array<size_t, 1> temp{ Ndof };
            vector_ = LocalVector(temp);
            vector_.fill(0.);
        } else
        {
            vector_.resize({ Ndof });
            vector_.Zero();
        }
    }


    template<class DerivedT, class MatrixTypeT>
    void
    ElementaryDataStorage<DerivedT, LinearAlgebraNS::type::matrix, MatrixTypeT>::AllocateMatrix(std::size_t Ndof_row,
                                                                                                std::size_t Ndof_col)
    {
        assert(Ndof_row > 0);
        assert(Ndof_col > 0);

        if constexpr (std::is_same<MatrixTypeT, LocalMatrix>())
        {
            matrix_ = LocalMatrix({ Ndof_row, Ndof_col });
            matrix_.fill(0.);
        } else
        {
            matrix_.resize({ Ndof_row, Ndof_col });
            matrix_.Zero();
        }
    }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_ELEMENTARYDATASTORAGE_DOT_HXX_
// *** MoReFEM end header guards *** < //
