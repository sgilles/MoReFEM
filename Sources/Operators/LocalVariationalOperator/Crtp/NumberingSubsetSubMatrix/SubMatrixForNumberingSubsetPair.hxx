// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_NUMBERINGSUBSETSUBMATRIX_SUBMATRIXFORNUMBERINGSUBSETPAIR_DOT_HXX_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_NUMBERINGSUBSETSUBMATRIX_SUBMATRIXFORNUMBERINGSUBSETPAIR_DOT_HXX_
// IWYU pragma: private, include "Operators/LocalVariationalOperator/Crtp/NumberingSubsetSubMatrix/SubMatrixForNumberingSubsetPair.hpp"
// *** MoReFEM header guards *** < //


#include "Utilities/LinearAlgebra/LocalAlias.hpp"


namespace MoReFEM::Internal::LocalVariationalOperatorNS
{


    inline LocalMatrix& SubMatrixForNumberingSubsetPair::GetSubMatrix()
    {
        return local_matrix_;
    }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_NUMBERINGSUBSETSUBMATRIX_SUBMATRIXFORNUMBERINGSUBSETPAIR_DOT_HXX_
// *** MoReFEM end header guards *** < //
