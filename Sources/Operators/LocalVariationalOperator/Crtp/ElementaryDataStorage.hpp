// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_ELEMENTARYDATASTORAGE_DOT_HPP_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_ELEMENTARYDATASTORAGE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/LinearAlgebra/LocalAlias.hpp"


namespace MoReFEM::Internal::LocalVariationalOperatorNS
{


    /*!
     * \brief This CRTP class adds local vector and/or matrix storage to ElementaryData class.
     *
     * \tparam DerivedT A specialization of ElementaryData upon which the CRTP is applied.
     * \tparam IsMatrixOrVectorT Nature of the storage that is added to \a ElementaryDataT.
     * \tparam StorageTypeT Type of the vector or matrix stored.
     *
     * If both vector and matrix behaviour must be added (for elementary_data_type)
     * just inherit from two CRTP classes:
     *
     * \code
     * class elementary_data_type
     * : public Internal::LocalVariationalOperatorNS::ElementaryDataImpl,
     * public Crtp::ElementaryDataStorage<elementary_data_type, LinearAlgebraNS::type::vector>,
     * public Crtp::ElementaryDataStorage<elementary_data_type, LinearAlgebraNS::type::matrix>
     * { ... };
     * \endcode
     */
    template<class DerivedT, LinearAlgebraNS::type TypeT, class StorageTypeT>
    class ElementaryDataStorage;


    // ============================
    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


    template<class DerivedT, class VectorTypeT>
    class ElementaryDataStorage<DerivedT, LinearAlgebraNS::type::vector, VectorTypeT>
    {
      protected:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit ElementaryDataStorage() = default;

        //! Destructor.
        ~ElementaryDataStorage() = default;

        //! \copydoc doxygen_hide_copy_constructor
        ElementaryDataStorage(const ElementaryDataStorage& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ElementaryDataStorage(ElementaryDataStorage&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        ElementaryDataStorage& operator=(const ElementaryDataStorage& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ElementaryDataStorage& operator=(ElementaryDataStorage&& rhs) = delete;

        ///@}

      public:
        //! Constant access to the underlying elementary vector.
        const VectorTypeT& GetVectorResult() const;

        //! Non constant access to the underlying elementary vector.
        VectorTypeT& GetNonCstVectorResult();

      protected:
        //! Allocate the vector.
        void AllocateVector(std::size_t Ndof);


      private:
        //! Underlying elementary vector.
        VectorTypeT vector_;
    };


    template<class DerivedT, class MatrixTypeT>
    class ElementaryDataStorage<DerivedT, LinearAlgebraNS::type::matrix, MatrixTypeT>
    {
      protected:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit ElementaryDataStorage() = default;

        //! Destructor.
        ~ElementaryDataStorage() = default;

        //! \copydoc doxygen_hide_copy_constructor
        ElementaryDataStorage(const ElementaryDataStorage& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ElementaryDataStorage(ElementaryDataStorage&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        ElementaryDataStorage& operator=(const ElementaryDataStorage& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ElementaryDataStorage& operator=(ElementaryDataStorage&& rhs) = delete;

        ///@}

      public:
        //! Constant access to the underlying elementary matrix.
        const MatrixTypeT& GetMatrixResult() const;

        //! Non constant access to the underlying elementary matrix.
        MatrixTypeT& GetNonCstMatrixResult();


      protected:
        //! Allocate the matrix.
        void AllocateMatrix(std::size_t Ndof_row, std::size_t Ndof_col);


      private:
        //! Underlying elementary matrix.
        MatrixTypeT matrix_;
    };


    // ============================
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN
    // ============================


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Operators/LocalVariationalOperator/Crtp/ElementaryDataStorage.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_CRTP_ELEMENTARYDATASTORAGE_DOT_HPP_
// *** MoReFEM end header guards *** < //
