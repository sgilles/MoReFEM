// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_INTERNAL_ELEMENTARYDATAIMPL_DOT_HXX_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_INTERNAL_ELEMENTARYDATAIMPL_DOT_HXX_
// IWYU pragma: private, include "Operators/LocalVariationalOperator/Internal/ElementaryDataImpl.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Operators/LocalVariationalOperator/Internal/ElementaryDataImpl.hpp"


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <optional>
#include <vector>

#include "Operators/LocalVariationalOperator/Advanced/InformationAtQuadraturePoint.hpp"

#include "FiniteElement/FiniteElement/LocalFEltSpace.hpp"
#include "FiniteElement/QuadratureRules/QuadratureRule.hpp"
#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::LocalVariationalOperatorNS
{


    inline const Internal::RefFEltNS::RefLocalFEltSpace& ElementaryDataImpl::GetRefLocalFEltSpace() const noexcept
    {
        return ref_felt_space_;
    }


    inline const RefGeomElt& ElementaryDataImpl::GetRefGeomElt() const noexcept
    {
        return GetRefLocalFEltSpace().GetRefGeomElt();
    }


    inline std::size_t ElementaryDataImpl::NnodeRow() const noexcept
    {
        return Nnode_row_;
    }


    inline std::size_t ElementaryDataImpl::NdofRow() const noexcept
    {
        return Ndof_row_;
    }

    inline std::size_t ElementaryDataImpl::NnodeCol() const noexcept
    {
        return Nnode_col_;
    }


    inline std::size_t ElementaryDataImpl::NdofCol() const noexcept
    {
        return Ndof_col_;
    }


    inline std::size_t ElementaryDataImpl::GetGeomEltDimension() const noexcept
    {
        return geom_elt_dimension_;
    }


    inline std::size_t ElementaryDataImpl::NquadraturePoint() const noexcept
    {
        return GetQuadratureRule().NquadraturePoint();
    }


    inline std::size_t ElementaryDataImpl::GetFEltSpaceDimension() const noexcept
    {
        return felt_space_dimension_;
    }


    inline std::size_t ElementaryDataImpl::GetMeshDimension() const noexcept
    {
        return mesh_dimension_;
    }


    inline const LocalFEltSpace& ElementaryDataImpl::GetCurrentLocalFEltSpace() const noexcept
    {
        assert(!(!current_local_felt_space_));
        return *current_local_felt_space_;
    }


    inline const GeometricElt& ElementaryDataImpl::GetCurrentGeomElt() const noexcept
    {
        assert(!(!current_local_felt_space_));
        return current_local_felt_space_->GetGeometricElt();
    }


    inline std::size_t ElementaryDataImpl::NnodeInRefGeomElt() const noexcept
    {
        return Ncoords_in_geom_ref_elt_;
    }


    inline std::size_t ElementaryDataImpl::Nunknown() const noexcept
    {
        return ref_felt_list_.size();
    }

    inline std::size_t ElementaryDataImpl::NtestUnknown() const noexcept
    {
        return GetTestRefFEltList().size();
    }


    inline const std::vector<Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint>&
    ElementaryDataImpl ::GetInformationAtQuadraturePointList() const noexcept
    {
        assert(!infos_at_quad_pt_list_.empty());
        return infos_at_quad_pt_list_;
    }


    inline std::vector<Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint>&
    ElementaryDataImpl ::GetNonCstInformationAtQuadraturePointList()
    {
        return const_cast<std::vector<Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint>&>(
            GetInformationAtQuadraturePointList());
    }


    inline const Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint&
    ElementaryDataImpl ::GetInformationAtQuadraturePoint(std::size_t quadrature_pt_index) const noexcept
    {
        assert(quadrature_pt_index < infos_at_quad_pt_list_.size());
        return infos_at_quad_pt_list_[quadrature_pt_index];
    }


    inline const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr&
    ElementaryDataImpl ::GetRefFEltList() const noexcept
    {
        return ref_felt_list_;
    }


    inline const Advanced::RefFEltInLocalOperator::vector_const_unique_ptr&
    ElementaryDataImpl ::GetTestRefFEltList() const noexcept
    {
        // Optional: test_ref_felt_list_ is filled only if it differs from ref_felt_list_.
        if (test_ref_felt_list_)
            return test_ref_felt_list_.value();

        return ref_felt_list_;
    }


    inline void ElementaryDataImpl::SetCurrentLocalFEltSpace(const LocalFEltSpace* local_felt_space)
    {
        current_local_felt_space_ = local_felt_space;
    }


    inline const QuadratureRule& ElementaryDataImpl::GetQuadratureRule() const noexcept
    {
        return quadrature_rule_;
    }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_INTERNAL_ELEMENTARYDATAIMPL_DOT_HXX_
// *** MoReFEM end header guards *** < //
