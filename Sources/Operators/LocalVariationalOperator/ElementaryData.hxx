// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ELEMENTARYDATA_DOT_HXX_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ELEMENTARYDATA_DOT_HXX_
// IWYU pragma: private, include "Operators/LocalVariationalOperator/ElementaryData.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Operators/LocalVariationalOperator/ElementaryData.hpp"


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Advanced
{


    template<class MatrixTypeT>
    ElementaryData<OperatorNS::Nature::bilinear, MatrixTypeT, std::false_type>::ElementaryData(
        const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
        const QuadratureRule& quadrature_rule,
        const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
        const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage,
        std::size_t felt_space_dimension,
        std::size_t mesh_dimension,
        AllocateGradientFEltPhi do_allocate_gradient_felt_phi)
    : Internal::LocalVariationalOperatorNS::ElementaryDataImpl(ref_felt_space,
                                                               quadrature_rule,
                                                               unknown_storage,
                                                               test_unknown_storage,
                                                               felt_space_dimension,
                                                               mesh_dimension,
                                                               do_allocate_gradient_felt_phi),
      matrix_storage_parent()
    {
        Allocate();
    }


    template<class VectorTypeT>
    ElementaryData<OperatorNS::Nature::linear, std::false_type, VectorTypeT>::ElementaryData(
        const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
        const QuadratureRule& quadrature_rule,
        const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
        const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage,
        std::size_t felt_space_dimension,
        std::size_t mesh_dimension,
        AllocateGradientFEltPhi do_allocate_gradient_felt_phi)
    : Internal::LocalVariationalOperatorNS::ElementaryDataImpl(ref_felt_space,
                                                               quadrature_rule,
                                                               unknown_storage,
                                                               test_unknown_storage,
                                                               felt_space_dimension,
                                                               mesh_dimension,
                                                               do_allocate_gradient_felt_phi),
      vector_storage_parent()
    {
        Allocate();
    }


    template<class MatrixTypeT, class VectorTypeT>
    ElementaryData<OperatorNS::Nature::nonlinear, MatrixTypeT, VectorTypeT>::ElementaryData(
        const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
        const QuadratureRule& quadrature_rule,
        const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
        const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage,
        std::size_t felt_space_dimension,
        std::size_t mesh_dimension,
        AllocateGradientFEltPhi do_allocate_gradient_felt_phi)
    : Internal::LocalVariationalOperatorNS::ElementaryDataImpl(ref_felt_space,
                                                               quadrature_rule,
                                                               unknown_storage,
                                                               test_unknown_storage,
                                                               felt_space_dimension,
                                                               mesh_dimension,
                                                               do_allocate_gradient_felt_phi),
      vector_storage_parent(), matrix_storage_parent()
    {
        Allocate();
    }


    template<class MatrixTypeT>
    void ElementaryData<OperatorNS::Nature::bilinear, MatrixTypeT, std::false_type>::Allocate()
    {
        this->AllocateMatrix(this->NdofRow(), this->NdofCol());
    }


    template<class VectorTypeT>
    void ElementaryData<OperatorNS::Nature::linear, std::false_type, VectorTypeT>::Allocate()
    {
        this->AllocateVector(this->NdofRow());
    }


    template<class MatrixTypeT, class VectorTypeT>
    void ElementaryData<OperatorNS::Nature::nonlinear, MatrixTypeT, VectorTypeT>::Allocate()
    {
        this->AllocateMatrix(this->NdofRow(), this->NdofCol());
        this->AllocateVector(this->NdofRow());
    }


    template<class MatrixTypeT>
    void ElementaryData<OperatorNS::Nature::bilinear, MatrixTypeT, std::false_type>::Zero()
    {
        this->GetNonCstMatrixResult().fill(0.);
    }


    template<class VectorTypeT>
    void ElementaryData<OperatorNS::Nature::linear, std::false_type, VectorTypeT>::Zero()
    {
        this->GetNonCstVectorResult().fill(0.);
    }


    template<class MatrixTypeT, class VectorTypeT>
    void ElementaryData<OperatorNS::Nature::nonlinear, MatrixTypeT, VectorTypeT>::Zero()
    {
        this->GetNonCstMatrixResult().fill(0.);
        this->GetNonCstVectorResult().fill(0.);
    }


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ELEMENTARYDATA_DOT_HXX_
// *** MoReFEM end header guards *** < //
