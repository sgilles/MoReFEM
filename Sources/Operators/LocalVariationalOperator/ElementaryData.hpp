// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ELEMENTARYDATA_DOT_HPP_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ELEMENTARYDATA_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "Utilities/LinearAlgebra/Type.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/RefFiniteElement/Internal/RefLocalFEltSpace.hpp" // IWYU pragma: export

#include "Parameters/Parameter.hpp"

#include "Operators/Enum.hpp"
#include "Operators/LocalVariationalOperator/Crtp/ElementaryDataStorage.hpp"  // IWYU pragma: export
#include "Operators/LocalVariationalOperator/Internal/ElementaryDataImpl.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================


namespace MoReFEM
{


    template
    <
        class DerivedT,
        class LocalParameterOperatorT,
        ::MoReFEM::ParameterNS::Type TypeT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        template<::MoReFEM::ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
    >
    class GlobalParameterOperator;


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //

namespace MoReFEM::Internal::GlobalVariationalOperatorNS
{

    template
    <
        ::MoReFEM::Concept::Tuple TupleT,
        std::size_t I,
        std::size_t TupleSizeT,
        ::MoReFEM::Advanced::OperatorNS::Nature NatureT
    >
    struct LocalVariationalOperatorIterator;


} // namespace MoReFEM::Internal::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced
{


    /*!
     * \brief This object holds all the data required to compute the elementary matrix or vector and the resulting
     * one.
     *
     *
     * \attention the values inside the local matrices and vectors may change frequently, but the allocation does
     * not: the size of the matrices and vectors is fixed early in the program and is not modified until
     * the deallocation when the program is done.
     *
     * \tparam MatrixTypeT Type of the elementary matrix, or std::false_type if irrelevant.
     * \tparam VectorTypeT Type of the elementary vector, or std::false_type if irrelevant.
     *
     */
    template<OperatorNS::Nature OperatorNatureT, class MatrixTypeT, class VectorTypeT>
    class ElementaryData;


    /*!
     * \class doxygen_hide_elementary_data_unknown_and_test_unknown_param
     *
     * \param[in] unknown_storage List of unknown/numbering subset pairs involved in the current local operator.
     * There can be two at most; their relative ordering fix the structure of the local matrix (but it should be
     * completely transparent for the user anyway: the library takes care of fetching the appropriate elements
     * through RefFEltInLocalOperator class).
     * \param[in] test_unknown_storage Same as \a unknown_storage for test functions.
     */


    /*!
     * \brief Specialization of ElementaryData for vectors (i.e. the one used in linear form operators).
     */
    template<class VectorTypeT>
    class ElementaryData<OperatorNS::Nature::linear, std::false_type, VectorTypeT>
    : public Internal::LocalVariationalOperatorNS::ElementaryDataImpl,
      public Internal::LocalVariationalOperatorNS::ElementaryDataStorage<
          ElementaryData<OperatorNS::Nature::linear, std::false_type, VectorTypeT>,
          LinearAlgebraNS::type::vector,
          VectorTypeT>
    {
      private:
        //! Convenient alias.
        using self = ElementaryData<OperatorNS::Nature::linear, std::false_type, VectorTypeT>;

        //! Alias for parent.
        using vector_storage_parent = Internal::LocalVariationalOperatorNS::
            ElementaryDataStorage<self, LinearAlgebraNS::type::vector, VectorTypeT>;

      public:
        //! Alias to type of matrix (std::false_type if irrelevant).
        using matrix_type = std::false_type;

        //! Alias to type of vector (std::false_type if irrelevant).
        using vector_type = VectorTypeT;


        //! Friendship to GlobalParameterOperator, one of the two classes allowed to build such an object.
        // clang-format off
        template
        <
            class DerivedT,
            class LocalParameterOperatorT,
            ::MoReFEM::ParameterNS::Type TypeT,
            TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
            template<::MoReFEM::ParameterNS::Type, TIME_MANAGER_TEMPLATE_KEYWORD> class TimeDependencyT
        >
        // clang-format on
        friend class ::MoReFEM::GlobalParameterOperator;

        //! Friendship to an internal helper structure.
        template<::MoReFEM::Concept::Tuple TupleT,
                 std::size_t I,
                 std::size_t TupleSizeT,
                 Advanced::OperatorNS::Nature NatureT>
        friend struct ::MoReFEM::Internal::GlobalVariationalOperatorNS::LocalVariationalOperatorIterator;


      private:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] ref_felt_space There is exactly one RefGeomElt associated to such an object; this and the
         * list of unknowns involved for the operator related to this ElementaryData will provide all the data
         * required to build RefFEltInLocalOperator objects that allow easy navigation through the
         * elementary data (for instance methods Phi() or dPhi() use this object to extract conveniently the
         * required data). \param[in] quadrature_rule Quadrature rule to use for the enclosing local operator.
         * \copydoc doxygen_hide_elementary_data_unknown_and_test_unknown_param
         * \param[in] felt_space_dimension Dimension considered in the finite element space.
         * \param[in] mesh_dimension Dimension in the mesh.
         *  Must be equal or higher than felt_space_dimension.
         * \copydoc doxygen_hide_operator_do_allocate_gradient_felt_phi_arg
         */
        explicit ElementaryData(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                                const QuadratureRule& quadrature_rule,
                                const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
                                const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage,
                                std::size_t felt_space_dimension,
                                std::size_t mesh_dimension,
                                AllocateGradientFEltPhi do_allocate_gradient_felt_phi);

      public:
        //! Destructor.
        ~ElementaryData() = default;

        //! \copydoc doxygen_hide_copy_constructor
        ElementaryData(const ElementaryData& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ElementaryData(ElementaryData&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        ElementaryData& operator=(const ElementaryData& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ElementaryData& operator=(ElementaryData&& rhs) = default;

        ///@}

        //! Allocate the elementary vector.
        void Allocate();

        //! Zero the elementary vector.
        void Zero();
    };


    /*!
     * \brief Specialization of ElementaryData for matrices (i.e. the one used in bilinear form operators).
     */
    template<class MatrixTypeT>
    class ElementaryData<OperatorNS::Nature::bilinear, MatrixTypeT, std::false_type>
    : public Internal::LocalVariationalOperatorNS::ElementaryDataImpl,
      public Internal::LocalVariationalOperatorNS::ElementaryDataStorage<
          ElementaryData<OperatorNS::Nature::bilinear, MatrixTypeT, std::false_type>,
          LinearAlgebraNS::type::matrix,
          MatrixTypeT>
    {
      private:
        //! Convenient alias.
        using self = ElementaryData<OperatorNS::Nature::bilinear, MatrixTypeT, std::false_type>;

        //! Alias for parent.
        using matrix_storage_parent = Internal::LocalVariationalOperatorNS::
            ElementaryDataStorage<self, LinearAlgebraNS::type::matrix, MatrixTypeT>;

      public:
        //! Alias to type of matrix (std::false_type if irrelevant).
        using matrix_type = MatrixTypeT;

        //! Alias to type of vector (std::false_type if irrelevant).
        using vector_type = std::false_type;

        //! Friendship to an internal helper structure.
        template<::MoReFEM::Concept::Tuple TupleT,
                 std::size_t I,
                 std::size_t TupleSizeT,
                 Advanced::OperatorNS::Nature NatureT>
        friend struct ::MoReFEM::Internal::GlobalVariationalOperatorNS::LocalVariationalOperatorIterator;

      private:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] ref_felt_space There is exactly one RefGeomElt associated to such an object; this and the
         * list of unknowns involved for the operator related to this ElementaryData will provide all the data
         * required to build RefFEltInLocalOperator objects that allow easy navigation through the
         * elementary data (for instance methods Phi() or dPhi() use this object to extract conveniently the
         * required data). \param[in] quadrature_rule Quadrature rule to use for the enclosing local operator.
         * \copydoc doxygen_hide_elementary_data_unknown_and_test_unknown_param
         * \param[in] felt_space_dimension Dimension considered in the finite element space.
         * \param[in] mesh_dimension Dimension in the mesh.
         *  Must be equal or higher than felt_space_dimension.
         * \copydoc doxygen_hide_operator_do_allocate_gradient_felt_phi_arg
         */
        explicit ElementaryData(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                                const QuadratureRule& quadrature_rule,
                                const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
                                const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage,
                                std::size_t felt_space_dimension,
                                std::size_t mesh_dimension,
                                AllocateGradientFEltPhi do_allocate_gradient_felt_phi);

      public:
        //! Destructor.
        ~ElementaryData() = default;

        //! \copydoc doxygen_hide_copy_constructor
        ElementaryData(const ElementaryData& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ElementaryData(ElementaryData&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        ElementaryData& operator=(const ElementaryData& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ElementaryData& operator=(ElementaryData&& rhs) = delete;

        ///@}

        //! Allocate the elementary matrix.
        void Allocate();

        //! Zero the elementary matrix.
        void Zero();
    };


    /*!
     * \brief Specialization of ElementaryData for matrices and vectors (i.e. the one used in nonlinear form
     * operators).
     */
    template<class MatrixTypeT, class VectorTypeT>
    class ElementaryData<OperatorNS::Nature::nonlinear, MatrixTypeT, VectorTypeT>
    : public Internal::LocalVariationalOperatorNS::ElementaryDataImpl,
      public Internal::LocalVariationalOperatorNS::ElementaryDataStorage<
          ElementaryData<OperatorNS::Nature::nonlinear, MatrixTypeT, VectorTypeT>,
          LinearAlgebraNS::type::vector,
          VectorTypeT>,
      public Internal::LocalVariationalOperatorNS::ElementaryDataStorage<
          ElementaryData<OperatorNS::Nature::nonlinear, MatrixTypeT, VectorTypeT>,
          LinearAlgebraNS::type::matrix,
          MatrixTypeT>
    {

      private:
        //! Convenient alias.
        using self = ElementaryData<OperatorNS::Nature::nonlinear, MatrixTypeT, VectorTypeT>;

        //! Alias for parent.
        using vector_storage_parent = Internal::LocalVariationalOperatorNS::
            ElementaryDataStorage<self, LinearAlgebraNS::type::vector, VectorTypeT>;

        //! Alias for parent.
        using matrix_storage_parent = Internal::LocalVariationalOperatorNS::
            ElementaryDataStorage<self, LinearAlgebraNS::type::matrix, MatrixTypeT>;

      public:
        //! Alias to type of matrix (std::false_type if irrelevant).
        using matrix_type = MatrixTypeT;

        //! Alias to type of vector (std::false_type if irrelevant).
        using vector_type = VectorTypeT;

        //! Friendship to an internal helper structure.
        template<::MoReFEM::Concept::Tuple TupleT,
                 std::size_t I,
                 std::size_t TupleSizeT,
                 Advanced::OperatorNS::Nature NatureT>
        friend struct ::MoReFEM::Internal::GlobalVariationalOperatorNS::LocalVariationalOperatorIterator;


      private:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] ref_felt_space There is exactly one RefGeomElt associated to such an object; this and the
         * list of unknowns involved for the operator related to this ElementaryData will provide all the data
         * required to build RefFEltInLocalOperator objects that allow easy navigation through the
         * elementary data (for instance methods Phi() or dPhi() use this object to extract conveniently the
         * required data). \param[in] quadrature_rule Quadrature rule to use for the enclosing local operator.
         * \copydoc doxygen_hide_elementary_data_unknown_and_test_unknown_param
         * \param[in] felt_space_dimension Dimension considered in the finite element space.
         * \param[in] mesh_dimension Dimension in the mesh.
         *  Must be equal or higher than felt_space_dimension.
         * \copydoc doxygen_hide_operator_do_allocate_gradient_felt_phi_arg
         */
        explicit ElementaryData(const Internal::RefFEltNS::RefLocalFEltSpace& ref_felt_space,
                                const QuadratureRule& quadrature_rule,
                                const ExtendedUnknown::vector_const_shared_ptr& unknown_storage,
                                const ExtendedUnknown::vector_const_shared_ptr& test_unknown_storage,
                                std::size_t felt_space_dimension,
                                std::size_t mesh_dimension,
                                AllocateGradientFEltPhi do_allocate_gradient_felt_phi);

      public:
        //! Destructor.
        ~ElementaryData() = default;

        //! \copydoc doxygen_hide_copy_constructor
        ElementaryData(const ElementaryData& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        ElementaryData(ElementaryData&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        ElementaryData& operator=(const ElementaryData& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        ElementaryData& operator=(ElementaryData&& rhs) = default;

        ///@}

        //! Allocate the elementary matrix and vector.
        void Allocate();

        //! Zero the elementary matrix and vector.
        void Zero();
    };


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Operators/LocalVariationalOperator/ElementaryData.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_ELEMENTARYDATA_DOT_HPP_
// *** MoReFEM end header guards *** < //
