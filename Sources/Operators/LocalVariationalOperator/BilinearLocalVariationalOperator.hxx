// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorsGroup
 * \addtogroup OperatorsGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_BILINEARLOCALVARIATIONALOPERATOR_DOT_HXX_
#define MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_BILINEARLOCALVARIATIONALOPERATOR_DOT_HXX_
// IWYU pragma: private, include "Operators/LocalVariationalOperator/BilinearLocalVariationalOperator.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Operators/LocalVariationalOperator/BilinearLocalVariationalOperator.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    template<class MatrixTypeT>
    BilinearLocalVariationalOperator<MatrixTypeT>::BilinearLocalVariationalOperator(
        const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
        const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
        elementary_data_type&& elementary_data)
    : parent(unknown_list, test_unknown_list, std::move(elementary_data))
    {
        sub_matrix_parent::AllocateSubMatrices(this->GetElementaryData());
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorsGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORS_LOCALVARIATIONALOPERATOR_BILINEARLOCALVARIATIONALOPERATOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
