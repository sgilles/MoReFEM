// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_COMMANDLINEFLAGS_INTERNAL_COMMANDLINEFLAGS_DOT_HPP_
#define MOREFEM_CORE_COMMANDLINEFLAGS_INTERNAL_COMMANDLINEFLAGS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <string>

#include "Utilities/Singleton/Singleton.hpp" // IWYU pragma: export

#include "ThirdParty/Wrappers/Petsc/Advanced/Enum.hpp" // IWYU pragma: export

#include "Core/CommandLineFlags/Advanced/Enum.hpp" // IWYU pragma: export


namespace MoReFEM::Internal
{


    /*!
     *! \brief Singleton in charge of storing command line flags.
     *
     * Should not be invoked directly: the idea is to provide a dedicated function for each flag in
     * \a Advanced::CommandLineFlagsNS namespace.
     */
    class CommandLineFlags final : public Utilities::Singleton<CommandLineFlags>
    {


      public:
        //! \copydoc doxygen_hide_alias_self
        using self = CommandLineFlags;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

      private:
        //! \name Singleton requirements.
        ///@{

        /*!
         * \brief Constructor.
         *
         * Each of the flags considered should be set here (they are meant to be const data attributes).
         * The constructor is expected to be called in \a MoReFEM::Internal::MoReFEMDataNS::ParseCommandLine() function.
         *
         * \param[in] do_overwrite_directory Whether pre-existing output directory should be silently removed ot not.
         * \param[in] do_print_linalg_destruction Whether global matrix or vector destruction should be printed in standard output.
         *
         */
        explicit CommandLineFlags(Advanced::CommandLineFlagsNS::overwrite_directory do_overwrite_directory,
                                  Advanced::Wrappers::Petsc::print_linalg_destruction do_print_linalg_destruction);

        //! Destructor.
        virtual ~CommandLineFlags() override;

        //! Friendship declaration to Singleton template class (to enable call to constructor).
        friend class Utilities::Singleton<CommandLineFlags>;

        //! Name of the class.
        static const std::string& ClassName();


        ///@}

      public:
        //! Whether the output directory should be overwritten if it already exists at initialization.
        Advanced::CommandLineFlagsNS::overwrite_directory DoOverwriteDirectory() const noexcept;

        /*!
         * \brief Whether a message in standard output tells when a global linear algebra object is freed.
         *
         * \copydetails doxygen_hide_print_linalg_destruction_text
         */
        Advanced::Wrappers::Petsc::print_linalg_destruction DoPrintLinearAlgebraDestruction() const noexcept;

      private:
        /*!
         * \brief Enum to dictate how to handle already existing output directory at initialization.
         *
         * Set to true if --overwrite_directory is present in command line.
         *
         * \attention This flag may be given only for executables for which program_type is  program_type::model.
         */
        const Advanced::CommandLineFlagsNS::overwrite_directory do_overwrite_directory_;

        /*!
         * \brief If yes, a message is printed whenever a \a GlobalVector or a \a GlobalMatrix destructor is called.
         *
         * \copydetails doxygen_hide_print_linalg_destruction_text
         */
        const Advanced::Wrappers::Petsc::print_linalg_destruction do_print_linalg_destruction_;
    };


} // namespace MoReFEM::Internal


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
#include "Core/CommandLineFlags/Internal/CommandLineFlags.hxx"

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_COMMANDLINEFLAGS_INTERNAL_COMMANDLINEFLAGS_DOT_HPP_
// *** MoReFEM end header guards *** < //
