// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_NONE_DOT_HXX_
#define MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_NONE_DOT_HXX_
// IWYU pragma: private, include "Core/TimeManager/Policy/Evolution/None.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Core/TimeManager/Policy/Evolution/None.hpp"


#include "Utilities/InputData/Concept.hpp"


namespace MoReFEM::TimeManagerNS::Policy
{


    // clang-format off
    template
    <
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    None::None(const Wrappers::Mpi&, const ModelSettingsT&, const InputDataT&)
    { }


} // namespace MoReFEM::TimeManagerNS::Policy


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_TIMEMANAGER_POLICY_EVOLUTION_NONE_DOT_HXX_
// *** MoReFEM end header guards *** < //
