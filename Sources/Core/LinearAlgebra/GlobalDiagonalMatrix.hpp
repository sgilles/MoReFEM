// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_LINEARALGEBRA_GLOBALDIAGONALMATRIX_DOT_HPP_
#define MOREFEM_CORE_LINEARALGEBRA_GLOBALDIAGONALMATRIX_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <type_traits> // IWYU pragma: keep

#include "Core/LinearAlgebra/GlobalMatrix.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================


namespace MoReFEM { class NumberingSubset; }


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    /*!
     * \brief Specific case of GlobalMatrix which is square and diagonal.
     *
     * \internal <b><tt>[internal]</tt></b> Actually there is not much here that signs the diagonal structure of the
     * matrix, save the fact only one numbering subset is used for both rows and columns. However,
     * AllocateGlobalMatrix() provides an overload for this type that allocates only values on the diagonal.
     * \endinternal
     */
    class GlobalDiagonalMatrix final : public GlobalMatrix
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = GlobalDiagonalMatrix;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to direct parent.
        using parent = GlobalMatrix;

        static_assert(std::is_convertible<self*, parent*>());

        //! \copydoc doxygen_hide_global_linear_algebra_matrix_concept_keyword
        static inline constexpr bool ConceptIsGlobalLinearAlgebra = true;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] numbering_subset \a NumberingSubset to use to define the numbering of rows and columns of the
         * diagonal matrix.
         *
         * \copydetails doxygen_hide_petsc_matrix_name_arg
         * If name is not provided, a default name featuring the numbering subset number is used.
         */
        explicit GlobalDiagonalMatrix(const NumberingSubset& numbering_subset,
                                      std::optional<std::string> name = std::nullopt);

        //! Destructor.
        ~GlobalDiagonalMatrix() override;

        //! \copydoc doxygen_hide_copy_constructor
        GlobalDiagonalMatrix(const GlobalDiagonalMatrix& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        GlobalDiagonalMatrix(GlobalDiagonalMatrix&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        GlobalDiagonalMatrix& operator=(const GlobalDiagonalMatrix& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GlobalDiagonalMatrix& operator=(GlobalDiagonalMatrix&& rhs) = delete;

        ///@}
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/LinearAlgebra/GlobalDiagonalMatrix.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_LINEARALGEBRA_GLOBALDIAGONALMATRIX_DOT_HPP_
// *** MoReFEM end header guards *** < //
