// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_PARAMETER_INTERNAL_TRAITS_DOT_HXX_
#define MOREFEM_CORE_PARAMETER_INTERNAL_TRAITS_DOT_HXX_
// IWYU pragma: private, include "Core/Parameter/Internal/Traits.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep

#include "Core/Parameter/TypeEnum.hpp"


namespace MoReFEM::Internal::ParameterNS
{


    inline double Traits<Type::scalar>::AllocateDefaultValue(std::size_t, std::size_t) noexcept
    {
        return 0.;
    }


} // namespace MoReFEM::Internal::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_PARAMETER_INTERNAL_TRAITS_DOT_HXX_
// *** MoReFEM end header guards *** < //
