// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep

#include "Core/Parameter/Internal/Traits.hpp"


namespace MoReFEM::Internal::ParameterNS
{


    namespace // anonymous
    {


        LocalVector InitLocalVector(std::size_t Nrow);

        LocalMatrix InitLocalMatrix(std::size_t Nrow, std::size_t Ncol);


    } // namespace


    Traits<Type::vector>::return_type Traits<Type::vector>::AllocateDefaultValue(std::size_t Nrow, std::size_t) noexcept
    {
        static auto ret = InitLocalVector(Nrow);
        return ret;
    }


    Traits<Type::matrix>::return_type Traits<Type::matrix>::AllocateDefaultValue(std::size_t Nrow,
                                                                                 std::size_t Ncol) noexcept
    {
        static auto ret = InitLocalMatrix(Nrow, Ncol);
        return ret;
    }


    namespace // anonymous
    {


        LocalVector InitLocalVector(std::size_t Nrow)
        {
            LocalVector ret;
            ret.resize({ Nrow });
            ret.fill(0.);
            return ret;
        }


        LocalMatrix InitLocalMatrix(std::size_t Nrow, std::size_t Ncol)
        {
            LocalMatrix ret({ Nrow, Ncol });
            ret.fill(0.);
            return ret;
        }


    } // namespace


} // namespace MoReFEM::Internal::ParameterNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
