// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_PARAMETER_FIBERENUM_DOT_HPP_
#define MOREFEM_CORE_PARAMETER_FIBERENUM_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <string>


namespace MoReFEM::FiberNS
{


    //! Type of the input data read in the fiber file to instantiate the fiber parameter.
    enum class AtNodeOrAtQuadPt { none, at_node, at_quad_pt, irrelevant };


    /*!
     * \brief Returns a string that tells which value is chosen, for logging purpose.
     *
     * \param[in] value The enum value which we want printed as a string.
     *
     * \return The string representing the \a value.
     */
    std::string AsString(AtNodeOrAtQuadPt value);


} // namespace MoReFEM::FiberNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_PARAMETER_FIBERENUM_DOT_HPP_
// *** MoReFEM end header guards *** < //
