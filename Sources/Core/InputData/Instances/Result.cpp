// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/Result.hpp"


namespace MoReFEM::InputDataNS
{


    const std::string& Result::GetName()
    {
        static const std::string ret("Result");
        return ret;
    }


    const std::string& Result::OutputDirectory::NameInFile()
    {
        static const std::string ret("output_directory");
        return ret;
    }


    const std::string& Result::OutputDirectory::Description()
    {
        static const std::string ret(
            "Directory in which all the results will be written. This path may use the "
            "environment variable MOREFEM_RESULT_DIR, which is either provided in user's "
            "environment or automatically set to '/Volumes/Data/${USER}/MoReFEM/Results' "
            "in MoReFEM initialization step. You may also use ${MOREFEM_START_TIME} in the value "
            "which will be replaced by a time under format YYYY_MM_DD_HH_MM_SS."
            "Please do not read the value directly from this Lua file: whenever you need the "
            "path to the result directory, use instead MoReFEMData::GetResultDirectory().");
        return ret;
    }


    const std::string& Result::DisplayValue::NameInFile()
    {
        static const std::string ret("display_value");
        return ret;
    }


    const std::string& Result::DisplayValue::Description()
    {
        static const std::string ret(
            "Enables to skip some printing in the console. Can be used to WriteSolution every n time.");
        return ret;
    }


    const std::string& Result::DisplayValue::Constraint()
    {
        static const std::string ret("v > 0");
        return ret;
    }


    const std::string& Result::DisplayValue::DefaultValue()
    {
        static const std::string ret("1");
        return ret;
    }


    const std::string& Result::BinaryOutput::NameInFile()
    {
        static const std::string ret("binary_output");
        return ret;
    }


    const std::string& Result::BinaryOutput::Description()
    {
        static const std::string ret("Defines the solutions output format. Set to false for ascii or true for binary.");
        return ret;
    }


    const std::string& Result::BinaryOutput::DefaultValue()
    {
        static const std::string ret("false");
        return ret;
    }


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
