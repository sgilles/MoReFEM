// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_SPATIALFUNCTION_DOT_HXX_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_SPATIALFUNCTION_DOT_HXX_
// IWYU pragma: private, include "Core/InputData/Instances/Parameter/SpatialFunction.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Core/InputData/Instances/Parameter/SpatialFunction.hpp"


// IWYU pragma: private, include "Core/InputData/Instances/Parameter/Tools/SpatialFunction.hpp"

#include <cassert>
#include <cstdlib>
#include <iosfwd>
#include <string>


namespace MoReFEM::InputDataNS
{


    template<CoordsType CoordsTypeT>
    inline CoordsType SpatialFunction<CoordsTypeT>::GetCoordsType()
    {
        return CoordsTypeT;
    }


    template<CoordsType CoordsTypeT>
    const std::string& SpatialFunction<CoordsTypeT>::DescriptionCoordsType()
    {
        if constexpr (CoordsTypeT == CoordsType::local)
        {
            static const std::string ret(
                "\n Function expects as arguments local coordinates (e.g. coords of a quadrature point).");
            return ret;
        } else if constexpr (CoordsTypeT == CoordsType::global)
        {
            static const std::string ret(
                "\n Function expects as arguments global coordinates (coordinates in the mesh).");
            return ret;
        } else
        {
            assert(false && "All possible values of CoordsType should be handled explicitly here.");
            exit(EXIT_FAILURE);
        }
    }


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_SPATIALFUNCTION_DOT_HXX_
// *** MoReFEM end header guards *** < //
