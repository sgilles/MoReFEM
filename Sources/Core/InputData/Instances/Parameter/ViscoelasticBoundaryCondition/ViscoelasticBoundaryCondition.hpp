// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_VISCOELASTICBOUNDARYCONDITION_VISCOELASTICBOUNDARYCONDITION_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_VISCOELASTICBOUNDARYCONDITION_VISCOELASTICBOUNDARYCONDITION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Core/InputData/Instances/Parameter/Advanced/Parameter.hpp" // IWYU pragma: export
#include "Utilities/InputData/Advanced/Crtp/Section.hpp"             // IWYU pragma: export


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section
    struct ViscoelasticBoundaryCondition
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<ViscoelasticBoundaryCondition,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {

        /*!
         * \brief Return the name of the section in the input datum ('ViscoelasticBoundaryCondition' here).
         *
         */
        static const std::string& GetName();

        //! Convenient alias.
        using self = ViscoelasticBoundaryCondition;

        //! Friendship to section parent.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        /*!
         * \brief Choose how is described the  (through a scalar, a function, etc...)
         */
        struct Damping : public Internal::InputDataNS::ParamNS::ScalarParameter<Damping, ViscoelasticBoundaryCondition>
        {


            //! Convenient alias.
            using self = Damping;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, ViscoelasticBoundaryCondition>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g.
            //! ViscoelasticBoundaryCondition.poisson_ratio).
            static const std::string& GetName();

        }; // struct Damping


        /*!
         * \brief Choose how is described the  (through a scalar, a function, etc...)
         */
        struct Stiffness
        : public Internal::InputDataNS::ParamNS::ScalarParameter<Stiffness, ViscoelasticBoundaryCondition>
        {


            //! Convenient alias.
            using self = Stiffness;

            //! Alias to the parent class which defines the skeleton of a \ a Parameter in an input data file.
            using parent = Internal::InputDataNS::ParamNS::ScalarParameter<self, ViscoelasticBoundaryCondition>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            //! Friendship to an underlying internal class, which is required for the internal mechanics of
            //! interpreting the content of input data file.
            friend typename parent::section_type;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            //! Return the moniker of the section in the input data file (e.g.
            //! ViscoelasticBoundaryCondition.poisson_ratio).
            static const std::string& GetName();

        }; // struct Stiffness


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
                Damping,
                Stiffness
            >;
        // clang-format on

      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct ViscoelasticBoundaryCondition


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Core/InputData/Instances/Parameter/ViscoelasticBoundaryCondition/ViscoelasticBoundaryCondition.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_VISCOELASTICBOUNDARYCONDITION_VISCOELASTICBOUNDARYCONDITION_DOT_HPP_
// *** MoReFEM end header guards *** < //
