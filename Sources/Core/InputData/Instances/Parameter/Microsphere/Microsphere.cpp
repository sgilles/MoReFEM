// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/Parameter/Microsphere/Microsphere.hpp"


namespace MoReFEM::InputDataNS
{


    const std::string& Microsphere::GetName()
    {
        static const std::string ret("Microsphere");
        return ret;
    }


    const std::string& Microsphere::InPlaneFiberDispersionI4::GetName()
    {
        static const std::string ret("InPlaneFiberDispersionI4");
        return ret;
    }


    const std::string& Microsphere::OutOfPlaneFiberDispersionI4::GetName()
    {
        static const std::string ret("OutOfPlaneFiberDispersionI4");
        return ret;
    }


    const std::string& Microsphere::FiberStiffnessDensityI4::GetName()
    {
        static const std::string ret("FiberStiffnessDensityI4");
        return ret;
    }


    const std::string& Microsphere::InPlaneFiberDispersionI6::GetName()
    {
        static const std::string ret("InPlaneFiberDispersionI6");
        return ret;
    }


    const std::string& Microsphere::OutOfPlaneFiberDispersionI6::GetName()
    {
        static const std::string ret("OutOfPlaneFiberDispersionI6");
        return ret;
    }


    const std::string& Microsphere::FiberStiffnessDensityI6::GetName()
    {
        static const std::string ret("FiberStiffnessDensityI6");
        return ret;
    }


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
