// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_SOURCE_RECTANGULARSOURCETIMEPARAMETER_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_SOURCE_RECTANGULARSOURCETIMEPARAMETER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Core/InputData/Instances/Parameter/Source/Internal/RectangularSourceTimeParameter.hpp"
#include "Utilities/InputData/Advanced/InputData.hpp"


namespace MoReFEM::InputDataNS
{

    struct RectangularSourceTimeParameterTag
    { };


    //! \copydoc doxygen_hide_core_input_data_section_with_index
    // clang-format off
    template<std::size_t IndexT>
    class RectangularSourceTimeParameter
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection
    <
        RectangularSourceTimeParameter<IndexT>,
        IndexT,
        RectangularSourceTimeParameterTag,
        ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection
    >
    // clang-format on
    {
      public:
        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "RectangularSourceTimeParameter";
        }

        //! Convenient alias.
        using self = RectangularSourceTimeParameter<IndexT>;


        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        //! Initial time of the horizontal source in ReactionDiffusion.
        struct InitialTimeOfActivationList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<InitialTimeOfActivationList, self, double>,
          public ::MoReFEM::Internal::InputDataNS::RectangularSourceTimeParameterNS::InitialTimeOfActivationList
        { };


        //! Initial time of the horizontal source in ReactionDiffusion.
        struct FinalTimeOfActivationList
        : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<FinalTimeOfActivationList, self, double>,
          public ::MoReFEM::Internal::InputDataNS::RectangularSourceTimeParameterNS::FinalTimeOfActivationList
        { };


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
                InitialTimeOfActivationList,
                FinalTimeOfActivationList
            >;
        // clang-format on

      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct RectangularSourceTimeParameter


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_SOURCE_RECTANGULARSOURCETIMEPARAMETER_DOT_HPP_
// *** MoReFEM end header guards *** < //
