// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_HEART_HEART_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_HEART_HEART_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <tuple>
#include <type_traits> // IWYU pragma: keep

#include "Utilities/InputData/Advanced/InputData.hpp"


namespace MoReFEM::InputDataNS
{


    //! \copydoc doxygen_hide_core_input_data_section
    struct Heart
    : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<Heart,
                                                             ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {

        /*!
         * \brief Return the name of the section in the input datum ('Heart' here).
         *
         */
        static const std::string& GetName();

        //! Convenient alias.
        using self = Heart;

        //! Friendship to section parent.
        using parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        //! \copydoc doxygen_hide_core_input_data_section
        struct Time : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<Time, Heart>
        {

            /*!
             * \brief Return the name of the section in the input datum ('Time' here).
             *
             */
            static const std::string& GetName();

            //! Convenient alias.
            using self = Time;

            //! Friendship to section parent.
            using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, Heart>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            /*!
             * \brief Choose how is described the parameter (through a scalar, a function, etc...)
             */
            struct HeartBeatDuration
            : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<HeartBeatDuration, self, double>
            {


                //! Name of the input datum in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input datum.
                static const std::string& Description();


                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no
                 * value has been given in the input data file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a
                 * OptionFile-formatted.
                 */
                static const std::string& DefaultValue();

            }; // struct HeartBeatDuration

            //! Alias to the tuple of structs.
            // clang-format off
                using section_content_type = std::tuple
                <
                    HeartBeatDuration
                >;
            // clang-format on

          private:
            //! Content of the section.
            section_content_type section_content_;

        }; // struct Time


        //! \copydoc doxygen_hide_core_input_data_section
        struct Geometry : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<Geometry, Heart>
        {

            /*!
             * \brief Return the name of the section in the input datum ('Geometry' here).
             *
             */
            static const std::string& GetName();

            //! Convenient alias.
            using self = Geometry;

            //! Friendship to section parent.
            using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, Heart>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            /*!
             * \brief Choose how is described the parameter (through a scalar, a function, etc...)
             */
            struct Velocity : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Velocity, self, double>
            {


                //! Name of the input datum in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input datum.
                static const std::string& Description();


                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no
                 * value has been given in the input data file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a
                 * OptionFile-formatted.
                 */
                static const std::string& DefaultValue();

            }; // struct Velocity


            /*!
             * \brief Choose how is described the parameter (through a scalar, a function, etc...)
             */
            struct VelocitySlow : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<VelocitySlow, self, double>
            {


                //! Name of the input datum in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input datum.
                static const std::string& Description();


                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no
                 * value has been given in the input data file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a
                 * OptionFile-formatted.
                 */
                static const std::string& DefaultValue();

            }; // struct Velocity

            //! Alias to the tuple of structs.
            // clang-format off
                using section_content_type = std::tuple
                <
                    Velocity,
                    VelocitySlow
                >;
            // clang-format on

          private:
            //! Content of the section.
            section_content_type section_content_;

        }; // struct Geometry

        //! \copydoc doxygen_hide_core_input_data_section
        struct LongAxis : public ::MoReFEM::Advanced::InputDataNS::Crtp::Section<LongAxis, Heart>
        {

            /*!
             * \brief Return the name of the section in the input datum.
             *
             */
            static const std::string& GetName();

            //! Convenient alias.
            using self = LongAxis;

            //! Friendship to section parent.
            using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, Heart>;

            static_assert(std::is_convertible<self*, parent*>());

            //! \cond IGNORE_BLOCK_IN_DOXYGEN
            friend parent;
            //! \endcond IGNORE_BLOCK_IN_DOXYGEN

            /*!
             * \brief Choose how is described the  (through a scalar, a function, etc...)
             */
            struct X : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<X, self, double>
            {

                //! Name of the input datum in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input datum.
                static const std::string& Description();


                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no
                 * value has been given in the input data file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a
                 * OptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };

            /*!
             * \brief Choose how is described the  (through a scalar, a function, etc...)
             */
            struct Y : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Y, self, double>
            {

                //! Name of the input datum in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input datum.
                static const std::string& Description();


                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no
                 * value has been given in the input data file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a
                 * OptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };

            /*!
             * \brief Choose how is described the  (through a scalar, a function, etc...)
             */
            struct Z : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Z, self, double>
            {

                //! Name of the input datum in Lua input file.
                static const std::string& NameInFile();

                //! Description of the input datum.
                static const std::string& Description();


                /*!
                 * \return Default value.
                 *
                 * This is intended to be used only when the class is used to create a default file; never when no
                 * value has been given in the input data file (doing so is too much error prone...)
                 *
                 * This is given as a string; if no default value return an empty string. The value must be \a
                 * OptionFile-formatted.
                 */
                static const std::string& DefaultValue();
            };


            //! Alias to the tuple of structs.
            // clang-format off
                using section_content_type = std::tuple
                <
                    X,
                    Y,
                    Z
                >;
            // clang-format on

          private:
            //! Content of the section.
            section_content_type section_content_;


        }; // struct LongAxis


        //! Alias to the tuple of structs.
        // clang-format off
            using section_content_type = std::tuple
            <
                Time,
                Geometry,
                LongAxis
            >;
        // clang-format on

      private:
        //! Content of the section.
        section_content_type section_content_;

    }; // struct Heart


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_PARAMETER_HEART_HEART_DOT_HPP_
// *** MoReFEM end header guards *** < //
