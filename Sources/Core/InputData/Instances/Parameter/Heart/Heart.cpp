// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>


#include "Core/InputData/Instances/Parameter/Heart/Heart.hpp"


namespace MoReFEM::InputDataNS
{


    const std::string& Heart::GetName()
    {
        static const std::string ret("Heart");
        return ret;
    }


    const std::string& Heart::Time::GetName()
    {
        static const std::string ret("Time");
        return ret;
    }


    const std::string& Heart::Time::HeartBeatDuration::NameInFile()
    {
        static const std::string ret("HeartBeatDuration");
        return ret;
    }


    const std::string& Heart::Time::HeartBeatDuration::Description()
    {
        static const std::string ret("Heart beat duration.");
        return ret;
    }


    const std::string& Heart::Time::HeartBeatDuration::DefaultValue()
    {
        static const std::string ret("0.8");
        return ret;
    }


    const std::string& Heart::Geometry::GetName()
    {
        static const std::string ret("Geometry");
        return ret;
    }


    const std::string& Heart::Geometry::Velocity::NameInFile()
    {
        static const std::string ret("Velocity");
        return ret;
    }


    const std::string& Heart::Geometry::Velocity::Description()
    {
        static const std::string ret("Velocity of electrical activation.");
        return ret;
    }

    const std::string& Heart::Geometry::Velocity::DefaultValue()
    {
        static const std::string ret("0.5");
        return ret;
    }

    const std::string& Heart::Geometry::VelocitySlow::NameInFile()
    {
        static const std::string ret("VelocitySlow");
        return ret;
    }


    const std::string& Heart::Geometry::VelocitySlow::Description()
    {
        static const std::string ret("Slow velocity of electrical activation.");
        return ret;
    }

    const std::string& Heart::Geometry::VelocitySlow::DefaultValue()
    {
        static const std::string ret("60.");
        return ret;
    }


    const std::string& Heart::LongAxis::GetName()
    {
        static const std::string ret("LongAxis");
        return ret;
    }

    const std::string& Heart::LongAxis::X::NameInFile()
    {
        static const std::string ret("x");
        return ret;
    }


    const std::string& Heart::LongAxis::X::Description()
    {
        static const std::string ret("x coordinate of the long axis.");
        return ret;
    }

    const std::string& Heart::LongAxis::X::DefaultValue()
    {
        static const std::string ret("0.");
        return ret;
    }

    const std::string& Heart::LongAxis::Y::NameInFile()
    {
        static const std::string ret("y");
        return ret;
    }


    const std::string& Heart::LongAxis::Y::Description()
    {
        static const std::string ret("y coordinate of the long axis.");
        return ret;
    }

    const std::string& Heart::LongAxis::Y::DefaultValue()
    {
        static const std::string ret("0.");
        return ret;
    }

    const std::string& Heart::LongAxis::Z::NameInFile()
    {
        static const std::string ret("z");
        return ret;
    }


    const std::string& Heart::LongAxis::Z::Description()
    {
        static const std::string ret("z coordinate of the long axis.");
        return ret;
    }


    const std::string& Heart::LongAxis::Z::DefaultValue()
    {
        static const std::string ret("0.");
        return ret;
    }


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
