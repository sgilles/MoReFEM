// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/Geometry/Internal/Domain.hpp"


namespace MoReFEM::Internal::InputDataNS::DomainNS
{


    const std::string& MeshIndexList::NameInFile()
    {
        static const std::string ret("mesh_index");
        return ret;
    }


    const std::string& MeshIndexList::Description()
    {
        static const std::string ret("Index of the geometric mesh upon which the domain is defined (as defined "
                                     "in the present file). Might be left empty if domain not limited to one mesh; "
                                     "at most one value is expected here.");

        return ret;
    }


    const std::string& DimensionList::NameInFile()
    {
        static const std::string ret("dimension_list");
        return ret;
    }


    const std::string& DimensionList::Description()
    {
        static const std::string ret("List of dimensions encompassed by the domain. Might be left empty if no "
                                     "restriction at all upon dimensions.");
        return ret;
    }


    const std::string& DimensionList::Constraint()
    {
        static const std::string ret("value_in(v, {0, 1, 2, 3})");
        return ret;
    }


    const std::string& MeshLabelList::NameInFile()
    {
        static const std::string ret("mesh_label_list");
        return ret;
    }


    const std::string& MeshLabelList::Description()
    {
        static const std::string ret("List of mesh labels encompassed by the domain. Might be left empty if no "
                                     "restriction at all upon mesh labels. This parameter does not make sense "
                                     "if no mesh is defined for the domain.");

        return ret;
    }


    const std::string& GeomEltTypeList::NameInFile()
    {
        static const std::string ret("geometric_element_type_list");
        return ret;
    }


    const std::string& GeomEltTypeList::Description()
    {
        static const std::string ret(
            "List of geometric element types considered in the domain. Might be left "
            "empty if no restriction upon these. No constraint is applied at \a OptionFile level, "
            "as some geometric element types could be added after generation of current "
            "input data file. Current list is below; if an incorrect value is "
            "put there it will be detected a bit later when the domain object is built.\n "
            "The known types when this file was generated are: \n "
            ". Point1\n "
            ". Segment2, Segment3\n "
            ". Triangle3, Triangle6\n "
            ". Quadrangle4, Quadrangle8, Quadrangle9\n "
            ". Tetrahedron4, Tetrahedron10\n "
            ". Hexahedron8, Hexahedron20, Hexahedron27.");
        return ret;
    }


} // namespace MoReFEM::Internal::InputDataNS::DomainNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
