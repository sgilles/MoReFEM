// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/Geometry/Internal/Mesh.hpp"


namespace MoReFEM::Internal::InputDataNS::MeshNS
{


    const std::string& PathImpl::NameInFile()
    {
        static const std::string ret("mesh");
        return ret;
    }


    const std::string& PathImpl::Description()
    {
        static const std::string ret("Path of the mesh file to use.\nIMPORTANT: if run from preprocessed data, "
                                     "this path won't be used (the already cut mesh will be used instead).");
        return ret;
    }


    const std::string& FormatImpl::NameInFile()
    {
        static const std::string ret("format");
        return ret;
    }

    const std::string& FormatImpl::Description()
    {
        static const std::string ret("Format of the input mesh.");
        return ret;
    }


    const std::string& FormatImpl::Constraint()
    {
        static const std::string ret("value_in(v, {'Ensight', 'Medit'})");
        return ret;
    }


    const std::string& FormatImpl::DefaultValue()
    {
        static const std::string ret("\"Medit\"");
        return ret;
    }


    const std::string& DimensionImpl::NameInFile()
    {
        static const std::string ret("dimension");
        return ret;
    }


    const std::string& DimensionImpl::Description()
    {
        static const std::string ret(
            "Highest dimension of the input mesh. This dimension might be lower than the one "
            "effectively read in the mesh file; in which case Coords will be reduced provided all the "
            "dropped "
            "values are 0. If not, an exception is thrown.");
        return ret;
    }


    const std::string& DimensionImpl::Constraint()
    {
        static const std::string ret("v <= 3 and v > 0");
        return ret;
    }

    const std::string& SpaceUnitImpl::NameInFile()
    {
        static const std::string ret("space_unit");
        return ret;
    }


    const std::string& SpaceUnitImpl::Description()
    {
        static const std::string ret("Space unit of the mesh.");
        return ret;
    }

    const std::string& SpaceUnitImpl::DefaultValue()
    {
        static const std::string ret("1.");
        return ret;
    }


} // namespace MoReFEM::Internal::InputDataNS::MeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
