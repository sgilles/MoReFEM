// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <string>

#include "Core/InputData/Instances/Reaction/FitzHughNagumo.hpp"


namespace MoReFEM::InputDataNS::ReactionNS
{


    const std::string& FitzHughNagumo::GetName()
    {
        static const std::string ret("ReactionFitzHughNagumo");
        return ret;
    };


    const std::string& FitzHughNagumo::ACoefficient::NameInFile()
    {
        static const std::string ret("a");
        return ret;
    }


    const std::string& FitzHughNagumo::ACoefficient::Description()
    {
        static const std::string ret("Value of a in FitzHughNagumo.");
        return ret;
    }


    const std::string& FitzHughNagumo::ACoefficient::DefaultValue()
    {
        static const std::string ret("0.08");
        return ret;
    }


    const std::string& FitzHughNagumo::BCoefficient::NameInFile()
    {
        static const std::string ret("b");
        return ret;
    }


    const std::string& FitzHughNagumo::BCoefficient::Description()
    {
        static const std::string ret("Value of b in FitzHughNagumo.");
        return ret;
    }

    const std::string& FitzHughNagumo::BCoefficient::DefaultValue()
    {
        static const std::string ret("0.7");
        return ret;
    }


    const std::string& FitzHughNagumo::CCoefficient::NameInFile()
    {
        static const std::string ret("c");
        return ret;
    }


    const std::string& FitzHughNagumo::CCoefficient::Description()
    {
        static const std::string ret("Value of c in FitzHughNagumo.");
        return ret;
    }

    const std::string& FitzHughNagumo::CCoefficient::DefaultValue()
    {
        static const std::string ret("0.8");
        return ret;
    }


} // namespace MoReFEM::InputDataNS::ReactionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
