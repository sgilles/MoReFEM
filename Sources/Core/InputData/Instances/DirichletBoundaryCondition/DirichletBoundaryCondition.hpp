// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_CORE_INPUTDATA_INSTANCES_DIRICHLETBOUNDARYCONDITION_DIRICHLETBOUNDARYCONDITION_DOT_HPP_
#define MOREFEM_CORE_INPUTDATA_INSTANCES_DIRICHLETBOUNDARYCONDITION_DIRICHLETBOUNDARYCONDITION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/InputData/Advanced/InputData.hpp"

#include "Core/InputData/Instances/DirichletBoundaryCondition/Internal/DirichletBoundaryCondition.hpp"
#include "Core/InputData/Instances/Geometry/Domain.hpp"


namespace MoReFEM::InputDataNS
{

    //! \copydoc doxygen_hide_core_input_data_section_with_index
    template<std::size_t IndexT>
    struct DirichletBoundaryCondition : public ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
                                            DirichletBoundaryCondition<IndexT>,
                                            IndexT,
                                            Internal::InputDataNS::BoundaryConditionNS::Tag,
                                            ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>
    {

        //! \copydoc doxygen_hide_indexed_section_basename
        static std::string BaseName()
        {
            return "EssentialBoundaryCondition";
        }

        //! Convenient alias.
        using self = DirichletBoundaryCondition<IndexT>;

        //! \copydoc doxygen_hide_indexed_section_parent
        using parent = ::MoReFEM::Advanced::InputDataNS::Crtp::IndexedSection<
            DirichletBoundaryCondition<IndexT>,
            IndexT,
            Internal::InputDataNS::BoundaryConditionNS::Tag,
            ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        //! \copydoc doxygen_hide_indexed_section_grand_parent
        using grand_parent =
            ::MoReFEM::Advanced::InputDataNS::Crtp::Section<self, ::MoReFEM::Advanced::InputDataNS::NoEnclosingSection>;

        static_assert(std::is_convertible<self*, parent*>());
        static_assert(std::is_convertible<self*, grand_parent*>());

        //! \cond IGNORE_BLOCK_IN_DOXYGEN
        friend grand_parent;
        //! \endcond IGNORE_BLOCK_IN_DOXYGEN


        //! Indicates the domain upon which the finite element space is defined. This domain must be
        //! uni-dimensional.
        struct Component : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Component, self, std::string>,
                           public ::MoReFEM::Internal::InputDataNS::BoundaryConditionNS::Component
        {
            //! \copydoc doxygen_hide_alias_to_print_model_settings_in_lua_file
            using model_settings_token = typename parent::IndexedSectionDescription;
        };


        //! Indicates which unknowns are defined on the finite element space.
        struct UnknownName : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<UnknownName, self, std::string>,
                             public ::MoReFEM::Internal::InputDataNS::BoundaryConditionNS::UnknownName
        {
            //! \copydoc doxygen_hide_alias_to_print_model_settings_in_lua_file
            using model_settings_token = typename parent::IndexedSectionDescription;
        };


        //! Indicates for each unknowns the shape function to use.
        struct Values : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<Values, self, std::vector<double>>,
                        public ::MoReFEM::Internal::InputDataNS::BoundaryConditionNS::Values
        {
            //! \copydoc doxygen_hide_alias_to_print_model_settings_in_lua_file
            using model_settings_token = typename parent::IndexedSectionDescription;
        };


        //! Indicates the numbering subset to use for each unknown.
        struct DomainIndex : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<DomainIndex, self, std::size_t>,
                             public ::MoReFEM::Internal::InputDataNS::BoundaryConditionNS::DomainIndex
        {
            //! \copydoc doxygen_hide_alias_to_print_model_settings_in_lua_file
            using model_settings_token = typename parent::IndexedSectionDescription;
        };


        //! Indicates whether the boundary condition values might change at each time iteration,
        struct IsMutable : public ::MoReFEM::Advanced::InputDataNS::Crtp::Leaf<IsMutable, self, bool>,
                           public ::MoReFEM::Internal::InputDataNS::BoundaryConditionNS::IsMutable
        {
            //! \copydoc doxygen_hide_alias_to_print_model_settings_in_lua_file
            using model_settings_token = typename parent::IndexedSectionDescription;
        };


        //! Alias to the tuple of structs.
        // clang-format off
        using section_content_type = std::tuple
        <
            Component,
            UnknownName,
            Values,
            DomainIndex,
            IsMutable
        >;
        // clang-format on


      private:
        //! Content of the section.
        section_content_type section_content_;


    }; // struct DirichletBoundaryCondition


} // namespace MoReFEM::InputDataNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //

/*!
 * \brief Macro used in most models when defining a \a NumberingSubset in the \a ModelSettings.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 */
#define MOST_USUAL_MODEL_SETTINGS_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(enum_class_id)                               \
    ::MoReFEM::InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(enum_class_id)>::IndexedSectionDescription,  \
        ::MoReFEM::InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(enum_class_id)>::UnknownName,            \
        ::MoReFEM::InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(enum_class_id)>::DomainIndex,            \
        ::MoReFEM::InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(enum_class_id)>::IsMutable

/*!
 * \brief Macro used in most models when defining a \a Mesh in the \a InputData.
 *
 * \copydoc doxygen_hide_enum_class_id_for_input_data_macro
 *
 */
#define MOST_USUAL_INPUT_DATA_FIELDS_FOR_DIRICHLET_BOUNDARY_CONDITION(enum_class_id)                                   \
    ::MoReFEM::InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(enum_class_id)>::Component,                  \
        ::MoReFEM::InputDataNS::DirichletBoundaryCondition<EnumUnderlyingType(enum_class_id)>::Values


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_CORE_INPUTDATA_INSTANCES_DIRICHLETBOUNDARYCONDITION_DIRICHLETBOUNDARYCONDITION_DOT_HPP_
// *** MoReFEM end header guards *** < //
