// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup CoreGroup
 * \addtogroup CoreGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <filesystem>
#include <iosfwd> // for istringstream
#include <sstream>
#include <string_view>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/String/String.hpp"

#include "ThirdParty/Wrappers/Mpi/Mpi.hpp"

#include "Core/InterpretOutputFiles/Exceptions/Exception.hpp"
#include "Core/InterpretOutputFiles/TimeIteration/TimeIteration.hpp"


namespace MoReFEM::InterpretOutputFilesNS::Data
{


    TimeIteration::TimeIteration(const std::string& line)
    {
        std::istringstream iconv(line);

        iconv >> time_iteration_;

        if (iconv.fail())
            throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInLine(line);

        iconv.ignore(); // for ';'

        iconv >> time_;

        if (iconv.fail())
            throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInLine(line);

        iconv.ignore(); // for ';'

        std::size_t numbering_subset_id_as_int;
        iconv >> numbering_subset_id_as_int;
        numbering_subset_id_ = NumberingSubsetNS::unique_id{ numbering_subset_id_as_int };

        if (iconv.fail())
            throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInLine(line);

        iconv.ignore(); // for ';'

        std::string solution_filename;

        iconv >> solution_filename;

        if (iconv.fail())
            throw ExceptionNS::InterpretOutputFilesNS::InvalidFormatInLine(line);

        solution_filename_ = FilesystemNS::File{ solution_filename };
    }


    FilesystemNS::File TimeIteration::GetSolutionFilename(const Wrappers::Mpi& mpi) const noexcept
    {
        decltype(auto) file_with_wildcard = GetSolutionFilename();

        auto string = static_cast<std::string>(file_with_wildcard);

        auto Nreplacements = Utilities::String::Replace("*", std::to_string(mpi.GetRank<int>()), string);
        static_cast<void>(Nreplacements);
        assert(Nreplacements == 1ul);

        return FilesystemNS::File{ string };
    }


    void TimeIteration::Print(std::ostream& stream) const
    {
        std::vector<std::string> content;

        content.push_back(std::to_string(GetIteration()));
        content.push_back(std::to_string(GetTime()));
        content.push_back(std::to_string(GetNumberingSubsetId().Get()));
        content.push_back(static_cast<std::string>(GetSolutionFilename()));

        Utilities::PrintContainer<>::Do(content,
                                        stream,
                                        PrintNS::Delimiter::separator{ ";" },
                                        PrintNS::Delimiter::opener{ "" },
                                        PrintNS::Delimiter::closer{ "" });
    }


    std::ostream& operator<<(std::ostream& out, const TimeIteration& rhs)
    {
        rhs.Print(out);
        return out;
    }


} // namespace MoReFEM::InterpretOutputFilesNS::Data


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup CoreGroup
// *** MoReFEM Doxygen end of group *** < //
