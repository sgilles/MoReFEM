// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_UPDATECAUCHYGREENTENSOR_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_UPDATECAUCHYGREENTENSOR_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/ParameterOperator/UpdateCauchyGreenTensor.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "OperatorInstances/ParameterOperator/UpdateCauchyGreenTensor.hpp"


#include <tuple>

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#include "Operators/GlobalVariationalOperator/ExtractLocalDofValues.hpp"
#include "Operators/ParameterOperator/GlobalParameterOperator/GlobalParameterOperator.hpp" // IWYU pragma: export

#include "OperatorInstances/ParameterOperator/Local/UpdateCauchyGreenTensor.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GlobalVector; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::GlobalParameterOperatorNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    UpdateCauchyGreenTensor<TimeManagerT>::UpdateCauchyGreenTensor(
        const FEltSpace& felt_space,
        const Unknown& unknown,
        param_at_quad_pt_type& cauchy_green_tensor,
        const QuadratureRulePerTopology* const quadrature_rule_per_topology)
    : parent(felt_space, unknown, quadrature_rule_per_topology, AllocateGradientFEltPhi::yes, cauchy_green_tensor)
    {
        decltype(auto) extended_unknown_ptr = felt_space.GetExtendedUnknownPtr(unknown);

        felt_space.ComputeLocal2Global(std::move(extended_unknown_ptr), DoComputeProcessorWiseLocal2Global::yes);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& UpdateCauchyGreenTensor<TimeManagerT>::ClassName()
    {
        static std::string name("UpdateCauchyGreenTensor");
        return name;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline void UpdateCauchyGreenTensor<TimeManagerT>::Update(const GlobalVector& current_solid_displacement) const
    {
        return parent::UpdateImpl(current_solid_displacement);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline void UpdateCauchyGreenTensor<TimeManagerT>::SetComputeEltArrayArguments(
        const LocalFEltSpace& local_felt_space,
        local_operator_type& local_operator,
        const std::tuple<const GlobalVector&>& additional_args) const
    {
        const GlobalVector& solid_displacement = std::get<0>(additional_args);

        GlobalVariationalOperatorNS::ExtractLocalDofValues(local_felt_space,
                                                           this->GetExtendedUnknown(),
                                                           solid_displacement,
                                                           local_operator.GetNonCstIncrementLocalDisplacement());
    }


} // namespace MoReFEM::GlobalParameterOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_UPDATECAUCHYGREENTENSOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
