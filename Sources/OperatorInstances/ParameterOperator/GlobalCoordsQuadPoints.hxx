// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_GLOBALCOORDSQUADPOINTS_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_GLOBALCOORDSQUADPOINTS_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/ParameterOperator/GlobalCoordsQuadPoints.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "OperatorInstances/ParameterOperator/GlobalCoordsQuadPoints.hpp"


#include <tuple>

#include "FiniteElement/FiniteElementSpace/FEltSpace.hpp"

#include "Operators/ParameterOperator/GlobalParameterOperator/GlobalParameterOperator.hpp"


namespace MoReFEM::GlobalParameterOperatorNS
{

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline void GlobalCoordsQuadPoints<TimeManagerT>::Update() const
    {
        return parent::UpdateImpl();
    }

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline void
    GlobalCoordsQuadPoints<TimeManagerT>::SetComputeEltArrayArguments(const LocalFEltSpace& local_felt_space,
                                                                      local_operator_type& local_operator,
                                                                      const std::tuple<>&&) const
    {
        static_cast<void>(local_felt_space);
        static_cast<void>(local_operator);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    GlobalCoordsQuadPoints<TimeManagerT>::GlobalCoordsQuadPoints(
        const FEltSpace& felt_space,
        const Unknown& unknown,
        const QuadratureRulePerTopology* const quadrature_rule_per_topology,
        param_at_quad_pt_type& global_coords_quad_pt)
    // clang-format on
    : parent(felt_space, unknown, quadrature_rule_per_topology, AllocateGradientFEltPhi::yes, global_coords_quad_pt)
    {
        decltype(auto) extended_unknown_ptr = felt_space.GetExtendedUnknownPtr(unknown);

        felt_space.ComputeLocal2Global(std::move(extended_unknown_ptr), DoComputeProcessorWiseLocal2Global::yes);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& GlobalCoordsQuadPoints<TimeManagerT>::ClassName()
    {
        static std::string name("GlobalCoordsQuadPoints");
        return name;
    }


} // namespace MoReFEM::GlobalParameterOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_GLOBALCOORDSQUADPOINTS_DOT_HXX_
// *** MoReFEM end header guards *** < //
