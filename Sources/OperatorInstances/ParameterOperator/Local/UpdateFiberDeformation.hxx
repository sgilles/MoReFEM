// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_UPDATEFIBERDEFORMATION_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_UPDATEFIBERDEFORMATION_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/ParameterOperator/Local/UpdateFiberDeformation.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "OperatorInstances/ParameterOperator/Local/UpdateFiberDeformation.hpp"


#include <vector>

#include "Core/Parameter/TypeEnum.hpp"

#include "Parameters/ParameterAtQuadraturePoint.hpp"
#include "Parameters/TimeDependency/None.hpp"

namespace MoReFEM::LocalParameterOperatorNS
{

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    UpdateFiberDeformation<TimeManagerT>::UpdateFiberDeformation(
        const ExtendedUnknown::const_shared_ptr& a_unknown_storage,
        elementary_data_type&& a_elementary_data,
        scalar_param_at_quad_pt_type& fiber_deformation,
        const scalar_param_at_quad_pt_type& contraction_rheology_residual,
        const vectorial_param_at_quad_pt_type& schur_complement)

    : parent(a_unknown_storage, std::move(a_elementary_data), fiber_deformation),
      contraction_rheology_residual_(contraction_rheology_residual), schur_complement_(schur_complement)
    {
        const auto& elementary_data = parent::GetElementaryData();

        increment_local_displacement_.resize(elementary_data.NdofRow());
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& UpdateFiberDeformation<TimeManagerT>::ClassName()
    {
        static std::string name("UpdateFiberDeformation");
        return name;
    }

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void UpdateFiberDeformation<TimeManagerT>::ComputeEltArray()
    {
        auto& elementary_data = parent::GetNonCstElementaryData();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        const auto& increment_local_displacement = GetIncrementLocalDisplacement();

        const auto& ref_felt = elementary_data.GetRefFElt(parent::GetExtendedUnknown());

        const auto Ncomponent = Advanced::ComponentNS::index_type{ elementary_data.GetMeshDimension() };

        const std::size_t Nnode = ref_felt.Nnode();

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            const double residual = GetContractionRheologyResidual().GetValue(quad_pt, geom_elt);

            const auto& schur = GetSchurComplement().GetValue(quad_pt, geom_elt);

            double result_scalar_product = 0.;

            for (std::size_t node_index = 0; node_index < Nnode; ++node_index)
            {
                auto dof_index = node_index;

                for (Advanced::ComponentNS::index_type component{ 0ul }; component < Ncomponent;
                     ++component, dof_index += Nnode)
                {
                    result_scalar_product += schur(dof_index) * increment_local_displacement[dof_index];
                }
            }

            auto functor = [residual, result_scalar_product](double& fiber_deformation)
            {
                fiber_deformation += -(residual + result_scalar_product);
            };

            parent::GetNonCstParameter().UpdateValue(quad_pt, geom_elt, functor);
        }
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline const std::vector<double>&
    UpdateFiberDeformation<TimeManagerT>::GetIncrementLocalDisplacement() const noexcept
    {
        return increment_local_displacement_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline std::vector<double>& UpdateFiberDeformation<TimeManagerT>::GetNonCstIncrementLocalDisplacement() noexcept
    {
        return const_cast<std::vector<double>&>(GetIncrementLocalDisplacement());
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto UpdateFiberDeformation<TimeManagerT>::GetContractionRheologyResidual() const noexcept
        -> const scalar_param_at_quad_pt_type&
    {
        return contraction_rheology_residual_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto
    UpdateFiberDeformation<TimeManagerT>::GetSchurComplement() const noexcept -> const vectorial_param_at_quad_pt_type&
    {
        return schur_complement_;
    }


} // namespace MoReFEM::LocalParameterOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_UPDATEFIBERDEFORMATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
