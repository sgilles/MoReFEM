// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_UPDATECAUCHYGREENTENSOR_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_UPDATECAUCHYGREENTENSOR_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/ParameterOperator/Local/UpdateCauchyGreenTensor.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "OperatorInstances/ParameterOperator/Local/UpdateCauchyGreenTensor.hpp"


#include <vector>

#include "Utilities/LinearAlgebra/LocalAlias.hpp"

#include "Operators/LocalVariationalOperator/Advanced/GradientDisplacementMatrix.hpp"


namespace MoReFEM::LocalParameterOperatorNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    UpdateCauchyGreenTensor<TimeManagerT>::UpdateCauchyGreenTensor(
        const ExtendedUnknown::const_shared_ptr& a_unknown_storage,
        elementary_data_type&& a_elementary_data,
        param_at_quad_pt_type& cauchy_green_tensor)
    : parent(a_unknown_storage, std::move(a_elementary_data), cauchy_green_tensor)
    {
        const auto& elementary_data = parent::GetElementaryData();

        increment_local_displacement_.resize(elementary_data.NdofRow());

        const auto mesh_dimension = elementary_data.GetMeshDimension();

        gradient_displacement_.resize({ mesh_dimension, mesh_dimension });
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& UpdateCauchyGreenTensor<TimeManagerT>::ClassName()
    {
        static std::string name("UpdateCauchyGreenTensor<TimeManagerT>");
        return name;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void UpdateCauchyGreenTensor<TimeManagerT>::ComputeEltArray()
    {
        auto& elementary_data = parent::GetNonCstElementaryData();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        const auto& increment_local_displacement = GetIncrementLocalDisplacement();

        auto& gradient_displacement = GetNonCstGradientDisplacement();

        const auto mesh_dimension = elementary_data.GetMeshDimension();

        const auto& ref_felt = elementary_data.GetRefFElt(parent::GetExtendedUnknown());

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();

            assert(increment_local_displacement.size()
                   == static_cast<std::size_t>(quad_pt_unknown_data.Nnode() * infos_at_quad_pt.GetMeshDimension()));

            Advanced::OperatorNS::ComputeGradientDisplacementMatrix(
                quad_pt_unknown_data, ref_felt, increment_local_displacement, gradient_displacement);

            auto functor = [&gradient_displacement, mesh_dimension](LocalVector& cauchy_green_tensor)
            {
                switch (mesh_dimension)
                {
                case 1ul:
                    Internal::LocalParameterOperatorNS::Update1D(gradient_displacement, cauchy_green_tensor);
                    break;
                case 2ul:
                    Internal::LocalParameterOperatorNS::Update2D(gradient_displacement, cauchy_green_tensor);
                    break;
                case 3ul:
                    Internal::LocalParameterOperatorNS::Update3D(gradient_displacement, cauchy_green_tensor);
                    break;
                default:
                    assert(false);
                    exit(EXIT_FAILURE);
                }
            };

            parent::GetNonCstParameter().UpdateValue(quad_pt, geom_elt, functor);
        }
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline const std::vector<double>&
    UpdateCauchyGreenTensor<TimeManagerT>::GetIncrementLocalDisplacement() const noexcept
    {
        return increment_local_displacement_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline std::vector<double>& UpdateCauchyGreenTensor<TimeManagerT>::GetNonCstIncrementLocalDisplacement() noexcept
    {
        return const_cast<std::vector<double>&>(GetIncrementLocalDisplacement());
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline LocalMatrix& UpdateCauchyGreenTensor<TimeManagerT>::GetNonCstGradientDisplacement() noexcept
    {
        return gradient_displacement_;
    }


} // namespace MoReFEM::LocalParameterOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_UPDATECAUCHYGREENTENSOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
