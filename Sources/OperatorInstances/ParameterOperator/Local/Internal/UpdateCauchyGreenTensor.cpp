// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>

#include "Utilities/LinearAlgebra/LocalAlias.hpp"

#include "FiniteElement/Unknown/ExtendedUnknown.hpp"

#include "OperatorInstances/ParameterOperator/Local/Internal/UpdateCauchyGreenTensor.hpp"


namespace MoReFEM::Internal::LocalParameterOperatorNS
{


    void Update3D(const LocalMatrix& gradient_component_disp, LocalVector& cauchy_green_tensor)
    {
        using NumericNS::Square;

        assert(cauchy_green_tensor.size() == 6ul);

        // Component Cxx
        cauchy_green_tensor(0) = 1. + 2. * gradient_component_disp(0, 0) + Square(gradient_component_disp(0, 0))
                                 + Square(gradient_component_disp(1, 0)) + Square(gradient_component_disp(2, 0));

        // Component Cyy
        cauchy_green_tensor(1) = 1. + 2. * gradient_component_disp(1, 1) + Square(gradient_component_disp(0, 1))
                                 + Square(gradient_component_disp(1, 1)) + Square(gradient_component_disp(2, 1));

        // Component Czz
        cauchy_green_tensor(2) = 1. + 2. * gradient_component_disp(2, 2) + Square(gradient_component_disp(0, 2))
                                 + Square(gradient_component_disp(1, 2)) + Square(gradient_component_disp(2, 2));

        // Component Cxy
        cauchy_green_tensor(3) = gradient_component_disp(0, 1) + gradient_component_disp(1, 0)
                                 + gradient_component_disp(0, 0) * gradient_component_disp(0, 1)
                                 + gradient_component_disp(1, 0) * gradient_component_disp(1, 1)
                                 + gradient_component_disp(2, 0) * gradient_component_disp(2, 1);

        // Component Cyz
        cauchy_green_tensor(4) = gradient_component_disp(1, 2) + gradient_component_disp(2, 1)
                                 + gradient_component_disp(0, 2) * gradient_component_disp(0, 1)
                                 + gradient_component_disp(1, 2) * gradient_component_disp(1, 1)
                                 + gradient_component_disp(2, 2) * gradient_component_disp(2, 1);

        // Component Cxz
        cauchy_green_tensor(5) = gradient_component_disp(2, 0) + gradient_component_disp(0, 2)
                                 + gradient_component_disp(0, 2) * gradient_component_disp(0, 0)
                                 + gradient_component_disp(1, 2) * gradient_component_disp(1, 0)
                                 + gradient_component_disp(2, 2) * gradient_component_disp(2, 0);
    }


    void Update2D(const LocalMatrix& gradient_component_disp, LocalVector& cauchy_green_tensor)
    {
        using NumericNS::Square;

        assert(cauchy_green_tensor.size() == 3ul);

        // Component Cxx
        cauchy_green_tensor(0) = 1. + 2. * gradient_component_disp(0, 0) + Square(gradient_component_disp(0, 0))
                                 + Square(gradient_component_disp(1, 0));

        // Component Cyy
        cauchy_green_tensor(1) = 1. + 2. * gradient_component_disp(1, 1) + Square(gradient_component_disp(0, 1))
                                 + Square(gradient_component_disp(1, 1));

        // Component Cxy
        cauchy_green_tensor(2) = gradient_component_disp(0, 1) + gradient_component_disp(1, 0)
                                 + gradient_component_disp(0, 0) * gradient_component_disp(0, 1)
                                 + gradient_component_disp(1, 0) * gradient_component_disp(1, 1);
    }


    void Update1D(const LocalMatrix& gradient_component_disp, LocalVector& cauchy_green_tensor)
    {
        using NumericNS::Square;

        assert(cauchy_green_tensor.size() == 1ul);

        // Component Cxx
        cauchy_green_tensor(0) = 1. + 2. * gradient_component_disp(0, 0) + Square(gradient_component_disp(0, 0));
    }


} // namespace MoReFEM::Internal::LocalParameterOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
