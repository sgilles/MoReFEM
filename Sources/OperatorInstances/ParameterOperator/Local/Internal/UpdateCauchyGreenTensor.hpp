// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_INTERNAL_UPDATECAUCHYGREENTENSOR_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_INTERNAL_UPDATECAUCHYGREENTENSOR_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "Utilities/LinearAlgebra/LocalAlias.hpp"


namespace MoReFEM::Internal::LocalParameterOperatorNS
{


    /*!
     * \class doxygen_hide_local_param_op_update_arg
     *
     * \param[in] gradient_component_disp Gradient component of displacement
     * \param[out] cauchy_green_tensor Cauchy-Green tensor vector.
     */


    /*!
     * \brief Update for a tridimensional mesh.
     *
     * \copydoc doxygen_hide_local_param_op_update_arg
     */
    void Update3D(const LocalMatrix& gradient_component_disp, LocalVector& cauchy_green_tensor);

    /*!
     * \brief Update for a bidimensional mesh.
     *
     * \copydoc doxygen_hide_local_param_op_update_arg
     */
    void Update2D(const LocalMatrix& gradient_component_disp, LocalVector& cauchy_green_tensor);

    /*!
     * \brief Update for a one dimensional mesh.
     *
     * \copydoc doxygen_hide_local_param_op_update_arg
     */
    void Update1D(const LocalMatrix& gradient_component_disp, LocalVector& cauchy_green_tensor);


} // namespace MoReFEM::Internal::LocalParameterOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_PARAMETEROPERATOR_LOCAL_INTERNAL_UPDATECAUCHYGREENTENSOR_DOT_HPP_
// *** MoReFEM end header guards *** < //
