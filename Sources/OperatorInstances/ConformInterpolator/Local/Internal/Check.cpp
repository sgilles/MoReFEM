// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include <cassert>
#include <memory>
#include <type_traits> // IWYU pragma: keep
#include <vector>

#include "OperatorInstances/ConformInterpolator/Local/Internal/Check.hpp"


namespace MoReFEM::Internal::ConformInterpolatorNS::Local::Impl
{


#ifndef NDEBUG
    void AssertLocalNodeConsistency(const Advanced::LocalNode::vector_const_shared_ptr& p1_local_node_list,
                                    const Advanced::LocalNode::vector_const_shared_ptr& broader_local_node_list)
    {
        const auto Np1_node = p1_local_node_list.size();

        for (auto i = 0ul; i < Np1_node; ++i)
        {
            const auto& p1_local_node_ptr = p1_local_node_list[i];
            assert(!(!p1_local_node_ptr));
            const auto& local_interface = p1_local_node_ptr->GetLocalInterface();
            const auto& p1_vertex_list = local_interface.GetVertexIndexList();
            assert(p1_vertex_list.size() == 1ul);
            assert(p1_vertex_list.back() == i);

            const auto& broader_local_node_ptr = broader_local_node_list[i];
            assert(!(!broader_local_node_ptr));
            const auto& broader_vertex_list = broader_local_node_ptr->GetLocalInterface().GetVertexIndexList();
            assert(broader_vertex_list.size() == 1ul);
            assert(broader_vertex_list.back() == i);
        }
    }
#endif // NDEBUG


} // namespace MoReFEM::Internal::ConformInterpolatorNS::Local::Impl


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
