// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CIARLETGEYMONATDEVIATORIC_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CIARLETGEYMONATDEVIATORIC_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/CiarletGeymonatDeviatoric.hpp"
// *** MoReFEM header guards *** < //


// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/CiarletGeymonatDeviatoric<CoordsEnumT>.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::HyperelasticLawNS
{


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    const std::string& CiarletGeymonatDeviatoric<TimeManagerT, CoordsEnumT>::ClassName()
    {
        static const std::string ret("Ciarlet-Geymonat");
        return ret;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    CiarletGeymonatDeviatoric<TimeManagerT, CoordsEnumT>::CiarletGeymonatDeviatoric(std::size_t mesh_dimension,
                                                                                    const Solid<TimeManagerT>& solid)
    : invariant_holder_parent(mesh_dimension,
                              ::MoReFEM::InvariantNS::Content::invariants_and_first_and_second_deriv,
                              { mesh_dimension },
                              { mesh_dimension },
                              { mesh_dimension }),
      kappa1_(solid.GetKappa1()), kappa2_(solid.GetKappa2())
    { }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    double CiarletGeymonatDeviatoric<TimeManagerT, CoordsEnumT>::W(const QuadraturePoint& quad_pt,
                                                                   const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I1>();
        const double I2 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I2>();
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        const double I3_pow_minus_one_third = NumericNS::Pow(I3, -1. / 3.);

        return GetKappa1().GetValue(quad_pt, geom_elt) * (I1 * I3_pow_minus_one_third - 3.)
               + GetKappa2().GetValue(quad_pt, geom_elt) * (I2 * NumericNS::Square(I3_pow_minus_one_third) - 3.);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    double CiarletGeymonatDeviatoric<TimeManagerT, CoordsEnumT>::FirstDerivativeWThirdInvariant(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I1>();
        const double I2 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I2>();
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();

        constexpr const double minus_one_third = -1. / 3.;

        return GetKappa1().GetValue(quad_pt, geom_elt) * I1 * minus_one_third * NumericNS::Pow(I3, 4. * minus_one_third)
               + GetKappa2().GetValue(quad_pt, geom_elt) * I2 * 2. * minus_one_third
                     * NumericNS::Pow(I3, 5. * minus_one_third);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    double CiarletGeymonatDeviatoric<TimeManagerT, CoordsEnumT>::SecondDerivativeWThirdInvariant(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I1 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I1>();
        const double I2 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I2>();
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        constexpr const double minus_one_third = -1. / 3.;
        constexpr const double one_ninth = 1. / 9.;

        return GetKappa1().GetValue(quad_pt, geom_elt) * I1 * 4. * one_ninth * NumericNS::Pow(I3, 7. * minus_one_third)
               + GetKappa2().GetValue(quad_pt, geom_elt) * I2 * 10. * one_ninth
                     * NumericNS::Pow(I3, 8. * minus_one_third);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    double CiarletGeymonatDeviatoric<TimeManagerT, CoordsEnumT>::SecondDerivativeWFirstAndThirdInvariant(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        constexpr const double minus_one_third = -1. / 3.;

        return GetKappa1().GetValue(quad_pt, geom_elt) * minus_one_third * NumericNS::Pow(I3, 4. * minus_one_third);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    double CiarletGeymonatDeviatoric<TimeManagerT, CoordsEnumT>::SecondDerivativeWSecondAndThirdInvariant(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        constexpr const double minus_one_third = -1. / 3.;

        return GetKappa2().GetValue(quad_pt, geom_elt) * 2. * minus_one_third
               * NumericNS::Pow(I3, 5. * minus_one_third);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    double CiarletGeymonatDeviatoric<TimeManagerT, CoordsEnumT>::FirstDerivativeWFirstInvariant(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        return GetKappa1().GetValue(quad_pt, geom_elt) * NumericNS::Pow(I3, -1. / 3.);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    double CiarletGeymonatDeviatoric<TimeManagerT, CoordsEnumT>::FirstDerivativeWSecondInvariant(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) const
    {
        const double I3 = invariant_holder_parent::template GetInvariant<InvariantNS::index::I3>();
        return GetKappa2().GetValue(quad_pt, geom_elt) * NumericNS::Pow(I3, -2. / 3.);
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    inline constexpr double CiarletGeymonatDeviatoric<TimeManagerT, CoordsEnumT>::SecondDerivativeWFirstInvariant(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);

        return 0.;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    inline constexpr double CiarletGeymonatDeviatoric<TimeManagerT, CoordsEnumT>::SecondDerivativeWSecondInvariant(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);

        return 0.;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    inline constexpr double
    CiarletGeymonatDeviatoric<TimeManagerT, CoordsEnumT>::SecondDerivativeWFirstAndSecondInvariant(

        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt) noexcept
    {
        static_cast<void>(geom_elt);
        static_cast<void>(quad_pt);

        return 0.;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    inline auto
    CiarletGeymonatDeviatoric<TimeManagerT, CoordsEnumT>::GetKappa1() const noexcept -> const scalar_parameter_type&
    {
        return kappa1_;
    }


    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT
    >
    // clang-format on
    inline auto
    CiarletGeymonatDeviatoric<TimeManagerT, CoordsEnumT>::GetKappa2() const noexcept -> const scalar_parameter_type&
    {
        return kappa2_;
    }


} // namespace MoReFEM::HyperelasticLawNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CIARLETGEYMONATDEVIATORIC_DOT_HXX_
// *** MoReFEM end header guards *** < //
