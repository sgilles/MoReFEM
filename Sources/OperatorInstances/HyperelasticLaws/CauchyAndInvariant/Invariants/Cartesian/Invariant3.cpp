// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef>
#include <cstdlib>

#include "Utilities/LinearAlgebra/LocalAlias.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Cartesian/Invariant3.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::InvariantNS::CartesianNS
{


    namespace // anonymous
    {


        void SetConstantSecondDerivates(std::size_t mesh_dimension, LocalMatrix& out);

    } // namespace


    Invariant3::Invariant3(std::size_t mesh_dimension) : parent(mesh_dimension)
    {
        // Peculiar case as first derivate is constant.
        if (mesh_dimension == 1)
        {
            decltype(auto) first_deriv = parent::GetNonCstFirstDerivates();
            assert(first_deriv.size() == 1);
            first_deriv(0) = 1.;
        }


        if (mesh_dimension < 3)
        {
            SetConstantSecondDerivates(mesh_dimension, parent::GetNonCstSecondDerivates());
        }
    }


    void Invariant3::Update(const LocalVector& cauchy_green_tensor,
                            const ::MoReFEM::QuadraturePoint& quad_pt,
                            const ::MoReFEM::GeometricElt& geom_elt)
    {
        static_cast<void>(quad_pt);
        static_cast<void>(geom_elt);


        const auto mesh_dimension = GetMeshDimension();

        switch (mesh_dimension)
        {
        case 1ul:
        {
            assert(cauchy_green_tensor.size() == 1);
            const double value = cauchy_green_tensor(0);
            SetValue(value);
        }
        break;
        case 2ul:
        {
            assert(cauchy_green_tensor.size() == 3);
            const double value =
                cauchy_green_tensor(0) * cauchy_green_tensor(1) - NumericNS::Square(cauchy_green_tensor(2));
            SetValue(value);
        }
        break;
        case 3ul:
        {
            assert(cauchy_green_tensor.size() == 6);
            using NumericNS::Square;
            assert(cauchy_green_tensor.size() == 6);

            const double xx = cauchy_green_tensor(0);
            const double yy = cauchy_green_tensor(1);
            const double zz = cauchy_green_tensor(2);
            const double xy = cauchy_green_tensor(3);
            const double yz = cauchy_green_tensor(4);
            const double xz = cauchy_green_tensor(5);
            const double value = xx * yy * zz - xx * Square(yz) - yy * Square(xz) - zz * Square(xy) + 2. * xy * yz * xz;
            SetValue(value);
        }
        break;
        default:
            assert(false);
            exit(EXIT_FAILURE);
        }
    }


    void Invariant3::UpdateFirstDerivates(const LocalVector& cauchy_green_tensor,
                                          const ::MoReFEM::QuadraturePoint& quad_pt,
                                          const ::MoReFEM::GeometricElt& geom_elt)
    {
        static_cast<void>(quad_pt);
        static_cast<void>(geom_elt);

        decltype(auto) first_deriv = parent::GetNonCstFirstDerivates();

        const auto mesh_dimension = parent::GetMeshDimension();

        switch (mesh_dimension)
        {
        case 1ul:
            // Constant and already set in constructor.
            break;
        case 2ul:
        {
            assert(first_deriv.size() == 3);
            assert(cauchy_green_tensor.size() == 3);

            first_deriv(0) = cauchy_green_tensor(1);
            first_deriv(1) = cauchy_green_tensor(0);
            first_deriv(2) = -cauchy_green_tensor(2);
        }
        break;
        case 3ul:
        {
            assert(first_deriv.size() == 6);
            assert(cauchy_green_tensor.size() == 6);

            using NumericNS::Square;

            const double xx = cauchy_green_tensor(0);
            const double yy = cauchy_green_tensor(1);
            const double zz = cauchy_green_tensor(2);
            const double xy = cauchy_green_tensor(3);
            const double yz = cauchy_green_tensor(4);
            const double xz = cauchy_green_tensor(5);

            first_deriv(0) = yy * zz - Square(yz);
            first_deriv(1) = xx * zz - Square(xz);
            first_deriv(2) = xx * yy - Square(xy);
            first_deriv(3) = yz * xz - zz * xy;
            first_deriv(4) = xy * xz - xx * yz;
            first_deriv(5) = xy * yz - yy * xz;
        }
        break;
        default:
            assert(false);
            exit(EXIT_FAILURE);
        }
    }


    void Invariant3::UpdateSecondDerivates(const LocalVector& cauchy_green_tensor,
                                           const ::MoReFEM::QuadraturePoint& quad_pt,
                                           const ::MoReFEM::GeometricElt& geom_elt)
    {

        static_cast<void>(quad_pt);
        static_cast<void>(geom_elt);
        static_cast<void>(cauchy_green_tensor);

        const auto mesh_dimension = parent::GetMeshDimension();

        if (mesh_dimension
            == 3ul) // Do nothing for dimensions 1 and 2: the values are constant and are fixed at construction.
        {
            decltype(auto) second_deriv = parent::GetNonCstSecondDerivates();

            // [0    , Czz  , Cyy  , 0        , -Cyz     , 0        ],
            // [Czz  , 0    , Cxx  , 0        , 0        , -Cxz     ],
            // [Cyy  , Cxx  , 0    , -Cxy     , 0        , 0        ],
            // [0    , 0    , -Cxy , -0.5*Czz , 0.5*Cxz  , 0.5*Cyz  ],
            // [-Cyz , 0    , 0    , 0.5*Cxz  , -0.5*Cxx , 0.5*Cxy  ],
            // [0    , -Cxz , 0    , 0.5*Cyz  , 0.5*Cxy  , -0.5*Cyy ]

            assert(second_deriv.shape(0) == 6);
            assert(second_deriv.shape(1) == 6);
            assert(cauchy_green_tensor.size() == 6);

            const double xx = cauchy_green_tensor(0);
            const double yy = cauchy_green_tensor(1);
            const double zz = cauchy_green_tensor(2);
            const double xy = cauchy_green_tensor(3);
            const double yz = cauchy_green_tensor(4);
            const double xz = cauchy_green_tensor(5);

            second_deriv.fill(0.);

            second_deriv(3, 3) = -0.5 * zz;
            second_deriv(4, 4) = -0.5 * xx;
            second_deriv(5, 5) = -0.5 * yy;

            second_deriv(1, 0) = second_deriv(0, 1) = zz;
            second_deriv(2, 0) = second_deriv(0, 2) = yy;
            second_deriv(1, 2) = second_deriv(2, 1) = xx;

            second_deriv(0, 4) = second_deriv(4, 0) = -yz;
            second_deriv(1, 5) = second_deriv(5, 1) = -xz;
            second_deriv(2, 3) = second_deriv(3, 2) = -xy;

            second_deriv(3, 4) = second_deriv(4, 3) = 0.5 * xz;
            second_deriv(3, 5) = second_deriv(5, 3) = 0.5 * yz;
            second_deriv(5, 4) = second_deriv(4, 5) = 0.5 * xy;
        }
    }


    namespace // anonymous
    {


        void SetConstantSecondDerivates(std::size_t mesh_dimension, LocalMatrix& second_deriv)
        {
            assert(mesh_dimension < 3ul);

            switch (mesh_dimension)
            {
            case 1ul:
            {
                assert(second_deriv.shape(0) == 1);
                assert(second_deriv.shape(1) == 1);
                second_deriv.fill(0.);
            }
            break;
            case 2ul:
            {
                assert(second_deriv.shape(0) == 3);
                assert(second_deriv.shape(1) == 3);
                second_deriv.fill(0.);

                // [0, 1, 0]
                // [1, 0, 0]
                // [0, 0, -0.5]

                second_deriv(2, 2) = -0.5;
                second_deriv(0, 1) = second_deriv(1, 0) = 1.;
            }
            break;
            }
        }

    } // namespace


} // namespace MoReFEM::InvariantNS::CartesianNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
