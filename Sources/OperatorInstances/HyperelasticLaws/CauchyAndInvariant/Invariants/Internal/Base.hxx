// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_INTERNAL_BASE_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_INTERNAL_BASE_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Internal/Base.hpp"
// *** MoReFEM header guards *** < //


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::InvariantNS
{


    template<::MoReFEM::InvariantNS::index EnumT>
    Base<EnumT>::Base(std::size_t mesh_dimension) : mesh_dimension_(mesh_dimension)
    {
        assert(mesh_dimension > 0ul);
        assert(mesh_dimension < 4ul);

        const auto size = (mesh_dimension == 1ul ? 1ul : (mesh_dimension == 2ul ? 3ul : 6ul));
        first_derivates_.resize({ size });
        first_derivates_.fill(0.);

        second_derivates_.resize({ size, size });
        second_derivates_.fill(0.);
    }


    template<::MoReFEM::InvariantNS::index EnumT>
    std::size_t Base<EnumT>::GetLinearAlgebraSize() const noexcept
    {
        return GetFirstDerivates().size();
    }


    template<::MoReFEM::InvariantNS::index EnumT>
    inline double Base<EnumT>::GetValue() const noexcept
    {
        return value_;
    }


    template<::MoReFEM::InvariantNS::index EnumT>
    inline void Base<EnumT>::SetValue(double value) noexcept
    {
        value_ = value;
    }


    template<::MoReFEM::InvariantNS::index EnumT>
    std::size_t Base<EnumT>::GetMeshDimension() const noexcept
    {
        return mesh_dimension_;
    }


    template<::MoReFEM::InvariantNS::index EnumT>
    const LocalVector& Base<EnumT>::GetFirstDerivates() const noexcept
    {
        return first_derivates_;
    }


    template<::MoReFEM::InvariantNS::index EnumT>
    LocalVector& Base<EnumT>::GetNonCstFirstDerivates() noexcept
    {
        return const_cast<LocalVector&>(GetFirstDerivates());
    }


    template<::MoReFEM::InvariantNS::index EnumT>
    auto Base<EnumT>::GetSecondDerivates() const noexcept -> const LocalMatrix&
    {
        return second_derivates_;
    }


    template<::MoReFEM::InvariantNS::index EnumT>
    auto Base<EnumT>::GetNonCstSecondDerivates() noexcept -> LocalMatrix&
    {
        return const_cast<LocalMatrix&>(GetSecondDerivates());
    }


} // namespace MoReFEM::Internal::InvariantNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_INTERNAL_BASE_DOT_HXX_
// *** MoReFEM end header guards *** < //
