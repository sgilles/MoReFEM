// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_INTERNAL_TUPLEITERATION_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_INTERNAL_TUPLEITERATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <tuple>

#include "Utilities/Containers/Tuple/Concept.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Enum.hpp"
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/GeneralizedInput.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { class GeometricElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::InvariantNS
{

    /*!
     * \brief Helper class to mimic iteration over the elements of \a TupleT.
     *
     * \tparam TupleT Tuple or tuple-like container considered; here is expected to be variadic template arguments
     * of \a InvariantHolder class.
     * \tparam IndexT Index in the tuple of the element currently under investigation.
     */
    template<::MoReFEM::Concept::Tuple TupleT, std::size_t Index>
    struct TupleIteration
    {
        static_assert(Index < std::tuple_size<TupleT>());

        //! Alias to current type considered in the tuple.
        using current_type = std::tuple_element_t<Index, TupleT>;

        //! Alias to the next element to consider (if \a is_last is false)
        using next_element = TupleIteration<TupleT, Index + 1>;

        //! Whether \a Index points to the last element of the tuple or not.
        static constexpr bool is_last = (Index + 1 == std::tuple_size<TupleT>() ? true : false);

        /*!
         * \brief Facility to identify where is in \a TupleT the element which matching enum type is \a EnumT.
         *
         * If none, the code fails to compile
         *
         * \return Position in the tuple of the element which matches \a EnumT.
         */
        template<::MoReFEM::InvariantNS::index EnumT>
        static constexpr std::size_t FindIndex();

        /*!
         * \brief Tells whether an invariant which index is \a EnumT is present in \a TupleT
         *
         * \return True if a matching invariant was found.
         */
        template<::MoReFEM::InvariantNS::index EnumT>
        static constexpr bool IsPresent();


        /*!
         * \brief Update \a invariant_holder content - both for all invariants and first and second derivates if
         * mandated by \a invariant_holder.GetContent() - for cartesian coordinates.
         *
         * \copydoc doxygen_hide_invariant_cartesian_update_common
         *
         * \param[in,out] invariant_holder \a InvariantHolder which content is to be updated.
         */
        template<class InvariantHolderT>
        static void Update(const ::MoReFEM::LocalVector& cauchy_green_tensor,
                           const ::MoReFEM::QuadraturePoint& quad_pt,
                           const ::MoReFEM::GeometricElt& geom_elt,
                           InvariantHolderT& invariant_holder);


        /*!
         * \brief Update \a invariant_holder content - both for all invariants and first and second derivates if
         * mandated by \a invariant_holder.GetContent() - for generalized coordinates.
         *
         * \copydoc doxygen_hide_invariant_generalized_update_common
         *
         * \param[in,out] invariant_holder \a InvariantHolder which content is to be updated.
         */
        template<class InvariantHolderT>
        static void Update(const LocalMatrix& matricial_cauchy_green_tensor,
                           const ::MoReFEM::QuadraturePoint& quad_pt,
                           const ::MoReFEM::GeometricElt& geom_elt,
                           const ::MoReFEM::InvariantNS::GeneralizedNS::Input& generalized_input,
                           const LocalMatrix& contravariant_basis,
                           InvariantHolderT& invariant_holder);
    };


} // namespace MoReFEM::Internal::InvariantNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Internal/TupleIteration.hxx"

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_INTERNAL_TUPLEITERATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
