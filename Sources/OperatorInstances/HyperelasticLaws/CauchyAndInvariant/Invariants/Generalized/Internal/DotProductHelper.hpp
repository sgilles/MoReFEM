// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_INTERNAL_DOTPRODUCTHELPER_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_INTERNAL_DOTPRODUCTHELPER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/LinearAlgebra/LocalAlias.hpp"

namespace MoReFEM::Internal::InvariantNS::GeneralizedNS
{


    /*!
     * \brief Helper function used in computation of generalized invariants 4 and 6.
     *
     *
     * \param[in] component 0 for x, 1 for y, 2 for z.
     * \param[in] contravariant_basis Contravariant basis.
     * \param[in] tau_interpolate_at_quad_point Tau interpolate at quadrature point.
     *
     *  \return Value used in I4 and I6 computation.
     *
     */
    double DotProductHelper(std::size_t component,
                            const LocalMatrix& contravariant_basis,
                            const LocalVector& tau_interpolate_at_quad_point);


} // namespace MoReFEM::Internal::InvariantNS::GeneralizedNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_INTERNAL_DOTPRODUCTHELPER_DOT_HPP_
// *** MoReFEM end header guards *** < //
