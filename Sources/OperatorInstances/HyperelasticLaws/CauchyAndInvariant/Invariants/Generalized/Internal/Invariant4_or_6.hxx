// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_INTERNAL_INVARIANT4_OR_6_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_INTERNAL_INVARIANT4_OR_6_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/Internal/Invariant4_or_6.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/Internal/Invariant4_or_6.hpp"


#include <cstddef> // IWYU pragma: keep

#include "Utilities/LinearAlgebra/LocalAlias.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Geometry/GeometricElt/GeometricElt.hpp"

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/Internal/DotProductHelper.hpp"


namespace MoReFEM::Internal::InvariantNS::GeneralizedNS
{


    // clang-format off
    template
    <
        ::MoReFEM::InvariantNS::index InvariantIndexT,
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    Invariant4_or_6<InvariantIndexT, FiberPolicyT, TimeManagerT>::Invariant4_or_6(std::size_t mesh_dimension,
                                                                                  const fiber_type& fiber)
    : parent(mesh_dimension), fiber_(fiber)
    {
        assert(mesh_dimension == 3ul
               && "Generalized coordinates are used only for 3D-shells so far; "
                  "if you need them for lower dimensions please "
                  "[post an issue](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues)");
    }


    // clang-format off
    template
    <
        ::MoReFEM::InvariantNS::index InvariantIndexT,
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    void Invariant4_or_6<InvariantIndexT, FiberPolicyT, TimeManagerT>::Update(
        const LocalMatrix& matricial_cauchy_green_tensor,
        const ::MoReFEM::QuadraturePoint& quad_pt,
        const ::MoReFEM::GeometricElt& geom_elt,
        const ::MoReFEM::InvariantNS::GeneralizedNS::Input& generalized_input,
        const LocalMatrix& contravariant_basis)
    {
        static_cast<void>(generalized_input);

        const auto& tau_interpolate_at_quad_point = GetFiber().GetValue(quad_pt, geom_elt);

        assert(matricial_cauchy_green_tensor.shape(0) == 6 && matricial_cauchy_green_tensor.shape(1) == 1);
        assert(tau_interpolate_at_quad_point.size() == contravariant_basis.shape(0)
               && tau_interpolate_at_quad_point.size() == contravariant_basis.shape(1));

        const double Cxx = matricial_cauchy_green_tensor(0, 0);
        const double Cyy = matricial_cauchy_green_tensor(1, 0);
        const double Czz = matricial_cauchy_green_tensor(2, 0);
        const double Cxy = matricial_cauchy_green_tensor(3, 0);
        const double Cyz = matricial_cauchy_green_tensor(4, 0);
        const double Cxz = matricial_cauchy_green_tensor(5, 0);

        constexpr std::size_t first_component = 0;
        constexpr std::size_t second_component = 1;
        constexpr std::size_t third_component = 2;

        const double nx = DotProductHelper(first_component, contravariant_basis, tau_interpolate_at_quad_point);
        const double ny = DotProductHelper(second_component, contravariant_basis, tau_interpolate_at_quad_point);
        const double nz = DotProductHelper(third_component, contravariant_basis, tau_interpolate_at_quad_point);

        const double norm = nx * nx + ny * ny + nz * nz;
        double I4 = 0.;

        if (!(NumericNS::IsZero(norm)))
        {
            I4 = nx * (nx * Cxx + ny * Cxy + nz * Cxz) + ny * (nx * Cxy + ny * Cyy + nz * Cyz)
                 + nz * (nx * Cxz + ny * Cyz + nz * Czz);
        }

        parent::SetValue(I4);
    }


    // clang-format off
    template
    <
        ::MoReFEM::InvariantNS::index InvariantIndexT,
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    void Invariant4_or_6<InvariantIndexT, FiberPolicyT, TimeManagerT>::UpdateFirstDerivates(
        const LocalMatrix& matricial_cauchy_green_tensor,
        const ::MoReFEM::QuadraturePoint& quad_pt,
        const ::MoReFEM::GeometricElt& geom_elt,
        const ::MoReFEM::InvariantNS::GeneralizedNS::Input& generalized_input,
        const LocalMatrix& contravariant_basis)
    {
        static_cast<void>(matricial_cauchy_green_tensor);
        static_cast<void>(quad_pt);
        static_cast<void>(geom_elt);
        static_cast<void>(generalized_input);
        static_cast<void>(contravariant_basis);

        const auto& tau_interpolate_at_quad_point = GetFiber().GetValue(quad_pt, geom_elt);

        assert(tau_interpolate_at_quad_point.size() == contravariant_basis.shape(0)
               && tau_interpolate_at_quad_point.size() == contravariant_basis.shape(1));

        constexpr std::size_t first_component = 0;
        constexpr std::size_t second_component = 1;
        constexpr std::size_t third_component = 2;

        const double nx = DotProductHelper(first_component, contravariant_basis, tau_interpolate_at_quad_point);
        const double ny = DotProductHelper(second_component, contravariant_basis, tau_interpolate_at_quad_point);
        const double nz = DotProductHelper(third_component, contravariant_basis, tau_interpolate_at_quad_point);

        decltype(auto) first_deriv = parent::GetNonCstFirstDerivates();

        first_deriv.fill(0.);

        const double norm = nx * nx + ny * ny + nz * nz;

        if (!(NumericNS::IsZero(norm)))
        {
            first_deriv(0) = (nx * nx);
            first_deriv(1) = (ny * ny);
            first_deriv(2) = (nz * nz);
            first_deriv(3) = (nx * ny);
            first_deriv(4) = (ny * nz);
            first_deriv(5) = (nx * nz);
        }
    }


    // clang-format off
    template
    <
        ::MoReFEM::InvariantNS::index InvariantIndexT,
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    auto Invariant4_or_6<InvariantIndexT, FiberPolicyT, TimeManagerT>::GetFiber() const noexcept -> const fiber_type&
    {
        return fiber_;
    }


} // namespace MoReFEM::Internal::InvariantNS::GeneralizedNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_INTERNAL_INVARIANT4_OR_6_DOT_HXX_
// *** MoReFEM end header guards *** < //
