// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_INTERNAL_INVARIANT4_OR_6_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_INTERNAL_INVARIANT4_OR_6_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <type_traits>

#include "Utilities/LinearAlgebra/LocalAlias.hpp"

#include "Core/Parameter/FiberEnum.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Enum.hpp"
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/GeneralizedInput.hpp"
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Internal/Base.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { class GeometricElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::InvariantNS::GeneralizedNS
{


    /*!
     * \brief Invariant I4 and I6 use up same implementation, which is provided here once for DRY sake.
     *
     * \internal For this invariant second derivates is constant so there are no Update methods for it.
     */
    // clang-format off
    template
    <
        ::MoReFEM::InvariantNS::index InvariantIndexT,
        ::MoReFEM::FiberNS::AtNodeOrAtQuadPt FiberPolicyT,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    class Invariant4_or_6 : public ::MoReFEM::Internal::InvariantNS::Base<InvariantIndexT>
    {
      public:
        static_assert(InvariantIndexT == ::MoReFEM::InvariantNS::index::I4
                      || InvariantIndexT == ::MoReFEM::InvariantNS::index::I6);

        //! Alias to self.
        using self = Invariant4_or_6<InvariantIndexT, FiberPolicyT, TimeManagerT>;

        //! Parent class
        using parent = ::MoReFEM::Internal::InvariantNS::Base<InvariantIndexT>;

        static_assert(std::is_convertible<self*, parent*>());

        //! \copydoc doxygen_hide_fiber_type_alias
        using fiber_type = FiberList<FiberPolicyT, ParameterNS::Type::vector, TimeManagerT>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] mesh_dimension Dimension of the mesh.
         * \param[in] fiber Fiber considered
         */
        Invariant4_or_6(std::size_t mesh_dimension, const fiber_type& fiber);

        //! Destructor.
        ~Invariant4_or_6() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Invariant4_or_6(const Invariant4_or_6& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Invariant4_or_6(Invariant4_or_6&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Invariant4_or_6& operator=(const Invariant4_or_6& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Invariant4_or_6& operator=(Invariant4_or_6&& rhs) = delete;

        ///@}
      public:
        //! \copydoc doxygen_hide_invariant_generalized_update
        void Update(const LocalMatrix& matricial_cauchy_green_tensor,
                    const ::MoReFEM::QuadraturePoint& quad_pt,
                    const ::MoReFEM::GeometricElt& geom_elt,
                    const ::MoReFEM::InvariantNS::GeneralizedNS::Input& generalized_input,
                    const LocalMatrix& contravariant_basis);

        //! Fiber considered.
        const fiber_type& GetFiber() const noexcept;

        //! \copydoc doxygen_hide_invariant_generalized_update_first_deriv
        void UpdateFirstDerivates(const LocalMatrix& matricial_cauchy_green_tensor,
                                  const ::MoReFEM::QuadraturePoint& quad_pt,
                                  const ::MoReFEM::GeometricElt& geom_elt,
                                  const ::MoReFEM::InvariantNS::GeneralizedNS::Input& generalized_input,
                                  const LocalMatrix& contravariant_basis);


      private:
        //! Fiber considered.
        const fiber_type& fiber_;
    };


} // namespace MoReFEM::Internal::InvariantNS::GeneralizedNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/Internal/Invariant4_or_6.hxx"

// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_INTERNAL_INVARIANT4_OR_6_DOT_HPP_
// *** MoReFEM end header guards *** < //
