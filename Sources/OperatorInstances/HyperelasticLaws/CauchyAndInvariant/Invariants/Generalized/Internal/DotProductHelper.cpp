// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/Internal/DotProductHelper.hpp"


namespace MoReFEM::Internal::InvariantNS::GeneralizedNS
{


    double DotProductHelper(std::size_t component,
                            const LocalMatrix& contravariant_basis,
                            const LocalVector& tau_interpolate_at_quad_point)
    {
        double ret = 0.;
        for (auto i = 0ul, size = tau_interpolate_at_quad_point.size(); i < size; ++i)
            ret += tau_interpolate_at_quad_point(i) * contravariant_basis(i, component);

        return ret;
    };


} // namespace MoReFEM::Internal::InvariantNS::GeneralizedNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
