// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <array>
#include <cassert>
#include <cstddef>
#include <memory>
#include <utility>

#include "Utilities/Containers/EnumClass.hpp"
#include "Utilities/LinearAlgebra/LocalAlias.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/GeneralizedInput.hpp"
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/Invariant2.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::InvariantNS::GeneralizedNS
{


    Invariant2::Invariant2(std::size_t mesh_dimension) : parent(mesh_dimension)
    {
        assert(mesh_dimension == 3ul
               && "Generalized coordinates are used only for 3D-shells so far; "
                  "if you need them for lower dimensions please "
                  "[post an issue](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues)");


        const auto size = parent::GetLinearAlgebraSize();

        matrix_parent::InitLocalMatrixStorage({ {
            { 1, 1 },    // scalar_matrix
            { size, 1 }, // intermediate product
        } });

        // Second derivates are already filled with zeros
    }


    void Invariant2::Update(const LocalMatrix& matricial_cauchy_green_tensor,
                            const ::MoReFEM::QuadraturePoint& quad_pt,
                            const ::MoReFEM::GeometricElt& geom_elt,
                            const InvariantNS::GeneralizedNS::Input& generalized_input,
                            const LocalMatrix& contravariant_basis)
    {
        assert(generalized_input.IsI2Supported());

        static_cast<void>(quad_pt);
        static_cast<void>(geom_elt);
        static_cast<void>(contravariant_basis);

        decltype(auto) scalar_matrix =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::scalar_matrix)>();
        decltype(auto) intermediate_product =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::intermediate_product)>();

        using index = InvariantNS::GeneralizedNS::Input::LocalMatrixIndex;
        decltype(auto) I2_matrix_helper =
            generalized_input.template GetLocalMatrix<EnumUnderlyingType(index::I2_helper)>();

        xt::noalias(intermediate_product) = xt::linalg::dot(I2_matrix_helper, matricial_cauchy_green_tensor);
        xt::noalias(scalar_matrix) =
            0.5 * xt::linalg::dot(xt::transpose(matricial_cauchy_green_tensor), intermediate_product);

        assert(scalar_matrix.shape(0) == 1);
        assert(scalar_matrix.shape(1) == 1);

        SetValue(scalar_matrix(0, 0));
    }


    void Invariant2::UpdateFirstDerivates(const LocalMatrix& matricial_cauchy_green_tensor,
                                          const ::MoReFEM::QuadraturePoint& quad_pt,
                                          const ::MoReFEM::GeometricElt& geom_elt,
                                          const InvariantNS::GeneralizedNS::Input& generalized_input,
                                          const LocalMatrix& contravariant_basis)
    {
        assert(generalized_input.IsI2Supported());

        static_cast<void>(matricial_cauchy_green_tensor);
        static_cast<void>(quad_pt);
        static_cast<void>(geom_elt);

        static_cast<void>(contravariant_basis);

        decltype(auto) first_deriv = parent::GetNonCstFirstDerivates();
        decltype(auto) intermediate_product =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::intermediate_product)>();


        using index = InvariantNS::GeneralizedNS::Input::LocalMatrixIndex;

        decltype(auto) I2_matrix_helper =
            generalized_input.template GetLocalMatrix<EnumUnderlyingType(index::I2_helper)>();

        {
            xt::noalias(intermediate_product) = xt::linalg::dot(I2_matrix_helper, matricial_cauchy_green_tensor);

            assert(intermediate_product.shape(0) == 6);
            assert(intermediate_product.shape(1) == 1);

            const auto size = first_deriv.size();
            const std::size_t euclidean_dimension = 3ul;

            for (auto i = 0ul; i < size - euclidean_dimension; ++i)
            {
                first_deriv(i) = intermediate_product(i, 0);
            }

            for (auto i = euclidean_dimension; i < size; ++i)
            {
                first_deriv(i) = intermediate_product(i, 0);
                first_deriv(i) *= 0.5;
            }
        }
    }


    void Invariant2::UpdateSecondDerivates(const LocalMatrix& matricial_cauchy_green_tensor,
                                           const ::MoReFEM::QuadraturePoint& quad_pt,
                                           const ::MoReFEM::GeometricElt& geom_elt,
                                           const InvariantNS::GeneralizedNS::Input& generalized_input,
                                           const LocalMatrix& contravariant_basis)
    {
        assert(generalized_input.IsI2Supported());

        static_cast<void>(matricial_cauchy_green_tensor);
        static_cast<void>(quad_pt);
        static_cast<void>(geom_elt);
        static_cast<void>(contravariant_basis);

        decltype(auto) second_deriv = parent::GetNonCstSecondDerivates();

        using index = InvariantNS::GeneralizedNS::Input::LocalMatrixIndex;
        decltype(auto) I2_matrix_helper =
            generalized_input.template GetLocalMatrix<EnumUnderlyingType(index::I2_helper)>();


        const auto size = second_deriv.shape(0);
        assert(size == second_deriv.shape(1));

        // #1840 To improve!
        for (auto i = 0ul; i < size; ++i)
        {
            for (auto j = 0ul; j < size; ++j)
                second_deriv(i, j) = I2_matrix_helper(i, j);
        }

        for (auto i = 3ul; i < size; ++i)
            for (auto j = 0ul; j < size; ++j)
                second_deriv(i, j) *= 0.5;

        for (auto i = 0ul; i < size; ++i)
            for (auto j = 3ul; j < size; ++j)
                second_deriv(i, j) *= 0.5;
    }


} // namespace MoReFEM::InvariantNS::GeneralizedNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
