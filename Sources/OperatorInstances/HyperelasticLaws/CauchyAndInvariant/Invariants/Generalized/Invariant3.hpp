// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_INVARIANT3_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_INVARIANT3_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef>
#include <type_traits>

#include "Utilities/LinearAlgebra/LocalAlias.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Enum.hpp"
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Internal/Base.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }  // lines 31-31
namespace MoReFEM { class QuadraturePoint; }  // lines 30-30
namespace MoReFEM::InvariantNS::GeneralizedNS { class Input; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM::InvariantNS::GeneralizedNS
{

    /*!
     * \brief Invariant I3 for generalized coordinates
     */
    class Invariant3 : public ::MoReFEM::Internal::InvariantNS::Base<index::I3>
    {
      public:
        //! Alias to self.
        using self = Invariant3;

        //! Parent class
        using parent = ::MoReFEM::Internal::InvariantNS::Base<index::I3>;

        static_assert(std::is_convertible<self*, parent*>());

      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        //! \param[in] mesh_dimension Dimension of the mesh.
        Invariant3(std::size_t mesh_dimension);

        //! Destructor.
        ~Invariant3() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Invariant3(const Invariant3& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Invariant3(Invariant3&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Invariant3& operator=(const Invariant3& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Invariant3& operator=(Invariant3&& rhs) = delete;

        ///@}
      public:
        //! \copydoc doxygen_hide_invariant_generalized_update
        void Update(const LocalMatrix& matricial_cauchy_green_tensor,
                    const ::MoReFEM::QuadraturePoint& quad_pt,
                    const ::MoReFEM::GeometricElt& geom_elt,
                    const InvariantNS::GeneralizedNS::Input& generalized_input,
                    const LocalMatrix& contravariant_basis);

        //! \copydoc doxygen_hide_invariant_generalized_update_first_deriv
        void UpdateFirstDerivates(const LocalMatrix& matricial_cauchy_green_tensor,
                                  const ::MoReFEM::QuadraturePoint& quad_pt,
                                  const ::MoReFEM::GeometricElt& geom_elt,
                                  const InvariantNS::GeneralizedNS::Input& generalized_input,
                                  const LocalMatrix& contravariant_basis);

        //! \copydoc doxygen_hide_invariant_generalized_update_second_deriv
        void UpdateSecondDerivates(const LocalMatrix& matricial_cauchy_green_tensor,
                                   const ::MoReFEM::QuadraturePoint& quad_pt,
                                   const ::MoReFEM::GeometricElt& geom_elt,
                                   const InvariantNS::GeneralizedNS::Input& generalized_input,
                                   const LocalMatrix& contravariant_basis);
    };


} // namespace MoReFEM::InvariantNS::GeneralizedNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_INVARIANT3_DOT_HPP_
// *** MoReFEM end header guards *** < //
