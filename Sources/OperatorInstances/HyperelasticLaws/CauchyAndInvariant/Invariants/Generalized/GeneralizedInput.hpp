// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_GENERALIZEDINPUT_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_GENERALIZEDINPUT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <type_traits>

#include "Utilities/LinearAlgebra/LocalAlias.hpp"
#include "Utilities/LinearAlgebra/Storage/Local/LocalMatrixStorage.hpp"


namespace MoReFEM::InvariantNS::GeneralizedNS
{

    /*!
     * \brief Enum class to tell whether there is I2 considered or not.
     */
    enum class is_I2 { yes, no };


    /*!
     * \brief Class which stores few data used when computing invariants with generalized coordinates.
     */
    class Input : public Crtp::LocalMatrixStorage<Input, 3ul>
    {
      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Input;

        //! Alias to the parent that provides LocalMatrixStorage.
        using matrix_parent = Crtp::LocalMatrixStorage<Input, 3ul>;

        static_assert(std::is_convertible<self*, matrix_parent*>());

      public:
        /*!
         * \brief Constructor
         *
         * \param[in] is_invariant2 Whether there is invariant 2 involved. If so, there is an extra computation
         * for an helper matrix used to compute derivates related to this invariant.
         */
        explicit Input(is_I2 is_invariant2);

        //! Destructor.
        ~Input() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Input(const Input& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Input(Input&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Input& operator=(const Input& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Input& operator=(Input&& rhs) = delete;

        //! Indexes for local matrices.
        enum class LocalMatrixIndex : std::size_t {
            contravariant_metric_as_vector,
            transposed_contravariant_metric_as_vector,
            I2_helper
        };

        /*!
         * \brief Update the content of the class.
         *
         * \param[in] contravariant_metric_tensor Contravariant metric tensor.
         */
        void Update(const LocalMatrix& contravariant_metric_tensor);

        /*!
         * \brief Get the value of the last contravariant_metric_tensor determinant computed.
         *
         * \return Determinant computed at the last call of \a Update() (or 0 if none occurred).
         */
        double GetDeterminant() const noexcept;

        //! Whether there is invariant 2 involved. If so, there is an extra computation
        //! for an helper matrix used to compute derivates related to this invariant.
        bool IsI2Supported() const noexcept;

      private:
        /*!
         * \brief Compute the  determinant of contravariant_metric_tensor.
         *
         * \param[in] contravariant_metric_tensor Contravariant metric tensor.
         */
        void ComputeDeterminant(const LocalMatrix& contravariant_metric_tensor);

        //! Compute helper matrix useful for I2 invariant. Should be called only if is_invariant2_ is 'yes'.
        void ComputeI2Helper();

      private:
        //! Determinant of contravariant_metric_tensor
        double determinant_{};

        //! Whether there is invariant 2 involved. If so, there is an extra computation
        //! for an helper matrix used to compute derivates related to this invariant.
        const is_I2 is_invariant2_;
    };


} // namespace MoReFEM::InvariantNS::GeneralizedNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_CAUCHYANDINVARIANT_INVARIANTS_GENERALIZED_GENERALIZEDINPUT_DOT_HPP_
// *** MoReFEM end header guards *** < //
