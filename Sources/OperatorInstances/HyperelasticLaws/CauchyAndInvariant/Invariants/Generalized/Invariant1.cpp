// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef>
#include <memory>
#include <utility>

#include "Utilities/Containers/EnumClass.hpp"
#include "Utilities/LinearAlgebra/LocalAlias.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/GeneralizedInput.hpp"
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/Invariant1.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::InvariantNS::GeneralizedNS
{


    Invariant1::Invariant1(std::size_t mesh_dimension) : parent(mesh_dimension)
    {
        assert(mesh_dimension == 3ul
               && "Generalized coordinates are used only for 3D-shells so far; "
                  "if you need them for lower dimensions please "
                  "[post an issue](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues)");

        matrix_parent::InitLocalMatrixStorage({ {
            { 1ul, 1ul } // scalar_matrix
        } });

        // Second derivates are already filled with zeros
    }


    void Invariant1::Update(const LocalMatrix& matricial_cauchy_green_tensor,
                            const ::MoReFEM::QuadraturePoint& quad_pt,
                            const ::MoReFEM::GeometricElt& geom_elt,
                            const InvariantNS::GeneralizedNS::Input& generalized_input,
                            const LocalMatrix& contravariant_basis)
    {
        static_cast<void>(quad_pt);
        static_cast<void>(geom_elt);
        static_cast<void>(contravariant_basis);

        decltype(auto) scalar_matrix =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::scalar_matrix)>();

        assert(scalar_matrix.shape(0) == 1ul);
        assert(scalar_matrix.shape(1) == 1ul);

        using index = InvariantNS::GeneralizedNS::Input::LocalMatrixIndex;
        decltype(auto) transposed_contravariant_metric_tensor =
            generalized_input
                .template GetLocalMatrix<EnumUnderlyingType(index::transposed_contravariant_metric_as_vector)>();

        xt::noalias(scalar_matrix) =
            xt::linalg::dot(transposed_contravariant_metric_tensor, matricial_cauchy_green_tensor);

        SetValue(scalar_matrix(0, 0));
    }


    void Invariant1::UpdateFirstDerivates(const LocalMatrix& matricial_cauchy_green_tensor,
                                          const ::MoReFEM::QuadraturePoint& quad_pt,
                                          const ::MoReFEM::GeometricElt& geom_elt,
                                          const InvariantNS::GeneralizedNS::Input& generalized_input,
                                          const LocalMatrix& contravariant_basis)
    {
        static_cast<void>(matricial_cauchy_green_tensor);
        static_cast<void>(quad_pt);
        static_cast<void>(geom_elt);
        static_cast<void>(contravariant_basis);

        decltype(auto) first_deriv = parent::GetNonCstFirstDerivates();

        using index = InvariantNS::GeneralizedNS::Input::LocalMatrixIndex;
        decltype(auto) contravariant_metric_tensor =
            generalized_input.template GetLocalMatrix<EnumUnderlyingType(index::contravariant_metric_as_vector)>();

        assert(contravariant_metric_tensor.shape(0) == first_deriv.size());

        for (auto i = 0ul, size = first_deriv.size(); i < size; ++i)
        {
            first_deriv(i) = contravariant_metric_tensor(i, 0);
            if (i > 2ul)
                first_deriv(i) *= 0.5;
        }
    }


} // namespace MoReFEM::InvariantNS::GeneralizedNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
