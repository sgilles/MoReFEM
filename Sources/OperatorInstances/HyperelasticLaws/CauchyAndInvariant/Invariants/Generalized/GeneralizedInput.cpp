// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <utility>

#include "Utilities/Containers/EnumClass.hpp"
#include "Utilities/LinearAlgebra/LocalAlias.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Generalized/GeneralizedInput.hpp"


namespace MoReFEM::InvariantNS::GeneralizedNS
{


    Input::Input(is_I2 is_invariant2) : is_invariant2_(is_invariant2)
    {
        matrix_parent::InitLocalMatrixStorage({ {
            { 6, 1 }, // contravariant_metric_as_vector
            { 1, 6 }, // transposed_contravariant_metric_as_vector
            { 6, 6 }  // helper matrix - only used if I2 present
        } });
    }


    void Input::Update(const LocalMatrix& contravariant_metric_tensor)
    {
        assert(contravariant_metric_tensor.shape(0) == 3);
        assert(contravariant_metric_tensor.shape(1) == 3);

        auto& contravariant_metric_as_vector = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::contravariant_metric_as_vector)>();

        contravariant_metric_as_vector(0, 0) = contravariant_metric_tensor(0, 0);
        contravariant_metric_as_vector(1, 0) = contravariant_metric_tensor(1, 1);
        contravariant_metric_as_vector(2, 0) = contravariant_metric_tensor(2, 2);
        contravariant_metric_as_vector(3, 0) = 2. * contravariant_metric_tensor(0, 1);
        contravariant_metric_as_vector(4, 0) = 2. * contravariant_metric_tensor(1, 2);
        contravariant_metric_as_vector(5, 0) = 2. * contravariant_metric_tensor(0, 2);

        auto& transposed_contravariant_metric_as_vector = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::transposed_contravariant_metric_as_vector)>();

        xt::noalias(transposed_contravariant_metric_as_vector) = xt::transpose(contravariant_metric_as_vector);

        ComputeDeterminant(contravariant_metric_tensor);

        if (IsI2Supported())
            ComputeI2Helper();
    }


    void Input::ComputeDeterminant(const LocalMatrix& contravariant_metric_tensor)
    {
        determinant_ = xt::linalg::det(contravariant_metric_tensor);
    }


    double Input::GetDeterminant() const noexcept
    {
        return determinant_;
    }


    bool Input::IsI2Supported() const noexcept
    {
        return is_invariant2_ == is_I2::yes;
    }


    void Input::ComputeI2Helper()
    {
        assert(IsI2Supported());

        auto& contravariant_metric_as_vector = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::contravariant_metric_as_vector)>();

        using NumericNS::Square;
        assert(contravariant_metric_as_vector.shape(0) == 6 && contravariant_metric_as_vector.shape(1) == 1);

        const double g1 = contravariant_metric_as_vector(0, 0);
        const double g2 = contravariant_metric_as_vector(1, 0);
        const double g3 = contravariant_metric_as_vector(2, 0);
        const double g4 = contravariant_metric_as_vector(3, 0);
        const double g5 = contravariant_metric_as_vector(4, 0);
        const double g6 = contravariant_metric_as_vector(5, 0);

        auto& I2_helper = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::I2_helper)>();

        // Diagonal terms.
        I2_helper(0, 0) = 0.;
        I2_helper(1, 1) = 0.;
        I2_helper(2, 2) = 0.;
        I2_helper(3, 3) = 0.5 * Square(g4) - 2. * g1 * g2;
        I2_helper(4, 4) = 0.5 * Square(g5) - 2. * g2 * g3;
        I2_helper(5, 5) = 0.5 * Square(g6) - 2. * g1 * g3;

        // Upper triangle terms and symmetry.
        I2_helper(0, 1) = I2_helper(1, 0) = g1 * g2 - 0.25 * Square(g4);
        I2_helper(0, 2) = I2_helper(2, 0) = g1 * g3 - 0.25 * Square(g6);
        I2_helper(0, 3) = I2_helper(3, 0) = 0.;
        I2_helper(0, 4) = I2_helper(4, 0) = g1 * g5 - 0.5 * g4 * g6;
        I2_helper(0, 5) = I2_helper(5, 0) = 0.;

        I2_helper(1, 2) = I2_helper(2, 1) = g2 * g3 - 0.25 * Square(g5);
        I2_helper(1, 3) = I2_helper(3, 1) = 0.;
        I2_helper(1, 4) = I2_helper(4, 1) = 0.;
        I2_helper(1, 5) = I2_helper(5, 1) = g2 * g6 - 0.5 * g4 * g5;

        I2_helper(2, 3) = I2_helper(3, 2) = g3 * g4 - 0.5 * g5 * g6;
        I2_helper(2, 4) = I2_helper(4, 2) = 0.;
        I2_helper(2, 5) = I2_helper(5, 2) = 0.;

        I2_helper(3, 4) = I2_helper(4, 3) = 0.5 * g4 * g5 - g2 * g6;
        I2_helper(3, 5) = I2_helper(5, 3) = 0.5 * g4 * g6 - g1 * g5;

        I2_helper(4, 5) = I2_helper(5, 4) = 0.5 * g5 * g6 - g3 * g4;
    }


} // namespace MoReFEM::InvariantNS::GeneralizedNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
