// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_LOGI3PENALIZATION_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_LOGI3PENALIZATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <iosfwd>
#include <memory>
#include <string> // IWYU pragma: keep

#include "FiniteElement/QuadratureRules/QuadraturePoint.hpp"

#include "Core/Parameter/FiberEnum.hpp"

#include "ParameterInstances/Compound/Solid/Solid.hpp"
#include "Parameters/Parameter.hpp"
#include "Parameters/TimeDependency/None.hpp"

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Advanced/InvariantHolder.hpp" // IWYU pragma: export
#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Invariants/Invariant3.hpp"    // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class QuadraturePoint; }


// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::HyperelasticLawNS
{


    /*!
     * \brief Ciarlet-Geymonat laws, to use a a policy of class HyperElasticityLaw.
     */
    // clang-format off
    template
    <
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT,
        ::MoReFEM::InvariantNS::coords CoordsEnumT = ::MoReFEM::InvariantNS::coords::cartesian
    >
    class LogI3Penalization
    : public Advanced::InvariantHolder
    <
        typename InvariantNS::Invariant3<CoordsEnumT>::type
    >
    // clang-format on
    {
      public:
        //! Return the name of the hyperelastic law.
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_time_manager_type_alias
        using time_manager_type = TimeManagerT;

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = ScalarParameter<TimeManagerT, ParameterNS::TimeDependencyNS::None>;

        //! \copydoc doxygen_hide_alias_self
        using self = LogI3Penalization<TimeManagerT, CoordsEnumT>;

        //! \copydoc doxygen_hide_alias_const_unique_ptr
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Alias to parent.
        // clang-format off
        using invariant_holder_parent =
        Advanced::InvariantHolder
        <
            typename InvariantNS::Invariant3<CoordsEnumT>::type
        >;
        // clang-format on


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] mesh_dimension Dimension of the mesh.
         * \param[in] solid Object which provides the required material parameters for the solid.
         */
        explicit LogI3Penalization(const std::size_t mesh_dimension, const Solid<TimeManagerT>& solid);

        //! Destructor.
        ~LogI3Penalization() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        LogI3Penalization(const LogI3Penalization& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        LogI3Penalization(LogI3Penalization&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        LogI3Penalization& operator=(const LogI3Penalization& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        LogI3Penalization& operator=(LogI3Penalization&& rhs) = delete;

        ///@}


      public:
        //! Function W.
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double W(const QuadraturePoint& quadrature_point, const GeometricElt& geom_elt) const;

        //! Derivative of W with respect of third invariant (dWdI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double FirstDerivativeWThirdInvariant(const QuadraturePoint& quadrature_point,
                                              const GeometricElt& geom_elt) const;

        //! Second derivative of W with respect of third invariant (d2WdI3dI3)
        //! \copydoc doxygen_hide_hyperelastic_law_parameters
        double SecondDerivativeWThirdInvariant(const QuadraturePoint& quadrature_point,
                                               const GeometricElt& geom_elt) const;


      public:
        //! Hyperelastic bulk.
        const scalar_parameter_type& GetBulk() const noexcept;

      private:
        //! Bulk.
        const scalar_parameter_type& bulk_;
    };


} // namespace MoReFEM::HyperelasticLawNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/HyperelasticLaws/LogI3Penalization.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_HYPERELASTICLAWS_LOGI3PENALIZATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
