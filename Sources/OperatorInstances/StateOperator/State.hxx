// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_STATEOPERATOR_STATE_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_STATEOPERATOR_STATE_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/StateOperator/State.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "OperatorInstances/StateOperator/State.hpp"


#include <cassert>
#include <cstddef> // IWYU pragma: keep

#include "Utilities/Numeric/Numeric.hpp"


namespace MoReFEM::InterpolationOperatorNS
{


    inline const State::DirichletDofListType& State::GetProcessorWiseDirichletDofList() const
    {
        return processor_wise_dirichlet_dof_list_;
    }


    inline bool State::AreProcessorWiseDirichletDof() const
    {
        return !processor_wise_dirichlet_dof_list_.empty();
    }


#ifndef NDEBUG


    inline std::size_t State::NexpectedProgramWiseDofIncludingDirichlet() const noexcept
    {
        assert(Nexpected_program_wise_dof_including_dirichlet_ != NumericNS::UninitializedIndex<std::size_t>());
        return Nexpected_program_wise_dof_including_dirichlet_;
    }


    inline std::size_t State::NexpectedProgramWiseDofExcludingDirichlet() const noexcept
    {
        assert(Nexpected_program_wise_dof_excluding_dirichlet_ != NumericNS::UninitializedIndex<std::size_t>());
        return Nexpected_program_wise_dof_excluding_dirichlet_;
    }


    inline std::size_t State::NexpectedProcessorWiseDofIncludingDirichlet() const noexcept
    {
        assert(Nexpected_processor_wise_dof_including_dirichlet_ != NumericNS::UninitializedIndex<std::size_t>());
        return Nexpected_processor_wise_dof_including_dirichlet_;
    }


    inline std::size_t State::NexpectedProcessorWiseDofExcludingDirichlet() const noexcept
    {
        assert(Nexpected_processor_wise_dof_excluding_dirichlet_ != NumericNS::UninitializedIndex<std::size_t>());
        return Nexpected_processor_wise_dof_excluding_dirichlet_;
    }

#endif // NDEBUG


} // namespace MoReFEM::InterpolationOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_STATEOPERATOR_STATE_DOT_HXX_
// *** MoReFEM end header guards *** < //
