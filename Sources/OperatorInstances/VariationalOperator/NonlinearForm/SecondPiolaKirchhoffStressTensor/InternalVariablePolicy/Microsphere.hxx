// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_MICROSPHERE_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_MICROSPHERE_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Microsphere.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Microsphere.hpp"


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS
{


    // clang-format off
    template
    <
        std::size_t FiberIndexI4T,
        std::size_t FiberIndexI6T,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    template<class LocalOperatorStorageT>
    void Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::InitializeInternalVariablePolicy(
        const Domain& domain,
        const QuadratureRulePerTopology& quadrature_rule_per_topology,
        const TimeManagerT& time_manager,
        const LocalOperatorStorageT& local_operator_storage,
        input_internal_variable_policy_type* input_internal_variable_policy)
    {
        static_cast<void>(domain);
        static_cast<void>(quadrature_rule_per_topology);
        static_cast<void>(time_manager);
        static_cast<void>(local_operator_storage);

        assert(!(!input_internal_variable_policy));

        input_internal_variable_policy_ = input_internal_variable_policy;
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexI4T,
        std::size_t FiberIndexI6T,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline const typename Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::input_internal_variable_policy_type&
    Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::GetInputMicrosphere() const noexcept
    {
        assert(!(!input_internal_variable_policy_));
        return *input_internal_variable_policy_;
    }


    // clang-format off
    template
    <
        std::size_t FiberIndexI4T,
        std::size_t FiberIndexI6T,
        TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT
    >
    // clang-format on
    inline typename Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::input_internal_variable_policy_type&
    Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>::GetNonCstInputMicrosphere() noexcept
    {
        return const_cast<input_internal_variable_policy_type&>(GetInputMicrosphere());
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNALVARIABLEPOLICY_MICROSPHERE_DOT_HXX_
// *** MoReFEM end header guards *** < //
