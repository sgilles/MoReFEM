// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_INTERNAL_TYINGPOINT_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_INTERNAL_TYINGPOINT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <memory>
#include <vector>

#include "Utilities/LinearAlgebra/LocalAlias.hpp"

#include "Geometry/Coords/LocalCoords.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::Advanced { class RefFEltInLocalOperator; }
namespace MoReFEM::Internal::RefFEltNS { class BasicRefFElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    /*!
     * \brief Helper class to store data for a given single tying points.
     *
     *  Depending on the policy used, they might then be used directly (typically for \a None')and the non interpolated
     * of \a MITC4) or interpolated
     *  (\a MITC9, part of \a MITC4).
     */
    class TyingPoint
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = TyingPoint;

        //! Alias to shared pointer to a constant object.
        using const_shared_ptr = std::shared_ptr<self>;

        //! Alias to vector shared pointer to a constant object.
        using vector_const_shared_ptr = std::vector<const_shared_ptr>;


      public:
        /// \name Special members.
        ///@{

      private:
        /*!
         * \brief Straightforward constructor: all data attributes values are given and set directly.
         *
         * \param[in] shape_function_value Value of the shape function.
         * \param[in] dphi_geo Gradient of the geometric shape functions
         * \param[in] dphi_felt Gradient of the finite element shape functions (for the actual displacement unknown)
         * \param[in] dphi_test_felt Gradient of the finite element shape functions for the test functions (for the test function related to the
         * actual displacement unknown)
         */
        explicit TyingPoint(double shape_function_value,
                            LocalMatrix&& dphi_geo,
                            LocalMatrix&& dphi_felt,
                            LocalMatrix&& dphi_test_felt);

      public:
        /*!
         * \class doxygen_hide_ref_elements_geom_felt
         *
         * \param[in] ref_geom_elt Reference geometric element used.
         * \param[in] ref_felt Reference finite element used for the unknown.
         * \param[in] test_ref_felt Reference finite element used for the test function.
         */

        /*!
         * \brief Constructor.
         *
         * \param[in] shape_function_value Value of the shape function.
         * \param[in] local_coords \a LocalCoords related to the tying point (might be a \a QuadraturePoint or not).
         * \copydoc doxygen_hide_ref_elements_geom_felt
         */
        template<class LocalCoordsT>
        explicit TyingPoint(double shape_function_value,
                            LocalCoordsT&& local_coords,
                            const RefGeomElt& ref_geom_elt,
                            const Advanced::RefFEltInLocalOperator& ref_felt,
                            const Advanced::RefFEltInLocalOperator& test_ref_felt);


        //! Destructor.
        ~TyingPoint() = default;

        //! \copydoc doxygen_hide_copy_constructor
        TyingPoint(const TyingPoint& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        TyingPoint(TyingPoint&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        TyingPoint& operator=(const TyingPoint& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        TyingPoint& operator=(TyingPoint&& rhs) = delete;

        ///@}

      public:
        //! Returns the value of the shape functions.
        double GetShapeFunctionValue() const noexcept;

        //! Returns the values of the geometric gradient of shape functions.
        const LocalMatrix& GetGeometricGradient() const noexcept;

        //! Returns the values of the finite element gradient of shape functions for the unknown.
        const LocalMatrix& GetFEltGradient() const noexcept;

        //! Returns the values of the finite element gradient of shape functions for the test unknown.
        const LocalMatrix& GetTestFEltGradient() const noexcept;


      private:
        //! Value of the shape function.
        double shape_function_value_;

        //! Gradient of the geometric shape functions
        const LocalMatrix dphi_geo_;

        //! Gradient of the finite element shape functions (for the actual displacement unknown)
        const LocalMatrix dphi_felt_;

        //! Gradient of the finite element shape functions for the test functions (for the test function related to the
        //! actual displacement unknown).
        const LocalMatrix dphi_test_felt_;
    };


    /*!
     * \brief Helper function used in the public constructor of \a TyingPoint which computes the gradient of the geometric shape functions.
     *
     * \param[in] ref_geom_elt Reference geometric element used.
     * \param[in] local_coords \a LocalCoords related to the tying point (might be a \a QuadraturePoint or not).
     *
     * \return Gradient of the geometric shape functions.
     */
    LocalMatrix ComputeGeometricGradientAtLocalCoords(const RefGeomElt& ref_geom_elt, const LocalCoords& local_coords);


    /*!
     * \brief Helper function used in the public constructor of \a TyingPoint which computes the gradient of the finite element shape functions.
     *
     * \param[in] ref_felt Reference finite element used.
     * \param[in] local_coords \a LocalCoords related to the tying point (might be a \a QuadraturePoint or not).
     *
     * \return Gradient of the finite element shape functions.
     */
    LocalMatrix ComputeFEltGradientAtLocalCoords(const Internal::RefFEltNS::BasicRefFElt& ref_felt,
                                                 const LocalCoords& local_coords);


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPoint.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_INTERNAL_TYINGPOINT_DOT_HPP_
// *** MoReFEM end header guards *** < //
