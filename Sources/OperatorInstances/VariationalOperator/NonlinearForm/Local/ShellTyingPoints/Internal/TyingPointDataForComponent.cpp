// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPointDataForComponent.hpp"


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    TyingPointDataForComponent::TyingPointDataForComponent(const TyingPoint::vector_const_shared_ptr& tying_point_list,
                                                           LocalMatrix grad_grad_product)
    : tying_point_list_(tying_point_list), grad_grad_product_(grad_grad_product)
    { }


    TyingPointDataForComponent::TyingPointDataForComponent(const TyingPoint::const_shared_ptr& tying_point_ptr,
                                                           LocalMatrix grad_grad_product)
    : TyingPointDataForComponent(TyingPoint::vector_const_shared_ptr{ tying_point_ptr }, grad_grad_product)
    { }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
