// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_POLICIES_BASE_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_POLICIES_BASE_DOT_HPP_
// *** MoReFEM header guards *** < //


#include <array>
#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <vector>

#include "Utilities/Containers/EnumClass.hpp"

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/Enum.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Internal/TyingPointDataForComponent.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class QuadraturePoint; }
namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::Advanced { class RefFEltInLocalOperator; }
namespace MoReFEM::Advanced::LocalVariationalOperatorNS { class InformationAtQuadraturePoint; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS
{


    /*!
     * \brief Base class for tying point policy, which encapsulates the storage of the data and the related accessor.
     *
     * The method to fill these data is left for the inherited class.
     */
    class Base
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = Base;

        //! Convenient alias.
        using tying_pt_interpolation_component =
            Internal::LocalVariationalOperatorNS::TyingPointsNS::tying_pt_interpolation_component;

        // clang-format off
        //! Storage for the MITC data, computed during the initialization phase.
        using mitc_data_type =
        std::vector
        <
            std::array
            <
                TyingPointDataForComponent::unique_ptr,
                EnumUnderlyingType(tying_pt_interpolation_component::Ncomponents)
            >
        >;
        // clang-format on

        //! Returns the name of the operator.
        static const std::string& ClassName();

      public:
        /// \name Special members.
        ///@{

        //! Constructor which does not initialize the object completely. A call to InitTyingPointData() is required.
        explicit Base() = default;

        //! Destructor.
        virtual ~Base();

        //! \copydoc doxygen_hide_copy_constructor
        Base(const Base& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Base(Base&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Base& operator=(const Base& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Base& operator=(Base&& rhs) = delete;

        ///@}


      public:
        /*!
         * \class doxygen_hide_init_point_data_prototype
         *
         * \brief Method which actually computes all of the tying point related data and stores them in
         *  TyingPointDataForComponent objects.
         *
         * \param[in] infos_at_quad_pt_list List of InformationAtQuadraturePoint objects, for each quad point.
         * \param[in] ref_geom_elt Reference geometric element used.
         * \param[in] ref_felt Reference finite element used for the unknown.
         * \param[in] test_ref_felt Reference finite element used for the test function.
         *
         */

        //! \copydoc doxygen_hide_init_point_data_prototype
        virtual void
        InitTyingPointData(const std::vector<Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint>&
                               infos_at_quad_pt_list,
                           const RefGeomElt& ref_geom_elt,
                           const Advanced::RefFEltInLocalOperator& ref_felt,
                           const Advanced::RefFEltInLocalOperator& test_ref_felt) = 0;

        /*!
         * \brief Returns the tying point data for a given quadrature point and a given component.
         *
         * \param[in] quad_pt Quadrature point  for which data is sought.
         * \param[in] tying_pt_component Component index for which the data is sought.
         *
         * \return The \a TyingPointDataForComponent object.
         */
        const TyingPointDataForComponent&
        GetTyingPointDataForComponent(const QuadraturePoint& quad_pt,
                                      const tying_pt_interpolation_component tying_pt_component) const;

      protected:
        //! Non constant accessor to the stored data, for the sake of the derived class only.
        mitc_data_type& GetNonCstMITCData() noexcept;

      private:
        //! Attribute containing the tying point data for each tying component. Vector index stands for a quadrature
        //! point index and array index stands for a component index.
        mitc_data_type mitc_data_;
    };


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::TyingPointsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/ShellTyingPoints/Policies/Base.hxx"


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SHELLTYINGPOINTS_POLICIES_BASE_DOT_HPP_
// *** MoReFEM end header guards *** < //
