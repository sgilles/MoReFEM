// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_SAMECAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_SAMECAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/QuasiIncompressibleCauchyGreenPolicy/SameCauchyGreenMixedSolidIncompressibility.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/QuasiIncompressibleCauchyGreenPolicy/SameCauchyGreenMixedSolidIncompressibility.hpp"


#include <cstddef> // IWYU pragma: keep

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Enum.hpp" // IWYU pragma: export


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::
        SameCauchyGreenMixedSolidIncompressibility(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                                   const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                                                   elementary_data_type&& a_elementary_data,
                                                   const cauchy_green_tensor_type& cauchy_green_tensor,
                                                   const HydrostaticLawPolicyT* hydrostatic_law,
                                                   const HydrostaticLawPolicyT* should_be_nullptr)
    : NonlinearLocalVariationalOperator(unknown_list, test_unknown_list, std::move(a_elementary_data)), matrix_parent(),
      vector_parent(), cauchy_green_tensor_(cauchy_green_tensor), hydrostatic_law_(hydrostatic_law)
    {
        static_cast<void>(should_be_nullptr);
        assert(should_be_nullptr == nullptr);
        const auto& elementary_data = GetElementaryData();

        const auto& displacement_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto Nnode_displacement = displacement_ref_felt.Nnode();

        const auto& pressure_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));
        const auto Nnode_pressure = pressure_ref_felt.Nnode();

        const auto& test_displacement_ref_felt = elementary_data.GetRefFElt(GetNthTestUnknown(0));
        const auto Nnode_test_displacement = test_displacement_ref_felt.Nnode();

        const auto& test_pressure_ref_felt = elementary_data.GetRefFElt(GetNthTestUnknown(1));
        const auto Nnode_test_pressure = test_pressure_ref_felt.Nnode();

        const auto Ncomponent = Advanced::ComponentNS::index_type{ elementary_data.GetGeomEltDimension() };
        const auto square_Ncomponent = NumericNS::Square(Ncomponent.Get());

        std::size_t engineering_vect_size(0ul);

        switch (Ncomponent.Get())
        {
        case 2:
            engineering_vect_size = 3u;
            break;
        case 3:
            engineering_vect_size = 6u;
            break;
        default:
            assert(false);
            break;
        }

        former_local_displacement_.resize(elementary_data.GetRefFElt(GetNthUnknown(0)).Ndof());

        former_local_pressure_.resize(elementary_data.GetRefFElt(GetNthUnknown(1)).Ndof());

        matrix_parent::InitLocalMatrixStorage({ {
            { square_Ncomponent, square_Ncomponent },         // tangent_matrix_disp_disp
            { square_Ncomponent, 1 },                         // tangent_matrix_disp_pres
            { 1, square_Ncomponent },                         // tangent_matrix_pres_disp
            { Ncomponent.Get(), Nnode_displacement },         // transposed_dphi_displacement
            { Ncomponent.Get(), Ncomponent.Get() },           // displacement_gradient,
            { engineering_vect_size, engineering_vect_size }, // hydrostatic_tangent
            { Ncomponent.Get(), Ncomponent.Get() },           // gradient-based block
            { Nnode_test_displacement, Nnode_displacement },  // block_contribution
            { Nnode_test_displacement, Ncomponent.Get() },    // dphi_test_disp_mult_gradient_based_block
            { square_Ncomponent, square_Ncomponent },         // linear_part
            { square_Ncomponent, engineering_vect_size },     // linear_part_intermediate_matrix
            { Ncomponent.Get(), 1 },                          // column_matrix,
            { Nnode_test_displacement, 1 },                   // dphi_test_disp_mult_column_matrix,
            { Nnode_test_displacement, Nnode_pressure },      // block_contribution_disp_pres,
            { 1, Ncomponent.Get() },                          // row_matrix,
            { 1, Nnode_displacement },                        // row_matrix_mult_transposed_dphi_disp,
            { Nnode_test_pressure, Nnode_displacement }       // block_contribution_pres_disp
        } });


        this->vector_parent::InitLocalVectorStorage({ {
            square_Ncomponent,     // rhs_disp
            engineering_vect_size, // hysdrostatic_stress
            engineering_vect_size, // diff_hydrostatic_stress_wrt_pres
            square_Ncomponent,     // tangent_vector_disp_pres
            square_Ncomponent      // tangent_vector_pres_disp
        } });

        deriv_green_lagrange_ =
            std::make_unique<DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>>(Ncomponent.Get());
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::ClassName()
    {
        static std::string name("SameCauchyGreenMixedSolidIncompressibility");
        return name;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline const std::vector<double>&
    SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::GetFormerLocalDisplacement()
        const noexcept
    {
        return former_local_displacement_;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline std::vector<double>&
    SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                               TimeManagerT>::GetNonCstFormerLocalDisplacement() noexcept
    {
        return const_cast<std::vector<double>&>(GetFormerLocalDisplacement());
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline const std::vector<double>&
    SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::GetFormerLocalPressure()
        const noexcept
    {
        return former_local_pressure_;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline std::vector<double>&
    SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                               TimeManagerT>::GetNonCstFormerLocalPressure() noexcept
    {
        return const_cast<std::vector<double>&>(GetFormerLocalPressure());
    }

    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::GetCauchyGreenTensor()
        const noexcept -> const cauchy_green_tensor_type&
    {
        return cauchy_green_tensor_;
    }

    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    DerivativeGreenLagrange<GreenLagrangeOrEta::green_lagrange>&
    SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                               TimeManagerT>::GetNonCstDerivativeGreenLagrange() noexcept
    {
        assert(!(!deriv_green_lagrange_));
        return *deriv_green_lagrange_;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::ComputeEltArray()
    {
        const auto& local_displacement = GetFormerLocalDisplacement();
        const auto& local_pressure = GetFormerLocalPressure();

        auto& elementary_data = GetNonCstElementaryData();

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();
        auto& vector_result = elementary_data.GetNonCstVectorResult();
        matrix_result.fill(0.);
        vector_result.fill(0.);

        auto& tangent_matrix_disp_disp =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix_disp_disp)>();

        auto& tangent_vector_disp_pres = this->vector_parent::template GetLocalVector<EnumUnderlyingType(
            LocalVectorIndex::tangent_vector_disp_pres)>();
        auto& tangent_vector_pres_disp = this->vector_parent::template GetLocalVector<EnumUnderlyingType(
            LocalVectorIndex::tangent_vector_pres_disp)>();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto& displacement_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto& pressure_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(1));

        const auto& test_displacement_ref_felt = elementary_data.GetRefFElt(GetNthTestUnknown(0));
        const auto& test_pressure_ref_felt = elementary_data.GetRefFElt(GetNthTestUnknown(1));

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        const auto felt_space_dimension = displacement_ref_felt.GetFEltSpaceDimension();

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            tangent_matrix_disp_disp.fill(0.);
            tangent_vector_disp_pres.fill(0.);
            tangent_vector_pres_disp.fill(0.);

            PrepareInternalDataForQuadraturePoint(infos_at_quad_pt,
                                                  geom_elt,
                                                  displacement_ref_felt,
                                                  pressure_ref_felt,
                                                  test_displacement_ref_felt,
                                                  test_pressure_ref_felt,
                                                  local_displacement,
                                                  local_pressure,
                                                  felt_space_dimension);
        }
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::
        PrepareInternalDataForQuadraturePoint(const InformationAtQuadraturePoint& infos_at_quad_pt,
                                              const GeometricElt& geom_elt,
                                              const Advanced::RefFEltInLocalOperator& displacement_ref_felt,
                                              const Advanced::RefFEltInLocalOperator& pressure_ref_felt,
                                              const Advanced::RefFEltInLocalOperator& test_displacement_ref_felt,
                                              const Advanced::RefFEltInLocalOperator& test_pressure_ref_felt,
                                              const std::vector<double>& local_displacement,
                                              const std::vector<double>& local_pressure,
                                              const std::size_t felt_space_dimension)
    {
        switch (felt_space_dimension)
        {
        case 2:
        {
            this->PrepareInternalDataForQuadraturePointForDimension<2>(infos_at_quad_pt,
                                                                       geom_elt,
                                                                       displacement_ref_felt,
                                                                       pressure_ref_felt,
                                                                       test_displacement_ref_felt,
                                                                       test_pressure_ref_felt,
                                                                       local_displacement,
                                                                       local_pressure);
            break;
        }
        case 3:
        {
            this->PrepareInternalDataForQuadraturePointForDimension<3>(infos_at_quad_pt,
                                                                       geom_elt,
                                                                       displacement_ref_felt,
                                                                       pressure_ref_felt,
                                                                       test_displacement_ref_felt,
                                                                       test_pressure_ref_felt,
                                                                       local_displacement,
                                                                       local_pressure);
            break;
        }
        default:
            assert(false);
        }
    }

    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<std::size_t FeltSpaceDimensionT>
    void SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::
        PrepareInternalDataForQuadraturePointForDimension(
            const InformationAtQuadraturePoint& infos_at_quad_pt,
            const GeometricElt& geom_elt,
            const Advanced::RefFEltInLocalOperator& displacement_ref_felt,
            const Advanced::RefFEltInLocalOperator& pressure_ref_felt,
            const Advanced::RefFEltInLocalOperator& test_displacement_ref_felt,
            const Advanced::RefFEltInLocalOperator& test_pressure_ref_felt,
            const std::vector<double>& local_displacement,
            const std::vector<double>& local_pressure)
    {
        auto& elementary_data = GetNonCstElementaryData();

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();
        auto& vector_result = elementary_data.GetNonCstVectorResult();

        const auto Ncomponent = Advanced::ComponentNS::index_type{ displacement_ref_felt.GetMeshDimension() };

        const auto Nnode_disp = displacement_ref_felt.Nnode();
        const auto Nnode_pressure = pressure_ref_felt.Nnode();

        const auto Nnode_test_disp = test_displacement_ref_felt.Nnode();
        const auto Nnode_test_pressure = test_pressure_ref_felt.Nnode();

        const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

        decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
        decltype(auto) quad_pt_test_unknown_list_data = infos_at_quad_pt.GetTestUnknownData();

        const auto weight_meas = quad_pt.GetWeight() * quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant();

        auto& tangent_matrix_disp_disp =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix_disp_disp)>();

        auto& tangent_vector_disp_pres = this->vector_parent::template GetLocalVector<EnumUnderlyingType(
            LocalVectorIndex::tangent_vector_disp_pres)>();

        auto& tangent_vector_pres_disp = this->vector_parent::template GetLocalVector<EnumUnderlyingType(
            LocalVectorIndex::tangent_vector_pres_disp)>();

        auto& linear_part_intermediate_matrix = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::linear_part_intermediate_matrix)>();

        auto& linear_part = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::linear_part)>();

        auto& displacement_gradient =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::displacement_gradient)>();

        auto& hydrostatic_stress =
            this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::hydrostatic_stress)>();

        auto& diff_hydrostatic_stress_wrt_pres = this->vector_parent::template GetLocalVector<EnumUnderlyingType(
            LocalVectorIndex::diff_hydrostatic_stress_wrt_pres)>();

        auto& hydrostatic_tangent =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::hydrostatic_tangent)>();

        auto& rhs_disp = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::rhs_disp)>();

        auto& gradient_based_block =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::gradient_based_block)>();

        auto& dphi_test_disp_mult_gradient_based_block = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::dphi_test_disp_mult_gradient_based_block)>();

        auto& block_contribution =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_contribution)>();

        auto& transposed_dphi_displacement = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::transposed_dphi_displacement)>();

        auto& column_matrix =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::column_matrix)>();
        auto& dphi_test_disp_mult_column_matrix = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::dphi_test_disp_mult_column_matrix)>();
        auto& block_contribution_disp_pres = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::block_contribution_disp_pres)>();
        auto& row_matrix = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::row_matrix)>();
        auto& row_matrix_mult_transposed_dphi_disp = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::row_matrix_mult_transposed_dphi_disp)>();
        auto& block_contribution_pres_disp = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::block_contribution_pres_disp)>();

        const auto& grad_felt_phi = quad_pt_unknown_data.GetGradientFEltPhi();                // on (u p)
        const auto& test_grad_felt_phi = quad_pt_test_unknown_list_data.GetGradientFEltPhi(); // on (u* p*)

        const auto& dphi_displacement = ExtractSubMatrix(grad_felt_phi, displacement_ref_felt);

        const auto& dphi_test_displacement = ExtractSubMatrix(test_grad_felt_phi, test_displacement_ref_felt);

        const auto& phi = quad_pt_unknown_data.GetFEltPhi();                // on (u p)
        const auto& test_phi = quad_pt_test_unknown_list_data.GetFEltPhi(); // on (u* p*)

        const auto& pressure_phi = ExtractSubVector(phi, pressure_ref_felt);
        const auto& test_pressure_phi = ExtractSubVector(test_phi, test_pressure_ref_felt);

        xt::noalias(transposed_dphi_displacement) = xt::transpose(dphi_displacement);

        Advanced::OperatorNS::ComputeGradientDisplacementMatrix(
            quad_pt_unknown_data, displacement_ref_felt, local_displacement, displacement_gradient);

        auto& derivative_green_lagrange = GetNonCstDerivativeGreenLagrange();
        const auto& derivative_green_lagrange_at_quad_point = derivative_green_lagrange.Update(displacement_gradient);
        const auto& transposed_derivative_green_lagrange_at_quad_point = derivative_green_lagrange.GetTransposed();
        const auto& cauchy_green_tensor = GetCauchyGreenTensor();
        const auto& cauchy_green_tensor_at_quad_point = cauchy_green_tensor.GetValue(quad_pt, geom_elt);

        GetNonCstHydrostaticLaw().UpdateInvariants(cauchy_green_tensor_at_quad_point, quad_pt, geom_elt);

        double pressure_at_quad_point = 0.;
        for (auto node_index = 0ul; node_index < Nnode_pressure; ++node_index)
        {
            pressure_at_quad_point += pressure_phi(node_index) * local_pressure[node_index];
        }

        decltype(auto) hydrostatic_law = GetHydrostaticLaw();

        const auto& dI3dC =
            hydrostatic_law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I3>();

        hydrostatic_stress = dI3dC;
        diff_hydrostatic_stress_wrt_pres = hydrostatic_stress;

        const double inv3 = hydrostatic_law.template GetInvariant<::MoReFEM::InvariantNS::index::I3>();
        const double sqrt_inv3 = std::sqrt(inv3);

        xt::noalias(hydrostatic_stress) *= -pressure_at_quad_point / sqrt_inv3;
        xt::noalias(rhs_disp) = xt::linalg::dot(transposed_derivative_green_lagrange_at_quad_point, hydrostatic_stress);
        xt::noalias(diff_hydrostatic_stress_wrt_pres) /= -sqrt_inv3;


        const auto penalization_gradient = hydrostatic_law.FirstDerivativeWThirdInvariant(quad_pt, geom_elt);

        const double bulk = hydrostatic_law.GetBulk().GetValue(quad_pt, geom_elt);
        const auto rhs_pres = penalization_gradient + pressure_at_quad_point / bulk;

        // Residual on disp
        for (ComponentNS::index_type row_component{ 0ul }; row_component < Ncomponent; ++row_component)
        {
            const auto dof_first_index = test_displacement_ref_felt.GetIndexFirstDofInElementaryData(row_component);
            const auto component_first_index = row_component.Get() * Ncomponent.Get();

            // Compute the new contribution to vector_result here.
            // Product matrix vector is inlined here to avoid creation of an intermediate subset of \a rhs_part.
            for (auto row_node = 0ul; row_node < Nnode_disp; ++row_node)
            {
                double value = 0.;

                for (Advanced::ComponentNS::index_type col{ 0ul }; col < Ncomponent; ++col)
                    value += dphi_test_displacement(row_node, col.Get()) * rhs_disp(col.Get() + component_first_index);

                vector_result(dof_first_index + row_node) += value * weight_meas;
            }
        }

        // Residual on pres
        const auto dof_first_index_pres = Nnode_disp * Ncomponent.Get();
        for (auto row_node = 0ul; row_node < Nnode_pressure; ++row_node)
        {
            vector_result(dof_first_index_pres + row_node) += rhs_pres * weight_meas * test_pressure_phi(row_node);
        }

        // Derivative disp disp
        const auto& d2I3dCdC =
            hydrostatic_law.template GetSecondDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I3>();

        xt::noalias(hydrostatic_tangent) = xt::linalg::outer(dI3dC, dI3dC);

        xt::noalias(hydrostatic_tangent) *= (pressure_at_quad_point * NumericNS::Pow(inv3, -1.5));

        xt::noalias(hydrostatic_tangent) -= 2 * sqrt_inv3 * pressure_at_quad_point * d2I3dCdC;

        Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::ComputeLinearPart(
            derivative_green_lagrange_at_quad_point,
            transposed_derivative_green_lagrange_at_quad_point,
            hydrostatic_tangent,
            linear_part_intermediate_matrix,
            // < internal quantity; better design
            // would be to encapsulate it in
            // an Internal class but I have no time
            // to do this minor fix now.
            linear_part);

        Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS ::ComputeNonLinearPart<
            FeltSpaceDimensionT>(hydrostatic_stress, tangent_matrix_disp_disp);

        xt::noalias(tangent_matrix_disp_disp) += linear_part;

        // Derivative disp pres
        xt::noalias(tangent_vector_disp_pres) =
            xt::linalg::dot(transposed_derivative_green_lagrange_at_quad_point, diff_hydrostatic_stress_wrt_pres);

        // Derivative pres disp
        const auto penalization_scd_deriv = hydrostatic_law.SecondDerivativeWThirdInvariant(quad_pt, geom_elt);

        tangent_vector_pres_disp = tangent_vector_disp_pres; // Maybe not so true if the time scheme is different
        xt::noalias(tangent_vector_pres_disp) *= -2. * sqrt_inv3 * penalization_scd_deriv;

        // Derivative pres pres
        const double tangent_matrix_pres_pres = weight_meas / bulk;

        // Tangent disp disp
        for (ComponentNS::index_type row_component{ 0ul }; row_component < Ncomponent; ++row_component)
        {
            const auto row_first_index = test_displacement_ref_felt.GetIndexFirstDofInElementaryData(row_component);

            for (ComponentNS::index_type col_component{ 0ul }; col_component < Ncomponent; ++col_component)
            {
                const auto col_first_index = displacement_ref_felt.GetIndexFirstDofInElementaryData(col_component);

                Advanced::LocalVariationalOperatorNS::ExtractGradientBasedBlock(
                    tangent_matrix_disp_disp, row_component, col_component, gradient_based_block);

                xt::noalias(dphi_test_disp_mult_gradient_based_block) =
                    xt::linalg::dot(dphi_test_displacement, gradient_based_block);

                xt::noalias(block_contribution) =
                    weight_meas
                    * xt::linalg::dot(dphi_test_disp_mult_gradient_based_block, transposed_dphi_displacement);

                for (auto row_node = 0ul; row_node < Nnode_test_disp; ++row_node)
                {
                    for (auto col_node = 0ul; col_node < Nnode_disp; ++col_node)
                        matrix_result(row_first_index + row_node, col_first_index + col_node) +=
                            block_contribution(row_node, col_node);
                }
            }
        }

        // Tangent disp pres
        const auto col_first_index_pressure = Ncomponent.Get() * Nnode_disp;
        const auto row_first_index_pressure = Ncomponent.Get() * Nnode_test_disp;

        for (ComponentNS::index_type row_component{ 0ul }; row_component < Ncomponent; ++row_component)
        {
            const auto row_first_index_disp =
                test_displacement_ref_felt.GetIndexFirstDofInElementaryData(row_component);

            ExtractGradientBasedBlockColumnMatrix(tangent_vector_disp_pres, row_component, column_matrix);

            xt::noalias(dphi_test_disp_mult_column_matrix) =
                weight_meas * xt::linalg::dot(dphi_test_displacement, column_matrix);

            for (auto row_node = 0ul; row_node < Nnode_test_disp; ++row_node)
            {
                for (auto col_node = 0ul; col_node < Nnode_pressure; ++col_node)
                {
                    block_contribution_disp_pres(row_node, col_node) =
                        dphi_test_disp_mult_column_matrix(row_node, 0) * pressure_phi(col_node);
                }
            }

            for (auto row_node = 0ul; row_node < Nnode_test_disp; ++row_node)
            {
                for (auto col_node = 0ul; col_node < Nnode_pressure; ++col_node)
                    matrix_result(row_first_index_disp + row_node, col_first_index_pressure + col_node) +=
                        block_contribution_disp_pres(row_node, col_node);
            }
        }


        // Tangent pres disp
        for (ComponentNS::index_type col_component{ 0ul }; col_component < Ncomponent; ++col_component)
        {
            const auto col_first_index_disp = displacement_ref_felt.GetIndexFirstDofInElementaryData(col_component);

            ExtractGradientBasedBlockRowMatrix(tangent_vector_pres_disp, col_component, row_matrix);

            xt::noalias(row_matrix_mult_transposed_dphi_disp) =
                weight_meas * xt::linalg::dot(row_matrix, transposed_dphi_displacement);

            for (auto row_node = 0ul; row_node < Nnode_test_pressure; ++row_node)
            {
                for (auto col_node = 0ul; col_node < Nnode_disp; ++col_node)
                {
                    block_contribution_pres_disp(row_node, col_node) =
                        test_pressure_phi(row_node) * row_matrix_mult_transposed_dphi_disp(0, col_node);
                }
            }

            for (auto row_node = 0ul; row_node < Nnode_test_pressure; ++row_node)
            {
                for (auto col_node = 0ul; col_node < Nnode_disp; ++col_node)
                    matrix_result(row_first_index_pressure + row_node, col_first_index_disp + col_node) +=
                        block_contribution_pres_disp(row_node, col_node);
            }
        }

        // Tangent pres pres
        for (auto row_node = 0ul; row_node < Nnode_test_pressure; ++row_node)
        {
            for (auto col_node = 0ul; col_node < Nnode_pressure; ++col_node)
                matrix_result(row_first_index_pressure + row_node, col_first_index_pressure + col_node) +=
                    tangent_matrix_pres_pres * test_pressure_phi(row_node) * pressure_phi(col_node);
        }
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline const HydrostaticLawPolicyT&
    SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::GetHydrostaticLaw() const noexcept
    {
        assert(!(!hydrostatic_law_));
        return *hydrostatic_law_;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline HydrostaticLawPolicyT&
    SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::GetNonCstHydrostaticLaw() noexcept
    {
        return const_cast<HydrostaticLawPolicyT&>(GetHydrostaticLaw());
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_SAMECAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HXX_
// *** MoReFEM end header guards *** < //
