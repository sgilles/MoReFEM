// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_NONLINEARSHELL_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_NONLINEARSHELL_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/NonlinearShell.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::NonlinearShell(
        const ExtendedUnknown::vector_const_shared_ptr& a_unknown_storage,
        const ExtendedUnknown::vector_const_shared_ptr& a_test_unknown_storage,
        elementary_data_type&& a_elementary_data,
        const typename HyperelasticityPolicyT::law_type* hyperelastic_law)
    : HyperelasticityPolicyT(a_elementary_data.GetMeshDimension(), hyperelastic_law), TyingPointsPolicyT(),
      parent(a_unknown_storage, a_test_unknown_storage, std::move(a_elementary_data)), matrix_parent(), vector_parent(),
      generalized_input_(InvariantNS::GeneralizedNS::is_I2::yes)
    {
        const auto& elementary_data = GetElementaryData();
        const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto Nnode = unknown_ref_felt.Nnode();

        const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
        const auto Nnode_test = test_unknown_ref_felt.Nnode();
        const auto& ref_geom_elt = elementary_data.GetRefGeomElt();

#ifndef NDEBUG
        {
            const auto ref_felt_space_dimension = unknown_ref_felt.GetFEltSpaceDimension();

            const auto euclidean_dimension = unknown_ref_felt.GetMeshDimension();

            assert(ref_felt_space_dimension == euclidean_dimension && "This operator is a volumic operator.");
        }
#endif // NDEBUG

        former_local_displacement_.resize(elementary_data.NdofCol());

        matrix_parent::InitLocalMatrixStorage({ {
            { 6, 6 },                      // d2W
            { 3, 3 },                      // displacement_gradient_at_tying_pt
            { 3, 3 },                      // covariant_basis,
            { 3, 3 },                      // transposed_covariant_basis,
            { 3, 3 },                      // covariant_metric_tensor,
            { 3, 3 },                      // contravariant_metric_tensor,
            { 6, 1 },                      // cauchy_green_as_vector
            { 6, 3 * Nnode },              // De_dot_test_grad
            { 3 * Nnode_test, 6 },         // transposed_De_dot_test_grad
            { 3 * Nnode_test, 6 },         // de_dot_d2W
            { 3 * Nnode_test, 3 * Nnode }, // linear_part
            { 3, 3 },                      // covariant_basis_at_tying_pt
            { 3, 3 },                      // contravariant_basis
        } });

        vector_parent::InitLocalVectorStorage({ {
            6, // dW
            9  // extended_dW
        } });

        const auto& information_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        TyingPointsPolicyT::InitTyingPointData(
            information_at_quad_pt_list, ref_geom_elt, unknown_ref_felt, test_unknown_ref_felt);

        InitGradGradArray();
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::~NonlinearShell() = default;


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    const std::string& NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::ClassName()
    {
        static std::string name("NonlinearShell");
        return name;
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    void NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::InitLocalComputation()
    { }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    void NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::ComputeEltArray()
    {
        auto& elementary_data = GetNonCstElementaryData();

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();
        matrix_result.fill(0.);

        auto& linear_part = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::linear_part)>();
        linear_part.fill(0.);

        auto& vector_result = elementary_data.GetNonCstVectorResult();
        vector_result.fill(0.);

        const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        std::size_t quad_pt_index = 0;
        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
            const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            ComputeMetricTensorsAtQuadPt(quad_pt_unknown_data);

            PrepareInternalDataFromTyingPtToQuadPt(quad_pt);

            ComputeWDerivates(quad_pt, geom_elt, unknown_ref_felt);

            const auto& d2W = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::d2W)>();
            const auto& dW = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::dW)>();
            const auto& De_dot_test_grad =
                matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::De_dot_test_grad)>();
            const auto& transposed_De_dot_test_grad = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
                LocalMatrixIndex::transposed_De_dot_test_grad)>();

            const auto& covariant_basis =
                matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_basis)>();

            const auto& geometric_jacobian = std::fabs(xt::linalg::det(covariant_basis));

            const auto weight_meas = quad_pt.GetWeight() * geometric_jacobian;

            // Linear term.
            if (parent::DoAssembleIntoVector())
            {
                xt::noalias(vector_result) += weight_meas * xt::linalg::dot(transposed_De_dot_test_grad, dW);
            }

            // Bilinear terms. There are in fact two: one for the linear part and another for non-linear one.
            if (parent::DoAssembleIntoMatrix())
            {
                auto& De_dot_d2W =
                    matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::de_dot_d2W)>();
                xt::noalias(De_dot_d2W) = xt::linalg::dot(transposed_De_dot_test_grad, d2W);
                xt::noalias(linear_part) = xt::linalg::dot(De_dot_d2W, De_dot_test_grad);
                matrix_result += weight_meas * linear_part;

                const auto& extended_dW =
                    this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::extended_dW)>();

                const auto& grad_grad_ij_at_quad_pt = GetGradGradProductAtQuadPt(quad_pt_index);

                assert(grad_grad_ij_at_quad_pt.size() == extended_dW.size());
                // Non-linear part.
                for (auto i = 0ul, size = extended_dW.size(); i < size; ++i)
                    matrix_result += weight_meas * extended_dW(i) * grad_grad_ij_at_quad_pt[i];
            }

            ++quad_pt_index;
        } // loop over quadrature points
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    void NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::ComputeMetricTensorsAtQuadPt(
        const InfosAtQuadPointNS::ForUnknownList& quad_pt_unknown_data)
    {
        const auto& elementary_data = GetElementaryData();

        auto& covariant_basis =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_basis)>();
        covariant_basis.fill(0.);

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        const auto& dphi_geo = quad_pt_unknown_data.GetGradientRefGeometricPhi();
        const auto Nshape_function = dphi_geo.shape(0);
        constexpr auto euclidean_dimension = 3;

        for (auto shape_fct_index = 0ul; shape_fct_index < Nshape_function; ++shape_fct_index)
        {
            const auto& coords_in_geom_elt = geom_elt.GetCoord(shape_fct_index);

            for (auto component_shape_function = 0ul; component_shape_function < euclidean_dimension;
                 ++component_shape_function)
            {
                for (auto coord_index = 0ul; coord_index < euclidean_dimension; ++coord_index)
                {
                    covariant_basis(coord_index, component_shape_function) +=
                        coords_in_geom_elt[coord_index] * dphi_geo(shape_fct_index, component_shape_function);
                }
            }
        }

        auto& covariant_metric_tensor =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_metric_tensor)>();
        auto& contravariant_metric_tensor =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::contravariant_metric_tensor)>();

        auto& transposed_covariant_basis =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_covariant_basis)>();
        xt::noalias(transposed_covariant_basis) = xt::transpose(covariant_basis);
        xt::noalias(covariant_metric_tensor) = xt::linalg::dot(transposed_covariant_basis, covariant_basis);

        xt::noalias(contravariant_metric_tensor) = xt::linalg::inv(covariant_metric_tensor);

        GetNonCstGeneralizedInputArguments().Update(contravariant_metric_tensor);

        auto& contravariant_basis =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::contravariant_basis)>();
        xt::noalias(contravariant_basis) = xt::linalg::dot(covariant_basis, contravariant_metric_tensor);
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    auto NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::ComputeCovariantBasisAtTyingPoint(
        const LocalMatrix& dphi_at_tying_point) -> const LocalMatrix&
    {
        const auto& elementary_data = GetElementaryData();

        auto& covariant_basis_at_tying_pt =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_basis_at_tying_pt)>();
        covariant_basis_at_tying_pt.fill(0.);

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        const auto Nshape_function = dphi_at_tying_point.shape(0);
        constexpr auto euclidean_dimension = 3;

        for (auto shape_fct_index = 0ul; shape_fct_index < Nshape_function; ++shape_fct_index)
        {
            const auto& coords_in_geom_elt = geom_elt.GetCoord(shape_fct_index);

            for (auto component_shape_function = 0ul; component_shape_function < euclidean_dimension;
                 ++component_shape_function)
            {
                for (auto coord_index = 0ul; coord_index < euclidean_dimension; ++coord_index)
                {
                    covariant_basis_at_tying_pt(coord_index, component_shape_function) +=
                        coords_in_geom_elt[coord_index]
                        * dphi_at_tying_point(shape_fct_index, component_shape_function);
                }
            }
        }

        return covariant_basis_at_tying_pt;
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    auto NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::ComputeDisplacementGradientAtTyingPoint(
        const LocalMatrix& dphi_at_tying_point) -> const LocalMatrix&
    {
        auto& displacement_gradient = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::displacement_gradient_at_tying_pt)>();
        displacement_gradient.fill(0.);

        auto& local_displacement = GetFormerLocalDisplacement();

        constexpr auto euclidean_dimension = 3ul;

        const auto Nshape_function = dphi_at_tying_point.shape(0);

        for (auto shape_fct_index = 0ul; shape_fct_index < Nshape_function; ++shape_fct_index)
        {
            for (auto component_shape_function = 0ul; component_shape_function < euclidean_dimension;
                 ++component_shape_function)
            {
                for (auto coord_index = 0ul; coord_index < euclidean_dimension; ++coord_index)
                {
                    const auto local_displacement_index = Nshape_function * coord_index + shape_fct_index;
                    assert(local_displacement_index < local_displacement.size());

                    displacement_gradient(coord_index, component_shape_function) +=
                        local_displacement[local_displacement_index]
                        * dphi_at_tying_point(shape_fct_index, component_shape_function);
                }
            }
        }

        return displacement_gradient;
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    void NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::PrepareInternalDataFromTyingPtToQuadPt(
        const QuadraturePoint& quad_pt)
    {
        const auto& covariant_metric_tensor =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::covariant_metric_tensor)>();

        auto& cauchy_green_as_vector =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::cauchy_green_as_vector)>();
        assert(cauchy_green_as_vector.shape(0) == 6);
        cauchy_green_as_vector.fill(0.);

        auto& De_dot_test_grad =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::De_dot_test_grad)>();
        auto& transposed_De_dot_test_grad =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_De_dot_test_grad)>();

        De_dot_test_grad.fill(0.);
        transposed_De_dot_test_grad.fill(0.);

        const double C_11 = UpdateDiagonalStrainsFromTyingPt(quad_pt, tying_pt_interpolation_component::e_rr);
        const double C_22 = UpdateDiagonalStrainsFromTyingPt(quad_pt, tying_pt_interpolation_component::e_ss);
        const double C_33 = UpdateDiagonalStrainsFromTyingPt(quad_pt, tying_pt_interpolation_component::e_zz);
        const double C_12 = UpdateExtraDiagonalStrainsFromTyingPt(quad_pt, tying_pt_interpolation_component::e_rs);
        const double C_23 = UpdateExtraDiagonalStrainsFromTyingPt(quad_pt, tying_pt_interpolation_component::e_sz);
        const double C_13 = UpdateExtraDiagonalStrainsFromTyingPt(quad_pt, tying_pt_interpolation_component::e_rz);

        cauchy_green_as_vector(0, 0) = C_11 + covariant_metric_tensor(0, 0);
        cauchy_green_as_vector(1, 0) = C_22 + covariant_metric_tensor(1, 1);
        cauchy_green_as_vector(2, 0) = C_33 + covariant_metric_tensor(2, 2);
        cauchy_green_as_vector(3, 0) = C_12 + covariant_metric_tensor(0, 1);
        cauchy_green_as_vector(4, 0) = C_23 + covariant_metric_tensor(1, 2);
        cauchy_green_as_vector(5, 0) = C_13 + covariant_metric_tensor(0, 2);
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    void NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::ComputeWDerivates(
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt,
        const Advanced::RefFEltInLocalOperator& unknown_ref_felt)
    {
        auto& d2W = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::d2W)>();
        auto& dW = this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::dW)>();

        const auto& matricial_cauchy_green_tensor =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::cauchy_green_as_vector)>();

        decltype(auto) generalized_input_args = GetGeneralizedInputArguments();

        const auto& contravariant_basis =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::contravariant_basis)>();

        dW.fill(0.);
        d2W.fill(0.);

        const auto& hyperelasticity_ptr = static_cast<HyperelasticityPolicyT*>(this);
        auto& hyperelasticity = *hyperelasticity_ptr;

        hyperelasticity.ComputeWDerivates(quad_pt,
                                          geom_elt,
                                          unknown_ref_felt,
                                          matricial_cauchy_green_tensor,
                                          generalized_input_args,
                                          contravariant_basis,
                                          dW,
                                          d2W);

        auto& extended_dW =
            this->vector_parent::template GetLocalVector<EnumUnderlyingType(LocalVectorIndex::extended_dW)>();

        // We want the same order as the (i, j) loop for the grad-grad contribution for extended_dW.
        // Diagonal terms.
        for (auto i = 0u; i < 3; ++i)
            extended_dW(3 * i + i) = dW(i);

        // Extradiagonal terms.
        extended_dW(1) = extended_dW(3) = dW(3);
        extended_dW(2) = extended_dW(6) = dW(5);
        extended_dW(5) = extended_dW(7) = dW(4);
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    double NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::ComputeCauchyGreenAtTyingPointHelper(
        const LocalMatrix& covariant_basis,
        const LocalMatrix& displacement_gradient,
        const std::pair<tying_pt_interpolation_component, tying_pt_interpolation_component>& component_pair)
    {
        double dot_products = 0.;
        constexpr auto euclidean_dimension = 3;

        const auto i = EnumUnderlyingType(component_pair.first);
        const auto j = EnumUnderlyingType(component_pair.second);

        for (auto k = 0u; k < euclidean_dimension; ++k)
        {
            dot_products += covariant_basis(k, i) * displacement_gradient(k, j)
                            + covariant_basis(k, j) * displacement_gradient(k, i)
                            + displacement_gradient(k, i) * displacement_gradient(k, j);
        }

        return dot_products;
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    void NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::InitGradGradArray()
    {
        const auto& elementary_data = GetElementaryData();
        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            decltype(auto) quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            auto get_grad_grad = [this, &quad_pt](tying_pt_interpolation_component component) -> const LocalMatrix&
            {
                return this->GetTyingPointDataForComponent(quad_pt, component).GetGradGradProduct();
            };

            grad_grad_list_.push_back({ { get_grad_grad(tying_pt_interpolation_component::e_rr),
                                          get_grad_grad(tying_pt_interpolation_component::e_rs),
                                          get_grad_grad(tying_pt_interpolation_component::e_rz),
                                          get_grad_grad(tying_pt_interpolation_component::e_sr),
                                          get_grad_grad(tying_pt_interpolation_component::e_ss),
                                          get_grad_grad(tying_pt_interpolation_component::e_sz),
                                          get_grad_grad(tying_pt_interpolation_component::e_zr),
                                          get_grad_grad(tying_pt_interpolation_component::e_zs),
                                          get_grad_grad(tying_pt_interpolation_component::e_zz) } });

        } // Loop on quad pts.
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    double NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::UpdateDiagonalStrainsFromTyingPt(
        const QuadraturePoint& quad_pt,
        tying_pt_interpolation_component component)
    {
        double ret = 0.;
        auto& De_dot_test_grad =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::De_dot_test_grad)>();
        auto& transposed_De_dot_test_grad =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_De_dot_test_grad)>();

        decltype(auto) mitc_data = TyingPointsPolicyT::GetTyingPointDataForComponent(quad_pt, component);

        constexpr auto dimension = 3ul;

        const auto tensor_component = EnumUnderlyingType(component);

        const auto component_pair = std::make_pair(component, component);

        decltype(auto) tying_point_list = mitc_data.GetTyingPointList();

        for (const auto& tying_point_ptr : tying_point_list)
        {
            assert(!(!tying_point_ptr));
            const auto& tying_point = *tying_point_ptr;

            decltype(auto) dphi_geo = tying_point.GetGeometricGradient();
            decltype(auto) dphi_felt = tying_point.GetFEltGradient();

            const auto& displacement_gradient_at_tying_pt = ComputeDisplacementGradientAtTyingPoint(dphi_felt);
            const auto& covariant_basis_at_tying_pt = ComputeCovariantBasisAtTyingPoint(dphi_geo);

            const auto non_geometric_part = ComputeCauchyGreenAtTyingPointHelper(
                covariant_basis_at_tying_pt, displacement_gradient_at_tying_pt, component_pair);

            const auto tying_pt_shape_function_value = tying_point.GetShapeFunctionValue();

            ret += tying_pt_shape_function_value * non_geometric_part;

            decltype(auto) dphi_test_felt = tying_point.GetTestFEltGradient();
            const auto Nnode_test = dphi_test_felt.shape(0);
            const auto Nnode = dphi_felt.shape(0);

            for (auto comp = 0ul; comp < dimension; ++comp)
            {
                for (auto row_node = 0ul; row_node < Nnode; ++row_node)
                {
                    De_dot_test_grad(tensor_component, row_node + comp * Nnode) +=
                        tying_pt_shape_function_value
                        * (displacement_gradient_at_tying_pt(comp, tensor_component)
                           + covariant_basis_at_tying_pt(comp, tensor_component))
                        * dphi_felt(row_node, tensor_component);
                }

                for (auto row_node = 0ul; row_node < Nnode_test; ++row_node)
                {
                    transposed_De_dot_test_grad(row_node + comp * Nnode_test, tensor_component) +=
                        tying_pt_shape_function_value
                        * (displacement_gradient_at_tying_pt(comp, tensor_component)
                           + covariant_basis_at_tying_pt(comp, tensor_component))
                        * dphi_test_felt(row_node, tensor_component);
                }
            }
        }

        return ret;
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    double NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::UpdateExtraDiagonalStrainsFromTyingPt(
        const QuadraturePoint& quad_pt,
        tying_pt_interpolation_component component)
    {
        decltype(auto) mitc_data = TyingPointsPolicyT::GetTyingPointDataForComponent(quad_pt, component);

        double ret = 0.;
        auto& De_dot_test_grad =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::De_dot_test_grad)>();
        auto& transposed_De_dot_test_grad =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_De_dot_test_grad)>();

        const auto component_index = EnumUnderlyingType(component);

        const auto tensor_component_pair = ComputePair(component);

        const auto first_component = EnumUnderlyingType(tensor_component_pair.first);
        const auto second_component = EnumUnderlyingType(tensor_component_pair.second);

        constexpr auto dimension = 3ul;

        decltype(auto) tying_point_list = mitc_data.GetTyingPointList();

        for (const auto& tying_point_ptr : tying_point_list)
        {
            assert(!(!tying_point_ptr));
            const auto& tying_point = *tying_point_ptr;

            decltype(auto) dphi_geo = tying_point.GetGeometricGradient();
            decltype(auto) dphi_felt = tying_point.GetFEltGradient();
            decltype(auto) dphi_test_felt = tying_point.GetTestFEltGradient();

            const auto& displacement_gradient_at_tying_pt = ComputeDisplacementGradientAtTyingPoint(dphi_felt);
            const auto& covariant_basis_at_tying_pt = ComputeCovariantBasisAtTyingPoint(dphi_geo);

            const auto non_geometric_part = ComputeCauchyGreenAtTyingPointHelper(
                covariant_basis_at_tying_pt, displacement_gradient_at_tying_pt, tensor_component_pair);

            const auto tying_pt_shape_function_value = tying_point.GetShapeFunctionValue();
            ret += tying_pt_shape_function_value * non_geometric_part;
            const auto Nnode_test = dphi_test_felt.shape(0);
            const auto Nnode = dphi_felt.shape(0);

            for (auto comp = 0ul; comp < dimension; ++comp)
            {
                for (auto row_node = 0ul; row_node < Nnode; ++row_node)
                {
                    De_dot_test_grad(component_index, row_node + comp * Nnode) +=
                        tying_pt_shape_function_value
                        * ((displacement_gradient_at_tying_pt(comp, first_component)
                            + covariant_basis_at_tying_pt(comp, first_component))
                               * dphi_felt(row_node, second_component)
                           + (displacement_gradient_at_tying_pt(comp, second_component)
                              + covariant_basis_at_tying_pt(comp, second_component))
                                 * dphi_felt(row_node, first_component));
                }

                for (auto row_node = 0ul; row_node < Nnode_test; ++row_node)
                {
                    transposed_De_dot_test_grad(row_node + comp * Nnode_test, component_index) +=
                        tying_pt_shape_function_value
                        * ((displacement_gradient_at_tying_pt(comp, first_component)
                            + covariant_basis_at_tying_pt(comp, first_component))
                               * dphi_test_felt(row_node, second_component)
                           + (displacement_gradient_at_tying_pt(comp, second_component)
                              + covariant_basis_at_tying_pt(comp, second_component))
                                 * dphi_test_felt(row_node, first_component));
                }
            }
        }

        return ret;
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    inline const std::vector<double>&
    NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::GetFormerLocalDisplacement() const noexcept
    {
        return former_local_displacement_;
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    inline std::vector<double>&
    NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::GetNonCstFormerLocalDisplacement() noexcept
    {
        return const_cast<std::vector<double>&>(GetFormerLocalDisplacement());
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    inline const std::array<LocalMatrix, 9>&
    NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::GetGradGradProductAtQuadPt(
        const std::size_t quad_pt_index) const noexcept
    {
        assert(quad_pt_index < grad_grad_list_.size());
        return grad_grad_list_[quad_pt_index];
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    const InvariantNS::GeneralizedNS::Input&
    NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::GetGeneralizedInputArguments() const noexcept
    {
        return generalized_input_;
    }


    template<class HyperelasticityPolicyT, class TyingPointsPolicyT>
    InvariantNS::GeneralizedNS::Input&
    NonlinearShell<HyperelasticityPolicyT, TyingPointsPolicyT>::GetNonCstGeneralizedInputArguments() noexcept
    {
        return const_cast<InvariantNS::GeneralizedNS::Input&>(GetGeneralizedInputArguments());
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_NONLINEARSHELL_DOT_HXX_
// *** MoReFEM end header guards *** < //
