// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNAL_PARTIALSPECIALIZATION_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNAL_PARTIALSPECIALIZATION_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/PartialSpecialization.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/PartialSpecialization.hpp"


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
{


    template<std::size_t DimensionT, class HyperelasticityPolicyT>
    void ComputeWDerivatesHyperelasticity<DimensionT, HyperelasticityPolicyT>::Perform(
        const QuadraturePoint& quad_pt,
        const GeometricElt& geom_elt,
        const Advanced::RefFEltInLocalOperator& ref_felt,
        const LocalVector& cauchy_green_tensor_value,
        HyperelasticityPolicyT& hyperelasticity,
        LocalVector& dW,
        LocalMatrix& d2W)
    {
        hyperelasticity.ComputeWDerivates(quad_pt, geom_elt, ref_felt, cauchy_green_tensor_value, dW, d2W);
    }


    template<std::size_t DimensionT>
    void
    ComputeWDerivatesHyperelasticity<DimensionT, SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS::None>::
        Perform(const QuadraturePoint& quad_pt,
                const GeometricElt& geom_elt,
                const Advanced::RefFEltInLocalOperator&,
                const LocalVector& cauchy_green_tensor_value,
                SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS::None& hyperelasticity,
                LocalVector& dW,
                LocalMatrix& d2W)
    {
        static_cast<void>(quad_pt);
        static_cast<void>(geom_elt);
        static_cast<void>(cauchy_green_tensor_value);
        static_cast<void>(hyperelasticity);
        static_cast<void>(dW);
        static_cast<void>(d2W);
    }


    template<std::size_t DimensionT, class InternalVariablePolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void ComputeWDerivatesInternalVariable<DimensionT, InternalVariablePolicyT, TimeManagerT>::Perform(
        const ::MoReFEM::Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS ::ForUnknownList&
            quad_pt_unknown_data,
        const GeometricElt& geom_elt,
        const Advanced::RefFEltInLocalOperator& ref_felt,
        const LocalVector& cauchy_green_tensor_value,
        const LocalMatrix& transposed_De,
        InternalVariablePolicyT& internal_variable,
        LocalVector& dW,
        LocalMatrix& d2W)
    {
        internal_variable.template ComputeWDerivates<DimensionT>(
            quad_pt_unknown_data, geom_elt, ref_felt, cauchy_green_tensor_value, transposed_De, dW, d2W);
    }


    template<std::size_t DimensionT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void
    ComputeWDerivatesInternalVariable<DimensionT,
                                      SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::None<TimeManagerT>,
                                      TimeManagerT>::
        Perform(const ::MoReFEM::Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS ::ForUnknownList&
                    quad_pt_unknown_data,
                const GeometricElt& geom_elt,
                const Advanced::RefFEltInLocalOperator& ref_felt,
                const LocalVector& cauchy_green_tensor_value,
                const LocalMatrix& transposed_De,
                SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::None<TimeManagerT>& internal_variable,
                LocalVector& dW,
                LocalMatrix& d2W)
    {
        static_cast<void>(quad_pt_unknown_data);
        static_cast<void>(geom_elt);
        static_cast<void>(ref_felt);
        static_cast<void>(cauchy_green_tensor_value);
        static_cast<void>(transposed_De);
        static_cast<void>(internal_variable);
        static_cast<void>(dW);
        static_cast<void>(d2W);
    }


    template<class InternalVariablePolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void CorrectRHSWithActiveSchurComplement<InternalVariablePolicyT, TimeManagerT>::Perform(
        InternalVariablePolicyT& internal_variable,
        LocalVector& rhs)
    {
        internal_variable.CorrectRHSWithActiveSchurComplement(rhs);
    }


    template<std::size_t FiberIndexT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline void CorrectRHSWithActiveSchurComplement<
        SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::AnalyticalPrestress<FiberIndexT, TimeManagerT>,
        TimeManagerT>::Perform(SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS ::
                                   AnalyticalPrestress<FiberIndexT, TimeManagerT>& internal_variable,
                               LocalVector& rhs)
    {
        static_cast<void>(rhs);
        static_cast<void>(internal_variable);
    }


    template<std::size_t FiberIndexI4T, std::size_t FiberIndexI6T, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline void CorrectRHSWithActiveSchurComplement<
        SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::
            Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>,
        TimeManagerT>::Perform(SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS ::
                                   Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>& internal_variable,
                               LocalVector& rhs)
    {
        static_cast<void>(rhs);
        static_cast<void>(internal_variable);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline void CorrectRHSWithActiveSchurComplement<
        SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::None<TimeManagerT>,
        TimeManagerT>::Perform(SecondPiolaKirchhoffStressTensorNS::InternalVariablePolicyNS::None<TimeManagerT>&
                                   internal_variable,
                               LocalVector& rhs)
    {
        static_cast<void>(rhs);
        static_cast<void>(internal_variable);
    }


    template<std::size_t DimensionT, class ViscoelasticityPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void ComputeWDerivatesViscoelasticity<DimensionT, ViscoelasticityPolicyT, TimeManagerT>::Perform(
        const ::MoReFEM::Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS ::ForUnknownList&
            quad_pt_unknown_data,
        const GeometricElt& geom_elt,
        const Advanced::RefFEltInLocalOperator& ref_felt,
        const LocalMatrix& De,
        const LocalMatrix& transposed_De,
        ViscoelasticityPolicyT& viscoelasticity,
        LocalVector& dW,
        LocalMatrix& d2W)
    {
        viscoelasticity.template ComputeWDerivates<DimensionT>(
            quad_pt_unknown_data, geom_elt, ref_felt, De, transposed_De, dW, d2W);
    }


    template<std::size_t DimensionT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void
    ComputeWDerivatesViscoelasticity<DimensionT,
                                     SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None<TimeManagerT>,
                                     TimeManagerT>::
        Perform(const ::MoReFEM::Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS ::ForUnknownList&
                    quad_pt_unknown_data,
                const GeometricElt& geom_elt,
                const Advanced::RefFEltInLocalOperator&,
                const LocalMatrix& De,
                const LocalMatrix& transposed_De,
                SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None<TimeManagerT>& viscoelasticity,
                LocalVector& dW,
                LocalMatrix& d2W)
    {
        static_cast<void>(quad_pt_unknown_data);
        static_cast<void>(geom_elt);
        static_cast<void>(viscoelasticity);
        static_cast<void>(dW);
        static_cast<void>(d2W);
        static_cast<void>(De);
        static_cast<void>(transposed_De);
    }


    template<class ViscoelasticityPolicyT>
    void AddTangentMatrixViscoelasticity<ViscoelasticityPolicyT>::Perform(LocalMatrix& tangent_matrix,
                                                                          ViscoelasticityPolicyT& viscoelasticity)
    {
        const auto& tangent_matrix_visco = viscoelasticity.GetMatrixTangentVisco();

        tangent_matrix += tangent_matrix_visco;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline void
    AddTangentMatrixViscoelasticity<SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS::None<TimeManagerT>>::
        Perform(LocalMatrix& tangent_matrix, ViscoelasticityPolicyNS::None<TimeManagerT>& viscoelasticity)
    {
        static_cast<void>(tangent_matrix);
        static_cast<void>(viscoelasticity);
    }


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNAL_PARTIALSPECIALIZATION_DOT_HXX_
// *** MoReFEM end header guards *** < //
