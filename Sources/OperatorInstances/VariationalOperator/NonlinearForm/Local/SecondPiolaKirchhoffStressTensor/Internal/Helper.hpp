// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNAL_HELPER_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNAL_HELPER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/LinearAlgebra/LocalAlias.hpp"

#include "Core/Enum.hpp" // IWYU pragma: export

#include "Operators/LocalVariationalOperator/ElementaryData.hpp" // IWYU pragma: export


namespace MoReFEM::Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
{

    /*!
     * \brief Compute the non linear contribution to the tangent matrix.
     *
     * \tparam MeshDimensionT Dimension of the mesh.
     *
     * \param[in] dW First derivates of W.
     * \param[in,out] out Tangent matrix, to which non linear contribution is added.
     */
    template<std::size_t MeshDimensionT>
    void ComputeNonLinearPart(const LocalVector& dW, LocalMatrix& out);


    /*!
     * \brief Compute the linear contribution to the tangent matrix.
     *
     * \copydoc doxygen_hide_de_and_transpose_arg
     *
     * \param[in] d2W Second derivates of W.
     * \param[in,out] linear_part_intermediate_matrix Matrix used in the computation. It is already
     * allocated; we frankly don't care the values in it in the input or in the output of the function. A
     * better design would make that a private member of a dedicated struct, but I do not have the time to
     * implement this very minor change. \param[in,out] linear_part Linear contribution to the tangent
     * matrix.
     */
    void ComputeLinearPart(const LocalMatrix& De,
                           const LocalMatrix& transposed_De,
                           const LocalMatrix& d2W,
                           LocalMatrix& linear_part_intermediate_matrix,
                           LocalMatrix& linear_part);


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNAL_HELPER_DOT_HPP_
// *** MoReFEM end header guards *** < //
