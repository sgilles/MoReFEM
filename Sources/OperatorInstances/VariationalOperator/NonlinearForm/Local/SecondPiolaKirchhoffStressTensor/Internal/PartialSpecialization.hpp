// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNAL_PARTIALSPECIALIZATION_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNAL_PARTIALSPECIALIZATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/None.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/AnalyticalPrestress.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/Microsphere.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/InternalVariablePolicy/None.hpp"
#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/None.hpp"

namespace MoReFEM::Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS
{

    //! \copydoc doxygen_hide_namespace_cluttering
    namespace AdvancedCounterpart = ::MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS;

    //! \copydoc doxygen_hide_namespace_cluttering
    namespace HyperelasticityPolicyNS = AdvancedCounterpart::HyperelasticityPolicyNS;

    //! \copydoc doxygen_hide_namespace_cluttering
    namespace InternalVariablePolicyNS = AdvancedCounterpart::InternalVariablePolicyNS;

    //! \copydoc doxygen_hide_namespace_cluttering
    namespace ViscoelasticityPolicyNS = AdvancedCounterpart::ViscoelasticityPolicyNS;


    /*!
     * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::ComputeWDerivates() if the
     * HyperelasticityPolicyT is not HyperelasticityPolicyNS::None.
     *
     */
    template<std::size_t DimensionT, class HyperelasticityPolicyT>
    struct ComputeWDerivatesHyperelasticity
    {


        /*!
         * \copydoc doxygen_hide_second_piola_compute_W_deriv_hyperelasticity
         *
         * \param[in,out] hyperelasticity Object which contains relevant data about hyperelasticity.
         */
        static void Perform(const QuadraturePoint& quad_pt,
                            const GeometricElt& geom_elt,
                            const Advanced::RefFEltInLocalOperator& ref_felt,
                            const LocalVector& cauchy_green_tensor_value,
                            HyperelasticityPolicyT& hyperelasticity,
                            LocalVector& dW,
                            LocalMatrix& d2W);
    };


    /*!
     * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::ComputeWDerivates() if the
     * HyperelasticityPolicyT is HyperelasticityPolicyNS::None.
     *
     * \internal <b><tt>[internal]</tt></b> Using this struct allows not to define a full-fledged
     * HyperelasticityPolicyNS::None policy in which all the methods would be asked to do nothing.
     * \endinternal
     *
     */
    template<std::size_t DimensionT>
    struct ComputeWDerivatesHyperelasticity<DimensionT, HyperelasticityPolicyNS::None>
    {


        //! Do nothing: specialization for the case hyperelastic policy is None.
        static void Perform(const QuadraturePoint&,
                            const GeometricElt&,
                            const Advanced::RefFEltInLocalOperator&,
                            const LocalVector&,
                            HyperelasticityPolicyNS::None&,
                            LocalVector&,
                            LocalMatrix&);
    };


    /*!
     * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::ComputeWDerivates() if the
     * InternalVariablePolicyT is not InternalVariablePolicyT::None.
     *
     */
    template<std::size_t DimensionT, class InternalVariablePolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    struct ComputeWDerivatesInternalVariable
    {


        /*!
         * \copydoc doxygen_hide_second_piola_compute_W_derivates_internal_variable
         *
         * \copydoc doxygen_hide_dW_d2W_derivates_arg
         *
         * \param[in,out] internal_variable Object which contains relevant data about active stress.
         */
        static void Perform(const ::MoReFEM::Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList&
                                quad_pt_unknown_data,
                            const GeometricElt& geom_elt,
                            const Advanced::RefFEltInLocalOperator& ref_felt,
                            const LocalVector& cauchy_green_tensor_value,
                            const LocalMatrix& transposed_De,
                            InternalVariablePolicyT& internal_variable,
                            LocalVector& dW,
                            LocalMatrix& d2W);
    };


    /*!
     * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::ComputeWDerivates() if the
     * InternalVariablePolicyT is InternalVariablePolicyT::None.
     *
     * \internal <b><tt>[internal]</tt></b> Using this struct allows not to define a full-fledged
     * InternalVariablePolicyT::None policy in which all the methods would be asked to do nothing.
     * \endinternal
     *
     */
    template<std::size_t DimensionT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    struct ComputeWDerivatesInternalVariable<DimensionT, InternalVariablePolicyNS::None<TimeManagerT>, TimeManagerT>
    {


        //! Do nothing: specialization for the case active stress policy is None.
        static void Perform(const ::MoReFEM::Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList&,
                            const GeometricElt&,
                            const Advanced::RefFEltInLocalOperator&,
                            const LocalVector&,
                            const LocalMatrix&,
                            InternalVariablePolicyNS::None<TimeManagerT>&,
                            LocalVector&,
                            LocalMatrix&);
    };


    /*!
     * \brief Helper struct used to call
     * SecondPiolaKirchhoffStressTensor::CorrectRHSWithActiveSchurComplement() if the
     * InternalVariablePolicyT is not InternalVariablePolicyT::None.
     *
     */
    template<class InternalVariablePolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    struct CorrectRHSWithActiveSchurComplement
    {


        /*!
         * \brief Add active schur complement to RHS.
         *
         * \param[in,out] internal_variable Object that manages internal variable data.
         * \param[in,out] rhs Rhs being modified.
         */
        static void Perform(InternalVariablePolicyT& internal_variable, LocalVector& rhs);
    };


    /*!
     * \brief Helper struct used to call
     * SecondPiolaKirchhoffStressTensor::CorrectRHSWithActiveSchurComplement() if the
     * InternalVariablePolicyT is InternalVariablePolicyT::AnalyticalPrestress.
     *
     * \internal <b><tt>[internal]</tt></b> Using this struct allows not to define the method for
     *  InternalVariablePolicyT::AnalyticalPrestress policy.
     * \endinternal
     *
     */
    template<std::size_t FiberIndexT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    struct CorrectRHSWithActiveSchurComplement<InternalVariablePolicyNS::AnalyticalPrestress<FiberIndexT, TimeManagerT>,
                                               TimeManagerT>
    {


        /*!
         * \brief Add active schur complement to RHS.
         *
         * \param[in,out] internal_variable Object that manages internal variable data.
         * \param[in,out] rhs Rhs being modified.
         */
        static void Perform(InternalVariablePolicyNS::AnalyticalPrestress<FiberIndexT, TimeManagerT>& internal_variable,
                            LocalVector& rhs);
    };


    /*!
     * \brief Helper struct used to call
     * SecondPiolaKirchhoffStressTensor::CorrectRHSWithActiveSchurComplement() if the
     * InternalVariablePolicyT is InternalVariablePolicyT::Microsphere.
     *
     * \internal <b><tt>[internal]</tt></b> Using this struct allows not to define the method for
     *  InternalVariablePolicyT::Microsphere policy.
     *
     */
    template<std::size_t FiberIndexI4T, std::size_t FiberIndexI6T, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    struct CorrectRHSWithActiveSchurComplement<
        InternalVariablePolicyNS::Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>,
        TimeManagerT>
    {


        /*!
         * \brief Gets rid of the active schur complement to RHS (explicit formulation is used instead).
         *
         * \param[in,out] internal_variable Object that manages internal variable data.
         * \param[in,out] rhs Rhs being modified.
         */
        static void
        Perform(InternalVariablePolicyNS::Microsphere<FiberIndexI4T, FiberIndexI6T, TimeManagerT>& internal_variable,
                LocalVector& rhs);
    };


    /*!
     * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::ComputeWDerivates() if the
     * InternalVariablePolicyT is InternalVariablePolicyT::None.
     *
     * \internal <b><tt>[internal]</tt></b> Using this struct allows not to define a full-fledged
     * InternalVariablePolicyT::None policy in which all the methods would be asked to do nothing.
     * \endinternal
     *
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    struct CorrectRHSWithActiveSchurComplement<InternalVariablePolicyNS::None<TimeManagerT>, TimeManagerT>
    {


        //! Do nothing: specialization for the case active stress policy is None.
        static void Perform(InternalVariablePolicyNS::None<TimeManagerT>&, LocalVector&);
    };


    /*!
     * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::ComputeWDerivates() if the
     * ViscoelasticityPolicyT is not ViscoelasticityPolicyT::None.
     *
     */
    template<std::size_t DimensionT, class ViscoelasticityPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    struct ComputeWDerivatesViscoelasticity
    {


        //! \copydoc doxygen_hide_namespace_cluttering
        using InformationAtQuadraturePoint =
            ::MoReFEM::Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint;

        /*!
         * \copydoc doxygen_hide_second_piola_compute_W_derivates_visco_elasticity
         *
         * \param[in,out] viscoelasticity Object which contains relevant data about visco-elasticity.
         *
         * \copydoc doxygen_hide_dW_d2W_derivates_arg
         */
        static void Perform(const ::MoReFEM::Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList&
                                quad_pt_unknown_data,
                            const GeometricElt& geom_elt,
                            const Advanced::RefFEltInLocalOperator& ref_felt,
                            const LocalMatrix& De,
                            const LocalMatrix& transposed_De,
                            ViscoelasticityPolicyT& viscoelasticity,
                            LocalVector& dW,
                            LocalMatrix& d2W);
    };


    /*!
     * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::ComputeWDerivates() if the
     * ViscoelasticityPolicyT is ViscoelasticityPolicyT::None.
     *
     * \internal <b><tt>[internal]</tt></b> Using this struct allows not to define a full-fledged
     * ViscoelasticityPolicyT::None policy in which all the methods would be asked to do nothing.
     * \endinternal
     *
     */
    template<std::size_t DimensionT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    struct ComputeWDerivatesViscoelasticity<DimensionT, ViscoelasticityPolicyNS::None<TimeManagerT>, TimeManagerT>
    {


        //! \copydoc doxygen_hide_namespace_cluttering
        using InformationAtQuadraturePoint =
            ::MoReFEM::Advanced::LocalVariationalOperatorNS::InformationAtQuadraturePoint;


        //! Do nothing: specialization for the case visco-elasticity policy is None.
        static void Perform(const ::MoReFEM::Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS::ForUnknownList&,
                            const GeometricElt&,
                            const Advanced::RefFEltInLocalOperator&,
                            const LocalMatrix&,
                            const LocalMatrix&,
                            ViscoelasticityPolicyNS::None<TimeManagerT>&,
                            LocalVector&,
                            LocalMatrix&);
    };


    /*!
     * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::AddTangentMatrixViscoelasticity()
     * if the ViscoelasticityPolicyT is not ViscoelasticityPolicyT::None.
     *
     */
    template<class ViscoelasticityPolicyT>
    struct AddTangentMatrixViscoelasticity
    {


        /*!
         * \brief Add contribution of the visco-elasticity to the tangent matrix.
         *
         * \param[in,out] tangent_matrix Tangent matrix to which visco-elasticity contribution is to be
         * added. \param[in,out] viscoelasticity Object which handles visco-elasticity computations. Its
         * ComputeWDerivates() method will be called.
         */
        static void Perform(LocalMatrix& tangent_matrix, ViscoelasticityPolicyT& viscoelasticity);
    };


    /*!
     * \brief Helper struct used to call SecondPiolaKirchhoffStressTensor::AddTangentMatrixViscoelasticity()
     * if the ViscoelasticityPolicyT is ViscoelasticityPolicyT::None.
     *
     * \internal <b><tt>[internal]</tt></b> Using this struct allows not to define a full-fledged
     * ViscoelasticityPolicyT::None policy in which all the methods would be asked to do nothing.
     * \endinternal
     *
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    struct AddTangentMatrixViscoelasticity<ViscoelasticityPolicyNS::None<TimeManagerT>>
    {


        //! Specialization for the ViscoelasticityPolicyNS::None case; do absolutely nothing.
        static void Perform(LocalMatrix&, ViscoelasticityPolicyNS::None<TimeManagerT>&);
    };


} // namespace MoReFEM::Internal::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/Internal/PartialSpecialization.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_INTERNAL_PARTIALSPECIALIZATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
