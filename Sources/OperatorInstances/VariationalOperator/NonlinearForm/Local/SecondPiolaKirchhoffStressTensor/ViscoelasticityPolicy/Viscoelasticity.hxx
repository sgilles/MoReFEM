// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_VISCOELASTICITYPOLICY_VISCOELASTICITY_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_VISCOELASTICITYPOLICY_VISCOELASTICITY_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/Viscoelasticity.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/ViscoelasticityPolicy/Viscoelasticity.hpp"


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::Viscoelasticity(const std::size_t mesh_dimension,
                                                                            const std::size_t Ndof_unknown,
                                                                            const std::size_t Ncomponent,
                                                                            const Solid<TimeManagerT>& solid,
                                                                            const TimeManagerT& time_manager)
    : matrix_parent(), vector_parent(), time_manager_(time_manager), viscosity_(solid.GetViscosity())
    {
        std::pair<std::size_t, std::size_t> identity_size;
        const std::size_t square_Ncomponent = NumericNS::Square(Ncomponent);

        switch (mesh_dimension)
        {
        case 2u:
            identity_size = { 3, 3 };
            break;
        case 3u:
            identity_size = { 6, 6 };
            break;
        default:
            assert(false);
            break;
        }

        matrix_parent::InitLocalMatrixStorage(
            { { identity_size, { square_Ncomponent, square_Ncomponent }, { mesh_dimension, mesh_dimension } } });


        this->vector_parent::InitLocalVectorStorage({ { mesh_dimension * mesh_dimension } });

        auto& d2W_visco = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::d2W_visco)>();
        d2W_visco = xt::eye(d2W_visco.shape(0), 0); // identity matrix

        matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex ::tangent_matrix_visco)>().fill(0.);

        local_velocity_.resize(Ndof_unknown);

        deriv_eta_ =
            std::make_unique<Advanced::LocalVariationalOperatorNS ::DerivativeGreenLagrange<GreenLagrangeOrEta::eta>>(
                mesh_dimension);
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    void Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::MatrixToVector(const LocalMatrix& gradient_matrix,
                                                                                LocalVector& vector)
    {
        const auto size = gradient_matrix.shape(0);
        assert(size == gradient_matrix.shape(1) && "Implemented for square matrix");

        for (auto i = 0ul; i < size; ++i)
        {
            for (auto j = 0ul; j < size; ++j)
            {
                vector(i * size + j) = gradient_matrix(i, j);
            }
        }
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    template<std::size_t DimensionT>
    void Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::ComputeWDerivates(
        const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS ::ForUnknownList& quad_pt_unknown_data,
        const GeometricElt& geom_elt,
        const Advanced::RefFEltInLocalOperator& ref_felt,
        const LocalMatrix& De,
        const LocalMatrix& transposed_De,
        LocalVector& dW,
        LocalMatrix& d2W)
    {
        const auto run_case = GetTimeManager().GetStaticOrDynamic();

        if (run_case == StaticOrDynamic::dynamic_)
        {
            if constexpr (DerivatesWithRespectToT == DerivatesWithRespectTo::displacement_and_velocity)
            {
                double eta = GetViscosity().GetValue(quad_pt_unknown_data.GetQuadraturePoint(), geom_elt);
                double dt = GetTimeManager().GetTimeStep();

                const auto& local_velocity = GetLocalVelocity();

                auto& tangent_matrix_visco = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
                    LocalMatrixIndex::tangent_matrix_visco)>();
                tangent_matrix_visco.fill(0.);
                auto& d2W_visco =
                    matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::d2W_visco)>();
                auto& matrix_gradient_velocity =
                    matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::gradient_velocity)>();

                auto& vector_gradient_velocity = this->vector_parent::template GetLocalVector<EnumUnderlyingType(
                    LocalVectorIndex::velocity_gradient)>();

                matrix_gradient_velocity.fill(0.);
                Advanced::OperatorNS::ComputeGradientDisplacementMatrix(
                    quad_pt_unknown_data, ref_felt, local_velocity, matrix_gradient_velocity);

                MatrixToVector(matrix_gradient_velocity, vector_gradient_velocity);

                auto& derivative_eta = GetNonCstDerivativeEta();
                const auto& Deta = derivative_eta.Update(matrix_gradient_velocity);

                xt::noalias(dW) += eta * xt::linalg::dot(De, vector_gradient_velocity);
                xt::noalias(d2W) += 2. * eta / dt * d2W_visco;
                xt::noalias(tangent_matrix_visco) = eta * xt::linalg::dot(transposed_De, Deta);
            } else if constexpr (DerivatesWithRespectToT == DerivatesWithRespectTo::displacement)
            {
                double eta = GetViscosity().GetValue(quad_pt_unknown_data.GetQuadraturePoint(), geom_elt);
                const auto& local_velocity = GetLocalVelocity();

                auto& tangent_matrix_visco = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
                    LocalMatrixIndex::tangent_matrix_visco)>();
                tangent_matrix_visco.fill(0.);
                auto& matrix_gradient_velocity =
                    matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::gradient_velocity)>();

                auto& vector_gradient_velocity = this->vector_parent::template GetLocalVector<EnumUnderlyingType(
                    LocalVectorIndex::velocity_gradient)>();

                matrix_gradient_velocity.fill(0.);
                Advanced::OperatorNS::ComputeGradientDisplacementMatrix(
                    quad_pt_unknown_data, ref_felt, local_velocity, matrix_gradient_velocity);

                MatrixToVector(matrix_gradient_velocity, vector_gradient_velocity);

                auto& derivative_eta = GetNonCstDerivativeEta();
                const auto& Deta = derivative_eta.Update(matrix_gradient_velocity);

                xt::noalias(dW) += eta * xt::linalg::dot(De, vector_gradient_velocity);
                xt::noalias(tangent_matrix_visco) = eta * xt::linalg::dot(transposed_De, Deta);
            } else
            {
                static_assert(DerivatesWithRespectToT == DerivatesWithRespectTo::velocity);

                double eta = GetViscosity().GetValue(quad_pt_unknown_data.GetQuadraturePoint(), geom_elt);

                auto& tangent_matrix_visco = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
                    LocalMatrixIndex::tangent_matrix_visco)>();
                tangent_matrix_visco.fill(0.);
                auto& d2W_visco =
                    matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::d2W_visco)>();

                xt::noalias(d2W) += eta * d2W_visco;
            }
        }
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    inline const TimeManagerT& Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::GetTimeManager() const noexcept
    {
        return time_manager_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    inline const std::vector<double>&
    Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::GetLocalVelocity() const noexcept
    {
        return local_velocity_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    inline std::vector<double>&
    Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::GetNonCstLocalVelocity() noexcept
    {
        return const_cast<std::vector<double>&>(GetLocalVelocity());
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    inline auto Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::GetNonCstDerivativeEta() noexcept
        -> Advanced::LocalVariationalOperatorNS::DerivativeGreenLagrange<
            Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::GreenLagrangeOrEta::eta>&
    {
        assert(!(!deriv_eta_));
        return *deriv_eta_;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    inline const LocalMatrix&
    Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::GetMatrixTangentVisco() const noexcept
    {
        return this
            ->matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::tangent_matrix_visco)>();
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT, DerivatesWithRespectTo DerivatesWithRespectToT>
    inline auto Viscoelasticity<TimeManagerT, DerivatesWithRespectToT>::GetViscosity() const noexcept
        -> const scalar_parameter_type&
    {
        return viscosity_;
    }


} // namespace
  // MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::ViscoelasticityPolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_VISCOELASTICITYPOLICY_VISCOELASTICITY_DOT_HXX_
// *** MoReFEM end header guards *** < //
