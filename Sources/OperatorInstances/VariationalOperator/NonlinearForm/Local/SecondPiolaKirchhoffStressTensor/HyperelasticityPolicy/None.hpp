// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_HYPERELASTICITYPOLICY_NONE_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_HYPERELASTICITYPOLICY_NONE_DOT_HPP_
// *** MoReFEM header guards *** < //


// IWYU pragma: no_include <__nullptr>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <vector>

namespace MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS
{


    /*!
     * \brief Policy to use when there are no hyperelasticity involved in the
     * \a SecondPiolaKirchhoffStressTensor operator.
     */
    class None
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = None;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Alias to vector of unique pointers.
        using vector_unique_ptr = std::vector<unique_ptr>;

        //! Expected alias.
        using law_type = std::nullptr_t;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         */
        explicit None(std::size_t, const law_type*);

        //! Destructor.
        ~None() = default;

        //! \copydoc doxygen_hide_copy_constructor
        None(const None& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        None(None&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        None& operator=(const None& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        None& operator=(None&& rhs) = delete;

        ///@}
    };


} // namespace
  // MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_HYPERELASTICITYPOLICY_NONE_DOT_HPP_
// *** MoReFEM end header guards *** < //
