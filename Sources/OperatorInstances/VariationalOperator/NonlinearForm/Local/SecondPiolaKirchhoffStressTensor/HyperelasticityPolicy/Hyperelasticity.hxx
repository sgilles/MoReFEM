// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_HYPERELASTICITYPOLICY_HYPERELASTICITY_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_HYPERELASTICITYPOLICY_HYPERELASTICITY_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/Hyperelasticity.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/Local/SecondPiolaKirchhoffStressTensor/HyperelasticityPolicy/Hyperelasticity.hpp"


#include <cstddef> // IWYU pragma: keep

#include "OperatorInstances/HyperelasticLaws/CauchyAndInvariant/Enum.hpp" // IWYU pragma: export


namespace MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS
{


    template<class HyperelasticLawT>
    Hyperelasticity<HyperelasticLawT>::Hyperelasticity(const std::size_t mesh_dimension,
                                                       const HyperelasticLawT* const hyperelastic_law)
    : hyperelastic_law_(hyperelastic_law)
    {
        switch (mesh_dimension)
        {
        case 1u:
            GetNonCstWorkMatrixOuterProduct().resize({ 1, 1 });
            break;
        case 2u:
            GetNonCstWorkMatrixOuterProduct().resize({ 3, 3 });
            break;
        case 3u:
            GetNonCstWorkMatrixOuterProduct().resize({ 6, 6 });
            break;
        default:
            assert(false);
            break;
        }
    }


    template<class HyperelasticLawT>
    void Hyperelasticity<HyperelasticLawT>::ComputeWDerivates(const QuadraturePoint& quad_pt,
                                                              const GeometricElt& geom_elt,
                                                              const Advanced::RefFEltInLocalOperator& ref_felt,
                                                              const LocalVector& cauchy_green_tensor_value,
                                                              LocalVector& dW,
                                                              LocalMatrix& d2W)
    {
        static_cast<void>(ref_felt);

        GetNonCstHyperelasticLaw().UpdateInvariants(cauchy_green_tensor_value, quad_pt, geom_elt);

        ComputeChainRule(quad_pt, geom_elt, dW, d2W);
    }


    template<class HyperelasticLawT>
    void
    Hyperelasticity<HyperelasticLawT>::ComputeWDerivates(const QuadraturePoint& quad_pt,
                                                         const GeometricElt& geom_elt,
                                                         const Advanced::RefFEltInLocalOperator& ref_felt,
                                                         const LocalMatrix& matricial_cauchy_green_tensor,
                                                         const InvariantNS::GeneralizedNS::Input& generalized_input,
                                                         const LocalMatrix& contravariant_basis,
                                                         LocalVector& dW,
                                                         LocalMatrix& d2W)
    {
        static_cast<void>(ref_felt);

        GetNonCstHyperelasticLaw().UpdateInvariants(
            matricial_cauchy_green_tensor, quad_pt, geom_elt, generalized_input, contravariant_basis);

        ComputeChainRule(quad_pt, geom_elt, dW, d2W);
    }


    template<class HyperelasticLawT>
    void Hyperelasticity<HyperelasticLawT>::ComputeChainRule(const QuadraturePoint& quad_pt,
                                                             const GeometricElt& geom_elt,
                                                             LocalVector& dW,
                                                             LocalMatrix& d2W)
    {
        decltype(auto) law = GetHyperelasticLaw();

        constexpr bool is_I1 = HyperelasticLawT::template IsPresent<::MoReFEM::InvariantNS::index::I1>();
        constexpr bool is_I2 = HyperelasticLawT::template IsPresent<::MoReFEM::InvariantNS::index::I2>();
        constexpr bool is_I3 = HyperelasticLawT::template IsPresent<::MoReFEM::InvariantNS::index::I3>();
        constexpr bool is_I4 = HyperelasticLawT::template IsPresent<::MoReFEM::InvariantNS::index::I4>();
        constexpr bool is_I6 = HyperelasticLawT::template IsPresent<::MoReFEM::InvariantNS::index::I6>();

        {
            assert(::MoReFEM::Wrappers::Xtensor::IsZeroMatrix(d2W));
            assert(::MoReFEM::Wrappers::Xtensor::IsZeroVector(dW));

            {
                using namespace ::MoReFEM::Wrappers::Xtensor;

                auto& outer_prod = GetNonCstWorkMatrixOuterProduct();

                if constexpr (is_I1)
                {
                    const double dWdI1 = law.FirstDerivativeWFirstInvariant(quad_pt, geom_elt);
                    const auto& dI1dC =
                        law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I1>();

                    xt::noalias(dW) += dWdI1 * dI1dC;

                    const double d2WdI1dI1 = law.SecondDerivativeWFirstInvariant(quad_pt, geom_elt);

                    if (!NumericNS::IsZero(d2WdI1dI1))
                    {
                        xt::noalias(outer_prod) = xt::linalg::outer(dI1dC, dI1dC);
                        xt::noalias(d2W) += d2WdI1dI1 * outer_prod;
                    }

                    if constexpr (is_I2)
                    {
                        const auto& dI2dC =
                            law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I2>();

                        const double d2WdI1dI2 = law.SecondDerivativeWFirstAndSecondInvariant(quad_pt, geom_elt);

                        if (!NumericNS::IsZero(d2WdI1dI2))
                        {
                            xt::noalias(outer_prod) = xt::linalg::outer(dI1dC, dI2dC);
                            xt::noalias(d2W) += d2WdI1dI2 * outer_prod;
                            xt::noalias(outer_prod) = xt::linalg::outer(dI2dC, dI1dC);
                            xt::noalias(d2W) += d2WdI1dI2 * outer_prod;
                        }
                    }

                    if constexpr (is_I3)
                    {
                        const auto& dI3dC =
                            law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I3>();

                        const double d2WdI1dI3 = law.SecondDerivativeWFirstAndThirdInvariant(quad_pt, geom_elt);

                        if (!NumericNS::IsZero(d2WdI1dI3))
                        {
                            xt::noalias(outer_prod) = xt::linalg::outer(dI1dC, dI3dC);
                            xt::noalias(d2W) += d2WdI1dI3 * outer_prod;
                            xt::noalias(outer_prod) = xt::linalg::outer(dI3dC, dI1dC);
                            xt::noalias(d2W) += d2WdI1dI3 * outer_prod;
                        }
                    }

                    if constexpr (is_I4)
                    {
                        const auto& dI4dC =
                            law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I4>();

                        const double d2WdI1dI4 = law.SecondDerivativeWFirstAndFourthInvariant(quad_pt, geom_elt);

                        if (!NumericNS::IsZero(d2WdI1dI4))
                        {
                            xt::noalias(outer_prod) = xt::linalg::outer(dI1dC, dI4dC);
                            xt::noalias(d2W) += d2WdI1dI4 * outer_prod;
                            xt::noalias(outer_prod) = xt::linalg::outer(dI4dC, dI1dC);
                            xt::noalias(d2W) += d2WdI1dI4 * outer_prod;
                        }
                    }

                    if constexpr (is_I6)
                    {
                        const auto& dI6dC =
                            law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I6>();

                        const double d2WdI1dI6 = law.SecondDerivativeWFirstAndSixthInvariant(quad_pt, geom_elt);

                        if (!NumericNS::IsZero(d2WdI1dI6))
                        {
                            xt::noalias(outer_prod) = xt::linalg::outer(dI1dC, dI6dC);
                            xt::noalias(d2W) += d2WdI1dI6 * outer_prod;
                            xt::noalias(outer_prod) = xt::linalg::outer(dI6dC, dI1dC);
                            xt::noalias(d2W) += d2WdI1dI6 * outer_prod;
                        }
                    }
                }

                if constexpr (is_I2)
                {
                    const double dWdI2 = law.FirstDerivativeWSecondInvariant(quad_pt, geom_elt);
                    xt::noalias(d2W) +=
                        dWdI2 * law.template GetSecondDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I2>();

                    const auto& dI2dC =
                        law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I2>();

                    xt::noalias(dW) += dWdI2 * dI2dC;

                    const double d2WdI2dI2 = law.SecondDerivativeWSecondInvariant(quad_pt, geom_elt);

                    if (!NumericNS::IsZero(d2WdI2dI2))
                    {
                        xt::noalias(outer_prod) = xt::linalg::outer(dI2dC, dI2dC);
                        xt::noalias(d2W) += d2WdI2dI2 * outer_prod;
                    }

                    if constexpr (is_I3)
                    {
                        const auto& dI3dC =
                            law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I3>();

                        const double d2WdI2dI3 = law.SecondDerivativeWSecondAndThirdInvariant(quad_pt, geom_elt);

                        if (!NumericNS::IsZero(d2WdI2dI3))
                        {
                            xt::noalias(outer_prod) = xt::linalg::outer(dI2dC, dI3dC);
                            xt::noalias(d2W) += d2WdI2dI3 * outer_prod;
                            xt::noalias(outer_prod) = xt::linalg::outer(dI3dC, dI2dC);
                            xt::noalias(d2W) += d2WdI2dI3 * outer_prod;
                        }
                    }

                    if constexpr (is_I4)
                    {
                        const auto& dI4dC =
                            law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I4>();

                        const double d2WdI2dI4 = law.SecondDerivativeWSecondAndFourthInvariant(quad_pt, geom_elt);

                        if (!NumericNS::IsZero(d2WdI2dI4))
                        {
                            xt::noalias(outer_prod) = xt::linalg::outer(dI2dC, dI4dC);
                            xt::noalias(d2W) += d2WdI2dI4 * outer_prod;
                            xt::noalias(outer_prod) = xt::linalg::outer(dI4dC, dI2dC);
                            xt::noalias(d2W) += d2WdI2dI4 * outer_prod;
                        }
                    }

                    if constexpr (is_I6)
                    {
                        const auto& dI6dC =
                            law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I6>();

                        const double d2WdI2dI6 = law.SecondDerivativeWSecondAndSixthInvariant(quad_pt, geom_elt);

                        if (!NumericNS::IsZero(d2WdI2dI6))
                        {
                            xt::noalias(outer_prod) = xt::linalg::outer(dI2dC, dI6dC);
                            xt::noalias(d2W) += d2WdI2dI6 * outer_prod;
                            xt::noalias(outer_prod) = xt::linalg::outer(dI6dC, dI2dC);
                            xt::noalias(d2W) += d2WdI2dI6 * outer_prod;
                        }
                    }
                }

                if constexpr (is_I3)
                {
                    const double dWdI3 = law.FirstDerivativeWThirdInvariant(quad_pt, geom_elt);
                    xt::noalias(d2W) +=
                        dWdI3 * law.template GetSecondDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I3>();

                    const auto& dI3dC =
                        law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I3>();

                    xt::noalias(dW) += dWdI3 * dI3dC;

                    const double d2WdI3dI3 = law.SecondDerivativeWThirdInvariant(quad_pt, geom_elt);

                    if (!NumericNS::IsZero(d2WdI3dI3))
                    {
                        xt::noalias(outer_prod) = xt::linalg::outer(dI3dC, dI3dC);
                        xt::noalias(d2W) += d2WdI3dI3 * outer_prod;
                    }

                    if constexpr (is_I4)
                    {
                        const auto& dI4dC =
                            law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I4>();

                        const double d2WdI3dI4 = law.SecondDerivativeWThirdAndFourthInvariant(quad_pt, geom_elt);

                        if (!NumericNS::IsZero(d2WdI3dI4))
                        {
                            xt::noalias(outer_prod) = xt::linalg::outer(dI3dC, dI4dC);
                            xt::noalias(d2W) += d2WdI3dI4 * outer_prod;
                            xt::noalias(outer_prod) = xt::linalg::outer(dI4dC, dI3dC);
                            xt::noalias(d2W) += d2WdI3dI4 * outer_prod;
                        }
                    }

                    if constexpr (is_I6)
                    {
                        const auto& dI6dC =
                            law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I6>();

                        const double d2WdI3dI6 = law.SecondDerivativeWThirdAndSixthInvariant(quad_pt, geom_elt);

                        if (!NumericNS::IsZero(d2WdI3dI6))
                        {
                            xt::noalias(outer_prod) = xt::linalg::outer(dI3dC, dI6dC);
                            xt::noalias(d2W) += d2WdI3dI6 * outer_prod;
                            xt::noalias(outer_prod) = xt::linalg::outer(dI6dC, dI3dC);
                            xt::noalias(d2W) += d2WdI3dI6 * outer_prod;
                        }
                    }
                }


                if constexpr (is_I4)
                {
                    const double dWdI4 = law.FirstDerivativeWFourthInvariant(quad_pt, geom_elt);
                    const auto& dI4dC =
                        law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I4>();

                    xt::noalias(dW) += dWdI4 * dI4dC;

                    const double d2WdI4dI4 = law.SecondDerivativeWFourthInvariant(quad_pt, geom_elt);

                    if (!NumericNS::IsZero(d2WdI4dI4))
                    {
                        xt::noalias(outer_prod) = xt::linalg::outer(dI4dC, dI4dC);
                        xt::noalias(d2W) += d2WdI4dI4 * outer_prod;
                    }
                }

                if constexpr (is_I6)
                {
                    const double dWdI6 = law.FirstDerivativeWSixthInvariant(quad_pt, geom_elt);
                    const auto& dI6dC =
                        law.template GetFirstDerivativeWrtCauchyGreen<::MoReFEM::InvariantNS::index::I6>();

                    xt::noalias(dW) += dWdI6 * dI6dC;

                    const double d2WdI6dI6 = law.SecondDerivativeWSixthInvariant(quad_pt, geom_elt);

                    if (!NumericNS::IsZero(d2WdI6dI6))
                    {
                        xt::noalias(outer_prod) = xt::linalg::outer(dI6dC, dI6dC);
                        xt::noalias(d2W) += d2WdI6dI6 * outer_prod;
                    }
                }
            }
        }

        d2W *= 4.;
        dW *= 2.;
    }


    template<class HyperelasticLawT>
    inline LocalMatrix& Hyperelasticity<HyperelasticLawT>::GetNonCstWorkMatrixOuterProduct() noexcept
    {
        return work_matrix_outer_product_;
    }


    template<class HyperelasticLawT>
    inline auto Hyperelasticity<HyperelasticLawT>::GetHyperelasticLaw() const noexcept -> const law_type&
    {
        assert(!(!hyperelastic_law_));
        return *hyperelastic_law_;
    }


    template<class HyperelasticLawT>
    inline auto Hyperelasticity<HyperelasticLawT>::GetNonCstHyperelasticLaw() noexcept -> law_type&
    {
        return const_cast<law_type&>(GetHyperelasticLaw());
    }


} // namespace
  // MoReFEM::Advanced::LocalVariationalOperatorNS::SecondPiolaKirchhoffStressTensorNS::HyperelasticityPolicyNS

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_LOCAL_SECONDPIOLAKIRCHHOFFSTRESSTENSOR_HYPERELASTICITYPOLICY_HYPERELASTICITY_DOT_HXX_
// *** MoReFEM end header guards *** < //
