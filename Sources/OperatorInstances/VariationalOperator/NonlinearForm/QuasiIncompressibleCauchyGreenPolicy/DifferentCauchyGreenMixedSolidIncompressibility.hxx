// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_DIFFERENTCAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_DIFFERENTCAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/QuasiIncompressibleCauchyGreenPolicy/DifferentCauchyGreenMixedSolidIncompressibility.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/QuasiIncompressibleCauchyGreenPolicy/DifferentCauchyGreenMixedSolidIncompressibility.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS
{


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::
        DifferentCauchyGreenMixedSolidIncompressibility(
            const FEltSpace& felt_space,
            const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
            const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
            const cauchy_green_tensor_type& cauchy_green_tensor,
            const TimeManagerT& time_manager,
            const HydrostaticLawPolicyT* hydrostatic_law_volumetric_part,
            const HydrostaticLawPolicyT* hydrostatic_law_deviatoric_part,
            const QuadratureRulePerTopology* const quadrature_rule_per_topology)
    : parent(felt_space,
             unknown_list,
             test_unknown_list,
             quadrature_rule_per_topology,
             AllocateGradientFEltPhi::yes,
             DoComputeProcessorWiseLocal2Global::yes,
             cauchy_green_tensor,
             hydrostatic_law_volumetric_part,
             hydrostatic_law_deviatoric_part)
    {
        felt_space.ComputeLocal2Global(felt_space.GetExtendedUnknownPtr(*unknown_list[1]),
                                       DoComputeProcessorWiseLocal2Global::yes);

        assert(unknown_list[0]->GetNature() == UnknownNS::Nature::vectorial);
        assert(unknown_list[1]->GetNature() == UnknownNS::Nature::scalar);

        assert(test_unknown_list[0]->GetNature() == UnknownNS::Nature::vectorial);
        assert(test_unknown_list[1]->GetNature() == UnknownNS::Nature::scalar);

        const auto god_of_dof_ptr = felt_space.GetGodOfDofFromWeakPtr();
        const auto& god_of_dof = *god_of_dof_ptr;

        const auto& mesh = god_of_dof.GetMesh();
        decltype(auto) local_operator_storage = parent::GetLocalOperatorPerRefGeomElt();

        LocalVector initial_value;

        switch (mesh.GetDimension())
        {
        case 2ul:
            initial_value.resize({ 3ul });
            break;
        case 3ul:
            initial_value.resize({ 6ul });
            break;
        default:
            assert(false);
            exit(EXIT_FAILURE);
        }

        cauchy_green_tensor_volumetric_ =
            std::make_unique<cauchy_green_tensor_type>("Cauchy-Green tensor volumetric",
                                                       felt_space.GetDomain(),
                                                       parent::GetQuadratureRulePerTopology(),
                                                       initial_value,
                                                       time_manager);

        cauchy_green_tensor_operator_volumetric_ = std::make_unique<update_cauchy_green_op_type>(
            felt_space, *unknown_list[0], *cauchy_green_tensor_volumetric_, &parent::GetQuadratureRulePerTopology());

        // Feed CauchyGreen param to each local operator.

        decltype(auto) cauchy_green_tensor_volumetric_raw = cauchy_green_tensor_volumetric_.get();

        Internal::GlobalVariationalOperatorNS::ApplySetCauchyGreenTensor<
            typename parent::local_operator_storage_type,
            0ul,
            std::tuple_size<typename parent::local_operator_storage_type>::value>::
            Perform(local_operator_storage, cauchy_green_tensor_volumetric_raw);
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::ClassName()
    {
        static std::string name("DifferentCauchyGreenMixedSolidIncompressibility");
        return name;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    auto DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                         TimeManagerT>::GetCauchyGreenTensorVolumetric() const noexcept
        -> const cauchy_green_tensor_type&

    {
        assert(!(!cauchy_green_tensor_volumetric_));
        return *cauchy_green_tensor_volumetric_;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT,
                                                                TimeManagerT>::GetCauchyGreenTensorOperatorVolumetric()
        const noexcept -> const update_cauchy_green_op_type&
    {
        assert(!(!cauchy_green_tensor_operator_volumetric_));
        return *cauchy_green_tensor_operator_volumetric_;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<class LinearAlgebraTupleT>
    inline void DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::Assemble(
        LinearAlgebraTupleT&& linear_algebra_tuple,
        ConstRefMonolithicDeviatoricGlobalVector input_vector,
        const Domain& domain) const
    {
        GetCauchyGreenTensorOperatorVolumetric().Update(input_vector.Get());

        return parent::AssembleImpl(std::move(linear_algebra_tuple), domain, input_vector);
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<class LinearAlgebraTupleT>
    inline void DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::Assemble(
        LinearAlgebraTupleT&& linear_algebra_tuple,
        ConstRefMonolithicDeviatoricGlobalVector deviatoric_vector,
        ConstRefMonolithicVolumetricGlobalVector volumetric_vector,
        const Domain& domain) const
    {
        GetCauchyGreenTensorOperatorVolumetric().Update(volumetric_vector.Get());

        return parent::AssembleImpl(std::move(linear_algebra_tuple), domain, deviatoric_vector, volumetric_vector);
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<class LocalOperatorTypeT>
    inline void
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::SetComputeEltArrayArguments(
        const LocalFEltSpace& local_felt_space,
        LocalOperatorTypeT& local_operator,
        const std::tuple<ConstRefMonolithicDeviatoricGlobalVector>& additional_arguments) const
    {
        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(0),
                              std::get<0>(additional_arguments).Get(),
                              local_operator.GetNonCstFormerLocalDisplacementDeviatoric());

        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(1),
                              std::get<0>(additional_arguments).Get(),
                              local_operator.GetNonCstFormerLocalPressureDeviatoric());

        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(0),
                              std::get<0>(additional_arguments).Get(),
                              local_operator.GetNonCstFormerLocalDisplacementVolumetric());

        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(1),
                              std::get<0>(additional_arguments).Get(),
                              local_operator.GetNonCstFormerLocalPressureVolumetric());
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<class LocalOperatorTypeT>
    inline void
    DifferentCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::SetComputeEltArrayArguments(
        const LocalFEltSpace& local_felt_space,
        LocalOperatorTypeT& local_operator,
        const std::tuple<ConstRefMonolithicDeviatoricGlobalVector, ConstRefMonolithicVolumetricGlobalVector>&
            additional_arguments) const
    {
        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(0),
                              std::get<0>(additional_arguments).Get(),
                              local_operator.GetNonCstFormerLocalDisplacementDeviatoric());

        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(1),
                              std::get<0>(additional_arguments).Get(),
                              local_operator.GetNonCstFormerLocalPressureDeviatoric());

        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(0),
                              std::get<1>(additional_arguments).Get(),
                              local_operator.GetNonCstFormerLocalDisplacementVolumetric());

        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(1),
                              std::get<1>(additional_arguments).Get(),
                              local_operator.GetNonCstFormerLocalPressureVolumetric());
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_DIFFERENTCAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HXX_
// *** MoReFEM end header guards *** < //
