// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_SAMECAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_SAMECAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/QuasiIncompressibleCauchyGreenPolicy/SameCauchyGreenMixedSolidIncompressibility.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/NonlinearForm/QuasiIncompressibleCauchyGreenPolicy/SameCauchyGreenMixedSolidIncompressibility.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS
{


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::
        SameCauchyGreenMixedSolidIncompressibility(const FEltSpace& felt_space,
                                                   const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
                                                   const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
                                                   const cauchy_green_tensor_type& cauchy_green_tensor,
                                                   const TimeManagerT& time_manager,
                                                   const HydrostaticLawPolicyT* hydrostatic_law,
                                                   const HydrostaticLawPolicyT* should_be_nullptr,
                                                   const QuadratureRulePerTopology* const quadrature_rule_per_topology)
    : parent(felt_space,
             unknown_list,
             test_unknown_list,
             quadrature_rule_per_topology,
             AllocateGradientFEltPhi::yes,
             DoComputeProcessorWiseLocal2Global::yes,
             cauchy_green_tensor,
             hydrostatic_law,
             nullptr)
    {
        static_cast<void>(should_be_nullptr);
        assert(should_be_nullptr == nullptr);
        static_cast<void>(time_manager);
        felt_space.ComputeLocal2Global(felt_space.GetExtendedUnknownPtr(*unknown_list[1]),
                                       DoComputeProcessorWiseLocal2Global::yes);

        assert(unknown_list[0]->GetNature() == UnknownNS::Nature::vectorial);
        assert(unknown_list[1]->GetNature() == UnknownNS::Nature::scalar);

        assert(test_unknown_list[0]->GetNature() == UnknownNS::Nature::vectorial);
        assert(test_unknown_list[1]->GetNature() == UnknownNS::Nature::scalar);
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::ClassName()
    {
        static std::string name("SameCauchyGreenMixedSolidIncompressibility");
        return name;
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<class LinearAlgebraTupleT>
    inline void SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::Assemble(
        LinearAlgebraTupleT&& linear_algebra_tuple,
        ConstRefMonolithicDeviatoricGlobalVector input_vector,
        const Domain& domain) const
    {
        return parent::AssembleImpl(std::move(linear_algebra_tuple), domain, input_vector);
    }


    template<class HydrostaticLawPolicyT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<class LocalOperatorTypeT>
    inline void
    SameCauchyGreenMixedSolidIncompressibility<HydrostaticLawPolicyT, TimeManagerT>::SetComputeEltArrayArguments(
        const LocalFEltSpace& local_felt_space,
        LocalOperatorTypeT& local_operator,
        const std::tuple<ConstRefMonolithicDeviatoricGlobalVector>& additional_arguments) const
    {
        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(0),
                              std::get<0>(additional_arguments).Get(),
                              local_operator.GetNonCstFormerLocalDisplacement());

        ExtractLocalDofValues(local_felt_space,
                              this->GetNthUnknown(1),
                              std::get<0>(additional_arguments).Get(),
                              local_operator.GetNonCstFormerLocalPressure());
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_NONLINEARFORM_QUASIINCOMPRESSIBLECAUCHYGREENPOLICY_SAMECAUCHYGREENMIXEDSOLIDINCOMPRESSIBILITY_DOT_HXX_
// *** MoReFEM end header guards *** < //
