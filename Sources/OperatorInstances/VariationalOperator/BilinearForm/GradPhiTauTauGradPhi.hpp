// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_GRADPHITAUTAUGRADPHI_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_GRADPHITAUTAUGRADPHI_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
#include <type_traits>
// IWYU pragma: no_include <string>

#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForHpp.hpp" // IWYU pragma: export

#include "ParameterInstances/Fiber/FiberList.hpp"        // IWYU pragma: export
#include "ParameterInstances/Fiber/FiberListManager.hpp" // IWYU pragma: export

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradPhiTauTauGradPhi.hpp" // IWYU pragma: export


namespace MoReFEM::GlobalVariationalOperatorNS
{


    /*!
     * \brief Operator description.
     *
     * \todo #9 Describe operator!
     */
    // clang-format off
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class GradPhiTauTauGradPhi final
    : public GlobalVariationalOperatorNS::SameForAllRefGeomElt
    <
        GradPhiTauTauGradPhi<TimeManagerT>,
        Advanced::OperatorNS::Nature::bilinear,
        Advanced::LocalVariationalOperatorNS::GradPhiTauTauGradPhi<TimeManagerT>
    >
    // clang-format on
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = GradPhiTauTauGradPhi<TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to local operator.
        using local_operator_type = Advanced::LocalVariationalOperatorNS::GradPhiTauTauGradPhi<TimeManagerT>;

        //! Convenient alias to pinpoint the GlobalVariationalOperator parent.
        // clang-format off
        using parent = GlobalVariationalOperatorNS::SameForAllRefGeomElt
        <
            self,
            Advanced::OperatorNS::Nature::bilinear,
            local_operator_type
        >;
        // clang-format on

        static_assert(std::is_convertible<self*, parent*>());

        //! Friendship to the parent class so that the CRTP can reach private methods defined below.
        friend parent;

        //! Unique ptr.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = typename local_operator_type::scalar_parameter_type;

        //! Alias to the type of a vectorial fiber.
        using vectorial_fiber_type = typename local_operator_type::vectorial_fiber_type;


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] unknown_ptr The (scalar) potential.
         * \copydoc doxygen_hide_test_unknown_ptr_param
         *
         * \param[in] transverse_diffusion_tensor transverse_diffusion_tensor
         * \param[in] fiber_diffusion_tensor fiber_diffusion_tensor
         * \param[in] fibers Fibers.
         * \copydoc doxygen_hide_gvo_felt_space_arg
         *
         * \copydoc doxygen_hide_quadrature_rule_per_topology_nullptr_arg
         */
        explicit GradPhiTauTauGradPhi(const FEltSpace& felt_space,
                                      const Unknown::const_shared_ptr unknown_ptr,
                                      const Unknown::const_shared_ptr test_unknown_ptr,
                                      const scalar_parameter_type& transverse_diffusion_tensor,
                                      const scalar_parameter_type& fiber_diffusion_tensor,
                                      const vectorial_fiber_type& fibers,
                                      const QuadratureRulePerTopology* const quadrature_rule_per_topology = nullptr);


        //! Destructor.
        ~GradPhiTauTauGradPhi() = default;

        //! \copydoc doxygen_hide_copy_constructor
        GradPhiTauTauGradPhi(const GradPhiTauTauGradPhi& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        GradPhiTauTauGradPhi(GradPhiTauTauGradPhi&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        GradPhiTauTauGradPhi& operator=(const GradPhiTauTauGradPhi& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GradPhiTauTauGradPhi& operator=(GradPhiTauTauGradPhi&& rhs) = delete;

        ///@}


      public:
        /*!
         * \brief Assemble into one or several matrices.
         *
         * \tparam LinearAlgebraTupleT A tuple that may include \a GlobalMatrixWithCoefficient objects.
         *
         * \param[in] global_matrix_with_coeff_list List of global matrices into which the operator is
         * assembled. These matrices are assumed to be already properly allocated.
         * \param[in] domain Domain upon which the assembling takes place. Beware: if this domain is not a subset
         * of the finite element space, assembling can only occur in a subset of the domain defined in the finite
         * element space; if current \a domain is not a subset of finite element space one, assembling will occur
         * upon the intersection of both.
         *
         */
        template<class LinearAlgebraTupleT>
        void Assemble(LinearAlgebraTupleT&& global_matrix_with_coeff_list, const Domain& domain = Domain()) const;
    };


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiTauTauGradPhi.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_GRADPHITAUTAUGRADPHI_DOT_HPP_
// *** MoReFEM end header guards *** < //
