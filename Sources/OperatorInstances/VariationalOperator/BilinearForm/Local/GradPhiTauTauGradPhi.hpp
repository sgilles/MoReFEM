// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_GRADPHITAUTAUGRADPHI_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_GRADPHITAUTAUGRADPHI_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForHpp.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { template <FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT> class FiberList; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief Local operator for \a GradPhiTauTauGradPhi.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class GradPhiTauTauGradPhi final : public BilinearLocalVariationalOperator<LocalMatrix>,
                                       public Crtp::LocalMatrixStorage<GradPhiTauTauGradPhi<TimeManagerT>, 4ul>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = GradPhiTauTauGradPhi<TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = ScalarParameter<TimeManagerT, ParameterNS::TimeDependencyNS::None>;

        //! Alias to the parent that provides LocalMatrixStorage.
        using matrix_parent = Crtp::LocalMatrixStorage<self, 4ul>;

        //! Alias to the type of a vectorial fiber.
        using vectorial_fiber_type =
            FiberList<FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector, TimeManagerT>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type
         * (vector_shared_ptr) is due to constraints from genericity; for current operator it is expected to
         * hold exactly two unknowns (the first one vectorial and the second one scalar).
         * \copydoc doxygen_hide_test_extended_unknown_list_param
         * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
         * \param[in] transverse_diffusion_tensor Transverse diffusion tensor.
         * \param[in] fiber_diffusion_tensor Fiber diffusion tensor.
         * \param[in] fibers Vectorial fibers.
         *
         * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
         * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
         * method. \endinternal
         */
        explicit GradPhiTauTauGradPhi(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                      const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                                      elementary_data_type&& elementary_data,
                                      const scalar_parameter_type& transverse_diffusion_tensor,
                                      const scalar_parameter_type& fiber_diffusion_tensor,
                                      const vectorial_fiber_type& fibers);

        //! Destructor.
        virtual ~GradPhiTauTauGradPhi() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        GradPhiTauTauGradPhi(const GradPhiTauTauGradPhi& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        GradPhiTauTauGradPhi(GradPhiTauTauGradPhi&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        GradPhiTauTauGradPhi& operator=(const GradPhiTauTauGradPhi& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        GradPhiTauTauGradPhi& operator=(GradPhiTauTauGradPhi&& rhs) = delete;

        ///@}


        //! Compute the elementary vector.
        void ComputeEltArray();

        //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
        void InitLocalComputation()
        { }

        //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
        void FinalizeLocalComputation()
        { }


      private:
        //! Get the transverse diffusion tensor.
        const scalar_parameter_type& GetTransverseDiffusionTensor() const noexcept;

        //! Get the fiber diffusion tensor.
        const scalar_parameter_type& GetFiberDiffusionTensor() const noexcept;

        //! Get the fiber.
        const vectorial_fiber_type& GetFibers() const noexcept;

      private:
        //! \name Material parameters.
        ///@{

        //! First diffusion tensor = sigma_t.
        const scalar_parameter_type& transverse_diffusion_tensor_;

        //! Second diffusion tensor = sigma_l. (longitudinal)
        const scalar_parameter_type& fiber_diffusion_tensor_;

        //! Fibers parameter.
        const vectorial_fiber_type& fibers_;

        ///@}

      private:
        /// \name Useful indexes to fetch the work matrices and vectors.
        ///@{

        //! Indexes of local matrices.
        enum class LocalMatrixIndex : std::size_t { block_matrix = 0, transposed_dphi, dphi_test_sigma, tau_sigma };

        ///@}
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradPhiTauTauGradPhi.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_GRADPHITAUTAUGRADPHI_DOT_HPP_
// *** MoReFEM end header guards *** < //
