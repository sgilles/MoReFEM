// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_VARIABLEMASS_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_VARIABLEMASS_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForHpp.hpp" // IWYU pragma: export


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief Implementation of mass operator with a weight.
     */
    template<class ParameterT>
    class VariableMass final : public BilinearLocalVariationalOperator<LocalMatrix>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = VariableMass<ParameterT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to parent.
        using parent = BilinearLocalVariationalOperator<LocalMatrix>;

        static_assert(std::is_convertible<self*, parent*>());

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type (STL vector)
         * is due to constraints from genericity; for current operator it is expected to hold exactly
         * one unknown.
         * \copydoc doxygen_hide_test_extended_unknown_list_param
         * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
         * \param[in] parameter_factor \a Parameter which provides the weight considered at each \a
         * QuadraturePoint.
         *
         * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
         * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
         * method. \endinternal
         */
        explicit VariableMass(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                              const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                              elementary_data_type&& elementary_data,
                              const ParameterT& parameter_factor);

        //! Destructor.
        virtual ~VariableMass() override = default;

        //! \copydoc doxygen_hide_copy_constructor
        VariableMass(const VariableMass& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        VariableMass(VariableMass&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        VariableMass& operator=(const VariableMass& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        VariableMass& operator=(VariableMass&& rhs) = delete;


        ///@}


        /*!
         * \brief Compute the elementary \a OperatorNatureT.
         *
         * For current operator, only matrix makes sense; so only this specialization is actually defined.
         *
         * Here we consider the case of the operator for one unknown, so the structure of the matrix is (for 3d
         * case):
         *
         *
         *
         *    vx1, vx2, ..., vxm, vy1, vy2, ..., vym, vz1, vz2, ..., vzm
         * (                                                              ) // Same ordering for rows.
         * (                                                              )
         * (                                                              )
         * (                                                              )
         * (                                                              )
         * (                                                              )
         * (                                                              )
         * (                                                              )
         * (                                                              )
         * (                                                              )
         * (                                                              )
         * (                                                              )
         * (                                                              )
         * (                                                              )
         * (                                                              )
         * (                                                              )
         */
        void ComputeEltArray();

        //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
        void InitLocalComputation()
        { }

        //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
        void FinalizeLocalComputation()
        { }


      private:
        //! Accessor to the parameter factor.
        const ParameterT& GetParameterFactor() const noexcept;


      private:
        //! Store a reference to the parameter factor.
        const ParameterT& parameter_factor_;
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/VariableMass.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_VARIABLEMASS_DOT_HPP_
// *** MoReFEM end header guards *** < //
