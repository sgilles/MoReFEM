// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_GRADONGRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_GRADONGRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradOnGradientBasedElasticityTensor.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/Local/GradOnGradientBasedElasticityTensor.hpp"


#include <cassert>
#include <memory>

#include "Operators/LocalVariationalOperator/Advanced/ExtractGradientBasedBlock.hpp"

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/Internal/GradOnGradientBasedElasticityTensor.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    GradOnGradientBasedElasticityTensor<TimeManagerT>::GradOnGradientBasedElasticityTensor(
        const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
        const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
        elementary_data_type&& a_elementary_data,
        const scalar_parameter_type& young_modulus,
        const scalar_parameter_type& poisson_ratio,
        const ParameterNS::GradientBasedElasticityTensorConfiguration configuration)
    : BilinearLocalVariationalOperator(unknown_list, test_unknown_list, std::move(a_elementary_data)), matrix_parent()
    {
        assert(unknown_list.size() == 1);
        assert(test_unknown_list.size() == 1);

        const auto& elementary_data = GetElementaryData();

        gradient_based_elasticity_tensor_parameter_ =
            Internal::LocalVariationalOperatorNS::InitGradientBasedElasticityTensor<TimeManagerT>(
                young_modulus, poisson_ratio, configuration);

        const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto Nnode_for_unknown = unknown_ref_felt.Nnode();

        const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
        const auto Nnode_for_test_unknown = test_unknown_ref_felt.Nnode();

        const auto felt_space_dimension = unknown_ref_felt.GetFEltSpaceDimension();

        matrix_parent::InitLocalMatrixStorage({ {
            { felt_space_dimension, felt_space_dimension },  // gradient_based_block
            { Nnode_for_test_unknown, Nnode_for_unknown },   // block_contribution
            { felt_space_dimension, Nnode_for_unknown },     // transposed dphi
            { Nnode_for_test_unknown, felt_space_dimension } // dphi_test x gradient_based_block
        } });
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& GradOnGradientBasedElasticityTensor<TimeManagerT>::ClassName()
    {
        static std::string name("GradOnGradientBasedElasticityTensor");
        return name;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    void GradOnGradientBasedElasticityTensor<TimeManagerT>::ComputeEltArray()
    {
        auto& elementary_data = GetNonCstElementaryData();

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();

        auto& gradient_based_block =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::gradient_based_block)>();
        auto& block_contribution =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::block_contribution)>();
        auto& transposed_dphi =
            matrix_parent::template GetLocalMatrix<EnumUnderlyingType(LocalMatrixIndex::transposed_dphi)>();
        auto& dphi_test_mult_gradient_based_block = matrix_parent::template GetLocalMatrix<EnumUnderlyingType(
            LocalMatrixIndex::dphi_test_mult_gradient_based_block)>();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto& unknown_ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto Nnode_for_unknown = unknown_ref_felt.Nnode();

        const auto& test_unknown_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));
        const auto Nnode_for_test_unknown = test_unknown_ref_felt.Nnode();

        const auto Ncomponent = unknown_ref_felt.Ncomponent();

        assert(Ncomponent == test_unknown_ref_felt.Ncomponent());

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();

        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
            decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

            const auto& gradient_based_elasticity_tensor =
                GetNonCstGradientBasedElasticityTensor().GetValue(quad_pt, geom_elt);

            const double factor = quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant() * quad_pt.GetWeight();

            const auto& dphi = quad_pt_unknown_data.GetGradientFEltPhi();

            const auto& dphi_test = test_quad_pt_unknown_data.GetGradientFEltPhi();

            xt::noalias(transposed_dphi) = xt::transpose(dphi);


            for (ComponentNS::index_type row_component{ 0ul }; row_component < Ncomponent; ++row_component)
            {
                const auto row_first_index = test_unknown_ref_felt.GetIndexFirstDofInElementaryData(row_component);

                for (ComponentNS::index_type col_component{ 0ul }; col_component < Ncomponent; ++col_component)
                {
                    const auto col_first_index = unknown_ref_felt.GetIndexFirstDofInElementaryData(col_component);

                    Advanced::LocalVariationalOperatorNS::ExtractGradientBasedBlock(
                        gradient_based_elasticity_tensor, row_component, col_component, gradient_based_block);

                    xt::noalias(dphi_test_mult_gradient_based_block) = xt::linalg::dot(dphi_test, gradient_based_block);

                    xt::noalias(block_contribution) =
                        factor * xt::linalg::dot(dphi_test_mult_gradient_based_block, transposed_dphi);

                    for (auto row_node = 0ul; row_node < Nnode_for_test_unknown; ++row_node)
                    {
                        for (auto col_node = 0ul; col_node < Nnode_for_unknown; ++col_node)
                            matrix_result(row_first_index + row_node, col_first_index + col_node) +=
                                block_contribution(row_node, col_node);
                    }
                }
            }
        }
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    inline auto GradOnGradientBasedElasticityTensor<TimeManagerT>::GetNonCstGradientBasedElasticityTensor()
        -> matrix_parameter_type&
    {
        assert(!(!gradient_based_elasticity_tensor_parameter_));
        return *gradient_based_elasticity_tensor_parameter_;
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_GRADONGRADIENTBASEDELASTICITYTENSOR_DOT_HXX_
// *** MoReFEM end header guards *** < //
