// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_SURFACICBIDOMAIN_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_SURFACICBIDOMAIN_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "OperatorInstances/VariationalOperator/BilinearForm/Local/FwdForHpp.hpp" // IWYU pragma: export

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS { class ForUnknownList; }
namespace MoReFEM { template <FiberNS::AtNodeOrAtQuadPt FiberPolicyT, ParameterNS::Type TypeT, TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT> class FiberList; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief Local operator for \a SurfacicBidomain.
     */
    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    class SurfacicBidomain final
    // clang-format off
    : public BilinearLocalVariationalOperator<LocalMatrix>,
    public Crtp::LocalMatrixStorage<SurfacicBidomain<TimeManagerT>, 18u>,
    public Crtp::LocalVectorStorage<SurfacicBidomain<TimeManagerT>, 3ul>
    // clang-format on
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = SurfacicBidomain<TimeManagerT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! \copydoc doxygen_hide_operator_alias_scalar_parameter
        using scalar_parameter_type = ScalarParameter<TimeManagerT, ParameterNS::TimeDependencyNS::None>;

        //! Alias to the parent that provides LocalMatrixStorage.
        using matrix_parent = Crtp::LocalMatrixStorage<self, 18ul>;

        //! Alias to the parent that provides LocalVectorStorage.
        using vector_parent = Crtp::LocalVectorStorage<self, 3ul>;

        //! Alias to the type of a scalar fiber.
        using scalar_fiber_type =
            FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::scalar, TimeManagerT>;

        //! Alias to the type of a vectorial fiber.
        using vectorial_fiber_type =
            FiberList<::MoReFEM::FiberNS::AtNodeOrAtQuadPt::at_node, ParameterNS::Type::vector, TimeManagerT>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type (vector_shared_ptr)
         * is due to constraints from genericity; for current operator it is expected to hold exactly
         * two unknowns (the first one vectorial and the second one scalar).
         * \copydoc doxygen_hide_test_extended_unknown_list_param
         * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
         * \param[in] intracellular_trans_diffusion_tensor Intracellular trans diffusion tensor.
         * \param[in] extracellular_trans_diffusion_tensor Extracellular trans diffusion tensor.
         * \param[in] intracellular_fiber_diffusion_tensor Intracellular fiber diffusion tensor.
         * \param[in] extracellular_fiber_diffusion_tensor Extracellular fiber diffusion tensor.
         * \param[in] heterogeneous_conductivity_coefficient Heterogeneous conductivity coefficient.
         * \param[in] fibers Fibers.
         * \param[in] angles Angles. \todo #9 Should be completed.
         *
         * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved only in
         * GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList() method.
         * \endinternal
         */
        explicit SurfacicBidomain(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                  const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                                  elementary_data_type&& elementary_data,
                                  const scalar_parameter_type& intracellular_trans_diffusion_tensor,
                                  const scalar_parameter_type& extracellular_trans_diffusion_tensor,
                                  const scalar_parameter_type& intracellular_fiber_diffusion_tensor,
                                  const scalar_parameter_type& extracellular_fiber_diffusion_tensor,
                                  const scalar_parameter_type& heterogeneous_conductivity_coefficient,
                                  const vectorial_fiber_type& fibers,
                                  const scalar_fiber_type& angles);

        //! Destructor.
        virtual ~SurfacicBidomain() override;

        //! \copydoc doxygen_hide_copy_constructor
        SurfacicBidomain(const SurfacicBidomain& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        SurfacicBidomain(SurfacicBidomain&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        SurfacicBidomain& operator=(const SurfacicBidomain& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        SurfacicBidomain& operator=(SurfacicBidomain&& rhs) = delete;

        ///@}

        //! Compute the elementary vector.
        void ComputeEltArray();

        //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
        void InitLocalComputation()
        { }

        //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
        void FinalizeLocalComputation()
        { }


      private:
        /*!
         * \brief Compute contravariant basis.
         *
         * \copydoc doxygen_hide_quad_pt_unknown_data_arg
         */
        void ComputeContravariantBasis(
            const Advanced::LocalVariationalOperatorNS::InfosAtQuadPointNS ::ForUnknownList& quad_pt_unknown_data);


      private:
        //! Get the first diffusion tensor.
        const scalar_parameter_type& GetIntracelluarTransDiffusionTensor() const noexcept;

        //! Get the second diffusion tensor.
        const scalar_parameter_type& GetExtracelluarTransDiffusionTensor() const noexcept;

        //! Get the third diffusion tensor.
        const scalar_parameter_type& GetIntracelluarFiberDiffusionTensor() const noexcept;

        //! Get the fourth diffusion tensor.
        const scalar_parameter_type& GetExtracelluarFiberDiffusionTensor() const noexcept;

        //! Get the coefficient for Heterogeneous Conductivity.
        const scalar_parameter_type& GetHeterogeneousConductivityCoefficient() const noexcept;

        //! Get the fiber.
        const vectorial_fiber_type& GetFibers() const noexcept;

        //! Get the angles.
        const scalar_fiber_type& GetAngles() const noexcept;

      private:
        //! \name Material parameters.
        ///@{

        //! First diffusion tensor = sigma_i_t.
        const scalar_parameter_type& intracellular_trans_diffusion_tensor_;

        //! Second diffusion tensor = sigma_e_t.
        const scalar_parameter_type& extracellular_trans_diffusion_tensor_;

        //! Third diffusion tensor = sigma_i_l.
        const scalar_parameter_type& intracellular_fiber_diffusion_tensor_;

        //! Fourth diffusion tensor = sigma_e_l.
        const scalar_parameter_type& extracellular_fiber_diffusion_tensor_;

        //! Coefficient for heterogeneous_conductivity.
        const scalar_parameter_type& heterogeneous_conductivity_coefficient_;

        //! Fibers parameter.
        const vectorial_fiber_type& fibers_;

        //! Angles parameter.
        const scalar_fiber_type& angles_;

        ///@}

      private:
        /// \name Useful indexes to fetch the work matrices and vectors.
        ///@{

        //! Indexes for local matrices.
        enum class LocalMatrixIndex : std::size_t {
            block_matrix1 = 0,
            block_matrix2,
            block_matrix3,
            block_matrix4,
            transposed_dphi,
            transposed_dpsi,
            dphi_test_sigma,
            dpsi_test_sigma,
            dphi_test_contravariant_metric_tensor,
            dpsi_test_contravariant_metric_tensor,
            tau_sigma,
            tau_ortho_sigma,
            covariant_basis,
            contravariant_basis,
            transposed_covariant_basis,
            covariant_metric_tensor,
            contravariant_metric_tensor,
            reduced_contravariant_metric_tensor
        };


        //! Indexes for local vectors.
        enum class LocalVectorIndex : std::size_t {
            tau_interpolate_ortho = 0,
            tau_covariant_basis,
            tau_ortho_covariant_basis
        };

        ///@}
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/BilinearForm/Local/SurfacicBidomain.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_SURFACICBIDOMAIN_DOT_HPP_
// *** MoReFEM end header guards *** < //
