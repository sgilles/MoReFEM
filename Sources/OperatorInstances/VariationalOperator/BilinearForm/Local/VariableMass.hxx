// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_VARIABLEMASS_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_VARIABLEMASS_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/Local/VariableMass.hpp"
// *** MoReFEM header guards *** < //


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    template<class ParameterT>
    VariableMass<ParameterT>::VariableMass(const ExtendedUnknown::vector_const_shared_ptr& unknown_list,
                                           const ExtendedUnknown::vector_const_shared_ptr& test_unknown_list,
                                           elementary_data_type&& a_elementary_data,
                                           const ParameterT& parameter_factor)
    : BilinearLocalVariationalOperator(unknown_list, test_unknown_list, std::move(a_elementary_data)),
      parameter_factor_(parameter_factor)
    {
        assert(unknown_list.size() == 1);
        assert(test_unknown_list.size() == 1);
    }


    template<class ParameterT>
    const std::string& VariableMass<ParameterT>::ClassName()
    {
        static std::string name("VariableMass");
        return name;
    }


    template<class ParameterT>
    void VariableMass<ParameterT>::ComputeEltArray()
    {
        auto& elementary_data = GetNonCstElementaryData();

        const auto& infos_at_quad_pt_list = elementary_data.GetInformationAtQuadraturePointList();

        const auto& ref_felt = elementary_data.GetRefFElt(GetNthUnknown(0));
        const auto& test_ref_felt = elementary_data.GetTestRefFElt(GetNthTestUnknown(0));

        const auto Ncomponent = ref_felt.Ncomponent();

        assert(Ncomponent == test_ref_felt.Ncomponent());

        auto& matrix_result = elementary_data.GetNonCstMatrixResult();

        const auto Nnode_for_unknown = ref_felt.Nnode();
        const auto Nnode_for_test_unknown = test_ref_felt.Nnode();

        const auto& geom_elt = elementary_data.GetCurrentGeomElt();
        const auto& parameter_factor = GetParameterFactor();


        for (const auto& infos_at_quad_pt : infos_at_quad_pt_list)
        {
            decltype(auto) quad_pt_unknown_data = infos_at_quad_pt.GetUnknownData();
            decltype(auto) test_quad_pt_unknown_data = infos_at_quad_pt.GetTestUnknownData();

            const auto& phi_unknown = quad_pt_unknown_data.GetRefFEltPhi();
            const auto& phi_test_unknown = test_quad_pt_unknown_data.GetRefFEltPhi();

            const auto& quad_pt = infos_at_quad_pt.GetQuadraturePoint();

            const double factor = quad_pt.GetWeight() * quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant()
                                  * parameter_factor.GetValue(quad_pt, geom_elt);

            assert(NumericNS::AreEqual(quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant(),
                                       test_quad_pt_unknown_data.GetAbsoluteValueJacobianDeterminant()));

            for (auto m = 0ul; m < Nnode_for_test_unknown; ++m)
            {
                const double m_value = factor * phi_test_unknown(m);

                for (auto n = 0ul; n < Nnode_for_unknown; ++n)
                {
                    const double value = m_value * phi_unknown(n);

                    for (ComponentNS::index_type component{ 0ul }; component < Ncomponent; ++component)
                    {
                        const auto shift = Nnode_for_test_unknown * component.Get();
                        matrix_result(m + shift, n + shift) += value;
                    }
                }
            }
        }
    }


    template<class ParameterT>
    inline const ParameterT& VariableMass<ParameterT>::GetParameterFactor() const noexcept
    {
        return parameter_factor_;
    }


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_LOCAL_VARIABLEMASS_DOT_HXX_
// *** MoReFEM end header guards *** < //
