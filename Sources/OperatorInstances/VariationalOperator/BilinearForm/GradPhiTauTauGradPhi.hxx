// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_GRADPHITAUTAUGRADPHI_DOT_HXX_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_GRADPHITAUTAUGRADPHI_DOT_HXX_
// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiTauTauGradPhi.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "OperatorInstances/VariationalOperator/BilinearForm/GradPhiTauTauGradPhi.hpp"


#include <string>

#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForCpp.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForHpp.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS
{


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    GradPhiTauTauGradPhi<TimeManagerT>::GradPhiTauTauGradPhi(
        const FEltSpace& felt_space,
        const Unknown::const_shared_ptr unknown_ptr,
        const Unknown::const_shared_ptr test_unknown_ptr,
        const scalar_parameter_type& transverse_diffusion_tensor,
        const scalar_parameter_type& fiber_diffusion_tensor,
        const vectorial_fiber_type& fibers,
        const QuadratureRulePerTopology* const quadrature_rule_per_topology)
    : parent(felt_space,
             unknown_ptr,
             test_unknown_ptr,
             quadrature_rule_per_topology,
             AllocateGradientFEltPhi::yes,
             DoComputeProcessorWiseLocal2Global::no,
             transverse_diffusion_tensor,
             fiber_diffusion_tensor,
             fibers)
    { }

    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    const std::string& GradPhiTauTauGradPhi<TimeManagerT>::ClassName()
    {
        static std::string name("GradPhiTauTauGradPhi");
        return name;
    }


    template<TIME_MANAGER_TEMPLATE_KEYWORD TimeManagerT>
    template<class LinearAlgebraTupleT>
    inline void GradPhiTauTauGradPhi<TimeManagerT>::Assemble(LinearAlgebraTupleT&& linear_algebra_tuple,
                                                             const Domain& domain) const
    {
        return parent::template AssembleImpl<>(std::move(linear_algebra_tuple), domain);
    }


} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_BILINEARFORM_GRADPHITAUTAUGRADPHI_DOT_HXX_
// *** MoReFEM end header guards *** < //
