// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

#include "OperatorInstances/VariationalOperator/BilinearForm/ScalarDivVectorial.hpp"
#include "OperatorInstances/VariationalOperator/BilinearForm/FwdForCpp.hpp"


namespace MoReFEM::GlobalVariationalOperatorNS
{


    ScalarDivVectorial::ScalarDivVectorial(const FEltSpace& felt_space,
                                           const std::array<Unknown::const_shared_ptr, 2>& unknown_list,
                                           const std::array<Unknown::const_shared_ptr, 2>& test_unknown_list,
                                           const double alpha,
                                           const double beta,
                                           const QuadratureRulePerTopology* const quadrature_rule_per_topology)
    : parent(felt_space,
             unknown_list,
             test_unknown_list,
             quadrature_rule_per_topology,
             AllocateGradientFEltPhi::yes,
             DoComputeProcessorWiseLocal2Global::no,
             alpha,
             beta)
    {
        assert(unknown_list[0]->GetNature() == UnknownNS::Nature::vectorial);
        assert(unknown_list[1]->GetNature() == UnknownNS::Nature::scalar);

        assert(test_unknown_list[0]->GetNature() == UnknownNS::Nature::vectorial);
        assert(test_unknown_list[1]->GetNature() == UnknownNS::Nature::scalar);
    }


    const std::string& ScalarDivVectorial::ClassName()
    {
        static std::string name("ScalarDivVectorial");
        return name;
    }

} // namespace MoReFEM::GlobalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //
