// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup OperatorInstancesGroup
 * \addtogroup OperatorInstancesGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_DOT_HPP_
#define MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include "OperatorInstances/VariationalOperator/LinearForm/Local/FwdForHpp.hpp" // IWYU pragma: export

#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource/ReactionLaw/Concept.hpp"


namespace MoReFEM::Advanced::LocalVariationalOperatorNS
{


    /*!
     * \brief Implementation of a non-linear source f(u,w), u the solution of the EDP and w a parameter.
     * f is the reaction law.
     * w depends of a reaction law dw/dt = g(u,w), g is the gate law.
     * u is the potential and w is the gate.
     *
     * As of now this Operator is not completely generic. It only takes into account one parameter.
     * u may be vectorial but the user will have to be careful in f to update properly its component the way he
     * wants. For now there is only one function so it done as follow for u = (u1 u2) f_res = (f1 f2) = (f(u1,w)
     * f(u2,w)) It is not very difficult in the reaction law to change change and adapt for more excotic laws.
     * Same thing for g where one have to be careful how to deal with each component of u in the reaction law.
     * Ex : g(u, w) = g(u1, w) or g(u2, w) if u = (u1 u2) (See the reaction law to see how to deal with u)
     *
     * \copydoc doxygen_hide_reaction_law_template_arg
     */
    // clang-format off
    template
    <
        Advanced::Concept::ReactionLaw ReactionLawT
    >
    // clang-format on
    class NonLinearSource final : public LinearLocalVariationalOperator<LocalVector>
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = NonLinearSource<ReactionLawT>;

        //! Alias to unique pointer.
        using unique_ptr = std::unique_ptr<self>;

        //! \copydoc doxygen_hide_time_manager_type_alias
        using time_manager_type = typename ReactionLawT::time_manager_type;

        //! Returns the name of the operator.
        static const std::string& ClassName();

        //! Alias to the reaction law type.
        using reaction_law_type = ReactionLawT;

        //! Alias to parent.
        using parent = LinearLocalVariationalOperator<LocalVector>;

        static_assert(std::is_convertible<self*, parent*>());

        //! Use elementary_data_type from parent.
        using elementary_data_type = typename parent::elementary_data_type;

        //! Alias to a scalar parameter at quadrature point.
        using ScalarParameterAtQuadPt = ParameterAtQuadraturePoint<ParameterNS::Type::scalar,
                                                                   time_manager_type,
                                                                   ParameterNS::TimeDependencyNS::None>;

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \param[in] extended_unknown_list List of unknowns considered by the operators. Its type
         * (vector_shared_ptr) is due to constraints from genericity; for current operator it is expected to
         * hold exactly two unknowns (the first one vectorial and the second one scalar).
         * \copydoc doxygen_hide_test_extended_unknown_list_param
         * \param[in] elementary_data Elementary matrices and vectors that will perform the calculations.
         *
         * \internal <b><tt>[internal]</tt></b> This constructor must not be called manually: it is involved
         * only in GlobalVariationalOperator<DerivedT, LocalVariationalOperatorT>::CreateLocalOperatorList()
         * method. \endinternal
         *
         * \param[in] reaction_law It depends also of a reaction law to determine how the source depends on the
         * potential and the gate and also how the gate evolves in time.
         */
        explicit NonLinearSource(const ExtendedUnknown::vector_const_shared_ptr& extended_unknown_list,
                                 const ExtendedUnknown::vector_const_shared_ptr& test_extended_unknown_list,
                                 elementary_data_type&& elementary_data,
                                 reaction_law_type& reaction_law);

        //! Destructor.
        virtual ~NonLinearSource() override;

        //! \copydoc doxygen_hide_copy_constructor
        NonLinearSource(const NonLinearSource& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        NonLinearSource(NonLinearSource&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        NonLinearSource& operator=(const NonLinearSource& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        NonLinearSource& operator=(NonLinearSource&& rhs) = delete;

        ///@}

        //! Compute the elementary vector.
        void ComputeEltArray();

        //! \copydoc doxygen_hide_local_variational_operator_empty_init_local_computation
        void InitLocalComputation()
        { }

        //! \copydoc doxygen_hide_local_variational_operator_empty_finalize_local_computation
        void FinalizeLocalComputation()
        { }


        //! Constant accessor to the former local solution required by ComputeEltArray().
        const std::vector<double>& GetFormerLocalSolution() const noexcept;

        //! Non constant accessor to the former local solution required by ComputeEltArray().
        std::vector<double>& GetNonCstFormerLocalSolution() noexcept;

      public:
        //! Non constant accessor to the gate.
        ScalarParameterAtQuadPt& GetNonCstGate() noexcept;

        //! Constant accessor to the gate.
        const ScalarParameterAtQuadPt& GetGate() const noexcept;

      private:
        //! Accessor to the reaction law.
        const reaction_law_type& GetReactionLaw() const noexcept;

        //! Non constant accessor to the reaction law.
        reaction_law_type& GetNonCstReactionLaw() noexcept;

      public:
        //! Reaction law.
        reaction_law_type& reaction_law_;

        /*!
         * \brief Solution obtained at previous time iteration expressed at the local level.
         *
         * \internal <b><tt>[internal]</tt></b> This is a work variable that should be used only within
         * ComputeEltArray.
         * \endinternal
         */
        std::vector<double> former_local_solution_;
    };


} // namespace MoReFEM::Advanced::LocalVariationalOperatorNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup OperatorInstancesGroup
// *** MoReFEM Doxygen end of group *** < //


#include "OperatorInstances/VariationalOperator/LinearForm/Local/NonLinearSource.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_OPERATORINSTANCES_VARIATIONALOPERATOR_LINEARFORM_LOCAL_NONLINEARSOURCE_DOT_HPP_
// *** MoReFEM end header guards *** < //
