// BEWARE: if dimension is in/out, use the 3_bis comment!
/*!
* \class doxygen_hide_mesh_constructor_1
*
* \param[in] mesh_id Unique identifier of the \a Mesh.
* \param[in] dimension Dimension of the mesh.
* \copydoc doxygen_hide_space_unit_arg
*/


/*!
* \class doxygen_hide_mesh_constructor_1_bis
*
* \param[in] mesh_id Unique identifier of the \a Mesh.
* \param[in,out] dimension Dimension of the mesh read in the input file.
* \copydoc doxygen_hide_space_unit_arg
*/


/*!
* \class doxygen_hide_mesh_constructor_2
*
* \param[in] do_build_edge Whether edges should be built or not.
* \param[in] do_build_face Whether faces should be built or not.
* \param[in] do_build_volume Whether volumes should be built or not.
* \param[in] do_build_pseudo_normals Whether pseudo-normals should be built or not.
*/

// BEWARE: if parameters are in/out, use the 3_bis comment!
/*!
* \class doxygen_hide_mesh_constructor_3
*
* \param[in,out] unsort_processor_wise_geom_elt_list  List of processor-wise\a GeometricElt. No specific order is expected here.
* \param[in,out] unsort_ghost_geom_elt_list List of ghost\a GeometricElt. No specific order is expected here.
* \param[in] coords_list List of \a Coords objects.
* \param[in] mesh_label_list List of \a MeshLabels.
*/


/*!
* \class doxygen_hide_mesh_constructor_3_bis
*
* \param[in,out] unsort_processor_wise_geom_elt_list  List of processor-wise\a GeometricElt. No specific order is expected here.
* \param[in,out] unsort_ghost_geom_elt_list List of ghost\a GeometricElt. No specific order is expected here.
* \param[in,out] coords_list List of \a Coords objects.
* \param[in,out] mesh_label_list List of \a MeshLabels.
*/


/*!
* \class doxygen_hide_mesh_constructor_4
*
* \param[in] mesh_file File from which the data will be loaded.
*/

/*!
* \class doxygen_hide_mesh_constructor_5
*
* \copydoc doxygen_hide_mesh_constructor_4
* \param[in] format Format of the input file.
*/


/*!
* \class doxygen_hide_mesh_optional_prepartitioned_data_arg
*
* \param[in] prepartitioned_data Optional parameter which is used when we are in fact loading a \a Mesh from prepartitioned data. In this
* case, this file includes the relevant data required to rebuild the \a Mesh identically as the one that was serialized in an earlier run.
*/
