// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_INTERNAL_MESHMANAGER_DOT_HXX_
#define MOREFEM_GEOMETRY_MESH_INTERNAL_MESHMANAGER_DOT_HXX_
// IWYU pragma: private, include "Geometry/Mesh/Internal/MeshManager.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Geometry/Mesh/Internal/MeshManager.hpp"


// IWYU pragma: no_include <__tree>

#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <filesystem>
#include <memory>
#include <optional>
#include <sstream>

#include "Utilities/Filesystem/Directory.hpp"
#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "ThirdParty/Wrappers/Lua/OptionFile/OptionFile.hpp"

#include "Core/MoReFEMData/Advanced/Concept.hpp"

#include "Geometry/Mesh/Internal/Format/Format.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM::Internal::MeshNS
{


    // clang-format off
    template
    <
        class IndexedSectionDescriptionT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void MeshManager::Create(const IndexedSectionDescriptionT&,
                             const ModelSettingsT& model_settings,
                             const InputDataT& input_data)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        decltype(auto) mesh_file = ::MoReFEM::FilesystemNS::File{ std::filesystem::path(
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::Path>(model_settings, input_data)) };
        decltype(auto) dimension =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::Dimension>(model_settings, input_data);
        const auto format = FormatNS::GetType(
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::Format>(model_settings, input_data));
        const auto space_unit =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::SpaceUnit>(model_settings, input_data);

        Create(::MoReFEM::MeshNS::unique_id{ section_type::GetUniqueId() },
               mesh_file,
               dimension,
               format,
               space_unit,
               Mesh::BuildEdge::yes,
               Mesh::BuildFace::yes,
               Mesh::BuildVolume::yes,
               Mesh::BuildPseudoNormals::no,
               std::nullopt); // \todo #57
    }


    // clang-format off
    template
    <
        class IndexedSectionDescriptionT,
        ::MoReFEM::Advanced::Concept::MoReFEMDataType MoReFEMDataT
    >
    // clang-format on
    void MeshManager::LoadFromPrepartitionedData(const IndexedSectionDescriptionT&, const MoReFEMDataT& morefem_data)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        decltype(auto) dimension = ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::Dimension>(morefem_data);

        const auto format =
            FormatNS::GetType(::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::Format>(morefem_data));

        decltype(auto) parallelism_ptr = morefem_data.GetParallelismPtr();

        assert(!(!parallelism_ptr)
               && "This method should not be called in no parallelism defined in input "
                  "data file!");

        decltype(auto) parallelism_dir = parallelism_ptr->GetDirectory();

        std::ostringstream oconv;
        oconv << "Mesh_" << section_type::GetUniqueId();

        ::MoReFEM::FilesystemNS::Directory mesh_dir(
            parallelism_dir, std::filesystem::path{ oconv.str() }, ::MoReFEM::FilesystemNS::behaviour::read);

        mesh_dir.ActOnFilesystem();

        const auto lua_file = mesh_dir.AddFile("mesh_data.lua");

        ::MoReFEM::Wrappers::Lua::OptionFile prepartitioned_data(lua_file);

        const auto mesh_file = mesh_dir.AddFile("mesh.mesh");

        LoadFromPrepartitionedData(morefem_data.GetMpi(),
                                   ::MoReFEM::MeshNS::unique_id{ section_type::GetUniqueId() },
                                   mesh_file,
                                   prepartitioned_data,
                                   dimension,
                                   format);
    }


    inline Mesh& MeshManager::GetNonCstMesh(::MoReFEM::MeshNS::unique_id unique_id)
    {
        return const_cast<Mesh&>(GetMesh(unique_id));
    }


    inline const MeshManager::storage_type& MeshManager ::GetStorage() const noexcept
    {
        return storage_;
    }


    inline MeshManager::storage_type& MeshManager ::GetNonCstStorage() noexcept
    {
        return const_cast<storage_type&>(GetStorage());
    }


} // namespace MoReFEM::Internal::MeshNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_INTERNAL_MESHMANAGER_DOT_HXX_
// *** MoReFEM end header guards *** < //
