// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_EXCEPTIONS_FORMAT_DOT_HPP_
#define MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_EXCEPTIONS_FORMAT_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd> // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Utilities/Exceptions/Exception.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::ExceptionNS::Format
{

    //! Generic exception.
    class Exception : public MoReFEM::Exception
    {
      public:
        /*!
         * \brief Constructor with simple message
         *
         * \param[in] msg Message
         * \copydoc doxygen_hide_source_location

         */
        explicit Exception(const std::string& msg,
                           const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~Exception() override;

        //! \copydoc doxygen_hide_copy_constructor
        Exception(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        Exception(Exception&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        Exception& operator=(const Exception& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        Exception& operator=(Exception&& rhs) = default;
    };


    //! Thrown when mesh file couldn't be opened.
    class UnableToOpenFile final : public Exception
    {

      public:
        /*!
         * \brief Constructor
         *
         * \param[in] mesh_file Mesh file being read
         * \copydoc doxygen_hide_source_location

         */
        explicit UnableToOpenFile(const FilesystemNS::File& mesh_file,
                                  const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~UnableToOpenFile() override;

        //! \copydoc doxygen_hide_copy_constructor
        UnableToOpenFile(const UnableToOpenFile& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        UnableToOpenFile(UnableToOpenFile&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        UnableToOpenFile& operator=(const UnableToOpenFile& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        UnableToOpenFile& operator=(UnableToOpenFile&& rhs) = default;
    };


    //! Called when there is an attempt to write in Medit format a geometric elementtype not supported
    class UnsupportedGeometricElt final : public Exception
    {

      public:
        /*!
         * \brief Constructor
         *
         * \param[in] geometric_elt_identifier String that identifies the kind of geometric elementconsidered
         (eg 'Triangle3')
         * \param[in] format Name of the format which doesn't support the element (e.g. 'Ensight')
         * \copydoc doxygen_hide_source_location
         */
        explicit UnsupportedGeometricElt(const std::string& geometric_elt_identifier,
                                         const std::string& format,
                                         const std::source_location location = std::source_location::current());

        //! Destructor
        virtual ~UnsupportedGeometricElt() override;

        //! \copydoc doxygen_hide_copy_constructor
        UnsupportedGeometricElt(const UnsupportedGeometricElt& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        UnsupportedGeometricElt(UnsupportedGeometricElt&& rhs) = default;

        //! \copydoc doxygen_hide_copy_affectation
        UnsupportedGeometricElt& operator=(const UnsupportedGeometricElt& rhs) = default;

        //! \copydoc doxygen_hide_move_affectation
        UnsupportedGeometricElt& operator=(UnsupportedGeometricElt&& rhs) = default;
    };


} // namespace MoReFEM::ExceptionNS::Format


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_MESH_INTERNAL_FORMAT_EXCEPTIONS_FORMAT_DOT_HPP_
// *** MoReFEM end header guards *** < //
