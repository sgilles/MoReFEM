// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <iostream>
#include <memory>
#include <sstream>
#include <string>
#include <type_traits> // IWYU pragma: keep
#include <unordered_map>
#include <utility>
#include <vector>

#include "Utilities/Containers/Sort.hpp"
#include "Utilities/Containers/UnorderedMap.hpp"
#include "Utilities/Filesystem/File.hpp"

#include "ThirdParty/IncludeWithoutWarning/Libmeshb/Libmeshb.hpp"

#include "Geometry/Coords/Internal/Factory.hpp"
#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"
#include "Geometry/GeometricElt/Advanced/SortingCriterion.hpp"
#include "Geometry/Mesh/Internal/Format/Exceptions/Medit.hpp"
#include "Geometry/Mesh/Internal/Format/Format.hpp"
#include "Geometry/Mesh/Internal/Format/Medit.hpp" // IWYU pragma: associated
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM::Internal::MeshNS::FormatNS::Medit
{


    // Declarations in this first anonymous namespace; definitions (and comments) are in a second one at the end of
    // this file
    namespace // anonymous
    {


        //! Convenient alias.
        enum class Mode { read, write };

        //! Traits related to the mode enum. Only specialization matter!
        template<Mode ModeT>
        struct TraitsMode;


        template<>
        struct TraitsMode<Mode::read>
        {
            static const std::string& GetString();
        };


        template<>
        struct TraitsMode<Mode::write>
        {
            static const std::string& GetString();
        };


        /*!
         * \brief Performs checks upon path and extension before calling GmfOpenmesh.
         *
         * The reason is that error code returned by this command is not explicit at all; so if we
         * ensure path and extension are correct we may limit the reason of failure to inadequate
         * formatting of the file.
         *
         * \param[in] filename File to open.
         * \tparam mode Either "read" or "write".
         *
         */
        template<Mode ModeT>
        void CheckPath(const ::MoReFEM::FilesystemNS::File& filename,
                       const std::source_location location = std::source_location::current());


        /*!
         * \brief Abstract class from which MeditRead and MeditWrite are derived.
         */
        class Medit
        {
          public:
            //! Constructors.
            explicit Medit();
            explicit Medit(int version);

            //! Destructor.
            virtual ~Medit() = 0;

          protected:
            // Libmesh internal index of the mesh (required to destroy properly the mesh at the end).
            libmeshb_int mesh_index_{};

            // Libmesh file mesh version (1 to 3).
            int version_{};
        };


        class LabelHelper;


        /*
         * \brief Class to read the Medit file
         *
         * The purpose is to ensure there are no resource leaks: if an exception is thrown the libmesh object
         * will automatically be destroyed as the destructor will properly close the file
         *
         * Some attributes are references to the namesakes in 'Mesh' class: the point of current class is to fill
         * appropriately the 'Mesh' object from a Medit file.
         *
         */
        class MeditRead : public Medit
        {
          public:
            // \param[in] Nprocessor_wise_per_type See ReadGeometricElts().
            explicit MeditRead(::MoReFEM::MeshNS::unique_id mesh_id,
                               const ::MoReFEM::FilesystemNS::File& medit_filename,
                               const double space_unit,
                               std::optional<std::map<std::size_t, std::size_t>> Nprocessor_wise_per_type,
                               Coords::vector_shared_ptr& coords_list,
                               GeometricElt::vector_shared_ptr& processor_wise_geometric_elt_list,
                               GeometricElt::vector_shared_ptr& ghost_geometric_elt_list,
                               MeshLabel::vector_const_shared_ptr& label_list);

            // Destructor
            virtual ~MeditRead() override = default;

            //! Get dimension read;
            std::size_t GetDimension() const;

          private:
            // Read coords.
            void ReadCoords(const double space_unit, LabelHelper& label_helper);

            // Read geometric elements.
            // \param[in] Nprocessor_wise_per_type If defined, specify for each type the number of expected
            // processor wise elements for a given \a RefGeomElt. This relies upon the fact that by convention
            // processor-wise elements are written first; so we know that beyond the count given in second member
            // the \a GeometricElt should be considered as ghost. This is useful only when running from
            // prepartitioned data.
            void ReadGeometricElts(::MoReFEM::MeshNS::unique_id mesh_unique_id,
                                   std::optional<std::map<std::size_t, std::size_t>> Nprocessor_wise_per_type,
                                   LabelHelper& label_helper);

            //! Set the dimension.
            void SetDimension(std::size_t dimension);

            //! Get coords list.
            const Coords::vector_shared_ptr& GetCoordsList() const noexcept;

            // The file being read.
            const ::MoReFEM::FilesystemNS::File& GetFile() const noexcept;

          private:
            // The file being read.
            const ::MoReFEM::FilesystemNS::File& file_;

            // Dimension.
            std::size_t dimension_read_;

            //! List of all coords.
            Coords::vector_shared_ptr& coords_list_;

            //! List of all the processor-wise geometric elements.
            GeometricElt::vector_shared_ptr& processor_wise_geometric_elt_list_;

            //! List of all the ghost elements.
            GeometricElt::vector_shared_ptr& ghost_geometric_elt_list_;
        };


        /*
         * \brief Class to manage labels.
         *
         * In Medit files labels are integer index; this class is the bridge between those
         * indexes and the Label::shared_ptr objects.
         *
         */
        class LabelHelper
        {
          public:
            //! Constructor.
            explicit LabelHelper(::MoReFEM::MeshNS::unique_id mesh_id);

            //! Returns a Label object matching the index (if not existent create it on the fly)
            MeshLabel::const_shared_ptr FetchLabel(std::size_t index);

            //! Return number of labels
            std::size_t Nlabels() const;

            //! Obtain the list of all labels found
            void LabelList(MeshLabel::vector_const_shared_ptr& out) const;

          private:
            //! Unique id of the mesh for which the label are built.
            const ::MoReFEM::MeshNS::unique_id mesh_id_;

            //! Relationship between integer indexes (given as keys) and label objects
            std::unordered_map<std::size_t, MeshLabel::const_shared_ptr> objects_;
        };


        template<typename T, int DimensionT>
        void ReadCoordsHelper(libmeshb_int mesh_index,
                              std::size_t Ncoord,
                              Coords::vector_shared_ptr& coords_list,
                              const double space_unit,
                              LabelHelper& label_helper);


        class MeditWrite : public Medit
        {

          public:
            //! Constructor
            explicit MeditWrite(const ::MoReFEM::FilesystemNS::File& medit_filename,
                                int version,
                                int dimension,
                                const Coords::vector_shared_ptr& processor_wise_coords_list,
                                const Coords::vector_shared_ptr& ghost_coords_list,
                                const GeometricElt::vector_shared_ptr& geometric_elt_list);

            //@! Destructor
            virtual ~MeditWrite() override = default;
        };


    } // anonymous namespace


    void ReadFile(const ::MoReFEM::MeshNS::unique_id mesh_id,
                  const ::MoReFEM::FilesystemNS::File& file,
                  double space_unit,
                  std::optional<std::map<std::size_t, std::size_t>> Nprocessor_wise_geom_elt_per_type,
                  std::size_t& dimension,
                  GeometricElt::vector_shared_ptr& unsort_processor_wise_element_list,
                  GeometricElt::vector_shared_ptr& unsort_ghost_element_list,
                  Coords::vector_shared_ptr& coords_list,
                  MeshLabel::vector_const_shared_ptr& mesh_label_list)
    {
        MeditRead medit(mesh_id,
                        file,
                        space_unit,
                        Nprocessor_wise_geom_elt_per_type,
                        coords_list,
                        unsort_processor_wise_element_list,
                        unsort_ghost_element_list,
                        mesh_label_list);

        dimension = medit.GetDimension();
    }


    void WriteFile(const Mesh& mesh, const ::MoReFEM::FilesystemNS::File& mesh_file, int version)
    {
        // Ensure the geometric element list is ordered at Medit taste
        auto all_geom_elt_list = mesh.ComputeProcessorWiseAndGhostGeometricEltList();

        namespace sc = Advanced::GeometricEltNS::SortingCriterion;
        std::stable_sort(
            all_geom_elt_list.begin(), all_geom_elt_list.end(), Utilities::Sort<GeometricElt::shared_ptr, sc::Type<>>);

        MeditWrite(mesh_file,
                   version,
                   static_cast<int>(mesh.GetDimension()),
                   mesh.GetProcessorWiseCoordsList(),
                   mesh.GetGhostCoordsList(),
                   all_geom_elt_list);
    }


    // Definitions
    namespace // anonymous
    {


        const std::string& TraitsMode<Mode::read>::GetString()
        {
            static const std::string ret("read");
            return ret;
        }


        const std::string& TraitsMode<Mode::write>::GetString()
        {
            static const std::string ret("write");
            return ret;
        }


        namespace MeditExceptionNS = ::MoReFEM::ExceptionNS::Format::Medit;


        template<Mode ModeT>
        void CheckPath(const ::MoReFEM::FilesystemNS::File& file, const std::source_location location)
        {
            decltype(auto) file_extension{ file.Extension() };

            using traits = TraitsMode<ModeT>;

            if (file_extension != ".mesh" && file_extension != ".meshb")
                throw MeditExceptionNS::InvalidExtension(file, traits::GetString(), location);

            // In write mode, we don't care if the file already exists or not (it shouldn't in most cases this
            // method is called).
            if constexpr (ModeT == Mode::read)
            {
                if (!file.DoExist())
                    throw MeditExceptionNS::InvalidPath(file, traits::GetString(), location);
            }
        }


        LabelHelper::LabelHelper(const ::MoReFEM::MeshNS::unique_id mesh_id) : mesh_id_(mesh_id)
        {
            objects_.max_load_factor(Utilities::DefaultMaxLoadFactor());
        }


        MeshLabel::const_shared_ptr LabelHelper::FetchLabel(std::size_t index)
        {
            auto it = objects_.find(index);

            if (it != objects_.cend())
                return it->second;

            MeshLabel::const_shared_ptr new_surface = std::make_shared<MeshLabel>(mesh_id_, index, "");

            // Register the Label in the class
            objects_.insert(std::make_pair(index, new_surface));

            return new_surface;
        }


        void LabelHelper::LabelList(MeshLabel::vector_const_shared_ptr& out) const
        {
            assert(out.empty());
            out.reserve(objects_.size());
            for (const auto& labels : objects_)
                out.push_back(labels.second);
        }


        Medit::Medit()
        : mesh_index_(0), // init with status meaning an error; will be modified as soon as the file is read
          version_(0)     // init with dummy value
        { }

        Medit::Medit(int version)
        : mesh_index_(0), // init with status meaning an error; will be modified as soon as the file is read
          version_(version)
        { }


        MeditRead::MeditRead(const ::MoReFEM::MeshNS::unique_id mesh_id,
                             const ::MoReFEM::FilesystemNS::File& medit_filename,
                             const double space_unit,
                             std::optional<std::map<std::size_t, std::size_t>> Nprocessor_wise_per_type,
                             Coords::vector_shared_ptr& coords_list,
                             GeometricElt::vector_shared_ptr& processor_wise_geometric_elt_list,
                             GeometricElt::vector_shared_ptr& ghost_geometric_elt_list,
                             MeshLabel::vector_const_shared_ptr& label_list)
        : Medit(), file_(medit_filename), coords_list_(coords_list),
          processor_wise_geometric_elt_list_(processor_wise_geometric_elt_list),
          ghost_geometric_elt_list_(ghost_geometric_elt_list)
        {
            {
                // Open the file
                int dimension_as_int;

                CheckPath<Mode::read>(medit_filename);

                auto filename_as_string = static_cast<std::string>(medit_filename);

                std::vector<char> buf(filename_as_string.size() + 1ul);
                filename_as_string.copy(buf.data(), filename_as_string.size());
                buf[filename_as_string.size()] = '\0';

                mesh_index_ =
                    GmfOpenMesh(const_cast<char*>(filename_as_string.c_str()), GmfRead, &version_, &dimension_as_int);

                SetDimension(static_cast<std::size_t>(dimension_as_int));

                if (mesh_index_ == 0)
                    throw MeditExceptionNS::UnableToOpen(medit_filename, version_, "read");

                if (dimension_as_int < 2 || dimension_as_int > 3)
                    throw MeditExceptionNS::InvalidDimension(medit_filename, dimension_as_int);

                std::cout << "Reading from file " << medit_filename << std::endl;
                std::cout << "InputMesh : idx = " << mesh_index_ << ", version = " << version_
                          << ", dimension = " << dimension_as_int << "." << std::endl;

                if (version_ == 1)
                {
                    std::cout
                        << "\n[WARNING] Medit mesh displays a version '1', i.e. that single precision is intended "
                           "to be used within libmesh library. MoReFEM only sports double precision for coords, so "
                           "you should "
                           "probably modify the version in the mesh file to '2' or '3'."
                        << std::endl
                        << std::endl;
                }
            }

            // Local object that won't survive beyond constructor.
            LabelHelper label_helper(mesh_id);

            // Read coords.
            ReadCoords(space_unit, label_helper);

            // Read geometric elements.
            ReadGeometricElts(mesh_id, Nprocessor_wise_per_type, label_helper);

            // Fill label list.
            label_helper.LabelList(label_list);
        }


        Medit::~Medit()
        {
            if (mesh_index_)
                GmfCloseMesh(mesh_index_);
        }


        /*!
         * \brief Helper to read coords
         *
         * The point is to write the more concisely possible the possible choices (dim = 2 or 3)
         * and whether float or double should be called in libmesh interface.
         *
         * \tparam T Float if version 1 used to create the mesh file being read, double otherwise
         * \tparam DimensionT 2 or 3
         */

        template<typename T, int DimensionT>
        void ReadCoordsHelper(libmeshb_int mesh_index,
                              std::size_t Ncoord,
                              Coords::vector_shared_ptr& coords_list,
                              const double space_unit,
                              LabelHelper& label_helper)
        {
            std::array<T, 3> array;
            int label = -1;

            assert(coords_list.empty());
            coords_list.reserve(Ncoord);

            // Medit convention is to numerate from 1 to Ncoord; I follow it there.
            for (auto i{ 1u }; i <= Ncoord; ++i)
            {
                if (DimensionT == 2) // compile-time decision
                {
                    GmfGetLin(mesh_index, GmfVertices, &array[0], &array[1], &label);
                    array[2] = 0.;
                } else
                    GmfGetLin(mesh_index, GmfVertices, &array[0], &array[1], &array[2], &label);

                assert("Should be positive or null!" && label != -1);

                auto&& coord_ptr = Internal::CoordsNS::Factory::FromArray(array, space_unit);
                Coords& coord = *coord_ptr;
                coord.SetIndexFromMeshFile(::MoReFEM::CoordsNS::index_from_mesh_file(i));

                coord.SetMeshLabel(label_helper.FetchLabel(static_cast<std::size_t>(label)));
                coords_list.emplace_back(std::move(coord_ptr));
            }

            std::cout << "Number of labels read in coords = " << label_helper.Nlabels() << std::endl;
        }


        void MeditRead::ReadCoords(const double space_unit, LabelHelper& label_helper)
        {
            // Determine first the number of coords
            assert(coords_list_.empty());
            auto Ncoord = static_cast<std::size_t>(GmfStatKwd(mesh_index_, GmfVertices));

            GmfGotoKwd(mesh_index_, GmfVertices);

            const auto dimension = GetDimension();

            switch (version_)
            {
            case 1: // float used in the file
                if (dimension == 2)
                    ReadCoordsHelper<float, 2>(mesh_index_, Ncoord, coords_list_, space_unit, label_helper);
                else // dimension 3; other options already ruled out by previous check.
                    ReadCoordsHelper<float, 3>(mesh_index_, Ncoord, coords_list_, space_unit, label_helper);
                break;
            case 2: // double used in the file
            case 3:
                if (dimension == 2)
                    ReadCoordsHelper<double, 2>(mesh_index_, Ncoord, coords_list_, space_unit, label_helper);
                else // dimension 3; other options already ruled out by previous check.
                    ReadCoordsHelper<double, 3>(mesh_index_, Ncoord, coords_list_, space_unit, label_helper);
                break;
            default:
                throw MoReFEM::Exception("Libmesh file version should be 1, 2 or 3");
            }
        }


        std::size_t MeditRead::GetDimension() const
        {
            return dimension_read_;
        }


        void MeditRead::SetDimension(std::size_t dimension)
        {
            dimension_read_ = dimension;
        }


        // \param[in] Ngeom_elt_in_mesh Number of geometric elements of the chosen code found in the mesh.
        std::size_t NprocessorWiseGeomElt(std::size_t medit_code,
                                          const std::map<std::size_t, std::size_t>& Nprocessor_wise_per_type,
                                          std::size_t Ngeom_elt_in_mesh)
        {
            const auto it = Nprocessor_wise_per_type.find(medit_code);

            if (it == Nprocessor_wise_per_type.cend())
                return 0ul;

            std::size_t ret;

            ret = it->second;
            if (ret > Ngeom_elt_in_mesh)
            {
                std::ostringstream oconv;
                oconv << "Invalid object Nprocessor_wise_per_type: for geometric "
                         "elements with Medit code "
                      << medit_code
                      << " (look at GmfKwdCod enum) there are more expected "
                         "processor-wise elements that there are such elements in the mesh. This highlights "
                         "either a bug in the code or the fact you are not using the proper set of prepartitioned "
                         "data to run your model.";
                throw Exception(oconv.str());
            }

            return ret;
        }


        const ::MoReFEM::FilesystemNS::File& MeditRead::GetFile() const noexcept
        {
            return file_;
        }


        void MeditRead::ReadGeometricElts(const ::MoReFEM::MeshNS::unique_id mesh_unique_id,
                                          std::optional<std::map<std::size_t, std::size_t>> Nprocessor_wise_per_type,
                                          LabelHelper& label_helper)
        {
            // Iterate through all registered geometric elements
            const auto& geometric_elt_factory = ::MoReFEM::Advanced::GeometricEltFactory::CreateOrGetInstance();
            const auto& medit_type_list = geometric_elt_factory.MeditRefGeomEltList();

            auto Ncoord = static_cast<std::size_t>(GmfStatKwd(mesh_index_, GmfVertices));

            ::MoReFEM::GeomEltNS::index_type geometric_elt_index{
                1ul
            }; // Medit convention is to make indexes begin at 1.

            for (const auto& [type_code, createGeometricEltFunction] : medit_type_list)
            {
                GmfGotoKwd(mesh_index_, type_code);

                // Check whether there are geometric elements of this kind in the file
                auto number = static_cast<std::size_t>(GmfStatKwd(mesh_index_, type_code));
                if (number == 0)
                    continue;

                auto Nprocessor_wise = number;

                if (Nprocessor_wise_per_type)
                    Nprocessor_wise = NprocessorWiseGeomElt(type_code, Nprocessor_wise_per_type.value(), number);

                for (auto index_geom_elt = 0ul; index_geom_elt < number; ++index_geom_elt)
                {
                    GeometricElt::shared_ptr new_geometric_elt_ptr(createGeometricEltFunction(mesh_unique_id));
                    GeometricElt& new_geometric_element = *new_geometric_elt_ptr;

                    int label_int;
                    new_geometric_element.ReadMeditFormat(GetFile(), GetCoordsList(), mesh_index_, Ncoord, label_int);

                    auto label = label_helper.FetchLabel(static_cast<std::size_t>(label_int));

                    new_geometric_element.SetMeshLabel(label);
                    new_geometric_element.SetIndex(geometric_elt_index++);

                    if (index_geom_elt < Nprocessor_wise)
                        processor_wise_geometric_elt_list_.emplace_back(new_geometric_elt_ptr);
                    else
                        ghost_geometric_elt_list_.emplace_back(new_geometric_elt_ptr);
                }
            }
        }


        const Coords::vector_shared_ptr& MeditRead::GetCoordsList() const noexcept
        {
            return coords_list_;
        }


        MeditWrite::MeditWrite(const ::MoReFEM::FilesystemNS::File& medit_filename,
                               int version,
                               int dimension,
                               const Coords::vector_shared_ptr& processor_wise_coords_list,
                               const Coords::vector_shared_ptr& ghost_coords_list,
                               const GeometricElt::vector_shared_ptr& geometric_elt_list)
        : Medit(version)
        {
            const int medit_dimension = dimension > 2 ? dimension : 2;

            if (medit_dimension < 2 || medit_dimension > 3)
                throw MeditExceptionNS::InvalidDimension(medit_filename, medit_dimension);

            {
                // ====================
                // Open the file
                // ====================
                CheckPath<Mode::write>(medit_filename);

                auto filename_as_string = static_cast<std::string>(medit_filename);

                mesh_index_ =
                    GmfOpenMesh(const_cast<char*>(filename_as_string.c_str()), GmfWrite, version, medit_dimension);

                if (mesh_index_ == 0)
                    throw MeditExceptionNS::UnableToOpen(medit_filename, version_, "write");
            }

            auto Nprocessor_wise_coord = processor_wise_coords_list.size();
            auto Nghost_coord = ghost_coords_list.size();

            {
                // ====================
                // Write the coords
                // ====================


                GmfSetKwd(mesh_index_, GmfVertices, static_cast<libmeshb_int>(Nprocessor_wise_coord + Nghost_coord));

                if (version == 1)
                {
                    for (const auto& coord_ptr : processor_wise_coords_list)
                        WriteMeditFormat<float>(static_cast<std::size_t>(dimension), *coord_ptr, mesh_index_);

                    for (const auto& coord_ptr : ghost_coords_list)
                        WriteMeditFormat<float>(static_cast<std::size_t>(dimension), *coord_ptr, mesh_index_);
                } else if (version == 2 || version == 3)
                {
                    for (const auto& coord_ptr : processor_wise_coords_list)
                        WriteMeditFormat<double>(static_cast<std::size_t>(dimension), *coord_ptr, mesh_index_);

                    for (const auto& coord_ptr : ghost_coords_list)
                        WriteMeditFormat<double>(static_cast<std::size_t>(dimension), *coord_ptr, mesh_index_);
                } else
                    assert("Libmesh file version should be 1, 2 or 3" && false);
            }

            {
                // ====================
                // Write the geometric elements
                // ====================

                if (geometric_elt_list.empty())
                    return;

                // Use a map to denombrate how many geometric elements of each type is present.
                std::map<GmfKwdCod, GeometricElt::vector_shared_ptr> geometric_elt_by_type;

                for (auto geometric_elt_ptr_ : geometric_elt_list)
                {
                    auto geometric_elt_code = geometric_elt_ptr_->GetMeditIdentifier();

                    geometric_elt_by_type[geometric_elt_code].push_back(geometric_elt_ptr_);
                }

                // In parallel case, we need to reindex the Coords: Medit principle for GeometricElt is to
                // design the Coords with an index from 1 to Ncoords, where this index is the position in the list
                // of Coords written just above.
                // In sequential it should be completely harmless; check it with an assert.

                std::unordered_map<::MoReFEM::CoordsNS::index_from_mesh_file, int> reindexing;
                {
                    reindexing.max_load_factor(Utilities::DefaultMaxLoadFactor());
                    reindexing.reserve(Nprocessor_wise_coord + Nghost_coord);
                }

                for (std::size_t i = 0; i < Nprocessor_wise_coord; ++i)
                    reindexing.insert(
                        std::make_pair(processor_wise_coords_list[i]->GetIndexFromMeshFile(), static_cast<int>(i) + 1));

                for (std::size_t i = 0; i < Nghost_coord; ++i)
                    reindexing.insert(std::make_pair(ghost_coords_list[i]->GetIndexFromMeshFile(),
                                                     static_cast<int>(i + Nprocessor_wise_coord) + 1));

                assert(reindexing.size() == Nprocessor_wise_coord + Nghost_coord);

                // Now we can write into the file with all information from the map
                for (const auto& block : geometric_elt_by_type)
                {
                    const auto& geometric_elt_code = block.first;           // alias
                    const auto& geometric_elt_per_type_list = block.second; // alias
                    auto Ngeometric_elements = geometric_elt_per_type_list.size();

                    GmfSetKwd(mesh_index_, geometric_elt_code, static_cast<int>(Ngeometric_elements));

                    for (const auto& geometric_elt_ptr : geometric_elt_per_type_list)
                        geometric_elt_ptr->WriteMeditFormat(mesh_index_, reindexing);
                }
            }
        }


        std::size_t LabelHelper::Nlabels() const
        {
            return static_cast<std::size_t>(objects_.size());
        }


    } // anonymous namespace


} // namespace MoReFEM::Internal::MeshNS::FormatNS::Medit


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
