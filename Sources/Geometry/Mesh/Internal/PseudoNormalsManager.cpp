// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <algorithm>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <vector>

#include "Geometry/Domain/Domain.hpp"
#include "Geometry/Domain/DomainManager.hpp"
#include "Geometry/Mesh/Internal/PseudoNormalsManager.hpp"
#include "Geometry/Mesh/Mesh.hpp"


namespace MoReFEM::Internal
{


    PseudoNormalsManager::~PseudoNormalsManager() = default;


    const std::string& PseudoNormalsManager::ClassName()
    {
        static const std::string ret("PseudoNormalsManager");
        return ret;
    }


    PseudoNormalsManager::PseudoNormalsManager() = default;


    void PseudoNormalsManager::Create(const std::vector<::MoReFEM::DomainNS::unique_id>& domain_index_list, Mesh& mesh)
    {
        std::cout << "===========================================" << std::endl;
        std::cout << "[WARNING] Pseudo-normals computation." << std::endl;
        std::cout << "For now the computation is limited to Triangle3 and the corresponding Edges and Vertices."
                  << std::endl;
        std::cout << "In the case of a volumic mesh the orientation of the triangles with respect to the volume is "
                     "tested and in this case all the triangles must be defined with an outside oriented normal."
                  << std::endl;

        const auto& domain_manager = DomainManager::GetInstance();

        std::vector<std::size_t> label_list_index;

        // If domain_index_list is empty no restriction applied on domain.
        if (domain_index_list.empty())
        {
            const auto& mesh_label_list = mesh.GetLabelList();

            const std::size_t mesh_label_list_size = mesh_label_list.size();

            assert(std::none_of(
                mesh_label_list.cbegin(), mesh_label_list.cend(), Utilities::IsNullptr<MeshLabel::const_shared_ptr>));

            for (std::size_t i = 0ul; i < mesh_label_list_size; ++i)
            {
                const auto& mesh_label = *mesh_label_list[i];
                label_list_index.push_back(mesh_label.GetIndex());
            }
        } else
        {
            for (const auto domain_index : domain_index_list)
            {
                const auto& mesh_label_list = domain_manager.GetDomain(domain_index).GetMeshLabelList();

                assert(std::none_of(mesh_label_list.cbegin(),
                                    mesh_label_list.cend(),
                                    Utilities::IsNullptr<MeshLabel::const_shared_ptr>));

                const std::size_t mesh_label_list_size = mesh_label_list.size();

                for (std::size_t j = 0ul; j < mesh_label_list_size; ++j)
                {
                    const auto& mesh_label = *mesh_label_list[j];
                    label_list_index.push_back(mesh_label.GetIndex());
                }
            }
        }

        // mesh.ComputePseudoNormals(label_list_index); //#938 deactivated for the moment.

        std::cout << "===========================================" << std::endl;
    }


    void PseudoNormalsManager::Clear()
    { }


} // namespace MoReFEM::Internal


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
