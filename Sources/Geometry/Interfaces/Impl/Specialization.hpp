// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_IMPL_SPECIALIZATION_DOT_HPP_
#define MOREFEM_GEOMETRY_INTERFACES_IMPL_SPECIALIZATION_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Numeric/Numeric.hpp"                             // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Addable.hpp"              // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/AsMpiDatatype.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"           // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp" // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Hashable.hpp"             // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Printable.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/StrongType.hpp"                  // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Interface; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::InterfaceNS
{


    /*!
     * \brief Functor to act as hash function in an unordered map.
     *
     * The has index is generated from both the nature and the index of the interface.
     */
    struct Hash
    {


        //! Operator() to provide the hash function.
        //! \param[in] interface \a Interface for which the hash is computed.
        std::size_t operator()(const Interface* const interface) const;
    };


    /*!
     * \brief Functor used to identify already existing interfaces when a new interface is built.
     *
     *
     * As a new interface is not yet properly indexed at this point, comparison is performed upon the coords list
     * (which is less efficient so that's why standard operator< is not implemented this way).
     *
     * Due to its expected usage, nullptr is not expected to be one of the arguments of the functor.
     *
     * Likewise, it is expected to be used to compare two interfaces of the same nature; an assert
     * checks that in debug.
     */
    struct LessByCoords
    {


        //! \copydoc doxygen_hide_operator_equal
        bool operator()(const Interface::shared_ptr& lhs, const Interface::shared_ptr& rhs) const;
    };


} // namespace MoReFEM::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_IMPL_SPECIALIZATION_DOT_HPP_
// *** MoReFEM end header guards *** < //
