// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_INSTANCES_EDGE_DOT_HPP_
#define MOREFEM_GEOMETRY_INTERFACES_INSTANCES_EDGE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>
#include <memory>
#include <vector>

#include "Utilities/Containers/PointerComparison.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/Interfaces/Internal/TInterface.hpp" // IWYU pragma: export


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class Mesh; }
namespace MoReFEM { class GeometricElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================

namespace MoReFEM
{


    /*!
     * \brief Class in charge of the edge interface.
     *
     */
    class Edge final : public Internal::InterfaceNS::TInterface<Edge, InterfaceNS::Nature::edge>
    {

      public:
        //! Alias over shared_ptr.
        using shared_ptr = std::shared_ptr<Edge>;

        //! Alias over vector of shared_ptr.
        using vector_shared_ptr = std::vector<shared_ptr>;


        /*!
         * \brief Typedef useful when the vertices are built.
         *
         * When a new Vertex is built, index is not yet associated to it; one must determine first whether
         * it has already been built by another GeometricElt. The type below is used to list all those
         * already built.
         */
        using InterfaceMap = Utilities::PointerComparison::
            Map<Edge::shared_ptr, std::vector<const GeometricElt*>, InterfaceNS::LessByCoords>;

        //! Friendship.
        friend Mesh;

        //! Name of the class.
        static const std::string& ClassName();


      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor.
         *
         * \tparam LocalEdgeContentT Type of the container or structure that represents the interface in the
         * local topology. It is given by TopologyT::EdgeContent; where TopologyT is the topology of the GeometricElt
         * for which the Interface is built.
         *
         * \param[in] local_edge_content Integer values that represent the interface within the local topology.
         * Is is one of the element of TopologyT::GetEdgeList() (see above for the
         * meaning of TopologyT).
         * \param[in] coords_list_in_geom_elt List of Coords objects within the GeometricElt for which the interface
         * is built.
         */
        template<class LocalEdgeContentT>
        explicit Edge(const LocalEdgeContentT& local_edge_content,
                      const Coords::vector_shared_ptr& coords_list_in_geom_elt);

        //! Destructor.
        ~Edge() override;

        //! \copydoc doxygen_hide_copy_constructor
        Edge(const Edge& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Edge(Edge&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Edge& operator=(const Edge& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Edge& operator=(Edge&& rhs) = delete;


        ///@}

        //! Number of pseudo normals for edges computed.
        static std::size_t NumberOfEdgesPseudoNormalComputed();

      private:
        /*!
         * \brief Compute the pseudo-normal of an edge.
         *
         * \param[in] geom_elt_list List of the geometric elements that share this edge.
         *
         * \warning Should not be called directly. To compute pseudo-normals use PseudoNormals1 in Lua that will use
         * the dedicated magager to compute them.
         *
         */
        void ComputePseudoNormal(const std::vector<const GeometricElt*>& geom_elt_list);

      private:
        //! Counter for pseudo-normal edges computation.
        static std::size_t counter_pseudo_normal_edge_;
    };

} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Interfaces/Instances/Edge.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_INSTANCES_EDGE_DOT_HPP_
// *** MoReFEM end header guards *** < //
