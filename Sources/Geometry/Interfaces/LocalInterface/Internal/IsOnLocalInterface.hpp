// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERFACES_LOCALINTERFACE_INTERNAL_ISONLOCALINTERFACE_DOT_HPP_
#define MOREFEM_GEOMETRY_INTERFACES_LOCALINTERFACE_INTERNAL_ISONLOCALINTERFACE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Geometry/Coords/LocalCoords.hpp"
#include "Geometry/Interfaces/Advanced/LocalData.hpp"


namespace MoReFEM::Internal::InterfaceNS
{


    /*!
     * \brief Whether the \a coords are on the vertex_index -th vertex.
     *
     * \tparam TopologyT Topology considered.
     *
     * \param[in] local_vertex_index Index of the vertex considered; must be in [0, TopologyT::Nvertex[.
     * \param[in] local_coords \a LocalCoords which position is under scrutiny.
     *
     * \return True of the \a LocalCoords is on the vertex pointed by \a local_vertex_index.
     */
    template<::MoReFEM::Concept::TopologyTraitsClass TopologyT>
    bool IsOnVertex(std::size_t local_vertex_index, const LocalCoords& local_coords);


    /*!
     * \brief Whether the \a coords are on the edge_index -th edge for a segment, quadrangle or hexahedron.
     *
     * \tparam TopologyT Topology of a spectral based element (i.e. segment, quadrangle or hexahedron).
     *
     * \param[in] local_edge_index Index of the edge considered; must be in [0, TopologyT::Nedge[.
     * \param[in] local_coords \a LocalCoords which position is under scrutiny.
     *
     * \return True of the \a LocalCoords is on the edge pointed by \a local_edge_index.
     * Coords of a vertex on the edge would also returns true here.
     */
    template<::MoReFEM::Concept::TopologyTraitsClass TopologyT>
    bool IsOnEdge_Spectral(std::size_t local_edge_index, const LocalCoords& local_coords);

} // namespace MoReFEM::Internal::InterfaceNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Interfaces/LocalInterface/Internal/IsOnLocalInterface.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERFACES_LOCALINTERFACE_INTERNAL_ISONLOCALINTERFACE_DOT_HPP_
// *** MoReFEM end header guards *** < //
