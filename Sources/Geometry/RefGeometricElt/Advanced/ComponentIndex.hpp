// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_ADVANCED_COMPONENTINDEX_DOT_HPP_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_ADVANCED_COMPONENTINDEX_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Utilities/Type/StrongType/Skills/Addable.hpp"              // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Comparable.hpp"           // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/DefaultConstructible.hpp" // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Divisible.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Incrementable.hpp"        // IWYU pragma: export
#include "Utilities/Type/StrongType/Skills/Printable.hpp"            // IWYU pragma: export
#include "Utilities/Type/StrongType/StrongType.hpp"                  // IWYU pragma: export


namespace MoReFEM::Advanced::ComponentNS
{

    //! Strong type for \a Component index.
    // clang-format off
    using index_type =
        StrongType
        <
            std::size_t,
            struct index_type_tag,
            StrongTypeNS::Comparable,
            StrongTypeNS::Printable,
            StrongTypeNS::Addable,
            StrongTypeNS::Incrementable,
            StrongTypeNS::DefaultConstructible
        >;

    // clang-format on


} // namespace MoReFEM::Advanced::ComponentNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_ADVANCED_COMPONENTINDEX_DOT_HPP_
// *** MoReFEM end header guards *** < //
