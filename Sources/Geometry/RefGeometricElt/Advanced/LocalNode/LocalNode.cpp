// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <iostream>
#include <type_traits> // IWYU pragma: keep
#include <utility>

#include "Geometry/RefGeometricElt/Advanced/LocalNode/LocalNode.hpp"


namespace MoReFEM::Advanced
{


    LocalNode::LocalNode(Advanced::InterfaceNS::LocalInterface&& local_interface,
                         LocalNodeNS::index_type index,
                         const LocalCoords& local_coords)
    : local_interface_(std::move(local_interface)), index_(index), local_coords_(local_coords)
    { }


    void LocalNode::Print(std::ostream& out) const
    {
        out << "LocalNode " << GetIndex() << " [" << GetLocalInterface().GetNature() << "]";
    }


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
