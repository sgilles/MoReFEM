// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_TRIANGLE_TOPOLOGY_TRIANGLE_DOT_HPP_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_TRIANGLE_TOPOLOGY_TRIANGLE_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep
#include <iosfwd>  // IWYU pragma: keep
// IWYU pragma: no_include <string>

#include "Geometry/RefGeometricElt/Advanced/Topology/Concept.hpp"
#include "Geometry/RefGeometricElt/Instances/Segment/Topology/Segment.hpp"
#include "Geometry/RefGeometricElt/Instances/TopologyCommon.hpp"


namespace MoReFEM::RefGeomEltNS::TopologyNS
{


    /*!
     * \brief Topology of a Triangle.
     */
    class Triangle final
    {

      public:
        //! Data.
        enum : std::size_t {
            dimension = 2u,
            Nvertex = 3u,
            Nedge = 3u,
            Nface = 1u,
            Nvolume = 0ul,
        };


        //! Topology of an edge.
        using EdgeTopology = Segment;

        //! Topology of a face.
        using FaceTopology = Triangle;

        //! Container used to store all the points of an edge. The index of the points are actually stored.
        using EdgeContent = std::array<std::size_t, EdgeTopology::Nvertex>;

        //! Container used to store all the points of a face. The index of the points are actually stored.
        using FaceContent = std::array<std::size_t, FaceTopology::Nvertex>;

        //! Container used to store the local coordinates of the vertices.
        using LocalCoordListType = std::vector<LocalCoords>;

        /*!
         * \brief Interface in which interior dofs (i.e. those without continuity with neighbor elements) are
         * stored.
         */
        static InterfaceNS::Nature GetInteriorInterface();

        //! Returns the enum that tags the topology.
        static ::MoReFEM::TopologyNS::Type GetType() noexcept;


      public:
        /// \name Special members.
        ///@{

        //! Constructor.
        explicit Triangle() = default;

      protected:
        //! Destructor.
        ~Triangle() = default;

        //! \copydoc doxygen_hide_copy_constructor
        Triangle(const Triangle& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Triangle(Triangle&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Triangle& operator=(const Triangle& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Triangle& operator=(Triangle&& rhs) = delete;

        ///@}

      public:
        //! Return the name of the topology.
        static const std::string& ClassName();

        /*!
         * \copydoc doxygen_hide_is_on_local_vertex
         */
        static bool IsOnVertex(std::size_t local_vertex_index, const LocalCoords& local_coords);

        /*!
         * \copydoc doxygen_hide_is_on_local_edge
         */
        static bool IsOnEdge(std::size_t local_edge_index, const LocalCoords& local_coords);

        /*!
         * \copydoc doxygen_hide_is_on_local_face
         */
        static bool IsOnFace(std::size_t local_face_index, const LocalCoords& local_coords);

        //! \copydoc doxygen_hide_transform_face_point
        static LocalCoords
        TransformFacePoint(const LocalCoords& coords, std::size_t face_index, std::size_t orientation);


        //! Return the local coordinates of the vertices.
        static const LocalCoordListType& GetQ1LocalCoordsList();

      private:
        //! Whether a \a local_coords is inside the reference triangle or not.
        //! \param[in] local_coords \a LocalCoords which status related to the \a RefGeomElt is sought.
        static bool IsInside(const LocalCoords& local_coords);

        //! Return the list of edges.
        static const std::array<EdgeContent, Nedge>& GetEdgeList();

        //! Return the list of faces.
        static const std::array<FaceContent, Nface>& GetFaceList();


        /*!
         * \brief Friendship to LocalData.
         *
         * As you can see above, list of local edges, faces and list of vertex coordinates are private
         * members; to access them you must use the LocalData class.
         *
         * For instance:
         *
         * \code
         * const auto& topology_face =
         * RefGeomEltNS::TopologyNS::LocalData<TopologyT>::GetFace(local_face_index); \endcode
         *
         */
        template<::MoReFEM::Concept::TopologyTraitsClass TopologyT>
        friend struct Advanced::InterfaceNS::LocalData;
    };


} // namespace MoReFEM::RefGeomEltNS::TopologyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_TRIANGLE_TOPOLOGY_TRIANGLE_DOT_HPP_
// *** MoReFEM end header guards *** < //
