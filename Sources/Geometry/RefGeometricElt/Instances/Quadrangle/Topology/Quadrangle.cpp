// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef>     // IWYU pragma: keep
#include <type_traits> // IWYU pragma: keep

#include "Geometry/RefGeometricElt/Instances/Quadrangle/Topology/Quadrangle.hpp"
#include "Geometry/RefGeometricElt/Instances/TopologyFwd.hpp"


namespace MoReFEM::RefGeomEltNS::TopologyNS
{


    const std::string& Quadrangle::ClassName()
    {
        static const std::string ret("Quadrangle");
        return ret;
    }


    /************************************************************************
     *
     *     3-----------2
     *     |           |
     *     |           |
     *     |           |
     *     |           |
     *     |           |
     *     0-----------1
     *
     *************************************************************************/


    const std::array<Quadrangle::EdgeContent, Quadrangle::Nedge>& Quadrangle::GetEdgeList()
    {
        static std::array<Quadrangle::EdgeContent, Quadrangle::Nedge> ret{
            { { { 0ul, 1u } }, { { 1u, 2u } }, { { 3u, 2u } }, { { 0ul, 3u } } }
        };

        return ret;
    }


    const std::array<Quadrangle::FaceContent, Quadrangle::Nface>& Quadrangle::GetFaceList()
    {
        static std::array<Quadrangle::FaceContent, Quadrangle::Nface> ret{ { { { 0ul, 1u, 2u, 3u } } } };

        return ret;
    }


    const std::vector<LocalCoords>& Quadrangle::GetQ1LocalCoordsList()
    {
        static std::vector<LocalCoords> ret{ { -1., -1. }, { 1., -1. }, { 1., 1. }, { -1., 1. } };

        return ret;
    }


    bool Quadrangle::IsOnVertex(std::size_t vertex_index, const LocalCoords& coords)
    {
        return Internal::InterfaceNS::IsOnVertex<Quadrangle>(vertex_index, coords);
    }


    bool Quadrangle::IsOnEdge(std::size_t edge_index, const LocalCoords& coords)
    {
        return Internal::InterfaceNS::IsOnEdge_Spectral<Quadrangle>(edge_index, coords);
    }


    bool Quadrangle::IsOnFace(std::size_t face_index, const LocalCoords& coords)
    {
        assert(face_index < Nface);
        assert(face_index == 0ul);
        static_cast<void>(face_index);

        const auto Ncomponent = dimension;

        const auto& local_coords_list = GetQ1LocalCoordsList();

        for (auto component = 0ul; component < Ncomponent; ++component)
        {
            // Take two diagonally opposed vertices to check whether the point is inside or not.
            const double min = std::min(local_coords_list[0][component], local_coords_list[2][component]);
            const double max = std::max(local_coords_list[0][component], local_coords_list[2][component]);

            // Check whether the coordinates in between vertices.
            if (coords[component] < min || coords[component] > max)
                return false;
        }


        return true;
    }


    LocalCoords
    Quadrangle::TransformFacePoint(const LocalCoords& coords, std::size_t face_index, std::size_t orientation)
    {
        assert(IsOnFace(face_index, coords));
        assert((orientation < 8u));
        assert(face_index == 0ul && "Only choice for a 2D quadrangle...");
        static_cast<void>(face_index); // to avoid warning in release mode.

        double x_before = coords[0];
        double y_before = coords[1];

        assert(!NumericNS::AreEqual(x_before, std::numeric_limits<double>::lowest()));
        assert(!NumericNS::AreEqual(y_before, std::numeric_limits<double>::lowest()));

        double x_after = std::numeric_limits<double>::lowest();
        double y_after = std::numeric_limits<double>::lowest();


        // Switch to the reference quadrangle [0,1]^2 (due to the history of the code ie Ondomatic...). #896.
        x_before = 0.5 * (x_before + 1.);
        y_before = 0.5 * (y_before + 1.);

        switch (orientation)
        {
        case 0ul:
        {
            x_after = x_before;
            y_after = y_before;
            break;
        }
        case 1u:
        {
            x_after = 1. - y_before;
            y_after = x_before;
            break;
        }
        case 2u:
        {
            x_after = 1. - x_before;
            y_after = 1. - y_before;
            break;
        }
        case 3u:
        {
            x_after = y_before;
            y_after = 1. - x_before;
            break;
        }
        case 4u:
        {
            x_after = y_before;
            y_after = x_before;
            break;
        }
        case 5u:
        {
            x_after = 1. - x_before;
            y_after = y_before;
            break;
        }
        case 6u:
        {
            x_after = 1. - y_before;
            y_after = 1. - x_before;

            break;
        }
        case 7u:
        {
            x_after = x_before;
            y_after = 1. - y_before;
            break;
        }
        }

        // Switch back to [-1,1]^2. #896.
        x_after = 2. * x_after - 1.;
        y_after = 2. * y_after - 1.;

        assert(!NumericNS::AreEqual(x_after, std::numeric_limits<double>::lowest()));
        assert(!NumericNS::AreEqual(y_after, std::numeric_limits<double>::lowest()));

        LocalCoords ret({ x_after, y_after });
        return ret;
    }


    InterfaceNS::Nature Quadrangle::GetInteriorInterface()
    {
        return InterfaceNS::Nature::face;
    }


    ::MoReFEM::TopologyNS::Type Quadrangle::GetType() noexcept
    {
        return ::MoReFEM::TopologyNS::Type::quadrangle;
    }


    bool Quadrangle::IsInside(const LocalCoords& coords)
    {
        const auto& coords_list = GetQ1LocalCoordsList();
        assert(coords_list.size() == 4ul);

        const auto r = coords.r();
        const auto s = coords.s();

        {
            const auto& bottom_left = coords_list[0];

            if (r < bottom_left.r() || s < bottom_left.s())
                return false;
        }

        {
            const auto& top_right = coords_list[2];

            if (r > top_right.r() || s > top_right.s())
                return false;
        }

        return true;
    }


} // namespace MoReFEM::RefGeomEltNS::TopologyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
