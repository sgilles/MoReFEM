// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep

#include "Geometry/RefGeometricElt/Instances/Segment/Topology/Segment.hpp"
#include "Geometry/RefGeometricElt/Instances/TopologyFwd.hpp"


namespace MoReFEM::RefGeomEltNS::TopologyNS
{


    const std::string& Segment::ClassName()
    {
        static const std::string ret("Segment");
        return ret;
    }


    /************************************************************************
     *   0-----------1         *
     *************************************************************************/


    const std::array<Segment::EdgeContent, Segment::Nedge>& Segment::GetEdgeList()
    {
        static std::array<Segment::EdgeContent, Segment::Nedge> ret{ { { { 0ul, 1u } } } };

        return ret;
    }


    const std::vector<Segment::FaceContent>& Segment::GetFaceList()
    {
        static std::vector<Segment::FaceContent> empty;
        return empty;
    }


    const std::vector<LocalCoords>& Segment::GetQ1LocalCoordsList()
    {
        static std::vector<LocalCoords> ret{ { -1. }, { 1. } };

        return ret;
    }


    bool Segment::IsOnVertex(std::size_t vertex_index, const LocalCoords& coords)
    {
        return Internal::InterfaceNS::IsOnVertex<Segment>(vertex_index, coords);
    }


    bool Segment::IsOnEdge(std::size_t edge_index, const LocalCoords& coords)
    {
        assert(edge_index == 0ul); // the only relevant value for a \a Segment...
        static_cast<void>(edge_index);

        static_assert(dimension == 1u);

        assert(coords.GetDimension() == dimension);
        const auto& local_coords_list = GetQ1LocalCoordsList();
        assert(local_coords_list.size() == 2ul);

        const double min = std::min(local_coords_list[0][0], local_coords_list[1][0]);
        const double max = std::max(local_coords_list[0][0], local_coords_list[1][0]);

        // Check whether the coordinates in between vertices.
        if (coords[0] < min || coords[0] > max)
            return false;

        return true;
    }


    [[noreturn]] bool Segment::IsOnFace(std::size_t face_index, const LocalCoords& coords)
    {
        static_cast<void>(face_index);
        static_cast<void>(coords);
        assert(false);
        throw; // to avoid warning.
    }


    [[noreturn]] LocalCoords
    Segment::TransformFacePoint(const LocalCoords& coords, std::size_t face_index, std::size_t orientation)
    {
        static_cast<void>(face_index);
        static_cast<void>(coords);
        static_cast<void>(orientation);

        assert(false && "Should never be called!");
        throw; // to avoid warning.
    }


    InterfaceNS::Nature Segment::GetInteriorInterface()
    {
        return InterfaceNS::Nature::edge;
    }


    ::MoReFEM::TopologyNS::Type Segment::GetType() noexcept
    {
        return ::MoReFEM::TopologyNS::Type::segment;
    }


    bool Segment::IsInside(const LocalCoords& coords)
    {
        const auto& coords_list = GetQ1LocalCoordsList();
        assert(coords_list.size() == 2ul);

        return coords.r() >= coords_list[0].r() && coords.r() <= coords_list[1].r();
    }


} // namespace MoReFEM::RefGeomEltNS::TopologyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
