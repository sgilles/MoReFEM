// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <array>
#include <functional>

#include "Geometry/Coords/LocalCoords.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Instances/Segment/ShapeFunction/Segment2.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/Alias.hpp"
#include "Geometry/RefGeometricElt/Internal/ShapeFunction/ConstantShapeFunction.hpp"


namespace MoReFEM::RefGeomEltNS::ShapeFunctionNS
{


    const std::array<ShapeFunctionType, 2>& Segment2::ShapeFunctionList()
    {
        static std::array<ShapeFunctionType, 2> ret{ { [](const auto& local_coords)
                                                       {
                                                           return 0.5 * (1. - local_coords.r());
                                                       },
                                                       [](const auto& local_coords)
                                                       {
                                                           return 0.5 * (1. + local_coords.r());
                                                       } } };

        return ret;
    };


    const std::array<ShapeFunctionType, 2>& Segment2::FirstDerivateShapeFunctionList()
    {
        static std::array<ShapeFunctionType, 2> ret{ { Constant<-1, 2>(), Constant<1, 2>() } };

        return ret;
    };


    const std::array<ShapeFunctionType, 2>& Segment2::SecondDerivateShapeFunctionList()
    {

        static std::array<ShapeFunctionType, 2> ret{ { Constant<0>(), Constant<0>() } };

        return ret;
    };


} // namespace MoReFEM::RefGeomEltNS::ShapeFunctionNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
