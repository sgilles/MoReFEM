// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //
// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_TOPOLOGYFWD_DOT_HPP_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_TOPOLOGYFWD_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <string> // IWYU pragma: keep // IWYU pragma: export
// IWYU pragma: no_include <iosfwd> // IWYU pragma: export

#include <algorithm>   // IWYU pragma: export
#include <cassert>     // IWYU pragma: export
#include <limits>      // IWYU pragma: export
#include <tuple>       // IWYU pragma: export
#include <type_traits> // IWYU pragma: keep // IWYU pragma: export
#include <utility>     // IWYU pragma: export

#include "Utilities/Numeric/Numeric.hpp" // IWYU pragma: export

#include "Geometry/Interfaces/Advanced/LocalData.hpp"                         // IWYU pragma: export
#include "Geometry/Interfaces/LocalInterface/Internal/IsOnLocalInterface.hpp" // IWYU pragma: export

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_TOPOLOGYFWD_DOT_HPP_
// *** MoReFEM end header guards *** < //
