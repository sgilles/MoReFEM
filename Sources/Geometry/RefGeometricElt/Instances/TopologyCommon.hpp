// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //

// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_TOPOLOGYCOMMON_DOT_HPP_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_TOPOLOGYCOMMON_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>       // IWYU pragma: export
#include <iosfwd>      // IWYU pragma: export
#include <type_traits> // IWYU pragma: keep // IWYU pragma: export
#include <vector>      // IWYU pragma: export

#include "Geometry/Coords/LocalCoords.hpp"                      // IWYU pragma: export
#include "Geometry/Interfaces/EnumInterface.hpp"                // IWYU pragma: export
#include "Geometry/RefGeometricElt/Advanced/ComponentIndex.hpp" // IWYU pragma: export
#include "Geometry/RefGeometricElt/EnumTopology.hpp"            // IWYU pragma: export

// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_INSTANCES_TOPOLOGYCOMMON_DOT_HPP_
// *** MoReFEM end header guards *** < //
