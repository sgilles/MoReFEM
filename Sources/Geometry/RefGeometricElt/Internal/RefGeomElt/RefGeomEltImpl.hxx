// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_REFGEOMELT_REFGEOMELTIMPL_DOT_HXX_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_REFGEOMELT_REFGEOMELTIMPL_DOT_HXX_
// IWYU pragma: private, include "Geometry/RefGeometricElt/Internal/RefGeomElt/RefGeomEltImpl.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Geometry/RefGeometricElt/Internal/RefGeomElt/RefGeomEltImpl.hpp"


#include <cstddef> // IWYU pragma: keep


namespace MoReFEM::Internal::RefGeomEltNS
{


    template<class DerivedT, class ShapeFunctionTraitsT, ::MoReFEM::Concept::TopologyTraitsClass TopologyT>
    constexpr std::size_t RefGeomEltImpl<DerivedT, ShapeFunctionTraitsT, TopologyT>::NshapeFunction()
    {
        return ShapeFunctionTraitsT::ShapeFunctionList().size();
    }


    template<class DerivedT, class ShapeFunctionTraitsT, ::MoReFEM::Concept::TopologyTraitsClass TopologyT>
    constexpr std::size_t RefGeomEltImpl<DerivedT, ShapeFunctionTraitsT, TopologyT>::Ncoordinates()
    {
        return TopologyT::Nderivate_component_;
    }


    template<::MoReFEM::Concept::TopologyTraitsClass TopologyT>
    LocalCoords ComputeBarycenter()
    {
        const auto& coords_list = TopologyT::GetQ1LocalCoordsList();
        const std::size_t dimension = TopologyT::dimension;

        const double inv_Ncoords = 1. / static_cast<double>(coords_list.size());

        std::vector<double> barycenter_coords(dimension, 0.);

        for (std::size_t i = 0ul; i < dimension; ++i)
        {
            double& current_barycenter_coord = barycenter_coords[i];

            for (const auto& coords : coords_list)
                current_barycenter_coord += inv_Ncoords * coords[i];
        }

        return LocalCoords(barycenter_coords);
    }


    template<class DerivedT, class ShapeFunctionTraitsT, ::MoReFEM::Concept::TopologyTraitsClass TopologyT>
    const LocalCoords& RefGeomEltImpl<DerivedT, ShapeFunctionTraitsT, TopologyT>::GetBarycenter()
    {
        static LocalCoords barycenter = ComputeBarycenter<TopologyT>();
        return barycenter;
    }


    template<class DerivedT, class ShapeFunctionTraitsT, ::MoReFEM::Concept::TopologyTraitsClass TopologyT>
    double RefGeomEltImpl<DerivedT, ShapeFunctionTraitsT, TopologyT>::SecondDerivateShapeFunction(
        LocalNodeNS::index_type i,
        Advanced::ComponentNS::index_type icoor,
        Advanced::ComponentNS::index_type jcoor,
        const LocalCoords& local_coords)
    {
        auto Ncoor = ShapeFunctionTraitsT::Nderivate_component_;
        assert(i.Get() < NshapeFunction() && icoor.Get() < Ncoor && jcoor.Get() < Ncoor);
        return ShapeFunctionTraitsT::SecondDerivateShapeFunctionList()[(i.Get() * Ncoor + icoor.Get()) * Ncoor
                                                                       + jcoor.Get()](local_coords);
    }


} // namespace MoReFEM::Internal::RefGeomEltNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_INTERNAL_REFGEOMELT_REFGEOMELTIMPL_DOT_HXX_
// *** MoReFEM end header guards *** < //
