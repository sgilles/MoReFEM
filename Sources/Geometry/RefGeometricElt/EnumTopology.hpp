// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_REFGEOMETRICELT_ENUMTOPOLOGY_DOT_HPP_
#define MOREFEM_GEOMETRY_REFGEOMETRICELT_ENUMTOPOLOGY_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <iosfwd>


namespace MoReFEM::TopologyNS
{

    /*!
     * \brief Identifier of the topology.
     */
    enum class Type { point, segment, triangle, tetrahedron, quadrangle, hexahedron };


    /*!
     * \copydoc doxygen_hide_std_stream_out_overload
     *
     * The name of the underlying topology is written in lower case, for instance "triangle".
     *
     */
    std::ostream& operator<<(std::ostream& out, const Type rhs);


} // namespace MoReFEM::TopologyNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_REFGEOMETRICELT_ENUMTOPOLOGY_DOT_HPP_
// *** MoReFEM end header guards *** < //
