// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_COORDS_INTERNAL_COORDINDEXES_DOT_HXX_
#define MOREFEM_GEOMETRY_COORDS_INTERNAL_COORDINDEXES_DOT_HXX_
// IWYU pragma: private, include "Geometry/Coords/Internal/CoordIndexes.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Geometry/Coords/Internal/CoordIndexes.hpp"


#include <cassert>
#include <optional>

#include "Geometry/Coords/StrongType.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Internal::CoordsNS { enum class DoCheckFirstCall; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::CoordsNS
{


    template<DoCheckFirstCall DoCheckFirstCallT>
    void CoordIndexes::SetProgramWisePosition(::MoReFEM::CoordsNS::program_wise_position value) noexcept
    {
        if constexpr (DoCheckFirstCallT == DoCheckFirstCall::yes)
            assert(!program_wise_position_.has_value() && "Should be assigned only once!");

        program_wise_position_ = value;

        if constexpr (DoCheckFirstCallT == DoCheckFirstCall::yes)
            assert(!processor_wise_position_.has_value() && "Should also not have been assigned before!");

        processor_wise_position_ = ::MoReFEM::CoordsNS::processor_wise_position(value.Get());
    }


    template<DoCheckFirstCall DoCheckFirstCallT>
    void CoordIndexes::SetProcessorWisePosition(::MoReFEM::CoordsNS::processor_wise_position value) noexcept
    {
        if constexpr (DoCheckFirstCallT == DoCheckFirstCall::yes)
        {
            assert(was_set_position_in_reduced_coord_list_already_called_ == false && "Should be assigned only once!");

#ifndef NDEBUG
            was_set_position_in_reduced_coord_list_already_called_ = true;
#endif // NDEBUG
        }

        processor_wise_position_ = value;
    }


    inline ::MoReFEM::CoordsNS::index_from_mesh_file CoordIndexes::GetFileIndex() const noexcept
    {
        assert(file_index_.has_value());
        return file_index_.value();
    }


    inline ::MoReFEM::CoordsNS::program_wise_position CoordIndexes::GetProgramWisePosition() const noexcept
    {
        assert(program_wise_position_ != std::nullopt);
        return program_wise_position_.value();
    }


    inline ::MoReFEM::CoordsNS::processor_wise_position CoordIndexes::GetProcessorWisePosition() const noexcept
    {
        assert(processor_wise_position_.has_value());
        return processor_wise_position_.value();
    }


} // namespace MoReFEM::Internal::CoordsNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_COORDS_INTERNAL_COORDINDEXES_DOT_HXX_
// *** MoReFEM end header guards *** < //
