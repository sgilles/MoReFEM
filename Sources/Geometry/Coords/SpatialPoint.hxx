// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_COORDS_SPATIALPOINT_DOT_HXX_
#define MOREFEM_GEOMETRY_COORDS_SPATIALPOINT_DOT_HXX_
// IWYU pragma: private, include "Geometry/Coords/SpatialPoint.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Geometry/Coords/SpatialPoint.hpp"


#include <algorithm>
#include <array>
#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <tuple>
#include <type_traits> // IWYU pragma: keep


namespace MoReFEM
{


    template<class T>
    SpatialPoint::SpatialPoint(T&& array, const double space_unit)
    {
        using T_without_reference = typename std::remove_reference_t<T>;

        static_assert(std::is_floating_point<typename T_without_reference::value_type>::value,
                      "T must be an array of floating-point type!");

        static_assert(std::tuple_size<T_without_reference>::value == 3u,
                      "T must be a std::array of size 3. The condition is necessary but not "
                      "enough to ensure this, but it's a good start (the existence of iterators also helps below).");

        std::transform(array.cbegin(),
                       array.cend(),
                       coordinate_list_.begin(),
                       [space_unit](auto value)
                       {
                           return static_cast<double>(value) * space_unit;
                       });
    }


    inline double SpatialPoint::x() const
    {
        return coordinate_list_[0];
    }


    inline double SpatialPoint::y() const
    {
        return coordinate_list_[1];
    }


    inline double SpatialPoint::z() const
    {
        return coordinate_list_[2];
    }


    inline double SpatialPoint::operator[](std::size_t index) const
    {
        assert(index < coordinate_list_.size());
        return coordinate_list_[index];
    }


    inline double& SpatialPoint::GetNonCstValue(std::size_t index)
    {
        assert(index < coordinate_list_.size());
        return coordinate_list_[index];
    }


    inline const std::array<double, 3>& SpatialPoint::GetCoordinateList() const noexcept
    {
        return coordinate_list_;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_COORDS_SPATIALPOINT_DOT_HXX_
// *** MoReFEM end header guards *** < //
