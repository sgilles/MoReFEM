// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <array>
#include <cassert>
#include <cmath>
#include <cstddef> // IWYU pragma: keep
#include <istream>
#include <string_view>


#include "Utilities/Containers/Print.hpp"
#include "Utilities/Numeric/Numeric.hpp"

#include "Geometry/Coords/SpatialPoint.hpp"


namespace MoReFEM
{


    SpatialPoint::~SpatialPoint() = default;


    SpatialPoint::SpatialPoint()
    {
        coordinate_list_.fill(0.);
    }


    SpatialPoint::SpatialPoint(double x, double y, double z, const double space_unit)
    : coordinate_list_({ { x * space_unit, y * space_unit, z * space_unit } })
    { }


    SpatialPoint::SpatialPoint(std::size_t Ncoor, std::istream& stream, const double space_unit)
    {
        assert(Ncoor <= 3u);

        std::array<double, 3ul> buf_coordinates;
        buf_coordinates.fill(0.);

        auto initialPosition = stream.tellg();

        for (std::size_t i = 0ul; i < Ncoor; ++i)
        {
            stream >> buf_coordinates[i];
            buf_coordinates[i] *= space_unit;
        }

        if (stream)
        {
            // Modify point only if failbit not set.
            coordinate_list_ = buf_coordinates;
        } else
        {
            // In case of failure, put back the cursor at the point it was before trying unsuccessfully to read a point
            auto state = stream.rdstate();
            stream.clear();
            stream.seekg(initialPosition);
            stream.setstate(state);
        }
    }


    void SpatialPoint::Print(std::ostream& out) const
    {
        Utilities::PrintContainer<>::Do(coordinate_list_,
                                        out,
                                        PrintNS::Delimiter::separator(", "),
                                        PrintNS::Delimiter::opener("("),
                                        PrintNS::Delimiter::closer(")"));
    }


    double Distance(const SpatialPoint& point1, const SpatialPoint& point2)
    {
        double sum = 0.;

        for (auto i = 0ul; i < 3u; ++i)
            sum += NumericNS::Square(point1[i] - point2[i]);

        return std::sqrt(sum);
    }


    void SpatialPoint::Reset()
    {
        coordinate_list_.fill(0.);
    }


    std::ostream& operator<<(std::ostream& stream, const SpatialPoint& point)
    {
        point.Print(stream);
        return stream;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
