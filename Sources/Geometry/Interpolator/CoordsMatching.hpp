// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_INTERPOLATOR_COORDSMATCHING_DOT_HPP_
#define MOREFEM_GEOMETRY_INTERPOLATOR_COORDSMATCHING_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <array>
#include <cstddef> // IWYU pragma: keep
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

#include "Utilities/Containers/Array.hpp"
#include "Utilities/UniqueId/UniqueId.hpp" // IWYU pragma: export

#include "Geometry/Coords/StrongType.hpp"
#include "Geometry/Mesh/UniqueId.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::FilesystemNS { class File; }
namespace MoReFEM::Internal::MeshNS { class CoordsMatchingManager; }
namespace MoReFEM::NonConformInterpolatorNS { class FromCoordsMatching; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::MeshNS::InterpolationNS
{


    /*!
     * \brief Interpolation between vertices of two meshes as given by an ad hoc file.
     *
     * This class is built upon the data read in the input data file, and is used only within
     * \a FromCoordsMatching interpolator.
     *
     * \attention #1630 Works only for P1 geometry!
     *
     * \internal This class doesn't use a unique id (it wouldn't be practical as an entry in the Lua file may result in two \a CoordsMatching
     * objects).
     */
    class CoordsMatching
    {

      public:
        //! \copydoc doxygen_hide_alias_self
        using self = CoordsMatching;

        //! Alias for unique pointer.
        using const_unique_ptr = std::unique_ptr<const self>;

        //! Alias for vector of unique pointers.
        using vector_const_unique_ptr = std::vector<const_unique_ptr>;

        //! Frienship to the class that actually requires \a CoordsMatching information.
        friend class NonConformInterpolatorNS::FromCoordsMatching;

        //! Friendship
        friend class MoReFEM::Internal::MeshNS::CoordsMatchingManager;

        //! The type of the underlying map between source and target indexes.
        // clang-format off
                using mapping_type =
                    std::unordered_map
                    <
                        ::MoReFEM::CoordsNS::index_from_mesh_file,
                        ::MoReFEM::CoordsNS::index_from_mesh_file
                    >;
        // clang-format on

        //! Name of the class.
        static const std::string& ClassName();

      public:
        /// \name Special members.
        ///@{

        /*!
         * \brief Constructor from an input data file.
         *
         * \param[in] interpolation_file  File that gives for each Coords on the first mesh on the interface the index of
         * the equivalent Coords in the second mesh.
         */
        explicit CoordsMatching(const FilesystemNS::File& interpolation_file);

        //! Destructor.
        ~CoordsMatching() = default;

      private:
        //! \copydoc doxygen_hide_copy_constructor
        CoordsMatching(const CoordsMatching& rhs) = default;

        //! \copydoc doxygen_hide_move_constructor
        CoordsMatching(CoordsMatching&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        CoordsMatching& operator=(const CoordsMatching& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        CoordsMatching& operator=(CoordsMatching&& rhs) = delete;

        ///@}

      public:
        //! Returns the source index that matches the given \a target_index.
        //! \param[in] target_index Index used as filter.
        CoordsNS::index_from_mesh_file FindSourceIndex(CoordsNS::index_from_mesh_file target_index) const;

        //! Returns thetarget index that matches the given \a source_index.
        //! \param[in] source_index Index used as filter.
        CoordsNS::index_from_mesh_file FindTargetIndex(CoordsNS::index_from_mesh_file source_index) const;

        /*!
         * \brief Returns the source indexes that matches the given \a target_indexes.
         *
         * \param[in] target_indexes Indexes used as filter.
         *
         * \return The match of \a target_indexes on the source side, in exactly the same order.
         */
        std::vector<CoordsNS::index_from_mesh_file>
        FindSourceIndex(const std::vector<CoordsNS::index_from_mesh_file>& target_indexes) const;

        /*!
         * \brief Returns the target indexes that matches the given \a source_indexes.
         *
         * \param[in] source_indexes Indexes used as filter.
         *
         * \return The match of \a source_indexes on the target side, in exactly the same order.
         */
        std::vector<CoordsNS::index_from_mesh_file>
        FindTargetIndex(const std::vector<CoordsNS::index_from_mesh_file>& source_indexes) const;

        //! Returns the unique id of the "source" mesh.
        MeshNS::unique_id GetSourceMeshId() const noexcept;

        //! Returns the unique id of the "target" mesh.
        MeshNS::unique_id GetTargetMeshId() const noexcept;

        //! Returns the pair of unique ids source/target.
        const std::array<MeshNS::unique_id, 2ul>& GetMeshIds() const noexcept;

      private:
        //! Mapping source -> target (i.e. given a source index what is the mapped \a Coords in the target
        //! mesh).
        const mapping_type& GetSourceToTarget() const noexcept;

        //! Mapping target -> source (i.e. given a target index what is the mapped \a Coords in the source
        //! mesh).
        const mapping_type& GetTargetToSource() const noexcept;


        //! Method that does the bulk of constructor job. Should not be called out of constructor.
        //! \param[in] filename Name of the file in which the information are stored.
        void Read(const FilesystemNS::File& filename);

        /*!
         * \brief Set the meshes involved in the interpolation from the line in the interpolation file.
         *
         * \param[in] filename Filename of the interpolation file.Used only to enrich message in exceptions.
         * \param[in] line The expected line in the interpolation file.
         *
         * The line should look like:
         * 'Meshes: 1 2'
         * where 1 and 2 are the unique ids of the meshes involved.
         */
        void SetMeshes(const FilesystemNS::File& filename, std::string line);

        //! Generate a new \a CoordsMatching object when source and targets are reversed.
        CoordsMatching::const_unique_ptr GenerateReverseCoordsMatching() const;

      private:
        //! Relationship between vertices of both meshes. The \a i-th element of this vector is matched with the
        //! \a i-th element of \a target_index_list_.
        std::vector<::MoReFEM::CoordsNS::index_from_mesh_file> source_index_list_;

        //! Must be the same size as \a source_index_list_.
        std::vector<::MoReFEM::CoordsNS::index_from_mesh_file> target_index_list_;

        //! Mapping source -> target (i.e. given a source index what is the mapped \a Coords in the target
        //! mesh).
        mapping_type source_to_target_;

        //! Mapping target -> source (i.e. given a target index what is the mapped \a Coords in the source
        //! mesh).
        mapping_type target_to_source_;

        //! Unique ids of the "source"  and "target" meshes respectively..
        std::array<MeshNS::unique_id, 2ul> mesh_ids_ =
            Utilities::FilledWithUninitializedIndex<MeshNS::unique_id, 2ul>();
    };


} // namespace MoReFEM::MeshNS::InterpolationNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Interpolator/CoordsMatching.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_INTERPOLATOR_COORDSMATCHING_DOT_HPP_
// *** MoReFEM end header guards *** < //
