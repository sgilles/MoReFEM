// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cassert>
#include <cstddef> // IWYU pragma: keep
#include <functional>
#include <memory>
#include <sstream>
#include <string> // IWYU pragma: keep
#include <string_view>
#include <unordered_map>
#include <unordered_set>
#include <utility>

#include "Utilities/Containers/Print.hpp"
#include "Utilities/Containers/PrintPolicy/Key.hpp"
#include "Utilities/Containers/UnorderedMap.hpp"

#include "Geometry/Coords/Coords.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltEnum.hpp"
#include "Geometry/GeometricElt/Advanced/GeometricEltFactory.hpp"
#include "Geometry/GeometricElt/GeometricElt.hpp"
#include "Geometry/GeometricElt/Internal/Exceptions/GeometricEltFactory.hpp"
#include "Geometry/RefGeometricElt/RefGeomElt.hpp"


namespace MoReFEM::Advanced
{


    GeometricEltFactory::~GeometricEltFactory() = default;


    const std::string GeometricEltFactory::ClassName()
    {
        static const std::string ret("GeometricEltFactory");
        return ret;
    }


    GeometricEltFactory::GeometricEltFactory()
    {
        callbacks_ensight_.max_load_factor(Utilities::DefaultMaxLoadFactor());
        callbacks_.max_load_factor(Utilities::DefaultMaxLoadFactor());
        geom_ref_elt_type_list_.max_load_factor(Utilities::DefaultMaxLoadFactor());
        geometric_elt_name_list_.max_load_factor(Utilities::DefaultMaxLoadFactor());
        ensight_name_matching_.max_load_factor(Utilities::DefaultMaxLoadFactor());
    }


    GeometricElt::unique_ptr
    GeometricEltFactory ::CreateFromEnsightName(MeshNS::unique_id mesh_unique_id,
                                                const Coords::vector_shared_ptr& mesh_coords_list,
                                                const Advanced::GeomEltNS::EnsightName& geometric_elt_name,
                                                std::istream& stream) const
    {
        CallBackEnsight::const_iterator it = callbacks_ensight_.find(geometric_elt_name);

        if (it == callbacks_ensight_.cend())
            throw ExceptionNS::Factory::GeometricElt::InvalidEnsightGeometricEltName(geometric_elt_name);

        auto ret = it->second(mesh_unique_id, mesh_coords_list, stream);

        // If stream state is invalid return nullptr instead!
        if (stream)
            return ret;

        return nullptr;
    }


    RefGeomElt::shared_ptr GeometricEltFactory ::GetRefGeomEltPtr(Advanced::GeometricEltEnum identifier) const
    {
        auto it = geom_ref_elt_type_list_.find(identifier);

        if (it == geom_ref_elt_type_list_.cend())
            throw ExceptionNS::Factory::GeometricElt::InvalidGeometricEltId(identifier);

        const auto& pointer = it->second;
        assert(!(!pointer));

        return pointer;
    }


    Advanced::GeometricEltEnum
    GeometricEltFactory ::GetIdentifier(const Advanced::GeomEltNS::GenericName& geometric_reference_name) const
    {
        auto it = match_name_enum_.find(geometric_reference_name);

        if (it == match_name_enum_.cend())
            throw ExceptionNS::Factory::GeometricElt::InvalidGeometricEltName(geometric_reference_name);

        return it->second;
    }


    const std::string GeometricEltFactory::EnsightGeometricEltNames() const
    {
        if (callbacks_ensight_.empty())
            throw MoReFEM::Exception("No Ensight geometric elements defined!!!");

        std::ostringstream oconv;
        Utilities::PrintContainer<Utilities::PrintPolicyNS::Key>::Do(callbacks_ensight_,
                                                                     oconv,
                                                                     PrintNS::Delimiter::separator(", "),
                                                                     PrintNS::Delimiter::opener("["),
                                                                     PrintNS::Delimiter::closer("]"));

        return oconv.str();
    }


    GeometricElt::unique_ptr GeometricEltFactory::CreateFromIdentifier(Advanced::GeometricEltEnum identifier,
                                                                       MeshNS::unique_id mesh_unique_id) const
    {
        const auto it = callbacks_.find(identifier);

        if (it == callbacks_.cend())
            throw ExceptionNS::Factory::GeometricElt::InvalidGeometricEltId(identifier);

        return it->second(mesh_unique_id);
    }


    const RefGeomElt&
    GeometricEltFactory::GetRefGeomElt(const Advanced::GeomEltNS::GenericName& ref_geom_elt_name) const
    {
        auto identifier = GetIdentifier(ref_geom_elt_name);

        return GetRefGeomElt(identifier);
    }


    const RefGeomElt& GeometricEltFactory::GetRefGeomElt(const GeomEltNS::EnsightName& ensight_name) const
    {
        auto it = ensight_name_matching_.find(ensight_name);

        assert(it != ensight_name_matching_.cend());
        return GetRefGeomElt(it->second);
    }


    void GeometricEltFactory::CheckNotEmpty()
    {
        if (callbacks_.empty())
        {
            std::ostringstream oconv;
            oconv << "Content of the GeometricEltFactory is empty: this means no kind of RefGeometricElt is "
                     "registered at all in MoReFEM library... This may happen if the build settings are not right: "
                     "MoReFEM uses up a design pattern which loads the available RefGeometricElement but this might "
                     "be "
                     "optimized away by the compiler if the proper options are not properly set (this is handled by "
                     "WHOLE_ARCHIVE predefined feature in the CMake files. You should first have a look at an existing "
                     "CMakeLists.txt if this is  "
                     "happening in a test (for instance if there is a direct dependency of the executable upon "
                     "${MOREFEM_EXTENSIVE_TEST_TOOLS} putting the flags mentioned above is counter-productive on some "
                     "architectures). In any case, don't hesitate to open an issue "
                     "(https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM/-/issues) if you meet the problem and "
                     "can't figure out what goes wrong.";

            throw Exception(oconv.str());
        }
    }


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
