// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_GEOMETRICELT_INSTANCES_QUADRANGLE_QUADRANGLE8_DOT_HPP_
#define MOREFEM_GEOMETRY_GEOMETRICELT_INSTANCES_QUADRANGLE_QUADRANGLE8_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Geometry/GeometricElt/Instances/FwdForHpp.hpp"
#include "Geometry/RefGeometricElt/Instances/Quadrangle/Traits/Quadrangle8.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }
namespace MoReFEM::RefGeomEltNS { class Quadrangle8; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    /*!
     * \brief A Quadrangle8 geometric element read in a mesh.
     *
     * We are not considering here a generic Quadrangle8 object (that's the role of MoReFEM::RefGeomEltNS::Quadrangle8),
     * but rather a specific geometric element of the mesh, with for instance the coordinates of its coord, the list of
     * its interfaces and so forth.
     */
    class Quadrangle8 final : public Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Quadrangle8>
    {
      public:
        //! \copydoc doxygen_hide_geom_elt_minimal_constructor
        explicit Quadrangle8(MeshNS::unique_id mesh_unique_id);

        //! \copydoc doxygen_hide_geom_elt_stream_constructor
        explicit Quadrangle8(MeshNS::unique_id mesh_unique_id,
                             const Coords::vector_shared_ptr& mesh_coords_list,
                             std::istream& stream);

        //! \copydoc doxygen_hide_geom_elt_vector_coords_constructor
        explicit Quadrangle8(MeshNS::unique_id mesh_unique_id,
                             const Coords::vector_shared_ptr& mesh_coords_list,
                             std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>&& coords_index_list);

        //! Destructor.
        ~Quadrangle8() override;

        //! \copydoc doxygen_hide_copy_constructor
        Quadrangle8(const Quadrangle8& rhs) = delete;

        //! \copydoc doxygen_hide_move_constructor
        Quadrangle8(Quadrangle8&& rhs) = delete;

        //! \copydoc doxygen_hide_copy_affectation
        Quadrangle8& operator=(const Quadrangle8& rhs) = delete;

        //! \copydoc doxygen_hide_move_affectation
        Quadrangle8& operator=(Quadrangle8&& rhs) = delete;

        //! Reference geometric element.
        virtual const RefGeomElt& GetRefGeomElt() const override final;

      private:
        //! Reference geometric element.
        static const RefGeomEltNS::Quadrangle8& StaticRefGeomElt();
    };


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_GEOMETRICELT_INSTANCES_QUADRANGLE_QUADRANGLE8_DOT_HPP_
// *** MoReFEM end header guards *** < //
