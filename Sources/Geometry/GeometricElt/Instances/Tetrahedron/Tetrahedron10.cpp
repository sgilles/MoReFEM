// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


#include <cstddef> // IWYU pragma: keep
#include <iostream>
#include <utility>

#include "Utilities/Warnings/Pragma.hpp"

#include "Geometry/GeometricElt/Instances/FwdForCpp.hpp"
#include "Geometry/GeometricElt/Instances/Tetrahedron/Tetrahedron10.hpp"
#include "Geometry/GeometricElt/Internal/Exceptions/GeometricElt.hpp"
#include "Geometry/RefGeometricElt/Instances/Tetrahedron/Format/Tetrahedron10.hpp" // IWYU pragma: keep
#include "Geometry/RefGeometricElt/Instances/Tetrahedron/Tetrahedron10.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM
{


    // The anonymous namespace is extremely important here: function is invisible from other modules
    namespace // anonymous
    {


        auto CreateEnsight(::MoReFEM::MeshNS::unique_id mesh_unique_id,
                           const Coords::vector_shared_ptr& mesh_coords_list,
                           std::istream& in)
        {
            return std::make_unique<Tetrahedron10>(mesh_unique_id, mesh_coords_list, in);
        }


        auto CreateMinimal(::MoReFEM::MeshNS::unique_id mesh_unique_id)
        {
            return std::make_unique<Tetrahedron10>(mesh_unique_id);
        }


        // Register the geometric element in the 'GeometricEltFactory' singleton
        // The return value is mandatory: we can't simply call a void function outside function boundaries
        // See "Modern C++ Design", Chapter 8, P205
        __attribute__((unused)) const bool registered = // NOLINT
            Advanced::GeometricEltFactory::CreateOrGetInstance().RegisterGeometricElt<RefGeomEltNS::Tetrahedron10>(
                CreateMinimal,
                CreateEnsight);


    } // anonymous namespace


    PRAGMA_DIAGNOSTIC(push)
    PRAGMA_DIAGNOSTIC(ignored "-Wmissing-noreturn")

    Tetrahedron10::Tetrahedron10(MeshNS::unique_id mesh_unique_id)
    : Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Tetrahedron10>(mesh_unique_id)
    {
        static bool is_first_call = true;

        if (is_first_call)
        {
            ExceptionNS::GeometricElt::UnsupportedP2Q2 exception;
            std::cerr << "[WARNING] " << exception.GetRawMessage() << std::endl;
        }
    }


    Tetrahedron10::Tetrahedron10(MeshNS::unique_id mesh_unique_id,
                                 const Coords::vector_shared_ptr& mesh_coords_list,
                                 std::istream& stream)
    : Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Tetrahedron10>(mesh_unique_id, mesh_coords_list, stream)
    {
        static bool is_first_call = true;

        if (is_first_call)
        {
            ExceptionNS::GeometricElt::UnsupportedP2Q2 exception;
            std::cerr << "[WARNING] " << exception.GetRawMessage() << std::endl;
        }
    }


    Tetrahedron10::Tetrahedron10(MeshNS::unique_id mesh_unique_id,
                                 const Coords::vector_shared_ptr& mesh_coords_list,
                                 std::vector<::MoReFEM::CoordsNS::index_from_mesh_file>&& coords_index_list)
    : Internal::GeomEltNS::TGeometricElt<RefGeomEltNS::Traits::Tetrahedron10>(mesh_unique_id,
                                                                              mesh_coords_list,
                                                                              std::move(coords_index_list))
    {
        static bool is_first_call = true;

        if (is_first_call)
        {
            ExceptionNS::GeometricElt::UnsupportedP2Q2 exception;
            std::cerr << "[WARNING] " << exception.GetRawMessage() << std::endl;
        }
    }

    PRAGMA_DIAGNOSTIC(pop)


    Tetrahedron10::~Tetrahedron10() = default;


    const RefGeomElt& Tetrahedron10::GetRefGeomElt() const
    {
        return StaticRefGeomElt();
    }


    const RefGeomEltNS::Tetrahedron10& Tetrahedron10::StaticRefGeomElt()
    {
        static RefGeomEltNS::Tetrahedron10 ret;
        return ret;
    }


} // namespace MoReFEM


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //
