// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_DOMAIN_INTERNAL_DOMAINHELPER_DOT_HPP_
#define MOREFEM_GEOMETRY_DOMAIN_INTERNAL_DOMAINHELPER_DOT_HPP_
// *** MoReFEM header guards *** < //

#include <cstddef> // IWYU pragma: keep

#include "Geometry/Domain/MeshLabel.hpp"


// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM { class GeometricElt; }
namespace MoReFEM { class RefGeomElt; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Internal::DomainNS
{


    /*!
     * \brief Returns whether the \a geometric_elt belongs to the mesh identified by
     * \a mesh_identifier.
     *
     * \param[in] geometric_elt Geometric element upon which the test is performed.
     * \param[in] mesh_identifier Identifier of the mesh tested.
     *
     * \return Whether \a geometric_elt belongs to the \a Mesh which id is
     * mesh_identifier.
     */
    bool IsObjectInMesh(const GeometricElt& geometric_elt, ::MoReFEM::MeshNS::unique_id mesh_identifier);

    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    /*!
     * \brief A function to make the code compile.
     *
     * It should never be called in runtime!
     */
    [[noreturn]] bool IsObjectInMesh(const RefGeomElt&, ::MoReFEM::MeshNS::unique_id);
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN


    /*!
     * \brief Returns whether \a geometric_element's MeshLabel is in \a mesh_label_list.
     *
     * \param[in] geometric_elt Geometric element upon which the test is performed.
     * \param[in] mesh_label_list Mesh labels in the domain.
     *
     * \return Whether \a geometric_elt belongs to any of the \a MeshLabel cited in \a mesh_label_list.
     */
    bool IsMeshLabelInList(const GeometricElt& geometric_elt,
                           const MeshLabel::vector_const_shared_ptr& mesh_label_list);


    //! \cond IGNORE_BLOCK_IN_DOXYGEN
    /*!
     * \brief A function to make the code compile.
     *
     * It should never be called in runtime!
     */
    [[noreturn]] bool IsMeshLabelInList(const RefGeomElt&, const MeshLabel::vector_const_shared_ptr&);
    //! \endcond IGNORE_BLOCK_IN_DOXYGEN


} // namespace MoReFEM::Internal::DomainNS


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


#include "Geometry/Domain/Internal/DomainHelper.hxx" // IWYU pragma: export


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_DOMAIN_INTERNAL_DOMAINHELPER_DOT_HPP_
// *** MoReFEM end header guards *** < //
