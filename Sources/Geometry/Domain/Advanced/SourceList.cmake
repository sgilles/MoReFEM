### ===================================================================================
### This file is generated automatically by Scripts/generate_cmake_source_list.py.
### Do not edit it manually! 
### Convention is that:
###   - When a CMake file is manually managed, it is named canonically CMakeLists.txt.
###.  - When it is generated automatically, it is named SourceList.cmake.
### ===================================================================================


target_sources(${MOREFEM_GEOMETRY}

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/LightweightDomainList.cpp
		${CMAKE_CURRENT_LIST_DIR}/LightweightDomainListManager.cpp

	PRIVATE
		${CMAKE_CURRENT_LIST_DIR}/Criterion.hpp
		${CMAKE_CURRENT_LIST_DIR}/LightweightDomainList.hpp
		${CMAKE_CURRENT_LIST_DIR}/LightweightDomainList.hxx
		${CMAKE_CURRENT_LIST_DIR}/LightweightDomainListManager.hpp
		${CMAKE_CURRENT_LIST_DIR}/LightweightDomainListManager.hxx
		${CMAKE_CURRENT_LIST_DIR}/SourceList.cmake
		${CMAKE_CURRENT_LIST_DIR}/UniqueId.hpp
)

