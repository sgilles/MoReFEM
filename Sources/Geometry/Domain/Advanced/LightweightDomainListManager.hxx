// > *** MoReFEM Doxygen information and copyright notice *** //
/*!
 * This file is part of the [MoReFEM](https://gitlab.inria.fr/MoReFEM/CoreLibrary/MoReFEM) library.
 * It is released under the LGPL v3 license; a copy is provided in the LICENSE file shipped with it.
 *
 * MoReFEM is mostly developed by [M3DISIM team-project](https://www.inria.fr/m3disim).
 * Contacts are [Sebastien Gilles](mailto:sebastien.gilles@inria.fr) and [Jerome Diaz](mailto:jerome.diaz@inria.fr).
 *
 * \file
 *
 * \ingroup GeometryGroup
 * \addtogroup GeometryGroup
 * @{
 */
// *** MoReFEM Doxygen information and copyright notice *** < //


// > *** MoReFEM header guards *** //
#ifndef MOREFEM_GEOMETRY_DOMAIN_ADVANCED_LIGHTWEIGHTDOMAINLISTMANAGER_DOT_HXX_
#define MOREFEM_GEOMETRY_DOMAIN_ADVANCED_LIGHTWEIGHTDOMAINLISTMANAGER_DOT_HXX_
// IWYU pragma: private, include "Geometry/Domain/Advanced/LightweightDomainListManager.hpp"
// *** MoReFEM header guards *** < //

// IWYU pragma: private, include "Geometry/Domain/Advanced/LightweightDomainListManager.hpp"


#include <algorithm>
#include <cstddef> // IWYU pragma: keep
#include <vector>

#include "Utilities/InputData/Concept.hpp"
#include "Utilities/InputData/Extract.hpp"

#include "Geometry/Domain/UniqueId.hpp" // IWYU pragma: export
#include "Geometry/Mesh/UniqueId.hpp"

// ============================
// clang-format off
//! \cond IGNORE_BLOCK_IN_DOXYGEN
// Forward declarations.
// ============================

namespace MoReFEM::Advanced { class LightweightDomainList; }

// ============================
// End of forward declarations.
//! \endcond IGNORE_BLOCK_IN_DOXYGEN
// clang-format on
// ============================


namespace MoReFEM::Advanced
{


    // clang-format off
    template
    <
        class IndexedSectionDescriptionT,
        ::MoReFEM::Concept::ModelSettingsType ModelSettingsT,
        ::MoReFEM::Concept::InputDataType InputDataT
    >
    // clang-format on
    void LightweightDomainListManager ::Create(const IndexedSectionDescriptionT&,
                                               const ModelSettingsT& model_settings,
                                               const InputDataT& input_data)
    {
        using section_type = typename IndexedSectionDescriptionT::enclosing_section_type;

        const auto mesh_index = ::MoReFEM::MeshNS::unique_id{
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::MeshIndex>(model_settings, input_data)
        };
        decltype(auto) number_in_domain_list =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::NumberInDomainList>(model_settings, input_data);
        decltype(auto) raw_domain_unique_id_list =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::DomainIndexList>(model_settings, input_data);
        decltype(auto) mesh_label_list =
            ::MoReFEM::InputDataNS::ExtractLeaf<typename section_type::MeshLabelList>(model_settings, input_data);

        std::vector<::MoReFEM::DomainNS::unique_id> domain_unique_id_list(raw_domain_unique_id_list.size());

        std::transform(raw_domain_unique_id_list.cbegin(),
                       raw_domain_unique_id_list.cend(),
                       domain_unique_id_list.begin(),
                       [](const std::size_t index)
                       {
                           return ::MoReFEM::DomainNS::unique_id{ index };
                       });

        Create(section_type::GetUniqueId(), mesh_index, domain_unique_id_list, mesh_label_list, number_in_domain_list);
    }


    inline LightweightDomainList& LightweightDomainListManager::GetNonCstLightweightDomainList(std::size_t unique_id)
    {
        return const_cast<LightweightDomainList&>(GetLightweightDomainList(unique_id));
    }


    inline const LightweightDomainListManager::storage_type&
    LightweightDomainListManager::GetLightweightDomainListStorage() const noexcept
    {
        return lightweight_domain_list_storage_;
    }


    inline auto LightweightDomainListManager::GetNonCstLightweightDomainListStorage() noexcept -> storage_type&
    {
        return const_cast<storage_type&>(GetLightweightDomainListStorage());
    }


} // namespace MoReFEM::Advanced


// > *** MoReFEM Doxygen end of group *** //
///@} // \addtogroup GeometryGroup
// *** MoReFEM Doxygen end of group *** < //


// > *** MoReFEM end header guards *** //
#endif // MOREFEM_GEOMETRY_DOMAIN_ADVANCED_LIGHTWEIGHTDOMAINLISTMANAGER_DOT_HXX_
// *** MoReFEM end header guards *** < //
