
# #################################################################################################################
# Tests can be sort in two broad categories:
#
# - Tests that essentially run a MoReFEM executable, typically on a dedicated Lua file named demo.lua or something
# similar.
# - Tests that use up the generic Boost Test infrastructure: no main are explicitly involved here.
# 
# The way to propagate the command line arguments aren't the same:
# - MoReFEM executables typically use TCLAP library, which provides an interface similar to what argparse provides
# for the Python language
# - Boost test executables use up positional arguments.
#
# The syntactic difference could be hard to grasp at first glance, and we also ended up with multiples lines and 
# blocks to deal with variations of the same kind of test (typically many tests are run twice: in sequential and 
# in parallel).
# 
# The point of the following custom commands is therefore twofold:
# - Obfuscate the difference in syntax between the two types of tests.
# - Provide much synthetic calls, with a same command which may under the hood call several `add_test` commands.
#
# #################################################################################################################

include (${CMAKE_CURRENT_LIST_DIR}/TestCustomCommands-Boost.cmake)
include (${CMAKE_CURRENT_LIST_DIR}/TestCustomCommands-RunModel.cmake)

