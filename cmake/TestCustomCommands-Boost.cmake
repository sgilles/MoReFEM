# #################################################################################################################
# TO UNDERSTAND THE TWO DIFFERENT TYPES OF TESTS, PLEASE READ THE COMMENT IN TestCustomCommands.cmake !
# #################################################################################################################

# #################################################################################################################
# Helper function which parses and interpret the arguments provided to the actual function (this one is not 
# expected to be used directly)
#
# - options
#    . BOOST_TEST: I don't like it much, but some tests regarding operators are actually implemented as almost full
# fledged model instances and we nonetheless need to specify outputs should be in the main test output directory, 
# not the directory intended for model instances. In this case, specify this option. This is only used for 
# morefem_boost_test_check_results(); for the other functions here it is ignored.
#
# - oneValueArgs
#
#    . LUA Path to the Lua input file; may use environment variables.
#    . NAME Base name of the test.
#    . EXE Name of the executable considered (created with a `add_executable` command). 
#    . TIMEOUT Time out of the test, in seconds, to avoid dangling program.
#    . NPROC specifies the number of ranks used. For a sequential test the argument is ignored; for a parallel
# test a value of 4 is applied by default if none is specified.
# 
#
# #################################################################################################################

function(morefem_boost_test_helper)
    set(options BOOST_TEST)
    set(oneValueArgs EXE TIMEOUT NAME NPROC LUA)
    set(multiValueArgs "")

    cmake_parse_arguments(MY_COMMAND "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )

    if (NOT MY_COMMAND_NAME)
        message(FATAL_ERROR "Missing NAME argument!")
    endif()

    if (NOT MY_COMMAND_TIMEOUT)
        message(FATAL_ERROR "Missing TIMEOUT argument!")
    endif()

    if (NOT MY_COMMAND_EXE)
        message(FATAL_ERROR "Missing EXE argument!")
    endif()

    if (MY_COMMAND_BOOST_TEST)
        set(is_boost_test True)
    endif()

    set(Nproc ${MY_COMMAND_NPROC} PARENT_SCOPE)
    set(test_name ${MY_COMMAND_NAME} PARENT_SCOPE)
    set(timeout ${MY_COMMAND_TIMEOUT} PARENT_SCOPE)
    set(lua_file ${MY_COMMAND_LUA} PARENT_SCOPE)
    set(executable ${MY_COMMAND_EXE} PARENT_SCOPE)    
    set(is_boost_test ${is_boost_test} PARENT_SCOPE)

endfunction()


# #################################################################################################################
# Function which calls an executable which purpose is to check the results of a given model run, usually by
# perusing the post processed data in Ensight output.
#
# You should have a look at one of the model instance in MoReFEM to see it in action.
# 
# See morefem_boost_test_helper() for arguments.
# #################################################################################################################
function(morefem_boost_test_check_results)

    morefem_boost_test_helper(${ARGN})
    set(command_name ${test_name}CheckResults)

    if (is_boost_test)
        set(output_directory ${MOREFEM_TEST_OUTPUT_DIR}) 
    else()
        set(output_directory ${MOREFEM_MODEL_INSTANCES_OUTPUT_DIR}) 
    endif()

    add_test(${command_name}
            ${executable}
            --
            ${MOREFEM_ROOT}
            ${output_directory}
            ${lua_file})

    set_tests_properties(${command_name} PROPERTIES TIMEOUT ${timeout})

endfunction()


# #################################################################################################################
# Function which calls `add_test` and adds in command line arguments additional intel which may be used in the 
# test (or not if for instance LUA is not specified in the test).
#
# On the three arguments given in command line arguments:
# - The two first ones are required if the test use the TestNS::FixtureNS::TestEnvironment fixture (most of the 
# tests actually do so).
# - The third one is required of the test use the TestNS::FixtureNS::Model fixture (which encompass the 
# TestNS::FixtureNS::TestEnvironment one).
# 
# See morefem_boost_test_helper() for arguments.
# #################################################################################################################
function(morefem_boost_test_sequential_mode)

    morefem_boost_test_helper(${ARGN})
    set(command_name ${test_name})

    add_test(${test_name}
            ${executable}
            --
            ${MOREFEM_ROOT}
            ${MOREFEM_TEST_OUTPUT_DIR}/Seq
            ${lua_file})

    set_tests_properties(${command_name} PROPERTIES TIMEOUT ${timeout})

endfunction()


# #################################################################################################################
# Same as morefem_boost_test_sequential_mode() for parallel mode.
#
# Base test name is augmented by an appropriate suffix to distinguish from a sequential test.
#
# By default 4 ranks are used; if a different amount is needed please use NPROC argument.
# #################################################################################################################
function(morefem_boost_test_parallel_mode)

    morefem_boost_test_helper(${ARGN})
    if (NOT Nproc)
        set(Nproc 4)
        set(proc_dir "Mpi4")
    else()
        set(proc_dir "Mpi${Nproc}")
    endif()

    set(command_name ${test_name}-mpi)

    add_test(${command_name}
            ${OPEN_MPI_INCL_DIR}/../bin/mpirun
            --oversubscribe
            -np ${Nproc}
            ${executable}
            --
            ${MOREFEM_ROOT}
            ${MOREFEM_TEST_OUTPUT_DIR}/${proc_dir}
            ${lua_file})

    set_tests_properties(${command_name} PROPERTIES TIMEOUT ${timeout})

endfunction()


# #################################################################################################################
# Calls both morefem_boost_test_sequential_mode() and morefem_boost_test_parallel_mode().
# #################################################################################################################
function(morefem_boost_test_both_modes)
    morefem_boost_test_sequential_mode(${ARGN})
    morefem_boost_test_parallel_mode(${ARGN})
endfunction()