include(CheckCXXCompilerFlag)


# add a compiler flag only if it is accepted
function(add_cxx_compiler_flag _flag)
  string(REPLACE "-" "_" _flag_var ${_flag})
  check_cxx_compiler_flag("${_flag}" CXX_COMPILER${_flag_var}_OK)

  if(CXX_COMPILER${_flag_var}_OK)
    add_compile_options($<$<COMPILE_LANGUAGE:CXX>:${_flag}>)
  endif()
endfunction()

function(add_cxx_compiler_debug_flag _flag)
  if (${CMAKE_BUILD_TYPE} STREQUAL "Debug")
    add_cxx_compiler_flag(${_flag})
  endif()
endfunction()

function(add_c_compiler_flag _flag)
  string(REPLACE "-" "_" _flag_var ${_flag})
  check_c_compiler_flag("${_flag}" C_COMPILER${_flag_var}_OK)

  if(C_COMPILER${_flag_var}_OK)
    add_compile_options($<$<COMPILE_LANGUAGE:C>:${_flag}>)
  endif()
endfunction()



##########################################################################################
#
# Enables fine tuning of the way files are organized within a IDE.
#
# Basic CMake behaviour puts all in "Source files" and "Header files";  in MoReFEM
# source_group command has been used to follow the filesystem layout.
# However, by default all targets are displayed on the same level and the hierarchy
# can therefore be polluted by the myriad of tests - each of which is a target...
# Calling this function allows targets to be sort in folders, so for instance for
# the example above all test may be put in an aptly named folder in the IDE.
    
# _target The name of the target (typically an executable or a library)
# _folder_in_ide Name of the folder in which the related source files should be put in the IDE.
# _path The path on the filesystem of the directory in which the target is defined. This
# path should be given relatively to the "Sources" directory of MoReFEM.
#
# The most current case is that _folder is one step removed from _folder_in_ide, e.g.:
# 
# morefem_organize_IDE(TestMovemesh Test/FiniteElement/FiniteElementSpace Test/FiniteElement/FiniteElementSpace/Movemesh) 
# 
# The reason is that it would be cumbersome to navigate: CMake creates a level with the
# target name and we would get something like Test/FiniteElement/FiniteElementSpace/Movemesh/test_movemesh/
# so we decided to skip the Movemesh subdir which would contain nothing else.
# However, we may choose to use same value for both in more complicated cases - for instance
# if a test requires a lib or several executables we make it clear in the hierarchy by putting
# the additional level in _path, e.g. for CoordsMatching folder which sports 2 executables:
# - Test/Geometry/CoordsMatching
#      + TestCoordsMatching
#      + TestReverseCoordsMatching
##########################################################################################

function(morefem_organize_IDE _target _folder_in_ide _path)
    set_target_properties(${_target} PROPERTIES FOLDER ${_folder_in_ide})
    get_property(current_module_files TARGET ${_target} PROPERTY SOURCES)
    source_group(TREE "${CMAKE_CURRENT_SOURCE_DIR}/${_path}" FILES ${current_module_files})
endfunction()


##########################################################################################
#
# Find a library and store it in _macro; if not a message error is issued.
    
# Example: morefem_find_library(LIB_OPEN_MPI "mpi" ${OPEN_MPI_LIB_DIR} "Openmpi")    
# _macro Name in which the library is accessible within CMake.
# _libnames Name or names under which the libraries might be found in the paths.
# _dir Directory into which the library should be found. When this macro is called, it is usually with a path set in Paths.cmake.
# _name Name of the library, for the error message. 
#
##########################################################################################
function(morefem_find_library _macro _libnames _dir _name)
    
    find_library(${_macro} ${_libnames} PATHS ${_dir} NO_DEFAULT_PATH)

    if (NOT ${_macro})
         message(FATAL_ERROR "${_name} library not found in path ${_dir}")
    endif()
endfunction()    


# ##########################################################################################
#
# Set the alias related to a module depending on the value of BUILD_MOREFEM_UNIQUE_LIBRARY:
# - If True, the alias is set to morefem, the unique library into which all the code is built.
# - If False, a new (empty at the moment) library is built and the alias points to this library.
# For instance morefem_library_module(MOREFEM_UTILITIES morefem_utilities) will create morefem_utilities and set MOREFEM_UTILITIES to point to this library.
# \param[out] _alias Alias to the library, e.g. MOREFEM_UTILITIES.
# \param[in] module Module considered; if BUILD_MOREFEM_UNIQUE_LIBRARY is False the module library will use that name.
# If True it is not used.
    #
# ##########################################################################################
function(morefem_library_module _alias _module)
    if (BUILD_MOREFEM_UNIQUE_LIBRARY)
        set(${_alias} MoReFEM PARENT_SCOPE)		
    else()		
        add_library(${_module} ${LIBRARY_TYPE} "")
        set(${_alias} ${_module} PARENT_SCOPE)
    endif()
endfunction()


# ##########################################################################################
# A wrapper over install() command which performs the following additional tasks:
# - If the target is an executable, call set_target_properties() to properly define RPATH for the target. This is 
# required at least for shared libraries on macOS.
# - Install the targets in the MoReFEM installation directories.  
# ##########################################################################################
include(GenerateExportHeader)

function(morefem_install)
    foreach(target ${ARGN})
        
        get_target_property(target_type ${target} TYPE)
        if (target_type STREQUAL "EXECUTABLE")
            if (APPLE)
                # Additional step to make shared library work on macOS; see https://gist.github.com/robertmaynard/5750737
                set_target_properties(${target} PROPERTIES INSTALL_RPATH "@loader_path/../lib")
            endif()
        elseif()
            generate_export_header(${target})
        endif ()
        
        install(TARGETS ${target}
                EXPORT "${target}TARGETS"
                RUNTIME DESTINATION ${MOREFEM_INSTALL_DIR_EXE}
                LIBRARY DESTINATION ${MOREFEM_INSTALL_DIR_LIB}
                ARCHIVE DESTINATION ${MOREFEM_INSTALL_DIR_LIB} 
                INCLUDES DESTINATION ${MOREFEM_INSTALL_DIR_INCL}) 
                
        
    endforeach()
endfunction()



# ##########################################################################################
#
# Set the alias related to a module depending on the value of BUILD_MOREFEM_UNIQUE_LIBRARY:
# - If True, the alias is set to morefem, the unique library into which all the code is built.
# - If False, a new (empty at the moment) library is built and the alias points to this library.
# For instance morefem_library_module(MOREFEM_UTILITIES morefem_utilities) will create morefem_utilities and set MOREFEM_UTILITIES to point to this library.
# \param[out] _alias Alias to the library, e.g. MOREFEM_UTILITIES.
# \param[in] module Module considered; if BUILD_MOREFEM_UNIQUE_LIBRARY is False the module library will use that name.
# If True it is not used.
    #
# ##########################################################################################
function(morefem_library_module _alias _module)
    if (BUILD_MOREFEM_UNIQUE_LIBRARY)
        set(${_alias} MoReFEM PARENT_SCOPE)
    else()
        add_library(${_module} ${LIBRARY_TYPE} "")
        set(${_alias} ${_module} PARENT_SCOPE)
    endif()
endfunction()


# ##########################################################################################
# There seems to be a way to get all the source files related to a given target, but what I 
# want when preparing installation part is just the list of header files, which extension is
# in my case .h, .hpp or .hxx.
# This function fulfills this need.
# \param[in] _target The target for which header files are sought.
# \param[out] _list The list of header files found.
# ##########################################################################################
function(extract_header_files _target _list _include_dir)
    get_property(sources TARGET ${_target} PROPERTY SOURCES)
    
    list(APPEND ret "")
    

    
    foreach(source in ${sources})
        string(REGEX MATCH .h$ h ${source})
        string(REGEX MATCH .hpp$ hpp ${source})
        string(REGEX MATCH .hxx$ hxx ${source})  

        if(h OR hpp OR hxx)
            
            string(REPLACE ${_include_dir} "\${MOREFEM_INSTALL_DIR_INCL}" shorter_path ${source})
            list(APPEND ret ${shorter_path})
        endif()
        
    endforeach()
    
    set(${_list} ${ret} PARENT_SCOPE)
    
endfunction() 


# ##########################################################################################
# Apply LTO to target if this is supported by the compiler.
# ##########################################################################################
function(apply_lto_if_supported _target)
    if (NOT ${CMAKE_BUILD_TYPE} STREQUAL "Debug")
        check_ipo_supported(RESULT result)
        if(result)
            message(STATUS "Applying LTO to " ${_target})
            set_property(TARGET ${_target} PROPERTY INTERPROCEDURAL_OPTIMIZATION TRUE)
        endif()
    endif()
endfunction()


include (${CMAKE_CURRENT_LIST_DIR}/TestCustomCommands.cmake)